<?php // Book A Session - Settings Page

if ( ! defined( 'ABSPATH' ) ) exit;

function book_a_session_display_payments_settings_page() {
	
	if ( ! current_user_can( 'manage_options' ) ) return;
	
    ?>
    
	<div class="wrap">
		<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
		<form action="options.php" method="post">
			
			<?php
			
			// Output security fields
			settings_fields( 'book_a_session_options' );
			
			// Output setting sections
			do_settings_sections( 'book_a_session' );
			
			// Submit button
			submit_button();
			
			?>
			
		</form>
	</div>
	
	<?php
	
}