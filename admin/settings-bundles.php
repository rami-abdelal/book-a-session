<?php // Book A Session - Bundles Settings Page

if ( ! defined( 'ABSPATH' ) ) exit;

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
 }

if( ! class_exists( 'Bundles_List_Table' ) ) {
    require_once plugin_dir_path( __FILE__ ) . 'class-bundles-list-table.php';
}

// Edit

if (   ! empty( $_GET['edit'] ) 
    && ! empty( $_GET['quantity'] )
    && ! empty( $_POST['submit_edit_bundle'] ) ) {
    
    if ( current_user_can( "manage_options" ) && wp_verify_nonce( $_REQUEST["_wpnonce"], "book_a_session_edit_bundle_" . $_GET["quantity"] ) ) {

        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_bundle";
        $currency_array = book_a_session_get_table_array( "currency" );
        $bundle_currency_array = book_a_session_get_table_array( "bundle_currency" );
        $table_name_bundle_currency = $wpdb->prefix . "book_a_session_bundle_currency";
        $table_name_bundle_payment_method = $wpdb->prefix . "book_a_session_bundle_payment_method";
        $payment_method_array = book_a_session_get_table_array( "payment_method" );
        $bundle_payment_method_array = book_a_session_get_table_array( "bundle_payment_method" );

        // Validation
    
        if ( ! empty( $_POST['quantity'] ) && ! empty( $_POST['name'] )) {
                
            $value_array = array( 
                'quantity'	=> $_POST['quantity'],
                'name'      => $_POST['name']
            );
            $edit_bundle_db_result[] = $wpdb->update( 
                $table_name, 
                $value_array, 
                array( 'quantity' => $_GET['quantity'] ),
                array( 
                    '%d', 
                    '%s'
                    ) 
                );
            
            if ( $edit_bundle_db_result ) {

                // Discount Edit

                // Loop through currencies

                for ( $i = 0; $i < count( $currency_array ); $i++ ) {

                    // When one isn't empty

                    if ( ! empty( $_POST[ 'currency_' . $currency_array[$i]->id ] ) ) {

                        // Find out if it's already saved in the table

                        $match_found = false;

                        if ( ! empty( $bundle_currency_array ) ) {

                            for ( $j = 0; $j < count( $bundle_currency_array ); $j++ ) {

                                // If it's saved in the table, update it. Record that a match was found and count it to determine overall success later
        
                                if ( $bundle_currency_array[$j]->quantity == $_GET['quantity'] && $bundle_currency_array[$j]->currency_id == $currency_array[$i]->id ) {

                                    $match_found = true;

                                    $bundle_currency_where_array = array(
                                        'quantity'         => $_GET['quantity'], 
                                        'currency_id'       => $currency_array[$i]->id
                                    );

                                    $bundle_currency_where_format_array = array(
                                        '%d',
                                        '%d'
                                    );   

                                    $bundle_currency_value_array = array( 
                                        'quantity' => $_GET['quantity'],
                                        'discount'  => $_POST[ 'currency_' . $currency_array[$i]->id ]
                                    );
                
                                    $bundle_currency_value_format_array = array(
                                        '%d',
                                        '%f'
                                    );   
                                    
                                    $edit_bundle_db_result[] = $wpdb->update( 
                                        $table_name_bundle_currency, 
                                        $bundle_currency_value_array, 
                                        $bundle_currency_where_array, 
                                        $bundle_currency_value_format_array, 
                                        $bundle_currency_where_format_array 
                                    );
            
                                    break;
                                }
        
                            }

                        }

                        // If it doesn't already exist, insert

                        if ( ! $match_found ) {

                            $bundle_currency_value_array = array( 
                                'quantity'     => $_GET['quantity'], 
                                'currency_id'   => $currency_array[$i]->id,
                                'discount'      => $_POST[ 'currency_' . $currency_array[$i]->id ]
                            );
        
                            $bundle_currency_format_array = array(
                                '%d',
                                '%d',
                                '%f'
                            );   
                            
                            $edit_bundle_db_result[] = $wpdb->insert( $table_name_bundle_currency, $bundle_currency_value_array, $bundle_currency_format_array );
        
                        }

                    } else {

                        // If there's no value posted, find out if it's saved in the table

                        $match_found = false;

                        // If there're no discounts there's nothing to delete

                        if ( ! empty( $bundle_currency_array ) ) {

                            for ( $j = 0; $j < count( $bundle_currency_array ); $j++ ) {

                                // But if there are and if it's present, declare that a match was found, delete the record, count it as an edit to determine overall success later and break
        
                                if ( $bundle_currency_array[$j]->quantity == $_GET['quantity'] && $bundle_currency_array[$j]->currency_id == $currency_array[$i]->id ) {

                                    $match_found = true;

                                    $bundle_currency_where_array = array( 
                                        'quantity'     => $_GET['quantity'], 
                                        'currency_id'   => $currency_array[$i]->id
                                    );
                
                                    $bundle_currency_format_array = array(
                                        '%d',
                                        '%d'
                                    );   
                                    
                                    $edit_bundle_db_result[] = $wpdb->delete( $table_name_bundle_currency, $bundle_currency_where_array, $bundle_currency_format_array );  

                                    break;

                                }
        
                            }

                        }

                    }

                }

                // Bundle Payment Method Edit

                // Loop through payment methods

                $count_edit = 0;
                $count_no_edit = 0;

                for ( $i = 0; $i < count( $payment_method_array ); $i++ ) {

                    // When one is checked

                    if ( ! empty( $_POST[ 'payment_method_' . $payment_method_array[$i]->id ] ) ) {

                        // Find out if it's already saved in the table

                        $match_found = false;

                        if ( ! empty( $bundle_payment_method_array ) ) {

                            for ( $j = 0; $j < count( $bundle_payment_method_array ); $j++ ) {

                                // If it's saved in the table, nothing needs to happen to it. Record that a match was found and count it to determine overall success later
        
                                if ( $bundle_payment_method_array[$j]->quantity == $_GET['quantity'] && $bundle_payment_method_array[$j]->payment_method_id == $payment_method_array[$i]->id ) {
                                    $match_found = true;
                                    $count_no_edit++;
                                    break;
                                }
        
                            }

                        }

                        if ( ! $match_found ) {

                            $bundle_payment_method_value_array = array( 
                                'quantity'       => $_GET['quantity'], 
                                'payment_method_id' => $payment_method_array[$i]->id
                            );
        
                            $bundle_payment_method_format_array = array(
                                '%d',
                                '%d'
                            );   
                            
                            $edit_bundle_db_result_array[] = $wpdb->insert( $table_name_bundle_payment_method, $bundle_payment_method_value_array, $bundle_payment_method_format_array );
                            $count_edit++;
        
                        }

                    } else {

                        // If it's not checked, find out if it's saved in the table

                        $match_found = false;

                        if ( ! empty( $bundle_payment_method_array ) ) {

                            for ( $j = 0; $j < count( $bundle_payment_method_array ); $j++ ) {

                                // If it's present, declare that a match was found, delete the record, count it as an edit to determine overall success later and break
        
                                if ( $bundle_payment_method_array[$j]->quantity == $_GET['quantity'] && $bundle_payment_method_array[$j]->payment_method_id == $payment_method_array[$i]->id ) {

                                    $match_found = true;

                                    $bundle_payment_method_value_array = array( 
                                        'quantity'   => $_GET['quantity'], 
                                        'payment_method_id'   => $payment_method_array[$i]->id
                                    );
                
                                    $bundle_payment_method_format_array = array(
                                        '%d',
                                        '%d'
                                    );   
                                    
                                    $edit_bundle_db_result_array[] = $wpdb->delete( $table_name_bundle_payment_method, $bundle_payment_method_value_array, $bundle_payment_method_format_array );  

                                    $count_edit++;

                                    break;

                                }
        
                            }

                        }

                        // If it's not present, count as no edit

                        if ( ! $match_found ) {

                            $count_no_edit++;
        
                        }

                    }

                }

                if ( ! in_array( false, $edit_bundle_db_result, true ) ) {
                    add_action( 'admin_notices', 'book_a_session_admin_notice_add_multiple_success' );
                } else {
                    add_action( 'admin_notices', 'book_a_session_admin_notice_add_multiple_warning' );
                }

            } else {
                add_action( 'admin_notices', 'book_a_session_admin_notice_edit_error' );
            }   
    
        } else {
            add_action( 'admin_notices', 'book_a_session_admin_notice_validation_error' );
        }

    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_edit_security_error' );
    }
        
}

// Delete
if ( isset( $_GET['delete'] ) && isset( $_GET['quantity'] ) && isset( $_GET['page'] ) ) {

    if ( $_GET['page'] == 'book_a_session_bundles' && current_user_can( "manage_options" ) && wp_verify_nonce( $_REQUEST["_wpnonce"], "book_a_session_delete_bundle_" . $_GET["quantity"] ) ) {

        // Delete Bundle
        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_bundle";
        $delete_bundle_db_result[] = $wpdb->delete( $table_name, array( 'quantity' => $_GET['quantity'] ), array( '%d' ) );

        // Delete Discounts
        $table_name_bundle_currency = $wpdb->prefix . "book_a_session_bundle_currency";
        $delete_bundle_db_result[] = $wpdb->delete( $table_name_bundle_currency, array( 'quantity' => $_GET['quantity'] ), array( '%d' ) );

        // Delete Bundle Payment Methods
        $table_name_bundle_payment_method = $wpdb->prefix . "book_a_session_bundle_payment_method";
        $delete_bundle_db_result[] = $wpdb->delete( $table_name_bundle_payment_method, array( 'quantity' => $_GET['quantity'] ), array( '%d' ) );

        ! in_array( false, $delete_bundle_db_result, true ) ? add_action( 'admin_notices', 'book_a_session_admin_notice_delete_success' ) : add_action( 'admin_notices', 'book_a_session_admin_notice_delete_error' );

    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_delete_security_error' );
    }

}

// Add
if ( isset( $_POST['submit_add_bundle'] ) ) {

    if ( current_user_can( "manage_options" ) ) {

        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_bundle";
    
        // Validation
    
        if ( ! empty( $_POST['quantity'] ) && ! empty( $_POST['name'] )) {
                
            $value_array = array( 
                'quantity'	=> $_POST['quantity'],
                'name'      => $_POST['name']
            );
            $add_bundle_db_result[] = $wpdb->insert( 
                $table_name, 
                $value_array, 
                array( 
                    '%d', 
                    '%s'
                    ) 
                );
            
            
            if ( $add_bundle_db_result ) {

                // Bundle Payment Method Insertion

                $payment_method_array = book_a_session_get_table_array( "payment_method" );

                $table_name_bundle_payment_method = $wpdb->prefix . "book_a_session_bundle_payment_method";

                for ( $i = 0; $i < count( $payment_method_array ); $i++ ) {

                    if ( ! empty( $_POST[ 'payment_method_' . $payment_method_array[$i]->id ] ) ) {

                        $bundle_payment_method_value_array = array( 
                            'quantity'       => $_POST['quantity'], 
                            'payment_method_id' => $payment_method_array[$i]->id
                        );

                        $bundle_payment_method_format_array = array(
                            '%d',
                            '%d'
                        );   

                        $add_bundle_db_result[] = $wpdb->insert( $table_name_bundle_payment_method, $bundle_payment_method_value_array, $bundle_payment_method_format_array );
    
                    }

                }

                // Add discounts
                $currency_array = book_a_session_get_table_array( "currency" );
                $table_name_discount = $wpdb->prefix . "book_a_session_bundle_currency";

                for ( $i = 0; $i < count( $currency_array ); $i++ ) {

                    if ( ! empty( $_POST['currency_' . $currency_array[$i]->id ] ) ) {

                        $value_array = array( 
                            'quantity'	    => $_POST['quantity'],
                            'currency_id'   => $currency_array[$i]->id,
                            'discount'      => $_POST['currency_' . $currency_array[$i]->id ]
                        );
    
                        $add_bundle_db_result[] = $wpdb->insert( 
                            $table_name_discount, 
                            $value_array, 
                            array( 
                                '%d', 
                                '%d',
                                '%f'
                                ) 
                            );
    
                    }
                } 
    
                if ( ! in_array( false, $add_bundle_db_result, true ) ) {
                    add_action( 'admin_notices', 'book_a_session_admin_notice_add_multiple_success' );
                } else {
                    add_action( 'admin_notices', 'book_a_session_admin_notice_add_multiple_warning' );
                }

            } else {
                add_action( 'admin_notices', 'book_a_session_admin_notice_add_error' );
            }   
    
        } else {
                add_action( 'admin_notices', 'book_a_session_admin_notice_validation_error' );
        }

    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_add_security_error' );
    }

}


function book_a_session_display_bundles_settings_page() {
	if ( ! current_user_can( 'manage_options' ) ) return;

    ?>
	<div class="wrap">
		<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
		<?php 

function page_tabs_bundle( $current = 'View' ) {
    $tabs = array(
        'view'   => __( 'View bundles', 'book_a_session' ), 
		'add'  => __( 'Add bundle', 'book_a_session' ),
		'edit'  => __( 'Edit bundles', 'book_a_session' )
    );
    $html = '<h2 class="nav-tab-wrapper">';
    foreach( $tabs as $tab => $name ){
        $class = ( $tab == $current ) ? 'nav-tab-active' : '';
        $html .= '<a class="nav-tab ' . $class . '" href="?page=book_a_session_bundles&tab=' . $tab . '">' . $name . '</a>';
    }
    $html .= '</h2>';
    echo $html;
}

// Tabs
$tab = ( ! empty( $_GET['tab'] ) ) ? esc_attr( $_GET['tab'] ) : 'view';
page_tabs_bundle( $tab );

// View bundles
if ( $tab == 'view' ) {

	echo '<h3>View bundles</h3>';
	echo '<form>';
	$bundles_list = new bundles_List_Table;
	echo '</form>';

}

// Add
elseif ( $tab == 'add' ) {


    $html =     "<table class='form-table'><form method='POST'>";
	$html.=     "<tr><h3>Add a new bundle</h3><p class='description'>You can currently only have one bundle for each quantity.</p></tr>";
    // Quantity input[type="number"] step="any" min="0" max="99" required
    $html .=    "<tr id='bundle_add_row_quantity'><th scope='row'>";
    $html .=    "<label for='quantity'>Quantity of sessions in bundle</label></th>";
    $html .=    "<td><input type='number' step='1' min='0' max='99' name='quantity' id='bundle_add_quantity' placeholder='1-999' style='min-width:300px;'";
    if ( isset( $_POST['quantity'] ) ) {
        $html .= " value='" . $_POST['quantity'] . "'>";
    } else {
        $html .= ">";
	}
    if ( isset( $_POST['submit_add_bundle'] ) ) {
        if ( empty( $_POST['quantity'] ) ) {
          $html .= "<p class='description book-a-session-error'>Please enter a whole number.</p>";
        }
	} 
    $html .=    "</td></tr>";
    // Name input[type="text"] required
    $html .=    "<tr><th scope='row'>";
	$html .=    "<label for='name'>Name</label></th>";
    $html .=    "<td><input type='text' name='name' id='bundle_add_name' placeholder='Name your bundle' style='min-width:300px;'";
    if ( isset( $_POST['name'] ) ) {
        $html .= " value='" . $_POST['name'] . "'>";
    } else {
        $html .= ">";
    }
    if ( isset( $_POST['submit_add_bundle'] ) ) {
        if ( empty( $_POST['name'] ) ) {
          $html .= "<p class='description book-a-session-error'>Please enter the bundle name.</p>";
        }
	} 
	$html .= 	"<p class='description'>In a future update, you will have the option of using this to title your bundle on the website. Currently, bundles are referred to simply by their quantity, e.g. 1 session, or 4 sessions.</p>";
    $html .=    "</td></tr>";
    // Payment methods checkbox
    $html .=    "<tr><th scope='row' colspan='2'><h3>Payment Methods</h3><p class='description'>Check the payment methods you accept for orders at this bundle.</p>";
    $html .=    "<a href='javascript:void(0)' class='select-all-payment-method'>Select all</a>&nbsp;&nbsp;<a href='javascript:void(0)' class='unselect-all-payment-method'>Unselect all</a>";
    $html .=    "</th></tr>";
    $payment_method_array = book_a_session_get_table_array( "payment_method" );
    for ($i = 0; $i < count( $payment_method_array ); $i++) {
        $html .=    "<tr id='bundle_add_row_payment_method_" . $payment_method_array[$i]->id . "' class='payment-method'><th scope='row'>";
        $html .=    "<label for='payment_method_" . $payment_method_array[$i]->id . "'>" . $payment_method_array[$i]->name . "</label>";
        $html .=    "</th><td>";
            $html .=    "<label for='payment_method_" . $payment_method_array[$i]->id . "'><input type='checkbox' id='payment_method_" . $payment_method_array[$i]->id . "' name='payment_method_" . $payment_method_array[$i]->id . "' value='1'"; 
        if ( ! empty( $_POST['payment_method_' . $payment_method_array[$i]->id ] ) ) {
            $html .= " checked>";
        } else {
            $html .= ">";
        }
        $html .=    "Check to accept " . $payment_method_array[$i]->name  . " for this bundle.</label>";  
        $html .=    "</td></tr>"; 
    }

    // Bundle Discounts - Loop through and create a row and field for each currency
    $html .=    "<tr><th scope='row' colspan='2'><h3>Discounts</h3><p class='description'>Enter a positive amount, e.g. 10, or 0 for no discount. So if the session price is usually 50, entering 10 in one of these fields would make it 40.</p>";
    $currency_array = book_a_session_get_table_array( "currency" );
    if ( ! empty( $currency_array ) ) {
        for ($i = 0; $i < count($currency_array); $i++) {
            $html .= "<tr><th scope='row'>";
            $html .= "<label for='bundle_add_currency_" . $currency_array[$i]->id . "'>Discount for " . $currency_array[$i]->code;
            $html .= "</label></th>";
            $html .= "<td><input type='number' step='any' min='0' name='currency_". $currency_array[$i]->id . "' id='bundle_add_currency_" . $currency_array[$i]->id . "' placeholder='0.00' style='min-width:300px;'";
            if ( isset( $_POST[ "currency_" . $currency_array[$i]->id ] ) ) {
                $html .= " value='" . $_POST[ "currency_" . $currency_array[$i]->id ] . "'>";
            } else {
                $html .= ">";
            }    
            $html .= "</td></tr>";
        }
    } else {
        $html .= "<tr><th scope='row'>";
        $html .= "<label>No currencies found</label></th>";    
        $html .= "<td><p class='description'>No currencies found. Add a Currency in the Currency settings page.</p></td></tr>";
	}


	$html .=    "</tbody></table>";
    $html .=    "<input type='submit' class='button button-primary' value='Add bundle' name='submit_add_bundle'>";
    $html .=    "</form>";
	
	
	echo $html;



} elseif ( $tab == 'edit' ) {

    if ( isset( $_GET["edit"] ) && ! empty( $_GET['quantity'] )  ) {

        echo '<h3>Edit a bundle</h3>';
        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_bundle";
        $table_name_bundle_currency = $wpdb->prefix . "book_a_session_bundle_currency";
        $payment_table_name = $wpdb->prefix . "book_a_session_bundle_payment_method";
        $id = intval( $_GET['quantity'] );

        $row_to_edit = $wpdb->get_row("
        SELECT  *
        FROM    $table_name
        WHERE   quantity = $id", OBJECT );

        $payment_method = $wpdb->get_results("
        SELECT  *
        FROM    $payment_table_name
        WHERE   quantity = $id", OBJECT );

        $discounts = $wpdb->get_results("
        SELECT  *
        FROM    $table_name_bundle_currency
        WHERE   quantity = $id", OBJECT );

        $html =     "<table class='form-table'><form method='POST'>";
		// Quantity input[type="number"] step="any" min="0" max="99" required
		$html .=    "<tr id='bundle_edit_row_quantity'><th scope='row'>";
		$html .=    "<label for='quantity'>Quantity</label></th>";
		$html .=    "<td><input type='number' step='1' min='0' max='99' name='quantity' id='bundle_edit_quantity' placeholder='1-999' style='min-width:300px;'";
        if ( isset( $row_to_edit->quantity ) ) {
            $html .= " value='" . $row_to_edit->quantity . "'";
		}

		$html .=    "></td></tr>";
		// Name input[type="text"] required
		$html .=    "<tr><th scope='row'>";
		$html .=    "<label for='name'>Name</label></th>";
		$html .=    "<td><input type='text' name='name' id='bundle_edit_name' placeholder='Name your bundle' style='min-width:300px;'";
        if ( isset( $row_to_edit->name ) ) {
            $html .= " value='" . $row_to_edit->name . "'";
        } 
		$html .= 	"><p class='description'>In a future update, you will have the option of using this to title your bundle on the website. Currently, bundles are referred to simply by their quantity, e.g. 1 session, or 4 sessions.</p>";
        $html .=    "</td></tr>";
        // Payment methods checkbox
        $html .=    "<tr><th scope='row' colspan='2'><h3>Payment Methods</h3><p class='description'>Check the payment methods you accept for this bundle.</p>";
        $html .=    "<a href='javascript:void(0)' class='select-all-payment-method'>Select all</a>&nbsp;&nbsp;<a href='javascript:void(0)' class='unselect-all-payment-method'>Unselect all</a>";
        $html .=    "</th></tr>";
        $payment_method_array = book_a_session_get_table_array( "payment_method" );
        for ($i = 0; $i < count( $payment_method_array ); $i++) {
            $html .=    "<tr id='bundle_add_row_payment_method_" . $payment_method_array[$i]->id . "' class='payment-method'><th scope='row'>";
            $html .=    "<label for='payment_method_" . $payment_method_array[$i]->id . "'>" . $payment_method_array[$i]->name . "</label>";
            $html .=    "</th><td>";
            $html .=    "<label for='payment_method_" . $payment_method_array[$i]->id . "'><input type='checkbox' id='payment_method_" . $payment_method_array[$i]->id . "' name='payment_method_" . $payment_method_array[$i]->id . "' value='1'";
            $match = false;
            for ($j = 0; $j < count( $payment_method ); $j++) {
                if ( isset( $payment_method[$j]->payment_method_id ) ) {
                    if ( $payment_method[$j]->payment_method_id == $payment_method_array[$i]->id ) {
                        $html .= " checked>";
                        $match = true;
                    }
                }
            }
            if ( ! $match ) {
                $html .= ">";
            } 
            $html .=    "Check to accept " . $payment_method_array[$i]->name  . " for this bundle.</label>";  
            $html .=    "</td></tr>"; 
        }
        
        // Bundle Discounts - Loop through and create a row and field for each currency
        $html .=    "<tr><th scope='row' colspan='2'><h3>Discounts</h3><p class='description'>Enter a positive amount, e.g. 10, or 0 for no discount. So if the session price is usually 50, entering 10 in one of these fields would make it 40.</p>";
        $currency_array = book_a_session_get_table_array( "currency" );
        if ( ! empty( $currency_array ) ) {
            for ($i = 0; $i < count($currency_array); $i++) {
                $html .= "<tr><th scope='row'>";
                $html .= "<label for='bundle_add_currency_" . $currency_array[$i]->id . "'>Discount for " . $currency_array[$i]->code;
                $html .= "</label></th>";
                $html .= "<td><input type='number' step='any' min='0' name='currency_". $currency_array[$i]->id . "' id='bundle_add_currency_" . $currency_array[$i]->id . "' placeholder='0.00' style='min-width:300px;'";
                $match = false;
                for ( $j = 0; $j < count( $discounts ); $j++ ){
                    if ( isset( $discounts[$j]->currency_id ) && isset( $discounts[$j]->discount ) ) {
                        if ( $discounts[$j]->currency_id == $currency_array[$i]->id ) {
                            $html .= " value='" . $discounts[$j]->discount . "'>";
                            $match = true;
                            break;
                        }
                    }
                }
                if ( ! $match ) {
                    $html .= ">";
                } 
                $html .= "</td></tr>";
            }
        } else {
            $html .= "<tr><th scope='row'>";
            $html .= "<label>No currencies found</label></th>";    
            $html .= "<td><p class='description'>No currencies found. Add a Currency in the Currency settings page.</p></td></tr>";
        }


		$html .=    "</tbody></table>";
		$html .=    "<input type='submit' class='button button-primary' value='Save changes' name='submit_edit_bundle'>";
		$html .=    "</form>";    
             
        echo $html;

    } else {

        echo '<h3>Edit a bundle</h3>';
        echo '<form>';
        $bundles_list = new Bundles_List_Table;
        echo '</form>';
    
    }

} else {

	echo '<h3>View bundles</h3>';
	echo '<form>';
	$bundles_list = new Bundles_List_Table;
	echo '</form>';

}
// Code after the tabs (outside)
		?>


	</div>

	<?php

}



