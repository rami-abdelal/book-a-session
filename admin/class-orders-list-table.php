<?php

if (!class_exists('WP_List_Table')) {
    require_once (ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
 
}

class Orders_List_Table extends WP_List_Table  {


    public function __construct()
    {
        parent::__construct(array(
            'singular' => 'order',
            'plural' => 'orders',
            'ajax' => true
        ));

        $this->prepare_items();
        $this->display();
        
    }

    public function prepare_items()
    {
    $this->_column_headers = $this->get_column_info();
    $columns = $this->get_columns();
    $hidden = $this->get_hidden_columns();
    $sortable = $this->get_sortable_columns();
    $this->_column_headers = array(
        $columns,
        $hidden,
        $sortable
    );
    
    $this->process_bulk_action();
    $per_page = $this->get_items_per_page('records_per_page', 10);
    $current_page = $this->get_pagenum();
    $total_items = self::record_count();
    $data = self::get_records($per_page, $current_page);
    $this->set_pagination_args(
                      ['total_items' => $total_items, 
                   'per_page' => $per_page
                  ]);
    $this->items = $data;
    }

    /** * 
    *Retrieve records data from the database
    * * @param int $per_page
    * @param int $page_number
    * * @return mixed
    */
    public static function get_records($per_page = 10, $page_number = 1)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'book_a_session_order';

        $sql = "SELECT
                *
                FROM " .  
                $table_name;

        /*if (isset($_REQUEST['s'])) {
        $sql.= ' where column1 LIKE "%' . $_REQUEST['s'] . '%" or column2 LIKE "%' . $_REQUEST['s'] . '%"';
        }*/
         
        if (!empty($_REQUEST['orderby'])) {
                $sql.= ' ORDER BY ' . esc_sql($_REQUEST['orderby']);
            $sql.= !empty($_REQUEST['order']) ? ' ' . esc_sql($_REQUEST['order']) : ' ASC';
        } else {
            $sql .= ' ORDER BY created DESC';
        }
        $sql.= " LIMIT $per_page";
        $sql.= ' OFFSET ' . ($page_number - 1) * $per_page;
        $result = $wpdb->get_results($sql, 'ARRAY_A');
        return $result;
    }

    function get_columns()
        {
            $columns = [
                'order_created'=>__('Created'),
                'order'=>__('Order'),
                'order_details'=>__('Details'),
                'status' =>__('Status'),
                'order_total'=>__('Total'),
                'actions'=>__('Actions')
                  ];
            return $columns;
        }       

    public function get_hidden_columns()
    {
        // Setup Hidden columns and return them
        return array(
        );
    }

    /** 
    * Columns to make sortable. 
    * * @return array 
    */
    public function get_sortable_columns()
    {
        $sortable_columns = array(
            'order_created'=>array('created', true),
            'order_total'=>array('total_due', true),
          );
        return $sortable_columns;
    }

    /** 
    *Text displayed when no record data is available 
    */
    public function no_items()
    {
        _e('No orders found.', 'bx');
    }

    /** 
    * Returns the count of records in the database. 
    * * @return null|string 
    */
    public static function record_count()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'book_a_session_order';

        $sql = "SELECT
                COUNT(*) 
                FROM " . 
                $table_name;
        return $wpdb->get_var($sql);
    }

        public function column_default($item, $column_name) {

            $item["amount_paid"] > 0.00 ? $amount = book_a_session_get_price_tag($item["amount_paid"],$item["currency_id"]) : $amount = "&mdash;";

            $order_contents_array = $item["session"] ? book_a_session_get_table_array( "session", "start_datetime", "ASC", "*", array( "order_id", "=", $item["order_id"] ) ) : book_a_session_get_table_array( "project", false, false, "*", array( "order_id", "=", $item["order_id"] ) );
            
            $nonce_delete = "&_wpnonce=" . wp_create_nonce( 'book_a_session_delete_order_' . $item["id"] );
            $nonce_edit = "&_wpnonce=" . wp_create_nonce( 'book_a_session_edit_order_' . $item["id"] );            
            $admin_edit_page_url = 'admin.php?page=book_a_session_orders&tab=edit'; 

            switch ($column_name) {

                    case "order_created"                        : 

                        echo "<table><tbody><tr><th>Client:</th><td>" . book_a_session_get_user_name( (int)$item["user_id"] ) . "</td></tr>";
                        echo "<tr><th>Date:</th><td>" . date( get_option( "date_format" ), strtotime( $item["created"] ) ) . "</td></tr>";
                        echo "<tr><th>Time:</th><td>" . date( get_option( "time_format" ), strtotime( $item["created"] ) ) . "</td></tr>";
                        if ( $item["session"] ) echo count( $order_contents_array ) > 1 ? "<tr><th>Order:</th><td>" . count( $order_contents_array ) . " Sessions</td></tr>" : "<tr><th>Order:</th><td>1 Session</td></tr>";
                        else "<tr><th>Order:</th><td>Project</td></tr>";
                        echo "<th scope='row'>ID:</th><td>" . $item["order_id"] . "</td></tr>";
                        echo "</tbody></table>";

                        break;

                    case "order"                                :
                    
                        if ( $item["session"] ) {

                            echo "<table><tbody>";

                            foreach ( $order_contents_array as $order_content ) {

                                $datetime =  book_a_session_get_session_time( (int)$order_content->schedule_id, false, $order_content->date );

                                echo "<tr><th scope='row'>" . $datetime["start_datetime_object"]->format( get_option( "date_format" ) ) . "</td><td>" . $datetime["time_string"] . "</td></tr>"; 
                                
                            }

                            echo "</tbody></table>";

                        } else {

                            echo "<table><tbody>";
                            echo "<tr><th scope='row'>Start:</th>";
                            echo "<td>" . $order_content->start_date . "</td></tr>";
                            echo "<tr><th scope='row'>End:</th>";
                            echo "<td>" . $order_content->end_date . "</td></tr>";

                        }

                        break;

                    case "order_details"                        : echo "<table><tbody><tr>"    . 
                    "<th scope='row'>Service:</th><td>"         . book_a_session_get_service_name($item["service_id"])                    . "</td></tr>" .
                    "<th scope='row'>Region:</th><td>"          . book_a_session_core_get_region_by_id($item["region_id"])["name"]        . "</td></tr>" .
                    "<th scope='row'>Location:</th><td>"        . book_a_session_get_location_name($item["location_id"])                  . "</td></tr>" .
                    "<th scope='row'>Practitioner:</th><td>"    . book_a_session_get_practitioner_name((int)$item["practitioner_id"])     . "</td></tr>" .
                    "<th scope='row'>Client:</th><td>"          . book_a_session_get_user_name((int)$item["user_id"])                     . "</td></tr>" .
                    "<th scope='row'>Quantity:</th><td>"        . $item["quantity"]                                                      . "</td></tr>" .
                                                                "</tbody></table>"; break;

                    case "status"                               : echo "<table><tbody><tr>"    . 
                    "<th scope='row'>Payment Method:</th><td>"  . book_a_session_get_payment_method($item["payment_method_id"])           . "</td></tr>" .
                    "<th scope='row'>Booking Status:</th><td>"  . $item["booking_status"]                                                 . "</td></tr>" .
                    "<th scope='row'>Payment Status:</th><td>"  . $item["payment_status"]                                                 . "</td></tr>" .
                    "<th scope='row'>VFC Mobile:</th><td>"      ; echo $item["vfc_mobile"] ? $item["vfc_mobile"] : "&mdash;";          echo "</td></tr>" .
                    "<th scope='row'>Amount Paid:</th><td>"     . $amount                                                                 . "</td></tr>" .
                    "<th scope='row'>Note:</th><td>"            ; echo $item["note"] ? $item["note"] : "&mdash;";                      echo "</td></tr>" .
                                                                  "</tbody></table>"; break;  
                    case "order_currency_id"                  : echo $item["currency_id"]; break;
                    case "order_total"                : echo book_a_session_get_price_tag( $item["total_due"], $item["currency_id"] ); break;

                    case "actions" : echo   "<a href='" . $admin_edit_page_url . $nonce_edit . "&edit=1&order_id=" . $item["id"] . "'>Edit</a>" . 
                                            "&nbsp;&nbsp;&nbsp;" . 
                                            "<a class='book-a-session-row-delete' href='" . $admin_edit_page_url . $nonce_delete . "&delete=1&order_id=" . $item["id"] . "'>Delete</a>"; 
                    break;
                    return $item[ $column_name ];
                    default:
                        return $item[ $column_name ] ;
            
                }

        }

}