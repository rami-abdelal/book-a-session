<?php 
/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://rami-abdelal.co.uk
 * @since      1.0.0
 *
 * @package    Book_A_Session
 * @subpackage book_a_session/admin/settings-callbacks.php
 */

if ( ! defined( 'ABSPATH' ) ) exit;

// Validate plugin settings

function book_a_session_validate_options( $input ) {
	
	// todo: add validation functionality..
	
	return $input;
	
}

/**
 * Defines the default values for options.
 * 
 * @param 	$group 		A required string used to determine which set of default option data to load.
 * 
 * 						Expected string must be one of the following:
 * 
 * 						all|general|practitioners|schedule|regions|paymentmethods|locations|servicetypes|services|bundles|
 * 						orders|timetables|emails|invoices|payments|receipts
 * 
 * 						Passing "all" will return the entire array and all of its values therein.
 * 
 * @param $id			A required string used as a key to find and return the correct value from the stored arrays.
 * 
 * @return  $default	Returns the default value for the determined field, or false on error.
 * 
 */


// callback: text field
function book_a_session_callback_field_text( $args ) {

	$id    = isset( $args['id'] )    ? $args['id']    : '';
	$label = isset( $args['label'] ) ? $args['label'] : '';

	// IDs should look something like this: bookasession_general_logo_url. Delimited by underscores, the first word will always be a plugin namespace, and the second is used to identify the option group. The following if statement extracts the option group from the ID to properly refer to the correct option in the WP database

	if ( ! empty( $id ) ) {

		// Extract group and option names, assigning the latter to $id again, effectively shortening the id while keeping the group name for a change in option naming format

		$id_array = explode( "_", $id );
		$group = $id_array[1];
		unset( $id_array[0] );
		unset( $id_array[1] );
		if ( count( $id_array ) > 1 ) {
			$id = implode( "_", $id_array );
		} else {
			$id = $id_array[2];
		}

		$option = isset( get_option( 'book_a_session_options_' . $group )[ $id ] ) ? get_option( 'book_a_session_options_' . $group )[ $id ] : book_a_session_options_default( $group, $id );
	
		$value = isset( $option ) ? sanitize_text_field( $option ) : '';
		
		echo '<input id="book_a_session_options_'. $group . "_" . $id .'" name="book_a_session_options_' . $group . '['. $id .']" type="text" size="40" value="'. $value .'"><br />';
		echo '<label class="book-a-session-sublabel"  for="book_a_session_options_'. $group . "_" . $id .'">'. $label .'</label>';
		
	}	
	
}

// callback: text field
function book_a_session_callback_field_password( $args ) {

	$id    = isset( $args['id'] )    ? $args['id']    : '';
	$label = isset( $args['label'] ) ? $args['label'] : '';

	// IDs should look something like this: bookasession_general_logo_url. Delimited by underscores, the first word will always be a plugin namespace, and the second is used to identify the option group. The following if statement extracts the option group from the ID to properly refer to the correct option in the WP database

	if ( ! empty( $id ) ) {

		// Extract group and option names, assigning the latter to $id again, effectively shortening the id while keeping the group name for a change in option naming format

		$id_array = explode( "_", $id );
		$group = $id_array[1];
		unset( $id_array[0] );
		unset( $id_array[1] );
		if ( count( $id_array ) > 1 ) {
			$id = implode( "_", $id_array );
		} else {
			$id = $id_array[2];
		}

		$option = isset( get_option( 'book_a_session_options_' . $group )[ $id ] ) ? get_option( 'book_a_session_options_' . $group )[ $id ] : book_a_session_options_default( $group, $id );
	
		$value = isset( $option ) ? sanitize_text_field( $option ) : '';
		
		echo '<input id="book_a_session_options_'. $group . "_" . $id .'" name="book_a_session_options_' . $group . '['. $id .']" type="password" size="40" value="'. $value .'" placeholder="Enter your password"><br />';
		echo '<label class="book-a-session-sublabel"  for="book_a_session_options_'. $group . "_" . $id .'">'. $label .'</label>';
		
	}	
	
}

// callback: Just a label
function book_a_session_callback_field_label( $args ) {

	$label = $args["content"];

	echo '<label>'. $label .'</label>';
		
}


// callback: number field
function book_a_session_callback_field_number( $args ) {

	$id    = isset( $args['id'] )    ? $args['id']    : '';
	$label = isset( $args['label'] ) ? $args['label'] : '';
	$step = isset( $args['step'] ) ? $args['step'] : 'any';
	$max = isset( $args['max'] ) ? $args['max'] : false;
	$min = isset( $args['min'] ) ? $args['min'] : false;

	// IDs should look something like this: bookasession_general_logo_url. Delimited by underscores, the first word will always be a plugin namespace, and the second is used to identify the option group. The following if statement extracts the option group from the ID to properly refer to the correct option in the WP database

	if ( ! empty( $id ) ) {

		// Extract group and option names, assigning the latter to $id again, effectively shortening the id while keeping the group name for a change in option naming format

		$id_array = explode( "_", $id );
		$group = $id_array[1];
		unset( $id_array[0] );
		unset( $id_array[1] );
		if ( count( $id_array ) > 1 ) {
			$id = implode( "_", $id_array );
		} else {
			$id = $id_array[2];
		}

		$option = isset( get_option( 'book_a_session_options_' . $group )[ $id ] ) ? get_option( 'book_a_session_options_' . $group )[ $id ] : book_a_session_options_default( $group, $id );	

		$value = isset( $option ) ? sanitize_text_field( $option ) : '';
		
		echo '<input id="book_a_session_options_'. $group . "_" . $id .'" name="book_a_session_options_'. $group . '[' . $id .']" type="number" step="' . $step . '" ';
		echo $max !== false ? 'max="' . $max . '" ' : ''; 
		echo $min !== false ? 'min="' . $min . '" ' : ''; 
		echo 'size="40" value="'. $value .'"><br />';
		echo '<label class="book-a-session-sublabel"  for="book_a_session_options_'. $group . "_" . $id .'">'. $label .'</label>';
	
	}		
	
	
}


// callback: radio field
function book_a_session_callback_field_radio( $args ) {

	$id    = isset( $args['id'] )    ? $args['id']    : '';
	$label = isset( $args['label'] ) ? $args['label'] : '';
	$inline = isset( $args['inline'] ) ? $args['inline'] : true;

	// IDs should look something like this: bookasession_general_logo_url. Delimited by underscores, the first word will always be a plugin namespace, and the second is used to identify the option group. The following if statement extracts the option group from the ID to properly refer to the correct option in the WP database

	if ( ! empty( $id ) ) {

		// Extract group and option names, assigning the latter to $id again, effectively shortening the id while keeping the group name for a change in option naming format

		$id_array = explode( "_", $id );
		$group = $id_array[1];
		unset( $id_array[0] );
		unset( $id_array[1] );
		if ( count( $id_array ) > 1 ) {
			$id = implode( "_", $id_array );
		} else {
			$id = $id_array[2];
		}

		$option = isset( get_option( 'book_a_session_options_' . $group )[ $id ] ) ? get_option( 'book_a_session_options_' . $group )[ $id ] : book_a_session_options_default( $group, $id );

		$selected_option = isset( $option ) ? sanitize_text_field( $option ) : '';
	
		foreach ( $args['radio_options'] as $value => $label ) {
			
			$checked = checked( $selected_option == $value, 1, false );
			
			echo "<label><input name='book_a_session_options_". $group . "[" . $id ."]' type='radio' value='". $value . "'" . $checked ."> ";
			echo '<span>'. $label;
			echo $inline ? '</span>&nbsp;&nbsp;</label>' : '</span></label><br>';
			
		}
	
		echo ! empty( $args['description'] ) ? '<p class="description">' . sanitize_text_field( $args['description'] ) . '</p>' : '';
	
	}		

}

// callback: textarea field
function book_a_session_callback_field_textarea( $args ) {
	
	$id    = isset( $args['id'] )    ? $args['id']    : '';
	$label = isset( $args['label'] ) ? $args['label'] : '';	

	// IDs should look something like this: bookasession_general_logo_url. Delimited by underscores, the first word will always be a plugin namespace, and the second is used to identify the option group. The following if statement extracts the option group from the ID to properly refer to the correct option in the WP database

	if ( ! empty( $id ) ) {

		// Extract group and option names, assigning the latter to $id again, effectively shortening the id while keeping the group name for a change in option naming format

		$id_array = explode( "_", $id );
		$group = $id_array[1];
		unset( $id_array[0] );
		unset( $id_array[1] );
		if ( count( $id_array ) > 1 ) {
			$id = implode( "_", $id_array );
		} else {
			$id = $id_array[2];
		}

		$option = isset( get_option( 'book_a_session_options_' . $group )[ $id ] ) ? get_option( 'book_a_session_options_' . $group )[ $id ] : book_a_session_options_default( $group, $id );

		$allowed_tags = wp_kses_allowed_html( 'post' );
		
		$value = isset( $option ) ? wp_kses( stripslashes_deep( $option ), $allowed_tags ) : '';
		
		echo '<textarea id="book_a_session_options_'. $group . "_" . $id .'" name="book_a_session_options_'. $group . '[' . $id .']" rows="5" cols="50">'. $value .'</textarea><br />';
		echo '<label class="book-a-session-sublabel"  for="book_a_session_options_'. $group . "_" . $id .'">'. $label .'</label>';

	}		
	
}

// callback: checkbox field
function book_a_session_callback_field_checkbox( $args ) {

	$id    = isset( $args['id'] )    ? $args['id']    : '';
	$label = isset( $args['label'] ) ? $args['label'] : '';

	// IDs should look something like this: bookasession_general_logo_url. Delimited by underscores, the first word will always be a plugin namespace, and the second is used to identify the option group. The following if statement extracts the option group from the ID to properly refer to the correct option in the WP database

	if ( ! empty( $id ) ) {

		// Extract group and option names, assigning the latter to $id again, effectively shortening the id while keeping the group name for a change in option naming format

		$id_array = explode( "_", $id );
		$group = $id_array[1];
		unset( $id_array[0] );
		unset( $id_array[1] );
		if ( count( $id_array ) > 1 ) {
			$id = implode( "_", $id_array );
		} else {
			$id = $id_array[2];
		}

		$option = isset( get_option( 'book_a_session_options_' . $group )[ $id ] ) ? get_option( 'book_a_session_options_' . $group )[ $id ] : book_a_session_options_default( $group, $id );

		$checked = isset( $option ) ? checked( $option, 1, false ) : '';
		
		echo '<input id="book_a_session_options_'. $group . "_" . $id .'" name="book_a_session_options_'. $group . '[' . $id .']" type="checkbox" value="1"'. $checked .'> ';
		echo '<label for="book_a_session_options_'. $group . "_" . $id .'">'. $label . '</label>';

	}		
	
}

// callback: select field
function book_a_session_callback_field_select( $args ) {

	$id    = isset( $args['id'] )    ? $args['id']    : '';
	$label = isset( $args['label'] ) ? $args['label'] : '';

	// IDs should look something like this: bookasession_general_logo_url. Delimited by underscores, the first word will always be a plugin namespace, and the second is used to identify the option group. The following if statement extracts the option group from the ID to properly refer to the correct option in the WP database

	if ( ! empty( $id ) ) {

		// Extract group and option names, assigning the latter to $id again, effectively shortening the id while keeping the group name for a change in option naming format

		$id_array = explode( "_", $id );
		$group = $id_array[1];
		unset( $id_array[0] );
		unset( $id_array[1] );
		if ( count( $id_array ) > 1 ) {
			$id = implode( "_", $id_array );
		} else {
			$id = $id_array[2];
		}

		$option = isset( get_option( 'book_a_session_options_' . $group )[ $id ] ) ? get_option( 'book_a_session_options_' . $group )[ $id ] : book_a_session_options_default( $group, $id );
	
		$selected_option = isset( $option ) ? sanitize_text_field( $option ) : '';
		
		$select_options = array(
			
			'default'   => "Default",
			'light'     => "Light",
			'blue'      => "Blue",
			'coffee'    => "Coffee",
			'ectoplasm' => "Ectoplasm",
			'midnight'  => "Midnight",
			'ocean'     => "Ocean",
			'sunrise'   => "Sunrise",
			
		);
		
		echo '<select id="book_a_session_options_'. $group . "_" . $id .'" name="book_a_session_options_'. $group . '[' . $id .']">';
		
		foreach ( $select_options as $value => $option ) {
			
			$selected = selected( $selected_option === $value, true, false );
			
			echo '<option value="'. $value .'"'. $selected .'>'. $option .'</option>';
			
		}
		
		echo '</select> <label class="book-a-session-sublabel"  for="book_a_session_options_'. $group . "_" . $id .'">'. $label .'</label>';

	}		
	
}

/**
 * Creates select field for selecting Wordpress pages.
 * 
 * @param 	string $group		- Option group name as defined when registering setttings in settings-register.php.
 * @param	string $id			- Option ID as defined when registering settings. More documentation available in above functions.
 * @param	int    $selected	- Optional. Provide a page ID of the currently selected page to display in the display box.
 * 
 */ 

function book_a_session_callback_dropdown_pages( $args ) {
	
	$group = $args["group"];
	$id = $args["id"];
	$selected = $args["selected"];
	$description = ! empty( $args["description"] ) ? $args["description"] : false;
	$label = ! empty( $args["label"] ) ? $args["label"] : false;

	$selected = ! empty( intval( $selected ) ) ? intval( $selected ) : 0;

	$args = array(
		"name" => "book_a_session_options_" . $group . "[" . $id . "]",
		"id" => "book_a_session_options_" . $group . "_" . $id,
		"selected" => $selected
	);

	wp_dropdown_pages( $args );

	echo $label ? "<br><label class='book-a-session-sublabel'  for='book_a_session_options_" . $group . "_" . $id . "'>" . $label . "</label>" : "";
	echo $description ? "<p class='description'>" . $description . "</p>" : "";

}

function book_a_session_callback_image_select( $args ) {

	$group = ! empty( $args["group"] ) ? $args["group"] : false;
	$id = ! empty( $args["id"] ) ? $args["id"] : false;
	$placeholder = ! empty( $args["placeholder"] ) ? $args["placeholder"] : false;
	$option = ! empty( $args["option"] ) ? true : false;
	$description = ! empty( $args["description"] ) ? $args["description"] : false;
	$label = ! empty( $args["label"] ) ? $args["label"] : false;
	$image_id = ! empty( $args["image_id"] ) ? $args["image_id"] : false;
	$name = ! empty( $args["name"] ) ? $args["name"] : false;
	$image_select_html = "";

	$args = array(
		"name" => "book_a_session_options_" . $group . "[" . $id . "]",
		"id" => "book_a_session_options_" . $group . "_" . $id,
	);

	if ( $option ) {

		// Settings callback HTML

		$image_id = ! empty( get_option( 'book_a_session_options_' . $group )[ $id ] ) ? get_option( 'book_a_session_options_' . $group )[ $id ] : false;
	
		if( intval( $image_id ) > 0 ) {
			// Change with the image size you want to use
			$image = wp_get_attachment_image( $image_id, 'medium', false, array( 'class' => 'book-a-session-preview-image' ) );
		} else {
			// Some default image
			$image = '<img class="book-a-session-preview-image" src="' . plugin_dir_url( __FILE__ ) . 'img/default-image.png">';
			
		}
		$image_select_html .= $image . "<br>";
		$image_select_html .= '<input type="hidden" name="book_a_session_options_' . $group . '[' . $id . ']" class="book-a-session-image-select" value="' . esc_attr( $image_id ) . '" class="regular-text" id="book_a_session_options_' . $group . '_' . $id . '">';
		$image_select_html .= '<input type="button" class="button-primary book_a_session_media_manager" value="Select or upload image">';
		$image_select_html .= $label ? "<br><label class='book-a-session-sublabel'  for='book_a_session_options_" . $group . "_" . $id . "'>" . $label . "</label><br>" : "";
		$image_select_html .= $description ? "<p class='description'>" . $description . "</p>" : "";

		echo $image_select_html;

	} else {

		// Regular form for editing database rows

		if( intval( $image_id ) > 0 ) {
			// Change with the image size you want to use
			$image = wp_get_attachment_image( $image_id, 'medium', false, array( 'class' => 'book-a-session-preview-image' ) );
		} else {
			// Some default image
			$image = '<img class="book-a-session-preview-image" src="' . plugin_dir_url( __FILE__ ) . 'img/default-image.png">';
			
		}

		$image_select_html .= $image . "<br>";
		$image_select_html .= '<input type="hidden" name="' . $name . '" class="book-a-session-image-select" value="' . esc_attr( $image_id ) . '" class="regular-text" id="' . $name . '">';
		$image_select_html .= '<input type="button" class="button-primary book_a_session_media_manager" value="Select or upload image">';
		$image_select_html .= $label ? "<br><label class='book-a-session-sublabel' for='$name'>$label</label><br>" : "";
		$image_select_html .= $description ? "<p class='description'>" . $description . "</p>" : "";

		return $image_select_html;

	}


}

// Ajax action to refresh the user image
add_action( 'wp_ajax_book_a_session_get_image', 'book_a_session_get_image' );

function book_a_session_get_image() {

    if ( isset( $_GET['id'] ) ) {

        $image = wp_get_attachment_image( filter_input( INPUT_GET, 'id', FILTER_VALIDATE_INT ), 'medium', false, array( 'class' => 'book-a-session-preview-image' ) );
        $data = array(
            'image'    => $image,
		);
		
		wp_send_json_success( $data );
		
    } else {
		
		wp_send_json_error();

	}
	
}

function book_a_session_callback_section_general_settings() {
    
    echo "";
    
}

function book_a_session_callback_section_bundle_settings() {
    
    echo "<p>To add, edit or delete bundles, go to the <a href='admin.php?page=book_a_session_bundles'>Bundles</a> page.</p>";
    
}

function book_a_session_callback_section_currency_settings() {
    
    echo "<p>To add, edit or delete currencies, go to the <a href='admin.php?page=book_a_session_currencies'>Currencies</a> page.</p>";
    
}

function book_a_session_callback_section_payment_method_settings() {
    
	echo "<p>Choose which payment methods you'd like to make available for customers to use. There are more payment method related settings in the Invoices tab.</p>";
	echo "<p class='description'>Note: disabled payment methods still appear as usual throughout the settings pages, but if they are disabled here, they will never display as an option for customers.</p>";
    
}

function book_a_session_callback_section_payment_method_settings_1() {

	echo "<p>Control the representation of PayPal as a payment method option on the frontend form, and connect your PayPal account to Book A Session.</p>";

}

function book_a_session_callback_section_payment_method_settings_2() {

	echo "<p>Control the representation of Cash as a payment method option on the frontend form.</p>";

}

function book_a_session_callback_section_payment_method_settings_3() {

	echo "<p>Control the representation of Vodafone Cash as a payment method option on the frontend form. There are more Vodafone Cash related settings in the Invoices tab, go there to set up the due date period and enter the number you want customers to send Vodafone Cash payments to.</p>";

}

function book_a_session_callback_section_payment_method_settings_4() {

	echo "<p>Control the representation of Credit / Debit Card (Stripe) as a payment method option on the frontend form, and connect your Stripe account to Book A Session.</p>";

}
    
function book_a_session_callback_section_location_settings() {
    
    echo "<p>To add, edit or delete locations, go to the <a href='admin.php?page=book_a_session_locations'>Locations</a> page.</p>";
    
}

function book_a_session_callback_section_practitioner_settings() {
    
	echo "<p>To assign a practitioner, change a user's role to Practitioner or Admin Practitioner. Practitioner's have the same capabiities as Contributors, and Administrator Practitioners have the same capabilities as Administrators.</p>";
	
	echo "<p>You can change a user to one of those roles by going to <a href='" . admin_url() . "users.php" . "' target='_blank'>Users > All Users</a>, checking the boxes for the relevant users, and using the dropdown box that reads 'Change role to...' at the top to pick either Practitioner or Administrator Practitioner, and finally hitting 'Change'.</p>";

	echo "<p>Currently, changing capabilities for either of those roles is not supported in Book A Session. If you want to change capabilities for Practitioners, e.g. you want them to be able to publish their own posts, or do something beyond what a Contributor can do (which is add, edit and delete their own posts, but not publish them), or perhaps you don't want them to have any capabilities at all, you can use a free plugin such as <a href='https://wordpress.org/plugins/user-role-editor/'>User Role Editor</a> or another plugin that can let you change capabilities for Practitioners.</p>";

	echo "<p>Book A Session relies on finding users with the roles with an id of <code>admin_practitioner</code> and <code>practitioner</code>. You're free to change what they mean or what they can do, so long as your practitioners are assigned to one role or the other, they have email addresses, and first and last names, the system will work as intended.</p>";

}

function book_a_session_callback_section_schedule_settings() {
    
    echo "<p>To add to, modify or delete your schedule, go to the <a href='admin.php?page=book_a_session_schedule'>Schedule page</a>.</p>";
    
}

function book_a_session_callback_section_timetable_settings() {
    
    echo "";
    
}

function book_a_session_callback_section_invoice_settings() {
	
    echo "<p>To add, edit or delete invoices, go to the <a href='admin.php?page=book_a_session_invoices'>Invoices</a> page.</p>";
	echo "<p>Invoices are created and sent to customers who are ordering without paying during checkout, prompting them to pay by a due date. Cash payments are due on the date and time of the session, and online payments are usually due during checkout.</p>";
	echo '<h3>Due Date Period Settings</h3>';
	echo '<p>To determine the due date by which customers must make Vodafone Cash payments and online payments (if you choose not to require online payment during checkout), we need to specify a due date period. Those payment methods will only be available for sessions starting after the end of this period, 
		  so it\'s recommended to keep this period short if you want customers to book with them on short notice.</p>';
	echo '<p>For example, if the due date period is 3 days after the invoice would be issued, customers ordering on the 1st of May can\'t pay with an online payment method or Vodafone Cash unless their earliest session date is after the 4th of May. Online payments only behave this way if you\'ve set "Online Payment Payable" to "Payable by due date" on the Payment Methods tab, but Vodafone Cash payments are always subject to this due date period.</p>';
    
}

function book_a_session_callback_section_invoice_online_settings() {
    
    echo "<p>Online payment invoices are created when customers choose to pay with either PayPal or Credit / Debit Card (Stripe), and you've set the \"Online Payment Payable\" option to \"Payable by due date\" under the Payment Methods tab. If an online payment invoice is created, payment would be required in full at the end of the due date period, which you can specify above. If full payment isn't made by the due date, the order will be cancelled.</p>";
    
}

function book_a_session_callback_section_invoice_online_due_settings() {

    echo "<p>Online payment due invoices are created when online payments are set to be payabale by a due date and a customer has chosen an online payment method such as PayPal or Credit / Debit Card (Stripe), prompting the customer to pay the balance to finalise their order.</p>";

}

function book_a_session_callback_section_invoice_online_partly_paid_settings() {

    echo "<p>Online payment partly paid invoices are created when a part payment has been made toward an order with PayPal or Credit / Debit Card (Stripe), prompting the customer to pay the remaining balance to finalise their order.</p>";

}

function book_a_session_callback_section_invoice_online_paid_settings() {

    echo "<p>Online payment paid invoices are created when full payment has been made for an order with PayPal or Credit / Debit Card (Stripe), clearing the invoice and finalising the order.</p>";

}

function book_a_session_callback_section_invoice_cash_settings() {
    
    echo "<p>Cash invoices are created when customers choose to pay with cash. Due dates are set to the date and time of the first session.</p>";
    
}

function book_a_session_callback_section_invoice_cash_due_settings() {

    echo "<p>Cash payment due invoices are created when a customer has chosen cash as their payment method. The order is booked and finalised once the order is made, and payment in full is required in person at the first session.</p>";

}

function book_a_session_callback_section_invoice_cash_partly_paid_settings() {

    echo "<p>Cash partly paid invoices are created when a part payment has been made toward an order with cash, prompting the customer to pay the remaining balance.</p>";

}

function book_a_session_callback_section_invoice_cash_paid_settings() {

    echo "<p>Cash paid invoices are created when full payment has been made for an order with cash, clearing the invoice.</p>";

}

function book_a_session_callback_section_invoice_vfc_settings() {
    
    echo "<p>Vodafone Cash invoices are created when customers choose to pay with Vodafone Cash. Payment is required in full at the end of the due date period you can specify at the top of this page. If the balance isn't paid in full by the due date, the order will be cancelled.</p>";
    
}

function book_a_session_callback_section_invoice_vfc_due_settings() {

    echo "<p>Vodafone Cash due invoices are created when a customer has chosen Vodafone Cash as their payment method, prompting the customer to send the full payment to your receiving mobile number to finalise their order.</p>";

}

function book_a_session_callback_section_invoice_vfc_partly_paid_settings() {

    echo "<p>Vodafone Cash partly paid invoices are created when a part payment has been made toward an order with Vodafone Cash, prompting the customer to send a payment to your receiving mobile number to clear the remaining balance and finalise their order.</p>";

}

function book_a_session_callback_section_invoice_vfc_paid_settings() {

    echo "<p>Vodafone Cash paid invoices are created when full payment has been made for an order with Vodafone Cash, clearing the invoice and finalising the order.</p>";

}

function book_a_session_callback_section_receipt_settings() {
    
    echo "<p>Receipts are coming soon.</p>";
    
}

function book_a_session_callback_section_email_settings() {
    
    ?>

	<ul>
		<li>If you're not setting up SMTP in the advanced settings section, we recommend avoiding using addresses from Gmail and similar sites as they can cause delivery problems in this system.</li>
		<li>The most common screen widths are <code>1366</code> and <code>1920</code>. It's best to choose a maximum width under <code>960</code> to maximise readability.</li>
		<li>Color fields support all CSS color codes such as Hex: <code>#fafafa</code>, RGB/RGBA: <code>rgba(250, 250, 250, 1)</code> and HSL: <code>hsl(0, 0%, 98%)</code>.</li>
	</ul>

	<?php
    
}

function book_a_session_callback_section_email_phpmailer_settings() {

	echo "<p>WordPress emails sometimes don't arrive at their destination, this is because, sometimes, your hosting provider prevents unauthenticated emails (which is the kind WordPress sends by default) from being sent. They do this to help prevent the likelihood of their servers being marked as a server that sends spam emails, which can cause problems for other customers.</p>";

	echo "<p>To get around this, you can authenticate your emails. By filling out your information below, Book A Session emails will be sent by first logging into your email account, then sending authenticated emails from your email address directly, just like you would manually, but automatically instead. This kind of email is not prevented by any hosting providers, so the likelihood of your emails being delivered successfully will increase significantly.</p>";

	echo "<p>That being said, it's important to pick a good email provider. When you've finished entering your SMTP details below, and you've enabled the Authenticated Emails setting, all emails will be sent from their computers. At that point, if some emails aren't arriving, it would be an issue you should contact them about. This most often happens with email accounts provided by hosting companies, as their main focus is hosting websites, rather than email.</p>";
	
	echo "<p>If your emails still aren't arriving reliably, try switching from TLS to SSL or vice versa, contacting your hosting provider explaining the issue, using a different email provider, or disabling Authenticated Emails below, instead and using a dedicated email plugin such as WP Mail SMTP along with a reliable mailer such as Mailgun (instructions for which should be provided by the plugin developers).</p>";

}

function book_a_session_callback_section_payment_settings() {
    
    echo "<p>Payments are coming soon.</p>";
    
}

function book_a_session_callback_section_order_settings() {
    
    echo "<p>To add, edit or delete orders, go to the <a href='admin.php?page=book_a_session_orders'>Orders</a> page.</p>";
    
}

function book_a_session_callback_section_service_settings() {
    
    echo "<p>To add, edit or delete services, go to the <a href='admin.php?page=book_a_session_services'>Services</a> page.</p>";
    
}

function book_a_session_callback_section_service_type_settings() {
    
    echo "<p>To add, edit or delete service types, go to the <a href='admin.php?page=book_a_session_service_types'>Service Types</a> page.</p>";
    
}

function book_a_session_callback_section_region_settings() {
    
    echo "<p>To add, edit or delete regions, go to the <a href='admin.php?page=book_a_session_regions'>Regions</a> page.</p>";
    
}