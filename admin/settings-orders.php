<?php // Book A Session - orders Settings Page

if ( ! defined( 'ABSPATH' ) ) exit;

// Edit

if (   ! empty( $_GET['edit'] ) 
    && ! empty( $_GET['order_id'] )
    && ! empty( $_POST['submit_edit_order'] ) ) {

    // If the user can manage options (is an admin), the nonce is verified, and the user has tried to update the order, go ahead and attempt to do so

    if ( current_user_can( "manage_options" ) && wp_verify_nonce( $_REQUEST["_wpnonce"], "book_a_session_edit_order_" . $_GET["order_id"] ) ) {

        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_order";

        // Proceed if the region is supplied, otherwise show a verification error
    
        if ( ! empty( $_POST['region_id'] ) ) {

            // Get the currency ID from the supplied region ID
    
            $region_input_array = array( "id" => $_POST['region_id'] );
            $region_array = book_a_session_get_region_by_id( $region_input_array );
            $currency_id = $region_array["currency_id"];

            // If everything else required to update to the order is present, proceed to preparing the updated values for updating the database
        
            if (    
                    ! empty( $_POST['quantity'] )          
                &&  ! empty( $_POST['service_id'] )         
                &&  ! empty( $_POST['region_id'] )          
                &&  ! empty( $_POST['practitioner_id'] )    
                &&  ! empty( $_POST['user_id'] )            
                &&  ! empty( $_POST['location_id'] )        
                &&  ! empty( $currency_id )                          
                &&  ! empty( $_POST['total'] )                 
                &&  ! empty( $_POST['payment_method_id'] )  
                &&  ! empty( $_POST['booking_status'] )     
                &&  ! empty( $_POST['payment_status'] )     
                &&  isset(   $_POST['amount_paid'] )  

            ) {

                // Prepare the values

                $value_array = array( 

                    'created'           => date("Y-m-d H:i:s"), 
                    'session'           => 1,
                    'project'           => 0,    
                    'quantity'          => $_POST['quantity'], 
                    'service_id'        => $_POST['service_id'], 
                    'order_id'          => $_POST['order_id'],
                    'region_id'         => $_POST['region_id'], 
                    'practitioner_id'   => $_POST['practitioner_id'], 
                    'user_id'           => $_POST['user_id'], 
                    'location_id'       => $_POST['location_id'], 
                    'payment_method_id' => $_POST['payment_method_id'], 
                    'currency_id'       => $currency_id, 
                    'base_price'        => $_POST['base_price'],
                    'location_charge'   => $_POST['location_charge'],
                    'discount'          => $_POST['discount'],
                    'total'             => $_POST['total'], 
                    'amount_paid'       => $_POST['amount_paid'], 
                    'total_due'         => $_POST['total_due'],
                    'booking_status'    => $_POST['booking_status'], 
                    'payment_status'    => $_POST['payment_status'], 
                    'vfc_mobile'        => $_POST['vfc_mobile'], 
                    'note'              => $_POST['note']

                );

                // Update the 

                $edit_order_db_result = $wpdb->update( 

                    $table_name, 
                    $value_array,
                    array( 'id' => $_GET['order_id'] ), 
                    array( 

                        '%s', // Created
                        '%d', // Session
                        '%d', // Project
                        '%d', // Quantity
                        '%d', // Service ID
                        '%d', // Order ID
                        '%d', // Region ID
                        '%d', // Practitioner ID
                        '%d', // User ID
                        '%d', // Location ID
                        '%d', // Payment Method ID
                        '%d', // Currency ID
                        '%f', // Base Price
                        '%f', // Location Charge
                        '%f', // Discount
                        '%f', // Total
                        '%f', // Amount Paid
                        '%f', // Total Due
                        '%s', // Booking Status
                        '%s', // Payment Status
                        '%s', // VFC Mobile
                        '%s'  // Note

                        ) 

                );
    
                if ( $edit_order_db_result ) {

                    // If the order edit worked (which would update all values entered in the form except for session_time_X and session_date_X), update the sessions, taking care of the rest of the form values

                    $edit_session_db_result = array();
                    $table_name_session = $wpdb->prefix . "book_a_session_session";
                    $table_name_timetable = $wpdb->prefix . "book_a_session_timetable";

                    // Get current sessions in an array from the database

                    $session_array = book_a_session_get_table_array( "session", false, false, "*", array( "order_id", "=", intval( $_GET["order_id"] ) ) );

                    // If we could find the sessions, begin looping through, otherwise show a warning regarding multiple items

                    if ( $session_array ) {

                        // Loop through each session and use wpdb->update() to update them one by one

                        foreach ( $session_array as $session ) :

                            // Get datetime by using book_a_session_get_session_time() which will return a datetime object when we pass a schedule ID and date

                            $datetime = book_a_session_get_session_time( $_POST[ "session_time_" . $session->id ], false, $_POST[ "session_date_" . $session->id ] );

                            // Determines which session to update

                            $session_where_array = array(

                                "id" => $session->id,

                            );

                            // Determines the new values for said session

                            $session_value_array = array(

                                "schedule_id" => $_POST[ "session_time_" . $session->id ],
                                "date" => $_POST[ "session_date" . $session->id ],
                                "start_datetime" => $datetime->start_datetime_object->format( "Y-m-d H:i:s" ),
                                "end_datetime" => $datetime->end_datetime_object->format( "Y-m-d H:i:s" ),

                            );

                            // Determines the intended format of these values sprintf style to prevent SQL injection attacks

                            $session_value_format_array = array(

                                "%d", // Schedule ID
                                "%s", // Date
                                "%s", // Start Datetime
                                "%s", // End Datetime

                            );

                            // Determines the intended format for the where values sprintf style, this isn't necessary as these values are generated automatically by MySQL via auto increment though it's better to be safe

                            $session_where_format_array = array(

                                "%d", // ID

                            );

                            // Update the row and store the result in a variable on its own to check if it worked before trying to update the timetable entry

                            $session_updated = $wpdb->update(

                                $table_name_session,
                                $session_value_array,
                                $session_where_array,
                                $session_value_format_array,
                                $session_where_format_array

                            );

                            // Push the result to an array to determine success later

                            $edit_session_db_result[] = $session_updated;

                            // Try to update the timetable if the session was updated

                            if ( $session_updated ) {

                                // Determines which timetable entry we'll attempt to update

                                $timetable_where_array = array(

                                    "order_id"          => intval( $_GET[ "order_id" ] ),
                                    "date"              => $session->date,
                                    "schedule_id"       => $session->schedule_id,

                                );
                                
                                // Determines the new values for this timetable entry

                                $timetable_value_array = array(

                                    "date"              => $_POST[ "session_date" . $session->id ],
                                    "start_datetime"    => $datetime->start_datetime_object->format( "Y-m-d H:i:s" ),
                                    "end_datetime"      => $datetime->end_datetime_object->format( "Y-m-d H:i:s" ),
                                    "schedule_id"       => $_POST[ "session_time_" . $session->id ],
                                    "service_id"        => $_POST[ "service_id" ],
                                    "location_id"       => $_POST[ "location_id" ],
                                    "practitioner_id"   => $_POST[ "practitioner_id" ],
                                    "client_id"         => $_POST[ "user_id" ],

                                );

                                // Determines the intended format for timetable values to help prevent SQL injection

                                $timetable_value_format_array = array(

                                    "%s", // Date
                                    "%s", // Start Datetime
                                    "%s", // End Datetime
                                    "%d", // Schedule ID
                                    "%d", // Service ID
                                    "%d", // Location ID
                                    "%d", // Practitioner ID
                                    "%d", // Client ID

                                );

                                // Determines the intended format for timetable where values to help prevent SQL injection

                                $timetable_where_format_array = array(

                                    "%d", // Order ID
                                    "%s", // Date
                                    "%d", // Schedule ID

                                );

                                // Push the result to the result array to check for success

                                $edit_session_db_result[] = $wpdb->update( 

                                    $table_name_timetable,
                                    $timetable_value_array,
                                    $timetable_where_array,
                                    $timetable_value_format_array,
                                    $timetable_where_format_array

                                ); 

                            } else $edit_session_db_result[] = false;

                            // If the session wasn't updated, simply add another false without trying to update the timetable entry

                        endforeach;

                        // Determine Success

                        if ( ! in_array( false, $edit_session_db_result, true ) ) add_action( 'admin_notices', 'book_a_session_admin_notice_edit_multiple_success' );
                        else add_action( 'admin_notices', 'book_a_session_admin_notice_edit_multiple_warning' );

                    } else add_action( 'admin_notices', 'book_a_session_admin_notice_edit_multiple_warning' );

                } else add_action( 'admin_notices', 'book_a_session_admin_notice_edit_error' );
    
            } else add_action( 'admin_notices', 'book_a_session_admin_notice_validation_error' );
    
        } else add_action( 'admin_notices', 'book_a_session_admin_notice_validation_error' );    

    } else add_action( 'admin_notices', 'book_a_session_admin_notice_edit_security_error' );

}

// Delete
if ( isset( $_GET['delete'] ) && isset( $_GET['order_id'] ) ) {

    if ( $_GET['page'] == 'book_a_session_orders' && current_user_can( "manage_options" ) && wp_verify_nonce( $_REQUEST["_wpnonce"], "book_a_session_delete_order_" . $_GET["order_id"] ) ) {

        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_order";
        $delete_order_db_result = $wpdb->delete( $table_name, array( 'id' => $_GET['order_id'] ), array( '%d' ) );
        $delete_order_db_result ? add_action( 'admin_notices', 'book_a_session_admin_notice_delete_success' ) : add_action( 'admin_notices', 'book_a_session_admin_notice_delete_error' );

    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_delete_security_error' );
    }

}

// Add
if ( isset( $_POST['submit_add_order'] ) ) {

    // TO DO: Make this project and session based. Currently, it is only session based.

    if ( current_user_can( "manage_options" ) ) {

        // Get Currency from selected Region
        if ( ! empty( $_POST['region_id'] ) ) {

            $region_input_array = array( "id" => $_POST['region_id'] );
            $region_array = book_a_session_get_region_by_id( $region_input_array );
            $currency_id = $region_array["currency_id"];

            // Determine Total
            if ( ! empty( $_POST['location_id'] ) && ! empty( $currency_id ) && ! empty( $_POST["service_id"] ) ) {

                $base_price = 0.00;
                $service_price_input_array = array( "service_id", "=", $_POST['service_id'], "currency_id", "=", $currency_id );
                $service_price_result = book_a_session_get_table_array( "service_price", false, false, "*", $service_price_input_array );
                $service_price_result ? $service_price = $service_price_result[0] : $service_price = false;
                $location_charge_input_array = array( "location_id" => $_POST['location_id'], "currency_id" => $currency_id );
                $location_charge = book_a_session_get_location_charge_by_id( $location_charge_input_array );
                $bundle_discount_input_array = array( "bundle_id" => $_POST['quantity'], "currency_id" => $currency_id );
                $bundle_discount = book_a_session_get_bundle_currency_by_id(  $bundle_discount_input_array );
                $total_overridden = false;

                $amount_paid = ! empty( $_POST['amount_paid'] ) ? $_POST['amount_paid'] : 0.00; 


                // If they've overriden the base price, location charge, discount, total or total due, (any amounts except amount paid) use their provided amounts

                if ( ! empty( $_POST["base_price"] ) || ! empty( $_POST["location_charge"] ) || ! empty( $_POST["discount"] ) || ! empty( $_POST["total"] ) || ! empty( $_POST["total_due"] ) ) {

                    // Base price determination

                    (float)$base_price = ! empty( $_POST["base_price"] ) ? (float)$_POST["base_price"] : false;
                    (float)$base_price = ! $base_price && ! empty( $service_price ) ? (float)$service_price * $_POST["quantity"] : false;
                    if ( ! $base_price ) $base_price = (float)$region_array["session_price"] * $_POST["quantity"];

                    // Location charge determination

                    (float)$location_charge = ! empty( $_POST["location_charge"] ) ? (float)$_POST["location_charge"] * $_POST["quantity"] : false;
                    (float)$location_charge = ! $location_charge && $location_charge["addition_charge"] ? (float)$location_charge["addition_charge"] * $_POST["quantity"] : 0.00;

                    // Discount determination

                    (float)$discount = ! empty( $_POST["discount"] ) ? (float)$_POST["discount"] * $_POST["quantity"] : false;
                    (float)$discount = ! $discount && $bundle_discount["discount"] ? (float)$bundle_discount["discount"] * $_POST["quantity"] : 0.00;
                    
                    // Total determination

                    (float)$total = ! empty( $_POST["total"] ) ? (float)$_POST["total"] : $base_price + $location_charge - $discount;

                    // Total Due determination

                    (float)$total_due = ! empty( $_POST["total_due"] ) ? (float)$_POST["total_due"] : false;
                    (float)$total_due = ! $total_due ? (float)$_POST["total_due"] : $total - $amount_paid;

                    $total_overridden = true;
                
                // Or if there's a service price set, use that and apply any applicable charges and discounts
                } elseif ( ! empty( $service_price ) ) {

                    // Using Service Price
                    (float)$base_price = (float)$service_price->session_price * $_POST["quantity"];

                    // Add Location charge
                    if ( ! empty( $location_charge ) ) { 

                        (float)$location_charge = (float)$location_charge["addition_charge"] * $_POST["quantity"];

                    } else $location_charge = 0.00;

                    // Subtract Discount
                    if ( ! empty( $discount ) ) {

                        (float)$discount = (float)$bundle_discount["discount"] * $_POST["quantity"];

                    } else $discount = 0.00;

                    (float)$total = $base_price + $location_charge - $discount;
                    (float)$total_due = (float)$total - (float)$amount_paid;

                // Otherwise use the default region price, applying applicable charges and discounts
                } else {

                    // Using Region Price

                    (float)$base_price = (float)$region_array["session_price"] * $_POST["quantity"];

                    // Add Location charge
                    if ( ! empty( $location_charge ) ) { 

                        (float)$location_charge = (float)$location_charge["addition_charge"] * $_POST["quantity"];

                    } else $location_charge = 0.00;

                    // Subtract Discount
                    if ( ! empty( $bundle_discount ) ) {

                        (float)$discount = (float)$bundle_discount["discount"] * $_POST["quantity"];

                    } else $discount = 0.00;

                    (float)$total = $base_price + $location_charge - $discount;
                    (float)$total_due = (float)$total - (float)$amount_paid;

                }

            }

        }

        if ( ! empty( $_POST['quantity'] ) ) {

            // Prepare database insertion

            $order_id = book_a_session_generate_order_id();
            $quantity = $_POST['quantity'];
            $insertionCount = 0;
            $session = $project = 0;
            $add_order_db_result = array();

            if ( ! empty( $_POST['session_project'] ) ) {

                if ( $_POST['session_project'] === 'session' ) { $session = 1; $project = 0; }
                else { $project = 1; $session = 0; }

            } else $session = 1;

            // Insert order

            if (! empty( $_POST['service_id'] )         
            &&  ! empty( $_POST['region_id'] )          
            &&  ! empty( $_POST['practitioner_id'] )    
            &&  ! empty( $_POST['user_id'] )            
            &&  ! empty( $_POST['location_id'] )        
            &&  ! empty( $currency_id )                          
            &&  ! empty( $total )                 
            &&  ! empty( $_POST['payment_method_id'] )  
            &&  ! empty( $_POST['booking_status'] )     
            &&  ! empty( $_POST['payment_status'] )     
            &&  ! empty( $amount_paid )            
            ) :

            global $wpdb;
            $table_name_order = $wpdb->prefix . "book_a_session_order";

            // Prepare order values

            $order_value_array = array( 
                'order_id'          => $order_id,
                'created'           => date("Y-m-d H:i:s"), 
                'session'           => $session,
                'project'           => $project,          
                'quantity'          => $_POST['quantity'], 
                'service_id'        => $_POST['service_id'], 
                'region_id'         => $_POST['region_id'], 
                'practitioner_id'   => $_POST['practitioner_id'], 
                'user_id'           => $_POST['user_id'], 
                'location_id'       => $_POST['location_id'], 
                'payment_method_id' => $_POST['payment_method_id'], 
                'currency_id'       => $currency_id, 
                'base_price'        => $base_price,
                'location_charge'   => $location_charge,
                'discount'          => $discount,
                'total'             => $total, 
                'amount_paid'       => $amount_paid, 
                'total_due'         => $total_due,
                'booking_status'    => $_POST['booking_status'], 
                'payment_status'    => $_POST['payment_status'], 
                'vfc_mobile'        => $_POST['vfc_mobile'], 
                'note'              => $_POST['note']
            );

            // Insert order

            $order_result = $wpdb->insert( 
                $table_name_order, 
                $order_value_array, 
                array( 
                    '%d', // Order ID
                    '%s', // Created
                    '%d', // Session
                    '%d', // Project
                    '%d', // Quantity
                    '%d', // Service ID
                    '%d', // Region ID
                    '%d', // Practitioner ID
                    '%d', // User ID
                    '%d', // Location ID
                    '%d', // Payment Method ID
                    '%d', // Currency ID
                    '%f', // Base Price
                    '%f', // Location Charge
                    '%f', // Discount
                    '%f', // Total
                    '%f', // Amount Paid
                    '%f', // Total Due
                    '%s', // Booking Status
                    '%s', // Payment Status
                    '%s', // VFC Mobile
                    '%s', // Note
  
                    ) 
            );

            $add_order_db_result[] = $order_result;

            // Loop through the posted quantity

            for ($i = 0; $i < $quantity; $i++) :

                global $wpdb;
                $table_name_session = $wpdb->prefix . "book_a_session_session";

                // Check all required values have been posted or obtained otherwise
                
                if ( $session ) :

                    // Prepare session values

                    $session_datetime = book_a_session_get_session_time( $_POST['session_time'][$i], false, $_POST['session_date'][$i] );

                    $session_value_array = array( 
                        'order_id'          => $order_id,
                        'schedule_id'       => $_POST['session_time'][$i],
                        'date'              => $_POST['session_date'][$i],
                        'start_datetime'    => $session_datetime["start_datetime_object"]->format("Y-m-d H:i:s"),
                        'end_datetime'      => $session_datetime["end_datetime_object"]->format("Y-m-d H:i:s"),
                    );

                    // Insert order

                    $session_result = $wpdb->insert( 
                        $table_name_session, 
                        $session_value_array, 
                        array( 
                            '%d', // Order ID
                            '%d', // Schedule ID
                            '%s', // Date
                            '%s', // Start Date Time
                            '%s', // End Date Time
                            ) 
                    );

                    $add_order_db_result[] = $session_result;

                else :

                    // TO DO: Add Project Start Date and Project End Fields that appear if $_POST['session_project] === 'project' 

                endif;

            endfor;

        endif;
            
    
    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_validation_error' );
    }

        // Create Invoice

        if ( ! in_array( false, $add_order_db_result, true ) ) {
           
            // Set defaults for optional values

            $_POST['payment_method_id'] == 3 ? $vfc_invoice         = 1 : $vfc_invoice      = 0;
            $total_overridden                ? $total_overridden    = 1 : $total_overridden = 0;

            $session_date_and_time = array();

            for ( $i = 0; $i < count( $quantity ); $i++ ) {

                if ( $_POST['session_date'][$i] && $_POST['session_time'][$i] ) {

                    $datetime = book_a_session_get_session_time( $_POST['session_time'][$i], false, $_POST['session_date'][$i] );

                    $session_date_and_time[] = array(
                        "date" => $_POST['session_date'][$i],
                        "time" => $datetime["time_string"],
                        "start_datetime_object" => $datetime["start_datetime_object"],
                        "end_datetime_object" => $datetime["end_datetime_object"],
                    );
    
                }

            }

            usort( $session_date_and_time, function( $a, $b ) {

                $ad = $a["start_datetime_object"];
                $bd = $b["start_datetime_object"];

                if ( $ad == $bd ) {

                    return 0;

                }

                return $ad < $bd ? -1 : 1;

            } );            
            
            // Determine payment instructions

            if (
                $_POST['payment_status'] == 'Cash payment due'              || 
                $_POST['payment_status'] == 'Online payment due'            || 
                $_POST['payment_status'] == 'Vodafone Cash payment due'     ||
                $_POST['payment_status'] == 'Cash payment partly paid'      ||
                $_POST['payment_status'] == 'Online payment partly paid'    ||
                $_POST['payment_status'] == 'Vodafone Cash partly paid'     ){ 

                $due = 1;
                $due_by_date = $session_date_and_time[0]["start_datetime_object"]->format( "Y-m-d H:i:s" );

                if ( $_POST['payment_status'] == 'Cash payment due' || $_POST['payment_status'] == 'Cash payment partly paid' ) { 

                    // Cash instructions
                    $cash_due_instruction = get_option( 'book_a_session_options' )['payment_method_cash_due_instructions'] ? get_option( 'book_a_session_options' )['payment_method_cash_due_instructions'] : 'Cash payment due on first meeting.';
                    $due_by_instruction = $cash_due_instruction;
                    
                } elseif ( $_POST['payment_status'] == 'Vodafone Cash payment due' || $_POST['payment_status'] == 'Vodafone Cash partly paid' ) {

                    // Get Vodafone Cash invoice option values

                    $receiving_vfc_mobile           = get_option( 'book_a_session_options' )['payment_method_vfc_receiving_mobile_number'];
                    $due_date_offset_measurement    = get_option( 'book_a_session_options' )['payment_method_vfc_due_offset_measurement'];
                    $due_date_offset_anchor         = get_option( 'book_a_session_options' )['payment_method_vfc_due_offset_anchor'];
                    $due_date_offset_value          = get_option( 'book_a_session_options' )['payment_method_vfc_due_offset_value'];

                    if ( (int)$due_date_offset_value === 1 ) {

                        // Remove 's' at end of string (minutes, hours, days) when it's only one minute, hour, or day.

                        $due_date_offset_measurement = preg_replace( "/s$/", "", $due_date_offset_measurement );

                    }

                    // If due date options have been entered and retrieved correctly

                    if ( ! empty( $due_date_offset_anchor ) && ! empty( $due_date_offset_measurement ) && ! empty( $due_date_offset_value ) ) {

                        // Calculate due date if the period is to be measured from the issue date, i.e. issue date +1 day

                        if ( $due_date_offset_anchor === 'issue_date' ) {

                            $issue_date = date("Y-m-d H:i:s");
                            $str = '+' . $due_date_offset_value . ' ' . $due_date_offset_measurement;
                            $due_by_date = date( "Y-m-d H:i:s", strtotime( $str, strtotime( $issue_date ) ) );

                        // Calculate due date if the period is to be measured from the session date, by getting the first session date, its time, putting them together as a datetime object, and modifying it according to user options, i.e. session date -1 day.

                        } elseif ( $due_date_offset_anchor === 'session_date' ) {
    
                            $session_date =  $_POST['session_date'][0];
                            $session_time = book_a_session_get_table_array( "schedule", false, false, "start_time", array( "id", "=", $_POST["session_time"][0] ) )[0]->start_time;
                            $session_date = date( $session_date . " " . $session_time );
                            $str = '-' . $due_date_offset_value . ' ' . $due_date_offset_measurement;
                            $due_by_date = date( "Y-m-d H:i:s", strtotime( $str, strtotime( $session_date ) ) );

                        }

                    }

                    // VFC instructions

                    $vfc_due_instruction = get_option( 'book_a_session_options' )['payment_method_vfc_due_instructions'] ? get_option( 'book_a_session_options' )['payment_method_vfc_due_instructions'] : 'Vodafone Cash payment due before order can be confirmed. Please send the payment to the number above. Once received and confirmed, we\'ll send you your confirmation email. Thanks!';
                    $due_by_instruction = $vfc_due_instruction;

                } elseif ( $_POST['payment_status'] == 'Online payment due' || $_POST['payment_status'] == 'Online payment partly paid' ) {

                    // Online payment instructions

                    $online_due_instruction = get_option( 'book_a_session_options' )['payment_method_online_due_instructions'] ? get_option( 'book_a_session_options' )['payment_method_online_due_instructions'] : 'Payment due before order can be confirmed.';
                    $due_by_instruction = $online_due_instruction;

                }

            } else {

                $due                =    0;
                $due_by_date        = date("Y-m-d H:i:s");
                $due_by_instruction = null;

            }

            // Set price breakdown values and totals

            (float)$grand_total = ( (float)$base_price + (float)$location_charge - (float)$discount ) * $_POST['quantity'];
            (float)$grand_total_due = (float)$grand_total - (float)$amount_paid;

            // Prepare invoice insertion

            global $wpdb;
            $table_name_invoice = $wpdb->prefix . "book_a_session_invoice";

            // Prepare invoice values

            $invoice_value_array = array(

                'issue_date'            => date("Y-m-d"),
                'order_id'              => $order_id,
                'user_id'               => $_POST['user_id'],
                'vfc_invoice'           => $vfc_invoice,
                'vfc_mobile_number'     => $_POST['vfc_mobile'],
                'due'                   => $due,
                'due_by_date'           => $due_by_date,
                'due_by_instruction'    => $due_by_instruction,
                'total_overridden'      => $total_overridden,
                'currency_id'           => $currency_id,
                'base_price'            => $base_price,
                'location_charge'       => $location_charge,
                'discount'              => $discount,
                'quantity'              => $_POST['quantity'],
                'amount_paid'           => $amount_paid,
                'total'                 => $total,
                'grand_total'           => $grand_total,
                'grand_total_due'       => $grand_total_due

            );

            // Prepare invoice value formats

            $invoice_format_array = array(

                '%s', // Issue Date
                '%d', // Order ID
                '%d', // User ID
                '%d', // VFC Invoice
                '%s', // VFC Mobile Number
                '%d', // Due
                '%s', // Due By Date
                '%s', // Due By Instruction
                '%d', // Total Overridden
                '%d', // Currency ID
                '%f', // Base Price
                '%f', // Location Charge
                '%f', // Discount
                '%d', // Quantity
                '%f', // Amount Paid
                '%f', // Total
                '%f', // Grand Total
                '%f'  // Grand Total Due

            );

            // Add Invoice

            global $wpdb;
            $table_name_invoice = $wpdb->prefix . "book_a_session_invoice";

            $add_invoice_db_result = $wpdb->insert( $table_name_invoice, $invoice_value_array, $invoice_format_array );

            // If that worked, check if we are required to send any invoice emails

            if ( ! empty ( $add_invoice_db_result ) ) {

                // Get invoice id

                $added_invoice = $wpdb->get_row( "SELECT id from $table_name_invoice ORDER BY id DESC LIMIT 1", OBJECT );
                $added_invoice_id = $added_invoice->id;

                // Initialise recipients argument array for book_a_session_send_invoice()

                $recipients = array();

                // Populate the array depending on posted values

                $recipients["client"]       = ! empty( $_POST['send_invoice_client'] )          ? true : false;
                $recipients["practitioner"] = ! empty( $_POST['send_invoice_practitioner'] )    ? true : false;
                $recipients["self"]         = ! empty( $_POST['send_invoice_self'] )            ? true : false;

                // If any of the above are true, send invoice

                if ( in_array( true, $recipients, true ) ) {

                    $invoice_send_result[] = book_a_session_send_invoice( $added_invoice_id, false, $recipients );

                }


            }
            $table_name_order = $wpdb->prefix . "book_a_session_order";
            $table_name_session = $wpdb->prefix . "book_a_session_session";

            // Get the order we just inserted by searching for the order ID we used, using the order id and associated data to add timetable entries

            $order = $wpdb->get_row(
                "
                SELECT id, created, service_id, practitioner_id, user_id, location_id 
                FROM $table_name_order 
                WHERE order_id = $order_id
                ", OBJECT );

            $sessions = $wpdb->get_results(
                "
                SELECT * 
                FROM $table_name_session 
                WHERE order_id = $order_id
                ", OBJECT );

            // Prepare timetable insertion

            $table_name_timetable = $wpdb->prefix . "book_a_session_timetable";
            $add_timetable_db_result = array();

            // If we found more than one order, loop over the array and insert each order into the timetable

            if ( $order ) {

                if ( is_array( $sessions ) ) {

                    foreach ( $sessions as $session_row ) {
                        
                        // Prepare timetable values for each order
        
                        $timetable_value_array = array(
        
                            'date'              => $session_row->date,
                            'start_datetime'    => $session_row->start_datetime,
                            'end_datetime'      => $session_row->end_datetime,
                            'schedule_id'       => $session_row->schedule_id,
                            'order_id'          => $order_id, 
                            'service_id'        => $order->service_id,
                            'location_id'       => $order->location_id,
                            'practitioner_id'   => $order->practitioner_id, 
                            'client_id'         => $order->user_id,
        
                        );
        
                        // Prepare timetable value formats for each order
        
                        $timetable_format_array = array(
        
                            '%s', // Date
                            '%s', // Start Datetime
                            '%s', // End Datetime
                            '%d', // Schedule ID
                            '%d', // Service ID
                            '%d', // order ID
                            '%d', // Location ID
                            '%d', // Practitioner ID
                            '%d', // Client ID
        
                        );
        
                        // Add Timetable entries
        
                        $add_timetable_db_result[] = $wpdb->insert( $table_name_timetable, $timetable_value_array, $timetable_format_array );
        
                    }

                // Otherwise insert a single order
        
                } else {

                    $timetable_value_array = array(
        
                        'date'              => $sessions->date,
                        'start_datetime'    => $sessions->start_datetime,
                        'end_datetime'      => $sessions->end_datetime,
                        'schedule_id'       => $sessions->schedule_id,
                        'order_id'          => $order_id, 
                        'service_id'        => $order->service_id,
                        'location_id'       => $order->location_id,
                        'practitioner_id'   => $order->practitioner_id, 
                        'client_id'         => $order->user_id,
    
                    );
    
                    // Prepare timetable value formats for each order
    
                    $timetable_format_array = array(
    
                        '%s', // Date
                        '%s', // Start Datetime
                        '%s', // End Datetime
                        '%d', // Schedule ID
                        '%d', // Service ID
                        '%d', // order ID
                        '%d', // Location ID
                        '%d', // Practitioner ID
                        '%d', // Client ID
    
                    );

                    // Add Timetable entry

                    $add_timetable_db_result[] = $wpdb->insert( $table_name_timetable, $timetable_value_array, $timetable_format_array );

                }

            }

            // Determine success

            add_action( 'admin_notices', 'book_a_session_admin_notice_add_success' );            

        } else {
            add_action( 'admin_notices', 'book_a_session_admin_notice_add_error' );
        }

    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_add_security_error' );
    }

}

if ( ! class_exists( 'WP_List_Table' ) ) require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );

if ( ! class_exists( 'Orders_List_Table' ) ) require_once plugin_dir_path( __FILE__ ) . 'class-orders-list-table.php';


function book_a_session_display_orders_settings_page() {
	if ( ! current_user_can( 'manage_options' ) ) return;

    ?>
	<div class="wrap">
		<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
		<?php 

function page_tabs_order( $current = 'View' ) {
    $tabs = array(
        'view'   => __( 'View orders', 'book_a_session' ), 
		'add'  => __( 'Add order', 'book_a_session' ),
		'edit'  => __( 'Edit orders', 'book_a_session' )
    );
    $html = '<h2 class="nav-tab-wrapper">';
    foreach( $tabs as $tab => $name ){
        $class = ( $tab == $current ) ? 'nav-tab-active' : '';
        $html .= '<a class="nav-tab ' . $class . '" href="?page=book_a_session_orders&tab=' . $tab . '">' . $name . '</a>';
    }
    $html .= '</h2>';
    echo $html;
}

// Tabs
$tab = ( ! empty( $_GET['tab'] ) ) ? esc_attr( $_GET['tab'] ) : 'view';
page_tabs_order( $tab );

// View orders
if ( $tab == 'view' ) {

	echo '<h3>View orders</h3>';
	echo '<form>';
	$orders_list = new orders_List_Table;
    echo '</form>';

}

// Add order
elseif ( $tab == 'add' ) {

	echo '<h2>Add a new order manually</h2>';
	$quantity;
	$service_price_id;
    $user_id;
    echo '<p>If a customer can\'t make an order for themselves on the front end of the website, you can create an order for them here.';
    echo '<br>There are a few differences between creating an order here as an administrator, and creating an order on the front end as a customer. Read the guide at the bottom of the page for further information.</p>';
    echo '<h3>Add an order form</h3>';
    $html =     "<table class='form-table'><form method='POST'>";
    $html .=    "<tbody>";

    // Session or Project radio required
    $html .=    "<tr id='order_add_row_session_project'><th scope='row'>";
    $html .=    "<label for='session_project'>Order Type</label></th>";
    $html .=    "<td><label><input type='radio' name='session_project' id='order_add_session_project' value='session'>Session Order</label>";
    $html .=    "<label><input type='radio' name='session_project' id='order_add_session_project' value='project'>Project Order</label>";
    $html .=    "</td></tr>";

    // Region select required
    $html .=    "<tr id='order_add_row_region_id'><th scope='row'>";
    $html .=    "<label for='region_id'>Region</label></th>";
    $html .=    "<td><select name='region_id' id='order_add_region_id' required>";
    $region_array = book_a_session_get_table_array( "region" );
    if ( ! empty( $region_array ) ) {
        if ( empty( $_POST['region_id'] ) ) {
            $html .= "<option selected disabled>Select a region</option>";
        } else {
            $html .= "<option disabled>Select a region</option>";
        }
        for ($i = 0; $i < count($region_array); $i++) {
            $html .= "<option value='" . $region_array[$i]->id . "'";
            if ( isset( $_POST['submit_add_order'] ) ) {
                if ( ! empty( $_POST['region_id'] ) ) {
                    if ( $_POST['region_id'] == $region_array[$i]->id ) {
                        $html .= " selected";
                    } 
                }
            }
            $html .= ">";
            $html .= $region_array[$i]->name . ". Timezone: " . $region_array[$i]->timezone . ". Session price: " . book_a_session_get_price_tag( $region_array[$i]->session_price, $region_array[$i]->currency_id ) . ".";
            $html .= "</option>";
        }
        $html .= "</select>";
        if ( isset( $_POST['submit_add_order'] ) && empty( $_POST['region_id'] ) ) {
            $html .= "<p class='description book-a-session-error'>Please select a valid region.</p>";
        }
        $html .= "</td>";
    } else {
        $html .= "<option selected disabled>No regions found</option>";
        $html .= "</select><br><small>Add a Region in the Regions settings page</small></td>";
    }    
    // Service select required
    $html .=    "<tr id='order_add_row_service_id'><th scope='row'>";
    $html .=    "<label for='service_id'>Service</label></th>";
    $html .=    "<td><select name='service_id' id='order_add_service_id' required>";    
    $service_array = book_a_session_get_table_array( "service" );
    if ( ! empty( $service_array ) ) {
        if ( empty( $_POST['service_id'] ) ) {
            $html .= "<option selected disabled>Select a service</option>";
        } else {
            $html .= "<option disabled>Select a service</option>";
        }
        for ($i = 0; $i < count($service_array); $i++) {
            $html .= "<option value='" . $service_array[$i]->id . "'";
            if ( isset( $_POST['submit_add_order'] ) ) {
                if ( ! empty( $_POST['service_id'] ) ) {
                    if ( $_POST['service_id'] == $service_array[$i]->id ) {
                        $html .= " selected";
                    } 
                }
            }
            $html .= ">";
            $html .= $service_array[$i]->name;
            $html .= "</option>";
        }
        $html .= "</select>";
        if ( isset( $_POST['submit_add_order'] ) ) {
            if ( empty( $_POST['service_id'] ) ) {
              $html .= "<p class='description book-a-session-error'>Please select a service.</p>";
            }
        } 
        $html .= "</td>";
    } else {
        $html .= "<option selected disabled>No services found</option>";
        $html .= "</select><br><small>Add a Service in the Services settings page</small></td>";
    }
    // Bundle quantity select required
    $html .=    "<tr id='order_add_row_quantity'><th scope='row'>";
    $html .=    "<label for='quantity'>Quantity</label></th>";
    $html .=    "<td><select name='quantity' id='order_add_quantity' required>";
    $bundle_array = book_a_session_get_table_array( "bundle", "quantity", "ASC" );
    $highest_quantity;
    if ( ! empty( $bundle_array ) ) {
        if ( empty( $_POST['quantity'] ) ) {
            $html .= "<option selected disabled>Select a quantity</option>";
        } else {
            $html .= "<option disabled>Select a quantity</option>";
        }
        $html .=    "<option value='1'>Project</option>";
        for ($i = 0; $i < count($bundle_array); $i++) {
            $highest_quantity = $bundle_array[$i]->quantity;
            $html .= "<option value='" . $bundle_array[$i]->quantity . "'";
            if ( isset( $_POST['submit_add_order'] ) ) {
                if ( ! empty( $_POST['quantity'] ) ) {
                    if ( $_POST['quantity'] == $bundle_array[$i]->quantity ) {
                        $html .= " selected";
                    } 
                }
            }
            $html .= ">";
            (int)$bundle_array[$i]->quantity > 1 ? $html .= $bundle_array[$i]->quantity . ' sessions' : $html .= $bundle_array[$i]->quantity . ' session';
            $html .= "</option>";
        }
        $html .= "</select>";
        if ( isset( $_POST['submit_add_order'] ) ) {
            if ( empty( $_POST['quantity'] ) ) {
              $html .= "<p class='description book-a-session-error'>Please select a quantity.</p>";
            }
        } 
        $html .= "</td>";
    } else {
        $html .= "<option selected disabled>Select a quantity</option>";
        $html .= "<option value='1'>Project</option>";
        $html .= "<option value='1'>1 session</option>";
        $html .= "</select><br><p class='description'>Add more quantities in the Bundle settings page.</sp></td>";
    }

    // Session Date input[type="date"]

    for ($i = 0; $i < $highest_quantity; $i++) {
        
        $html .=    "<tr id='order_add_row_session_date_" . $i . "' class='order_add_row_session_date'><th scope='row'>";
        $html .=    "<label for='session_date[" . $i . "]'>Date of session</label></th>";
        $html .=    "<td><input type='date' name='session_date[" . $i . "]' id='order_add_session_date_" . $i . "'";
        $html .=    " min='" . date("Y-m-d", time() + 86400) . "'";
        if ( isset( $_POST['submit_add_order'] ) ) {
            if ( ! empty( $_POST['session_date'][$i] ) ) {
                $html .= " value='" . $_POST['session_date'][$i] . "'>";
            } else {
                $html .= ">";
                $html .= "<p class='description book-a-session-error'>Please enter a valid date.</p>";
            }
        }
        $html .=    "</td></tr>";

        // Session Time select required
        $html .=    "<tr id='order_add_row_session_time_" . $i . "' class='order_add_row_session_time'><th scope='row'>";
        $html .=    "<label for='session_time[" . $i . "]'>Time of session</label></th>";
        $html .=    "<td><select name='session_time[" . $i . "]' id='order_add_session_time_" . $i . "' required>";    
        $schedule_array = book_a_session_get_table_array( "schedule", "start_time", "ASC" );
        if ( ! empty( $schedule_array ) ) {
            if ( empty( $_POST['session_time'][$i] ) ) {
                $html .= "<option selected disabled>Select a session time</option>";
            } else {
                $html .= "<option disabled>Select a session time</option>";
            }
            for ($j = 0; $j < count($schedule_array); $j++) {
                $html .= "<option value='" . $schedule_array[$j]->id . "'";
                if ( isset( $_POST['submit_add_order'] ) ) {
                    if ( ! empty( $_POST['session_time'][$i] ) ) {
                        if ( $_POST['session_time'][$i] == $schedule_array[$j]->id ) {
                            $html .= " selected";
                        } 
                    }
                }
                $html .= ">";
                $html .= book_a_session_get_session_time($schedule_array[$j]->id)["time_string"];
                $html .= "</option>";
            }
            $html .= "</select>";
            if ( isset( $_POST['submit_add_order'] ) ) {
                if ( empty( $_POST['session_time'][$i] ) ) {
                $html .= "<p class='description book-a-session-error'>Please select a session time.</p>";
                }
            } 
            $html .= "</td>";
        } else {
            $html .= "<option selected disabled>No session times found</option>";
            $html .= "</select><br><p class='description'>Add a Session time in the Schedule settings page</p></td>";
        }

    }
    // Location select required
    $html .=    "<tr id='order_add_row_location_id'><th scope='row'>";
    $html .=    "<label for='location_id'>Location</label></th>";
    $html .=    "<td><select name='location_id' id='order_add_location_id' required>";    
    $location_array = book_a_session_get_table_array( "location" );
    if ( ! empty( $location_array ) ) {
        if ( empty( $_POST['location_id'] ) ) {
            $html .= "<option selected disabled>Select a location</option>";
        } else {
            $html .= "<option disabled>Select a location</option>";
        }
        for ($i = 0; $i < count($location_array); $i++) {
            $html .= "<option value='" . $location_array[$i]->id . "'";
            if ( isset( $_POST['submit_add_order'] ) ) {
                if ( ! empty( $_POST['location_id'] ) ) {
                    if ( $_POST['location_id'] == $location_array[$i]->id ) {
                        $html .= " selected";
                    } 
                }
            }
            $html .= ">";
            $html .= $location_array[$i]->name;
            $html .= "</option>";
        }
        $html .= "</select>";
        if ( isset( $_POST['submit_add_order'] ) ) {
            if ( empty( $_POST['location_id'] ) ) {
              $html .= "<p class='description book-a-session-error'>Please select a location.</p>";
            }
        } 
        $html .= "</td>";
    } else {
        $html .= "<option selected disabled>No locations found</option>";
        $html .= "</select><br><p class='description'>Add a location in the locations settings page</p></td>";
    }
    // Practitioner select
    $html .=    "<tr id='order_add_row_practitioner_id'><th scope='row'>";
    $html .=    "<label for='practitioner_id'>Practitioner</label></th>";
    $html .=    "<td><select name='practitioner_id' id='order_add_practitioner_id' required>";    
    $practitioner_array = book_a_session_get_practitioner_array();
    if ( ! empty( $practitioner_array ) ) {
        if ( empty( $_POST['practitioner_id'] ) ) {
            $html .= "<option selected disabled>Select a practitioner</option>";
        } else {
            $html .= "<option disabled>Select a practitioner</option>";
        }
        for ($i = 0; $i < count($practitioner_array); $i++) {
            $html .= "<option value='" . $practitioner_array[$i]->ID . "'";
            if ( isset( $_POST['submit_add_order'] ) ) {
                if ( ! empty( $_POST['practitioner_id'] ) ) {
                    if ( $_POST['practitioner_id'] == $practitioner_array[$i]->ID ) {
                        $html .= " selected";
                    } 
                }
            }
            $html .= ">";
            $html .= book_a_session_get_practitioner_name($practitioner_array[$i]->ID);
            $html .= "</option>";
        }
        $html .= "</select>";
        if ( isset( $_POST['submit_add_order'] ) ) {
            if ( empty( $_POST['practitioner_id'] ) ) {
              $html .= "<p class='description book-a-session-error'>Please select a practitioner.</p>";
            }
        } 
        $html .= "</td>";
    } else {
        $html .= "<option selected disabled>No practitioners found</option>";
        $html .= "</select><br><p class='description'>Set your role, or another user's role to Admin Practitioner or Practitioner first.</p></td>";
    }
    // Client select required
    $html .=    "<tr id='order_add_row_user_id'><th scope='row'>";
    $html .=    "<label for='user_id'>Client</label></th>";
    $html .=    "<td><select name='user_id' id='order_add_user_id' required>";    
    $user_array = get_users();
    if ( ! empty( $user_array ) ) {    
        if ( empty( $_POST['user_id'] ) ) {
            $html .= "<option selected disabled>Select a registered user</option>";
        } else {
            $html .= "<option disabled>Select a registered user</option>";
        }
        for ($i = 0; $i < count($user_array); $i++) {
            $html .= "<option value='" . $user_array[$i]->ID . "'";
            if ( isset( $_POST['submit_add_order'] ) ) {
                if ( ! empty( $_POST['user_id'] ) ) {
                    if ( $_POST['user_id'] == $user_array[$i]->ID ) {
                        $html .= " selected";
                    } 
                }
            }
            $html .= ">";
            $html .= $user_array[$i]->first_name . " " . $user_array[$i]->last_name;
            $html .= "</option>";
        }
        $html .= "</select>";
        if ( isset( $_POST['submit_add_order'] ) ) {
            if ( empty( $_POST['user_id'] ) ) {
              $html .= "<p class='description book-a-session-error'>Please select a registered user.</p>";
            }
        } 
        $html .= "</td>";
    } else {
        $html .= "<option selected disabled>No users found</option>";
        $html .= "</select><p class='description'>Please contact the plugin developer.</p></td>";
    }
    // Payment Method select required
    $html .=    "<tr id='order_add_row_payment_method_id'><th scope='row'>";
    $html .=    "<label for='payment_method_id'>Payment Method</label></th>";
    $html .=    "<td><select name='payment_method_id' id='order_add_payment_method_id' required>";    
    $payment_method_array = book_a_session_get_table_array( "payment_method" );
    if ( ! empty( $payment_method_array ) ) {
        $payment_method_option_accepted = false;
        $payment_method_option_count = 0;
        if ( empty( $_POST['payment_method_id'] ) ) {
            $html .= "<option selected disabled>Select a payment method</option>";
        } else {
            $html .= "<option disabled>Select a payment method</option>";
        }
        for ($i = 0; $i < count($payment_method_array); $i++) {
            if ( ! empty ( get_option( 'book_a_session_options_paymentmethods' )[ "accept" . $payment_method_array[$i]->id ] ) ) {
                $payment_method_option_accepted = true;
                $payment_method_option_count++;
                $html .= "<option value='" . $payment_method_array[$i]->id . "'";
                if ( isset( $_POST['payment_method_id'] ) ) {
                    if ( $_POST['payment_method_id'] == $payment_method_array[$i]->id ) {
                            $html .= " selected";
                        } 
                }
                $html .= ">";
                $html .= $payment_method_array[$i]->name;
                $html .= "</option>";
                $payment_method_option_accepted = false;
            } else {
                $html .= "<option value='" . $payment_method_array[$i]->id . "' disabled>";
                $html .= $payment_method_array[$i]->name;
                $html .= "</option>";
            }
        }
        $html .= "</select>";
        if ( isset( $_POST['submit_add_order'] ) ) {
            if ( empty( $_POST['payment_method_id'] ) ) {
                $html .= "<p class='description book-a-session-error'>Please select a payment method.</p>";
            } 
        } elseif ( $payment_method_option_count < 1 ) {
            $html .= "<p class='description'>Accept a payment method in the Book A Session settings page.</p>";
        }
        $html .= "</td>";
    } else {
        $html .= "<option selected disabled>No payment methods found</option>";
        $html .= "</select><p class='description'>Please contact the plugin developer.</p></td>";
    }
    // Base price input[type="number"] step="any" min="0" optional
    $html .=    "<tr id='order_add_row_base_price'><th scope='row'>";
    $html .=    "<label for='order_add_base_price'>Base Price (Optional)</label></th>";
    $html .=    "<td><input type='number' step='any' min='0' name='base_price' id='order_add_base_price' placeholder='0.00' style='min-width:300px;'";
    if ( isset( $_POST['base_price'] ) ) {
        $html .= " value='" . $_POST['base_price'] . "'>";
    } else {
        $html .= ">";
    }
    $html .=    "</td></tr>";
    // Location charge input[type="number"] step="any" min="0" optional
    $html .=    "<tr id='order_add_row_location_charge'><th scope='row'>";
    $html .=    "<label for='order_add_location_charge'>Location Charge (Optional)</label></th>";
    $html .=    "<td><input type='number' step='any' min='0' name='location_charge' id='order_add_location_charge' placeholder='0.00' style='min-width:300px;'";
    if ( isset( $_POST['location_charge'] ) ) {
        $html .= " value='" . $_POST['location_charge'] . "'>";
    } else {
        $html .= ">";
    }
    $html .=    "</td></tr>";
    // Discount input[type="number"] step="any" min="0" optional
    $html .=    "<tr id='order_add_row_discount'><th scope='row'>";
    $html .=    "<label for='order_add_discount'>Discount (Optional)</label></th>";
    $html .=    "<td><input type='number' step='any' min='0' name='discount' id='order_add_discount' placeholder='0.00' style='min-width:300px;'";
    if ( isset( $_POST['discount'] ) ) {
        $html .= " value='" . $_POST['discount'] . "'>";
    } else {
        $html .= ">";
    }
    $html .=    "</td></tr>";
    // Total input[type="number"] step="any" min="0" optional
    $html .=    "<tr id='order_add_row_total'><th scope='row'>";
    $html .=    "<label for='order_add_total'>Total (Optional)</label></th>";
    $html .=    "<td><input type='number' step='any' min='0' name='total' id='order_add_total' placeholder='0.00' style='min-width:300px;'";
    if ( isset( $_POST['total'] ) ) {
        $html .= " value='" . $_POST['total'] . "'>";
    } else {
        $html .= ">";
    }
    $html .=    "</td></tr>";
    // Amount Paid input[type="number"] step="any" min="0" optional
    $html .=    "<tr id='order_add_row_amount_paid'><th scope='row'>";
    $html .=    "<label for='order_add_amount_paid'>Amount Paid (Optional)</label></th>";
    $html .=    "<td><input type='number' step='any' min='0' name='amount_paid' id='order_add_amount_paid' placeholder='0.00' style='min-width:300px;'";
    if ( isset( $_POST['amount_paid'] ) ) {
        $html .= " value='" . $_POST['amount_paid'] . "'>";
    } else {
        $html .= ">";
    }
    $html .=    "</td></tr>";
    // Total Due input[type="number"] step="any" min="0" optional
    $html .=    "<tr id='order_add_row_total_due'><th scope='row'>";
    $html .=    "<label for='order_add_total_due'>Total Due (Optional)</label></th>";
    $html .=    "<td><input type='number' step='any' min='0' name='total_due' id='order_add_total_due' placeholder='0.00' style='min-width:300px;'";
    if ( isset( $_POST['total_due'] ) ) {
        $html .= " value='" . $_POST['total_due'] . "'>";
    } else {
        $html .= ">";
    }
    $html .=    "</td></tr>";
    // VFC Mobile input[type="text"] optional
    $html .=    "<tr id='order_add_row_vfc_mobile'><th scope='row'>";
    $html .=    "<label for='vfc_mobile'>Vodafone Cash Mobile Number (Optional)</label>";
    $html .=    "</th><td><input type='text' name='vfc_mobile' id='order_add_vfc_mobile' placeholder='Enter if Vodafone Cash payment' style='min-width:300px;'"; 
    if ( isset( $_POST['vfc_mobile'] ) ) {
        $html .= " value='" . $_POST['vfc_mobile'] . "'>";
    } else {
        $html .= ">";
    }
    $html .=    "</td></tr>";
    // Booking Status select required
    $html .=    "<tr id='order_add_row_booking_status'><th scope='row'>";
    $html .=    "<label for='booking_status'>Booking Status</label></th>";
    $html .=    "<td><select name='booking_status' id='order_add_booking_status' required>";    
    $booking_status_array = book_a_session_get_booking_status_array();
    if ( ! empty( $booking_status_array ) ) {
        if ( empty( $_POST['booking_status'] ) ) {
                $html .= "<option selected disabled>Select a Booking Status</option>";
            } else {
                $html .= "<option disabled>Select a Booking Status</option>";
        }    
        for ($i = 0; $i < count($booking_status_array); $i++) {
            
            $html .= "<option value='" . str_replace( "'", "", $booking_status_array[$i] ). "'";
            if ( isset( $_POST['booking_status'] ) ) {
                if ( $_POST['booking_status'] == str_replace( "'", "", $booking_status_array[$i] ) ) {
                        $html .= " selected";
                } 
            }
            $html .= ">";
            $html .= str_replace( "'", "", $booking_status_array[$i] );
            $html .= "</option>";        
        }
        $html .= "</select>";
        if ( isset( $_POST['submit_add_order'] ) ) {
            if ( empty( $_POST['booking_status'] ) ) {
              $html .= "<p class='description book-a-session-error'>Please select a Booking Status.</p>";
            }
        } 
        $html .= "</td>";
    } else {
        $html .= "<option selected disabled>No Booking Statuses found</option>";
        $html .= "</select><p class='description'>Please contact the plugin developer.</p></td>";
    }
    // Payment status select required
    $html .=    "<tr id='order_add_row_payment_status'><th scope='row'>";
    $html .=    "<label for='payment_status'>Payment Status</label></th>";
    $html .=    "<td><select name='payment_status' id='order_add_payment_status' required>";    
    $payment_status_array = book_a_session_get_payment_status_array();
    if ( ! empty( $payment_status_array ) ) {
        if ( empty( $_POST['payment_status'] ) ) {
                $html .= "<option selected disabled>Select a payment status</option>";
            } else {
                $html .= "<option disabled>Select a payment status</option>";
        }    
        for ($i = 0; $i < count($payment_status_array); $i++) {
            
            $html .= "<option value='" . str_replace( "'", "", $payment_status_array[$i] ). "'";
            if ( isset( $_POST['payment_status'] ) ) {
                if ( $_POST['payment_status'] == str_replace( "'", "", $payment_status_array[$i] ) ) {
                        $html .= " selected";
                } 
            }
            $html .= ">";
            $html .= str_replace( "'", "", $payment_status_array[$i] );
            $html .= "</option>";        
        }
        $html .= "</select>";
        if ( isset( $_POST['submit_add_order'] ) ) {
            if ( empty( $_POST['payment_status'] ) ) {
              $html .= "<p class='description book-a-session-error'>Please select a payment status.</p>";
            }
        } 
        $html .= "</td>";
    } else {
        $html .= "<option selected disabled>No payment statuses found</option>";
        $html .= "</select><p class='description'>Please contact the plugin developer.</p></td>";
    }
    // Order ID
    $html .=    "<tr id='order_add_row_order_id'><th scope='row'>";
    $html .=    "<label for='order_id'>Order ID (Optional)</label></th>";   
    $html .=    "<td><input type='text'name='order_id' id='order_add_order_id' placeholder='10 digits' maxlength='10' style='min-width:300px;'";
    if ( isset( $_POST['order_id'] ) ) {
        $html .= " value='" . $_POST['order_id'] . "'>";
    } else {
        $html .= " value=''>";
    }
    $html .=    "</td></tr>";
    // Note
    $html .=    "<tr id='order_add_row_note'><th scope='row'>";
    $html .=    "<label for='note'>Note (Optional)</label></th>";
    $html .=    "<td><textarea name='note' id='order_add_note' placeholder='Additional information' style='min-width:300px;'";
    if ( isset( $_POST['note'] ) ) {
        $html .= " value='" . esc_textarea( stripslashes( $_POST['note'] ) ) . "'></textarea>";
    } else {
        $html .= "></textarea>";
    }
    $html .=    "</td></tr>";

    // Send Invoice checkbox required
    $html .=    "<tr id='order_add_row_send_invoice_client'><th scope='row'>";
    $html .=    "<label for='send_invoice_client'>Send Invoice to Client</label>";
    $html .=    "</th><td><label for='send_invoice_client'><input type='checkbox' id='send_invoice_client' name='send_invoice_client' value='1'"; 
    if ( isset( $_POST['send_invoice_client'] ) ) {
        $html .= " checked>";
    } else {
        $html .= ">";
    }
    $html .=    "Check to send an invoice for this order to the client's registered email address.</label>";
    $html .=    "</td></tr>";
    
    // Send Invoice Practitioner checkbox required
    $html .=    "<tr id='order_add_row_send_invoice_practitioner'><th scope='row'>";
    $html .=    "<label for='send_invoice_practitioner'>Send Invoice To Practitioner</label>";
    $html .=    "</th><td><label for='send_invoice_practitioner'><input type='checkbox' id='send_invoice_practitioner' name='send_invoice_practitioner' value='1'"; 
    if ( isset( $_POST['send_invoice_practitioner'] ) ) {
        $html .= " checked>";
    } else {
        $html .= ">";
    }
    $html .=    "Check to send an invoice for this order to the practitioner's registered email address.</label>";
    $html .=    "</td></tr>";

    // Send Invoice self checkbox required
    $html .=    "<tr id='order_add_row_send_invoice_self'><th scope='row'>";
    $html .=    "<label for='send_invoice_self'>Send Invoice To Yourself</label>";
    $html .=    "</th><td><label for='send_invoice_self'><input type='checkbox' id='send_invoice_self' name='send_invoice_self' value='1'"; 
    if ( isset( $_POST['send_invoice_self'] ) ) {
        $html .= " checked>";
    } else {
        $html .= ">";
    }
    $html .=    "Check to send an invoice for this order to your registered email address.</label>";
    $html .=    "</td></tr>";



    $html .=    "</tbody></table>";
    $html .=    "<input type='submit' class='button button-primary' value='Add order' name='submit_add_order'>";
    $html .=    "</form>";

	
    echo $html;

    echo '<br><br><h3>Guide for using this form</h3>';
    echo '<table class="form-table"><tbody>';
    echo '<tr><th scope="col">Client</th><td>If the client does not already have a user account, one must be created. You can ask them to register for themselves, or you can create their account for them. When doing so, you <strong>must</strong> provide a <strong>username</strong>, their <strong>first name</strong>, <strong>last name</strong>, ';
    echo 'and their <strong>email address</strong>, despite WordPress not usually requiring first and last names. You can then select their account in the Client drop down selection field below. Create a new user <a href="' . get_admin_url() . 'user-new.php" target="_blank">here</a>.</td></tr>';
    echo '<tr><th scope="col">Region & Location</th><td>The front end form automatically prevents clients from selecting Locations unavailable in their selected Region, but this one does not. So please ensure you select their correct Region and Location.</td></tr>';
    echo '<tr><th scope="col">Total</th><td>This form gives you the option of overriding the total price for this order, accounting for all of its sessions or the entire project. This can be used to provide a special discount or to account for an unusual expense. ';
    echo 'Keep in mind, the price will apply to every session in the bundle. Overriding the Total also negates any discounts and extra Location charges that would otherwise be automatically applied according to the Bundle Discounts and Location Charges entered when setting up the plugin. So keep the Total blank in most cases.</td></tr>';
    echo '<tr><th scope="col">Vodafone Cash</th><td>The front end form forces the client to enter their Vodafone Cash mobile phone number when they\'ve selected Vodafone Cash as their payment method. However, this form does not. Please ensure you enter their mobile in such cases, as it is used to confirm that they\'ve sent the payment. Only then can the order be finalised.</td></tr>';
    echo '<tr><th scope="col">Booking Status</th><td>The front end form does not include this field, however this one does. Here you can specify the status of the order. Booked means the order will occur without any further steps, so Practitioners and Clients should meet at the Location on the date and time specified. ';
    echo 'Pending means the order requires payment or other further details before being booked. In most cases, Pending is only used when they will be paying with Vodafone Cash, for which, confirmation of receipt of Vodafone Cash payment is required before the Booking Status can be changed to Booked. ';
    echo 'Completed is used when the service has been carried out successfully, and Missing Client and Missing Practitioner can be selected in those circumstances for record keeping purposes.</td></tr>';
    echo '<tr><th scope="col">Payment Status</th><td>As with Booking Status, payment status is handled automatically by the front end form so it is only included here. Online payment due is used for special circumstances where you\'ve created an order manually using this form because you\'ve agreed to accept online payment after finalising their order. The rest of the options are dealt with automatically by the front end form and actions administrators may be asked to take to change the status via email links, but you can also change the payment status manually here, under special circumstances. ';
    echo 'Please select the correct payment status to reflect the payment method that will be used. In most cases, the only two options you would select here on this form are Cash payment due, and Vodafone Cash payment due.</td></tr>';
    echo '<tr><th scope="col">Order ID</th><td>This is usually generated automatically on both this form and the front end form. It is a unique identifier for every order made. When you select a bundle with a quantity higher than 1, each session in the bundle will have its own order, visible as different rows in the View orders table, but they will all have the same Order ID, as they are part of the same order. ';
    echo 'In the case that an administrator or a client didn\'t book as many sessions as they wanted, you can use this form to add a session to their bundle by simply setting the session price to the desired amount, and using the same Order ID by entering it into this field. Otherwise, this field is best left blank so a new ID can be automatically generated.</td></tr>';
    echo '<tr><th scope="col">Payment Method</th><td>If you do not see all the payment methods on this form, it is probably because you may have unselected it in the Book A Session Settings Page, under the Payment Method section. Go there and check all the payment methods you want to accept.</td></tr>';
    echo '</tbody></table>';




} elseif ( $tab == 'edit' ) {

    if ( isset( $_GET["edit"] ) && ! empty( $_GET['order_id'] )  ) {

        echo '<h3>Edit an order</h3>';
        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_order";
        $id = intval( $_GET['order_id'] );

        $row_to_edit = $wpdb->get_row("
        SELECT  *
        FROM    $table_name
        WHERE   id = $id", OBJECT );

        $session_array = book_a_session_get_table_array( "session", false, false, "*", array( "order_id", "=", $row_to_edit->order_id ) );

        $html =     "<table class='form-table'><form method='POST'>";
        // Region select required
        $html .=    "<tbody>";
        $html .=    "<tr id='order_edit_row_region_id'><th scope='row'>";
        $html .=    "<label for='region_id'>Region</label></th>";
        $html .=    "<td><select name='region_id' id='order_edit_region_id' required>";
        $region_array = book_a_session_get_table_array( "region" );
        if ( ! empty( $region_array ) ) {
            for ($i = 0; $i < count($region_array); $i++) {
                $html .= "<option value='" . $region_array[$i]->id . "'";
                if ( isset( $row_to_edit ) ) {
                     if ( $row_to_edit->id == $region_array[$i]->id ) {
                            $html .= " selected";
                    }                
                }
                $html .= ">";
                $html .= $region_array[$i]->name . ". Timezone: " . $region_array[$i]->timezone . ". Session price: " . book_a_session_get_price_tag( $region_array[$i]->session_price, $region_array[$i]->currency_id ) . ".";
                $html .= "</option>";
            }
            $html .= "</select>";
            $html .= "</td>";
        } else {
            $html .= "<option selected disabled>No regions found</option>";
            $html .= "</select><br><small>Add a Region in the Regions settings page</small></td>";
        }    
        // Service select required
        $html .=    "<tr id='order_edit_row_service_id'><th scope='row'>";
        $html .=    "<label for='service_id'>Service</label></th>";
        $html .=    "<td><select name='service_id' id='order_edit_service_id' required>";    
        $service_array = book_a_session_get_table_array( "service" );
        if ( ! empty( $service_array ) ) {
            for ($i = 0; $i < count($service_array); $i++) {
                $html .= "<option value='" . $service_array[$i]->id . "'";
                if ( isset( $row_to_edit ) ) {
                    if ( $row_to_edit->service_id == $service_array[$i]->id ) {
                           $html .= " selected";
                   }                
               }
               $html .= ">";
                $html .= $service_array[$i]->name;
                $html .= "</option>";
            }
            $html .= "</select>";
            $html .= "</td>";
        } else {
            $html .= "<option selected disabled>No services found</option>";
            $html .= "</select><br><small>Add a Service in the Services settings page</small></td>";
        }
        // Bundle quantity select required
        $html .=    "<tr id='order_edit_row_quantity'><th scope='row'>";
        $html .=    "<label for='quantity'>Quantity</label></th>";
        $html .=    "<td><select name='quantity' id='order_edit_quantity' required>";
        $bundle_array = book_a_session_get_table_array( "bundle" );
        if ( ! empty( $bundle_array ) ) {
            $html .= "<option value='1'>Project</option>";
            for ($i = 0; $i < count($bundle_array); $i++) {
                $highest_quantity = $bundle_array[$i]->quantity;
                $html .= "<option value='" . $bundle_array[$i]->quantity . "'";
                if ( isset( $row_to_edit ) ) {
                    if ( $row_to_edit->quantity == $bundle_array[$i]->quantity ) {
                           $html .= " selected";
                   }                
                }
               $html .= ">";
                (int)$bundle_array[$i]->quantity > 1 ? $html .= $bundle_array[$i]->quantity . ' sessions' : $html .= $bundle_array[$i]->quantity . ' session';
                $html .= "</option>";
            }
            $html .= "</select>";
            $html .= "</td>";
        } else {
            $html .= "<option value='1'>Project</option>";
            $html .= "<option value='1'>1 session</option>";
            $html .= "</select>";
        }

        // Session Date input[type="date"]
        
        for ( $i = 0; $i < count( $session_array ); $i++ ) {

            $html .=    "<tr id='order_edit_row_session_date_" . $session_array[$i]->id . "' class='order_edit_row_session_date'><th scope='row'>";
            $html .=    "<label for='order_edit_session_date_" . $session_array[$i]->id . "'>Date of session";
            $html .=    count( $session_array ) > 1 ? " " . ( $i + 1 ) : "";
            $html .=    "</label></th>";
            $html .=    "<td><input type='date' name='session_date_'" . $session_array[$i]->id . " id='order_edit_session_date_" . $session_array[$i]->id . "'";
            $html .=    " value='" . $session_array[$i]->date . "'>";
            $html .=    "</td></tr>";

            // Session Time select required
            $html .=    "<tr id='order_edit_row_session_time_" . $session_array[$i]->id . "' class='order_edit_row_session_time'><th scope='row'>";
            $html .=    "<label for='order_edit_session_time_" . $session_array[$i]->id . "'>Time of session";
            $html .=    count( $session_array ) > 1 ? " " . ( $i + 1 ) : "";
            $html .=    "</label></th>";
            $html .=    "<td><select name='session_time_" . $session_array[$i]->id . "' id='order_edit_session_time_" . $session_array[$i]->id . "' required>";    
            $schedule_array = book_a_session_get_table_array( "schedule", "start_time", "ASC" );
            if ( ! empty( $schedule_array ) ) {
                for ($j = 0; $j < count($schedule_array); $j++) {
                    $html .= "<option value='" . $schedule_array[$j]->id . "'";
                    if ( isset( $session_array ) ) {
                        if ( $session_array[$i]->schedule_id == $schedule_array[$j]->id ) {
                               $html .= " selected";
                       }                
                    }
                    $html .= ">";
                    $html .= book_a_session_get_session_time($schedule_array[$j]->id)["time_string"];
                    $html .= "</option>";
                }
                $html .= "</select>";
                $html .= "</td>";
            } else {
                $html .= "<option selected disabled>No session times found</option>";
                $html .= "</select><br><p class='description'>Add a Session time in the Schedule settings page</p></td>";
            }

        }

        // Location select required
        $html .=    "<tr id='order_edit_row_location_id'><th scope='row'>";
        $html .=    "<label for='location_id'>Location</label></th>";
        $html .=    "<td><select name='location_id' id='order_edit_location_id' required>";    
        $location_array = book_a_session_get_table_array( "location" );
        if ( ! empty( $location_array ) ) {
            for ($i = 0; $i < count($location_array); $i++) {
                $html .= "<option value='" . $location_array[$i]->id . "'";
                if ( isset( $row_to_edit ) ) {
                    if ( $row_to_edit->location_id == $location_array[$i]->id ) {
                           $html .= " selected";
                   }                
                }
                $html .= ">";
                $html .= $location_array[$i]->name;
                $html .= "</option>";
            }
            $html .= "</select>";
            $html .= "</td>";
        } else {
            $html .= "<option selected disabled>No locations found</option>";
            $html .= "</select><br><p class='description'>Add a location in the locations settings page</p></td>";
        }
        // Practitioner select
        $html .=    "<tr id='order_edit_row_practitioner_id'><th scope='row'>";
        $html .=    "<label for='practitioner_id'>Practitioner</label></th>";
        $html .=    "<td><select name='practitioner_id' id='order_edit_practitioner_id' required>";    
        $practitioner_array = book_a_session_get_practitioner_array();
        if ( ! empty( $practitioner_array ) ) {
            for ($i = 0; $i < count($practitioner_array); $i++) {
                $html .= "<option value='" . $practitioner_array[$i]->ID . "'";
                if ( isset( $row_to_edit ) ) {
                    if ( $row_to_edit->practitioner_id == $practitioner_array[$i]->ID ) {
                           $html .= " selected";
                   }                
                }
                $html .= ">";
                $html .= book_a_session_get_practitioner_name($practitioner_array[$i]->ID);
                $html .= "</option>";
            }
            $html .= "</select>";
            $html .= "</td>";
        } else {
            $html .= "<option selected disabled>No practitioners found</option>";
            $html .= "</select><br><p class='description'>Set your role, or another user's role to Admin Practitioner or Practitioner first.</p></td>";
        }
        // Client select required
        $html .=    "<tr id='order_edit_row_user_id'><th scope='row'>";
        $html .=    "<label for='user_id'>Client</label></th>";
        $html .=    "<td><select name='user_id' id='order_edit_user_id' required>";    
        $user_array = get_users();
        if ( ! empty( $user_array ) ) {    
            for ($i = 0; $i < count($user_array); $i++) {
                $html .= "<option value='" . $user_array[$i]->ID . "'";
                if ( isset( $row_to_edit ) ) {
                    if ( $row_to_edit->user_id == $user_array[$i]->ID ) {
                           $html .= " selected";
                   }                
                }
                $html .= ">";
                $html .= $user_array[$i]->first_name . " " . $user_array[$i]->last_name;
                $html .= "</option>";
            }
            $html .= "</select>";
            $html .= "</td>";
        } else {
            $html .= "<option selected disabled>No users found</option>";
            $html .= "</select><p class='description'>Please contact the plugin developer.</p></td>";
        }
        // Payment Method select required
        $html .=    "<tr id='order_edit_row_payment_method_id'><th scope='row'>";
        $html .=    "<label for='payment_method_id'>Payment Method</label></th>";
        $html .=    "<td><select name='payment_method_id' id='order_edit_payment_method_id' required>";    
        $payment_method_array = book_a_session_get_table_array( "payment_method" );
        if ( ! empty( $payment_method_array ) ) {
        $payment_method_option_flag = false;
        $payment_method_option_count = 0;
            for ($i = 0; $i < count($payment_method_array); $i++) {
                if ( ! empty ( get_option( 'book_a_session_options_paymentmethods' )[ "accept" . $payment_method_array[$i]->id ] ) ) {
                    $payment_method_option_flag = true;
                    $payment_method_option_count++;
                    $html .= "<option value='" . $payment_method_array[$i]->id . "'";
                    if ( isset( $row_to_edit ) ) {
                        if ( $row_to_edit->payment_method_id == $payment_method_array[$i]->id ) {
                               $html .= " selected";
                       }                
                    }
                    $html .= ">";
                    $html .= $payment_method_array[$i]->name;
                    $html .= "</option>";
                    $payment_method_option_flag = false;
                    } else {
                        $html .= "<option value='" . $payment_method_array[$i]->id . "' disabled>";
                        $html .= $payment_method_array[$i]->name;
                        $html .= "</option>";
                    }
            }
            $html .= "</select>";
        if ( $payment_method_option_count < 1 ) {
                $html .= "<p class='description'>Accept a payment method in the Book A Session settings page.</p>";
            }
            $html .= "</td>";
        } else {
            $html .= "<option selected disabled>No payment methods found</option>";
            $html .= "</select><p class='description'>Please contact the plugin developer.</p></td>";
        }
        // Total input[type="number"] step="any" min="0" optional
        $html .=    "<tr id='order_edit_row_total'><th scope='row'>";
        $html .=    "<label for='total'>Total</label></th>";
        $html .=    "<td><input type='number' step='any' min='0' name='total' id='order_edit_total' placeholder='0.00' style='min-width:300px;'";
        if ( isset( $row_to_edit->total ) ) {
            $html .= " value='" . $row_to_edit->total . "'>";
        } else {
            $html .= ">";
        }
        $html .=    "</td></tr>";
        // VFC Mobile input[type="text"] optional
        $html .=    "<tr id='order_edit_row_vfc_mobile'><th scope='row'>";
        $html .=    "<label for='vfc_mobile'>Vodafone Cash Mobile Number (Optional)</label>";
        $html .=    "</th><td><input type='text' name='vfc_mobile' id='order_edit_vfc_mobile' placeholder='Enter if Vodafone Cash payment' style='min-width:300px;'"; 
        if ( isset( $row_to_edit->vfc_mobile ) ) {
            $html .= " value='" . $row_to_edit->vfc_mobile . "'>";
        } else {
            $html .= ">";
        }
        $html .=    "</td></tr>";
        // Booking Status select required
        $html .=    "<tr id='order_edit_row_booking_status'><th scope='row'>";
        $html .=    "<label for='booking_status'>Booking Status</label></th>";
        $html .=    "<td><select name='booking_status' id='order_edit_booking_status' required>";    
        $booking_status_array = book_a_session_get_booking_status_array();
        if ( ! empty( $booking_status_array ) ) {
            for ($i = 0; $i < count($booking_status_array); $i++) {
                
                $html .= "<option value='" . str_replace( "'", "", $booking_status_array[$i] ). "'";
                if ( isset( $row_to_edit ) ) {
                    if ( $row_to_edit->booking_status == str_replace( "'", "", $booking_status_array[$i] ) ) {
                           $html .= " selected";
                   }                
                }
                $html .= ">";
                $html .= str_replace( "'", "", $booking_status_array[$i] );
                $html .= "</option>";        
            }
            $html .= "</select>";
            $html .= "</td>";
        } else {
            $html .= "<option selected disabled>No Booking Statuses found</option>";
            $html .= "</select><p class='description'>Please contact the plugin developer.</p></td>";
        }
        // Payment status select required
        $html .=    "<tr id='order_edit_row_payment_status'><th scope='row'>";
        $html .=    "<label for='payment_status'>Payment Status</label></th>";
        $html .=    "<td><select name='payment_status' id='order_edit_payment_status' required>";    
        $payment_status_array = book_a_session_get_payment_status_array();
        if ( ! empty( $payment_status_array ) ) {
            for ($i = 0; $i < count($payment_status_array); $i++) {
                
                $html .= "<option value='" . str_replace( "'", "", $payment_status_array[$i] ). "'";
                if ( isset( $row_to_edit ) ) {
                    if ( $row_to_edit->payment_status == str_replace( "'", "", $payment_status_array[$i] ) ) {
                           $html .= " selected";
                   }                
                }
                $html .= ">";
                $html .= str_replace( "'", "", $payment_status_array[$i] );
                $html .= "</option>";        
            }
            $html .= "</select>";
            $html .= "</td>";
        } else {
            $html .= "<option selected disabled>No payment statuses found</option>";
            $html .= "</select><p class='description'>Please contact the plugin developer.</p></td>";
        }
        // Amount Paid
        $html .=    "<tr id='order_edit_row_amount_paid'><th scope='row'>";
        $html .=    "<label for='amount_paid'>Amount paid</label></th>";
        $html .=    "<td><input type='number' step='any' min='0' name='amount_paid' id='order_edit_amound_paid' placeholder='0.00' style='min-width:300px;'";
        if ( isset( $row_to_edit->amount_paid ) ) {
            $html .= " value='" . $row_to_edit->amount_paid . "'>";
        } else {
            $html .= " value='0.00'>";
        }
        $html .=    "</td></tr>";
        // Order ID
        $html .=    "<tr id='order_edit_row_order_id'><th scope='row'>";
        $html .=    "<label for='order_id'>Order ID</label>";
        $html .=    "<td><input type='text'name='order_id' id='order_edit_order_id' placeholder='10 digits' maxlength='10' style='min-width:300px;'";
        if ( isset( $row_to_edit->order_id ) ) {
            $html .= " value='" . $row_to_edit->order_id . "'>";
        } else {
            $html .= " value=''>";
        }
        $html .=    "</td></tr>";
        // Note
        $html .=    "<tr id='order_edit_row_note'><th scope='row'>";
        $html .=    "<label for='note'>Note</label></th>";
        $html .=    "<td><textarea name='note' id='order_edit_note' placeholder='Additional information' style='min-width:300px;'>";
        if ( isset( $row_to_edit->note ) ) {
            $html .= esc_textarea( stripslashes( $row_to_edit->note ) );
        }
        $html .=    "</textarea></td></tr>";
        $html .=    "</tbody></table>";
        $html .=    "<input type='submit' class='button button-primary' value='Save changes' name='submit_edit_order'>";
        $html .=    "</form>";

        
        echo $html;

    } else {

        echo '<h3>Select an order to edit</h3>';
        echo '<form>';
        $orders_list = new Orders_List_Table;
        echo '</form>';

    }

} else {

	echo '<h3>View orders</h3>';
	echo '<form>';
	$orders_list = new Orders_List_Table;
	echo '</form>';

}
// Code after the tabs (outside)
		?>


	</div>

	<?php

}