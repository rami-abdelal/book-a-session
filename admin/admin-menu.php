<?php // Book A Session - Admin Menu

if ( ! defined( 'ABSPATH' ) ) exit;

// Add top-level administrative menu

function book_a_session_add_toplevel_menu() {
	
	/* 
		add_menu_page(
			string   $page_title, 
			string   $menu_title, 
			string   $capability, 
			string   $menu_slug, 
			callable $function = '', 
			string   $icon_url = '', 
			int      $position = null 
		)
	*/
	
	add_menu_page(
		'Book A Session Settings',
		'Book A Session',
		'manage_options',
		'book_a_session',
		'book_a_session_display_general_settings_page',
		'dashicons-admin-generic',
		null
	);
    
	
}

function book_a_session_add_submenus() {
    
    // add sub-level administrative menu
	
	/*
	
	add_submenu_page(
		string   $parent_slug,
		string   $page_title,
		string   $menu_title,
		string   $capability,
		string   $menu_slug,
		callable $function = ''
	);
	
	*/

	add_submenu_page(
		'book_a_session',
		'Schedule - Book A Session',
		'Schedule',
		'manage_options',
		'book_a_session_schedule',
		'book_a_session_display_schedule_settings_page'
	);

	add_submenu_page(
		'book_a_session',
		'Currencies - Book A Session',
		'Currencies',
		'manage_options',
		'book_a_session_currencies',
		'book_a_session_display_currencies_settings_page'
	);

	add_submenu_page(
		'book_a_session',
		'Regions - Book A Session',
		'Regions',
		'manage_options',
		'book_a_session_regions',
		'book_a_session_display_regions_settings_page'
	);
	
	add_submenu_page(
		'book_a_session',
		'Locations - Book A Session',
		'Locations',
		'manage_options',
		'book_a_session_locations',
		'book_a_session_display_locations_settings_page'
	);

	add_submenu_page(
		'book_a_session',
		'Service Types - Book A Session',
		'Service Types',
		'manage_options',
		'book_a_session_service_types',
		'book_a_session_display_service_types_settings_page'
	);

	add_submenu_page(
		'book_a_session',
		'Services - Book A Session',
		'Services',
		'manage_options',
		'book_a_session_services',
		'book_a_session_display_services_settings_page'
	);

	add_submenu_page(
		'book_a_session',
		'Bundles - Book A Session',
		'Bundles',
		'manage_options',
		'book_a_session_bundles',
		'book_a_session_display_bundles_settings_page'
	);

	add_submenu_page(
		'book_a_session',
		'Orders - Book A Session',
		'Orders',
		'manage_options',
		'book_a_session_orders',
		'book_a_session_display_orders_settings_page'
	);

	add_submenu_page(
		'book_a_session',
		'Invoices - Book A Session',
		'Invoices',
		'manage_options',
		'book_a_session_invoices',
		'book_a_session_display_invoices_settings_page'
	);

	add_submenu_page(
		'book_a_session',
		'Payments - Book A Session',
		'Payments',
		'manage_options',
		'book_a_session_payments',
		'book_a_session_display_payments_settings_page'
	);

}


	




add_action( 'admin_menu', 'book_a_session_add_toplevel_menu' );
add_action( 'admin_menu', 'book_a_session_add_submenus' );
