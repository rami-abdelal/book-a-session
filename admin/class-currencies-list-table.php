<?php

if (!class_exists('WP_List_Table')) {
    require_once (ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
 
}

class Currencies_List_Table extends WP_List_Table  {


    public function __construct()
    {
        parent::__construct(array(
            'singular' => 'currency',
            'plural' => 'currencies',
            'ajax' => true
        ));

        $this->prepare_items();
        $this->display();
        
    }

    public function prepare_items()
    {
    $this->_column_headers = $this->get_column_info();
    $columns = $this->get_columns();
    $hidden = $this->get_hidden_columns();
    $sortable = $this->get_sortable_columns();
    $this->_column_headers = array(
        $columns,
        $hidden,
        $sortable
    );
    
    $this->process_bulk_action();
    $per_page = $this->get_items_per_page('records_per_page', 10);
    $current_page = $this->get_pagenum();
    $total_items = self::record_count();
    $data = self::get_records($per_page, $current_page);
    $this->set_pagination_args(
                      ['total_items' => $total_items, 
                   'per_page' => $per_page 
                  ]);
    $this->items = $data;
    }

    /** * 
    *Retrieve records data from the database
    * * @param int $per_page
    * @param int $page_number
    * * @return mixed
    */
    public static function get_records($per_page = 10, $page_number = 1)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'book_a_session_currency';

        $sql = "SELECT
                *
                FROM " .  
                $table_name;

        /*if (isset($_REQUEST['s'])) {
        $sql.= ' where column1 LIKE "%' . $_REQUEST['s'] . '%" or column2 LIKE "%' . $_REQUEST['s'] . '%"';
        }*/
         
        if (!empty($_REQUEST['orderby'])) {
                $sql.= ' ORDER BY ' . esc_sql($_REQUEST['orderby']);
            $sql.= !empty($_REQUEST['order']) ? ' ' . esc_sql($_REQUEST['order']) : ' ASC';
        }
        $sql.= " LIMIT $per_page";
        $sql.= ' OFFSET ' . ($page_number - 1) * $per_page;
        $result = $wpdb->get_results($sql, 'ARRAY_A');
        return $result;
    }

    function get_columns()
        {
            $columns = [
                'currency_code'                     =>__('Code'),
                'currency_symbol_after'             =>__('Label after amount?'),
                'currency_use_alternative_symbol'   =>__('Use symbol as label?'),
                'currency_symbol'                   =>__('Symbol'),
                'currency_example'                  =>__('Example'),
                'actions'                           =>__('Actions')
                  ];
            return $columns;
        }       

    public function get_hidden_columns()
    {
        // Setup Hidden columns and return them
        return array(
            'currency_id'
        );
    }

    /** 
    * Columns to make sortable. 
    * * @return array 
    */
    public function get_sortable_columns()
    {
        $sortable_columns = array(
            'currency_code'=>array('code', true)
          );
        return $sortable_columns;
    }

    /** 
    *Text displayed when no record data is available 
    */
    public function no_items()
    {
        _e('No currencies found.', 'bx');
    }

    /** 
    * Returns the count of records in the database. 
    * * @return null|string 
    */
    public static function record_count()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'book_a_session_currency';

        $sql = "SELECT
                COUNT(*) 
                FROM " . 
                $table_name;
        return $wpdb->get_var($sql);
    }

        public function column_default($item, $column_name) {

            $nonce_delete = "&_wpnonce=" . wp_create_nonce( 'book_a_session_delete_currency_' . $item["id"] );
            $nonce_edit = "&_wpnonce=" . wp_create_nonce( 'book_a_session_edit_currency_' . $item["id"] );            
            $admin_edit_page_url = 'admin.php?page=book_a_session_currencies&tab=edit'; 

            switch ($column_name) {

                    case "currency_code"                        : echo $item["code"]; break;
                    case "currency_symbol_after"                : echo $item["symbol_after"] ? "&#10003;" : "<span class='book-a-session-admin-declined'>&mdash;</span>"; break;
                    case "currency_use_alternative_symbol"      : echo $item["use_alternative_symbol"] ? "&#10003;" : "<span class='book-a-session-admin-declined'>&mdash;</span>"; break;
                    case "currency_symbol"                      : echo $item["symbol"] ? $item["symbol"] : "<span class='book-a-session-admin-declined'>&mdash;</span>"; break;
                    case "currency_example"                     : echo book_a_session_get_price_tag( 299.99, $item["id"] ); break;
                    case "actions" : echo   "<a href='" . $admin_edit_page_url . $nonce_edit . "&edit=1&currency_id=" . $item["id"] . "'>Edit</a>" . 
                                            "&nbsp;&nbsp;&nbsp;" . 
                                            "<a class='book-a-session-row-delete' href='" . $admin_edit_page_url . $nonce_delete . "&delete=1&currency_id=" . $item["id"] . "'>Delete</a>"; 
                    break;
                    return $item[ $column_name ];
                    default:
                        return $item[ $column_name ] ;
            
                }

        }

}