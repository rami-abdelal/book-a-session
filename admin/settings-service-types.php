<?php // Book A Session - Service Types Settings Page

if ( ! defined( 'ABSPATH' ) ) exit;

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
 }

if( ! class_exists( 'Service_Types_List_Table' ) ) {
    require_once plugin_dir_path( __FILE__ ) . 'class-service-types-list-table.php';
}

// Edit

if (   ! empty( $_GET['edit'] ) 
    && ! empty( $_GET['service_type_id'] )
    && ! empty( $_POST['submit_edit_service_type'] ) ) {

    if ( wp_verify_nonce( $_REQUEST['_wpnonce'], 'book_a_session_edit_service_type_' . $_GET['service_type_id'] )
    &&   current_user_can( "manage_options" ) ) {

        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_service_type";

        // Validation

        if (    ! empty( $_POST['name'] )          
            &&  ! empty( $_POST['slug'] )       
            &&  ! empty( $_POST['practitioner_title'] )          
            &&  ! empty( $_POST['practitioner_slug'] ) 
            ) {
            

            $value_array = array( 
                'name'              	=> $_POST['name'],
                'slug'       			=> $_POST['slug'],
                'practitioner_title'	=> $_POST['practitioner_title'], 
                'practitioner_slug'   	=> $_POST['practitioner_slug']
            );
            $edit_service_type_db_result = $wpdb->update( 
                $table_name, 
                $value_array, 
                array( 'id' => $_GET['service_type_id'] ),
                array( 
                    '%s', 
                    '%s',
                    '%s',
                    '%s'
                    ) 
                );
            
            if ( $edit_service_type_db_result ) {
                add_action( 'admin_notices', 'book_a_session_admin_notice_edit_success' );
            } else {
                add_action( 'admin_notices', 'book_a_session_admin_notice_edit_error' );
            }   

        } else {
            add_action( 'admin_notices', 'book_a_session_admin_notice_validation_error' );
        }

    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_edit_security_error' );
    }

}

// Delete
if (    isset( $_GET['delete'] ) 
    &&  isset( $_GET['service_type_id'] ) 
    &&  isset( $_GET['page'] )  
    ) {

    if ( wp_verify_nonce( $_REQUEST['_wpnonce'], 'book_a_session_delete_service_type_' . $_GET['service_type_id'] ) &&  current_user_can( 'manage_options' ) ) {

        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_service_type";
        $delete_service_type_db_result = $wpdb->delete( $table_name, array( 'id' => $_GET['service_type_id'] ), array( '%d' ) );
        $delete_service_type_db_result ? add_action( 'admin_notices', 'book_a_session_admin_notice_delete_success' ) : add_action( 'admin_notices', 'book_a_session_admin_notice_delete_error' );

    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_delete_security_error' );
    }

}

// Add
if ( isset( $_POST['submit_add_service_type'] ) ) {

    if ( current_user_can( "manage_options" ) ) {

        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_service_type";
    
        // Validation
    
        if ( ! empty( $_POST['name'] )              	&&
             ! empty( $_POST['slug'] )       			&&    
             ! empty( $_POST['practitioner_title'] )	&& 
             ! empty( $_POST['practitioner_slug'] )   
            ) {
            $value_array = array( 
                'name'              	=> $_POST['name'],
                'slug'       			=> $_POST['slug'],
                'practitioner_title'	=> $_POST['practitioner_title'], 
                'practitioner_slug'   	=> $_POST['practitioner_slug']
            );
            $add_service_type_db_result = $wpdb->insert( 
                $table_name, 
                $value_array,
                array( 
                    '%s', 
                    '%s',
                    '%s',
                    '%s'
                    ) 
                );
            
            if ( $add_service_type_db_result ) {
                add_action( 'admin_notices', 'book_a_session_admin_notice_add_success' );
            } else {
                add_action( 'admin_notices', 'book_a_session_admin_notice_add_error' );
            }   
    
        } else {
                add_action( 'admin_notices', 'book_a_session_admin_notice_validation_error' );
        }

    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_add_security_error' );
    }

}


function book_a_session_display_service_types_settings_page() {
    
	if ( ! current_user_can( 'manage_options' ) ) return;

    ?>
	<div class="wrap">
		<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
		<?php 

function page_tabs_service_type( $current = 'View' ) {
    $tabs = array(
        'view'   => __( 'View service types', 'book_a_session' ), 
		'add'  => __( 'Add service type', 'book_a_session' ),
		'edit'  => __( 'Edit service types', 'book_a_session' )
    );
    $html = '<h2 class="nav-tab-wrapper">';
    foreach( $tabs as $tab => $name ){
        $class = ( $tab == $current ) ? 'nav-tab-active' : '';
        $html .= '<a class="nav-tab ' . $class . '" href="?page=book_a_session_service_types&tab=' . $tab . '">' . $name . '</a>';
    }
    $html .= '</h2>';
    echo $html;
}

// Tabs
$tab = ( ! empty( $_GET['tab'] ) ) ? esc_attr( $_GET['tab'] ) : 'view';
page_tabs_service_type( $tab );

// View service types
if ( $tab == 'view' ) {

	echo '<h3>View service types</h3>';
	echo '<form>';
	$service_types_list = new Service_Types_List_Table;
	echo '</form>';

}

// Add
elseif ( $tab == 'add' ) {


	$html =     "<table class='form-table'><form method='POST'>";
    $html .=    "<tr><th colspan='2'><h3>Add a new service type</h3>";
    $html .=    "<p class='description'>What kind of services are you providing? Web Development? Accounting? Teaching?</p></th></tr>";
    
    // Name input[type="text"] required
    $html .=    "<tr><th scope='row'>";
	$html .=    "<label for='name'>Name</label></th>";
    $html .=    "<td><input type='text' name='name' id='service_type_add_name' placeholder='Name your service type' style='min-width:300px;'";
    if ( isset( $_POST['name'] ) ) {
        $html .= " value='" . $_POST['name'] . "'>";
    } else {
        $html .= ">";
    }
    if ( isset( $_POST['submit_add_service_type'] ) ) {
        if ( empty( $_POST['name'] ) ) {
          $html .= "<p class='description book-a-session-error'>Please enter the service type name.</p>";
        }
    } 
    $html .=    "</td></tr>";
    // Slug input[type="text"] required
    $html .=    "<tr><th scope='row'>";
	$html .=    "<label for='slug'>Slug</label></th>";
    $html .=    "<td><input type='text' name='slug' id='service_type_add_slug' placeholder='Enter its slug' style='min-width:300px;'";
    if ( isset( $_POST['slug'] ) ) {
        $html .= " value='" . $_POST['slug'] . "'>";
    } else {
        $html .= ">";
	}
	$html .= 	"<p class='description'>The “slug” is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens.</p>";
    if ( isset( $_POST['submit_add_service_type'] ) ) {
        if ( empty( $_POST['slug'] ) ) {
          $html .= "<p class='description book-a-session-error'>Please enter the service type slug.</p>";
        }
    } 
    $html .=    "</td></tr>";
    // Practitioner Title input[type="text"] required
    $html .=    "<tr><th scope='row'>";
	$html .=    "<label for='practitioner_title'>Practitioner Title</label></th>";
    $html .=    "<td><input type='text' name='practitioner_title' id='service_type_add_practitioner_title' placeholder='Enter the title here' style='min-width:300px;'";
    if ( isset( $_POST['practitioner_title'] ) ) {
        $html .= " value='" . $_POST['practitioner_title'] . "'>";
    } else {
        $html .= ">";
	}
	$html .= 	"<p class='description'>If this service type is Computer Repair, a suitable title may be Technician, which will be replace Practitioner on your website for all services belonging to this service type.</p>";
    if ( isset( $_POST['submit_add_service_type'] ) ) {
        if ( empty( $_POST['practitioner_title'] ) ) {
          $html .= "<p class='description book-a-session-error'>Please enter the practitioner title.</p>";
        }
    } 
    $html .=    "</td></tr>";
    // Practitioner Slug input[type="text"] required
    $html .=    "<tr><th scope='row'>";
	$html .=    "<label for='practitioner_slug'>Practitioner Slug</label></th>";
    $html .=    "<td><input type='text' name='practitioner_slug' id='service_type_add_practitioner_slug' placeholder='Enter its slug' style='min-width:300px;'";
    if ( isset( $_POST['practitioner_slug'] ) ) {
        $html .= " value='" . $_POST['practitioner_slug'] . "'>";
    } else {
        $html .= ">";
	}
	$html .= 	"<p class='description'>A practitioner “slug” is the URL-friendly version of the practitioner title. It is usually all lowercase and contains only letters, numbers, and hyphens.</p>";
    if ( isset( $_POST['submit_add_service_type'] ) ) {
        if ( empty( $_POST['practitioner_slug'] ) ) {
          $html .= "<p class='description book-a-session-error'>Please enter the practitioner slug.</p>";
        }
    } 
    $html .=    "</td></tr>";


	$html .=    "</tbody></table>";
    $html .=    "<input type='submit' class='button button-primary' value='Add service type' name='submit_add_service_type'>";
    $html .=    "</form>";
	
	
	echo $html;



} elseif ( $tab == 'edit' ) {

    if ( isset( $_GET["edit"] ) 
    && ! empty( $_GET['service_type_id'] ) ) {

        echo '<h3>Edit a service type</h3>';
        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_service_type";
        $id = intval( $_GET['service_type_id'] );

        $row_to_edit = $wpdb->get_row("
        SELECT  *
        FROM    $table_name
        WHERE   id = $id", OBJECT );

        $html =     "<table class='form-table'><form method='POST'>";
	
		// Name input[type="text"] required
		$html .=    "<tr><th scope='row'>";
		$html .=    "<label for='name'>Name</label></th>";
		$html .=    "<td><input type='text' name='name' id='service_type_edit_name' placeholder='Name your service type' style='min-width:300px;'";
        if ( isset( $row_to_edit->name ) ) {
            $html .= " value='" . $row_to_edit->name . "'";
        } 
		$html .=    "></td></tr>";
		// Slug input[type="text"] required
		$html .=    "<tr><th scope='row'>";
		$html .=    "<label for='slug'>Slug</label></th>";
		$html .=    "<td><input type='text' name='slug' id='service_type_edit_slug' placeholder='Enter its slug' style='min-width:300px;'";
        if ( isset( $row_to_edit->slug ) ) {
            $html .= " value='" . $row_to_edit->slug . "'";
        } 
		$html .= 	"><p class='description'>The “slug” is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens.</p>";
		$html .=    "</td></tr>";
		// Practitioner Title input[type="text"] required
		$html .=    "<tr><th scope='row'>";
		$html .=    "<label for='practitioner_title'>Practitioner Title</label></th>";
		$html .=    "<td><input type='text' name='practitioner_title' id='service_type_edit_practitioner_title' placeholder='Enter the title here' style='min-width:300px;'";
        if ( isset( $row_to_edit->practitioner_title ) ) {
            $html .= " value='" . $row_to_edit->practitioner_title . "'";
        } 
		$html .= 	"><p class='description'>If this service type is Computer Repair, a suitable title may be Technician, which will be replace Practitioner on your website for all services belonging to this service type.</p>";
		$html .=    "</td></tr>";    

		// Practitioner Slug input[type="text"] required
		$html .=    "<tr><th scope='row'>";
		$html .=    "<label for='practitioner_slug'>Practitioner Slug</label></th>";
		$html .=    "<td><input type='text' name='practitioner_slug' id='service_type_edit_practitioner_slug' placeholder='Enter its slug' style='min-width:300px;'";
        if ( isset( $row_to_edit->practitioner_slug ) ) {
            $html .= " value='" . $row_to_edit->practitioner_slug . "'";
        } 
		$html .= 	"><p class='description'>A practitioner “slug” is the URL-friendly version of the practitioner title. It is usually all lowercase and contains only letters, numbers, and hyphens.</p>";
		$html .=    "</td></tr>";
		$html .=    "</tbody></table>";
        $html .=    "<input type='submit' class='button button-primary' value='Save changes' name='submit_edit_service_type'>";
        $html .=    "</form>";

        
        
        
        echo $html;

    } else {

        echo '<h3>Edit a service type</h3>';
        echo '<form>';
        $service_types_list = new Service_Types_List_Table;
        echo '</form>';
    
    }

} else {

	echo '<h3>View service types</h3>';
	echo '<form>';
	$service_types_list = new Service_Types_List_Table;
	echo '</form>';

}
// Code after the tabs (outside)
		?>


	</div>

	<?php

}



