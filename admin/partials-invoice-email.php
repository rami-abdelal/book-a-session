<?php 

/**
 *  Book A Session Partial:  
 *  HTML Invoice Email Template
 */

 $body  = "<div style='width:            100%;
                       height:           100%;
                       background-color: #ececec;
                       padding:          25px;'>";
 $body .= "<div style='width:            650px;
                       margin:           0 auto;
                       position:         relative;
                       background-color: #ffffff;
                       padding:          25px;
                       border-radius:    4px;
                       box-shadow:       0px 2px rgba(0,0,0,.2);'>";
 $body .= "<h1>" . $invoice_header_text . "</h1>";
 $body .= "<h4>" . $quantity;
 $body .= $quantity == 1 ? " Session of " : " Sessions of ";
 $body .= $service_name . " - " . $location_name . ".</h4>";
 $body .= "<table style='
                       width:            100%;
                       border:           none;
                       outline:          none;'>";
 $body .= "<tbody>";
 $body .= "<tr>";
 $body .= "<th scope='col' style='width: 80px;'>Date</th>";
 $body .= "<th scope='col' style='width: 80px;'>Time</th>";
 $body .= "<th scope='col'>Price</th>";
 $body .= "</tr>";
 foreach ( $session_date_and_time as $invoice_datetime ) {
    $body .= "<tr>";
    $body .= "<td>" . $invoice_datetime["date"]        . "</td>";
    $body .= "<td>" . $invoice_datetime["time_string"] . "</td>";
    $body .= "<td>" . book_a_session_get_price_tag( $invoice->base_price, $invoice->currency_id ) . "</td>";
    $body .= "</tr>"; 
}
 $body .= "</tbody>";
 $body .= "</table>";
 $body .= "</div>";
 $body .= "</div>";
 $body .= "";
 $body .= "";
 $body .= "";
 $body .= "";
 $body .= "";
 $body .= "";
 $body .= "";
 $body .= "";
 $body .= "";
 $body .= "";
 $body .= "";
 $body .= "";
 $body .= "";
 $body .= "";
 $body .= "";
 $body .= "";

