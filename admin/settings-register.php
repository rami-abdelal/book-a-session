<?php // Book A Session - Register Settings

if ( ! defined( 'ABSPATH' ) ) exit;

// Register settings

function book_a_session_register_settings() {
	
	/*
	
	register_setting( 
		string   $option_group, 
		string   $option_name, 
		callable $sanitize_callback
	);
	
	*/

	/**
	 * Register a setting for each tab in the general settings page.
	 * This means each tab page will have one database entry that all fields within it will share.
	 * All Book A Session options start with book_a_session_options_. The last word identifies the group.
	 * Even if the final string of characters delimited by underscores make up two words, there should be no spaces
	 * For example: book_a_session_paymentmethods.
	 * "paymentmethods" must be written in the same way when registering fields below.
	 * To retrieve an option called logo_url in the General tab, you'd want to write this:
	 * get_option( 'book_a_session_options_general' )[ "logo_url" ];
	 * 
	 */ 
	
	register_setting( 
		'book_a_session_options_general', 
		'book_a_session_options_general', 
		'book_a_session_callback_validate_options' 
	);
	
	register_setting( 
		'book_a_session_options_practitioners', 
		'book_a_session_options_practitioners', 
		'book_a_session_callback_validate_options' 
	);
	
	register_setting( 
		'book_a_session_options_schedule', 
		'book_a_session_options_schedule', 
		'book_a_session_callback_validate_options' 
	);

	register_setting( 
		'book_a_session_options_currencies', 
		'book_a_session_options_currencies', 
		'book_a_session_callback_validate_options' 
	);
	
	register_setting( 
		'book_a_session_options_regions', 
		'book_a_session_options_regions', 
		'book_a_session_callback_validate_options' 
	);
	
	register_setting( 
		'book_a_session_options_paymentmethods', 
		'book_a_session_options_paymentmethods', 
		'book_a_session_callback_validate_options' 
	);
	
	register_setting( 
		'book_a_session_options_locations', 
		'book_a_session_options_locations', 
		'book_a_session_callback_validate_options' 
	);
	
	register_setting( 
		'book_a_session_options_servicetypes', 
		'book_a_session_options_servicetypes', 
		'book_a_session_callback_validate_options' 
	);
	
	register_setting( 
		'book_a_session_options_services', 
		'book_a_session_options_services', 
		'book_a_session_callback_validate_options' 
	);
	
	register_setting( 
		'book_a_session_options_bundles', 
		'book_a_session_options_bundles', 
		'book_a_session_callback_validate_options' 
	);
	
	register_setting( 
		'book_a_session_options_orders', 
		'book_a_session_options_orders', 
		'book_a_session_callback_validate_options' 
	);
	
	register_setting( 
		'book_a_session_options_timetables', 
		'book_a_session_options_timetables', 
		'book_a_session_callback_validate_options' 
	);
	
	register_setting( 
		'book_a_session_options_emails', 
		'book_a_session_options_emails', 
		'book_a_session_callback_validate_options' 
	);
	
	register_setting( 
		'book_a_session_options_invoices', 
		'book_a_session_options_invoices', 
		'book_a_session_callback_validate_options' 
	);
	
	register_setting( 
		'book_a_session_options_payments', 
		'book_a_session_options_payments', 
		'book_a_session_callback_validate_options' 
	);	
	
	/*
	
	add_settings_section( 
		string   $id, 
		string   $title, 
		callable $callback, 
		string   $page
	);
		*/
	
	add_settings_section( 
		'book_a_session_section_general_settings', 
		'General Settings', 
		'book_a_session_callback_section_general_settings', 
		'book_a_session_general'
	);

	add_settings_section( 
		'book_a_session_section_bundle_settings', 
		'Bundle Settings', 
		'book_a_session_callback_section_bundle_settings', 
		'book_a_session_bundles'
	);

	add_settings_section( 
		'book_a_session_section_currency_settings', 
		'Currency Settings', 
		'book_a_session_callback_section_currency_settings', 
		'book_a_session_currencies'
	);

	add_settings_section( 
		'book_a_session_section_payment_method_settings', 
		'Payment Method Settings', 
		'book_a_session_callback_section_payment_method_settings', 
		'book_a_session_payment_methods'
	);	

	add_settings_section( 
		'book_a_session_section_location_settings', 
		'Location Settings', 
		'book_a_session_callback_section_location_settings', 
		'book_a_session_locations'
	);

	add_settings_section( 
		'book_a_session_section_practitioner_settings', 
		'Practitioner Settings', 
		'book_a_session_callback_section_practitioner_settings', 
		'book_a_session_practitioners'
	);

	add_settings_section( 
		'book_a_session_section_schedule_settings', 
		'Schedule Settings', 
		'book_a_session_callback_section_schedule_settings', 
		'book_a_session_schedule'
	);

	add_settings_section( 
		'book_a_session_section_timetable_settings', 
		'Timetable Settings', 
		'book_a_session_callback_section_timetable_settings', 
		'book_a_session_timetable'
	);

	add_settings_section( 
		'book_a_session_section_invoice_settings', 
		'Invoice Settings', 
		'book_a_session_callback_section_invoice_settings', 
		'book_a_session_invoices'
	);

	add_settings_section( 
		'book_a_session_section_invoice_online_settings', 
		'Online Payment Invoice Settings', 
		'book_a_session_callback_section_invoice_online_settings', 
		'book_a_session_invoices'
	);

	add_settings_section( 
		'book_a_session_section_invoice_online_due_settings', 
		'Online Payment Due Invoices', 
		'book_a_session_callback_section_invoice_online_due_settings', 
		'book_a_session_invoices'
	);

	add_settings_section( 
		'book_a_session_section_invoice_online_partly_paid_settings', 
		'Online Payment Partly Paid Invoices', 
		'book_a_session_callback_section_invoice_online_partly_paid_settings', 
		'book_a_session_invoices'
	);

	add_settings_section( 
		'book_a_session_section_invoice_online_paid_settings', 
		'Online Payment Paid Invoices', 
		'book_a_session_callback_section_invoice_online_paid_settings', 
		'book_a_session_invoices'
	);

	add_settings_section( 
		'book_a_session_section_invoice_cash_settings', 
		'Cash Invoice Settings', 
		'book_a_session_callback_section_invoice_cash_settings', 
		'book_a_session_invoices'
	);

	add_settings_section( 
		'book_a_session_section_invoice_cash_due_settings', 
		'Cash Due Invoices', 
		'book_a_session_callback_section_invoice_cash_due_settings', 
		'book_a_session_invoices'
	);

	add_settings_section( 
		'book_a_session_section_invoice_cash_partly_paid_settings', 
		'Cash Partly Paid Invoices', 
		'book_a_session_callback_section_invoice_cash_partly_paid_settings', 
		'book_a_session_invoices'
	);

	add_settings_section( 
		'book_a_session_section_invoice_cash_paid_settings', 
		'Cash Paid Invoices', 
		'book_a_session_callback_section_invoice_cash_paid_settings', 
		'book_a_session_invoices'
	);

	add_settings_section( 
		'book_a_session_section_invoice_vfc_settings', 
		'Vodafone Cash Invoice Settings', 
		'book_a_session_callback_section_invoice_vfc_settings', 
		'book_a_session_invoices'
	);

	add_settings_section( 
		'book_a_session_section_invoice_vfc_due_settings', 
		'Vodafone Cash Due Invoices', 
		'book_a_session_callback_section_invoice_vfc_due_settings', 
		'book_a_session_invoices'
	);

	add_settings_section( 
		'book_a_session_section_invoice_vfc_partly_paid_settings', 
		'Vodafone Cash Partly Paid Invoices', 
		'book_a_session_callback_section_invoice_vfc_partly_paid_settings', 
		'book_a_session_invoices'
	);

	add_settings_section( 
		'book_a_session_section_invoice_vfc_paid_settings', 
		'Vodafone Cash Paid Invoices', 
		'book_a_session_callback_section_invoice_vfc_paid_settings', 
		'book_a_session_invoices'
	);
	
	add_settings_section( 
		'book_a_session_section_email_settings', 
		'Email Settings', 
		'book_a_session_callback_section_email_settings', 
		'book_a_session_emails'
	);

	add_settings_section( 
		'book_a_session_section_email_phpmailer_settings', 
		'Advanced Email Settings', 
		'book_a_session_callback_section_email_phpmailer_settings', 
		'book_a_session_emails'
	);

	add_settings_section( 
		'book_a_session_section_payment_settings', 
		'Payment Settings', 
		'book_a_session_callback_section_payment_settings', 
		'book_a_session_payments'
	);

	add_settings_section( 
		'book_a_session_section_order_settings', 
		'Order Settings', 
		'book_a_session_callback_section_order_settings', 
		'book_a_session_orders'
	);

	add_settings_section( 
		'book_a_session_section_service_settings', 
		'Service Settings', 
		'book_a_session_callback_section_service_settings', 
		'book_a_session_services'
	);

	add_settings_section( 
		'book_a_session_section_service_type_settings', 
		'Service Type Settings', 
		'book_a_session_callback_section_service_type_settings', 
		'book_a_session_service_types'
	);

	add_settings_section( 
		'book_a_session_section_region_settings', 
		'Region Settings', 
		'book_a_session_callback_section_region_settings', 
		'book_a_session_regions'
	);
    
	/*
	add_settings_field(
    	string   $id,
		string   $title,
		callable $callback,
		string   $page,
		string   $section = 'default',
		array    $args = []
	);


	/**
	 * IDs should follow this pattern: bookasession_optiongroup_option_name_can_have_multiple_underscores
	 * Optiongroup refers to the last word of option groups entered for settings registered with register_setting() above. It must be one word.
	 * IDs will be exploded by "_", creating an array of varying lengths.
	 * The first item will always be bookasession. This is discarded.
	 * The second will be the final word of the option group as mentioned above.
	 * This is used when building form HTML in callback functions in settings-callbacks.php, used to identify which tab the field belongs to and which table row WP should store the option in.
	 * That is saved separately and discarded from the array. If there's more than one item left in the array, they are imploded back together with underscores to be used as the option name.
	 * Otherwise if there's just one item in the array (if you haven't used an underscore after optiongroup), that is used as the option name.
	 *  
	 */
	
	// General

	add_settings_field(
		'bookasession_general_image_logo',
		'Logo',
		'book_a_session_callback_image_select',
		'book_a_session_general',
		'book_a_session_section_general_settings',[ 
			'id'  			=>  'image_logo', 
			'group' 		=>	'general',
			'option'		=> 	true,
			'label' 		=> 	'We recommend using a .png logo with a transparent background for best results.',
							]
	);

	add_settings_field(
		'bookasession_general_image_logo_square',
		'Logo (Square)',
		'book_a_session_callback_image_select',
		'book_a_session_general',
		'book_a_session_section_general_settings',[ 
			'id'  			=>  'image_logo_square', 
			'group' 		=>	'general',
			'option'		=> 	true,
			'label' 		=> 	'The recommended minimum size is 128x128px. Supported image types are: .gif, .jpeg and .png',
							]
	);

	$frontend_page_selected = isset( get_option( "book_a_session_options_general" )["frontend_page_id"] ) ? intval( get_option( "book_a_session_options_general" )["frontend_page_id"] ) : 0;

	add_settings_field(
		'bookasession_general_frontend_page_id',
		'Frontend Form Page',
		'book_a_session_callback_dropdown_pages',
		'book_a_session_general',
		'book_a_session_section_general_settings',[ 
			"group" => "general",
			"id"	=> "frontend_page_id",
			"selected" => $frontend_page_selected,
			"label" => "Select the page your services and order form will appear on. If you haven't created the page yet, you'll need to do so, select it here, hit save at the bottom of this page, and add the following shortcode to the page content: <code>[book-a-session frontend]</code>."
			]
	);

	$dashboard_page_selected = isset( get_option( "book_a_session_options_general" )["dashboard_page_id"] ) ? intval( get_option( "book_a_session_options_general" )["dashboard_page_id"] ) : 0;

	add_settings_field(
		'bookasession_general_dashboard_page_id',
		'Dashboard Page',
		'book_a_session_callback_dropdown_pages',
		'book_a_session_general',
		'book_a_session_section_general_settings',[ 
			"group" => "general",
			"id"	=> "dashboard_page_id",
			"selected" => $dashboard_page_selected,
			"label" => "Select the page your Dashboard will appear on. If you haven't created the page yet, you'll need to do so, select it here, hit save at the bottom of this page, and add the following shortcode to the page content: <code>[book-a-session dashboard]</code>."
			]
	);

	$services_page_selected = isset( get_option( "book_a_session_options_general" )["services_page_id"] ) ? intval( get_option( "book_a_session_options_general" )["services_page_id"] ) : 0;

	add_settings_field(
		'bookasession_general_services_page_id',
		'Services Page',
		'book_a_session_callback_dropdown_pages',
		'book_a_session_general',
		'book_a_session_section_general_settings',[ 
			"group" => "general",
			"id"	=> "services_page_id",
			"selected" => $services_page_selected,
			"label" => "Select the page your services will appear on. If you haven't created the page yet, you'll need to do so, select it here, hit save at the bottom of this page, and add the following shortcode to the page content: <code>[book-a-session services]</code>."
			]
	);

	$login_page_selected = isset( get_option( "book_a_session_options_general" )["login_page_id"] ) ? intval( get_option( "book_a_session_options_general" )["login_page_id"] ) : 0;

	add_settings_field(
		'bookasession_general_login_page_id',
		'Login Page',
		'book_a_session_callback_dropdown_pages',
		'book_a_session_general',
		'book_a_session_section_general_settings',[ 
			"group" => "general",
			"id"	=> "login_page_id",
			"selected" => $login_page_selected,
			"label" => "Select the page your Login form will appear on. If you haven't created the page yet, you'll need to do so, select it here, hit save at the bottom of this page, and add the following shortcode to the page content: <code>[book-a-session login]</code>."
			]
	);


	$register_page_selected = isset( get_option( "book_a_session_options_general" )["register_page_id"] ) ? intval( get_option( "book_a_session_options_general" )["register_page_id"] ) : 0;

	add_settings_field(
		'bookasession_general_register_page_id',
		'Register Page',
		'book_a_session_callback_dropdown_pages',
		'book_a_session_general',
		'book_a_session_section_general_settings',[ 
			"group" => "general",
			"id"	=> "register_page_id",
			"selected" => $register_page_selected,
			"label" => "Select the page your Registration form will appear on. If you haven't created the page yet, you'll need to do so, select it here, hit save at the bottom of this page, and add the following shortcode to the page content: <code>[book-a-session register]</code>."
			]
	);


	$forgot_password_page_selected = isset( get_option( "book_a_session_options_general" )["dashboard_page_id"] ) ? intval( get_option( "book_a_session_options_general" )["dashboard_page_id"] ) : 0;

	add_settings_field(
		'bookasession_general_forgot_password_page_id',
		'Forgot Password Page',
		'book_a_session_callback_dropdown_pages',
		'book_a_session_general',
		'book_a_session_section_general_settings',[ 
			"group" => "general",
			"id"	=> "forgot_password_page_id",
			"selected" => $forgot_password_page_selected,
			"label" => "Select the page your password recovery form will appear on. If you haven't created the page yet, you'll need to do so, select it here, hit save at the bottom of this page, and add the following shortcode to the page content: <code>[book-a-session forgot-password]</code>."
			]
	);

	add_settings_field(
		'bookasession_general_savings_tag',
		'Frontend Bundle Savings Tag',
		'book_a_session_callback_field_radio',
		'book_a_session_general',
		'book_a_session_section_general_settings',[ 
			'id'  	=>  'bookasession_general_savings_tag', 
			'description' => 	'Enable or disable the display of a savings tags on bundles on the frontend form? These output the total savings customers would make by choosing a bundle with a discount greater than 0.00. For instance, if you have an 8 session bundle with a discount of £15.00 each, they would be saving £120.00 and the tag would read Save £120.00!',
			'radio_options' => array(
				true => 'Enable',
				false => 'Disable'
			),
			'inline' => true
		]
	);

	add_settings_field(
		'bookasession_general_practitioner_title',
		'Frontend Practitioner Title',
		'book_a_session_callback_field_radio',
		'book_a_session_general',
		'book_a_session_section_general_settings',[ 
			'id'  	=>  'bookasession_general_practitioner_title', 
			'description' => 	'Enable or disable the replacement of the term <code>Practitioner</code> on the frontend form with your supplied Practitioner Title. e.g. if a service type\'s Practitioner Title is Coach, enabling this would make the frontend form refer to a Practitioner as a Coach when a service of this service type is chosen.',
			'radio_options' => array(
				true => 'Enable',
				false => 'Disable'
			),
			'inline' => true
		]
	);

	add_settings_field(
		'bookasession_general_quantity_top',
		'Frontend Quantity Top Text',
		'book_a_session_callback_field_text',
		'book_a_session_general',
		'book_a_session_section_general_settings',[ 
			'id'  	=>  'bookasession_general_quantity_top', 
			'label' => 	'Enter the text for the top of the Quantity section of the frontend form prompting users to select their quantity / bundle.',
		]
	);

	add_settings_field(
		'bookasession_general_quantity_bottom',
		'Frontend Quantity Bottom Text',
		'book_a_session_callback_field_text',
		'book_a_session_general',
		'book_a_session_section_general_settings',[ 
			'id'  	=>  'bookasession_general_quantity_bottom', 
			'label' => 	'Enter the text for the bottom of the Quantity section of the frontend form after users have selected their quantity / bundle.',
		]
	);

	add_settings_field(
		'bookasession_general_location_top',
		'Frontend Location Top Text',
		'book_a_session_callback_field_text',
		'book_a_session_general',
		'book_a_session_section_general_settings',[ 
			'id'  	=>  'bookasession_general_location_top', 
			'label' => 	'Enter the text for the top of the Location section of the frontend form prompting users to select their location.',
		]
	);

	add_settings_field(
		'bookasession_general_location_bottom',
		'Frontend Location Bottom Text',
		'book_a_session_callback_field_text',
		'book_a_session_general',
		'book_a_session_section_general_settings',[ 
			'id'  	=>  'bookasession_general_location_bottom', 
			'label' => 	'Enter the text for the bottom of the Location section of the frontend form after users have selected their location.',
		]
	);

	add_settings_field(
		'bookasession_general_practitioner_top',
		'Frontend Practitioner Top Text',
		'book_a_session_callback_field_text',
		'book_a_session_general',
		'book_a_session_section_general_settings',[ 
			'id'  	=>  'bookasession_general_practitioner_top', 
			'label' => 	'Enter the text for the top of the Practitioner section of the frontend form prompting users to select their practitioner. Note: If the Frontend Practitioner Title option is enabled, the word Practitioner / practitioner will be replaced with the Practitioner Title. Also, the Practitioner section will only appear if you have more than 1 Practitioner.',
		]
	);

	add_settings_field(
		'bookasession_general_practitioner_bottom',
		'Frontend Practitioner Bottom Text',
		'book_a_session_callback_field_text',
		'book_a_session_general',
		'book_a_session_section_general_settings',[ 
			'id'  	=>  'bookasession_general_practitioner_bottom', 
			'label' => 	'Enter the text for the bottom of the Practitioner section of the frontend form after users have selected their practitioner. Note: If the Frontend Practitioner Title option is enabled, the word Practitioner / practitioner will be replaced with the Practitioner Title.  Also, the Practitioner section will only appear if you have more than 1 Practitioner.',
		]
	);
	
	add_settings_field(
		'bookasession_general_payment_method_top',
		'Frontend Payment Method Top Text',
		'book_a_session_callback_field_text',
		'book_a_session_general',
		'book_a_session_section_general_settings',[ 
			'id'  	=>  'bookasession_general_payment_method_top', 
			'label' => 	'Enter the text for the top of the Payment Method section of the frontend form prompting users to select their payment method.',
		]
	);

	add_settings_field(
		'bookasession_general_practitioner_bottom',
		'Frontend Payment Method Bottom Text',
		'book_a_session_callback_field_text',
		'book_a_session_general',
		'book_a_session_section_general_settings',[ 
			'id'  	=>  'bookasession_general_payment_method_bottom', 
			'label' => 	'Enter the text for the bottom of the Payment Method section of the frontend form after users have selected their payment method.',
		]
	);

	add_settings_field(
		'bookasession_general_dashboard_private_locations',
		'Dashboard Private Location',
		'book_a_session_callback_field_radio',
		'book_a_session_general',
		'book_a_session_section_general_settings',[ 
			'id'  	=>  'bookasession_general_dashboard_private_locations', 
			'description' => 	'Enable or disable the display of a page in client dashboards containing the addresses of any private locations for which they\'ve got a confirmed order.',
			'radio_options' => array(
				true => 'Enable',
				false => 'Disable'
			),
			'inline' => true
		]
	);

	// Schedule

	add_settings_field(
		'bookasession_schedule_timezone',
		'Timezone',
		'book_a_session_callback_field_text',
		'book_a_session_schedule',
		'book_a_session_section_schedule_settings',[ 
			'id'  	=>  'bookasession_schedule_timezone', 
			'label' => 	'Enter the default timezone for the session times in your schedule. 
						<p class="description">e.g. Europe/London. Find your timezone <a href="http://php.net/manual/en/timezones.php" target="_blank">here</a>.</p>
						<p class="description">This is used to calculate and change the times in your schedule for users who select Regions with a timezone different from your own.' ]
	);

	// Create payment method acceptance settings automatically

    $payment_method_array = book_a_session_get_table_array( "payment_method" );

        for ($i = 0; $i < count($payment_method_array); $i++) {

			add_settings_section( 
				'book_a_session_section_payment_method_settings_' . $payment_method_array[$i]->id, 
				$payment_method_array[$i]->name . ' Settings', 
				'book_a_session_callback_section_payment_method_settings_' . $payment_method_array[$i]->id, 
				'book_a_session_payment_methods'
			);

			add_settings_field(
				'bookasession_paymentmethods_accept' . $payment_method_array[$i]->id,
				'Accept '. $payment_method_array[$i]->name,
				'book_a_session_callback_field_radio',
				'book_a_session_payment_methods',
				'book_a_session_section_payment_method_settings_' . $payment_method_array[$i]->id,[ 
					'id'  	=>  'bookasession_paymentmethods_accept' . $payment_method_array[$i]->id, 
					'description' => 	'Enable or disable ' . $payment_method_array[$i]->name . ' on the frontend.',
					'radio_options' => array(
						true => 'Enable',
						false => 'Disable'
					),
					'inline' => true
				]
			);

			add_settings_field(
				'bookasession_paymentmethods_frontend_icon_' . $payment_method_array[$i]->id,
				'Icon for '. $payment_method_array[$i]->name,
				'book_a_session_callback_field_radio',
				'book_a_session_payment_methods',
				'book_a_session_section_payment_method_settings_' . $payment_method_array[$i]->id,[ 
					'id'  	=>  'bookasession_paymentmethods_frontend_icon_' . $payment_method_array[$i]->id, 
					'description' => 	'Enable or disable the display of an icon for ' . $payment_method_array[$i]->name . ' on the frontend.',
					'radio_options' => array(
						true => 'Enable',
						false => 'Disable'
					),
					'inline' => true
				]
			);

			add_settings_field(
				'bookasession_paymentmethods_frontend_title_' . $payment_method_array[$i]->id,
				'Title for ' . $payment_method_array[$i]->name,
				'book_a_session_callback_field_text',
				'book_a_session_payment_methods',
				'book_a_session_section_payment_method_settings_' . $payment_method_array[$i]->id,[ 
					'id'  	=>  'bookasession_paymentmethods_frontend_title_' . $payment_method_array[$i]->id, 
					'label' => 	'Enter a title to appear on the frontend form for ' . $payment_method_array[$i]->name . " here. If nothing is entered here, and as long as an icon is displayed, a title will not be shown. Otherwise, the payment method's default name will be used instead."]
			);			
			
			add_settings_field(
				'bookasession_paymentmethods_frontend_subtitle_' . $payment_method_array[$i]->id,
				'Subtitle for ' . $payment_method_array[$i]->name,
				'book_a_session_callback_field_text',
				'book_a_session_payment_methods',
				'book_a_session_section_payment_method_settings_' . $payment_method_array[$i]->id,[ 
					'id'  	=>  'bookasession_paymentmethods_frontend_subtitle_' . $payment_method_array[$i]->id, 
					'label' => 	'Enter a subtitle to appear on the frontend form for ' . $payment_method_array[$i]->name . " here. If nothing is entered here, the payment method's default name will be used instead."]
			);

			add_settings_field(
				'bookasession_paymentmethods_frontend_tag_' . $payment_method_array[$i]->id,
				'Tag for ' . $payment_method_array[$i]->name,
				'book_a_session_callback_field_text',
				'book_a_session_payment_methods',
				'book_a_session_section_payment_method_settings_' . $payment_method_array[$i]->id,[ 
					'id'  	=>  'bookasession_paymentmethods_frontend_tag_' . $payment_method_array[$i]->id, 
					'label' => 	'Enter a tag to appear on the frontend form for ' . $payment_method_array[$i]->name . " here. If nothing is entered here, a tag will not be shown for this payment method."]
			);

			add_settings_field(
				'bookasession_paymentmethods_frontend_top_' . $payment_method_array[$i]->id,
				'Frontend Top Text for ' . $payment_method_array[$i]->name,
				'book_a_session_callback_field_textarea',
				'book_a_session_payment_methods',
				'book_a_session_section_payment_method_settings_' . $payment_method_array[$i]->id,[ 
					'id'  	=>  'bookasession_paymentmethods_frontend_top_' . $payment_method_array[$i]->id, 
					'label' => 	'Enter text to appear on the top of the frontend form once a user has chosen ' . $payment_method_array[$i]->name . " as their payment method."]
			);

			add_settings_field(
				'bookasession_paymentmethods_frontend_bottom_' . $payment_method_array[$i]->id,
				'Frontend Bottom Text for ' . $payment_method_array[$i]->name,
				'book_a_session_callback_field_textarea',
				'book_a_session_payment_methods',
				'book_a_session_section_payment_method_settings_' . $payment_method_array[$i]->id,[ 
					'id'  	=>  'bookasession_paymentmethods_frontend_bottom_' . $payment_method_array[$i]->id, 
					'label' => 	'Enter text to appear on the bottom of the frontend form once a user has chosen ' . $payment_method_array[$i]->name . " as their payment method."]
			);

			if ( $payment_method_array[$i]->id == "1" ) {

				// PayPal

				add_settings_field(
					'bookasession_paymentmethods_sandbox_live_' . $payment_method_array[$i]->id,
					'Sandbox / Live API environment for '. $payment_method_array[$i]->name,
					'book_a_session_callback_field_radio',
					'book_a_session_payment_methods',
					'book_a_session_section_payment_method_settings_' . $payment_method_array[$i]->id,[ 
						'id'  	=>  'bookasession_paymentmethods_sandbox_live_' . $payment_method_array[$i]->id, 
						'description' => 	'When testing your website and its PayPal integration, switch to Sandbox. When your website is live, switch to Live mode.',
						'radio_options' => array(
							'sandbox' => 'Sandbox',
							'production' => 'Live'
						),
						'inline' => true
					]
				);

				add_settings_field(
					'bookasession_paymentmethods_sandbox_id_' . $payment_method_array[$i]->id,
					'Sandbox ID for ' . $payment_method_array[$i]->name,
					'book_a_session_callback_field_text',
					'book_a_session_payment_methods',
					'book_a_session_section_payment_method_settings_' . $payment_method_array[$i]->id,[ 
						'id'  	=>  'bookasession_paymentmethods_sandbox_id_' . $payment_method_array[$i]->id, 
						'label' => 	'Enter the sandbox ID for ' . $payment_method_array[$i]->name . " here. You can get a Sandbox ID by signing up for or logging into a PayPal Business Account, and going to https://developer.paypal.com/developer/applications/create to create an app. If you already have one, find it in your developer dashboard, click on it and click on Sandbox at the top to find your Client ID and paste it here. "]
				);

				add_settings_field(
					'bookasession_paymentmethods_live_id_' . $payment_method_array[$i]->id,
					'Live ID for ' . $payment_method_array[$i]->name,
					'book_a_session_callback_field_text',
					'book_a_session_payment_methods',
					'book_a_session_section_payment_method_settings_' . $payment_method_array[$i]->id,[ 
						'id'  	=>  'bookasession_paymentmethods_live_id_' . $payment_method_array[$i]->id, 
						'label' => 	'Enter the live ID for ' . $payment_method_array[$i]->name . " here. You can get a Live ID by signing up for or logging into a PayPal Business Account, and going to https://developer.paypal.com/developer/applications/create to create an app. If you already have one, find it in your developer dashboard, click on it and click on Live at the top to find your Client ID and paste it here. "]
				);

				add_settings_field(
					'bookasession_paymentmethods_sandbox_secret_' . $payment_method_array[$i]->id,
					'Sandbox Secret for ' . $payment_method_array[$i]->name,
					'book_a_session_callback_field_text',
					'book_a_session_payment_methods',
					'book_a_session_section_payment_method_settings_' . $payment_method_array[$i]->id,[ 
						'id'  	=>  'bookasession_paymentmethods_sandbox_secret_' . $payment_method_array[$i]->id, 
						'label' => 	'Enter the Sandbox Secret for ' . $payment_method_array[$i]->name . " here. You can get a Sandbox Secret by signing up for or logging into a PayPal Business Account, and going to https://developer.paypal.com/developer/applications/create to create an app. If you already have one, find it in your developer dashboard, click on it and click on Sandbox at the top to find your Secret and paste it here. "]
				);

				add_settings_field(
					'bookasession_paymentmethods_live_secret_' . $payment_method_array[$i]->id,
					'Live Secret for ' . $payment_method_array[$i]->name,
					'book_a_session_callback_field_text',
					'book_a_session_payment_methods',
					'book_a_session_section_payment_method_settings_' . $payment_method_array[$i]->id,[ 
						'id'  	=>  'bookasession_paymentmethods_live_secret_' . $payment_method_array[$i]->id, 
						'label' => 	'Enter the Live Secret for ' . $payment_method_array[$i]->name . " here. You can get a Live Secret by signing up for or logging into a PayPal Business Account, and going to https://developer.paypal.com/developer/applications/create to create an app. If you already have one, find it in your developer dashboard, click on it and click on Live at the top to find your Secret and paste it here. "]
				);

			}

			if ( $payment_method_array[$i]->id == "4" ) {

				// Stripe

				add_settings_field(
					'bookasession_paymentmethods_test_live_' . $payment_method_array[$i]->id,
					'Test / Live API environment for '. $payment_method_array[$i]->name,
					'book_a_session_callback_field_radio',
					'book_a_session_payment_methods',
					'book_a_session_section_payment_method_settings_' . $payment_method_array[$i]->id,[ 
						'id'  	=>  'bookasession_paymentmethods_test_live_' . $payment_method_array[$i]->id, 
						'description' => 	'When testing your website and its ' . $payment_method_array[$i]->name . ' integration, switch to Test. When your website is live, switch to Live mode.',
						'radio_options' => array(
							'test' => 'Test',
							'live' => 'Live'
						),
						'inline' => true
					]
				);

				add_settings_field(
					'bookasession_paymentmethods_test_pub_key_' . $payment_method_array[$i]->id,
					'Test Publishable Key for ' . $payment_method_array[$i]->name,
					'book_a_session_callback_field_text',
					'book_a_session_payment_methods',
					'book_a_session_section_payment_method_settings_' . $payment_method_array[$i]->id,[ 
						'id'  	=>  'bookasession_paymentmethods_test_pub_key_' . $payment_method_array[$i]->id, 
						'label' => 	'Enter the test publishable key for ' . $payment_method_array[$i]->name . " here. You can get a test publishable key by signing up for or logging into a Stripe account, and going to https://dashboard.stripe.com/account/apikeys. If the View test data toggle is switched off, switch it on to find your test keys. Then copy the publishable key in the table titled Standard API keys and paste it here."]
				);

				add_settings_field(
					'bookasession_paymentmethods_test_sec_key_' . $payment_method_array[$i]->id,
					'Test Secret Key for ' . $payment_method_array[$i]->name,
					'book_a_session_callback_field_text',
					'book_a_session_payment_methods',
					'book_a_session_section_payment_method_settings_' . $payment_method_array[$i]->id,[ 
						'id'  	=>  'bookasession_paymentmethods_test_sec_key_' . $payment_method_array[$i]->id, 
						'label' => 	'Enter the test secret key for ' . $payment_method_array[$i]->name . " here. You can get a test secret key by signing up for or logging into a Stripe account, and going to https://dashboard.stripe.com/account/apikeys. If the View test data toggle is switched off, switch it on to find your test keys. Then click on Reveal test key token in the table titled Standard API keys and paste the revealed value here."]
				);

				add_settings_field(
					'bookasession_paymentmethods_live_pub_key_' . $payment_method_array[$i]->id,
					'Live Publishable Key for ' . $payment_method_array[$i]->name,
					'book_a_session_callback_field_text',
					'book_a_session_payment_methods',
					'book_a_session_section_payment_method_settings_' . $payment_method_array[$i]->id,[ 
						'id'  	=>  'bookasession_paymentmethods_live_pub_key_' . $payment_method_array[$i]->id, 
						'label' => 	'Enter the live publishable key for ' . $payment_method_array[$i]->name . " here. You can get a test publishable key by signing up for or logging into a Stripe account, and going to https://dashboard.stripe.com/account/apikeys. If the View test data toggle is switched on, switch it off to find your live keys. Then copy the publishable key in the table titled Standard API keys and paste it here."]
				);

				add_settings_field(
					'bookasession_paymentmethods_live_sec_key_' . $payment_method_array[$i]->id,
					'Live Secret Key for ' . $payment_method_array[$i]->name,
					'book_a_session_callback_field_text',
					'book_a_session_payment_methods',
					'book_a_session_section_payment_method_settings_' . $payment_method_array[$i]->id,[ 
						'id'  	=>  'bookasession_paymentmethods_live_sec_key_' . $payment_method_array[$i]->id, 
						'label' => 	'Enter the live secret key for ' . $payment_method_array[$i]->name . " here. You can get a live secret key by signing up for or logging into a Stripe account, and going to https://dashboard.stripe.com/account/apikeys. If the View test data toggle is switched on, switch it off to find your live keys. Then click on Reveal live key token in the table titled Standard API keys and paste the revealed value here."]
				);

			}

		}

	add_settings_field(
		'bookasession_paymentmethods_online_payable',
		'Online Payment Payable',
		'book_a_session_callback_field_radio',
		'book_a_session_payment_methods',
		'book_a_session_section_payment_method_settings',[ 
			'id'  	=>  'bookasession_paymentmethods_online_payable', 
			'description' => 	'In a future update, you can choose whether online payments are required during the checkout process, or by a due date (sending an invoice via email requesting payment, and enabling online payments in the dashboard). This applies to PayPal and Credit / Debit Card (Stripe). Currently, online payments are always due during checkout, regardless of which is chosen here.',
			'radio_options' => array(
				'checkout' => 'Payable during checkout',
				'due_date' => 'Payable by due date'
			),
			'inline' => false
		]
	);

	/* Timetables */

	add_settings_field(
		'bookasession_timetables_tooltip_max',
		'Maximum Displayed Sessions in Tooltips',
		'book_a_session_callback_field_number',
		'book_a_session_timetable',
		'book_a_session_section_timetable_settings',[ 
			'id'  	=>  'bookasession_timetables_tooltip_max', 
			'label' => 	'What\'s the maximum number of sessions to display in a tooltip for one day in Calendar Timetables, or one day\'s session slot on Week Timetables? If this number is exceeded, the rest are hidden, and a message is displayed with the number of hidden sessions. Tooltips only display on Week and Calendar Timetables.',
			'step'	=>	'1',
			'min'	=>	'1']
	);

	add_settings_field(
		'bookasession_timetables_result_limit',
		'Simple Timetable Default Limit',
		'book_a_session_callback_field_number',
		'book_a_session_timetable',
		'book_a_session_section_timetable_settings',[ 
			'id'  	=>  'bookasession_timetables_result_limit', 
			'label' => 	'What\'s the <i>default</i> maximum number of results you want loaded for Simple Timetables?<br>This can be overridden by using the timetable_limit shortcode attribute, e.g. <code>[book-a-session timetable="all" timetable_format="simple_upcoming" <strong>timetable_limit="100"</strong>]</code>',
			'step'	=>	'1',
			'min'	=>	'1']
	);

	add_settings_field(
		'bookasession_timetables_week_load',
		'Week Timetable / Frontend Date Picker Weeks',
		'book_a_session_callback_field_number',
		'book_a_session_timetable',
		'book_a_session_section_timetable_settings',[ 
			'id'  	=>  'bookasession_timetables_week_load', 
			'label' => 	'How many weeks do you want loaded for Week Timetables and the date picker on the frontend form?',
			'step'	=>	'1',
			'min'	=>	'1']
	);
			
	add_settings_field(
		'bookasession_timetables_month_load',
		'Calendar Timetable Months',
		'book_a_session_callback_field_number',
		'book_a_session_timetable',
		'book_a_session_section_timetable_settings',[ 
			'id'  	=>  'bookasession_timetables_month_load', 
			'label' => 	'How many months do you want loaded for Calendar Timetables?',
			'step'	=>	'1',
			'min'	=>	'1']
	);
		
	/* Email */

	add_settings_field(
		'bookasession_emails_from_email_address',
		'Sender Email Address',
		'book_a_session_callback_field_text',
		'book_a_session_emails',
		'book_a_session_section_email_settings',[ 
			'id'  	=>  'bookasession_emails_from_email_address', 
			'label' => 	'Enter the email address emails should be sent from. We recommend avoiding using addresses from Gmail and similar sites as they can cause delivery problems in this system.']
	);

	add_settings_field(
		'bookasession_emails_from_name',
		'Sender Name',
		'book_a_session_callback_field_text',
		'book_a_session_emails',
		'book_a_session_section_email_settings',[ 
			'id'  	=>  'bookasession_emails_from_name', 
			'label' => 	'Enter a name that will appear next to the emaill address, which will look like this: John &lt;john@example.com&gt;. By default, this will be the website\'s name.']
	);

	/* PHPMailer Email */

	add_settings_field(
		'bookasession_emails_phpmailer_enabled',
		'Authenticated Emails',
		'book_a_session_callback_field_radio',
		'book_a_session_emails',
		'book_a_session_section_email_phpmailer_settings',[ 
			'id'  	=>  'bookasession_emails_phpmailer_enabled',  
			'description' => 	'Choose whether your emails should be authenticated or not. If you enable this, you need to fill out all non optional fields below to get it working.',
			'radio_options' => array(
				true => 'Enable',
				false => 'Disable'
			),
			'inline' => true
		]
	);

	add_settings_field(
		'bookasession_emails_phpmailer_host',
		'SMTP Host',
		'book_a_session_callback_field_text',
		'book_a_session_emails',
		'book_a_session_section_email_phpmailer_settings',[ 
			'id'  	=>  'bookasession_emails_phpmailer_host', 
			'label' => 	'Enter your SMTP host address. You can usually find this in your email account, or by searching for your email provider and <code>smtp</code>.']
	);

	add_settings_field(
		'bookasession_emails_phpmailer_smtpsecure',
		'Authentication (Optional)',
		'book_a_session_callback_field_radio',
		'book_a_session_emails',
		'book_a_session_section_email_phpmailer_settings',[ 
			'id'  	=>  'bookasession_emails_phpmailer_smtpsecure',  
			'description' => 'Choose your authentication method. If the port provided to you is <code>465</code>, choose SSL. If it\'s <code>587</code>, choose TLS. Otherwise, consult your email provider.',
			'radio_options' => array(
				"ssl" => 'SSL',
				"tls" => 'TLS'
			),
			'inline' => true
		]
	);

	add_settings_field(
		'bookasession_emails_phpmailer_smtp_port',
		'SMTP Port',
		'book_a_session_callback_field_number',
		'book_a_session_emails',
		'book_a_session_section_email_phpmailer_settings',[ 
			'id'  	=>  'bookasession_emails_phpmailer_smtp_port',  
			'label' => 	'Enter the SMTP Port. If you chose TLS as your Authentication option, this port number is usually <code>587</code>. If you chose SSL, it\'s likely <code>465</code>. Be sure to check with your email provider.',
			'step'	=>	'1',
			'min'	=>	'1'
		]
	);

	add_settings_field(
		'bookasession_emails_phpmailer_username',
		'SMTP Username',
		'book_a_session_callback_field_text',
		'book_a_session_emails',
		'book_a_session_section_email_phpmailer_settings',[ 
			'id'  	=>  'bookasession_emails_phpmailer_username', 
			'label' => 	'Enter your SMTP username. This is usually your email address, or the letters before the <code>@</code> in your email address, though it could be something else.']
	);

	if ( ! defined( 'BOOK_A_SESSION_SMTP_PASSWORD' ) ) {

		add_settings_field(
			'bookasession_emails_phpmailer_password',
			'SMTP Password',
			'book_a_session_callback_field_password',
			'book_a_session_emails',
			'book_a_session_section_email_phpmailer_settings',[ 
				'id'  	=>  'bookasession_emails_phpmailer_password', 
				'label' => 	'Enter your SMTP email password. This should be the same password you would use to log into your email account.
				<br><br>
				<strong>Important:</strong> Passwords entered here must be stored in plaintext in your database. Because of the extra risks this poses, we recommend removing your password from this field when your website is available to the public (or avoiding it entirely), and instead editing your wp-config.php file and entering your password there, which is far more secure.
				<br><br>
				To do so, remove the password in the above field if it, go to the bottom of this page and save if you haven\'t done so already. Then, once the password is removed, open up your <a href="https://codex.wordpress.org/Editing_wp-config.php">wp-config.php</a> file and look for <code>define("WP_DEBUG", false);</code> on or around the 80th line, near the bottom. Underneath that, and before the line reading <code>/* That\'s all, stop editing! Happy blogging. */</code>, enter the following:
				<br><br>
				<code>define("BOOK_A_SESSION_SMTP_PASSWORD", "enter_your_password_here");</code>
				<br><br>
				Enter your password between the second pair of quotation marks, save the file and upload it to your WordPress installation directory. Then return here and refresh the page. If your password was found, this text will be replaced with a different message.']
		);
	
	} else {

		add_settings_field(
			'bookasession_emails_phpmailer_password',
			'SMTP Password',
			'book_a_session_callback_field_label',
			'book_a_session_emails',
			'book_a_session_section_email_phpmailer_settings',[ 
				'content' => 	'We have your SMTP password stored securely. To change it, edit <code>define("BOOK_A_SESSION_SMTP_PASSWORD", "enter_your_password_here");</code> in your wp-config.php file.']
		);

	}

	add_settings_field(
		'bookasession_emails_phpmailer_from_email_address',
		'Sender Email Address (Optional)',
		'book_a_session_callback_field_text',
		'book_a_session_emails',
		'book_a_session_section_email_phpmailer_settings',[ 
			'id'  	=>  'bookasession_emails_phpmailer_from_email_address', 
			'label' => 	'Enter the email address.']
	);

	add_settings_field(
		'bookasession_emails_phpmailer_from_name',
		'Sender Name (Optional)',
		'book_a_session_callback_field_text',
		'book_a_session_emails',
		'book_a_session_section_email_phpmailer_settings',[ 
			'id'  	=>  'bookasession_emails_phpmailer_from_name', 
			'label' => 	'Enter a name that will appear next to the emaill address, which will look like this: John &lt;john@example.com&gt;. By default, this will be the website\'s name.']
	);

	/* Invoices */

	/* Due Date Period */

	add_settings_field(
		'bookasession_invoices_due_date_offset_measurement',
		'Measurement',
		'book_a_session_callback_field_radio',
		'book_a_session_invoices',
		'book_a_session_section_invoice_settings',[ 
			'id'  			=>  'bookasession_invoices_due_date_offset_measurement', 
			'description' 	=> 	'Please choose whether you\'d like to measure the due date period in minutes, hours, or days.',
			'radio_options'	=> array( 
				'minutes' 	=> 'Minutes',
				'hours' 	=> 'Hours',
				'days' 		=> 'Days'
				)			
			]
	);
	
	add_settings_field(
		'bookasession_invoices_due_date_offset_value',
		'Value',
		'book_a_session_callback_field_number',
		'book_a_session_invoices',
		'book_a_session_section_invoice_settings',[ 
			'id'  	=>  'bookasession_invoices_due_date_offset_value', 
			'label' => 	'How many minutes, hours, or days?',
			'step'	=>	'1',
			'min'	=>	'0'
			]
	);

	add_settings_field(
		'bookasession_invoices_due_date_offset_anchor',
		'Anchor',
		'book_a_session_callback_field_radio',
		'book_a_session_invoices',
		'book_a_session_section_invoice_settings',[ 
			'id'  			=>  'bookasession_invoices_due_date_offset_anchor', 
			'description' 	=> 	'Will this period start from the date the invoice was issued, or before the first session date? 
			For instance, if want these invoices paid at least 3 days before the first session date, you\'d pick Days, 3, Session Date. 
			If payments should be due no later than 36 hours after they\'ve made their order, you\'d choose Hours, 36, Issue Date.',
			'radio_options'	=> array( 
				'issue_date' 	=> 'Issue Date',
				'session_date' 	=> 'Session Date'
				)			
			]
	);

	/* Online Invoices */

	add_settings_field(
		'bookasession_invoices_paypal_logo',
		'PayPal Logo',
		'book_a_session_callback_field_radio',
		'book_a_session_invoices',
		'book_a_session_section_invoice_online_settings',[ 
			'id'  	=>  'bookasession_invoices_paypal_logo', 
			'description' => "Choose whether you'd like to display a logo for PayPal due and partly paid invoices or not.",
			'radio_options' => array(
				true => "Display a PayPal logo",
				false => "No logo" 
			)]
	);

	add_settings_field(
		'bookasession_invoices_stripe_logo',
		'Credit / Debit Card (Stripe) Logo',
		'book_a_session_callback_field_radio',
		'book_a_session_invoices',
		'book_a_session_section_invoice_online_settings',[ 
			'id'  	=>  'bookasession_invoices_stripe_logo', 
			'description' => "Choose whether you'd like to display a logo for Credit / Debit Card (Stripe) due and partly paid invoices or not.",
			'radio_options' => array(
				true => "Display a Credit / Debit Card (Stripe) logo",
				false => "No logo" 
			)]
	);	

	/* Online Due */

	add_settings_field(
		'bookasession_invoices_email_subject_online_due',
		'Due: Email Subject',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_online_due_settings',[ 
			'id'  	=>  'bookasession_invoices_email_subject_online_due', 
			'label' => 	'Enter the subject of online payment due invoice emails.<br>
			<code>Invoice tags supported</code>']
	);	

	add_settings_field(
		'bookasession_invoices_reminder_email_subject_online_due',
		'Due: Reminder Email Subject',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_online_due_settings',[ 
			'id'  	=>  'bookasession_invoices_reminder_email_subject_online_due', 
			'label' => 	'Enter the subject of reminder online payment due invoice emails.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_hidden_preheader_online_due',
		'Due: Hidden Preheader',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_online_due_settings',[ 
			'id'  	=>  'bookasession_invoices_hidden_preheader_online_due', 
			'label' => 	'Enter a preheader that appears after the subject in email clients before opening an email, e.g. <i>Sender - Subject of the email - Preheader that won\'t appear on the email.</i><br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_header_large_text_online_due',
		'Due: Title',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_online_due_settings',[ 
			'id'  	=>  'bookasession_invoices_header_large_text_online_due', 
			'label' => 	'Enter the title text for the header in online payment due invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_header_subtitle_text_online_due',
		'Due: Subtitle',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_online_due_settings',[ 
			'id'  	=>  'bookasession_invoices_header_subtitle_text_online_due', 
			'label' => 	'Enter the subtitle text for the header in online payment due invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_table_title_online_due',
		'Due: Table Title',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_online_due_settings',[ 
			'id'  	=>  'bookasession_invoices_table_title_online_due', 
			'label' => 	'Enter the title text for the itemised table in online payment due invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_table_paragraph_online_due',
		'Due: Table Paragraph',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_online_due_settings',[ 
			'id'  	=>  'bookasession_invoices_table_paragraph_online_due', 
			'label' => 	'Enter the subtitle text for the header in online payment due invoices.<br>
			<code>Invoice tags supported</code>']
	);


	/* Online Partly Paid */

	add_settings_field(
		'bookasession_invoices_email_subject_online_partly_paid',
		'Partly Paid: Email Subject',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_online_partly_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_email_subject_online_partly_paid', 
			'label' => 	'Enter the subject of partly paid online payment invoice emails.<br>
			<code>Invoice tags supported</code>']
	);	

	add_settings_field(
		'bookasession_invoices_reminder_email_subject_online_partly_paid',
		'Partly Paid: Reminder Email Subject',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_online_partly_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_reminder_email_subject_online_partly_paid', 
			'label' => 	'Enter the subject of partly paid online invoice reminder emails.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_hidden_preheader_online_partly_paid',
		'Partly Paid: Hidden Preheader',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_online_partly_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_hidden_preheader_online_partly_paid', 
			'label' => 	'Enter a preheader that appears after the subject in email clients before opening an email, e.g. <i>Sender - Subject of the email - Preheader that won\'t appear on the email.</i><br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_header_large_text_online_partly_paid',
		'Partly Paid: Title',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_online_partly_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_header_large_text_online_partly_paid', 
			'label' => 	'Enter the title text for the header in partly paid online payment method invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_header_subtitle_text_online_partly_paid',
		'Partly Paid: Subtitle',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_online_partly_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_header_subtitle_text_online_partly_paid', 
			'label' => 	'Enter the subtitle text for the header in partly paid online payment invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_table_title_online_partly_paid',
		'Partly Paid: Table Title',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_online_partly_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_table_title_online_partly_paid', 
			'label' => 	'Enter the title text for the itemised table in partly paid online payment invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_table_paragraph_online_partly_paid',
		'Partly Paid: Table Paragraph',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_online_partly_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_table_paragraph_online_partly_paid', 
			'label' => 	'Enter the subtitle text for the header in partly paid online payment invoices.<br>
			<code>Invoice tags supported</code>']
	);


	/* Online Paid */

	add_settings_field(
		'bookasession_invoices_email_subject_online_paid',
		'Paid: Email Subject',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_online_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_email_subject_online_paid', 
			'label' => 	'Enter the subject of paid online payment invoice emails.<br>
			<code>Invoice tags supported</code>']
	);	

	add_settings_field(
		'bookasession_invoices_reminder_email_subject_online_paid',
		'Paid: Reminder Email Subject',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_online_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_reminder_email_subject_online_paid', 
			'label' => 	'Enter the subject of paid online payment invoice reminder emails.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_hidden_preheader_online_paid',
		'Paid: Hidden Preheader',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_online_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_hidden_preheader_online_paid', 
			'label' => 	'Enter a preheader that appears after the subject in email clients before opening an email, e.g. <i>Sender - Subject of the email - Preheader that won\'t appear on the email.</i><br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_header_large_text_online_paid',
		'Paid: Title',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_online_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_header_large_text_online_paid', 
			'label' => 	'Enter the title text for the header in paid online payment invoices.<br>
			<code>Invoice tags supported</code>']
		);
		
	add_settings_field(
		'bookasession_invoices_header_subtitle_text_online_paid',
		'Paid: Subtitle',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_online_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_header_subtitle_text_online_paid', 
			'label' => 	'Enter the subtitle text for the header in paid online payment invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_table_title_online_paid',
		'Paid: Table Title',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_online_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_table_title_online_paid', 
			'label' => 	'Enter the title text for the itemised table in paid online payment invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_table_paragraph_online_paid',
		'Paid: Table Paragraph',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_online_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_table_paragraph_online_paid', 
			'label' => 	'Enter the subtitle text for the header in paid online payment invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_completed_online_paid',
		'Paid: Completed Sentence',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_online_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_completed_online_paid', 
			'label' => 	'Enter an additional sentence to be included if a paid online payment invoice is sent after their session(s) have been completed.<br>
			<code>Invoice tags supported</code>']
	);

	/* Cash Invoices */

	add_settings_field(
		'bookasession_invoices_cash_logo',
		'Cash Logo',
		'book_a_session_callback_field_radio',
		'book_a_session_invoices',
		'book_a_session_section_invoice_cash_settings',[ 
			'id'  	=>  'bookasession_invoices_cash_logo', 
			'description' => "Choose whether you'd like to display a logo for cash invoices or not.",
			'radio_options' => array(
				true => "Display a cash logo",
				false => "No logo" 
			)]
	);	
/*
	add_settings_field(
		'bookasession_invoices_cash_logo_color',
		'Cash Logo Color',
		'book_a_session_callback_field_text',
		'book_a_session_invoices',
		'book_a_session_section_invoice_cash_settings',[ 
			'id'  	=>  'bookasession_invoices_cash_logo_color', 
			'label' => 	'Enter a CSS color code for the cash logo, such as Hex: <code>#2fb6fe</code>, RGB/RGBA: <code>rgba(48, 182, 254, 1)</code> or HSL: <code>hsl(201, 99%, 59%)</code>.']
	);
*/	
	/* Cash Due */

	add_settings_field(
		'bookasession_invoices_email_subject_cash_due',
		'Due: Email Subject',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_cash_due_settings',[ 
			'id'  	=>  'bookasession_invoices_email_subject_cash_due', 
			'label' => 	'Enter the subject of cash due invoice emails.<br>
			<code>Invoice tags supported</code>']
	);	
	
	add_settings_field(
		'bookasession_invoices_reminder_email_subject_cash_due',
		'Due: Reminder Email Subject',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_cash_due_settings',[ 
			'id'  	=>  'bookasession_invoices_reminder_email_subject_cash_due', 
			'label' => 	'Enter the subject of reminder cash due invoice emails.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_hidden_preheader_cash_due',
		'Due: Hidden Preheader',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_cash_due_settings',[ 
			'id'  	=>  'bookasession_invoices_hidden_preheader_cash_due', 
			'label' => 	'Enter a preheader that appears after the subject in email clients before opening an email, e.g. <i>Sender - Subject of the email - Preheader that won\'t appear on the email.</i><br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_header_large_text_cash_due',
		'Due: Title',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_cash_due_settings',[ 
			'id'  	=>  'bookasession_invoices_header_large_text_cash_due', 
			'label' => 	'Enter the title text for the header in Cash due invoices<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_header_subtitle_text_cash_due',
		'Due: Subtitle',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_cash_due_settings',[ 
			'id'  	=>  'bookasession_invoices_header_subtitle_text_cash_due', 
			'label' => 	'Enter the subtitle text for the header in cash due invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_table_title_cash_due',
		'Due: Table Title',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_cash_due_settings',[ 
			'id'  	=>  'bookasession_invoices_table_title_cash_due', 
			'label' => 	'Enter the title text for the itemised table in cash due invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_table_paragraph_cash_due',
		'Due: Table Paragraph',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_cash_due_settings',[ 
			'id'  	=>  'bookasession_invoices_table_paragraph_cash_due', 
			'label' => 	'Enter the subtitle text for the header in cash due invoices.<br>
			<code>Invoice tags supported</code>']
	);


	/* Cash Partly Paid */	

	add_settings_field(
		'bookasession_invoices_email_subject_cash_partly_paid',
		'Partly Paid: Email Subject',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_cash_partly_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_email_subject_cash_partly_paid', 
			'label' => 	'Enter the subject of partly paid cash invoice emails.<br>
			<code>Invoice tags supported</code>']
	);	
	
	add_settings_field(
		'bookasession_invoices_reminder_email_subject_cash_partly_paid',
		'Partly Paid: Reminder Email Subject',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_cash_partly_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_reminder_email_subject_cash_partly_paid', 
			'label' => 	'Enter the subject of partly paid cash invoice reminder emails.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_hidden_preheader_cash_partly_paid',
		'Partly Paid: Hidden Preheader',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_cash_partly_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_hidden_preheader_cash_partly_paid', 
			'label' => 	'Enter a preheader that appears after the subject in email clients before opening an email, e.g. <i>Sender - Subject of the email - Preheader that won\'t appear on the email.</i><br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_header_large_text_cash_partly_paid',
		'Partly Paid: Title',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_cash_partly_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_header_large_text_cash_partly_paid', 
			'label' => 	'Enter the title text for the header in partly paid Cash invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_header_subtitle_text_cash_partly_paid',
		'Partly Paid: Subtitle',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_cash_partly_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_header_subtitle_text_cash_partly_paid', 
			'label' => 	'Enter the subtitle text for the header in partly paid cash invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_table_title_cash_partly_paid',
		'Partly Paid: Table Title',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_cash_partly_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_table_title_cash_partly_paid', 
			'label' => 	'Enter the title text for the itemised table in partly paid cash invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_table_paragraph_cash_partly_paid',
		'Partly Paid: Table Paragraph',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_cash_partly_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_table_paragraph_cash_partly_paid', 
			'label' => 	'Enter the subtitle text for the header in partly paid cash invoices.<br>
			<code>Invoice tags supported</code>']
	);


	/* Cash Paid */

	add_settings_field(
		'bookasession_invoices_email_subject_cash_paid',
		'Paid: Email Subject',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_cash_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_email_subject_cash_paid', 
			'label' => 	'Enter the subject of paid Cash invoice emails.<br>
			<code>Invoice tags supported</code>']
	);	
	
	add_settings_field(
		'bookasession_invoices_reminder_email_subject_cash_paid',
		'Paid: Reminder Email Subject',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_cash_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_reminder_email_subject_cash_paid', 
			'label' => 	'Enter the subject of paid Cash invoice reminder emails.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_hidden_preheader_cash_paid',
		'Paid: Hidden Preheader',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_cash_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_hidden_preheader_cash_paid', 
			'label' => 	'Enter a preheader that appears after the subject in email clients before opening an email, e.g. <i>Sender - Subject of the email - Preheader that won\'t appear on the email.</i><br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_header_large_text_cash_paid',
		'Paid: Title',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_cash_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_header_large_text_cash_paid', 
			'label' => 	'Enter the title text for the header in paid cash invoices.<br>
			<code>Invoice tags supported</code>']
	);
	
	add_settings_field(
		'bookasession_invoices_header_subtitle_text_cash_paid',
		'Paid: Subtitle',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_cash_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_header_subtitle_text_cash_paid', 
			'label' => 	'Enter the subtitle text for the header in paid cash invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_table_title_cash_paid',
		'Paid: Table Title',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_cash_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_table_title_cash_paid', 
			'label' => 	'Enter the title text for the itemised table in paid cash invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_table_paragraph_cash_paid',
		'Paid: Table Paragraph',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_cash_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_table_paragraph_cash_paid', 
			'label' => 	'Enter the subtitle text for the header in paid cash invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_completed_cash_paid',
		'Paid: Completed Sentence',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_cash_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_completed_cash_paid', 
			'label' => 	'Enter an additional sentence to be included if a paid cash invoice is sent after their session(s) have been completed.<br>
			<code>Invoice tags supported</code>']
	);

	/* VFC Invoices */
	
	add_settings_field(
		'bookasession_invoices_vfc_receiving_mobile_number',
		'Vodafone Cash Mobile Number',
		'book_a_session_callback_field_text',
		'book_a_session_invoices',
		'book_a_session_section_invoice_vfc_settings',[ 
			'id'  	=>  'bookasession_invoices_vfc_receiving_mobile_number', 
			'label' => 	'Enter the number that will receive your Vodafone Cash payments.<br>
						<p class="description">This number can be inserted into Vodafone Cash invoices by typing <code>%vfcreceiver%</code> in any fields in the Vodafone Cash sections below that display <code>Invoice tags supported</code>, so customers know where to send their payments. We recommend using this tag instead of typing your number manually each time to save time and reduce the risk of errors, especially if you need to make changes to it in the future.</p>']
	);

	add_settings_field(
		'bookasession_invoices_vfc_logo',
		'Vodafone Cash Logo',
		'book_a_session_callback_field_radio',
		'book_a_session_invoices',
		'book_a_session_section_invoice_vfc_settings',[ 
			'id'  	=>  'bookasession_invoices_vfc_logo', 
			'description' => "Choose whether you'd like to display a logo for Vodafone Cash invoices or not.",
			'radio_options' => array(
				true => "Display a Vodafone Cash logo",
				false => "No logo" 
			)]
	);	
/*
	/* VFC Due */
		
	add_settings_field(
		'bookasession_invoices_email_subject_vfc_due',
		'Due: Email Subject',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_vfc_due_settings',[ 
			'id'  	=>  'bookasession_invoices_email_subject_vfc_due', 
			'label' => 	'Enter the subject of Vodafone Cash Due invoice emails.<br>
			<code>Invoice tags supported</code>']
	);	
	
	add_settings_field(
		'bookasession_invoices_reminder_email_subject_vfc_due',
		'Due: Reminder Email Subject',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_vfc_due_settings',[ 
			'id'  	=>  'bookasession_invoices_reminder_email_subject_vfc_due', 
			'label' => 	'Enter the subject of Vodafone Cash Due invoice reminder emails.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_hidden_preheader_vfc_due',
		'Due: Hidden Preheader',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_vfc_due_settings',[ 
			'id'  	=>  'bookasession_invoices_hidden_preheader_vfc_due', 
			'label' => 	'Enter a preheader that appears after the subject in email clients before opening an email, e.g. <i>Sender - Subject of the email - Preheader that won\'t appear on the email.</i><br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_header_large_text_vfc_due',
		'Due: Title',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_vfc_due_settings',[ 
			'id'  	=>  'bookasession_invoices_header_large_text_vfc_due', 
			'label' => 	'Enter the title text for the header in Vodafone Cash payment due invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_header_subtitle_text_vfc_due',
		'Due: Subtitle',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_vfc_due_settings',[ 
			'id'  	=>  'bookasession_invoices_header_subtitle_text_vfc_due', 
			'label' => 	'Enter the subtitle text for the header in Vodafone Cash due invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_table_title_vfc_due',
		'Due: Table Title',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_vfc_due_settings',[ 
			'id'  	=>  'bookasession_invoices_table_title_vfc_due', 
			'label' => 	'Enter the title text for the itemised table in Vodafone Cash due invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_table_paragraph_vfc_due',
		'Due: Table Paragraph',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_vfc_due_settings',[ 
			'id'  	=>  'bookasession_invoices_table_paragraph_vfc_due', 
			'label' => 	'Enter the subtitle text for the header in Vodafone Cash due invoices.<br>
			<code>Invoice tags supported</code>']
	);

	/* VFC Partly Paid */

	add_settings_field(
		'bookasession_invoices_email_subject_vfc_partly_paid',
		'Partly Paid: Email Subject',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_vfc_partly_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_email_subject_vfc_partly_paid', 
			'label' => 	'Enter the subject of Vodafone Cash partly paid invoice emails.<br>
			<code>Invoice tags supported</code>']
	);	
	
	add_settings_field(
		'bookasession_invoices_reminder_email_subject_vfc_partly_paid',
		'Partly Paid: Reminder Email Subject',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_vfc_partly_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_reminder_email_subject_vfc_partly_paid', 
			'label' => 	'Enter the subject of Vodafone Cash partly paid invoice reminder emails.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_hidden_preheader_vfc_partly_paid',
		'Partly Paid: Hidden Preheader',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_vfc_partly_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_hidden_preheader_vfc_partly_paid', 
			'label' => 	'Enter a preheader that appears after the subject in email clients before opening an email, e.g. <i>Sender - Subject of the email - Preheader that won\'t appear on the email.</i><br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_header_large_text_vfc_partly_paid',
		'Partly Paid: Title',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_vfc_partly_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_header_large_text_vfc_partly_paid', 
			'label' => 	'Enter the title text for the header in partly paid Vodafone Cash invoices.<br>
			<code>Invoice tags supported</code>']
	);
	add_settings_field(
		'bookasession_invoices_header_subtitle_text_vfc_partly_paid',
		'Partly Paid: Subtitle',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_vfc_partly_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_header_subtitle_text_vfc_partly_paid', 
			'label' => 	'Enter the subtitle text for the header in partly paid Vodafone Cash invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_table_title_vfc_partly_paid',
		'Partly Paid: Table Title',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_vfc_partly_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_table_title_vfc_partly_paid', 
			'label' => 	'Enter the title text for the itemised table in partly paid Vodafone Cash invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_table_paragraph_vfc_partly_paid',
		'Partly Paid: Table Paragraph',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_vfc_partly_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_table_paragraph_vfc_partly_paid', 
			'label' => 	'Enter the subtitle text for the header in partly paid Vodafone Cash invoices.<br>
			<code>Invoice tags supported</code>']
	);


	/* VFC Paid */

	add_settings_field(
		'bookasession_invoices_email_subject_vfc_paid',
		'Paid: Email Subject',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_vfc_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_email_subject_vfc_paid', 
			'label' => 	'Enter the subject of paid Vodafone Cash invoice emails.<br>
			<code>Invoice tags supported</code>']
	);	
	
	add_settings_field(
		'bookasession_invoices_reminder_email_subject_vfc_paid',
		'Paid: Reminder Email Subject',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_vfc_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_reminder_email_subject_vfc_paid', 
			'label' => 	'Enter the subject of paid Vodafone Cash invoice reminder emails.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_hidden_preheader_vfc_paid',
		'Paid: Hidden Preheader',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_vfc_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_hidden_preheader_vfc_paid', 
			'label' => 	'Enter a preheader that appears after the subject in email clients before opening an email, e.g. <i>Sender - Subject of the email - Preheader that won\'t appear on the email.</i><br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_header_large_text_vfc_paid',
		'Paid: Title',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_vfc_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_header_large_text_vfc_paid', 
			'label' => 	'Enter the title text for the header in paid Vodafone Cash invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_header_subtitle_text_vfc_paid',
		'Paid: Subtitle',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_vfc_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_header_subtitle_text_vfc_paid', 
			'label' => 	'Enter the subtitle text for the header in paid Vodafone Cash invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_table_title_vfc_paid',
		'Paid: Table Title',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_vfc_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_table_title_vfc_paid', 
			'label' => 	'Enter the title text for the itemised table in paid Vodafone Cash invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_table_paragraph_vfc_paid',
		'Paid: Table Paragraph',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_vfc_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_table_paragraph_vfc_paid', 
			'label' => 	'Enter the subtitle text for the header in paid Vodafone Cash invoices.<br>
			<code>Invoice tags supported</code>']
	);

	add_settings_field(
		'bookasession_invoices_completed_vfc_paid',
		'Paid: Completed Sentence',
		'book_a_session_callback_field_textarea',
		'book_a_session_invoices',
		'book_a_session_section_invoice_vfc_paid_settings',[ 
			'id'  	=>  'bookasession_invoices_completed_vfc_paid', 
			'label' => 	'Enter an additional sentence to be included if a paid Vodafone Cash invoice is sent after their session(s) have been completed.<br>
			<code>Invoice tags supported</code>']
	);

	// Email Template Options

	add_settings_field(
		'bookasession_emails_max_width',
		'Maximum Width',
		'book_a_session_callback_field_number',
		'book_a_session_emails',
		'book_a_session_section_email_settings',[ 
			'id'  	=>  'bookasession_emails_max_width', 
			'label' => 	'Enter the maximum width all emails should be, measured in pixels. Do not include px, only enter the number. The minimum value you can enter here is <code>320</code>.',
			'step'	=>	'1',
			'min'	=>	'320'
		]
	);

	add_settings_field(
		'bookasession_emails_logo_background_color',
		'Logo Background Color',
		'book_a_session_callback_field_text',
		'book_a_session_emails',
		'book_a_session_section_email_settings',[ 
			'id'  	=>  'bookasession_emails_logo_background_color', 
			'label' => 	'Enter a background color for the banner that will only contain your logo on all emails. This logo banner will only appear if you\'ve provided a logo URL in the General Settings section.']
	);

	add_settings_field(
		'bookasession_emails_header_background_color',
		'Header Background Color',
		'book_a_session_callback_field_text',
		'book_a_session_emails',
		'book_a_session_section_email_settings',[ 
			'id'  	=>  'bookasession_emails_header_background_color', 
			'label' => 	'Enter a background color for the email header that contains the main message of the email.
			<p class="description">This color is important as a fallback measure. If you choose a background image and a gradient, or both, they may not appear on all email clients. In the cases those effects aren\'t supported, this color appears instead. In the case that they do appear, or your gradient or image is transparent, this color will appear underneath.</p>']

	);

	add_settings_field(
		'bookasession_emails_logo_background_color',
		'Logo Background Color',
		'book_a_session_callback_field_text',
		'book_a_session_emails',
		'book_a_session_section_email_settings',[ 
			'id'  	=>  'bookasession_emails_logo_background_color', 
			'label' => 	'Enter a background color for the banner that will only contain your logo on all emails..']
	);

	add_settings_field(
		'bookasession_emails_header_background_image',
		'Header Background Image',
		'book_a_session_callback_field_radio',
		'book_a_session_emails',
		'book_a_session_section_email_settings',[ 
			'id'  					=> 'bookasession_emails_header_background_image', 
			'description' 			=> 'Choose whether you\'d like email headers to display a background image of a relevant Service image if applicable and available, a new image that you can specify below, or no image at all.',
			'radio_options'			=> array( 
				false				=> 'No Image',
				'service_image' 	=> 'Service Image',
				'new_image' 		=> 'New Image'
			),		
			'inline'				=> true	
			]
	);

	add_settings_field(
		'bookasession_emails_new_image_url',
		'New Image URL',
		'book_a_session_callback_field_text',
		'book_a_session_emails',
		'book_a_session_section_email_settings',[ 
			'id'  	=>  'bookasession_emails_new_image_url', 
			'label' => 	'If you chose to use a new image, enter the full URL of the new image, including http:// or https://.					
			<p class="description">To upload an image with WordPress, go to Media &gt; Add New, select your desired image and hit upload. 
			To find its URL, press Edit on the successful upload notification that will have appeared, or find the file in Media &gt; Library, and look for the File URL field at the top right. Copy the contents of that field and paste it here.']

	);

	add_settings_field(
		'bookasession_emails_header_background_screen_color',
		'Header Background Screen Color',
		'book_a_session_callback_field_text',
		'book_a_session_emails',
		'book_a_session_section_email_settings',[ 
			'id'  	=>  'bookasession_emails_header_background_screen_color', 
			'label' => 	'Enter an additional screen color for the email header that contains the main message of the email. This is usually a dark, semi transparent colour that allows you to read white text above an image more easily']

	);

	add_settings_field(
		'bookasession_emails_header_background_gradient',
		'Header Background Gradient',
		'book_a_session_callback_field_radio',
		'book_a_session_emails',
		'book_a_session_section_email_settings',[ 
			'id'  					=> 'bookasession_emails_header_background_gradient', 
			'description' 			=> 'Choose whether or not you\'d like a left to right background gradient on the header.',
			'radio_options'			=> array( 
				"true"				=> 'Yes',
				"false" 			=> 'No'
				)			
			]
	);

	add_settings_field(
		'bookasession_emails_header_background_gradient_left',
		'Header Background Gradient Color Left',
		'book_a_session_callback_field_text',
		'book_a_session_emails',
		'book_a_session_section_email_settings',[ 
			'id'  	=>  'bookasession_emails_header_background_gradient_left', 
			'label' => 	'Enter the color for the left of the gradient.']
	);

	add_settings_field(
		'bookasession_emails_header_background_gradient_right',
		'Header Background Gradient Color Right',
		'book_a_session_callback_field_text',
		'book_a_session_emails',
		'book_a_session_section_email_settings',[ 
			'id'  	=>  'bookasession_emails_header_background_gradient_right', 
			'label' => 	'Enter the color for the right of the gradient.']
	);
	

}

add_action( 'admin_init', 'book_a_session_register_settings' );
