<?php // Book A Session - Schedule Settings Page

if ( ! defined( 'ABSPATH' ) ) exit;

// Edit

if (   ! empty( $_GET['edit'] ) 
    && ! empty( $_GET['schedule_id'] )
    && ! empty( $_POST['submit_edit_schedule'] ) ) {
    
    if ( current_user_can( "manage_options" ) && wp_verify_nonce( $_REQUEST['_wpnonce'], 'book_a_session_edit_schedule_' . $_GET['schedule_id'] ) ) {

        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_schedule";
    
        // Validation
    
        if ( ! empty( $_POST['start_time'] ) && ! empty( $_POST['end_time'] ) ) {    

            $value_array = array( 
                'start_time'	=> $_POST['start_time'], 
                'end_time'      => $_POST['end_time']
			);
			
            $edit_schedule_db_result = $wpdb->update( 
                $table_name, 
                $value_array, 
                array( 'id' => $_GET['schedule_id'] ),
                array( 
                    '%s', 
                    '%s'
                    ) 
                );
            
            if ( $edit_schedule_db_result ) {
                add_action( 'admin_notices', 'book_a_session_admin_notice_edit_success' );
            } else {
                add_action( 'admin_notices', 'book_a_session_admin_notice_edit_error' );
            }   
            
        } else {
            add_action( 'admin_notices', 'book_a_session_admin_notice_validation_error' );
        }

    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_add_security_error' );
    }
        
}

// Delete
if ( isset( $_GET['delete'] ) && isset( $_GET['schedule_id'] ) && isset( $_GET['page'] ) ) {

    if ( current_user_can( 'manage_options' ) && $_GET['page'] == 'book_a_session_schedule' && wp_verify_nonce( $_REQUEST['_wpnonce'], "book_a_session_delete_schedule_" . $_GET['schedule_id'] ) ) {

        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_schedule";
        $delete_schedule_db_result = $wpdb->delete( $table_name, array( 'ID' => $_GET['schedule_id'] ), array( '%d' ) );
        $delete_schedule_db_result ? add_action( 'admin_notices', 'book_a_session_admin_notice_delete_success' ) : add_action( 'admin_notices', 'book_a_session_admin_notice_delete_error' );

    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_delete_security_error' );
    }

}

// Add
if ( isset( $_POST['submit_add_schedule'] ) ) {

    if ( current_user_can( 'manage_options' ) ) {
    
        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_schedule";

        if ( ! empty( $_POST['start_time'] ) && ! empty( $_POST['end_time'] ) ) {
            $value_array = array( 
                'start_time'	=> $_POST['start_time'], 
                'end_time'      => $_POST['end_time']
            );
            $add_schedule_db_result = $wpdb->insert( 
            $table_name, 
            $value_array, 
            array( 
                '%s', 
                '%s'
                ) 
            );
    
            if ( $add_schedule_db_result ) {
                add_action( 'admin_notices', 'book_a_session_admin_notice_add_success' );
            } else {
                add_action( 'admin_notices', 'book_a_session_admin_notice_add_error' );
            }   
      
        } else {
            add_action( 'admin_notices', 'book_a_session_admin_notice_validation_error' );
        }    
    
    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_add_security_error' );
    }

}

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
 }

if( ! class_exists( 'schedule_List_Table' ) ) {
    require_once plugin_dir_path( __FILE__ ) . 'class-schedule-list-table.php';
}

function book_a_session_display_schedule_settings_page() {
	if ( ! current_user_can( 'manage_options' ) ) return;

    ?>
	<div class="wrap">
		<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
		<?php 

function page_tabs_schedule( $current = 'View' ) {
    $tabs = array(
        'view'   => __( 'View schedule', 'book_a_session' ), 
		'add'  => __( 'Add session time', 'book_a_session' ),
		'edit'  => __( 'Edit session time', 'book_a_session' )
    );
    $html = '<h2 class="nav-tab-wrapper">';
    foreach( $tabs as $tab => $name ){
        $class = ( $tab == $current ) ? 'nav-tab-active' : '';
        $html .= '<a class="nav-tab ' . $class . '" href="?page=book_a_session_schedule&tab=' . $tab . '">' . $name . '</a>';
    }
    $html .= '</h2>';
    echo $html;
}

// Tabs
$tab = ( ! empty( $_GET['tab'] ) ) ? esc_attr( $_GET['tab'] ) : 'view';
page_tabs_schedule( $tab );

// View schedule
if ( $tab == 'view' ) {

	echo '<h3>View schedule</h3>';
	echo '<form>';
	$schedule_list = new Schedule_List_Table;
    echo '</form>';

}

// Add schedule
elseif ( $tab == 'add' ) {

	echo '<h2>Add a new session time</h2>';
	echo '<h4>Add a session time for each available slot during the day. Session times must not overlap, but one can end at the same time another begins.</h4>';
	echo '<p class="description">For example, 12:00-13:00, 13:00-14:00 and so on.</p>';
    $html =     "<table class='form-table'><form method='POST'>";
    // Start time input[type="time"] required
    $html .=    "<tr id='schedule_add_row_start_time'><th scope='row'>";
    $html .=    "<label for='start_time'>Session start time</label>";
    $html .=    "</th><td><input type='time' name='start_time' id='schedule_add_start_time' required placeholder='13:00' style='min-width:300px;'"; 
    if ( isset( $_POST['start_time'] ) ) {
        $html .= " value='" . $_POST['start_time'] . "'>";
    } else {
        $html .= ">";
    }
    if ( isset( $_POST['submit_add_schedule'] ) ) {
        if ( empty( $_POST['start_time'] ) ) {
          $html .= "<p class='description book-a-session-error'>Please enter the session's start time.</p>";
        }
	} 
	$html .= 	"<p class='description'>Please use 24 hour time, where 1pm is 13:00.</p>";
    $html .=    "</td></tr>";    
    // End time input[type="time"] required
    $html .=    "<tr id='schedule_add_row_end_time'><th scope='row'>";
    $html .=    "<label for='end_time'>Session end time</label>";
    $html .=    "</th><td><input type='time' name='end_time' id='schedule_add_end_time' required placeholder='14:00' style='min-width:300px;'"; 
    if ( isset( $_POST['end_time'] ) ) {
        $html .= " value='" . $_POST['end_time'] . "'>";
    } else {
        $html .= ">";
    }
    if ( isset( $_POST['submit_add_schedule'] ) ) {
        if ( empty( $_POST['end_time'] ) ) {
          $html .= "<p class='description book-a-session-error'>Please enter the session's end time.</p>";
        }
	} 
	$html .= 	"<p class='description'>Please use 24 hour time, where 1pm is 13:00.</p>";
    $html .=    "</td></tr>";  

    $html .=    "</tbody></table>";
    $html .=    "<input type='submit' class='button button-primary' value='Add session time' name='submit_add_schedule'>";
    $html .=    "</form>";

	
    echo $html;

} elseif ( $tab == 'edit' ) {

    if ( isset( $_GET["edit"] ) && ! empty( $_GET['schedule_id'] )  ) {

		echo '<h3>Edit a session time</h3>';
		global $wpdb;
		$table_name = $wpdb->prefix . "book_a_session_schedule";
		$id = intval( $_GET['schedule_id'] );

		$row_to_edit = $wpdb->get_row("
		SELECT  *
		FROM    $table_name
		WHERE   id = $id", OBJECT );

		$html =     "<table class='form-table'><form method='POST'>";

		echo '<h2>Add a new session time</h2>';
		echo '<h4>Add a session time for each available slot during the day. Session times must not overlap, but one can end at the same time another begins.</h4>';
		echo '<p class="description">For example, 12:00-13:00, 13:00-14:00 and so on.</p>';
		$html =     "<table class='form-table'><form method='POST'>";
		// Start time input[type="time"] required
		$html .=    "<tr id='schedule_add_row_start_time'><th scope='row'>";
		$html .=    "<label for='start_time'>Session start time</label>";
		$html .=    "</th><td><input type='time' name='start_time' id='schedule_add_start_time' required placeholder='13:00' style='min-width:300px;'"; 
		if ( isset( $row_to_edit->start_time ) ) {
			$html .= " value='" . $row_to_edit->start_time . "'>";
		} else {
			$html .= ">";
		}
		$html .= 	"<p class='description'>Please use 24 hour time, where 1pm is 13:00.</p>";
		$html .=    "</td></tr>";    
		// End time input[type="time"] required
		$html .=    "<tr id='schedule_add_row_end_time'><th scope='row'>";
		$html .=    "<label for='end_time'>Session end time</label>";
		$html .=    "</th><td><input type='time' name='end_time' id='schedule_add_end_time' required placeholder='14:00' style='min-width:300px;'"; 
		if ( isset( $row_to_edit->end_time ) ) {
			$html .= " value='" . $row_to_edit->end_time . "'>";
		} else {
			$html .= ">";
		}
		$html .= 	"<p class='description'>Please use 24 hour time, where 1pm is 13:00.</p>";
		$html .=    "</td></tr>";  
	
		$html .=    "</tbody></table>";
		$html .=    "<input type='submit' class='button button-primary' value='Save changes' name='submit_edit_schedule'>";
		$html .=    "</form>";

		echo $html;
		
	

    } else {

        echo '<h3>Select a session time to edit</h3>';
        echo '<form>';
        $schedule_list = new Schedule_List_Table;
        echo '</form>';

	}
	
} else {

	echo '<h3>View schedule</h3>';
	echo '<form>';
	$schedule_list = new Schedule_List_Table;
	echo '</form>';

}

		?>


	</div>

	<?php

}