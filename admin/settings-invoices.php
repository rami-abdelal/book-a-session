<?php // Book A Session - Settings Page

if ( ! defined( 'ABSPATH' ) ) exit;

// Delete
if ( isset( $_GET['delete'] ) && isset( $_GET['invoice_id'] ) && isset( $_GET['page'] ) ) {

    if ( current_user_can( 'manage_options' ) && $_GET['page'] == 'book_a_session_invoices' && wp_verify_nonce( $_REQUEST['_wpnonce'], "book_a_session_delete_invoice_" . $_GET['invoice_id'] ) ) {

        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_invoice";
        $delete_invoice_db_result[] = $wpdb->delete( $table_name, array( 'id' => $_GET['invoice_id'] ), array( '%d' ) );
        ! $delete_invoice_db_result ? add_action( 'admin_notices', 'book_a_session_admin_notice_delete_success' ) : add_action( 'admin_notices', 'book_a_session_admin_notice_delete_error' );

    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_delete_security_error' );
    }

}

if ( isset( $_GET['view'] ) && isset( $_GET['invoice_id']  ) )

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
 }

if( ! class_exists( 'Invoices_List_Table' ) ) {
    require_once plugin_dir_path( __FILE__ ) . 'class-invoices-list-table.php';
}

function book_a_session_display_invoices_settings_page() {
	if ( ! current_user_can( 'manage_options' ) ) return;

    ?>
	<div class="wrap">
		<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
		<?php 

	if ( isset( $_GET['view'] ) && isset( $_GET['invoice_id'] ) && isset( $_GET['page'] ) ) {

		if ( current_user_can( 'manage_options' ) && $_GET['page'] == 'book_a_session_invoices' && wp_verify_nonce( $_REQUEST['_wpnonce'], "book_a_session_view_invoice_" . $_GET['invoice_id'] ) ) {

			global $wpdb;
			$invoice_id = intval( $_GET['invoice_id'] );
			$table_name_invoice = $wpdb->prefix . "book_a_session_invoice";

			$invoice_row = $wpdb->get_row( "SELECT * FROM $table_name_invoice WHERE id = $invoice_id", OBJECT );

			if ( $invoice_row ) {

				$table_name_order = $wpdb->prefix . "book_a_session_order";
				$order_row = $wpdb->get_row( "SELECT * FROM $table_name_order WHERE order_id = $invoice_row->order_id", OBJECT );

				echo '<h2>Invoice for ' . book_a_session_get_user_name( $order_row->user_id ) . '</h2>';
				echo '<h4>Order ID: ' . $invoice_row->order_id . '</h4>';
				echo '<div style="border-radius:4px;box-shadow:0px 3px 20px rgba(0,0,0,.2);">';
				echo book_a_session_send_invoice( $_GET['invoice_id'], false, false, true );
				echo '</div>';
				echo '<br><br>';
				echo '<h3>View invoices</h3>';
				echo '<form>';
				$invoices_list = new Invoices_List_Table;
				echo '</form>';
				echo '</div>';
			
			} else {

				echo '<p class="description book-a-session-error">Error displaying this invoice. Please try again</p>';

				echo '<h3>View invoices</h3>';
				echo '<form>';
				$invoices_list = new Invoices_List_Table;
				echo '</form>';
				echo '</div>';
		
			}


		} else {

			add_action( 'admin_notices', 'book_a_session_admin_notice_view_security_error' );

		}

	} else {

		echo '<h3>View invoices</h3>';
		echo '<form>';
		$invoices_list = new Invoices_List_Table;
		echo '</form>';
		echo '</div>';
	
	}
	
}

