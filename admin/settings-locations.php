<?php // Book A Session - Locations Settings Page

if ( ! defined( 'ABSPATH' ) ) exit;

// Edit

if (   ! empty( $_GET['edit'] ) 
    && ! empty( $_GET['location_id'] )
    && ! empty( $_POST['submit_edit_location'] ) ) {

    if ( current_user_can( "manage_options" ) && wp_verify_nonce( $_REQUEST["_wpnonce"], "book_a_session_edit_location_" . $_GET["location_id"] ) ) {

        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_location";

        // Get all available resources to prepare for editing

        $schedule_array = book_a_session_get_table_array( "schedule" );
        $payment_method_array = book_a_session_get_table_array( "payment_method" );
        $currency_array = book_a_session_get_table_array( "currency" );
        $location_schedule_array = book_a_session_get_table_array( "location_schedule" );
        $location_payment_method_array = book_a_session_get_table_array( "location_payment_method" );
        $location_charge_array = book_a_session_get_table_array( "location_charge" );
        $table_name_location_schedule = $wpdb->prefix . "book_a_session_location_schedule";
        $table_name_location_payment_method = $wpdb->prefix . "book_a_session_location_payment_method";
        $table_name_location_charge = $wpdb->prefix . "book_a_session_location_charge";
        $edit_location_charge_db_result_array = array();
        $edit_location_payment_method_db_result_array = array();
        $edit_location_schedule_db_result_array = array();
        $edit_location_charge_result_flag = true;
        $edit_location_payment_method_result_flag = true;
        $edit_location_schedule_result_flag = true;
        $count_edit = 0;
        $count_no_edit = 0;
        
        // Edit Location
    
        if ( ! empty( $_POST['region_id'] ) && ! empty( $_POST['name'] ) ) {

            ! empty( $_POST['address_line_1'] ) ? $address1      =  $_POST['address_line_1']    : $address1      = null;
            ! empty( $_POST['address_line_2'] ) ? $address2      =  $_POST['address_line_2']    : $address2      = null;
            ! empty( $_POST['address_line_3'] ) ? $address3      =  $_POST['address_line_3']    : $address3      = null;
            ! empty( $_POST['city'] )           ? $city          =  $_POST['city']              : $city          = null;
            ! empty( $_POST['country'] )        ? $country       =  $_POST['country']           : $country       = null;
            ! empty( $_POST['postcode'] )       ? $postcode      =  $_POST['postcode']          : $postcode      = null;

            ! empty( $_POST['public_address'] ) ? $publicaddress = 1                            : $publicaddress = 0;
            ! empty( $_POST['accept_mon'] )     ? $accept_mon    = 1                            : $accept_mon    = 0;
            ! empty( $_POST['accept_tue'] )     ? $accept_tue    = 1                            : $accept_tue    = 0;
            ! empty( $_POST['accept_wed'] )     ? $accept_wed    = 1                            : $accept_wed    = 0;
            ! empty( $_POST['accept_thu'] )     ? $accept_thu    = 1                            : $accept_thu    = 0;
            ! empty( $_POST['accept_fri'] )     ? $accept_fri    = 1                            : $accept_fri    = 0;
            ! empty( $_POST['accept_sat'] )     ? $accept_sat    = 1                            : $accept_sat    = 0;
            ! empty( $_POST['accept_sun'] )     ? $accept_sun    = 1                            : $accept_sun    = 0;

            intval( $_POST['capacity'] ) > 0    ? $capacity      = intval( $_POST['capacity'] ) : $capacity      = 0;

            $value_array = array( 
                'region_id'         => $_POST['region_id'], 
                'name'              => $_POST['name'],
                'address_line_1'    => $address1,
                'address_line_2'    => $address2, 
                'address_line_3'    => $address3, 
                'city'              => $city,
                'country'           => $country, 
                'postcode_zipcode'  => $postcode, 
                'public_address'    => $publicaddress, 
                'accept_mon'        => $accept_mon, 
                'accept_tue'        => $accept_tue, 
                'accept_wed'        => $accept_wed, 
                'accept_thu'        => $accept_thu, 
                'accept_fri'        => $accept_fri, 
                'accept_sat'        => $accept_sat, 
                'accept_sun'        => $accept_sun,
                'tag'               => $_POST["tag"],
                'capacity'          => $capacity,
            );
            $edit_location_db_result = $wpdb->update( 
            $table_name, 
            $value_array, 
            array(
                'id' => $_GET['location_id']
            ),
            array( 
                '%d', // Region
                '%s', // Name
                '%s', // Address 1
                '%s', // Address 2
                '%s', // Address 3
                '%s', // City
                '%s', // Country
                '%s', // Postcode
                '%d', // Public Address
                '%d', // Mon
                '%d', // Tue
                '%d', // Wed
                '%d', // Thu
                '%d', // Fri
                '%d', // Sat
                '%d', // Sun   
                '%s', // Tag 
                '%d', // Capacity
                ) 
            );

            // Location Schedule Edit

            // Loop through session times

            for ( $i = 0; $i < count( $schedule_array ); $i++ ) {

                // When one is checked

                if ( ! empty( $_POST[ 'schedule_' . $schedule_array[$i]->id ] ) ) {

                    // Find out if it's already saved in the table

                    $match_found = false;

                    if ( ! empty( $location_schedule_array ) ) {

                        for ( $j = 0; $j < count( $location_schedule_array ); $j++ ) {

                            // If it's saved in the table, nothing needs to happen to it. Record that a match was found and count it to determine overall success later
    
                            if ( $location_schedule_array[$j]->location_id == $_GET['location_id'] && $location_schedule_array[$j]->schedule_id == $schedule_array[$i]->id ) {
                                $match_found = true;
                                $count_no_edit++;
                                break;
                            }
    
                        }

                    }

                    if ( ! $match_found ) {

                        $location_schedule_value_array = array( 
                            'location_id'   => $_GET['location_id'], 
                            'schedule_id'   => $schedule_array[$i]->id
                        );
    
                        $location_schedule_format_array = array(
                            '%d',
                            '%d'
                        );   
                        
                        $edit_location_schedule_db_result_array[] = $wpdb->insert( $table_name_location_schedule, $location_schedule_value_array, $location_schedule_format_array );
                        $count_edit++;
    
                    }

                } else {

                    // If it's not checked, find out if it's saved in the table

                    $match_found = false;

                    if ( ! empty( $location_schedule_array ) ) {

                        for ( $j = 0; $j < count( $location_schedule_array ); $j++ ) {

                            // If it's present, declare that a match was found, delete the record, count it as an edit to determine overall success later and break
    
                            if ( $location_schedule_array[$j]->location_id == $_GET['location_id'] && $location_schedule_array[$j]->schedule_id == $schedule_array[$i]->id ) {

                                $match_found = true;

                                $location_schedule_value_array = array( 
                                    'location_id'   => $_GET['location_id'], 
                                    'schedule_id'   => $schedule_array[$i]->id
                                );
            
                                $location_schedule_format_array = array(
                                    '%d',
                                    '%d'
                                );   
                                
                                $edit_location_schedule_db_result_array[] = $wpdb->delete( $table_name_location_schedule, $location_schedule_value_array, $location_schedule_format_array );  

                                $count_edit++;

                                break;

                            }
    
                        }

                    }

                    // If it's not present, count as no edit

                    if ( ! $match_found ) {

                        $count_no_edit++;
    
                    }

                }

            }

            // Location Payment Method Edit

            // Loop through payment methods

            for ( $i = 0; $i < count( $payment_method_array ); $i++ ) {

                // When one is checked

                if ( ! empty( $_POST[ 'payment_method_' . $payment_method_array[$i]->id ] ) ) {

                    // Find out if it's already saved in the table

                    $match_found = false;

                    if ( ! empty( $location_payment_method_array ) ) {

                        for ( $j = 0; $j < count( $location_payment_method_array ); $j++ ) {

                            // If it's saved in the table, nothing needs to happen to it. Record that a match was found and count it to determine overall success later
    
                            if ( $location_payment_method_array[$j]->location_id == $_GET['location_id'] && $location_payment_method_array[$j]->payment_method_id == $payment_method_array[$i]->id ) {
                                $match_found = true;
                                $count_no_edit++;
                                break;
                            }
    
                        }

                    }

                    if ( ! $match_found ) {

                        $location_payment_method_value_array = array( 
                            'location_id'       => $_GET['location_id'], 
                            'payment_method_id' => $payment_method_array[$i]->id
                        );
    
                        $location_payment_method_format_array = array(
                            '%d',
                            '%d'
                        );   
                        
                        $edit_location_payment_method_db_result_array[] = $wpdb->insert( $table_name_location_payment_method, $location_payment_method_value_array, $location_payment_method_format_array );
                        $count_edit++;
    
                    }

                } else {

                    // If it's not checked, find out if it's saved in the table

                    $match_found = false;

                    if ( ! empty( $location_payment_method_array ) ) {

                        for ( $j = 0; $j < count( $location_payment_method_array ); $j++ ) {

                            // If it's present, declare that a match was found, delete the record, count it as an edit to determine overall success later and break
    
                            if ( $location_payment_method_array[$j]->location_id == $_GET['location_id'] && $location_payment_method_array[$j]->payment_method_id == $payment_method_array[$i]->id ) {

                                $match_found = true;

                                $location_payment_method_value_array = array( 
                                    'location_id'   => $_GET['location_id'], 
                                    'payment_method_id'   => $payment_method_array[$i]->id
                                );
            
                                $location_payment_method_format_array = array(
                                    '%d',
                                    '%d'
                                );   
                                
                                $edit_location_payment_method_db_result_array[] = $wpdb->delete( $table_name_location_payment_method, $location_payment_method_value_array, $location_payment_method_format_array );  

                                $count_edit++;

                                break;

                            }
    
                        }

                    }

                    // If it's not present, count as no edit

                    if ( ! $match_found ) {

                        $count_no_edit++;
    
                    }

                }

            }

            // Location Charge Edit

            // Loop through currencies

            for ( $i = 0; $i < count( $currency_array ); $i++ ) {

                // When one isn't empty

                if ( ! empty( $_POST[ 'currency_' . $currency_array[$i]->id ] ) ) {

                    // Find out if it's already saved in the table

                    $match_found = false;

                    if ( ! empty( $location_charge_array ) ) {

                        for ( $j = 0; $j < count( $location_charge_array ); $j++ ) {

                            // If it's saved in the table, update it. Record that a match was found and count it to determine overall success later
    
                            if ( $location_charge_array[$j]->location_id == $_GET['location_id'] && $location_charge_array[$j]->currency_id == $currency_array[$i]->id ) {

                                $match_found = true;

                                $location_charge_where_array = array(
                                    'location_id'       => $_GET['location_id'], 
                                    'currency_id'       => $currency_array[$i]->id
                                );

                                $location_charge_where_format_array = array(
                                    '%d',
                                    '%d'
                                );   

                                $location_charge_value_array = array( 
                                    'addition_charge'   => $_POST[ 'currency_' . $currency_array[$i]->id ]
                                );
            
                                $location_charge_value_format_array = array(
                                    '%f'
                                );   
                                
                                $edit_location_charge_db_result_array[] = $wpdb->update( 
                                    $table_name_location_charge, 
                                    $location_charge_value_array, 
                                    $location_charge_where_array, 
                                    $location_charge_value_format_array, 
                                    $location_charge_where_format_array 
                                );
        
                                $count_edit++;

                                break;
                            }
    
                        }

                    }

                    // If it doesn't already exist, insert

                    if ( ! $match_found ) {

                        $location_charge_value_array = array( 
                            'location_id'       => $_GET['location_id'], 
                            'currency_id'       => $currency_array[$i]->id,
                            'addition_charge'   => $_POST[ 'currency_' . $currency_array[$i]->id ]
                        );
    
                        $location_charge_format_array = array(
                            '%d',
                            '%d',
                            '%f'
                        );   
                        
                        $edit_location_charge_db_result_array[] = $wpdb->insert( $table_name_location_charge, $location_charge_value_array, $location_charge_format_array );
                        $count_edit++;
    
                    }

                } else {

                    // If there's no value posted, find out if it's saved in the table

                    $match_found = false;

                    // If there're no location charges there's nothing to delete

                    if ( ! empty( $location_charge_array ) ) {

                        for ( $j = 0; $j < count( $location_charge_array ); $j++ ) {

                            // But if there are and if it's present, declare that a match was found, delete the record, count it as an edit to determine overall success later and break
    
                            if ( $location_charge_array[$j]->location_id == $_GET['location_id'] && $location_charge_array[$j]->currency_id == $currency_array[$i]->id ) {

                                $match_found = true;

                                $location_charge_where_array = array( 
                                    'location_id'   => $_GET['location_id'], 
                                    'currency_id'   => $currency_array[$i]->id
                                );
            
                                $location_charge_format_array = array(
                                    '%d',
                                    '%d'
                                );   
                                
                                $edit_location_charge_db_result_array[] = $wpdb->delete( $table_name_location_charge, $location_charge_where_array, $location_charge_format_array );  

                                $count_edit++;

                                break;

                            }
    
                        }

                    }

                    // If it's not present, count as no edit

                    if ( ! $match_found ) {

                        $count_no_edit++;
    
                    }

                }

            }

            if ( $edit_location_db_result == 0 ) {
                $edit_location_db_result = true;
            }
            $location_schedule_result_flag;
            $location_payment_method_result_flag;
            $location_charge_result_flag;

            foreach ( $edit_location_schedule_db_result_array as $schedule_result ) {
                if ( empty( $schedule_result ) ) { $location_schedule_result_flag = false; break; }
            }

            unset( $schedule_result );

            foreach ( $edit_location_payment_method_db_result_array as $payment_method_result ) {
                if ( empty( $payment_method_result ) ) { $location_payment_method_result_flag = false; break; }
            }

            unset( $payment_method_result );

            foreach ( $edit_location_charge_db_result_array as $charge_result ) {
                if ( empty( $charge_result ) ) { $location_charge_result_flag = false; break; }
            }

            unset( $charge_result );


            $all_edit_result_array[] = array(
                $edit_location_schedule_result_flag,          
                $edit_location_payment_method_result_flag,    
                $edit_location_charge_result_flag,            
                $edit_location_db_result
                );
                

            // If all changes were saved without issue
            if ( book_a_session_all_truthy_or_falsey( $all_edit_result_array ) ) {
                add_action( 'admin_notices', 'book_a_session_admin_notice_edit_multiple_success' );
            // If there's a mix of success and failure
            } else if ( in_array( true, $all_edit_result_array, true ) && in_array( false, $all_edit_result_array, true ) ) {
                add_action( 'admin_notices', 'book_a_session_admin_notice_edit_multiple_warning' );
            } else {
                add_action( 'admin_notices', 'book_a_session_admin_notice_edit_error' );            
            }

        } else {
            add_action( 'admin_notices', 'book_a_session_admin_notice_validation_error' );
        }
                 
    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_edit_security_error' );
    }

}

// Delete
if ( isset( $_GET['delete'] ) && isset( $_GET['location_id'] ) ) {

    if ( $_GET['page'] == 'book_a_session_locations' && current_user_can( "manage_options" ) && wp_verify_nonce( $_REQUEST["_wpnonce"], "book_a_session_delete_location_" . $_GET["location_id"] ) ) {

        global $wpdb;
        // Delete Location
        $table_name_location = $wpdb->prefix . "book_a_session_location";
        $delete_location_db_result[] = $wpdb->delete( $table_name_location, array( 'id' => $_GET['location_id'] ), array( '%d' ) );
        // Delete Location Schedule
        $table_name_location_schedule = $wpdb->prefix . "book_a_session_location_schedule";
        $delete_location_db_result[] = $wpdb->delete( $table_name_location_schedule, array( 'location_id' => $_GET['location_id'] ), array( '%d' ) );
        // Delete Location Payment Methods
        $table_name_location_payment_method = $wpdb->prefix . "book_a_session_location_payment_method";
        $delete_location_db_result[] = $wpdb->delete( $table_name_location_payment_method, array( 'location_id' => $_GET['location_id'] ), array( '%d' ) );
        // Delete Location Charges
        $table_name_location_charge = $wpdb->prefix . "book_a_session_location_charge";
        $delete_location_db_result[] = $wpdb->delete( $table_name_location_charge, array( 'location_id' => $_GET['location_id'] ), array( '%d' ) );
        // Service Locations
        $table_name_service_location = $wpdb->prefix . "book_a_session_service_location";
        $delete_location_db_result[] = $wpdb->delete( $table_name_service_location, array( 'location_id' => $_GET['location_id'] ), array( '%d' ) );

        ! in_array( false, $delete_location_db_result, true ) ? add_action( 'admin_notices', 'book_a_session_admin_notice_delete_success' ) : add_action( 'admin_notices', 'book_a_session_admin_notice_delete_error' );

    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_delete_security_error' );
    }

}

// Add
if ( isset( $_POST['submit_add_location'] ) ) {

    if ( current_user_can( "manage_options" ) ) {

        // Prepare insertion

        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_location";

        // Get all available resources
        $schedule_array = book_a_session_get_table_array( "schedule" );
        $payment_method_array = book_a_session_get_table_array( "payment_method" );
        $currency_array = book_a_session_get_table_array( "currency" );
        $add_location_charge_db_result_array = array();
        $add_location_payment_method_db_result_array = array();
        $add_location_schedule_db_result_array = array();
        $location_charge_result_flag = true;
        $location_payment_method_result_flag = true;
        $location_schedule_result_flag = true;
            
        // Validation for adding Location

        if ( ! empty( $_POST['region_id'] ) && ! empty( $_POST['name'] ) ) {

            ! empty( $_POST['address_line_1'] )  ? $address1      =  $_POST['address_line_1']    : $address1      = null;
            ! empty( $_POST['address_line_2'] )  ? $address2      =  $_POST['address_line_2']    : $address2      = null;
            ! empty( $_POST['address_line_3'] )  ? $address3      =  $_POST['address_line_3']    : $address3      = null;
            ! empty( $_POST['city'] )            ? $city          =  $_POST['city']              : $city          = null;
            ! empty( $_POST['country'] )         ? $country       =  $_POST['country']           : $country       = null;
            ! empty( $_POST['postcode'] )        ? $postcode      =  $_POST['postcode']          : $postcode      = null;

            ! empty( $_POST['public_address'] )  ? $publicaddress = 1                            : $publicaddress = 0;
            ! empty( $_POST['accept_mon'] )      ? $accept_mon    = 1                            : $accept_mon    = 0;
            ! empty( $_POST['accept_tue'] )      ? $accept_tue    = 1                            : $accept_tue    = 0;
            ! empty( $_POST['accept_wed'] )      ? $accept_wed    = 1                            : $accept_wed    = 0;
            ! empty( $_POST['accept_thu'] )      ? $accept_thu    = 1                            : $accept_thu    = 0;
            ! empty( $_POST['accept_fri'] )      ? $accept_fri    = 1                            : $accept_fri    = 0;
            ! empty( $_POST['accept_sat'] )      ? $accept_sat    = 1                            : $accept_sat    = 0;
            ! empty( $_POST['accept_sun'] )      ? $accept_sun    = 1                            : $accept_sun    = 0;

            intval( $_POST['capacity'] ) > 0     ? $capacity      = intval( $_POST['capacity'] ) : $capacity      = 0;

            $value_array = array( 
                'region_id'         => $_POST['region_id'], 
                'name'              => $_POST['name'],
                'address_line_1'    => $address1,
                'address_line_2'    => $address2, 
                'address_line_3'    => $address3, 
                'city'              => $city,
                'country'           => $country, 
                'postcode_zipcode'  => $postcode, 
                'public_address'    => $publicaddress, 
                'accept_mon'        => $accept_mon, 
                'accept_tue'        => $accept_tue, 
                'accept_wed'        => $accept_wed, 
                'accept_thu'        => $accept_thu, 
                'accept_fri'        => $accept_fri, 
                'accept_sat'        => $accept_sat, 
                'accept_sun'        => $accept_sun,
                'tag'               => $_POST["tag"],
                'capacity'          => $capacity,
            );
            $add_location_db_result = $wpdb->insert( 
            $table_name, 
            $value_array, 
            array( 
                '%d', // Region
                '%s', // Name
                '%s', // Address 1
                '%s', // Address 2
                '%s', // Address 3
                '%s', // City
                '%s', // Country
                '%s', // Postcode
                '%d', // Public Address
                '%d', // Mon
                '%d', // Tue
                '%d', // Wed
                '%d', // Thu
                '%d', // Fri
                '%d', // Sat
                '%d', // Sun 
                '%s', // Tag   
                '%d', // Capacity
                ) 
            );

            // On successful addition

            if ( ! empty( $add_location_db_result ) ) {

                // Get the newly added location's id and use it to populate other tables where applicable

                $added_location = $wpdb->get_row( "SELECT id from $table_name ORDER BY id DESC LIMIT 1", OBJECT );
                $added_location_id = $added_location->id;

                // Location Schedule Insertion

                $table_name_location_schedule = $wpdb->prefix . "book_a_session_location_schedule";

                for ( $i = 0; $i < count( $schedule_array ); $i++ ) {

                    if ( ! empty( $_POST[ 'schedule_' . $schedule_array[$i]->id ] ) ) {

                        $location_schedule_value_array = array( 
                            'location_id'   => $added_location_id, 
                            'schedule_id'   => $schedule_array[$i]->id
                        );

                        $location_schedule_format_array = array(
                            '%d',
                            '%d'
                        ); 

                        $add_location_schedule_db_result_array[] = $wpdb->insert( $table_name_location_schedule, $location_schedule_value_array, $location_schedule_format_array );
    
                    }

                }

                // Location Payment Method Insertion

                $table_name_location_payment_method = $wpdb->prefix . "book_a_session_location_payment_method";

                for ( $i = 0; $i < count( $payment_method_array ); $i++ ) {

                    if ( ! empty( $_POST[ 'payment_method_' . $payment_method_array[$i]->id ] ) ) {

                        $location_payment_method_value_array = array( 
                            'location_id'       => $added_location_id, 
                            'payment_method_id' => $payment_method_array[$i]->id
                        );

                        $location_payment_method_format_array = array(
                            '%d',
                            '%d'
                        );   

                        $add_location_payment_method_db_result_array[] = $wpdb->insert( $table_name_location_payment_method, $location_payment_method_value_array, $location_payment_method_format_array );
    
                    }

                }

                // Location Charge Insertion

                $table_name_location_charge = $wpdb->prefix . "book_a_session_location_charge";

                for ( $i = 0; $i < count( $currency_array ); $i++ ) {

                    if ( ! empty( $_POST[ 'currency_' . $currency_array[$i]->id ] ) ) {

                        $location_charge_value_array = array( 
                            'location_id'       => $added_location_id, 
                            'currency_id'       => $currency_array[$i]->id, 
                            'addition_charge'   => $_POST[ 'currency_' . $currency_array[$i]->id ] 
                        );

                        $location_charge_format_array = array(
                            '%d',
                            '%d',
                            '%f'
                        );   

                        $add_location_charge_db_result_array[] = $wpdb->insert( $table_name_location_charge, $location_charge_value_array, $location_charge_format_array );
    
                    }

                }

            } else {
                // If the location wasn't added, don't try to add the extras, just show an error.
                add_action( 'admin_notices', 'book_a_session_admin_notice_add_error' );
            }
    
        } else {
            add_action( 'admin_notices', 'book_a_session_admin_notice_validation_error' );
        }     

        foreach ( $add_location_schedule_db_result_array as $schedule_result ) {
            if ( empty( $schedule_result ) ) { $location_schedule_result_flag = false; break; }
        }

        unset( $schedule_result );

        foreach ( $add_location_payment_method_db_result_array as $payment_method_result ) {
            if ( empty( $payment_method_result ) ) { $location_payment_method_result_flag = false; break; }
        }

        unset( $payment_method_result );

        foreach ( $add_location_charge_db_result_array as $charge_result ) {
            if ( empty( $charge_result ) ) { $location_charge_result_flag = false; break; }
        }

        unset( $charge_result );

        $all_result_array[] = array(
            $location_schedule_result_flag,          
            $location_payment_method_result_flag,    
            $location_charge_result_flag,            
            $add_location_db_result 
            );
                

        // If all changes were saved without issue
        if ( book_a_session_all_truthy_or_falsey( $all_result_array ) ) {
            add_action( 'admin_notices', 'book_a_session_admin_notice_add_multiple_success' );
        // If there's a mix of success and failure
        } else if ( in_array( true, $all_result_array, true ) && in_array( false, $all_result_array, true ) ) {
            add_action( 'admin_notices', 'book_a_session_admin_notice_add_multiple_warning' );
        } else {
            add_action( 'admin_notices', 'book_a_session_admin_notice_add_error' );            
        }

    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_add_security_error' );
    }

}

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
 }

if( ! class_exists( 'Locations_List_Table' ) ) {
    require_once plugin_dir_path( __FILE__ ) . 'class-locations-list-table.php';
}

function book_a_session_display_locations_settings_page() {
	if ( ! current_user_can( 'manage_options' ) ) return;

    ?>
	<div class="wrap">
		<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
		<?php 

function page_tabs_location( $current = 'View' ) {
    $tabs = array(
        'view'   => __( 'View locations', 'book_a_session' ), 
		'add'  => __( 'Add location', 'book_a_session' ),
		'edit'  => __( 'Edit locations', 'book_a_session' )
    );
    $html = '<h2 class="nav-tab-wrapper">';
    foreach( $tabs as $tab => $name ){
        $class = ( $tab == $current ) ? 'nav-tab-active' : '';
        $html .= '<a class="nav-tab ' . $class . '" href="?page=book_a_session_locations&tab=' . $tab . '">' . $name . '</a>';
    }
    $html .= '</h2>';
    echo $html;
}

// Tabs
$tab = ( ! empty( $_GET['tab'] ) ) ? esc_attr( $_GET['tab'] ) : 'view';
page_tabs_location( $tab );

// View locations
if ( $tab == 'view' ) {

	echo '<h3>View locations</h3>';
	echo '<form>';
	$locations_list = new Locations_List_Table;
    echo '</form>';

}

// Add location
elseif ( $tab == 'add' ) {

    $html =     "<table class='form-table'><form method='POST'>";
    $html .=    "<tbody>";
    $html .=    "<tr><th scope='row' colspan='2'><h2>Add a new Location</h2><p class='description'>Where will your sessions take place? You can set up your office, a park, or even simply 'Online' as a location for your clients to choose from.</p></th></tr>";
    // Name input[type="text"] required
    $html .=    "<tr id='region_add_row_name'><th scope='row'>";
    $html .=    "<label for='name'>Name</label>";
    $html .=    "</th><td><input type='text' name='name' id='region_add_name' required placeholder='e.g. Office, Starbucks, Gym' style='min-width:300px;'"; 
    if ( isset( $_POST['name'] ) ) {
        $html .= " value='" . $_POST['name'] . "'>";
    } else {
        $html .= ">";
    }
    $html .=    "</td></tr>";
    // Region select required
    $html .=    "<tr id='location_add_row_region_id'><th scope='row'>";
    $html .=    "<label for='region_id'>Region</label></th>";
    $html .=    "<td><select name='region_id' id='location_add_region_id' required>";
    $region_array = book_a_session_get_table_array( "region" );
    if ( ! empty( $region_array ) ) {
        if ( empty( $_POST['region_id'] ) ) {
            $html .= "<option selected disabled>Select your location's region</option>";
        } else {
            $html .= "<option disabled>Select your location's region</option>";
        }
        for ($i = 0; $i < count($region_array); $i++) {
            $html .= "<option value='" . $region_array[$i]->id . "'";
            if ( isset( $_POST['submit_add_location'] ) ) {
                if ( ! empty( $_POST['region_id'] ) ) {
                    if ( $_POST['region_id'] == $region_array[$i]->id ) {
                        $html .= " selected";
                    } 
                }
            }
            $html .= ">";
            $html .= $region_array[$i]->name . ". Timezone: " . $region_array[$i]->timezone . ". Session price: " . book_a_session_get_price_tag( $region_array[$i]->session_price, $region_array[$i]->currency_id ) . ".";
            $html .= "</option>";
        }
        $html .= "</select>";
        if ( isset( $_POST['submit_add_location'] ) && empty( $_POST['region_id'] ) ) {
            $html .= "<p class='description book-a-session-error'>Please select a valid region.</p>";
        }
        $html .= "</td>";
    } else {
        $html .= "<option selected disabled>No regions found</option>";
        $html .= "</select><br><p class='description'>Add a Region in the Regions settings page</p></td>";
    }  

    // Capacity input[type="number"] step="1" min="0" optional
    $html .=    "<tr id='location_add_row_capacity'><th scope='row'>";
    $html .=    "<label for='capacity'>Capacity</label></th>";
    $html .=    "<td><input type='number' step='1' min='0' name='capacity' id='location_add_row_capacity' placeholder='Enter 0 for unlimited capacity' style='min-width:300px;'";
    if ( isset( $_POST[ "capacity" ] ) ) {
        $html .= " value='" . $_POST[ "capacity" ] . "'>";
    } else {
        $html .= ">";
    }
    $html .=    "<p class='description'>Enter the number of sessions this location can host at any given time, or 0 for unlimited capacity.</p></td></tr>";
      
    // Frontend Tag input[type="text"] optional
    $html .=    "<tr id='location_add_tag'><th scope='row'>";
    $html .=    "<label for='tag'>Frontend Tag (Optional)</label>";
    $html .=    "</th><td><input type='text' name='tag' id='location_add_tag' placeholder='Frontend Tag' style='min-width:300px;'"; 
    if ( isset( $_POST['tag'] ) ) {
        $html .= " value='" . $_POST['tag'] . "'>";
    } else {
        $html .= ">";
    }
    $html .=    "</td></tr>";

    $html .=    "<tr><th scope='row' colspan='2'><h3>Address</h3><p class='description'>Enter the address of this location. You don't need to do this for an online or phone location</p></th></tr>";
    // Address Line 1 input[type="text"] optional
    $html .=    "<tr id='location_add_row_address_line_1'><th scope='row'>";
    $html .=    "<label for='address_line_1'>Address Line 1 (Optional)</label>";
    $html .=    "</th><td><input type='text' name='address_line_1' id='location_add_address_line_1' placeholder='First line' style='min-width:300px;'"; 
    if ( isset( $_POST['address_line_1'] ) ) {
        $html .= " value='" . $_POST['address_line_1'] . "'>";
    } else {
        $html .= ">";
    }
    $html .=    "</td></tr>";
    // Address Line 2 input[type="text"] optional
    $html .=    "<tr id='location_add_row_address_line_2'><th scope='row'>";
    $html .=    "<label for='address_line_2'>Address Line 2 (Optional)</label>";
    $html .=    "</th><td><input type='text' name='address_line_2' id='location_add_address_line_2' placeholder='Second line' style='min-width:300px;'"; 
    if ( isset( $_POST['address_line_2'] ) ) {
        $html .= " value='" . $_POST['address_line_2'] . "'>";
    } else {
        $html .= ">";
    }
    $html .=    "</td></tr>";
    // Address Line 3 input[type="text"] optional
    $html .=    "<tr id='location_add_row_address_line_3'><th scope='row'>";
    $html .=    "<label for='address_line_3'>Address Line 3 (Optional)</label>";
    $html .=    "</th><td><input type='text' name='address_line_3' id='location_add_address_line_3' placeholder='Third line' style='min-width:300px;'"; 
    if ( isset( $_POST['address_line_3'] ) ) {
        $html .= " value='" . $_POST['address_line_3'] . "'>";
    } else {
        $html .= ">";
    }
    $html .=    "</td></tr>";
    // Postcode input[type="text"] optional
    $html .=    "<tr id='location_add_row_postcode'><th scope='row'>";
    $html .=    "<label for='postcode'>Postcode (Optional)</label>";
    $html .=    "</th><td><input type='text' name='postcode' id='location_add_postcode' placeholder='Postcode or ZIP code' style='min-width:300px;'"; 
    if ( isset( $_POST['postcode'] ) ) {
        $html .= " value='" . $_POST['postcode'] . "'>";
    } else {
        $html .= ">";
    }
    $html .=    "</td></tr>";
    // City input[type="text"] optional
    $html .=    "<tr id='location_add_row_city'><th scope='row'>";
    $html .=    "<label for='city'>City (Optional)</label>";
    $html .=    "</th><td><input type='text' name='city' id='location_add_city' placeholder='City' style='min-width:300px;'"; 
    if ( isset( $_POST['city'] ) ) {
        $html .= " value='" . $_POST['city'] . "'>";
    } else {
        $html .= ">";
    }
    $html .=    "</td></tr>";
    // Country input[type="text"] optional
    $html .=    "<tr id='location_add_row_country'><th scope='row'>";
    $html .=    "<label for='country'>Country (Optional)</label>";
    $html .=    "</th><td><input type='text' name='country' id='location_add_country' placeholder='Country' style='min-width:300px;'"; 
    if ( isset( $_POST['country'] ) ) {
        $html .= " value='" . $_POST['country'] . "'>";
    } else {
        $html .= ">";
    }
    $html .=    "</td></tr>";
    // Public Address checkbox
    $html .=    "<tr id='location_add_row_public_address'><th scope='row'>";
    $html .=    "<label for='public_address'>Public Address? (Optional)</label>";
    $html .=    "</th><td><label for='public_address'><input type='checkbox' id='public_address' name='public_address' value='1'"; 
    if ( ! empty( $_POST['public_address'] ) ) {
        $html .= " checked>";
    } else {
        $html .= ">";
    }
    $html .=    "Check to display this address openly to all. Uncheck if it's private.</label>";
    $html .=    "<p class='description'>Private addresses are shown only to those who've booked a session at this location.</p>";    
    $html .=    "</td></tr>";
    $html .=    "<tr><th scope='row' colspan='2'><h3>Schedule</h3><p class='description'>Add this location's daily and weekly availability. You can customise your daily schedule's session times and weekly schedule on a site-wide level in the Schedule settings page.</p></th></tr>";
    $schedule_array = book_a_session_get_table_array( "schedule", "start_time", "ASC", "id" );

    // Daily Schedule checkbox
    $html .=    "<tr id='location_add_row_location_schedule'><th scope='col'>";
    $html .=    "<label for='location_schedule'>Daily Schedule</label>";
    $html .=    "<br><a href='javascript:void(0)' class='select-all'>Select all</a>&nbsp;&nbsp;<a href='javascript:void(0)' class='unselect-all'>Unselect all</a>";
    $html .=    "</th><td>";
    for ($i = 0; $i < count( $schedule_array ); $i++) {
            $html .=    "<label for='schedule_" . $schedule_array[$i]->id . "'><input type='checkbox' id='schedule_" . $schedule_array[$i]->id . "' name='schedule_" . $schedule_array[$i]->id . "' value='1'"; 
        if ( empty( $_POST['schedule_' . $schedule_array[$i]->id ] ) ) {
            $html .= ">";
        } else {
            $html .= " checked>";
        }
        $html .=    book_a_session_get_session_time( $schedule_array[$i]->id )["time_string"] . "</label>&nbsp;&nbsp;&nbsp;";   
    }
    $html .=    "<p class='description'>Check session times available at this location.</p>";    
    $html .=    "</td></tr>";
    // Weekly Schedule checkbox
    $html .=    "<tr id='location_add_row_location_week'><th scope='row'>";
    $html .=    "<label for='location_week'>Weekly Schedule</label>";
    $html .=    "<br><a href='javascript:void(0)' class='select-all'>Select all</a>&nbsp;&nbsp;<a href='javascript:void(0)' class='unselect-all'>Unselect all</a>";
    $html .=    "</th><td>";
    // Mon
    $html .=    "<label for='accept_mon'><input type='checkbox' id='accept_mon' name='accept_mon' value='1'"; 
        if ( empty( $_POST['accept_mon'] ) ) {
            $html .= ">";
        } else {
            $html .= " checked >";
        }
    $html .=    "Mon</label>&nbsp;&nbsp;&nbsp;";   
    // Tue
    $html .=    "<label for='accept_tue'><input type='checkbox' id='accept_tue' name='accept_tue' value='1'"; 
        if ( empty( $_POST['accept_tue'] ) ) {
            $html .= ">";
        } else {
            $html .= " checked>";
        }
    $html .=    "Tue</label>&nbsp;&nbsp;&nbsp;"; 
    // Wed
    $html .=    "<label for='accept_wed'><input type='checkbox' id='accept_wed' name='accept_wed' value='1'"; 
        if ( empty( $_POST['accept_wed'] ) ) {
            $html .= ">";
        } else {
            $html .= " checked>";
        }
     $html .=    "Wed</label>&nbsp;&nbsp;&nbsp;";
    // Thu
    $html .=    "<label for='accept_thu'><input type='checkbox' id='accept_thu' name='accept_thu' value='1'"; 
        if ( empty( $_POST['accept_thu'] ) ) {
            $html .= ">";
        } else {
            $html .= " checked>";
        }
    $html .=    "Thu</label>&nbsp;&nbsp;&nbsp;";   
    // Fri
    $html .=    "<label for='accept_fri'><input type='checkbox' id='accept_fri' name='accept_fri' value='1'"; 
        if ( empty( $_POST['accept_fri'] ) ) {
            $html .= ">";
        } else {
            $html .= " checked>";
        }
    $html .=    "Fri</label>&nbsp;&nbsp;&nbsp;";   
    // Sat
    $html .=    "<label for='accept_sat'><input type='checkbox' id='accept_sat' name='accept_sat' value='1'"; 
        if ( empty( $_POST['accept_sat'] ) ) {
            $html .= ">";
        } else {
            $html .= " checked>";
        }
    $html .=    "Sat</label>&nbsp;&nbsp;&nbsp;";     
    // Sun
    $html .=    "<label for='accept_sun'><input type='checkbox' id='accept_sun' name='accept_sun' value='1'"; 
        if ( empty( $_POST['accept_sun'] ) ) {
            $html .= ">";
        } else {
            $html .= " checked>";
        }
    $html .=    "Sun</label>";
    $html .=    "<p class='description'>Check days available at this location.</p>";    
    $html .=    "</td></tr>";
    // Payment methods checkbox
    $html .=    "<tr><th scope='row' colspan='2'><h3>Payment Methods</h3><p class='description'>Check the payment methods you accept for orders at this location.</p>";
    $html .=    "<a href='javascript:void(0)' class='select-all-payment-method'>Select all</a>&nbsp;&nbsp;<a href='javascript:void(0)' class='unselect-all-payment-method'>Unselect all</a>";
    $html .=    "</th></tr>";
    $payment_method_array = book_a_session_get_table_array( "payment_method" );
    for ($i = 0; $i < count( $payment_method_array ); $i++) {
        $html .=    "<tr id='location_add_row_payment_method_" . $payment_method_array[$i]->id . "' class='payment-method'><th scope='row'>";
        $html .=    "<label for='payment_method_" . $payment_method_array[$i]->id . "'>" . $payment_method_array[$i]->name . "</label>";
        $html .=    "</th><td>";
            $html .=    "<label for='payment_method_" . $payment_method_array[$i]->id . "'><input type='checkbox' id='payment_method_" . $payment_method_array[$i]->id . "' name='payment_method_" . $payment_method_array[$i]->id . "' value='1'"; 
        if ( ! empty( $_POST['payment_method_' . $payment_method_array[$i]->id ] ) ) {
            $html .= " checked>";
        } else {
            $html .= ">";
        }
        $html .=    "Check to accept " . $payment_method_array[$i]->name  . " for this location.</label>";  
        $html .=    "</td></tr>"; 
    }
    // Location charge optional
    $html .=    "<tr><th scope='row' colspan='2'><h3>Extra Charges</h3><p class='description'>To charge extra for this location, enter an additional charge below for any applicable currencies.</p></th></tr>";
    $currency_array = book_a_session_get_table_array( "currency" );
    for ( $i = 0; $i < count(  $currency_array ); $i++ ){

        // Addition Charge input[type="number"] step="any" min="0" optional
        $html .=    "<tr id='location_charge_add_row_addition_charge'><th scope='row'>";
        $html .=    "<label for='currency_" . $currency_array[$i]->id . "'>" . $currency_array[$i]->code . " Charge (Optional)</label></th>";
        $html .=    "<td><input type='number' step='any' min='0' name='currency_" . $currency_array[$i]->id . "' id='currency_" . $currency_array[$i]->id . "' placeholder='0.00' style='min-width:300px;'";
        if ( isset( $_POST[ "currency_" . $currency_array[$i]->id ] ) ) {
            $html .= " value='" . $_POST[ "currency_" . $currency_array[$i]->id ] . "'>";
        } else {
            $html .= ">";
        }
        $html .=    "</td></tr>";

    }


    $html .=    "</tbody></table>";
    $html .=    "<input type='submit' class='button button-primary' value='Add location' name='submit_add_location'>";
    $html .=    "</form>";

    echo $html;

} elseif ( $tab == 'edit' ) {

    if ( isset( $_GET["edit"] ) && ! empty( $_GET['location_id'] )  ) {

        echo '<h3>Edit a location</h3>';
        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_location";
        $charge_table_name = $wpdb->prefix . "book_a_session_location_charge";
        $payment_table_name = $wpdb->prefix . "book_a_session_location_payment_method";
        $schedule_table_name = $wpdb->prefix . "book_a_session_location_schedule";
        $id = intval( $_GET['location_id'] );

        $row_to_edit = $wpdb->get_row("
        SELECT  *
        FROM    $table_name
        WHERE   id = $id", OBJECT );

        $charge = $wpdb->get_results("
        SELECT  *
        FROM    $charge_table_name
        WHERE   location_id = $id", OBJECT );

        $payment_method = $wpdb->get_results("
        SELECT  *
        FROM    $payment_table_name
        WHERE   location_id = $id", OBJECT );

        $schedule = $wpdb->get_results("
        SELECT  *
        FROM    $schedule_table_name
        WHERE   location_id = $id", OBJECT );

        $html =     "<table class='form-table'><form method='POST'>";
        $html .=    "<tbody>";
        // Name input[type="text"] required
        $html .=    "<tr id='region_edit_row_name'><th scope='row'>";
        $html .=    "<label for='name'>Name</label>";
        $html .=    "</th><td><input type='text' name='name' id='region_add_name' required placeholder='e.g. Office, Starbucks, Gym' style='min-width:300px;'"; 
        if ( isset( $row_to_edit->name ) ) {
            $html .= " value='" . $row_to_edit->name . "'>";
        } else {
            $html .= ">";
        }
        $html .=    "</td></tr>";
        // Region select required
        $html .=    "<tr id='location_add_row_region_id'><th scope='row'>";
        $html .=    "<label for='region_id'>Region</label></th>";
        $html .=    "<td><select name='region_id' id='location_add_region_id' required>";
        $region_array = book_a_session_get_table_array( "region" );
        if ( ! empty( $region_array ) ) {
            for ($i = 0; $i < count($region_array); $i++) {
                $html .= "<option value='" . $region_array[$i]->id . "'";
                if ( isset( $row_to_edit->region_id ) ) {
                     if ( $row_to_edit->region_id == $region_array[$i]->id ) {
                            $html .= " selected";
                    }                
                }
                $html .= ">";
                $html .= $region_array[$i]->name . ". Timezone: " . $region_array[$i]->timezone . ". Session price: " . book_a_session_get_price_tag( $region_array[$i]->session_price, $region_array[$i]->currency_id ) . ".";
                $html .= "</option>";
            }
            $html .= "</select>";
            $html .= "</td>";
        } else {
            $html .= "<option selected disabled>No regions found</option>";
            $html .= "</select><br><p class='description'>Add a Region in the Regions settings page</p></td>";
        }    

        // Capacity input[type="number"] step="1" min="0" optional
        $html .=    "<tr id='location_edit_row_capacity'><th scope='row'>";
        $html .=    "<label for='capacity'>Capacity</label></th>";
        $html .=    "<td><input type='number' step='1' min='0' name='capacity' id='location_edit_row_capacity' placeholder='Enter 0 for unlimited capacity' style='min-width:300px;'";
        if ( isset( $row_to_edit->capacity ) ) {
            $html .= " value='" . $row_to_edit->capacity . "'>";
        } else {
            $html .= ">";
        }
        $html .=    "<p class='description'>Enter the number of sessions this location can host at any given time, or 0 for unlimited capacity.</p></td></tr>";

        // Frontend Tag input[type="text"] optional
        $html .=    "<tr id='location_edit_row_tag'><th scope='row'>";
        $html .=    "<label for='tag'>Frontend Tag (Optional)</label>";
        $html .=    "</th><td><input type='text' name='tag' id='location_edit_tag' placeholder='Frontend Tag' style='min-width:300px;'"; 
        if ( isset( $row_to_edit->tag ) ) {
            $html .= " value='" . $row_to_edit->tag . "'>";
        } else {
            $html .= ">";
        }
        $html .=    "</td></tr>";
   
        $html .=    "<tr><th scope='row' colspan='2'><h3>Address</h3><p class='description'>Enter the address of this location. You don't need to do this for an online or phone location</p></th></tr>";
        // Address Line 1 input[type="text"] optional
        $html .=    "<tr id='location_add_row_address_line_1'><th scope='row'>";
        $html .=    "<label for='address_line_1'>Address Line 1 (Optional)</label>";
        $html .=    "</th><td><input type='text' name='address_line_1' id='location_add_address_line_1' placeholder='First line' style='min-width:300px;'"; 
        if ( isset( $row_to_edit->address_line_1 ) ) {
            $html .= " value='" . $row_to_edit->address_line_1 . "'>";
        } else {
            $html .= ">";
        }
        $html .=    "</td></tr>";
        // Address Line 2 input[type="text"] optional
        $html .=    "<tr id='location_add_row_address_line_2'><th scope='row'>";
        $html .=    "<label for='address_line_2'>Address Line 2 (Optional)</label>";
        $html .=    "</th><td><input type='text' name='address_line_2' id='location_add_address_line_2' placeholder='Second line' style='min-width:300px;'"; 
        if ( isset( $row_to_edit->address_line_2 ) ) {
            $html .= " value='" . $row_to_edit->address_line_2 . "'>";
        } else {
            $html .= ">";
        }
        $html .=    "</td></tr>";
        // Address Line 3 input[type="text"] optional
        $html .=    "<tr id='location_add_row_address_line_3'><th scope='row'>";
        $html .=    "<label for='address_line_3'>Address Line 3 (Optional)</label>";
        $html .=    "</th><td><input type='text' name='address_line_3' id='location_add_address_line_3' placeholder='Third line' style='min-width:300px;'"; 
        if ( isset( $row_to_edit->address_line_3 ) ) {
            $html .= " value='" . $row_to_edit->address_line_3 . "'>";
        } else {
            $html .= ">";
        }
        $html .=    "</td></tr>";
        // Postcode input[type="text"] optional
        $html .=    "<tr id='location_add_row_postcode'><th scope='row'>";
        $html .=    "<label for='postcode'>Postcode (Optional)</label>";
        $html .=    "</th><td><input type='text' name='postcode' id='location_add_postcode' placeholder='Postcode or ZIP code' style='min-width:300px;'"; 
        if ( isset( $row_to_edit->postcode_zipcode ) ) {
            $html .= " value='" . $row_to_edit->postcode_zipcode . "'>";
        } else {
            $html .= ">";
        }
        $html .=    "</td></tr>";
        // City input[type="text"] optional
        $html .=    "<tr id='location_edit_row_city'><th scope='row'>";
        $html .=    "<label for='city'>City (Optional)</label>";
        $html .=    "</th><td><input type='text' name='city' id='location_edit_city' placeholder='City' style='min-width:300px;'"; 
        if ( isset( $row_to_edit->city ) ) {
            $html .= " value='" . $row_to_edit->city . "'>";
        } else {
            $html .= ">";
        }
        $html .=    "</td></tr>";
        // Country input[type="text"] optional
        $html .=    "<tr id='location_edit_row_country'><th scope='row'>";
        $html .=    "<label for='country'>Country (Optional)</label>";
        $html .=    "</th><td><input type='text' name='country' id='location_edit_country' placeholder='Country' style='min-width:300px;'"; 
        if ( isset( $row_to_edit->country ) ) {
            $html .= " value='" . $row_to_edit->country . "'>";
        } else {
            $html .= ">";
        }
        $html .=    "</td></tr>";
        // Public Address checkbox
        $html .=    "<tr id='location_edit_row_public_address'><th scope='row'>";
        $html .=    "<label for='public_address'>Public Address? (Optional)</label>";
        $html .=    "</th><td><label for='public_address'><input type='checkbox' id='public_address' name='public_address' value='1'"; 
        if ( ! empty( $row_to_edit->public_address ) ) {
            $html .= " checked>";
        } else {
            $html .= ">";
        }
        $html .=    "Check to display this address openly to all. Uncheck if it's private.</label>";
        $html .=    "<p class='description'>Private addresses are shown only to those who've booked a session at this location.</p>";    
        $html .=    "</td></tr>";
        $html .=    "<tr><th scope='row' colspan='2'><h3>Schedule</h3><p class='description'>Add this location's daily and weekly availability. You can customise your daily schedule's session times and weekly schedule on a site-wide level in the Schedule settings page.</p></th></tr>";
        $schedule_array = book_a_session_get_table_array( "schedule", "start_time", "ASC" );
        // Daily Schedule checkbox
        $html .=    "<tr id='location_add_row_location_schedule'><th scope='col'>";
        $html .=    "<label for='location_schedule'>Daily Schedule</label>";
        $html .=    "<br><a href='javascript:void(0)' class='select-all'>Select all</a>&nbsp;&nbsp;<a href='javascript:void(0)' class='unselect-all'>Unselect all</a>";
        $html .=    "</th><td>";
        for ($i = 0; $i < count( $schedule_array ); $i++) {
            $html .=    "<label for='schedule_" . $schedule_array[$i]->id . "'><input type='checkbox' id='schedule_" . $schedule_array[$i]->id . "' name='schedule_" . $schedule_array[$i]->id . "' value='1'";
            $match = false;
            for ($j = 0; $j < count( $schedule ); $j++) {
                if ( isset( $schedule[$j]->schedule_id ) ) {
                    if ( $schedule[$j]->schedule_id == $schedule_array[$i]->id ) {
                        $html .= " checked>";
                        $match = true;
                    }
                }
            }
            if ( ! $match ) {
                $html .= ">";
            } 
            $html .=    book_a_session_get_session_time( $schedule_array[$i]->id )["time_string"] . "</label>&nbsp;&nbsp;&nbsp;";   
        }
        $html .=    "<p class='description'>Check session times available at this location.</p>";    
        $html .=    "</td></tr>";
        // Weekly Schedule checkbox
        $html .=    "<tr id='location_add_row_location_week'><th scope='row'>";
        $html .=    "<label for='location_week'>Weekly Schedule</label>";
        $html .=    "<br><a href='javascript:void(0)' class='select-all'>Select all</a>&nbsp;&nbsp;<a href='javascript:void(0)' class='unselect-all'>Unselect all</a>";
        $html .=    "</th><td>";
        // Mon
        $html .=    "<label for='accept_mon'><input type='checkbox' id='accept_mon' name='accept_mon' value='1'"; 
            if ( empty( $row_to_edit->accept_mon ) ) {
                $html .= ">";
            } else {
                $html .= " checked >";
            }
        $html .=    "Mon</label>&nbsp;&nbsp;&nbsp;";   
        // Tue
        $html .=    "<label for='accept_tue'><input type='checkbox' id='accept_tue' name='accept_tue' value='1'"; 
            if ( empty( $row_to_edit->accept_tue ) ) {
                $html .= ">";
            } else {
                $html .= " checked>";
            }
        $html .=    "Tue</label>&nbsp;&nbsp;&nbsp;"; 
        // Wed
        $html .=    "<label for='accept_wed'><input type='checkbox' id='accept_wed' name='accept_wed' value='1'"; 
            if ( empty( $row_to_edit->accept_wed ) ) {
                $html .= ">";
            } else {
                $html .= " checked>";
            }
         $html .=    "Wed</label>&nbsp;&nbsp;&nbsp;";
        // Thu
        $html .=    "<label for='accept_thu'><input type='checkbox' id='accept_thu' name='accept_thu' value='1'"; 
            if ( empty( $row_to_edit->accept_thu ) ) {
                $html .= ">";
            } else {
                $html .= " checked>";
            }
        $html .=    "Thu</label>&nbsp;&nbsp;&nbsp;";   
        // Fri
        $html .=    "<label for='accept_fri'><input type='checkbox' id='accept_fri' name='accept_fri' value='1'"; 
            if ( empty( $row_to_edit->accept_fri ) ) {
                $html .= ">";
            } else {
                $html .= " checked>";
            }
        $html .=    "Fri</label>&nbsp;&nbsp;&nbsp;";   
        // Sat
        $html .=    "<label for='accept_sat'><input type='checkbox' id='accept_sat' name='accept_sat' value='1'"; 
            if ( empty( $row_to_edit->accept_sat ) ) {
                $html .= ">";
            } else {
                $html .= " checked>";
            }
        $html .=    "Sat</label>&nbsp;&nbsp;&nbsp;";     
        // Sun
        $html .=    "<label for='accept_sun'><input type='checkbox' id='accept_sun' name='accept_sun' value='1'"; 
            if ( empty( $row_to_edit->accept_sun ) ) {
                $html .= ">";
            } else {
                $html .= " checked>";
            }
        $html .=    "Sun</label>";
        $html .=    "<p class='description'>Check days available at this location.</p>";    
        $html .=    "</td></tr>";
        // Payment methods checkbox
        $html .=    "<tr><th scope='row' colspan='2'><h3>Payment Methods</h3><p class='description'>Check the payment methods you accept for orders at this location.</p>";
        $html .=    "<a href='javascript:void(0)' class='select-all-payment-method'>Select all</a>&nbsp;&nbsp;<a href='javascript:void(0)' class='unselect-all-payment-method'>Unselect all</a>";
        $html .=    "</th></tr>";
        $payment_method_array = book_a_session_get_table_array( "payment_method" );
        for ($i = 0; $i < count( $payment_method_array ); $i++) {
            $html .=    "<tr id='location_add_row_payment_method_" . $payment_method_array[$i]->id . "' class='payment-method'><th scope='row'>";
            $html .=    "<label for='payment_method_" . $payment_method_array[$i]->id . "'>" . $payment_method_array[$i]->name . "</label>";
            $html .=    "</th><td>";
            $html .=    "<label for='payment_method_" . $payment_method_array[$i]->id . "'><input type='checkbox' id='payment_method_" . $payment_method_array[$i]->id . "' name='payment_method_" . $payment_method_array[$i]->id . "' value='1'";
            $match = false;
            for ($j = 0; $j < count( $payment_method ); $j++) {
                if ( isset( $payment_method[$j]->payment_method_id ) ) {
                    if ( $payment_method[$j]->payment_method_id == $payment_method_array[$i]->id ) {
                        $html .= " checked>";
                        $match = true;
                    }
                }
            }
            if ( ! $match ) {
                $html .= ">";
            } 
            $html .=    "Check to accept " . $payment_method_array[$i]->name  . " for this location.</label>";  
            $html .=    "</td></tr>"; 
        }
        // Location charge optional
        $html .=    "<tr><th scope='row' colspan='2'><h3>Extra Charges</h3><p class='description'>To charge extra for this location, enter an additional charge below for any applicable currencies.</p></th></tr>";
        $currency_array = book_a_session_get_table_array( "currency", "id", "ASC" );
        for ( $i = 0; $i < count( $currency_array ); $i++ ){
            // Addition Charge input[type="number"] step="any" min="0" optional
            $html .=    "<tr id='location_charge_add_row_addition_charge'><th scope='row'>";
            $html .=    "<label for='currency_" . $currency_array[$i]->id . "'>" . $currency_array[$i]->code . " Charge (Optional)</label></th>";
            $html .=    "<td><input type='number' step='any' min='0' name='currency_" . $currency_array[$i]->id . "' id='currency_" . $currency_array[$i]->id . "' placeholder='0.00' style='min-width:300px;'";
            $match = false;
            for ( $j = 0; $j < count( $charge ); $j++ ){
                if ( isset( $charge[$j]->currency_id ) && isset( $charge[$j]->addition_charge ) ) {
                    if ( $charge[$j]->currency_id == $currency_array[$i]->id ) {
                        $html .= " value='" . $charge[$j]->addition_charge . "'>";
                        $match = true;
                        break;
                    }
                }
            }
            if ( ! $match ) {
                $html .= ">";
            } 
            
            $html .=    "</td></tr>";   
        }        
        $html .=    "</tbody></table>";
        $html .=    "<input type='submit' class='button button-primary' value='Save changes' name='submit_edit_location'>";
        $html .=    "</form>";
        echo $html;

    } else {

        echo '<h3>Select a location to edit</h3>';
        echo '<form>';
        $locations_list = new Locations_List_Table;
        echo '</form>';

    }

} else {

	echo '<h3>View locations</h3>';
	echo '<form>';
	$locations_list = new Locations_List_Table;
	echo '</form>';

}
// Code after the tabs (outside)
		?>


	</div>

	<?php

}