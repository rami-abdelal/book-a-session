<?php // Book A Session - Currency Settings Page

if ( ! defined( 'ABSPATH' ) ) exit;

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
 }

if( ! class_exists( 'Currencies_List_Table' ) ) {
    require_once plugin_dir_path( __FILE__ ) . 'class-currencies-list-table.php';
}

// Edit

if (   ! empty( $_GET['edit'] ) 
    && ! empty( $_GET['currency_id'] )
    && ! empty( $_POST['submit_edit_currency'] ) ) {

    if ( current_user_can( "manage_options" ) && wp_verify_nonce( $_REQUEST["_wpnonce"], "book_a_session_edit_currency_" . $_GET["currency_id"] ) ) {

        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_currency";
    
        if ( ! empty( $_GET['currency_id'] ) && ! empty( $_POST['code'] ) ) {
    
            ! empty( $_POST['symbol_after'] )              ? $symbol_after            = 1  : $symbol_after            = 0;
            ! empty( $_POST['use_alternative_symbol'] )    ? $use_alternative_symbol  = 1  : $use_alternative_symbol  = 0;
    
            $value_array = array( 
                'code'                      => $_POST['code'], 
                'symbol_after'              => $symbol_after,
                'use_alternative_symbol'    => $use_alternative_symbol,
                'symbol'                    => $_POST['symbol']
            );
            $edit_currency_db_result = $wpdb->update( 
                $table_name, 
                $value_array,
                array( 'id' => $_GET['currency_id'] ), 
                array( 
                    '%s', 
                    '%d',
                    '%d',
                    '%s'
                    ) 
                );
    
                $edit_currency_db_result ? add_action( 'admin_notices', 'book_a_session_admin_notice_edit_success' ) : add_action( 'admin_notices', 'book_a_session_admin_notice_edit_error' );
    
        } else {
            add_action( 'admin_notices', 'book_a_session_admin_notice_validation_error' );
        }
    
    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_edit_security_error' );
    }

}

// Delete
if ( isset( $_GET['delete'] ) && isset( $_GET['currency_id'] ) ) {

    if ( $_GET['page'] == 'book_a_session_currencies' && current_user_can( "manage_options" ) && wp_verify_nonce( $_REQUEST["_wpnonce"], "book_a_session_delete_currency_" . $_GET["currency_id"] ) ) {

        global $wpdb;
        $table_name                  = $wpdb->prefix . "book_a_session_currency";
        $table_name_location_charge  = $wpdb->prefix . "book_a_session_location_charge";
        $table_name_service_price    = $wpdb->prefix . "book_a_session_service_price";
        $table_name_bundle_currency  = $wpdb->prefix . "book_a_session_bundle_currency";

        $delete_currency_db_result[] = $wpdb->delete( $table_name, array( 'id' => $_GET['currency_id'] ), array( '%d' ) );
        $delete_currency_db_result[] = $wpdb->delete( $table_name_location_charge, array( 'currency_id' => $_GET['currency_id'] ), array( '%d' ) );
        $delete_currency_db_result[] = $wpdb->delete( $table_name_service_price, array( 'currency_id' => $_GET['currency_id'] ), array( '%d' ) );
        $delete_currency_db_result[] = $wpdb->delete( $table_name_bundle_currency, array( 'currency_id' => $_GET['currency_id'] ), array( '%d' ) );

        ! in_array( false, $delete_currency_db_result, true ) ? add_action( 'admin_notices', 'book_a_session_admin_notice_delete_success' ) : add_action( 'admin_notices', 'book_a_session_admin_notice_delete_error' );

    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_delete_security_error' );
    }

} 

// Add
if ( isset( $_POST['submit_add_currency'] ) ) {

    if ( current_user_can( "manage_options" ) ) {

        // Prepare database insertion
        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_currency";

        if ( ! empty( $_POST['code'] ) ) {

            ! empty( $_POST['symbol_after'] )              ? $symbol_after            = 1  : $symbol_after            = 0;
            ! empty( $_POST['use_alternative_symbol'] )    ? $use_alternative_symbol  = 1  : $use_alternative_symbol  = 0;

            $value_array = array( 
                'code'                      => $_POST['code'], 
                'symbol_after'              => $symbol_after,
                'use_alternative_symbol'    => $use_alternative_symbol,
                'symbol'                    => $_POST['symbol']
            );
            $add_currency_db_result = $wpdb->insert( 
                $table_name, 
                $value_array,
                array( 
                    '%s', 
                    '%d',
                    '%d',
                    '%s'
                    ) 
                );

            $add_currency_db_result ? add_action( 'admin_notices', 'book_a_session_admin_notice_add_success' ) : add_action( 'admin_notices', 'book_a_session_admin_notice_add_error' );

        } else {
            add_action( 'admin_notices', 'book_a_session_admin_notice_validation_error' );
        }

    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_add_security_error' );
    }

}



function book_a_session_display_currencies_settings_page() {
	if ( ! current_user_can( 'manage_options' ) ) return;

    ?>
	<div class="wrap">
		<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
		<?php 

function page_tabs_currency( $current = 'View' ) {
    $tabs = array(
        'view'   => __( 'View currencies', 'book_a_session' ), 
		'add'  => __( 'Add currency', 'book_a_session' ),
		'edit'  => __( 'Edit currencies', 'book_a_session' )
    );
    $html = '<h2 class="nav-tab-wrapper">';
    foreach( $tabs as $tab => $name ){
        $class = ( $tab == $current ) ? 'nav-tab-active' : '';
        $html .= '<a class="nav-tab ' . $class . '" href="?page=book_a_session_currencies&tab=' . $tab . '">' . $name . '</a>';
    }
    $html .= '</h2>';
    echo $html;
}

// Tabs
$tab = ( ! empty( $_GET['tab'] ) ) ? esc_attr( $_GET['tab'] ) : 'view';
page_tabs_currency( $tab );

// View currencies
if ( $tab == 'view' ) {

	echo '<h3>View currencies</h3>';
	echo '<form>';
	$currencies_list = new Currencies_List_Table;
    echo '</form>';

}

// Add currency
elseif ( $tab == 'add' ) {

    $html =     "<table class='form-table'><form method='POST'>";
    $html .=    "<tr><th class='book-a-session-th-explanation' colspan='2'><h2>Add a new currency</h2><p>Currencies and Regions work together to behave like price profiles.</p>";
    $html .=    "<p>Adding multiple currencies and regions allows you to create different price points for clients in different regions, allowing you to set prices that differ from the exchange rate.</p>";
    $html .=    "<pThat being said, you don't have to add every currency your clients might be using, as online payment systems such as PayPal automatically convert currencies and prices for you.</p>";
    $html .=    "<p>For example: Say you were a service provider based in Egypt, and you want to provide services offline and online locally to Egyptians, but also you want to provide online services internationally. Also say your prices were 300 EGP per session. You could either:</p>";
    $html .=    "<ol><li>Add EGP as your only currency, tied to your only Region, Egypt. A client from the US or somewhere else would have no other option but to use the Egypt region, and would pay the Egyptian price, 300 EGP. Paying online, that price would then be converted to USD at the current exchange rate, which would be, as of writing: $16.94.</li>";
    $html .=    "<li>Add both EGP and USD tied to the Regions Egypt and International respectively. Egyptians would still pay 300 EGP, but adding International as a Region with USD as its currency, let's you set the price for them at $30.00.</li></ol>";
    $html .=    "<p>Setting different prices allows you to keep prices affordable for those in one region, but also allows you to set your prices competitively for another. Once you add currencies, you can create Regions to tie these currencies to. A Region can only have one currency, but it can consist of many countries, or every country except some. So in this example, we'd want to have International as one Region, using USD as its currency, and Egypt as the other Region, setting EGP as its currency.</p></th></tr>";
    // Code input[type="text"] required
    $html .=    "<tr id='currency_add_row_code'><th scope='row'>";
    $html .=    "<label for='code'>Currency code</label>";
    $html .=    "</th><td><input type='text' name='code' id='currency_add_code' required placeholder='e.g. USD, EGP, GBP' style='min-width:300px;'"; 
    if ( isset( $_POST['code'] ) ) {
        $html .= " value='" . $_POST['code'] . "'>";
    } else {
        $html .= ">";
    }
    if ( isset( $_POST['submit_add_currency'] ) ) {
        if ( empty( $_POST['code'] ) ) {
          $html .= "<p class='description book-a-session-error'>Please enter the currency code.</p>";
        }
    } 
    $html .=    "</td></tr>";    
    // Symbol input[type="text"] optional
    $html .=    "<tr id='currency_add_row_symbol'><th scope='row'>";
    $html .=    "<label for='symbol'>Symbol (Optional)</label>";
    $html .=    "</th><td><input type='text' name='symbol' id='currency_add_symbol' placeholder='e.g. $, £, €' style='min-width:300px;'"; 
    if ( isset( $_POST['symbol'] ) ) {
        $html .= " value='" . $_POST['symbol'] . "'>";
    } else {
        $html .= ">";
    }
    if ( isset( $_POST['submit_add_currency'] ) ) {
        if ( empty( $_POST['symbol'] ) && ! empty( $_POST['use_alternative_symbol'] )  ) {
          $html .= "<p class='description book-a-session-error'>Please enter the symbol to use, or uncheck Use Symbol as Label.</p>";
        }
    } 
    $html .=    "</td></tr>";
    // Use Alternative Symbol checkbox required
    $html .=    "<tr id='currency_add_row_use_alternative_symbol'><th scope='row'>";
    $html .=    "<label for='use_alternative_symbol'>Use Symbol as Label?</label>";
    $html .=    "</th><td><label for='use_alternative_symbol'><input type='checkbox' id='use_alternative_symbol' name='use_alternative_symbol' value='1'"; 
    if ( ! empty( $_POST['use_alternative_symbol'] ) ) {
        $html .= " checked>";
    } else {
        $html .= ">";
    }
    $html .=    "<strong>Yes:</strong> £50.00 / 50.00£. <strong>No:</strong> GBP 50.00 / 50.00 GBP.</label></td></tr>";
    // Symbol After checkbox required
    $html .=    "<tr id='currency_add_row_symbol_after'><th scope='row'>";
    $html .=    "<label for='symbol_after'>Label after amount?</label>";
    $html .=    "</th><td><label for='symbol_after'><input type='checkbox' id='symbol_after' name='symbol_after' value='1'"; 
    if ( ! empty( $_POST['symbol_after'] ) ) {
        $html .= " checked>";
    } else {
        $html .= ">";
    }
    $html .=    "<strong>Yes:</strong> 50.00 GBP / 50.00$. <strong>No:</strong> GBP 50.00 / £50.00</label></td></tr>";


    $html .=    "</tbody></table>";
    $html .=    "<input type='submit' class='button button-primary' value='Add currency' name='submit_add_currency'>";
    $html .=    "</form>";

	
    echo $html;

} elseif ( $tab == 'edit' ) {

    if ( isset( $_GET["edit"] ) && ! empty( $_GET['currency_id'] )  ) {

        echo '<h3>Edit a currency</h3>';
        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_currency";
        $id = intval( $_GET['currency_id'] );

        $row_to_edit = $wpdb->get_row( "
        SELECT  *
        FROM    $table_name
        WHERE   id = $id", OBJECT );

        $html =     "<table class='form-table'><form method='POST'>";
        // Code input[type="text"] required
        $html .=    "<tr id='currency_edit_row_code'><th scope='row'>";
        $html .=    "<label for='code'>Currency code</label>";
        $html .=    "</th><td><input type='text' name='code' id='currency_edit_code' required placeholder='e.g. USD, EGP, GBP' style='min-width:300px;'"; 
        if ( isset( $row_to_edit->code ) ) {
            $html .= " value='" . $row_to_edit->code . "'>";
        } else {
            $html .= ">";
        }
        if ( isset( $_POST['submit_edit_currency'] ) ) {
            if ( empty( $_POST['code'] ) ) {
            $html .= "<p class='description book-a-session-error'>Please enter the currency code.</p>";
            }
        } 
        $html .=    "</td></tr>";    
        // Symbol input[type="text"] optional
        $html .=    "<tr id='currency_edit_row_symbol'><th scope='row'>";
        $html .=    "<label for='symbol'>Symbol (Optional)</label>";
        $html .=    "</th><td><input type='text' name='symbol' id='currency_edit_symbol' placeholder='e.g. $, £, €' style='min-width:300px;'"; 
        if ( isset( $row_to_edit->symbol ) ) {
            $html .= " value='" . $row_to_edit->symbol . "'>";
        } else {
            $html .= ">";
        }
        if ( isset( $_POST['submit_edit_currency'] ) ) {
            if ( empty( $_POST['symbol'] ) && ! empty( $_POST['use_alternative_symbol'] )  ) {
            $html .= "<p class='description book-a-session-error'>Please enter the symbol to use, or uncheck Use Symbol as Label.</p>";
            }
        } 
        $html .=    "</td></tr>";
        // Use Alternative Symbol checkbox required
        $html .=    "<tr id='currency_edit_row_use_alternative_symbol'><th scope='row'>";
        $html .=    "<label for='use_alternative_symbol'>Use Symbol as Label?</label>";
        $html .=    "</th><td><label for='use_alternative_symbol'><input type='checkbox' id='use_alternative_symbol' name='use_alternative_symbol' value='1'"; 
        if ( ! empty( $row_to_edit->use_alternative_symbol ) ) {
            $html .= " checked>";
        } else {
            $html .= ">";
        }
        $html .=    "<strong>Yes:</strong> £50.00 / 50.00 £. <strong>No:</strong> GBP50.00 / 50.00 GBP.</label></td></tr>";
        // Symbol After checkbox required
        $html .=    "<tr id='currency_edit_row_symbol_after'><th scope='row'>";
        $html .=    "<label for='symbol_after'>Label after amount?</label>";
        $html .=    "</th><td><label for='symbol_after'><input type='checkbox' id='symbol_after' name='symbol_after' value='1'"; 
        if ( ! empty( $row_to_edit->symbol_after) ) {
            $html .= " checked>";
        } else {
            $html .= ">";
        }
        $html .=    "<strong>Yes:</strong> 50.00 GBP / 50.00 $. <strong>No:</strong> GBP50.00 / £50.00</label></td></tr>";
        $html .=    "</tbody></table>";

        $html .=    "<input type='submit' class='button button-primary' value='Save changes' name='submit_edit_currency'>";
        $html .=    "</form>";
            
        echo $html;

    } else {

        echo '<h3>Select a currency to edit</h3>';
        echo '<form>';
        $currencies_list = new Currencies_List_Table;
        echo '</form>';

    }

} else {

	echo '<h3>View currencies</h3>';
	echo '<form>';
	$currencies_list = new Currencys_List_Table;
	echo '</form>';

}
// Code after the tabs (outside)
		?>


	</div>

	<?php

}