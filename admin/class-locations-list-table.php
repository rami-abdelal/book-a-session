<?php

if (!class_exists('WP_List_Table')) {
    require_once (ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
 
}

class Locations_List_Table extends WP_List_Table  {


    public function __construct()
    {
        parent::__construct(array(
            'singular' => 'location',
            'plural' => 'locations',
            'ajax' => true
        ));

        $this->prepare_items();
        $this->display();
        
    }

    public function prepare_items()
    {
    $this->_column_headers = $this->get_column_info();
    $columns = $this->get_columns();
    $hidden = $this->get_hidden_columns();
    $sortable = $this->get_sortable_columns();
    $this->_column_headers = array(
        $columns,
        $hidden,
        $sortable
    );
    
    $this->process_bulk_action();
    $per_page = $this->get_items_per_page('records_per_page', 10);
    $current_page = $this->get_pagenum();
    $total_items = self::record_count();
    $data = self::get_records($per_page, $current_page);
    $this->set_pagination_args(
                      ['total_items' => $total_items, 
                   'per_page' => $per_page
                  ]);
    $this->items = $data;
    }

    /** * 
    *Retrieve records data from the database
    * * @param int $per_page
    * @param int $page_number
    * * @return mixed
    */
    public static function get_records($per_page = 10, $page_number = 1)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'book_a_session_location';

        $sql = "SELECT
                *
                FROM " .  
                $table_name;

        /*if (isset($_REQUEST['s'])) {
        $sql.= ' where column1 LIKE "%' . $_REQUEST['s'] . '%" or column2 LIKE "%' . $_REQUEST['s'] . '%"';
        }*/
         
        if (!empty($_REQUEST['orderby'])) {
                $sql.= ' ORDER BY ' . esc_sql($_REQUEST['orderby']);
            $sql.= !empty($_REQUEST['order']) ? ' ' . esc_sql($_REQUEST['order']) : ' ASC';
        } else {
            $sql .= ' ORDER BY region_id, name ASC';
        }
        $sql.= " LIMIT $per_page";
        $sql.= ' OFFSET ' . ($page_number - 1) * $per_page;
        $result = $wpdb->get_results($sql, 'ARRAY_A');
        return $result;
    }

    function get_columns()
        {
            $columns = [
                'location_name'=>__('Name'),
                'location_address'=>__('Address'),
                'location_schedule' =>__('Daily Schedule'),
                'location_week' =>__('Weekly Schedule'),
                'location_details'=>__('Details'),
                'actions'=>__('Actions')
                  ];
            return $columns;
        }       

    public function get_hidden_columns()
    {
        // Setup Hidden columns and return them
        return array(
        );
    }

    /** 
    * Columns to make sortable. 
    * * @return array 
    */
    public function get_sortable_columns()
    {
        $sortable_columns = array(
            'location_name'=>array('name', true)
          );
        return $sortable_columns;
    }

    /** 
    *Text displayed when no record data is available 
    */
    public function no_items()
    {
        _e('No locations found.', 'bx');
    }

    /** 
    * Returns the count of records in the database. 
    * * @return null|string 
    */
    public static function record_count()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'book_a_session_location';

        $sql = "SELECT
                COUNT(*) 
                FROM " . 
                $table_name;
        return $wpdb->get_var($sql);
    }

        public function column_default($item, $column_name) {

            $nonce_delete = "&_wpnonce=" . wp_create_nonce( 'book_a_session_delete_location_' . $item["id"] );
            $nonce_edit = "&_wpnonce=" . wp_create_nonce( 'book_a_session_edit_location_' . $item["id"] );            
            $admin_edit_page_url = 'admin.php?page=book_a_session_locations&tab=edit'; 

            $schedule_array = book_a_session_get_table_array( "schedule", "id", "ASC" );
            $location_schedule_array = book_a_session_get_table_array( "location_schedule" );
            $location_charge_array = book_a_session_get_table_array( "location_charge" );
            $location_payment_method_array = book_a_session_get_table_array( "location_payment_method" );
            $payment_method_array = book_a_session_get_table_array( "payment_method" );
            $currency_array = book_a_session_get_table_array( "currency" );

            switch ($column_name) {

                case "location_name"                        : echo "<strong>". $item["name"] . "</strong><p class='description'>Region: " .  book_a_session_core_get_region_by_id( $item["region_id"] )["name"] . "</p>"; break;

                case "location_address"                     : $lines = 0;
                                                            if ( ! empty( $item["public_address"] ) ) { echo "<strong>Public Address</strong><br>"; $lines++; }
                                                            if ( ! empty( $item["address_line_1"] ) ) { echo $item["address_line_1"] . "<br>"; $lines++; }
                                                            if ( ! empty( $item["address_line_2"] ) ) { echo $item["address_line_2"] . "<br>"; $lines++; }
                                                            if ( ! empty( $item["address_line_3"] ) ) { echo $item["address_line_3"] . "<br>"; $lines++; }
                                                            if ( ! empty( $item["city"] ) ) { echo $item["city"] . "<br>"; $lines++; }
                                                            if ( ! empty( $item["postcode_zipcode"] ) ) { echo $item["postcode_zipcode"] . "<br>"; $lines++; }
                                                            if ( ! empty( $item["country"] ) ) { echo $item["country"] . "<br>"; $lines++; }
                                                            if ( $lines == 0  )  { echo "&mdash;"; }

                                                            break;

                case "location_schedule"                    : echo "<table><tbody>";

                for ( $i = 0; $i < count( $schedule_array ); $i++ ) {

                    $no_match = true;

                    for ( $j = 0; $j < count( $location_schedule_array ); $j++ ) {
                        
                        if ( ! empty( $location_schedule_array[$j]->location_id ) && ! empty( $location_schedule_array[$j]->schedule_id ) ) {

                            if ( $location_schedule_array[$j]->location_id == $item["id"] && $location_schedule_array[$j]->schedule_id == $schedule_array[$i]->id ){
                                echo "<tr class='book-a-session-admin-accepted'><th scope='col'>" . book_a_session_get_session_time( $schedule_array[$i]->id )["time_string"] . "</th><td>&#10003;</td></tr>";
                                $no_match = false;
                            } 

                        }                        

                    }

                    if ( $no_match ) {
                        echo "<tr class='book-a-session-admin-declined'><th scope='col'>" . book_a_session_get_session_time( $schedule_array[$i]->id )["time_string"] . "</th><td>&mdash;</td></tr>";
                    }

                }
                                                              echo "</tbody></table>"; break;
                                                              
                case "location_week"                        : echo "<table><tbody>";
                echo $item["accept_mon"] == 1 ?  "<tr class='book-a-session-admin-accepted'><th scope='col'>Mon</th><td>&#10003;</td></tr>" : "<tr class='book-a-session-admin-declined'><th scope='col'>Mon</th><td>&mdash;</td></tr>";
                echo $item["accept_tue"] == 1 ?  "<tr class='book-a-session-admin-accepted'><th scope='col'>Tue</th><td>&#10003;</td></tr>" : "<tr class='book-a-session-admin-declined'><th scope='col'>Tue</th><td>&mdash;</td></tr>";
                echo $item["accept_wed"] == 1 ?  "<tr class='book-a-session-admin-accepted'><th scope='col'>Wed</th><td>&#10003;</td></tr>" : "<tr class='book-a-session-admin-declined'><th scope='col'>Wed</th><td>&mdash;</td></tr>";
                echo $item["accept_thu"] == 1 ?  "<tr class='book-a-session-admin-accepted'><th scope='col'>Thu</th><td>&#10003;</td></tr>" : "<tr class='book-a-session-admin-declined'><th scope='col'>Thu</th><td>&mdash;</td></tr>";
                echo $item["accept_fri"] == 1 ?  "<tr class='book-a-session-admin-accepted'><th scope='col'>Fri</th><td>&#10003;</td></tr>" : "<tr class='book-a-session-admin-declined'><th scope='col'>Fri</th><td>&mdash;</td></tr>";
                echo $item["accept_sat"] == 1 ?  "<tr class='book-a-session-admin-accepted'><th scope='col'>Sat</th><td>&#10003;</td></tr>" : "<tr class='book-a-session-admin-declined'><th scope='col'>Sat</th><td>&mdash;</td></tr>";
                echo $item["accept_sun"] == 1 ?  "<tr class='book-a-session-admin-accepted'><th scope='col'>Sun</th><td>&#10003;</td></tr>" : "<tr class='book-a-session-admin-declined'><th scope='col'>Sun</th><td>&mdash;</td></tr>";
                echo "</tbody></table>"; break;


                case "location_details"                    : echo "<table><tbody>";

                                                            for ( $i = 0; $i < count( $payment_method_array ); $i++ ) {

                                                                $no_match = true;


                                                                for ( $j = 0; $j < count( $location_payment_method_array ); $j++ ) {
                                                                    
                                                                    if ( ! empty( $location_payment_method_array[$j]->location_id ) && ! empty( $location_payment_method_array[$j]->payment_method_id ) ) {

                                                                        if ( $location_payment_method_array[$j]->location_id == $item["id"] && $location_payment_method_array[$j]->payment_method_id == $payment_method_array[$i]->id ){

                                                                            echo "<tr class='book-a-session-admin-accepted'><th scope='col'>" . book_a_session_get_payment_method( $payment_method_array[$i]->id ) . "</th><td>&#10003;</td></tr>";
                                                                            $no_match = false;
                                                                        } 

                                                                    }                        

                                                                }

                                                                if ( $no_match ) {                                                                  
                                                                    echo "<tr class='book-a-session-admin-declined'><th scope='col'>" . book_a_session_get_payment_method( $payment_method_array[$i]->id ) . "</th><td>&mdash;</td></tr>";
                                                                }

                                                            }

                                                            for ( $i = 0; $i < count( $currency_array ); $i++ ) {

                                                                for ( $j = 0; $j < count( $location_charge_array ); $j++ ) {
                                                                    
                                                                    if ( ! empty( $location_charge_array[$j]->location_id ) && ! empty( $location_charge_array[$j]->currency_id ) ) {

                                                                        if ( $location_charge_array[$j]->location_id == $item["id"] && $location_charge_array[$j]->currency_id == $currency_array[$i]->id ){

                                                                            echo "<tr><th scope='col'>Extra Charge " . $currency_array[$i]->code . "</th><td>" . 
                                                                            book_a_session_get_price_tag( $location_charge_array[$j]->addition_charge, $location_charge_array[$j]->currency_id ) . 
                                                                            "</td></tr>";
                                                                            
                                                                        } 

                                                                    }                        

                                                                }

                                                            }

                                                            echo "<tr><th scope='col'>Frontend Tag</th><td>"; 
                                                            echo $item["tag"] ? $item["tag"] : "&mdash;";
                                                            echo "</td></tr>";

                                                            echo "<tr><th scope='col'>Capacity</th><td>"; 
                                                            echo intval( $item["capacity"] ) > 0 ? intval( $item["capacity"] ) : "Unlimited";
                                                            echo "</td></tr>";
                                                            
                                                            echo "</tbody></table>"; break;

                case "actions" : echo   "<a href='" . $admin_edit_page_url . $nonce_edit . "&edit=1&location_id=" . $item["id"] . "'>Edit</a>" . 
                                        "&nbsp;&nbsp;&nbsp;" . 
                                        "<a class='book-a-session-row-delete' href='" . $admin_edit_page_url . $nonce_delete . "&delete=1&location_id=" . $item["id"] . "'>Delete</a>"; 
                break;
                return $item[ $column_name ];
                default:
                    return $item[ $column_name ] ;
            
                }

        }

}