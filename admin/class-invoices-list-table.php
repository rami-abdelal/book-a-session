<?php

if (!class_exists('WP_List_Table')) {
    require_once (ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
 
}

class Invoices_List_Table extends WP_List_Table  {


    public function __construct()
    {
        parent::__construct(array(
            'singular' => 'invoice',
            'plural' => 'invoices',
            'ajax' => true
        ));

        $this->prepare_items();
        $this->display();
        
    }

    public function prepare_items()
    {
    $this->_column_headers = $this->get_column_info();
    $columns = $this->get_columns();
    $hidden = $this->get_hidden_columns();
    $sortable = $this->get_sortable_columns();
    $this->_column_headers = array(
        $columns,
        $hidden,
        $sortable
    );
    
    $this->process_bulk_action();
    $per_page = $this->get_items_per_page('records_per_page', 10);
    $current_page = $this->get_pagenum();
    $total_items = self::record_count();
    $data = self::get_records($per_page, $current_page);
    $this->set_pagination_args(
                      ['total_items' => $total_items, 
                   'per_page' => $per_page 
                  ]);
    $this->items = $data;
    }

    /** * 
    *Retrieve records data from the database
    * * @param int $per_page
    * @param int $page_number
    * * @return mixed
    */
    public static function get_records($per_page = 10, $page_number = 1)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'book_a_session_invoice';

        $sql = "SELECT
                *
                FROM " .  
                $table_name;
         
        if (!empty($_REQUEST['orderby'])) {
                $sql.= ' ORDER BY ' . esc_sql($_REQUEST['orderby']);
            $sql.= !empty($_REQUEST['order']) ? ' ' . esc_sql($_REQUEST['order']) : ' ASC';
        }
        $sql.= " LIMIT $per_page";
        $sql.= ' OFFSET ' . ($page_number - 1) * $per_page;
        $result = $wpdb->get_results($sql, 'ARRAY_A');
        return $result;
    }

    function get_columns()
        {
            $columns = [
                'invoice_order'=>__('Order'),
                'invoice_details'=>__('Details'),
                'invoice_amount'=>__('Amount'),
                'actions'=>__('Actions')
                  ];
            return $columns;
        }       

    public function get_hidden_columns()
    {
        // Setup Hidden columns and return them
        return array(
        );
    }

    /** 
    * Columns to make sortable. 
    * * @return array 
    */
    public function get_sortable_columns()
    {
        $sortable_columns = array(
            'invoice_order'=>array('issue_date', true)
          );
        return $sortable_columns;
    }

    /** 
    *Text displayed when no record data is available 
    */
    public function no_items()
    {
        _e('No invoices found.', 'bx');
    }

    /** 
    * Returns the count of records in the database. 
    * * @return null|string 
    */
    public static function record_count()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'book_a_session_invoice';

        $sql = "SELECT
                COUNT(*) 
                FROM " . 
                $table_name;
        return $wpdb->get_var($sql);
    }

        public function column_default($item, $column_name) {
            
            $order_array = book_a_session_get_table_array( "order", false, false, "*", array( "order_id", "=", $item["order_id"] ) );
            if ( ! empty( $order_array ) ) {
                if ( is_array( $order_array ) ) { 
                    $order = $order_array[0];
                } else {
                    $order = $order_array;
                }
            } else {
                $order = false;
            }
            $nonce_view = "&_wpnonce=" . wp_create_nonce( 'book_a_session_view_invoice_' . $item["id"] );
            $nonce_delete = "&_wpnonce=" . wp_create_nonce( 'book_a_session_delete_invoice_' . $item["id"] );
            $admin_page_url = 'admin.php?page=book_a_session_invoices'; 
            switch ($column_name) {

                    case "invoice_order" : 

                        echo "Client: <strong>" . book_a_session_get_user_name( $order->user_id ) . "</strong>" . "<br>Order ID: " . $item["order_id"] . "<br>" . $order->booking_status . ": " . $order->payment_status; break;

                    case "invoice_details" : 
                        echo  
                        "<table><tbody><tr>" .
                        "<th scope='col'>Issue Date:</th><td>" . $item["issue_date"] . "</td></tr>" .
                        "<th scope='col'>Due By Date:</th><td>"        . $item["due_by_date"]                                                       . "</td></tr>" .
                        "</tbody></table>"; break;
                    case "invoice_amount"                    : echo book_a_session_get_price_tag( $item["grand_total_due"], (int)$item["currency_id"]) ; break;

                    case "actions" : echo   "<a href='" . $admin_page_url . $nonce_view . "&view=1&invoice_id=" . $item["id"] . "'>View</a>" . 
                                            "&nbsp;&nbsp;&nbsp;" . 
                                            "<a class='book-a-session-row-delete' href='" . $admin_page_url . $nonce_delete . "&delete=1&invoice_id=" . $item["id"] . "'>Delete</a>"; 
                    break;
                    return $item[ $column_name ];
                    default:
                        return $item[ $column_name ] ;
            
                }

        }

}