<?php

if (!class_exists('WP_List_Table')) {
    require_once (ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
 
}

class Regions_List_Table extends WP_List_Table  {


    public function __construct()
    {
        parent::__construct(array(
            'singular' => 'region',
            'plural' => 'regions',
            'ajax' => true
        ));

        $this->prepare_items();
        $this->display();
        
    }

    public function prepare_items()
    {
    $this->_column_headers = $this->get_column_info();
    $columns = $this->get_columns();
    $hidden = $this->get_hidden_columns();
    $sortable = $this->get_sortable_columns();
    $this->_column_headers = array(
        $columns,
        $hidden,
        $sortable
    );
    
    $this->process_bulk_action();
    $per_page = $this->get_items_per_page('records_per_page', 10);
    $current_page = $this->get_pagenum();
    $total_items = self::record_count();
    $data = self::get_records($per_page, $current_page);
    $this->set_pagination_args(
                      ['total_items' => $total_items, 
                   'per_page' => $per_page 
                  ]);
    $this->items = $data;
    }

    /** * 
    *Retrieve records data from the database
    * * @param int $per_page
    * @param int $page_number
    * * @return mixed
    */
    public static function get_records($per_page = 10, $page_number = 1)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'book_a_session_region';

        $sql = "SELECT
                *
                FROM " .  
                $table_name;

        /*if (isset($_REQUEST['s'])) {
        $sql.= ' where column1 LIKE "%' . $_REQUEST['s'] . '%" or column2 LIKE "%' . $_REQUEST['s'] . '%"';
        }*/
         
        if (!empty($_REQUEST['orderby'])) {
                $sql.= ' ORDER BY ' . esc_sql($_REQUEST['orderby']);
            $sql.= !empty($_REQUEST['order']) ? ' ' . esc_sql($_REQUEST['order']) : ' ASC';
        }
        $sql.= " LIMIT $per_page";
        $sql.= ' OFFSET ' . ($page_number - 1) * $per_page;
        $result = $wpdb->get_results($sql, 'ARRAY_A');
        return $result;
    }

    function get_columns()
        {
            $columns = [
                'region_name'=>__('Name'),
                'region_description'=>__('Description'),
                'region_countries'=>__('Countries'),                
                'region_session_price'=>__('Session price'),
                'actions'=>__('Actions')
                  ];
            return $columns;
        }       

    public function get_hidden_columns()
    {
        // Setup Hidden columns and return them
        return array(
        );
    }

    /** 
    * Columns to make sortable. 
    * * @return array 
    */
    public function get_sortable_columns()
    {
        $sortable_columns = array(
            'region_name'=>array('name', true)
          );
        return $sortable_columns;
    }

    /** 
    *Text displayed when no record data is available 
    */
    public function no_items()
    {
        _e('No regions found.', 'bx');
    }

    /** 
    * Returns the count of records in the database. 
    * * @return null|string 
    */
    public static function record_count()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'book_a_session_region';

        $sql = "SELECT
                COUNT(*) 
                FROM " . 
                $table_name;
        return $wpdb->get_var($sql);
    }

        public function column_default($item, $column_name) {
            $country_array = book_a_session_get_table_array( "country" );            
            $region_country_array = book_a_session_get_table_array( "region_country", false, false, "*", array( "region_id", "=", $item["id"] ) );
            $nonce_delete = "&_wpnonce=" . wp_create_nonce( 'book_a_session_delete_region_' . $item["id"] );
            $nonce_edit = "&_wpnonce=" . wp_create_nonce( 'book_a_session_edit_region_' . $item["id"] );
            $admin_edit_page_url = 'admin.php?page=book_a_session_regions&tab=edit'; 
            switch ($column_name) {

                    case "region_name"                          : echo "<strong>" . $item["name"] . "</strong>";
                    if ( ! empty( $item["image_id"] ) ) { echo "<br><br>" . wp_get_attachment_image( $item["image_id"], "thumbnail" ); } break;
                    case "region_description"                   : echo $item["description"] . "<br><br>"                                  . 
                                                                "<table><tbody><tr>"                                                      .
                    "<th scope='col'>International:</th><td>"   ; if ( $item["international"] ){ echo 'Yes'; } else { echo 'No'; }    echo  "</td></tr>" .
                    "<th scope='col'>Timezone:</th><td>"        . $item["timezone"]                                                       . "</td></tr>" .
                    "<th scope='col'>Week starts on:</th><td>"  ; if ( $item["sunday_week"] ){ echo 'Sunday'; } else { echo 'Monday'; }echo "</td></tr>" .
                    "<th scope='col'>Currency:</th><td>"    . book_a_session_get_currency_by_id(array("id"=>$item["currency_id"]))["code"]. "</td></tr>" .
                    "<th scope='col'>Code:</th><td>"            . $item["code"]                                                           . "</td></tr>" .
                    "</tbody></table>"; break;
                    case "region_countries"                    : echo "<div class='table-countries'><table><tbody>";
                                                                if ( $item["international"] || (int)count( $country_array ) == (int)count( $region_country_array ) ){ echo "<tr class='book-a-session-admin-accepted'><th scope='col'>All</th><td>&#10003;</td></tr>"; } else {   
                                                                    for ( $i = 0; $i < count( $country_array ); $i++ ) {
                                                    
                                                                        for ( $j = 0; $j < count( $region_country_array ); $j++ ) {
                                                                            
                                                                            if ( ! empty( $region_country_array[$j]->region_id ) && ! empty( $region_country_array[$j]->country_code ) ) {
                                                    
                                                                                if ( $region_country_array[$j]->region_id == $item["id"] && $region_country_array[$j]->country_code == $country_array[$i]->code ){
                                                                                    echo "<tr class='book-a-session-admin-accepted'><th scope='col'>" . $country_array[$i]->name . "</th><td>&#10003;</td></tr>";
                                                                                } 
                                                    
                                                                            }                        
                                                    
                                                                        }

                                                                    }

                                                                }
                                                                                                              echo "</tbody></table></div>";  break;
                                                                    case "region_session_price"                 : echo book_a_session_get_price_tag($item["session_price"],(int)$item["currency_id"]); break;

                    case "actions" : echo   "<a href='" . $admin_edit_page_url . $nonce_edit . "&edit=1&region_id=" . $item["id"] . "'>Edit</a>" . 
                                            "&nbsp;&nbsp;&nbsp;" . 
                                            "<a class='book-a-session-row-delete' href='" . $admin_edit_page_url . $nonce_delete . "&delete=1&region_id=" . $item["id"] . "'>Delete</a>"; 
                    break;
                    return $item[ $column_name ];
                    default:
                        return $item[ $column_name ] ;
            
                }

        }

}