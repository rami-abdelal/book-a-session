<?php // Book A Session - Settings Page

if ( ! defined( 'ABSPATH' ) ) exit;

// Display the plugin settings page

function book_a_session_display_general_settings_page() {
	
	if ( ! current_user_can( 'manage_options' ) ) return;

	// Init WP Media for image selection here

	wp_enqueue_media();

	?>
	
	<div class="wrap">
		<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
		<br>

		<?php

			function page_tabs_general_settings( $current = 'View' ) {

				$tabs = array(

					'general'   => __( 'General', 'book_a_session' ), 
					'practitioners'   => __( 'Practitioners', 'book_a_session' ), 
					'schedule'  => __( 'Schedule', 'book_a_session' ),
					'currencies'   => __( 'Currencies', 'book_a_session' ), 
					'regions'  => __( 'Regions', 'book_a_session' ),
					'payment_methods'  => __( 'Payment Methods', 'book_a_session' ),
					'locations'  => __( 'Locations', 'book_a_session' ),
					'service_types'  => __( 'Service Types', 'book_a_session' ),
					'services'  => __( 'Services', 'book_a_session' ),
					'bundles'  => __( 'Bundles', 'book_a_session' ),
					'orders'  => __( 'Orders', 'book_a_session' ),
					'timetables'  => __( 'Timetables', 'book_a_session' ),
					'email'  => __( 'Emails', 'book_a_session' ),
					'invoices'  => __( 'Invoices', 'book_a_session' ),
					'payments'  => __( 'Payments', 'book_a_session' ),
					
				);

				$html = '<h2 class="nav-tab-wrapper">';

				foreach( $tabs as $tab => $name ){

					$class = ( $tab == $current ) ? 'nav-tab-active' : '';
					$html .= '<a class="nav-tab ' . $class . '" href="?page=book_a_session&tab=' . $tab . '">' . $name . '</a>';

				}

				$html .= '</h2>';
				echo $html;

			}

			// Tabs

			$tab = ( ! empty( $_GET['tab'] ) ) ? esc_attr( $_GET['tab'] ) : 'general';
			page_tabs_general_settings( $tab );

			?>

			<form action="options.php" method="post">

			<?php

				if ( $tab == 'general' ) {

					// Output security fields
					settings_fields( 'book_a_session_options_general' );
					
					// Output setting sections
					do_settings_sections( 'book_a_session_general' );

					// Submit button
					submit_button();
					
				}

				if ( $tab == 'practitioners' ) {

					// Output security fields
					settings_fields( 'book_a_session_options_practitioners' );
					
					// Output setting sections
					do_settings_sections( 'book_a_session_practitioners' );

					// Submit button
					//submit_button();
					
				}

				if ( $tab == 'schedule' ) {

					// Output security fields
					settings_fields( 'book_a_session_options_schedule' );
					
					// Output setting sections
					do_settings_sections( 'book_a_session_schedule' );

					// Submit button
					submit_button();

					//$schedule_list = new Schedule_List_Table;
					
				}

				if ( $tab == 'currencies' ) {

					// Output security fields
					settings_fields( 'book_a_session_options_currencies' );
					
					// Output setting sections
					do_settings_sections( 'book_a_session_currencies' );

					// Submit button
					//submit_button();

					$currency_list = new Currencies_List_Table;
					
				}
				

				if ( $tab == 'regions' ) {

					// Output security fields
					settings_fields( 'book_a_session_options_regions' );
					
					// Output setting sections
					do_settings_sections( 'book_a_session_regions' );

					// Submit button disabled because there aren't currently any Location options
					//submit_button();

					$region_list = new Regions_List_Table;
				}

				if ( $tab == 'payment_methods' ) {

					// Output security fields
					settings_fields( 'book_a_session_options_paymentmethods' );
					
					// Output setting sections
					do_settings_sections( 'book_a_session_payment_methods' );

					// Submit button
					submit_button();
					
				}

				if ( $tab == 'locations' ) {

					// Output security fields
					settings_fields( 'book_a_session_options_locations' );
					
					// Output setting sections
					do_settings_sections( 'book_a_session_locations' );

					// Submit button disabled because there aren't currently any Location options
					//submit_button();
					
					$locations_list = new Locations_List_Table;

				}

				if ( $tab == 'service_types' ) {

					// Output security fields
					settings_fields( 'book_a_session_options_service_types' );
					
					// Output setting sections
					do_settings_sections( 'book_a_session_service_types' );

					// Submit button disabled because there aren't currently any Service Type options
					//submit_button();
					
					$service_types_list = new Service_Types_List_Table;


				}

				if ( $tab == 'services' ) {
					
					// Output security fields
					settings_fields( 'book_a_session_options_services' );
					
					// Output setting sections
					do_settings_sections( 'book_a_session_services' );

					// Submit button
					//submit_button();

					$service_list = new Services_List_Table;
					
				}

				if ( $tab == 'bundles' ) {
					
					// Output security fields
					settings_fields( 'book_a_session_options_bundles' );
					
					// Output setting sections
					do_settings_sections( 'book_a_session_bundles' );

					// Submit button
					//submit_button();

					$bundle_list = new Bundles_List_Table;
					
				}

				if ( $tab == 'orders' ) {
					
					// Output security fields
					settings_fields( 'book_a_session_options_orders' );
					
					// Output setting sections
					do_settings_sections( 'book_a_session_orders' );

					// Submit button
					//submit_button();

					$Order_list = new Orders_List_Table;
					
				}

				if ( $tab == 'timetables' ) {
					
					// Output security fields
					settings_fields( 'book_a_session_options_timetables' );
					
					// Output setting sections
					do_settings_sections( 'book_a_session_timetable' );

					// Submit button
					submit_button();
					
				}

				if ( $tab == 'email' ) {

					// Output security fields
					settings_fields( 'book_a_session_options_emails' );
					
					// Output setting sections
					do_settings_sections( 'book_a_session_emails' );

					// Submit button
					submit_button();
					
				}

				if ( $tab == 'invoices' ) {

					?>
					<div class="book-a-session-admin-settings-wrapper">
						<div class="book-a-session-admin-settings-content">
					<?php

					// Output security fields
					settings_fields( 'book_a_session_options_invoices' );
					
					// Output setting sections
					do_settings_sections( 'book_a_session_invoices' );

					// Submit button
					submit_button();
					
					?>
					</div>
						<div class="book-a-session-admin-settings-sidebar">
							<div class="card">
								<h2>Invoice tags</h2>
								<h4>Use the following tags in any fields marked<br><code>Invoice tags supported</code></h4>
								<p>If you type in a tag, including the % symbols, such as <code>%clientfname%</code>, the tags will be replaced dynamically with the data detailed in the description, according to each order and invoice.</p>
								<table>
									<thead>
										<tr>
											<th scope="col">Tag</th>
											<th scope="col">Description</th>
										</tr>
									</thead>
									<tbody>
									<tr>
											<td>
												<code>%sitename%</code>
											</td>
											<td>
												<?php echo get_bloginfo( 'name' ); ?>.
											</td>
										</tr>
										<tr>
											<td>
												<code>%orderid%</code>
											</td>
											<td>
												10 digit order ID number.
											</td>
										</tr>
										<tr>
											<td>
												<code>%quantity%</code>
											</td>
											<td>
												Number of sessions in this order, e.g. 1.
											</td>
										</tr>
										<tr>
											<td>
												<code>%duebytime%</code>
											</td>
											<td>
												Due by time.
											</td>
										</tr>
										<tr>
											<td>
												<code>%duebydate%</code>
											</td>
											<td>
												Due by date.
											</td>
										</tr>
										<tr>
											<td>
												<code>%service%</code>
											</td>
											<td>
												Service name.
											</td>
										</tr>
										<tr>
											<td>
												<code>%location%</code>
											</td>
											<td>
												Location name.
											</td>
										</tr>
										<tr>
											<td>
												<code>%locationaddress%</code>
											</td>
											<td>
												Prints Location address, if available.
											</td>
										</tr>
										<tr>
											<td>
												<code>%paymentmethod%</code>
											</td>
											<td>
												Payment method name.
											</td>
										</tr>
										<tr>
											<td>
												<code>%bookingstatus%</code>
											</td>
											<td>
												Booking status, e.g. Pending.
											</td>
										</tr>
										<tr>
											<td>
												<code>%paymentstatus%</code>
											</td>
											<td>
												Payment status, e.g. Cash payment due.
											</td>
										</tr>
										<tr>
											<td>
												<code>%totaldue%</code>
											</td>
											<td>
												Grand total due to be paid.
											</td>
										</tr>										
										<tr>
											<td>
												<code>%amountpaid%</code>
											</td>
											<td>
												Amount paid toward this invoice.
											</td>
										</tr>
										<tr>
											<td>
												<code>%clientfname%</code>
											</td>
											<td>
												Client's first name.
											</td>
										</tr>
										<tr>
											<td>
												<code>%clientlname%</code>
											</td>
											<td>
												Client's last name.
											</td>
										</tr>										
										<tr>
											<td>
												<code>%clientemail%</code>
											</td>
											<td>
												Client's email address.
											</td>
										</tr>
										<tr>
											<td>
												<code>%practitle%</code>
											</td>
											<td>
												Practitioner's title according to service type, e.g. Designer.
											</td>
										</tr>
										<tr>
											<td>
												<code>%pracfname%</code>
											</td>
											<td>
												Practitioner's first name.
											</td>
										</tr>
										<tr>
											<td>
												<code>%praclname%</code>
											</td>
											<td>
												Practitioner's last name.
											</td>
										</tr>
										<tr>
											<td>
												<code>%pracemail%</code>
											</td>
											<td>
												Practitioner's email address.
											</td>
										</tr>									
										<tr>
											<td>
												<code>%is/was%</code>
											</td>
											<td>
												Prints 'is' if there are any upcoming sessions in the order, or 'was' if there aren't.
											</td>
										</tr>
										<tr>
											<td>
												<code>%first/next/last%</code>
											</td>
											<td>
												Prints 'first' if the order's first of multiple sessions is upcoming, 'next' if the next session won't be their first in the order, and 'last' if all their sessions were completed. Prints nothing if there's only 1 session in the order.
											</td>
										</tr>
										<tr>
											<td>
												<code>%fnltime%</code>
											</td>
											<td>
												First, next, or last session's time.
											</td>
										</tr>
										<tr>
											<td>
												<code>%fnldate%</code>
											</td>
											<td>
												First, next, or last session's date.
											</td>
										</tr>
										<tr>
											<td>
												<code>%session(s)%</code>
											</td>
											<td>
												Prints 'session' if there's only 1 session in the order, or 'sessions' if there are more.
											</td>
										</tr>
										<tr>
											<td>
												<code>%vfcreceiver%</code>
											</td>
											<td>
												Prints the mobile number to receive Vodafone Cash payments if applicable and available.
											</td>
										</tr>
										<tr>
											<td>
												<code>%vfcsender%</code>
											</td>
											<td>
												Prints the mobile number the client has provided that will be sending Vodafone Cash payments, if applicable and available.
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<?php

				}

				if ( $tab == 'payments' ) {

					// Output security fields
					settings_fields( 'book_a_session_options_payments' );
					
					// Output setting sections
					do_settings_sections( 'book_a_session_payments' );

					// Submit button
					//submit_button();
					
				}

			?>
			
		</form>
	</div>
	
	<?php
	
}