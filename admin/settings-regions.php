<?php // Book A Session - Settings Page

if ( ! defined( 'ABSPATH' ) ) exit;

// Init WP Media for image selection here

wp_enqueue_media();

// Edit

if (   ! empty( $_GET['edit'] ) 
    && ! empty( $_GET['region_id'] )
    && ! empty( $_POST['submit_edit_region'] ) ) {
    
    if ( current_user_can( "manage_options" ) && wp_verify_nonce( $_REQUEST['_wpnonce'], 'book_a_session_edit_region_' . $_GET['region_id'] ) ) {

        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_region";
    
        // Validation
    
        if (    ! empty( $_POST['name'] )          
            &&  ! empty( $_POST['description'] )       
            &&  ! empty( $_POST['timezone'] )          
            &&  ! empty( $_POST['currency_id'] )    
            &&  ! empty( $_POST['session_price'] )            
            &&  ! empty( $_POST['code'] )        
            ) {
    
                ! empty( $_POST['international'] )   ? $international = 1                  : $international = 0;
                ! empty( $_POST['sunday_week'] )     ? $sunday_week   = 1                  : $sunday_week   = 0;
                ! empty( $_POST['image_id'] )        ? $image_id      = $_POST['image_id'] : $image_id      = null;

                $value_array = array( 
                'international'     => $international, 
                'name'              => $_POST['name'],
                'description'       => $_POST['description'],
                'timezone'          => $_POST['timezone'], 
                'currency_id'       => $_POST['currency_id'],
                'session_price'     => $_POST['session_price'],
                'code'              => $_POST['code'],
                'sunday_week'       => $sunday_week,
                'image_id'          => $image_id, 
            );
            $edit_region_db_result[] = $wpdb->update( 
                $table_name, 
                $value_array, 
                array( 'id' => $_GET['region_id'] ),
                array( 
                    '%d', // International
                    '%s', // Name
                    '%s', // Description
                    '%s', // Timezone
                    '%d', // Currency ID
                    '%f', // Session Price
                    '%s', // Code
                    '%d'  // Image ID
                    ) 
                );
            
            if ( $edit_region_db_result && ! $international ) {

                $country_array = book_a_session_get_table_array( "country" );
                $region_country_array = book_a_session_get_table_array( "region_country" );
                $table_name_region_country = $wpdb->prefix . "book_a_session_region_country";

                // Region Country Edit

                // Loop through countries

                for ( $i = 0; $i < count( $country_array ); $i++ ) {

                    // When one is checked

                    if ( ! empty( $_POST[ 'country_' . $country_array[$i]->code ] ) ) {

                        // Find out if it's already saved in the table

                        $match_found = false;

                        if ( ! empty( $region_country_array ) ) {

                            for ( $j = 0; $j < count( $region_country_array ); $j++ ) {

                                // If it's saved in the table, nothing needs to happen to it. Record that a match was found and count it to determine overall success later
        
                                if ( $region_country_array[$j]->region_id == $_GET['region_id'] && $region_country_array[$j]->country_code == $country_array[$i]->code ) {
                                    $match_found = true;
                                    break;
                                }
        
                            }

                        }

                        if ( ! $match_found ) {

                            $region_country_value_array = array( 
                                'country_code'  => $country_array[$i]->code,
                                'region_id'     => $_GET['region_id']
                            );
        
                            $region_country_format_array = array(
                                '%s',
                                '%d'
                            );   
                            
                            $edit_region_db_result[] = $wpdb->insert( $table_name_region_country, $region_country_value_array, $region_country_format_array );
        
                        }

                    } else {

                        // If it's not checked, find out if it's saved in the table

                        $match_found = false;

                        if ( ! empty( $region_country_array ) ) {

                            for ( $j = 0; $j < count( $region_country_array ); $j++ ) {

                                // If it's present, declare that a match was found, delete the record, count it as an edit to determine overall success later and break
        
                                if ( $region_country_array[$j]->region_id == $_GET['region_id'] && $region_country_array[$j]->country_code == $country_array[$i]->code ) {

                                    $match_found = true;

                                    $region_country_value_array = array( 
                                        'country_code'  => $country_array[$i]->code,
                                        'region_id'     => $_GET['region_id']
                                    );
                
                                    $region_country_format_array = array(
                                        '%s',
                                        '%d'
                                    );   
                                    
                                    $edit_region_db_result[] = $wpdb->delete( $table_name_region_country, $region_country_value_array, $region_country_format_array );  

                                    break;

                                }
        
                            }

                        }

                    }

                }

                if ( ! in_array( false, $edit_region_db_result, true ) ) {
                    add_action( 'admin_notices', 'book_a_session_admin_notice_edit_success' );
                } elseif ( in_array( false, $edit_region_db_result, true ) && in_array( 1, $edit_region_db_result, true ) ) {
                    add_action( 'admin_notices', 'book_a_session_admin_notice_edit_multiple_warning' );
                } else {
                    add_action( 'admin_notices', 'book_a_session_admin_notice_edit_error' );
                }


            // Delete all region country entries if international is checked
            } elseif ( $international ) {

                $table_name_region_country = $wpdb->prefix . "book_a_session_region_country";
                $delete_region_db_result = $wpdb->delete( $table_name_region_country, array( 'region_id' => $_GET['region_id'] ), array( '%d' ) );

            } else {
                add_action( 'admin_notices', 'book_a_session_admin_notice_edit_error' );
            }   
            
        } else {
            add_action( 'admin_notices', 'book_a_session_admin_notice_validation_error' );
        }

    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_add_security_error' );
    }
        
}

// Delete
if ( isset( $_GET['delete'] ) && isset( $_GET['region_id'] ) && isset( $_GET['page'] ) ) {

    if ( current_user_can( 'manage_options' ) && $_GET['page'] == 'book_a_session_regions' && wp_verify_nonce( $_REQUEST['_wpnonce'], "book_a_session_delete_region_" . $_GET['region_id'] ) ) {

        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_region";
        $table_name_region_country = $wpdb->prefix . "book_a_session_region_country";
        $delete_region_db_result[] = $wpdb->delete( $table_name, array( 'id' => $_GET['region_id'] ), array( '%d' ) );
        $delete_region_db_result[] = $wpdb->delete( $table_name_region_country, array( 'region_id' => $_GET['region_id'] ), array( '%d' ) );
        ! in_array( false, $delete_region_db_result, true ) ? add_action( 'admin_notices', 'book_a_session_admin_notice_delete_success' ) : add_action( 'admin_notices', 'book_a_session_admin_notice_delete_error' );

    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_delete_security_error' );
    }

}

// Add
if ( isset( $_POST['submit_add_region'] ) ) {

    if ( current_user_can( 'manage_options' ) ) {

        ! empty( $_POST['international'] )  ? $international    = 1                     : $international    = 0;
        ! empty( $_POST['sunday_week'] )    ? $sunday_week      = 1                     : $sunday_week      = 0;
        ! empty( $_POST['image_id'] )       ? $image_id         = $_POST['image_id']    : $image_id         = null;

        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_region";

        if (    ! empty( $_POST['name'] )          
            &&  ! empty( $_POST['description'] )       
            &&  ! empty( $_POST['timezone'] )          
            &&  ! empty( $_POST['currency_id'] )    
            &&  ! empty( $_POST['session_price'] )            
            &&  ! empty( $_POST['code'] )        
            ) {
                

            $value_array = array( 
                'international'     => $international, 
                'name'              => $_POST['name'],
                'description'       => $_POST['description'],
                'timezone'          => $_POST['timezone'], 
                'currency_id'       => $_POST['currency_id'],
                'session_price'     => $_POST['session_price'],
                'code'              => $_POST['code'],
                'sunday_week'       => $sunday_week,
                'image_id'          => $image_id, 
            );
            $add_region_db_result[] = $wpdb->insert( 
            $table_name, 
            $value_array, 
            array( 
                '%d', // International
                '%s', // Name
                '%s', // Description
                '%s', // Timezone
                '%d', // Currency ID
                '%f', // Session Price
                '%s', // Code
                '%d'  // Image ID
            ) 
            );
    
            if ( $add_region_db_result && ! $international ) {

                // Get the newly added location's id and use it to populate other tables where applicable

                $added_region = $wpdb->get_row( "SELECT id from $table_name ORDER BY id DESC LIMIT 1", OBJECT );
                $added_region_id = $added_region->id;

                $table_name_region_country = $wpdb->prefix . "book_a_session_region_country";
                $country_array = book_a_session_get_table_array( "country" );

                if ( ! empty( $country_array ) ) {

                    foreach( $country_array as $country ) {

                        if ( $_POST["country_" . $country->code ] ) {

                            $region_country_value_array = array( 
                                'country_code'  => $country->code, 
                                'region_id'     => $added_region_id
                            );
                            $add_region_db_result[] = $wpdb->insert( 
                            $table_name_region_country, 
                            $region_country_value_array, 
                            array( 
                                '%s', 
                                '%d'
                            ) );

                        }

                    }
    
                }

                if ( ! in_array( false, $add_region_db_result, true ) ) {
                    add_action( 'admin_notices', 'book_a_session_admin_notice_add_success' );
                } elseif ( in_array( false, $add_region_db_result, true ) && in_array( 1, $add_region_db_result, true ) ) {
                    add_action( 'admin_notices', 'book_a_session_admin_notice_add_multiple_warning' );
                } else {
                    add_action( 'admin_notices', 'book_a_session_admin_notice_add_error' );
                }

            } else {
                add_action( 'admin_notices', 'book_a_session_admin_notice_add_error' );
            }  
      
        } else {
            add_action( 'admin_notices', 'book_a_session_admin_notice_validation_error' );
        }    
    
    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_add_security_error' );
    }

}

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
 }

if( ! class_exists( 'regions_List_Table' ) ) {
    require_once plugin_dir_path( __FILE__ ) . 'class-regions-list-table.php';
}

function book_a_session_display_regions_settings_page() {
	if ( ! current_user_can( 'manage_options' ) ) return;

    ?>
	<div class="wrap">
		<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
		<?php 

function page_tabs_region( $current = 'View' ) {
    $tabs = array(
        'view'   => __( 'View regions', 'book_a_session' ), 
		'add'  => __( 'Add region', 'book_a_session' ),
		'edit'  => __( 'Edit regions', 'book_a_session' )
    );
    $html = '<h2 class="nav-tab-wrapper">';
    foreach( $tabs as $tab => $name ){
        $class = ( $tab == $current ) ? 'nav-tab-active' : '';
        $html .= '<a class="nav-tab ' . $class . '" href="?page=book_a_session_regions&tab=' . $tab . '">' . $name . '</a>';
    }
    $html .= '</h2>';
    echo $html;
}

// Tabs
$tab = ( ! empty( $_GET['tab'] ) ) ? esc_attr( $_GET['tab'] ) : 'view';
page_tabs_region( $tab );

// View regions
if ( $tab == 'view' ) {

	echo '<h3>View regions</h3>';
	echo '<form>';
	$regions_list = new Regions_List_Table;
    echo '</form>';

}

// Add region
elseif ( $tab == 'add' ) {

    $html =     "<table class='form-table'><form method='POST'>";
    $html .=    "<tr><th class='book-a-session-th-explanation' colspan='2'><h2>Add a new region</h2>";
    $html .=    "<p>Regions allow you to internationalise your business. Prices can be set differently for differing regions, they can be in different currencies, and your session times will be automatically converted to the region's timezone. Regions can be completely international, constituting all countries, or you can specify which countries are in your region, or even specify all countries besides some.</p>";
    $html .=    "<p>Book A Session will attempt to automatically determine a client's region by checking their IP address. If that doesn't work, we may use geolocation, which will prompt the client's browser to display a popup asking the client to allow us to check their location automatically. If they decline, Book A Session would then fall back on asking the client which region they belong to directly on the Services page.</p>";
    $html .=    "Note: You should only have <i>one</i> international region";
    $html .=    "</th></tr>";

    // Name input[type="text"] required
    $html .=    "<tr id='region_add_row_name'><th scope='row'>";
    $html .=    "<label for='name'>Name</label>";
    $html .=    "</th><td><input type='text' name='name' id='region_add_name' required placeholder='e.g. International' style='min-width:300px;'"; 
    if ( isset( $_POST['name'] ) ) {
        $html .= " value='" . $_POST['name'] . "'>";
    } else {
        $html .= ">";
    }
    if ( isset( $_POST['submit_add_region'] ) ) {
        if ( empty( $_POST['name'] ) ) {
          $html .= "<p class='description book-a-session-error'>Please enter the region's name.</p>";
        }
    } 
    $html .=    "</td></tr>";    
    // Description textarea required
    $html .=    "<tr id='region_add_row_description'><th scope='row'>";
    $html .=    "<label for='description'>Description</label>";
    $html .=    '</th><td><textarea type="text" name="description" id="region_add_description" required placeholder="Use this place to notify customers about this region\'s timezone, currency, session price, and perhaps even the places it constitutes." style="min-width:300px;min-height:100px;">'; 
    if ( isset( $_POST['description'] ) ) {
        $html .= esc_textarea( stripslashes( $_POST['description'] ) ) . "</textarea>";
    } else {
        $html .= "</textarea>";
    }
    if ( isset( $_POST['submit_add_region'] ) ) {
        if ( empty( $_POST['description'] ) ) {
          $html .= "<p class='description book-a-session-error'>Please enter the region's description.</p>";
        }
    } 
    $html .=    "</td></tr>";
    // Image WP Media Manager optional
    $html .=    "<tr><th scope='row'>";
	$html .=    "<label for='image_id'>Image (Optional)</label></th>";
    $html .=    "<td>";
    if ( isset( $_POST['image_id'] ) ) {
        $html.=  book_a_session_callback_image_select( array( 
            "option"        => false,
            "image_id"      => $_POST['image_id'],
            "name"          => "image_id"
            ) );
    } else {
        $html .= book_a_session_callback_image_select( array( 
            "option"        => false,
            "name"          => "image_id"
            ) );    
    }
    $html .=    "</td></tr>";
    // Timezone input[type="text"] required
    $html .=    "<tr id='region_add_row_timezone'><th scope='row'>";
    $html .=    "<label for='timezone'>Timezone</label>";
    $html .=    "</th><td><input type='text' name='timezone' id='region_add_timezone' required placeholder='e.g. Africa/Cairo or Europe/London' style='min-width:300px;'"; 
    if ( isset( $_POST['timezone'] ) ) {
        $html .= " value='" . $_POST['timezone'] . "'>";
    } else {
        $html .= ">";
    }
    if ( isset( $_POST['submit_add_region'] ) ) {
        if ( empty( $_POST['timezone'] ) ) {
          $html .= "<p class='description book-a-session-error'>Please enter the region's timezone.</p>";
        }
    } 
    $html .=    "<p class='description'>e.g. Europe/London. If unsure, enter UTC. Find your timezone <a href='https://php.net/manual/en/timezones.php' target='_blank'>here</a>.</p>";
    // Currency select required
    $html .=    "<tr id='region_add_row_currency_id'><th scope='row'>";
    $html .=    "<label for='currency_id'>Currency</label></th>";
    $html .=    "<td><select name='currency_id' id='region_add_currency_id' required>";    
    $currency_array = book_a_session_get_table_array( "currency" );
    if ( ! empty( $currency_array ) ) {
        if ( empty( $_POST['currency_id'] ) ) {
            $html .= "<option selected disabled>Select a currency</option>";
        } else {
            $html .= "<option disabled>Select a currency</option>";
        }
        for ($i = 0; $i < count($currency_array); $i++) {
            $html .= "<option value='" . $currency_array[$i]->id . "'";
            if ( isset( $_POST['submit_add_region'] ) ) {
                if ( ! empty( $_POST['currency_id'] ) ) {
                    if ( $_POST['currency_id'] == $currency_array[$i]->id ) {
                        $html .= " selected";
                    } 
                }
            }
            $html .= ">";
            $html .= $currency_array[$i]->code;
            $html .= "</option>";
        }
        $html .= "</select>";
        if ( isset( $_POST['submit_add_region'] ) ) {
            if ( empty( $_POST['currency_id'] ) ) {
              $html .= "<p class='description book-a-session-error'>Please select a currency.</p>";
            }
        } 
        $html .= "</td>";
    } else {
        $html .= "<option selected disabled>No currencies found</option>";
        $html .= "</select><br><p class='description'>Add a Currency in the Currency settings page</p></td>";
    }
    // Session price
    $html .=    "<tr id='order_add_row_session_price'><th scope='row'>";
    $html .=    "<label for='session_price'>Session price</label></th>";
    $html .=    "<td><input type='number' step='any' min='0' name='session_price' id='region_add_session_price' placeholder='49.99' style='min-width:300px;'";
    if ( isset( $_POST['session_price'] ) ) {
        $html .= " value='" . $_POST['session_price'] . "'>";
    } else {
        $html .= ">";
    }
    $html .=    "<p class='description'>Enter the amount on its own, without any letters or symbols.</p>";
    if ( isset( $_POST['submit_add_region'] ) ) {
        if ( empty( $_POST['session_price'] ) ) {
          $html .= "<p class='description book-a-session-error'>Please enter the region's session price.</p>";
        }
    } 
    $html .=    "</td></tr>";
    // Country code input[type="text"] required
    $html .=    "<tr id='region_add_row_code'><th scope='row'>";
    $html .=    "<label for='code'>Country code</label>";
    $html .=    "</th><td><input type='text' name='code' id='region_add_code' required placeholder='2 letters, e.g. US or GB' maxlength='2' style='min-width:300px;'"; 
    if ( isset( $_POST['code'] ) ) {
        $html .= " value='" . $_POST['code'] . "'>";
    } else {
        $html .= ">";
    }
    if ( isset( $_POST['submit_add_region'] ) ) {
        if ( empty( $_POST['code'] ) ) {
          $html .= "<p class='description book-a-session-error'>Please enter the country code.</p>";
        }
    } 
    $html .=    "<p class='description'>If this region constitutes several countries, use the country code of the currency you've selected. So if you're using USD, enter US. Find your country code <a href='https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements' target='_blank'>here</a>.</p>";
    // Sunday Week checkbox required
    $html .=    "<tr id='region_add_row_sunday_week'><th scope='row'>";
    $html .=    "<label for='sunday_week'>Week starts on Sunday?</label>";
    $html .=    "</th><td><label for='sunday_week'><input type='checkbox' id='sunday_week' name='sunday_week' value='1'"; 
    if ( ! empty( $_POST['sunday_week'] ) ) {
        $html .= " checked>";
    } else {
        $html .= ">";
    }
    $html .=    "Check if this region's weeks start on Sunday.</label>";
    $html .=    "<p class='description'>This will change any visible calendars for clients from a region that has this option checked.</p>";
    $html .=    "</td></tr>";
    // International checkbox required
    $html .=    "<tr id='region_add_row_international'><th scope='row'>";
    $html .=    "<label for='international'>International</label>";
    $html .=    "</th><td><label for='international'><input type='checkbox' id='international' name='international' value='1'"; 
    if ( ! empty( $_POST['international'] ) ) {
        $html .= " checked>";
    } else {
        $html .= ">";
    }
    $html .=    "Check if this region is international.</label>";
    $html .=    "<p class='description'>Checking this allows any Location belonging to this Region to appear as an option to any customers despite the Region they selected. This is useful for offering online or phone sessions, where Online or Phone could be locations in the international region, available to all.</p>";    
    $html .=    "</td></tr>";
    // Countries checkbox
    $html .=    "<tr class='region_add_countries'><th scope='row'><label>Countries</label><p class='description'>Check the countries in this region.</p>";
    $html .=    "<a href='javascript:void(0)' class='select-all-country'>Select all</a>&nbsp;&nbsp;<a href='javascript:void(0)' class='unselect-all-country'>Unselect all</a>";
    $html .=    "</th><td class='countries-checkboxes'>";
    $html .=    "<input type='search' name='search' id='country_search' class='book-a-session-search book-a-session-country-search' placeholder='Search'>";
    $html .=    "<div>";
    $country_array = book_a_session_get_table_array( "country" );
    if ( ! empty( $country_array ) ) {
        for ($i = 0; $i < count( $country_array ); $i++) {
                $html .=    "<label for='country_" . $country_array[$i]->code . "' class='country-label'><input class='country' type='checkbox' id='country_" . $country_array[$i]->code . "' name='country_" . $country_array[$i]->code . "' value='1'"; 
            if ( ! empty( $_POST['country_' . $country_array[$i]->code ] ) ) {
                $html .= " checked>";
            } else {
                $html .= ">";
            }
            $html .= $country_array[$i]->name . "</label>";  
        }
    } else {
        $html .=    "<tr><th scope='row' colspan='2'><p class='description'>No countries found. Please contact the plugin developer.</p></th></tr>";
    }
    $html .=    "</div>";

    $html .=    "</tbody></table>";
    $html .=    "<input type='submit' class='button button-primary' value='Add region' name='submit_add_region'>";
    $html .=    "</form>";

	
    echo $html;

} elseif ( $tab == 'edit' ) {

    if ( isset( $_GET["edit"] ) && ! empty( $_GET['region_id'] )  ) {

        echo '<h3>Edit a region</h3>';
        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_region";
        $table_name_region_country = $wpdb->prefix . "book_a_session_region_country";
        
        $id = intval( $_GET['region_id'] );

        $row_to_edit = $wpdb->get_row("
        SELECT  *
        FROM    $table_name
        WHERE   id = $id", OBJECT );

        $region_country = $wpdb->get_results("
        SELECT  *
        FROM    $table_name_region_country
        WHERE   region_id = $id", OBJECT );


        $html =     "<table class='form-table'><form method='POST'>";

        // Name input[type="text"] required
        $html .=    "<tr id='region_edit_row_name'><th scope='row'>";
        $html .=    "<label for='name'>Name</label>";
        $html .=    "</th><td><input type='text' name='name' id='region_edit_name' required placeholder='e.g. International' style='min-width:300px;'"; 
        if ( isset( $row_to_edit->name ) ) {
            $html .= " value='" . $row_to_edit->name . "'>";
        } else {
            $html .= ">";
        }
        if ( isset( $_POST['submit_edit_region'] ) ) {
            if ( empty( $_POST['name'] ) ) {
            $html .= "<p class='description book-a-session-error'>Please enter the region's name.</p>";
            }
        } 
        $html .=    "</td></tr>";    
        // Description textarea required
        $html .=    "<tr id='region_edit_row_description'><th scope='row'>";
        $html .=    "<label for='description'>Description</label>";
        $html .=    '</th><td><textarea type="text" name="description" id="region_edit_description" required placeholder="Use this place to notify customers about this region\'s timezone, currency, session price, and perhaps even the places it constitutes." style="min-width:300px;min-height:100px;">'; 
        if ( isset( $row_to_edit->description ) ) {
            $html .= esc_textarea( stripslashes( $row_to_edit->description ) ) . "</textarea>";
        } else {
            $html .= "</textarea>";
        }
        if ( isset( $_POST['submit_edit_region'] ) ) {
            if ( empty( $_POST['description'] ) ) {
            $html .= "<p class='description book-a-session-error'>Please enter the region's description.</p>";
            }
        } 
        $html .=    "</td></tr>";
        // Image WP Media Manager optional
        $html .=    "<tr><th scope='row'>";
        $html .=    "<label for='name'>Image (Optional)</label></th>";
        $html .=    "<td>";

        if ( isset( $row_to_edit->image_id ) ) {
            $html.=  book_a_session_callback_image_select( array( 
                "option"        => false,
                "image_id"      => $row_to_edit->image_id,
                "name"          => "image_id"
                ) );
        } else {
            $html .= book_a_session_callback_image_select( array( 
                "option"        => false,
                "name"          => "image_id"
                ) );    
        }
        $html .=    "</td></tr>";
        // Timezone input[type="text"] required
        $html .=    "<tr id='region_edit_row_timezone'><th scope='row'>";
        $html .=    "<label for='timezone'>Timezone</label>";
        $html .=    "</th><td><input type='text' name='timezone' id='region_edit_timezone' required placeholder='e.g. Africa/Cario or Europe/London' style='min-width:300px;'"; 
        if ( isset( $row_to_edit->timezone ) ) {
            $html .= " value='" . $row_to_edit->timezone . "'>";
        } else {
            $html .= ">";
        }
        if ( isset( $_POST['submit_edit_region'] ) ) {
            if ( empty( $_POST['timezone'] ) ) {
            $html .= "<p class='description book-a-session-error'>Please enter the region's timezone.</p>";
            }
        } 
        $html .=    "<p class='description'>e.g. Europe/London. Find your timezone <a href='https://php.net/manual/en/timezones.php' target='_blank'>here</a>.</p>";
        // Currency select required
        $html .=    "<tr id='region_edit_row_currency_id'><th scope='row'>";
        $html .=    "<label for='currency_id'>Currency</label></th>";
        $html .=    "<td><select name='currency_id' id='region_edit_currency_id' required>";    
        $currency_array = book_a_session_get_table_array( "currency" );
        if ( ! empty( $currency_array ) ) {
            for ($i = 0; $i < count($currency_array); $i++) {
                $html .= "<option value='" . $currency_array[$i]->id . "'";
                if ( isset( $row_to_edit->currency_id ) ) {
                    if ( $row_to_edit->currency_id == $currency_array[$i]->id ) {
                        $html .= " selected";
                }                
            }
        $html .= ">";
                $html .= $currency_array[$i]->code;
                $html .= "</option>";
            }
            $html .= "</select>";
            if ( isset( $_POST['submit_edit_region'] ) ) {
                if ( empty( $_POST['currency_id'] ) ) {
                $html .= "<p class='description book-a-session-error'>Please select a currency.</p>";
                }
            } 
            $html .= "</td>";
        } else {
            $html .= "<option selected disabled>No currencies found</option>";
            $html .= "</select><br><p class='description'>Add a Currency in the Currency settings page</p></td>";
        }
        // Session price
        $html .=    "<tr id='region_edit_row_session_price'><th scope='row'>";
        $html .=    "<label for='session_price'>Session price</label></th>";
        $html .=    "<td><input type='number' step='any' min='0' name='session_price' id='region_edit_session_price' placeholder='49.99' style='min-width:300px;'";
        if ( isset( $row_to_edit->session_price ) ) {
            $html .= " value='" . $row_to_edit->session_price . "'>";
        } else {
            $html .= ">";
        }
        $html .=    "<p class='description'>Enter the amount on its own, without any letters or symbols.</p>";
        if ( isset( $_POST['submit_edit_region'] ) ) {
            if ( empty( $_POST['session_price'] ) ) {
            $html .= "<p class='description book-a-session-error'>Please enter the region's session price.</p>";
            }
        } 
        $html .=    "</td></tr>";
        // Country code input[type="text"] required
        $html .=    "<tr id='region_edit_row_code'><th scope='row'>";
        $html .=    "<label for='code'>Country code</label>";
        $html .=    "</th><td><input type='text' name='code' id='region_edit_code' required placeholder='2 letters, e.g. US or GB' maxlength='2' style='min-width:300px;'"; 
        if ( isset( $row_to_edit->code ) ) {
            $html .= " value='" . $row_to_edit->code . "'>";
        } else {
            $html .= ">";
        }
        if ( isset( $_POST['submit_edit_region'] ) ) {
            if ( empty( $_POST['code'] ) ) {
            $html .= "<p class='description book-a-session-error'>Please enter the country code.</p>";
            }
        } 
        $html .=    "<p class='description'>If this region constitutes several countries, use the country code of the currency you've selected. So if you're using USD, enter US. Find your country code <a href='https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Officially_assigned_code_elements' target='_blank'>here</a>.</p>";
        // International checkbox required
        $html .=    "<tr id='region_edit_row_international'><th scope='row'>";
        $html .=    "<label for='international'>International</label>";
        $html .=    "</th><td><label for='international'><input type='checkbox' id='international' name='international' value='1'"; 
        if ( ! empty( $row_to_edit->international ) ) {
            $html .= " checked>";
        } else {
            $html .= ">";
        }
        $html .=    "Check if this region is international.</label>";
        $html .=    "<p class='description'>Checking this allows any Location belonging to this Region to appear as an option to any customers despite the Region they selected. This is useful for offering online or phone sessions.</p>";    
        $html .=    "</td></tr>";
        // Sunday Week checkbox required
        $html .=    "<tr id='region_edit_row_sunday_week'><th scope='row'>";
        $html .=    "<label for='sunday_week'>Week starts on Sunday?</label>";
        $html .=    "</th><td><label for='sunday_week'><input type='checkbox' id='sunday_week' name='sunday_week' value='1'"; 
        if ( ! empty( $row_to_edit->sunday_week ) ) {
            $html .= " checked>";
        } else {
            $html .= ">";
        }
        $html .=    "Check if this region's weeks start on Sunday.</label>";
        $html .=    "</td></tr>";
        // Countries checkbox
        $html .=    "<tr class='region_edit_countries'><th scope='row'><label>Countries</label><p class='description'>Check the countries in this region.</p>";
        $html .=    "<a href='javascript:void(0)' class='select-all-country'>Select all</a>&nbsp;&nbsp;<a href='javascript:void(0)' class='unselect-all-country'>Unselect all</a>";
        $html .=    "</th><td class='countries-checkboxes'>";
        $html .=    "<input type='search' name='search' id='country_search' class='book-a-session-search book-a-session-country-search' placeholder='Search'>";
        $html .=    "<div>";
        $country_array = book_a_session_get_table_array( "country" );
        if ( ! empty( $country_array ) ) {
            for ($i = 0; $i < count( $country_array ); $i++) {
                    $html .=    "<label for='country_" . $country_array[$i]->code . "' class='country-label'><input class='country' type='checkbox' id='country_" . $country_array[$i]->code . "' name='country_" . $country_array[$i]->code . "' value='1'"; 
                    $match = false;
                    for ($j = 0; $j < count( $region_country ); $j++) {
                        if ( isset( $region_country[$j]->country_code ) ) {
                            if ( $region_country[$j]->country_code == $country_array[$i]->code ) {
                                $html .= " checked>";
                                $match = true;
                            }
                        }
                    }
                    if ( ! $match ) {
                        $html .= ">";
                    } 
                    $html .= $country_array[$i]->name . "</label>";  
            }
        } else {
            $html .=    "<tr><th scope='row' colspan='2'><p class='description'>No countries found. Please contact the plugin developer.</p></th></tr>";
        }
        $html .=    "</div>";
        $html .=    "</tbody></table>";
        $html .=    "<input type='submit' class='button button-primary' value='Save changes' name='submit_edit_region'>";
        $html .=    "</form>";
        
        echo $html;

    } else {

        echo '<h3>Select a region to edit</h3>';
        echo '<form>';
        $regions_list = new Regions_List_Table;
        echo '</form>';

    }

} else {

	echo '<h3>View regions</h3>';
	echo '<form>';
	$regions_list = new Regions_List_Table;
	echo '</form>';

}
// Code after the tabs (outside)
		?>


	</div>

	<?php

}