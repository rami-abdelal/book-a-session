<?php

if (!class_exists('WP_List_Table')) {
    require_once (ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
 
}

class Bundles_List_Table extends WP_List_Table  {


    public function __construct()
    {
        parent::__construct(array(
            'singular' => 'bundle',
            'plural' => 'bundles',
            'ajax' => true
        ));

        $this->prepare_items();
        $this->display();
        
    }
    public function prepare_items()
    {
        $this->_column_headers = $this->get_column_info();
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array(
            $columns,
            $hidden,
            $sortable
        );
        
        $this->process_bulk_action();
        $per_page = $this->get_items_per_page('records_per_page', 10);
        $current_page = $this->get_pagenum();
        $total_items = self::record_count();
        $data = self::get_records($per_page, $current_page);
        $this->set_pagination_args(
                          ['total_items' => $total_items, 
                       'per_page' => $per_page 
                      ]);
        $this->items = $data;
        }


    /** * 
    *Retrieve records data from the database
    * * @param int $per_page
    * @param int $page_number
    * * @return mixed
    */
    public static function get_records($per_page = 10, $page_number = 1)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'book_a_session_bundle';

        $sql = "SELECT
                *
                FROM " .  
                $table_name;

        /*if (isset($_REQUEST['s'])) {
        $sql.= ' where column1 LIKE "%' . $_REQUEST['s'] . '%" or column2 LIKE "%' . $_REQUEST['s'] . '%"';
        }*/
            
        if (!empty($_REQUEST['orderby'])) {
                $sql.= ' ORDER BY ' . esc_sql($_REQUEST['orderby']);
            $sql.= !empty($_REQUEST['order']) ? ' ' . esc_sql($_REQUEST['order']) : ' ASC';
        } else {
            $sql .= " ORDER BY quantity ASC";
        }
        $sql.= " LIMIT $per_page";
        $sql.= ' OFFSET ' . ($page_number - 1) * $per_page;
        $result = $wpdb->get_results($sql, 'ARRAY_A');
        return $result;
    }

    function get_columns()
        {
            $columns = [
                'name'      =>__('Name'),
                'quantity'  =>__('Quantity'),
                'payment_methods' =>__('Payment Methods'),
                'discounts' =>__('Discount per Session'),
                'actions'   =>__('Actions')
                  ];
            return $columns;
        }       

    public function get_hidden_columns()
    {
        // Setup Hidden columns and return them
        return array(

        );
    }

    /** 
    * Columns to make sortable. 
    * * @return array 
    */
    public function get_sortable_columns()
    {
        $sortable_columns = array(
            'quantity'  =>array('quantity', true),
            'name'      =>array('name',     true)
            
          );
        return $sortable_columns;
    }

    /** 
    *Text displayed when no record data is available 
    */
    public function no_items()
    {
        _e('No bundles found.', 'bx');
    }

    /** 
    * Returns the count of records in the database. 
    * * @return null|string 
    */
    public static function record_count()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'book_a_session_bundle';

        $sql = "SELECT
                COUNT(*) 
                FROM " . 
                $table_name;
        return $wpdb->get_var($sql);
    }

        public function column_default($item, $column_name) {
            $currency_array = book_a_session_get_table_array( "currency" );
            $bundle_currency_array = book_a_session_get_table_array( "bundle_currency", null, null, "*", array( "quantity", "=", $item["quantity"] ) );
            $payment_method_array = book_a_session_get_table_array( "payment_method" );
            $bundle_payment_method_array = book_a_session_get_table_array( "bundle_payment_method", null, null, "*", array( "quantity", "=", $item["quantity"] ) );

            $nonce_delete = "&_wpnonce=" . wp_create_nonce( 'book_a_session_delete_bundle_' . $item["quantity"] );
            $nonce_edit = "&_wpnonce=" . wp_create_nonce( 'book_a_session_edit_bundle_' . $item["quantity"] );            
            $admin_edit_page_url = 'admin.php?page=book_a_session_bundles&tab=edit'; 

            switch ($column_name) {

                case "quantity"     : echo $item["quantity"]; break;
                case "name"         : echo $item["name"]; break;

                case "payment_methods" : echo "<table><tbody>"; 
                for ( $i = 0; $i < count( $payment_method_array ); $i++ ) {

                    $no_match = true;


                    for ( $j = 0; $j < count( $bundle_payment_method_array ); $j++ ) {
                        
                        if ( ! empty( $bundle_payment_method_array[$j]->quantity ) && ! empty( $bundle_payment_method_array[$j]->payment_method_id ) ) {

                            if ( $bundle_payment_method_array[$j]->quantity == $item["quantity"] && $bundle_payment_method_array[$j]->payment_method_id == $payment_method_array[$i]->id ){

                                echo "<tr class='book-a-session-admin-accepted'><th scope='col'>" . book_a_session_get_payment_method( $payment_method_array[$i]->id ) . "</th><td>&#10003;</td></tr>";
                                $no_match = false;
                            } 

                        }                        

                    }

                    if ( $no_match ) {                                                                  
                        echo "<tr class='book-a-session-admin-declined'><th scope='col'>" . book_a_session_get_payment_method( $payment_method_array[$i]->id ) . "</th><td>&mdash;</td></tr>";
                    }

                }
                echo "</tbody></table>";
                break;

                case "discounts"    : echo "<table><tbody>";

                $match = false;

                    if( ! empty( $currency_array ) ) {

                        for ( $i = 0; $i < count( $currency_array ); $i++ ) {

                            if ( ! empty( $bundle_currency_array ) ) {

                                for ( $j = 0; $j < count( $bundle_currency_array ); $j++ ) {
                                    
                                    if ( ! empty( $bundle_currency_array[$j]->quantity ) && ! empty( $bundle_currency_array[$j]->currency_id ) ) {
        
                                        if ( $bundle_currency_array[$j]->quantity == $item["quantity"] && $bundle_currency_array[$j]->currency_id == $currency_array[$i]->id ){
        
                                            echo "<tr><th scope='col'>" . $currency_array[$i]->code . "</th><td>"; 
                                            echo ( $bundle_currency_array[$j]->discount > 0.00 ) ? book_a_session_get_price_tag( $bundle_currency_array[$j]->discount, $bundle_currency_array[$j]->currency_id ) : "&mdash;";
                                            echo "</td></tr>";
                                            $match = true;
                                            
                                        } 
        
                                    }                        
        
                                }

                            }
                        }
    
                    }

                    if ( ! $match ) {

                        echo '&mdash;';

                    }

                echo "</tbody></table>"; break;
                
                case "actions"  : echo   "<a href='" . $admin_edit_page_url . $nonce_edit . "&edit=1&quantity=" . $item["quantity"] . "'>Edit</a>" . 
                                         "&nbsp;&nbsp;&nbsp;" . 
                                         "<a class='book-a-session-row-delete' href='" . $admin_edit_page_url . $nonce_delete . "&delete=1&quantity=" . $item["quantity"] . "'>Delete</a>"; 
                break;
                return $item[ $column_name ];
                    default:
                        return $item[ $column_name ] ;
            
                }

            //return $item[ $column_name ];
        }

}