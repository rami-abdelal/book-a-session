<?php // Book A Session - Services Settings Page

if ( ! defined( 'ABSPATH' ) ) exit;

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
 }

if( ! class_exists( 'Services_List_Table' ) ) {
    require_once plugin_dir_path( __FILE__ ) . 'class-services-list-table.php';
}

// Init WP Media for image selection here

wp_enqueue_media();

// Edit

if (   ! empty( $_GET['edit'] ) 
    && ! empty( $_GET['service_id'] )
    && ! empty( $_POST['submit_edit_service'] ) ) {
    
    // Security
    if ( current_user_can( 'manage_options' ) && wp_verify_nonce( $_REQUEST["_wpnonce"], 'book_a_session_edit_service_' . $_GET['service_id'] ) ) {

        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_service";
        $table_name_service_price = $wpdb->prefix . "book_a_session_service_price";
        $table_name_service_location = $wpdb->prefix . "book_a_session_service_location";        
        $service_price_array = book_a_session_get_table_array( "service_price" );
        $currency_array = book_a_session_get_table_array( "currency" );
        $location_array = book_a_session_get_table_array( "location" );
        $service_location_array = book_a_session_get_table_array( "service_location" );
        $count_edit = 0;
        $count_no_edit = 0;
    
        // Validation
        if (    ! empty( $_POST['name'] )          
            &&  ! empty( $_POST['description'] )       
            &&  ! empty( $_POST['service_type_id'] )          
            ) {
            ! empty( $_POST['image_id'] ) ? $image_id = $_POST['image_id'] : $image_id = null;
            $value_array = array( 
                'name'              => $_POST['name'],
                'description'       => $_POST['description'],
                'image_id'          => $image_id, 
                'service_type_id'   => $_POST['service_type_id']
            );
            $edit_service_db_result[] = $wpdb->update( 
                $table_name, 
                $value_array, 
                array( 'id' => $_GET['service_id'] ),
                array( 
                    '%s', 
                    '%s',
                    '%d',
                    '%d'
                    ) 
                );


            // Service Price Edit

            for ( $i = 0; $i < count( $currency_array ); $i++ ) {

                $match_found = false;

                // When one isn't empty

                if ( ! empty( $_POST[ 'currency_' . $currency_array[$i]->id ] ) ) {

                    // Find out if it's already saved in the table

                    if ( ! empty( $service_price_array ) ) {

                        for ( $j = 0; $j < count( $service_price_array ); $j++ ) {

                            // If it's saved in the table, update it. Record that a match was found and count it to determine overall success later
    
                            if ( $service_price_array[$j]->service_id == $_GET['service_id'] && $service_price_array[$j]->currency_id == $currency_array[$i]->id ) {

                                $match_found = true;

                                $service_price_where_array = array(
                                    'service_id'       => $_GET['service_id'], 
                                    'currency_id'       => $currency_array[$i]->id
                                );

                                $service_price_where_format_array = array(
                                    '%d',
                                    '%d'
                                );   

                                $service_price_value_array = array( 
                                    'session_price'   => $_POST[ 'currency_' . $currency_array[$i]->id ]
                                );
            
                                $service_price_value_format_array = array(
                                    '%f'
                                );   
                                
                                $edit_service_db_result[] = $wpdb->update( 
                                    $table_name_service_price, 
                                    $service_price_value_array, 
                                    $service_price_where_array, 
                                    $service_price_value_format_array, 
                                    $service_price_where_format_array 
                                );
                                break;
                            }
    
                        }

                    }

                    // If it doesn't already exist, insert

                    if ( ! $match_found ) {

                        $service_price_value_array = array( 
                            'service_id'       => $_GET['service_id'], 
                            'currency_id'      => $currency_array[$i]->id,
                            'session_price'    => $_POST[ 'currency_' . $currency_array[$i]->id ]
                        );
    
                        $service_price_format_array = array(
                            '%d',
                            '%d',
                            '%f'
                        );   
                        
                        $edit_service_db_result[] = $wpdb->insert( $table_name_service_price, $service_price_value_array, $service_price_format_array );
    
                    }

                } else {

                    // If there's no value posted, find out if it's saved in the table

                    if ( ! empty( $service_price_array ) ) {

                        for ( $j = 0; $j < count( $service_price_array ); $j++ ) {

                            // But if there are and if it's present, declare that a match was found, delete the record, count it as an edit to determine overall success later and break
    
                            if ( $service_price_array[$j]->service_id == $_GET['service_id'] && $service_price_array[$j]->currency_id == $currency_array[$i]->id ) {

                                $service_price_where_array = array( 
                                    'service_id'    => $_GET['service_id'], 
                                    'currency_id'   => $currency_array[$i]->id
                                );
            
                                $service_price_format_array = array(
                                    '%d',
                                    '%d'
                                );   
                                
                                $edit_service_db_result[] = $wpdb->delete( $table_name_service_price, $service_price_where_array, $service_price_format_array );  
                                break;

                            }
    
                        }

                    }

                }

            }
            
            // Service Locations Edit

            for ( $i = 0; $i < count( $location_array ); $i++ ) {

                // When one is checked

                if ( ! empty( $_POST[ 'location_' . $location_array[$i]->id ] ) ) {

                    // Find out if it's already saved in the table

                    $match_found = false;

                    if ( ! empty( $service_location_array ) ) {

                        for ( $j = 0; $j < count( $service_location_array ); $j++ ) {

                            // If it's saved in the table, nothing needs to happen to it. Record that a match was found and count it to determine overall success later
    
                            if ( $service_location_array[$j]->service_id == $_GET['service_id'] && $service_location_array[$j]->location_id == $location_array[$i]->id ) {
                                $match_found = true;
                                break;
                            }
    
                        }

                    }

                    if ( ! $match_found ) {

                        $service_location_value_array = array( 
                            'service_id'    => $_GET['service_id'], 
                            'location_id'   => $location_array[$i]->id
                        );
    
                        $service_location_format_array = array(
                            '%d',
                            '%d'
                        );   
                        
                        $edit_service_db_result[] = $wpdb->insert( $table_name_service_location, $service_location_value_array, $service_location_format_array );
                        $count_edit++;
    
                    }

                } else {

                    // If it's not checked, find out if it's saved in the table

                    if ( ! empty( $service_location_array ) ) {

                        for ( $j = 0; $j < count( $service_location_array ); $j++ ) {

                            // If it's present, delete the record and break
    
                            if ( $service_location_array[$j]->service_id == $_GET['service_id'] && $service_location_array[$j]->location_id == $location_array[$i]->id ) {

                                $match_found = true;

                                $service_location_value_array = array( 
                                    'service_id'    => $_GET['service_id'], 
                                    'location_id'   => $location_array[$i]->id
                                );
            
                                $service_location_format_array = array(
                                    '%d',
                                    '%d'
                                );   
                                
                                $edit_service_db_result[] = $wpdb->delete( $table_name_service_location, $service_location_value_array, $service_location_format_array );  
                                break;

                            }
    
                        }

                    }

                }

            }
   

            // If all changes were saved without issue
            if ( ! in_array( false, $edit_service_db_result, true ) ) {
                add_action( 'admin_notices', 'book_a_session_admin_notice_edit_multiple_success' );
            // Or if that's not the case, but there's still some other failure. Double check this as I believe, as it stands, the else would never fire off even when everything has failed. Checking extra edits besides the main location row edit is a moot exercise as if the extras were attempted, the first must have worked
            } else if ( in_array( false, $edit_service_db_result, true ) ) {
                add_action( 'admin_notices', 'book_a_session_admin_notice_edit_multiple_warning' );
            } else {
                add_action( 'admin_notices', 'book_a_session_admin_notice_edit_error' );            
            }
    
        } else {
            add_action( 'admin_notices', 'book_a_session_admin_notice_validation_error' );
        }

    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_edit_security_error' );
    }
          
}

// Delete
if ( isset( $_GET['delete'] ) && isset( $_GET['service_id'] ) && isset( $_GET['page'] ) ) {

    if ( current_user_can( 'manage_options' ) && wp_verify_nonce( $_REQUEST["_wpnonce"], 'book_a_session_delete_service_' . $_GET['service_id'] ) && $_GET['page'] == 'book_a_session_services' ) {

        global $wpdb;
        $table_name                     = $wpdb->prefix . "book_a_session_service";
        $table_name_service_price       = $wpdb->prefix . "book_a_session_service_price";
        $table_name_service_location    = $wpdb->prefix . "book_a_session_service_location";
        $delete_service_db_result[]     = $wpdb->delete( $table_name, array( 'id' => $_GET['service_id'] ), array( '%d' ) );
        $delete_service_db_result[]     = $wpdb->delete( $table_name_service_price, array( 'service_id' => $_GET['service_id'] ), array( '%d' ) );
        $delete_service_db_result[]     = $wpdb->delete( $table_name_service_location, array( 'service_id' => $_GET['service_id'] ), array( '%d' ) );

        ! in_array( false, $delete_service_db_result, true ) ? add_action( 'admin_notices', 'book_a_session_admin_notice_delete_success' ) : add_action( 'admin_notices', 'book_a_session_admin_notice_delete_error' );

    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_delete_security_error' );
    }

}

// Add
if ( isset( $_POST['submit_add_service'] ) ) {

    global $wpdb;
    $table_name = $wpdb->prefix . "book_a_session_service";
    $currency_array = book_a_session_get_table_array( "currency" );
    $location_array = book_a_session_get_table_array( "location" );
    $add_service_db_result;
    
    if ( current_user_can( "manage_options" ) ) {

        // Validation

        if (! empty( $_POST['name'] )              &&
            ! empty( $_POST['description'] )       &&    
            ! empty( $_POST['service_type_id'] )   
            ) {
            ! empty( $_POST['image_id'] ) ? $image_id = $_POST['image_id'] : $image_id = null;
            $value_array = array( 
                'name'              => $_POST['name'],
                'description'       => $_POST['description'],
                'image_id'          => $image_id, 
                'service_type_id'   => $_POST['service_type_id']
            );
            $add_service_db_result[] = $wpdb->insert( 
                $table_name, 
                $value_array,
                array( 
                    '%s', 
                    '%s',
                    '%d',
                    '%d'
                    ) 
                );

            if ( ! empty( $add_service_db_result ) ) {

                // Get the newly added service's id and use it to populate other tables where applicable

                $added_service = $wpdb->get_row( "SELECT id from $table_name ORDER BY id DESC LIMIT 1", OBJECT );
                $added_service_id = $added_service->id;

                // Service Price Insertion

                if ( ! empty( $currency_array ) ) {

                    $table_name_service_price = $wpdb->prefix . "book_a_session_service_price";

                    for ( $i = 0; $i < count( $currency_array ); $i++ ) {
    
                        if ( ! empty( $_POST[ 'currency_' . $currency_array[$i]->id ] ) ) {
    
                            $service_price_value_array = array( 
                                'service_id'    => $added_service_id, 
                                'currency_id'   => $currency_array[$i]->id,
                                'session_price' => $_POST[ 'currency_' . $currency_array[$i]->id ]
                            );
    
                            $service_price_format_array = array(
                                '%d',
                                '%d',
                                '%f'
                            ); 
    
                            $add_service_db_result_array[] = $wpdb->insert( $table_name_service_price, $service_price_value_array, $service_price_format_array );
        
                        }
    
                    }                
    
                }
                
                // Service Location Insertion

                if ( ! empty( $location_array ) ) {

                    $table_name_service_location = $wpdb->prefix . "book_a_session_service_location";

                    for ( $i = 0; $i < count( $location_array ); $i++ ) {

                        if ( ! empty( $_POST[ 'location_' . $location_array[$i]->id ] ) ) {
    
                            $service_location_value_array = array( 
                                'service_id'    => $added_service_id, 
                                'location_id'   => $location_array[$i]->id
                            );
    
                            $service_location_format_array = array(
                                '%d',
                                '%d'
                            ); 
    
                            $add_service_db_result_array[] = $wpdb->insert( $table_name_service_location, $service_location_value_array, $service_location_format_array );
        
                        }
    
                    }
    
                }

                if ( book_a_session_all_truthy_or_falsey( $add_service_db_result_array ) ) {
                    add_action( 'admin_notices', 'book_a_session_admin_notice_add_multiple_success' );
                } else {
                    add_action( 'admin_notices', 'book_a_session_admin_notice_add_multiple_warning' );
                }

            } else {
                // If the service wasn't added, don't try to add the extras, just show an error.
                add_action( 'admin_notices', 'book_a_session_admin_notice_add_error' );
            }


        } else {
            add_action( 'admin_notices', 'book_a_session_admin_notice_validation_error' );
        }

    } else {
        add_action( 'admin_notices', 'book_a_session_admin_notice_add_security_error' ); 
    }

}


function book_a_session_display_services_settings_page() {
	if ( ! current_user_can( 'manage_options' ) ) return;

    ?>
	<div class="wrap">
		<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
		<?php 

function page_tabs_service( $current = 'View' ) {
    $tabs = array(
        'view'   => __( 'View services', 'book_a_session' ), 
		'add'  => __( 'Add service', 'book_a_session' ),
		'edit'  => __( 'Edit services', 'book_a_session' )
    );
    $html = '<h2 class="nav-tab-wrapper">';
    foreach( $tabs as $tab => $name ){
        $class = ( $tab == $current ) ? 'nav-tab-active' : '';
        $html .= '<a class="nav-tab ' . $class . '" href="?page=book_a_session_services&tab=' . $tab . '">' . $name . '</a>';
    }
    $html .= '</h2>';
    echo $html;
}

// Tabs
$tab = ( ! empty( $_GET['tab'] ) ) ? esc_attr( $_GET['tab'] ) : 'view';
page_tabs_service( $tab );

// View services
if ( $tab == 'view' ) {

	echo '<h3>View services</h3>';
	echo '<form>';
	$services_list = new Services_List_Table;
	echo '</form>';

}

// Add service
elseif ( $tab == 'add' ) {

	echo '<h3>Add a new service</h3>';

	$html =     "<table class='form-table'><form method='POST'>";
	
    // Service Type select required
    $html .=    "<tr id='region_add_row_service_type_id'><th scope='row'>";
    $html .=    "<label for='service_type_id'>Service Type</label></th>";
    $html .=    "<td><select name='service_type_id' id='service_type_id' required>";    
    $service_type_array = book_a_session_get_table_array( "service_type" );
    if ( ! empty( $service_type_array ) ) {
        if ( empty( $_POST['service_type_id'] ) ) {
            $html .= "<option selected disabled>Select a service type</option>";
        } else {
            $html .= "<option disabled>Select a service type</option>";
        }
        for ($i = 0; $i < count($service_type_array); $i++) {
            $html .= "<option value='" . $service_type_array[$i]->id . "'";
            if ( isset( $_POST['submit_add_service'] ) ) {
                if ( ! empty( $_POST['service_type_id'] ) ) {
                    if ( $_POST['service_type_id'] == $service_type_array[$i]->id ) {
                        $html .= " selected";
                    } 
                }
            }
            $html .= ">";
            $html .= $service_type_array[$i]->name;
            $html .= "</option>";
        }
        $html .= "</select>";
        if ( isset( $_POST['submit_add_service'] ) ) {
            if ( empty( $_POST['service_type_id'] ) ) {
              $html .= "<p class='description book-a-session-error'>Please select a service type.</p>";
            }
        } 
        $html .= "</td>";
    } else {
        $html .= "<option selected disabled>No service types found</option>";
        $html .= "</select><br><p class='description'>Add a Service Type in the Service Type settings page</p></td>";
    }
    // Service Name
    $html .=    "<tr><th scope='row'>";
	$html .=    "<label for='name'>Service Name</label></th>";
    $html .=    "<td><input type='text' name='name' id='name' placeholder='Name your service' style='min-width:300px;'";
    if ( isset( $_POST['name'] ) ) {
        $html .= " value='" . $_POST['name'] . "'>";
    } else {
        $html .= ">";
    }
    if ( isset( $_POST['submit_add_service'] ) ) {
        if ( empty( $_POST['name'] ) ) {
          $html .= "<p class='description book-a-session-error'>Please enter the service name.</p>";
        }
    } 
    $html .=    "</td></tr>";

    // Service Description
    $html .=    "<tr><th scope='row'>";
    $html .=    "<label for='description'>Description</label></th>";
    $html .=    "<td><textarea name='description' id='description' placeholder='Describe the service' style='min-width:300px;'>";
    if ( isset( $_POST['description'] ) ) {
        $html .= esc_textarea( stripslashes( $_POST['description'] ) );
    }
    $html .= "</textarea>";
    if ( isset( $_POST['submit_add_service'] ) ) {
        if ( empty( $_POST['description'] ) ) {
          $html .= "<p class='description book-a-session-error'>Please enter the service description.</p>";
        }
    }     
    $html .=    "</td></tr>";
    // Service Image URL Optional
    $html .=    "<tr><th scope='row'>";
	$html .=    "<label for='image_id'>Image (Optional)</label></th>";
    $html .=    "<td>";
    if ( isset( $_POST['image_id'] ) ) {
        $html.=  book_a_session_callback_image_select( array( 
            "option"        => false,
            "image_id"      => $_POST['image_id'],
            "name"          => "image_id"
            ) );
    } else {
        $html .= book_a_session_callback_image_select( array( 
            "option"        => false,
            "name"          => "image_id"
            ) );    
    }
    $html .=    "</td></tr>";
    // Locations checkbox
    $html .=    "<tr><th scope='row' colspan='2'><h3>Locations</h3><p class='description'>Check the Locations that this service can be provided in.</p>";
    $html .=    "<a href='javascript:void(0)' class='select-all-location'>Select all</a>&nbsp;&nbsp;<a href='javascript:void(0)' class='unselect-all-location'>Unselect all</a>";
    $html .=    "</th></tr>";
    $location_array = book_a_session_get_table_array( "location" );
    if ( ! empty( $location_array ) ) {
        for ($i = 0; $i < count( $location_array ); $i++) {
            $html .=    "<tr id='location_add_row_location_" . $location_array[$i]->id . "' class='location'><th scope='row'>";
            $html .=    "<label for='location_" . $location_array[$i]->id . "'>" . $location_array[$i]->name . ": " . book_a_session_get_region_by_id( array( "id" => $location_array[$i]->region_id ) )["name"] . "</label>";
            $html .=    "</th><td>";
                $html .=    "<label for='location_" . $location_array[$i]->id . "'><input type='checkbox' id='location_" . $location_array[$i]->id . "' name='location_" . $location_array[$i]->id . "' value='1'"; 
            if ( ! empty( $_POST['location_' . $location_array[$i]->id ] ) ) {
                $html .= " checked>";
            } else {
                $html .= ">";
            }
            $html .=    "Check if this service can be provided here.</label>";  
            $html .=    "</td></tr>"; 
        }
    } else {
        $html .=    "<tr><th scope='row' colspan='2'><p class='description'>No locations found. To add possible Locations for this service, first add your locations in the Locations settings page.</p></th></tr>";
    }
    // Service Price optional
    $html .=    "<tr><th scope='row' colspan='2'><h3>Session price</h3><p class='description'>Session prices are usually determined by the user's selected Region by default. You can override this amount by manually setting the price per session for this service here. Enter your desired price for any applicable currencies. Keep in mind, you don't need to add every currency your clients might be using along with their prices, as prices will be converted automatically on checkout when paying via online payment systems such as Paypal and Stripe.</p></th></tr>";
    $currency_array = book_a_session_get_table_array( "currency", "id", "ASC" );
    if ( ! empty( $currency_array ) ) {
        for ( $i = 0; $i < count( $currency_array ); $i++ ){
            // Session price input[type="number"] step="any" min="0" optional
            $html .=    "<tr id='service_price_add_row_currency_" . $currency_array[$i]->id . "'><th scope='row'>";
            $html .=    "<label for='currency_" . $currency_array[$i]->id . "'> Session Price " . $currency_array[$i]->code . " (Optional)</label></th>";
            $html .=    "<td><input type='number' step='any' min='0' name='currency_" . $currency_array[$i]->id . "' id='currency_" . $currency_array[$i]->id . "' placeholder='0.00' style='min-width:300px;'";
            if ( isset( $_POST['currency_' . $currency_array[$i]->id ] ) ) {
                $html .= " value='" . $_POST['currency_' . $currency_array[$i]->id ] . "'>";
            } else {
                $html .= ">";
            }        
            $html .=    "</td></tr>";   
        }       
    } else {
        $html .= "<tr><th colspan='2'><p class='description'>No currencies found. To add service pricing, first add a currency in the Currencies settings page.</p></th></tr>";
    }

	$html .=    "</tbody></table>";
    $html .=    "<input type='submit' class='button button-primary' value='Add service' name='submit_add_service'>";
    $html .=    "</form>";
	
	
	echo $html;



} elseif ( $tab == 'edit' ) {

    if ( isset( $_GET["edit"] ) && ! empty( $_GET['service_id'] )  ) {

        echo '<h3>Edit a service</h3>';
        global $wpdb;
        $table_name = $wpdb->prefix . "book_a_session_service";
        $service_price_table_name = $wpdb->prefix . "book_a_session_service_price";
        $service_location_table_name = $wpdb->prefix . "book_a_session_service_location";
        
        $id = intval( $_GET['service_id'] );

        $row_to_edit = $wpdb->get_row("
        SELECT  *
        FROM    $table_name
        WHERE   id = $id", OBJECT );

        $service_price = $wpdb->get_results("
        SELECT  *
        FROM    $service_price_table_name
        WHERE   service_id = $id", OBJECT );

        $service_location = $wpdb->get_results("
        SELECT  *
        FROM    $service_location_table_name
        WHERE   service_id = $id", OBJECT );
        
        $html =     "<table class='form-table'><form method='POST'>";
	
        // Service Type
        $html .=    "<tbody><tr><th scope='row'>";
        $html .=    "<label for='service_type_id'>Service Type</label></th>";
        $html .=    "<td><select name='service_type_id' id='service_add_service_type_id'>";
        $service_type_array = book_a_session_get_table_array( "service_type" );
        if ( ! empty( $service_type_array ) ) {
                for ($i = 0; $i < count($service_type_array); $i++) {
                $html .= "<option value='" . $service_type_array[$i]->id . "'";
                if ( isset( $row_to_edit->service_type_id ) ) {
                    if ( $row_to_edit->service_type_id == $service_type_array[$i]->id ) {
                        $html .= " selected";
                    }
                } 
                    $html .= ">";
                $html .= $service_type_array[$i]->name;
                $html .= "</option>";
            }
            $html .= "</select>";
            if ( isset( $_POST['submit_edit_service'] ) && empty( $_POST['service_type_id'] ) ) {
                $html .= "<p class='description book-a-session-error'>Please select a valid service type.</p>";
            }
            $html .= "</td>";
        } else {
            $html .= "<option selected disabled>No service types found</option>";
            $html .= "</select><br><p class='description'>Add a Service Type in the Service Type settings page</p></td>";
        }
        
        // Service Name
        $html .=    "<tr><th scope='row'>";
        $html .=    "<label for='name'>Service Name</label></th>";
        $html .=    "<td><input type='text' name='name' id='service_add_name' placeholder='Name your service' style='min-width:300px;'";
        if ( isset( $row_to_edit->name ) ) {
            $html .= " value='" . $row_to_edit->name . "'";
        } 
        $html .=    "></td></tr>";
    
        // Service Description
        $html .=    "<tr><th scope='row'>";
        $html .=    "<label for='description'>Description</label></th>";
        $html .=    "<td><textarea name='description' id='service_add_description' placeholder='Describe the service' style='min-width:300px;'>";
        if ( isset( $row_to_edit->description ) ) {
            $html .= esc_textarea( stripslashes( $row_to_edit->description ) );
        }
        $html .=    "</textarea></td></tr>";
        
        // Service Image Optional
        $html .=    "<tr><th scope='row'>";
        $html .=    "<label for='name'>Image (Optional)</label></th>";
        $html .=    "<td>";

        if ( isset( $row_to_edit->image_id ) ) {
            $html.=  book_a_session_callback_image_select( array( 
                "option"        => false,
                "image_id"      => $row_to_edit->image_id,
                "name"          => "image_id"
                ) );
        } else {
            $html .= book_a_session_callback_image_select( array( 
                "option"        => false,
                "name"          => "image_id"
                ) );    
        }
        $html .=    "</td></tr>";
        // Locations checkbox
        $html .=    "<tr><th scope='row' colspan='2'><h3>Locations</h3><p class='description'>Check the Locations that this service can be provided in.</p>";
        $html .=    "<a href='javascript:void(0)' class='select-all-location'>Select all</a>&nbsp;&nbsp;<a href='javascript:void(0)' class='unselect-all-location'>Unselect all</a>";
        $html .=    "</th></tr>";
        $location_array = book_a_session_get_table_array( "location" );
        if ( ! empty( $location_array ) ) {
            for ($i = 0; $i < count( $location_array ); $i++) {
                $match = false;
                $html .=    "<tr id='location_add_row_location_" . $location_array[$i]->id . "' class='location'><th scope='row'>";
                $html .=    "<label for='location_" . $location_array[$i]->id . "'>" . $location_array[$i]->name . ": " . book_a_session_get_region_by_id( array( "id" => $location_array[$i]->region_id ) )["name"] . "</label>";
                $html .=    "</th><td>";
                    $html .=    "<label for='location_" . $location_array[$i]->id . "'><input type='checkbox' id='location_" . $location_array[$i]->id . "' name='location_" . $location_array[$i]->id . "' value='1'"; 
                    if ( ! empty ( $service_location ) ) {
                        for ( $j = 0; $j < count( $service_location ); $j++ ){
                            if ( isset( $service_location[$j]->location_id ) ) {
                                if( $service_location[$j]->location_id == $location_array[$i]->id ) {
                                    $html .= " checked>";
                                    $match = true;
                                    break;
                                }       
                            }
                        }
                    }
                    if ( ! $match ) {
                        $html .= " >";
                    }
                    $html .=    "Check if " . $row_to_edit->name . " can be carried out here.</label>";  
                $html .=    "</td></tr>"; 
            }    
        } else {
            $html .=    "<tr><th scope='row' colspan='2'><p class='description'>No locations found. To add possible Locations for this service, first add your locations in the Locations settings page.</p></th></tr>";
        }
        // Service Price optional
        $html .=    "<tr><th scope='row' colspan='2'><h3>Session price</h3><p class='description'>Session prices are usually determined by the user's selected Region by default. You can override this amount by manually setting the price per session for this service here. Enter your desired price for any applicable currencies.</p></th></tr>";
        $currency_array = book_a_session_get_table_array( "currency", "id", "ASC" );
        if ( ! empty( $currency_array ) ) {
            $match = false;
            for ( $i = 0; $i < count( $currency_array ); $i++ ){
                // Session price input[type="number"] step="any" min="0" optional
                $html .=    "<tr id='service_price_edit_row_currency_" . $currency_array[$i]->id . "'><th scope='row'>";
                $html .=    "<label for='currency_" . $currency_array[$i]->id . "'> Session Price " . $currency_array[$i]->code . " (Optional)</label></th>";
                $html .=    "<td><input type='number' step='any' min='0' name='currency_" . $currency_array[$i]->id . "' id='currency_" . $currency_array[$i]->id . "' placeholder='0.00' style='min-width:300px;'";
                for ( $j = 0; $j < count( $service_price ); $j++ ){
                    if ( isset( $service_price[$j]->currency_id ) ) {
                        if( $service_price[$j]->currency_id == $currency_array[$i]->id ) {
                            $html .= " value='" . $service_price[$j]->session_price . "'>";
                            $match = true;
                            break;
                        }       
                    }
                }
                if ( ! $match ) {
                    $html .= ">";
                }
                $html .=    "</td></tr>";   
            }       
        } else {
            $html .= "<tr><th colspan='2'><p class='description'>No currencies found. Add a currency in the Currencies settings page.</p></th></tr>";
        }
        
        $html .=    "</tbody></table>";
        $html .=    "<input type='submit' class='button button-primary' value='Save changes' name='submit_edit_service'>";
        $html .=    "</form>";

        
        
        
        echo $html;

    } else {

        echo '<h3>Edit a service</h3>';
        echo '<form>';
        $services_list = new Services_List_Table;
        echo '</form>';
    
    }

} else {

	echo '<h3>View services</h3>';
	echo '<form>';
	$services_list = new Services_List_Table;
	echo '</form>';

}
// Code after the tabs (outside)
		?>


	</div>

	<?php

}



