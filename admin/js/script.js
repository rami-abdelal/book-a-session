jQuery(document).ready(function( $ ) {
    
    var apiUrl = '../wp-json/book-a-session/v1/';

    function displayContent( data ){

        $.each( $(data), function(i){

            $('#wpbody-content div.wrap h1').after(
                "<ul><li>" + $(data)[i].id + "</li><li>"
                + $(data)[i].name + "</li><li>"
                + $(data)[i].description + "</li></ul>");

        });

    }

    $.ajax({
        url: apiUrl + 'services',
        success: function( data ) {
            //displayContent( data );
        },
        dataType: 'json'
      });
      

    // Select all and unselect all buttons

    $('th .select-all').each(function(index){
        $(this).click(function() {
            $(this).parent().next("td").find("input[type='checkbox']").prop('checked', true).change();
        });
    });
    $('th .unselect-all').each(function(index){
        $(this).click(function() {
            $(this).parent().next("td").find("input[type='checkbox']").prop('checked', false).change();
       });
    });
    $('th .select-all-payment-method').each(function(index){
        $(this).click(function() {
            $("tr.payment-method input[type='checkbox']").prop('checked', true).change();
        });
    });
    $('th .unselect-all-payment-method').each(function(index){
        $(this).click(function() {
            $("tr.payment-method input[type='checkbox']").prop('checked', false).change();
       });
    });
    $('th .select-all-location').each(function(index){
        $(this).click(function() {
            $("tr.location input[type='checkbox']").prop('checked', true).change();
        });
    });
    $('th .unselect-all-location').each(function(index){
        $(this).click(function() {
            $("tr.location input[type='checkbox']").prop('checked', false).change();
       });
    });
    $('.select-all-country').each(function(index){
        $(this).click(function() {
            $(".country").prop('checked', true).change();
        });
    });
    $('.unselect-all-country').each(function(index){
        $(this).click(function() {
            $(".country").prop('checked', false).change();
       });
    });

    // Adding and Editing Regions - Hide Country checkboxes if International is checked

    $("#international").change(function() {

        if ( ! $("#international").prop("checked") ) {
            $("tr.region_add_countries").show();
        } else {
            $("tr.region_add_countries").hide();
        }

    });

    if ( ! $("#international").prop("checked") ) {
        $("tr.region_add_countries").show();
    } else {
        $("tr.region_add_countries").hide();
    }

    $("#international").change(function() {

        if ( ! $("#international").prop("checked") ) {
            $("tr.region_edit_countries").show();
        } else {
            $("tr.region_edit_countries").hide();
        }

    });

    if ( ! $("#international").prop("checked") ) {
        $("tr.region_edit_countries").show();
    } else {
        $("tr.region_edit_countries").hide();
    }


    // Search Country

    $('.book-a-session-country-search').on("keyup", function() {
        var value = $(this).val().toLowerCase(); 
        $('.country-label').filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
        });
    });
    $('.book-a-session-country-search').on("click", function() {
        var value = $(this).val().toLowerCase(); 
        $('.country-label').filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
        });
    });
    $('.book-a-session-country-search').on("search", function() {
        var value = $(this).val().toLowerCase(); 
        $('.country-label').filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
        });
    });

    // Add Order - Add more session time, session date, and session location rows

    var maximumQuantity = $('.order_add_row_session_date').length;
    var dateRow = $( 'tr#order_add_row_session_date_0' );
    var timeRow = $( 'tr#order_add_row_session_time_0' );

            // Hide all but the first date and time row
            $('.order_add_row_session_date').each(function(index) {
                if ( index > 0 ) {
                    $('.order_add_row_session_date').eq(index).hide();
                }
             });
             $('.order_add_row_session_time').each(function(index) {
                if ( index > 0 ) {
                    $('.order_add_row_session_time').eq(index).hide();
                }
             });


    $( '#order_add_quantity' ).change( function() {

        var orderQuantity = $( '#order_add_quantity' ).val();

        if ( orderQuantity > 1 ) {

            dateRow.find( 'th label' ).html( "Date for session 1" );
            timeRow.find( 'th label' ).html( "Time for session 1" );  

                    // Show the correct date and time rows
            $('.order_add_row_session_date').each(function(index) {
                if ( index < orderQuantity ) {
                    $('.order_add_row_session_date').eq(index).show();
                    $(this).find( 'th label' ).html( "Date for session " + parseInt( index + 1 ) );
                } else {
                    $('.order_add_row_session_date').eq(index).hide();
                }
             });
             $('.order_add_row_session_time').each(function(index) {
                if ( index < orderQuantity ) {
                    $('.order_add_row_session_time').eq(index).show();
                    $(this).find( 'th label' ).html( "Time for session " + parseInt( index + 1 ) );
                } else {
                    $('.order_add_row_session_time').eq(index).hide();
                }
             });

        } else {
             // Remove numbering from the remaining date and time row
             dateRow.find( 'th label' ).html( "Date of session" );
             timeRow.find( 'th label' ).html( "Time of session" );   
            // Hide all but the first date and time row
            $('.order_add_row_session_date').each(function(index) {
                if ( index > 0 ) {
                    $('.order_add_row_session_date').eq(index).hide();
                }
             });
             $('.order_add_row_session_time').each(function(index) {
                if ( index > 0 ) {
                    $('.order_add_row_session_time').eq(index).hide();
                }
             });
        }


    });

    // Delete confirmation

    $('.book-a-session-row-delete').click(function(event) {

        var result = confirm("Are you sure you want to delete this? Deletion is permanent.");
        if ( ! result ) {
            event.preventDefault();
        }

    });

    // Waypoints Sticky - Admin sidebar

    if ( $('.book-a-session-admin-settings-sidebar').length ) {

        var sticky = new Waypoint.Sticky({
            element: $('.book-a-session-admin-settings-sidebar')[0]
        })
    
    } else {

        if ( sticky ) {

            sticky.destroy();

        }

    }

    // WP Image selection

    $('.book_a_session_media_manager').click(function( e ) {

        $field = $(this).parent().find( 'input.book-a-session-image-select' );
        $image = $(this).parent().find( 'img.book-a-session-preview-image' );

 

        e.preventDefault();
        var image_frame;
        if ( image_frame ) {
            image_frame.open();
        }

        // Define image_frame as wp.media object

        image_frame = wp.media({
                      title: 'Select Media',
                      multiple : false,
                      library : {
                           type : 'image',
                       }
                  });

        image_frame.on('close',function() {

            // On close, get selections and save to the hidden input
            // plus other AJAX stuff to refresh the image preview
            var selection =  image_frame.state().get('selection');
            var gallery_ids = new Array();
            var my_index = 0;
            selection.each(function(attachment) {
            gallery_ids[my_index] = attachment['id'];
            my_index++;
            });
            var ids = gallery_ids.join(",");
            $field.val(ids);
            Refresh_Image(ids, $image);

        });

        image_frame.on('open',function() {
        // On open, get the id from the hidden input
        // and select the appropiate images in the media manager
        var selection =  image_frame.state().get('selection');
        ids = $field.val().split(',');
        ids.forEach(function(id) {
            attachment = wp.media.attachment(id);
            attachment.fetch();
            selection.add( attachment ? [ attachment ] : [] );
        });

        });

    image_frame.open();
    });

    // Ajax request to refresh the image preview
    function Refresh_Image(the_id, $image){
        var data = {
            action: 'book_a_session_get_image',
            id: the_id
        };
    
        $.get(ajaxurl, data, function(response) {
    
            if ( response.success && response.data.image ) {
                
                $image.replaceWith( response.data.image );

            }


        });
    }
    

});