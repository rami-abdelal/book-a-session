<?php

if (!class_exists('WP_List_Table')) {
    require_once (ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
 
}

class Services_List_Table extends WP_List_Table  {


    public function __construct()
    {
        parent::__construct(array(
            'singular' => 'service',
            'plural' => 'services',
            'ajax' => true
        ));

        $this->prepare_items();
        $this->display();
        
    }

    public function prepare_items()
    {
        $this->_column_headers = $this->get_column_info();
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array(
            $columns,
            $hidden,
            $sortable
        );
        
        $this->process_bulk_action();
        $per_page = $this->get_items_per_page('records_per_page', 10);
        $current_page = $this->get_pagenum();
        $total_items = self::record_count();
        $data = self::get_records($per_page, $current_page);
        $this->set_pagination_args(
                          ['total_items' => $total_items, 
                       'per_page' => $per_page 
                      ]);
        $this->items = $data;
        }

    /** * 
    *Retrieve records data from the database
    * * @param int $per_page
    * @param int $page_number
    * * @return mixed
    */
    public static function get_records($per_page = 10, $page_number = 1)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'book_a_session_service';

        $sql = "SELECT
                *
                FROM " .  
                $table_name;
/*
        if (isset($_REQUEST['s'])) {
        $sql.= ' where column1 LIKE "%' . $_REQUEST['s'] . '%" or column2 LIKE "%' . $_REQUEST['s'] . '%"';
        }
  */        
        if (!empty($_REQUEST['orderby'])) {
                $sql.= ' ORDER BY ' . esc_sql($_REQUEST['orderby']);
            $sql.= !empty($_REQUEST['order']) ? ' ' . esc_sql($_REQUEST['order']) : ' ASC';
        }
        $sql.= " LIMIT $per_page";
        $sql.= ' OFFSET ' . ($page_number - 1) * $per_page;
        $result = $wpdb->get_results($sql, 'ARRAY_A');
        return $result;
    }

    function get_columns()
        {
            $columns = [
                'service_name'=>__('Name'),
                'service_type'=>__('Type'),
                'service_locations'=>__('Locations'),
                'service_session_price'=>__('Session Price'),
                'service_description'=>__('Description'),
                'actions'=>__('Actions')
                  ];
            return $columns;
        }       

    public function get_hidden_columns()
    {
        // Setup Hidden columns and return them
        return array(
        );
    }

    /** 
    * Columns to make sortable. 
    * * @return array 
    */
    public function get_sortable_columns()
    {
        $sortable_columns = array(
            'service_name'=>array('name', true),
            'service_type'=>array('service_type_id', true)
          );
        return $sortable_columns;
    }

    /** 
    *Text displayed when no record data is available 
    */
    public function no_items()
    {
        _e('No services found.', 'bx');
    }

    /** 
    * Returns the count of records in the database. 
    * * @return null|string 
    */
    public static function record_count()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'book_a_session_service';

        $sql = "SELECT
                COUNT(*) 
                FROM " . 
                $table_name;
        return $wpdb->get_var($sql);
    }

    public function column_default($item, $column_name) {
        $currency_array = book_a_session_get_table_array( "currency" );
        $service_price_array = book_a_session_get_table_array( "service_price" );
        $location_array = book_a_session_get_table_array( "location" );
        $service_location_array = book_a_session_get_table_array( "service_location", false, false, "*", array( "service_id", "=", $item["id"] ) );
        $nonce_delete = "&_wpnonce=" . wp_create_nonce( 'book_a_session_delete_service_' . $item["id"] );
        $nonce_edit = "&_wpnonce=" . wp_create_nonce( 'book_a_session_edit_service_' . $item["id"] );
        $admin_edit_page_url = 'admin.php?page=book_a_session_services&tab=edit'; 
        switch ($column_name) {

            case "service_name"        : echo "<span>" . stripslashes( $item["name"] ) . "</span>";
                                         if ( ! empty( $item["image_id"] ) ) { echo "<br><br>" . wp_get_attachment_image( $item["image_id"], "thumbnail" ); } break;

            case "service_description" : echo stripslashes( $item["description"] ); break;

            case "service_locations"                    : echo "<table><tbody>";

            if ( (int)count( $location_array ) == (int)count( $service_location_array ) ){ 
                echo "<tr class='book-a-session-admin-accepted'><th scope='col'>All</th><td>&#10003;</td></tr>";
            } elseif ( empty( $service_location_array ) ) {
                echo "&mdash;";
            } else {   
                for ( $i = 0; $i < count( $location_array ); $i++ ) {

                    for ( $j = 0; $j < count( $service_location_array ); $j++ ) {
                        
                        if ( ! empty( $service_location_array[$j]->service_id ) && ! empty( $service_location_array[$j]->location_id ) ) {

                            if ( $service_location_array[$j]->service_id == $item["id"] && $service_location_array[$j]->location_id == $location_array[$i]->id ){
                                echo "<tr class='book-a-session-admin-accepted'><th scope='col'>" . $location_array[$i]->name . "</th><td>&#10003;</td></tr>";
                            } 

                        }                        

                    }

                }

            }
                                                          echo "</tbody></table></div>";  break;


            case "service_session_price": 
            
            echo "<table><tbody>";

            if ( ! empty( $currency_array ) ) {

                $match = false;

                for ( $i = 0; $i < count( $currency_array ); $i++ ) {

                    if ( ! empty( $service_price_array ) ) { 
                        
                        for ( $j = 0; $j < count( $service_price_array ); $j++ ) {
                        
                            if ( ! empty( $service_price_array[$j]->service_id ) && ! empty( $service_price_array[$j]->currency_id ) ) {
        
                                if ( $service_price_array[$j]->service_id == $item["id"] && $service_price_array[$j]->currency_id == $currency_array[$i]->id ){
        
                                    echo "<tr><th scope='col'>" . $currency_array[$i]->code .  "</th><td>" . 
                                    book_a_session_get_price_tag( $service_price_array[$j]->session_price, $service_price_array[$j]->currency_id ) . 
                                    "</td></tr>";
                                    $match = true;
                                    break;
                                    
                                } 
        
                            }                        
        
                        }

                    }
    
                }

                if ( ! $match ) {
                    echo "Defaults to Region's session price.";
                }


            } else {
                echo "No currencies found.";
            }

            echo "</tbody></table>"; break;

            case "service_type"        : echo  book_a_session_get_service_type_by_id( array( 'id' => $item["service_type_id"] ) )["name"]; break;
            case "actions" : echo   "<a href='" . $admin_edit_page_url . $nonce_edit . "&edit=1&service_id=" . $item["id"] . "'>Edit</a>" . 
                                    "&nbsp;&nbsp;&nbsp;" . 
                                    "<a class='book-a-session-row-delete' href='" . $admin_edit_page_url . $nonce_delete . "&delete=1&service_id=" . $item["id"] . "'>Delete</a>"; 
            break;
            return $item[ $column_name ];
                default:
                    return $item[ $column_name ] ;
        
        }

    }

}