<?php // Book A Session - Admin Notices

if ( ! defined( 'ABSPATH' ) ) exit;

// Universal Validation Error Notice

function book_a_session_admin_notice_validation_error() {
    $class = 'notice notice-error';
    $message = __( 'Validation error. All fields are required unless stated otherwise. Check your submission values and try again.', 'book_a_session' );

    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
}

// Add Row Universal Admin Notice

function book_a_session_admin_notice_add_error() {
    $class = 'notice notice-error';
    $message = __( 'Error. Your item couldn\'t be added. Please try again.', 'book_a_session' );
    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
}

function book_a_session_admin_notice_add_success() {
    $class = 'notice notice-success is-dismissible';
    $message = __( 'Success! Your item has been added.', 'book_a_session' );
    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
}

function book_a_session_admin_notice_add_security_error() {
    $class = 'notice notice-error';
    $message = __( 'Security check failed. Your item couldn\'t be added. Please try again.', 'book_a_session' );
    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
}

function book_a_session_admin_notice_add_multiple_warning() {
    $class = 'notice notice-warning is-dismissible';
    $message = __( 'One or more items couldn\'t be added.', 'book_a_session' );
    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
}

function book_a_session_admin_notice_add_multiple_success() {
    $class = 'notice notice-success is-dismissible';
    $message = __( 'Success! All items were added.', 'book_a_session' );
    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
}

// Edit Row Universal Admin Notice

function book_a_session_admin_notice_edit_error() {
    $class = 'notice notice-error';
    $message = __( 'Error. Your item couldn\'t be edited. Please try again.', 'book_a_session' );
    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
}

function book_a_session_admin_notice_edit_success() {
    $class = 'notice notice-success is-dismissible';
    $message = __( 'Success! Your item has been edited.', 'book_a_session' );
    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
}

function book_a_session_admin_notice_edit_multiple_warning() {
    $class = 'notice notice-warning is-dismissible';
    $message = __( 'One or more items couldn\'t be edited.', 'book_a_session' );
    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
}

function book_a_session_admin_notice_edit_multiple_success() {
    $class = 'notice notice-success is-dismissible';
    $message = __( 'Success! All items changes were saved.', 'book_a_session' );
    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
}

function book_a_session_admin_notice_edit_security_error() {
    $class = 'notice notice-error';
    $message = __( 'Security check failed. Your item couldn\'t be edited. Please try again.', 'book_a_session' );
    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
}


// Delete Row Universal Admin Notice

function book_a_session_admin_notice_delete_error() {
    $class = 'notice notice-error';
    $message = __( 'Error. Your item couldn\'t be deleted. Please try again.', 'book_a_session' );
    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
}

function book_a_session_admin_notice_delete_success() {
    $class = 'notice notice-success is-dismissible';
    $message = __( 'Success! Your item has been deleted.', 'book_a_session' );
    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
}

function book_a_session_admin_notice_delete_security_error() {
    $class = 'notice notice-error';
    $message = __( 'Security check failed. Your item couldn\'t be deleted. Please try again.', 'book_a_session' );
    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
}

// View Content Universal Admin Notice

function book_a_session_admin_notice_view_security_error() {
    $class = 'notice notice-error';
    $message = __( 'Security check failed. You can\'t view this content. Please try again.', 'book_a_session' );
    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
}

// Options warning

function book_a_session_admin_notice_option_info() {
    $class = 'notice notice-info is-dismissible';
    $message = __( 'Book A Session: Get started by configuring your options in the Book A Session menu page', 'book_a_session' );
    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
}

function book_a_session_admin_notice_timezone_option_info() {
    $class = 'notice notice-info is-dismissible';
    $message = __( 'Book A Session: Your default timezone isn\'t set properly. To fix this, go to the Book A Session Settings page, scroll to the Schedule section, enter your timezone according to the provided link and hit Save Changes at the bottom of the page. This will ensure all dates and times are accurate for all of your users.', 'book_a_session' );
    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
}

