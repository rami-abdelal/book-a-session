jQuery(document).ready(function( $ ) {

    // Add Book A Session body class and insert loader and modal elements into DOM

    $('body').addClass('book-a-session');
    $('body').append("<div class='book-a-session-modal book-a-session-animated book-a-session-hidden'><div class='book-a-session-modal-inner book-a-session-card book-a-session-animated book-a-session-hidden'><div class='book-a-session-icon-close book-a-session-modal-ui book-a-session-icon book-a-session-animated'></div></div></div>");
    $('body').append("<div class='book-a-session-loader-background book-a-session-animated book-a-session-hidden'><div class='book-a-session-loader book-a-session-animated'>Loading...</div></div>");
    $('body').append("<div class='book-a-session-fixed-notice book-a-session-animated book-a-session-hidden'><div class='book-a-session-fixed-notice-content'><div class='book-a-session-icon-close book-a-session-icon book-a-session-animated'></div></div></div>");

    // Declare interval var so we can access it here when closing a modal

    var checkForMessages = null;

    function book_a_session_reveal_loader() {

        $('.book-a-session-loader').parent().removeClass('book-a-session-hidden');

    }

    function book_a_session_hide_loader() {

        $('.book-a-session-loader').parent().addClass('book-a-session-hidden');

    }

    function book_a_session_clear_modal() {

        $('.book-a-session-modal-inner:not(.book-a-session-dashboard-week-timetable-inner)').children().not('.book-a-session-modal-ui').remove();
        $('.book-a-session-modal-inner:not(.book-a-session-dashboard-week-timetable-inner)').removeClass( 'book-a-session-card-container' );
        if ( checkForMessages ) { clearInterval( checkForMessages ); }

    }

    function book_a_session_reveal_modal() {

        book_a_session_hide_loader();
        $('.book-a-session-modal:not(.book-a-session-modal-week-timetable)').removeClass('book-a-session-hidden');
        setTimeout(function(){$('.book-a-session-modal-inner:not(.book-a-session-dashboard-week-timetable-inner)').removeClass('book-a-session-hidden');},200);
        
    }

    function book_a_session_hide_modal() {

        book_a_session_hide_loader();
        $('.book-a-session-modal-inner:not(.book-a-session-dashboard-week-timetable-inner)').addClass('book-a-session-hidden');
        setTimeout(function(){$('.book-a-session-modal:not(.book-a-session-modal-week-timetable)').addClass('book-a-session-hidden');},100);

    }

    function book_a_session_hide_and_clear_modal() {

        book_a_session_hide_modal();
        setTimeout(function(){ book_a_session_clear_modal(); },200);

    }

    // TO DO: Have all of these elements prepared on load, instead of generating them as they're requested, which sometimes affects performance

    function book_a_session_reveal_fixed_notice( $content, $status, $type, $icon, $timeout ) {

        // First we define the function that does everything, then we use it under two different circumstances

        function book_a_session_process_fixed_notice() {

            // Start by removing the animated class so these changes can be done instantly

            $('.book-a-session-fixed-notice').removeClass('book-a-session-animated');

            // If the notice was hidden by the script or by the user, go straight to simply inserting the provided content and classes depending on the status string provided and reveal 

            $('.book-a-session-fixed-notice .book-a-session-fixed-notice-content').html( "<div class='book-a-session-icon-left'><span class='book-a-session-icon-envelope-open-o'></span></div><div class='book-a-session-fixed-notice-text'>" + $content + "</div><div class='book-a-session-icon-close book-a-session-icon book-a-session-animated'></div>" );

            // Assign event handler, as the icon close element is removed every time, and along with it, the bound event

            $('.book-a-session-fixed-notice-content .book-a-session-icon-close').on("click", function(){

                book_a_session_hide_fixed_notice();
        
            });

            // Apply classes and icons depending on supplied parameters

            if ( $status === "success" ) $('.book-a-session-fixed-notice').addClass('book-a-session-fixed-notice-success').find(".book-a-session-icon-left").empty().append("<span class='book-a-session-icon-check2'></span>");
            if ( $status === "warning" ) $('.book-a-session-fixed-notice').addClass('book-a-session-fixed-notice-warning').find(".book-a-session-icon-left").empty().append("<span class='book-a-session-icon-information-outline'></span>");
            if ( $status === "error" )   $('.book-a-session-fixed-notice').addClass('book-a-session-fixed-notice-error').find(".book-a-session-icon-left").empty().append("<span class='book-a-session-icon-cancel'></span>");
            if ( $status === "info" )    $('.book-a-session-fixed-notice').addClass('book-a-session-fixed-notice-info').find(".book-a-session-icon-left").empty().append("<span class='book-a-session-icon-information-outline'></span>");   

            // If a fourth string was supplied, any previous icons are removed and this string is applied as a class referring to an icon, i.e. book-a-session-icon-calendar. All available icon classes can be found in the public css: book-a-session/public/css/style.css 

            if ( $icon ) $('.book-a-session-fixed-notice .book-a-session-icon-left').empty().append("<span class='" + $icon + "'></span>");
            
            // If a third string was supplied, and it's "dismissable", we add a class to reveal the close icon. Otherwise, the icon is there, but has an opacity of 0 and its visibility is hidden, though that can be changed via CSS.

            if ( $type === "dismissable" ) $('.book-a-session-fixed-notice').addClass('book-a-session-fixed-notice-dismissable');

            // Finally reinstate the animated class and reveal the notice
    
            $('.book-a-session-fixed-notice').addClass('book-a-session-animated').removeClass('book-a-session-hidden');
            
            if ( $timeout ) {

                if ( $timeout > 0 ) setTimeout(function(){ book_a_session_hide_fixed_notice(); }, $timeout );

            }

        }

        // So if it's already hidden, just do the notice

        if ( $('.book-a-session-fixed-notice').hasClass('book-a-session-hidden') ) {

            book_a_session_process_fixed_notice();

        } else {

            // Otherwise, hide the notice first, waiting 260ms (which is the current transition time of book-a-session-animated plus an extra 50ms for padding processing time), then reveal it. This causes the notice to fade out the way it was, change while hidden, then appear again with the changed content

            book_a_session_hide_fixed_notice();

            setTimeout(function(){

                book_a_session_process_fixed_notice();

            },250);

        }

    }

    function book_a_session_hide_inline_loader( $this ) {

        if ( $this.hasClass('book-a-session-loading') ) {

            $this.find(".book-a-session-inline-loader").addClass('book-a-session-hidden')

            setTimeout(function(){

                $this.find(".book-a-session-inline-loader").remove();
                $this.removeClass('book-a-session-loading');

            }, 200 );

            book_a_session_hide_fixed_notice();
    
        }

    }

    function book_a_session_reveal_inline_loader( $this ) {

        if ( ! $this.hasClass('book-a-session-loading') ) {

            book_a_session_reveal_fixed_notice( "Loading...", "info", "", "book-a-session-icon-hourglass-end" );

            if ( $('book-a-session-loading').length > 0 ) {

                $('book-a-session-loading').each(function(){
                    book_a_session_hide_inline_loader( $(this) );
                });

            }

            $this.addClass('book-a-session-loading')
            .append("<div class='book-a-session-inline-loader book-a-session-loader book-a-session-animated book-a-session-hidden'></div>");

            $this.find(".book-a-session-inline-loader")
            .removeClass('book-a-session-hidden');
    
        }

    }

    // Close modal

    $('.book-a-session-modal:not(.book-a-session-modal-week-timetable) .book-a-session-icon-close, .book-a-session-modal:not(.book-a-session-modal-week-timetable)').click( function( e ) {

        // If the event's true target wasn't clicked ( the close button or the actual modal background ), don't do anything, that is, clicking the modal inner shouldn't work just because it's a modal child

        if ( e.target !== e.currentTarget ) return;

        // Otherwise hide modal
        book_a_session_hide_and_clear_modal();

    });

    function book_a_session_hide_fixed_notice() {

        $('.book-a-session-fixed-notice').addClass('book-a-session-hidden');

        setTimeout(function(){

            $('.book-a-session-fixed-notice').removeClass('book-a-session-animated');
            
            $('.book-a-session-fixed-notice').removeClass('book-a-session-fixed-notice-success book-a-session-fixed-notice-warning book-a-session-fixed-notice-error book-a-session-fixed-notice-info book-a-session-fixed-notice-dismissable');

            $('.book-a-session-fixed-notice').addClass('book-a-session-animated');   

        },200);

    }

    function book_a_session_reveal_login_registration_modal() {

        book_a_session_hide_loader();
        $('.book-a-session-modal-login-register').removeClass('book-a-session-hidden');
        setTimeout(function(){$('.book-a-session-modal-inner-login-register').removeClass('book-a-session-hidden');},200);

    }

    function book_a_session_hide_login_registration_modal() {

        book_a_session_hide_loader();
        $('.book-a-session-modal-inner-login-register').addClass('book-a-session-hidden');
        setTimeout(function(){$('.book-a-session-modal-login-register').addClass('book-a-session-hidden');},100);

    }

    // Close login / register modal event binding

    if ( $('.book-a-session-modal-login-register .book-a-session-icon-close').length !== 0 ) {

        $('.book-a-session-modal-login-register .book-a-session-icon-close, .book-a-session-modal-login-register').on("click", function(e){

            // This only happens when the close icon is clicked, or the modal background is clicked, not its descendents such as the modal inner

            if ( e.target !== e.currentTarget ) return;

            book_a_session_hide_login_registration_modal();

        });

    }

    if ( $('form#book-a-session-login').length !== 0 ) {

        var api_root = book_a_session_api.api_root;

        // Login
    
        $('form#book-a-session-login').on("submit", function(e){
    
            e.preventDefault();
    
            book_a_session_reveal_loader();
    
            var username = $('input[name="book_a_session_login_username"]').val().replace(/^\s+|\s+$/g, '').length != 0 ? $('input[name="book_a_session_login_username"]').val() : false;
            var password = $('input[name="book_a_session_login_password"]').val().replace(/^\s+|\s+$/g, '').length != 0 ? $('input[name="book_a_session_login_password"]').val() : false;
            var security = $('input[name="book_a_session_login_security"]').val().replace(/^\s+|\s+$/g, '').length != 0 ? $('input[name="book_a_session_login_security"]').val() : false;
            
            var remember = $('input[name="book_a_session_login_remember"]:checked').length != 0 ? true : false;
    
            if ( username && password && security ) {
    
                $.ajax({
    
                    url: api_root + "login",
                    method: "POST",
                    dataType: 'json',
                    data: {                           
    
                        "user_login": username,
                        "user_password": password,
                        "remember": remember,
                        "nonce": security,
    
                    },
    
                }).done(function(){
    
                    // Logged in
    
                    book_a_session_hide_login_registration_modal();
    
                    book_a_session_reveal_fixed_notice( "You're logged in! Reloading...", "success", "dismissable" );
    
                    location.reload();
    
                }).fail(function( login_fail ){
    
                    // Login failed
    
                    book_a_session_reveal_fixed_notice( login_fail.responseJSON.code, "error", "dismissable", "book-a-session-icon-vpn_key", 5000 );
    
                    book_a_session_hide_loader();
    
                });
    
            } else {
    
                book_a_session_hide_loader();
                book_a_session_reveal_fixed_notice( "We couldn't log you in with those details. Please try again.", "error", "dismissable", "book-a-session-icon-vpn_key", 5000 );
    
            }
    
        });    

    }


    if ( $('form#book-a-session-register').length !== 0 ) {

        var api_root = book_a_session_api.api_root;

        // Register

        $('form#book-a-session-register').on("submit", function(e){

            e.preventDefault();

            book_a_session_reveal_loader();

            var firstname = $('input[name="book_a_session_register_first_name"]').val().replace(/^\s+|\s+$/g, '').length != 0 ? $('input[name="book_a_session_register_first_name"]').val() : false;
            var lastname = $('input[name="book_a_session_register_last_name"]').val().replace(/^\s+|\s+$/g, '').length != 0 ? $('input[name="book_a_session_register_last_name"]').val() : false;
            var email = $('input[name="book_a_session_register_email"]').val().replace(/^\s+|\s+$/g, '').length != 0 ? $('input[name="book_a_session_register_email"]').val() : false;
            var region = $('select[name="book_a_session_register_region').find(":selected").val();

            var password = $('input[name="book_a_session_register_password"]').val().replace(/^\s+|\s+$/g, '').length != 0 ? $('input[name="book_a_session_register_password"]').val() : false;
            var confirm_password = $('input[name="book_a_session_register_confirm_password"]').val().replace(/^\s+|\s+$/g, '').length != 0 ? $('input[name="book_a_session_register_confirm_password"]').val() : false;

            var security = $('input[name="book_a_session_register_security"]').val().replace(/^\s+|\s+$/g, '').length != 0 ? $('input[name="book_a_session_register_security"]').val() : false;

            if ( firstname && lastname && email && region && password && confirm_password && password === confirm_password ) {

                $.ajax({

                    url: api_root + "register",
                    method: "POST",
                    dataType: 'json',
                    data: {                           

                        "firstname": firstname,
                        "lastname": lastname,
                        "email": email,
                        "region_id": region,
                        "password": password,
                        "nonce": security,

                    },

                }).done(function( register_success ){

                    // Logged in

                    book_a_session_hide_login_registration_modal();

                    book_a_session_reveal_fixed_notice( "You've been successfully registered and logged in! Reloading...", "success", "dismissable" );

                    location.reload();


                }).fail(function( register_fail ){

                    // Login failed

                    book_a_session_reveal_fixed_notice( register_fail.responseJSON.code, "error", "dismissable", "book-a-session-icon-vpn_key", 5000 );

                    book_a_session_hide_loader();

                });

            } else {

                book_a_session_hide_loader();

                if ( ! firstname ) $('input[name="book_a_session_register_first_name"]').addClass('book-a-session-error');
                if ( ! lastname ) $('input[name="book_a_session_register_last_name"]').addClass('book-a-session-error');
                if ( ! email ) $('input[name="book_a_session_register_email"]').addClass('book-a-session-error');
                if ( ! region ) $('input[name="book_a_session_register_region"]').addClass('book-a-session-error');
                if ( ! password ) $('input[name="book_a_session_register_password"]').addClass('book-a-session-error');
                if ( ! confirm_password ) $('input[name="book_a_session_register_confirm_password"]').addClass('book-a-session-error');

                if ( password !== confirm_password ) {

                    $('input[name="book_a_session_register_password"]').addClass('book-a-session-error');
                    $('input[name="book_a_session_register_confirm_password"]').addClass('book-a-session-error');

                }

                if ( ! security ) {

                    book_a_session_reveal_fixed_notice( "Security error. Please reload and try again.", "error", "dismissable", "book-a-session-icon-vpn_key", 5000 );

                } else {

                    book_a_session_reveal_fixed_notice( "You missed something. Please check what you've entered and try again, all fields are required.", "error", "dismissable", "book-a-session-icon-vpn_key", 5000 );

                }
            }

        });

    }

    if ( $('form#book-a-session-forgot-password').length !== 0 ) {

        // Forgot Password

        var api_root = book_a_session_api.api_root;

        $('form#book-a-session-forgot-password').on("submit", function(e){

            e.preventDefault();

            book_a_session_reveal_loader();

            book_a_session_reveal_fixed_notice( "Loading...", "info", "", "book-a-session-icon-hourglass-end" );

            var email = $('input[name="book_a_session_forgot_password_email"]').val().replace(/^\s+|\s+$/g, '').length != 0 ? $('input[name="book_a_session_forgot_password_email"]').val() : false;

            $.ajax({

                url: api_root + "password",
                method: "POST",
                dataType: 'json',
                data: {                           

                    "email": email,

                },

            }).done(function( recover_password_success ){

                // Verification link sent

                book_a_session_reveal_fixed_notice( "We've sent a verification link to " + email, "success", "dismissable" );

                book_a_session_hide_loader();

            }).fail(function( recover_password_fail ){

                // Verification link not sent

                book_a_session_reveal_fixed_notice( "Sorry, we couldn't process your password recovery request.", "error", "dismissable", "book-a-session-icon-vpn_key", 5000 );

                book_a_session_hide_loader();

            });

        });

    }

    function book_a_session_week_timetable() {

        if ( $('.book-a-session-week-timetable').length !== 0 ) {

            // Hide horizontal overflow as the week timetable creates a very wide element which prompts the browser to create a horizontal scrollbar, even though the content is hidden

            $('body').css({ "overflow-x": "hidden" });

            // Week timetable interaction

            var week_timetable_week_total_count = $( '.book-a-session-week-timetable-container-week' ).length;
            var week_timetable_width = $( '.book-a-session-week-timetable' ).outerWidth() / week_timetable_week_total_count;
            var week_timetable_week_counter = 1;
            var week_timetable_scroll_tracker = 0;

            // Reset position and button states in case interaction occurs before a window resize, which needs fresh calculations in order to function properly at a new size in case one of its containers changed size too.
            // TO DO: Calculate element size outside the function instead to test whether or not we should reset it, instead of firing every time the window resizes but the container size doesn't change.
            
            $( '.book-a-session-week-timetable' ).css( { "transform": "translateX(0px)", } );
            $( '.book-a-session-week-timetable-card .book-a-session-prev-button' ).addClass( 'book-a-session-hidden' );
            $( '.book-a-session-week-timetable-card .book-a-session-next-button' ).removeClass( 'book-a-session-hidden' );
            $( '.book-a-session-week-timetable-week-1' ).removeClass( 'book-a-session-hidden' );
            
            // Next button

            $( '.book-a-session-week-timetable-card .book-a-session-next-button' ).click( function() {

                // Only if it's revealed, never when hidden

                if ( ! $(this).hasClass('book-a-session-hidden') ) {

                    // Calculate translateX() value by subtracting the width of the timetable

                    week_timetable_scroll_tracker -= week_timetable_width;

                    week_timetable_week_counter++;

                    // Hide everything but container-week-(week_timetable_week_counter)
                    $('.book-a-session-week-timetable-container-week').addClass('book-a-session-hidden');
                    $('.book-a-session-week-timetable-week-' + week_timetable_week_counter ).removeClass('book-a-session-hidden');

                    // Reveal prev button when needed

                    if ( week_timetable_scroll_tracker <= 0 ) $( '.book-a-session-week-timetable-card .book-a-session-prev-button' ).removeClass( 'book-a-session-hidden' );

                    // Hide next button ( below was previously week_timetable_scroll_tracker <= -week_timetable_width * ( week_timetable_week_total_count - 1 ), instead of - 2, but for some reason the timetable allowed us to count to an extra week, so I added another to make up for that. Messy, but I'll look at it again later. Perhaps we'll judge with the week counter instead of the scroll tracker

                    if ( week_timetable_scroll_tracker <= -week_timetable_width * ( week_timetable_week_total_count - 2 ) ) $( '.book-a-session-week-timetable-card .book-a-session-next-button' ).addClass( 'book-a-session-hidden' );

                    // Apply the translateX value
                    
                    $( '.book-a-session-week-timetable' ).css( { "transform": "translateX(" + week_timetable_scroll_tracker + "px)", } );

                    $( '.book-a-session-week-timetable-card .book-a-session-scroll-tracker' ).css( { "width": ( week_timetable_week_counter ) * ( $('.book-a-session-week-timetable-card').outerWidth() / week_timetable_week_total_count) + "px",  } );

                }

            });

            // Previous button, hidden at first with class book-a-session-hidden

            $( '.book-a-session-week-timetable-card .book-a-session-prev-button' ).click( function() {

                // If it's clicked and its not hidden, preventing erroneous clicks

                if ( ! $(this).hasClass('book-a-session-hidden') ) {

                    // Calculate translateX() value by adding the width of the timetable

                    week_timetable_scroll_tracker += week_timetable_width;

                    week_timetable_week_counter--;

                    // Hide everything but container-week-(week_timetable_week_counter)
                    $('.book-a-session-week-timetable-container-week').addClass('book-a-session-hidden');
                    $('.book-a-session-week-timetable-week-' + week_timetable_week_counter ).removeClass('book-a-session-hidden');

                    // Hide prev button if at the end

                    if ( week_timetable_scroll_tracker >= 0 ) $( '.book-a-session-week-timetable-card .book-a-session-prev-button' ).addClass( 'book-a-session-hidden' );

                    // Reveal next button if not at the end

                    if ( week_timetable_scroll_tracker >= -week_timetable_width * week_timetable_week_total_count ) $( '.book-a-session-week-timetable-card .book-a-session-next-button' ).removeClass( 'book-a-session-hidden' );

                    // Apply the translateX value

                    $( '.book-a-session-week-timetable' ).css( { "transform": "translateX(" + week_timetable_scroll_tracker + "px)", } );    

                    $( '.book-a-session-week-timetable-card .book-a-session-scroll-tracker' ).css( { "width": ( week_timetable_week_counter ) * ( $('.book-a-session-week-timetable-card').outerWidth() / week_timetable_week_total_count) + "px",  } );

                }

            });

            // Set tooltip header height to be the same as the tooltip container height

            $('.book-a-session-week-timetable-session-time.book-a-session-container-tooltip').each( function() {

                var thisHeight = $(this).outerHeight();

                $(this).find('header').css({ "height": thisHeight + "px", });

            });

            // Set tooltip width to how many times the width of each column?

            var column_multiplier = 2;

            var column_width = $( '.book-a-session-week-timetable-container-date' ).width();

            $('.book-a-session-week-timetable .book-a-session-label-tooltip').css( { "width": ( column_width * column_multiplier ) + "px", } );

            // Size tooltips to be twice the width of its trigger

            $('.book-a-session-week-timetable-session-time.book-a-session-container-tooltip').each( function() {

                var $session = $(this);
                var $tooltip = $(this).find('.book-a-session-label-tooltip');

                // place the submenu in the correct position relevant to the menu item  "left": - $tooltip.outerWidth() + "px"

                $tooltip.css({
                    "width": $session.outerWidth() * 2,
                });

            });

            // Position tooltips based on their calculated width

            $('.book-a-session-week-timetable-session-time.book-a-session-container-tooltip').each( function() {

                var $session = $(this);
                var $tooltip = $(this).find('.book-a-session-label-tooltip');

                // place the submenu in the correct position relevant to the menu item  "left": - $tooltip.outerWidth() + "px"

                $tooltip.css({
                    "left": - $tooltip.outerWidth() + "px",
                });

            });

            // Change Monday, Tuesday and Wednesday tooltips to appear to the right instead of the left so as not to move out of bounds

            $('.book-a-session-week-timetable-day-mon .book-a-session-week-timetable-session-time.book-a-session-container-tooltip, .book-a-session-week-timetable-day-tue .book-a-session-week-timetable-session-time.book-a-session-container-tooltip, .book-a-session-week-timetable-day-wed .book-a-session-week-timetable-session-time.book-a-session-container-tooltip').each( function() {

                var $session = $(this);
                var $tooltip = $(this).find('.book-a-session-label-tooltip');

                // place the submenu in the correct position relevant to the menu item  "left": - $tooltip.outerWidth() + "px"

                $tooltip.css({
                    "right": - $tooltip.outerWidth() + "px",
                    "transform-origin": "center left",
                    "left": "auto",                
                });

            });

        }

    }

    function book_a_session_calendar_timetable() {

        if ( $('.book-a-session-calendar-timetable').length !== 0 ) {

            // Calendar timetable interaction

            var calendar_timetable_month_total_count = $( '.book-a-session-calendar-timetable-container-month' ).length;

            $( '.book-a-session-calendar-timetable' ).css( { "transform": "translateX(0px)", "width": "calc( 100% * " + calendar_timetable_month_total_count + " )" } );
            $( '.book-a-session-calendar-timetable-container-month' ).css( { "width": "calc( 100% / " + calendar_timetable_month_total_count + " )" } );

            var calendar_timetable_width = $( '.book-a-session-calendar-timetable-container-month' ).outerWidth();
            var calendar_timetable_month_counter = 1;
            var calendar_timetable_scroll_tracker = 0;

            // Reset position and button states in case interaction occurs before a window resize, which needs fresh calculations in order to function properly at a new size in case one of its containers changed size too.
            // TO DO: Calculate element size outside the function instead to test whether or not we should reset it, instead of firing every time the window resizes but the container size doesn't change.
            
            $( '.book-a-session-calendar-timetable-month-' + calendar_timetable_month_counter + ' .book-a-session-previous-month-button' ).addClass( 'book-a-session-hidden' );
            $( '.book-a-session-calendar-timetable-month-' + calendar_timetable_month_counter + ' .book-a-session-next-month-button' ).removeClass( 'book-a-session-hidden' );
            $( '.book-a-session-calendar-timetable-month-1' ).removeClass( 'book-a-session-hidden' );

            // Next button

            $( '.book-a-session-calendar-timetable-card .book-a-session-next-month-button' ).click( function() {

                // Only if it's revealed, never when hidden

                if ( ! $(this).hasClass('book-a-session-hidden') ) {

                    // Calculate translateX() value by subtracting the width of the timetable

                    calendar_timetable_scroll_tracker -= calendar_timetable_width;

                    calendar_timetable_month_counter++;

                    // Hide everything but container-month-(calendar_timetable_month_counter)
                    $('.book-a-session-calendar-timetable-container-month').addClass('book-a-session-hidden');
                    $('.book-a-session-calendar-timetable-month-'+ calendar_timetable_month_counter ).removeClass('book-a-session-hidden');

                    // Apply the translateX value
                    
                    $( '.book-a-session-calendar-timetable' ).css( { "transform": "translateX(" + calendar_timetable_scroll_tracker + "px)", } );

                    // Apply width for the scroll tracker

                    $( '.book-a-session-calendar-timetable-card .book-a-session-scroll-tracker' ).css( { "width": ( calendar_timetable_month_counter ) * ( $('.book-a-session-calendar-timetable-card').outerWidth() / calendar_timetable_month_total_count) + "px",  } );

                }

            });

            // Previous button, hidden at first with class book-a-session-hidden

            $( '.book-a-session-calendar-timetable-card .book-a-session-previous-month-button' ).click( function() {

                // If it's clicked and its not hidden, preventing erroneous clicks

                if ( ! $(this).hasClass('book-a-session-hidden') ) {

                    // Calculate translateX() value by adding the width of the timetable

                    calendar_timetable_scroll_tracker += calendar_timetable_width;

                    calendar_timetable_month_counter--;

                    // Hide everything but container-week-(calendar_timetable_month_counter)
                    $('.book-a-session-calendar-timetable-container-month').addClass('book-a-session-hidden');
                    $('.book-a-session-calendar-timetable-month-'+ calendar_timetable_month_counter ).removeClass('book-a-session-hidden');

                    // Apply the translateX value

                    $( '.book-a-session-calendar-timetable' ).css( { "transform": "translateX(" + calendar_timetable_scroll_tracker + "px)", } );   
                    
                    // Apply width for the scroll tracker

                    $( '.book-a-session-calendar-timetable-card .book-a-session-scroll-tracker' ).css( { "width": ( calendar_timetable_month_counter ) * ( $('.book-a-session-calendar-timetable-card').outerWidth() / calendar_timetable_month_total_count) + "px",  } );
                        
                }

            });

            // Set tooltip header height to be the same as the tooltip container height

            $('.book-a-session-calendar-timetable-card .book-a-session-container-tooltip').each( function() {

                var thisHeight = $(this).outerHeight();

                $(this).find('header').css({ "height": thisHeight + "px", });

            });

            // Set tooltip width to how many times the width of each column?

            var column_multiplier = 2;

            var column_width = $( '.book-a-session-calendar-timetable-container-date' ).width();

            $('.book-a-session-calendar-timetable .book-a-session-label-tooltip').css( { "width": ( column_width * column_multiplier ) + "px", } );

            // Position tooltips relative to the translating .book-a-session-calendar-timetable. Because it is translating, its position is technically set to relative, so absolutely positioned descendents must be positioned relative to it

            $('.book-a-session-calendar-timetable .book-a-session-container-tooltip').each( function() {

                var $session = $(this);
                var $tooltip = $(this).find('.book-a-session-label-tooltip');

                // place the submenu in the correct position relevant to the menu item  "left": - $tooltip.outerWidth() + "px"

                $tooltip.css({
                    "width": $session.outerWidth() * ( column_multiplier + 1 ),
                });

            });

            // Position tooltips based on their calculated width

            $('.book-a-session-calendar-timetable .book-a-session-container-tooltip').each( function() {

                var $session = $(this);
                var $tooltip = $(this).find('.book-a-session-label-tooltip');

                // place the submenu in the correct position relevant to the menu item  "left": - $tooltip.outerWidth() + "px"

                $tooltip.css({
                    "left": - $session.outerWidth() * ( column_multiplier + 1 ) + "px",
                });

            });

            // Change Monday, Tuesday and Wednesday tooltips to appear to the right instead of the left so as not to move out of bounds

            $('.book-a-session-calendar-timetable-day-mon.book-a-session-container-tooltip, .book-a-session-calendar-timetable-day-tue.book-a-session-container-tooltip, .book-a-session-calendar-timetable-day-wed.book-a-session-container-tooltip').each( function() {

                var $session = $(this);
                var $tooltip = $(this).find('.book-a-session-label-tooltip');

                // place the submenu in the correct position relevant to the menu item  "left": - $tooltip.outerWidth() + "px"

                $tooltip.css({
                    "right": - $session.outerWidth() * ( column_multiplier + 1 ) + "px",
                    "transform-origin": "center left",
                    "left": "auto",                
                });

            });

        }

    }

    /**
     * Book A Session Dashboards are HTML called by a PHP function book_a_session_dashboard() in /includes/dashboard.php which is also used
     * by the shortcode [book-a-session dashboard].
     * 
     * The dashboard contains a sidebar with a percentage based width, and a content area spanning the rest of the dashboard's total width.
     * The sidebar contains links, that, when pressed, reveal their corresponding content. This function handles the interactivity that
     * ensures links reveal the correct content, and the dashboard and content elements change to the correct height to reveal all the
     * content available and also so that it's never larger than it needs to be. 
     * 
     */

    function book_a_session_dashboard() {

        if ( $('.book-a-session-dashboard').length !== 0 ) {

            var resetting_password = book_a_session_api_order_authentication.resetting_password;

            if ( resetting_password ) book_a_session_reveal_fixed_notice( "You've logged in with a verification link, but you can't open any orders yet. Go to the Settings page to change your password, log out and log back in with your new password to regain full access.", "warning", "dismissable", "book-a-session-icon-vpn_key" );


            // Save all content div heights before changing them if necessary on load

            $('.book-a-session-dashboard .book-a-session-content').each( function() {

                if ( ! $(this).hasClass( 'book-a-session-hidden' ) ) {

                    if ( $('.book-a-session-sidebar').outerHeight() > $(this).outerHeight() ) {
                        $(this).css({ "height": $('.book-a-session-dashboard').outerHeight() + "px" });
                    }

                    if ( $('.book-a-session-sidebar').outerHeight() < $(this).outerHeight() ) {
                        $('.book-a-session-dashboard').css({ "height": $(this).outerHeight() + "px" });

                    } 

                }

                $(this).data( "css", { height: $(this).outerHeight() } );

            });

            // Event handling when links are clicked

            $('.book-a-session-dashboard nav ul li a[data-content], a[data-content]').click( function() {

                // The links have a data-content attribute containing the class name of the content div it must show, we're getting those here

                $data = $(this).attr('data-content');
                $content = $( '.' + $(this).attr('data-content') + '.book-a-session-content' ); 

                // Hide all content divs at first

                $( '.book-a-session-content' ).addClass( 'book-a-session-hidden' );

                // Reveal the content div associated with the clicked link

                $content.removeClass( 'book-a-session-hidden' );

                // Change the state of all links from current to a regular state, removing the CSS highlighting the link

                $('.book-a-session-dashboard nav ul li a[data-content]').removeClass('book-a-session-current');
                $('a[data-content]').removeClass('book-a-session-current');

                // Add the current state to the correct link

                $(this).addClass('book-a-session-current');
                $('a[data-content="' + $data + '"').addClass('book-a-session-current');

                // When the content's original height is shorter than the sidebar, change the dashboard and content height to the sidebar height.

                if ( $('.book-a-session-sidebar').outerHeight() > $content.data( "css" ).height ) {
                    $content.css({ "height": $('.book-a-session-sidebar').outerHeight() + "px" });
                    $('.book-a-session-dashboard').css({ "height": $('.book-a-session-sidebar').outerHeight() + "px" });
                }

                // When the content is taller than the sidebar, change the dashboard to the content height

                if ( $('.book-a-session-sidebar').outerHeight() < $content.data( "css" ).height ) {
                    $('.book-a-session-dashboard').css({ "height": $content.outerHeight() + "px" });
                } 

            });

            // Week Timetable Modal

            $('.book-a-session [data-modal]').on("click", function(){

                $modalClass = $(this).attr("data-modal");

                $( "." + $modalClass ).removeClass('book-a-session-hidden');

            });

            // Invoice viewer 

            $('a[data-invoice].book-a-session-view-invoice').click( function() {

                // Reveal loader

                book_a_session_reveal_loader();

                // Get invoice ID from PHP generated data-invoice attribute, then get custom endpoint route and nonce from wp_localize_script on dashboard.php

                $invoice_id = $(this).attr('data-invoice');
                var api_root = book_a_session_api_invoice_authentication.invoice_root;
                var api_nonce = book_a_session_api_invoice_authentication.invoice_nonce;

                // Request invoice by supplying found route, invoice id, and nonce. The invoice is only supplied if the cookie sent with this is validated and it belongs to a user who can either manage options or is the invoice recipient

                $.ajax({

                    url: api_root + $invoice_id,
                    method: 'GET',
                    beforeSend: function ( xhr ) {
                        xhr.setRequestHeader( 'X-WP-Nonce', api_nonce );
                    },

                }).done( function( data ) {

                    // If it works, insert iframe and invoice invoice as srcdoc

                    $('.book-a-session-modal-inner:not(.book-a-session-dashboard-week-timetable-inner)').append( "<iframe class='book-a-session-iframe'></iframe>" );
                    $('.book-a-session-modal-inner:not(.book-a-session-dashboard-week-timetable-inner) iframe').attr("srcdoc", "<style>* { font-family: Open Sans, Helvetica Neue, Helvetica, Arial, sans-serif; } body { font-size: 14px }</style>" + data);

                    book_a_session_reveal_modal();

                }).fail( function() {

                    // If it goes wrong append an error and reveal it

                    book_a_session_reveal_fixed_notice( "Sorry, we couldn't load your invoice", "error", "dismissable", "", 5000 );
                    book_a_session_hide_loader();

                });

            });

            // Session order viewer 

            $('a[data-session-order].book-a-session-view-order').click( function() {

                // Reveal loader

                book_a_session_reveal_loader();

                // Get order ID from PHP generated data-session-order attribute, then get custom endpoint route and nonce from wp_localize_script on dashboard.php

                $order_id = $(this).attr('data-session-order');
                var order_api_root = book_a_session_api_order_authentication.order_root;
                var api_nonce = book_a_session_api_order_authentication.order_nonce;

                // Request order by supplying found route, order id, and nonce. The session order is only supplied if the cookie sent with this is validated and it belongs to a user who has created the order, who is the practitioner for the order, or who can either manage options.

                $.ajax({

                    url: order_api_root + $order_id,
                    method: 'GET',
                    beforeSend: function ( xhr ) {
                        xhr.setRequestHeader( 'X-WP-Nonce', api_nonce );
                    },

                }).done( function( data ) {

                    // If it works, insert order viewer element into DOM and begin inserting any content depending on what's found

                    // Add card container class to remove default padding
                    $('.book-a-session-modal-inner:not(.book-a-session-dashboard-week-timetable-inner)').addClass( 'book-a-session-card-container book-a-session-order-viewer-container' );

                    // Order Viewer
                    $('.book-a-session-modal-inner:not(.book-a-session-dashboard-week-timetable-inner)').append( "<div class='book-a-session-order-viewer book-a-session-session-order'></div>" );

                    // Service image box, service title and grand total
                    $('.book-a-session-order-viewer').append( "<div class='book-a-session-service-image' style='background-image:url(" + data.order.service_image.url + ");'><div class='book-a-session-scrim'></div></div>" );
                    $('.book-a-session-order-viewer .book-a-session-scrim').append( "<h2>" + data.order.service_name + "</h2>" );
                    $('.book-a-session-order-viewer .book-a-session-scrim').append( "<h2>" + data.order.grand_total_tag + "</h2>" );

                    // Tab button container
                    $('.book-a-session-order-viewer').append( "<div class='book-a-session-tab-button-container'></div>" );

                    // Summary tab button
                    $('.book-a-session-order-viewer .book-a-session-tab-button-container')
                    .append( "<a href='javascript:void(0)' data-tab-content='summary' class='book-a-session-animated book-a-session-tab-button book-a-session-tab-button-current'><span class='book-a-session-icon book-a-session-icon-dashboard'></span>Summary</a>" );

                    // Tab content wrapper
                    $('.book-a-session-order-viewer')
                    .append( "<div class='book-a-session-tab-content-wrapper'></div>" );

                    // Tab content container
                    $('.book-a-session-order-viewer .book-a-session-tab-content-wrapper')
                    .append( "<div class='book-a-session-animated book-a-session-tab-content-container'></div>" );

                    // Summary tab content
                    $('.book-a-session-order-viewer .book-a-session-tab-content-container')
                    .append( "<div class='book-a-session-animated book-a-session-tab-content book-a-session-tab-content-summary'></div>" );

                    // Order ID
                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary').append('<h5 class="book-a-session-order-id">Order # ' + data.order.order_id + '</h5>');

                    // Order Date
                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary').append('<h5 class="book-a-session-order-date">Order Date: ' + data.order.created_datetime_converted_formatted.date + ' - ' + data.order.created_datetime_converted_formatted.time + '</h5>');

                    // Notice container
                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary')
                    .append( "<div class='book-a-session-notice book-a-session-card book-a-session-mini-card'><h2></h2></div>" );

                    // Payment due notice, only if the due amount is higher than 0 and they're not paying with cash in person

                    if ( data.order.total_due > 0.00 ) {

                        // Payment status as notice text
                        $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-notice h2')
                        .html( data.order.booking_status + ": " + data.order.total_due_tag + " due" );


                        if ( data.order.booking_status != 'Cancelled' ) {

                            if ( data.order.payment_method_id == 1 ) {

                                // Payment button
                                $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-notice')
                                .append( '<div id="paypal-button-container" class="book-a-session-payment-button-container">' );

                                if ( $('#paypal-button-container').length != 0 ) {

                                    var sandbox_live = book_a_session_api_order_authentication.payment_method_options_array.sandbox_live_1;
                                    var sandbox_id = book_a_session_api_order_authentication.payment_method_options_array.sandbox_id_1;
                                    var live_id = book_a_session_api_order_authentication.payment_method_options_array.live_id_1;
                                    
                                    var api_root = book_a_session_api_order_authentication.api_root;

                                    paypal.Button.render({
                                    
                                        // Set your environment
                                
                                        env: sandbox_live, // sandbox | production
                                
                                        // Specify the style of the button
                                
                                        style: {
                                            layout: 'horizontal', // horizontal | vertical
                                            label:  'checkout',  // checkout | credit | pay | buynow | generic
                                            size:   'responsive', // small | medium | large | responsive
                                            shape:  'pill',   // pill | rect
                                            color:  'gold'   // gold | blue | silver | black
                                        },
                                
                                        // PayPal Client IDs - replace with your own
                                        // Create a PayPal app: https://developer.paypal.com/developer/applications/create
                                
                                        client: {
                                            sandbox:    sandbox_id,
                                            production: live_id
                                        },
                                
                                        // Wait for the PayPal button to be clicked
                                
                                        payment: function( paypal_payment_data, paypal_payment_actions) {

                                            return paypal_payment_actions.payment.create({

                                                payment: {

                                                    transactions: [
                                                        {
                                                            amount: { 
                                                                total: data.order.total_due, 
                                                                currency: data.order.currency_code,
                                                            },
                                                            description: data.order.quantity > 1 ? data.order.quantity + " sessions of " + data.order.service_name + " - " + data.order.location_name + "." : data.order.service_namee + " - " + data.order.location_name + ".",
                                                            item_list: {
                                                                items: [
                                                                    {
                                                                        name: "Order # " + data.order.order_id + ": " + data.order.service_name,
                                                                        description: data.order.service_name + " - " + data.order.location_name,
                                                                        quantity: data.order.quantity.toString(),
                                                                        price: data.order.total.toString(),
                                                                        sku: ' SERVICE-ID-' + data.order.service_id,
                                                                        currency: data.order.currency_code,
                                                                    },
                                                                ]
                                                            }

                                                        }
                                                    ],

                                                }

                                            });

                                        },
                                
                                        // Wait for the payment to be authorized by the customer
                                
                                        onAuthorize: function( paypal_data, actions ) {

                                            return actions.payment.execute().then(function() {

                                                // Prepare payment information

                                                var intent = paypal_data.intent ? paypal_data.intent : "N/A";
                                                var paypal_orderID = paypal_data.orderID ? paypal_data.orderID : "N/A";
                                                var payerID = paypal_data.payerID ? paypal_data.payerID : "N/A";
                                                var paymentID = paypal_data.paymentID ? paypal_data.paymentID : "N/A";
                                                var paymentToken = paypal_data.paymentToken ? paypal_data.paymentToken : "N/A";

                                                var note_json = {

                                                    intent: intent,
                                                    orderID: paypal_orderID,
                                                    payerID: payerID,
                                                    paymentID: paymentID,
                                                    paymentToken: paymentToken,

                                                }

                                                book_a_session_reveal_fixed_notice( "Thanks! Your payment was successful, we're just working on creating your order. Your payment ID is " + charge_stripe_success.charge.id + ".", "success", "dismissable" );

                                                // Now record payment

                                                $.ajax({

                                                    url: api_root + "orders/" + data.order.order_id + "/payment/",
                                                    method: 'POST',
                                                    data: {

                                                        "payment_method_id": data.order.payment_method_id,
                                                        "invoice_id": data.invoices[0].id,
                                                        "payment_id": paymentID,
                                                        "total": data.order.total_due,
                                                        "success": 1,
                                                        "note": JSON.stringify( note_json ),
                                                        
                                                    },
                                                    beforeSend: function ( xhr ) {

                                                        xhr.setRequestHeader( 'X-WP-Nonce', api_nonce );

                                                    },
                        
                                                }).done( function( record_payment_success ) {

                                                    // Payment recorded

                                                    // PayPal thank you page

                                                    book_a_session_hide_and_clear_modal();

                                                    setTimeout(function(){
                                                        
                                                        // Thanks page container

                                                        $('.book-a-session-modal .book-a-session-modal-inner:not(.book-a-session-dashboard-week-timetable-inner)').append('<div class="book-a-session-animated book-a-session-thanks-page"></div').addClass('book-a-session-modal-thanks');

                                                        // Thanks page text

                                                        $('.book-a-session-thanks-page').append('<h1 class="book-a-session-thanks-title">Thanks!</h1>')
                                                        .append('<h3 class="book-a-session-thanks-subtitle">Your payment was successful.</h3>')
                                                        .append('<p>Your PayPal payment ID is ' + paymentID + '. Return to the dashboard and open this order again to review it.</p>');

                                                        // Order link

                                                        $('.book-a-session-thanks-page').append('<a href="javascript:void(0)" class="book-a-session-button-icon-right book-a-session-button-icon book-a-session-animated book-a-session-button book-a-session-high-button">Dashboard<span class="book-a-session-icon book-a-session-icon-dashboard"></span></a>');

                                                        $('.book-a-session-thanks-page a').on("click", function(){

                                                            book_a_session_hide_and_clear_modal();

                                                            setTimeout(function(){

                                                                $('.book-a-session-modal .book-a-session-modal-inner:not(.book-a-session-dashboard-week-timetable-inner)').removeClass('book-a-session-modal-thanks');
                                                                $('[data-session-order="' + data.order.order_id + '"]').first().click();

                                                            },200);

                                                        });

                                                        book_a_session_reveal_modal();

                                                        book_a_session_hide_loader();

                                                    },200);

                                                }).fail( function( record_payment_fail ){

                                                    // Payment not recorded

                                                    // PayPal thank you page

                                                    book_a_session_hide_and_clear_modal();

                                                    setTimeout(function(){
                                                        
                                                        // Thanks page container

                                                        $('.book-a-session-modal .book-a-session-modal-inner:not(.book-a-session-dashboard-week-timetable-inner)').append('<div class="book-a-session-animated book-a-session-thanks-page"></div').addClass('book-a-session-modal-thanks');

                                                        // Thanks page text

                                                        $('.book-a-session-thanks-page').append('<h1 class="book-a-session-thanks-title">Thanks!</h1>')
                                                        .append('<h3 class="book-a-session-thanks-subtitle">Your payment was successful.</h3>')
                                                        .append('<p>Your PayPal payment ID is ' + paymentID + '. Return to the dashboard and open this order again to review it.</p>');

                                                        // Order link

                                                        $('.book-a-session-thanks-page').append('<a href="javascript:void(0)" class="book-a-session-button-icon-right book-a-session-button-icon book-a-session-animated book-a-session-button book-a-session-high-button">Dashboard<span class="book-a-session-icon book-a-session-icon-dashboard"></span></a>');

                                                        $('.book-a-session-thanks-page a').on("click", function(){

                                                            book_a_session_hide_and_clear_modal();

                                                            setTimeout(function(){

                                                                $('.book-a-session-modal .book-a-session-modal-inner:not(.book-a-session-dashboard-week-timetable-inner)').removeClass('book-a-session-modal-thanks');
                                                                $('[data-session-order="' + data.order.order_id + '"]').first().click();

                                                            },200);

                                                        });

                                                        book_a_session_reveal_modal();

                                                        book_a_session_hide_loader();

                                                        // Let them know the payment wasn't recorded

                                                        book_a_session_reveal_fixed_notice( "Thanks! Your payment was successful, but there was an error updating your order to reflect this. Your PayPal payment ID is " + paymentID + ". Keep note of this number and contact us or your " + data.order.practitioner_title.toLowerCase() + ".", "warning", "dismissable" );

                                                    },200);
                                                    
                                                });

                                            });

                                        }
                                    
                                    }, '#paypal-button-container');
                            
                                }    

                            } else if ( data.order.payment_method_id == 2 ) {
                            
                                // Cash

                                if ( data.order.current_user_is_prac ) {

                                    // If the current user is the order's practitioner or an administrator, show a form allowing them to record a received cash payment
                                    
                                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-notice')
                                    .after("<div class='book-a-session-notice book-a-session-record-payment-container book-a-session-card book-a-session-mini-card'><h3>Record a cash payment</h3><div class='book-a-session-record-payment book-a-session-card book-a-session-mini-card'></div></div>");

                                    $(".book-a-session-record-payment").append("<input type='number' step='.01' min='0.01' placeholder='Enter the amount only, i.e. " + data.order.total_due + "' value='" + data.order.total_due + "' class='book-a-session-animated'><span class='book-a-session-record-payment-currency'>" + data.order.currency_code.toUpperCase() + "</span><a href='javascript:void(0)' class='book-a-session-record-payment-button book-a-session-animated book-a-session-button book-a-session-big-button book-a-session-high-button book-a-session-button-icon-right'><div class='book-a-session-button-text'>Record</div><span class='book-a-session-icon book-a-session-icon-banknote'></span></a>");
                                    
                                    var api_root = book_a_session_api_order_authentication.api_root;

                                    $('.book-a-session-record-payment a').on("click", function(){

                                        if ( $('.book-a-session-record-payment input[type="number"]').val() > 0.00 ) {

                                            book_a_session_reveal_loader();

                                            var date = new Date();
                                            var iso = date.toISOString().match(/(\d{4}\-\d{2}\-\d{2})T(\d{2}:\d{2}:\d{2})/);
                                            var note_json = { 

                                                "user_id": data.order.current_user_id,
                                                "user_username": data.order.current_user_username,
                                                "user_email": data.order.current_user_email,
                                                "date": iso[1],
                                                "time": iso[2],
                                                "datetime": iso[1] + " " + iso[2],

                                            }

                                            $.ajax({

                                                url: api_root + "orders/" + data.order.order_id + "/payment/",
                                                method: 'POST',
                                                data: {

                                                    "payment_method_id": data.order.payment_method_id,
                                                    "payment_id": "Cash Payment",
                                                    "invoice_id": data.invoices[0].id,
                                                    "total": $('.book-a-session-record-payment input[type="number"]').val(),
                                                    "success": 1,
                                                    "note": JSON.stringify( note_json ),
                                                    
                                                },
                                                beforeSend: function ( xhr ) {

                                                    xhr.setRequestHeader( 'X-WP-Nonce', api_nonce );

                                                },
                    
                                            }).done( function() {
                                                
                                                // Payment recorded

                                                book_a_session_reveal_fixed_notice( "Payment recorded successfully", "success", "dismissable", "book-a-session-icon-banknote", 5000 );

                                                book_a_session_hide_and_clear_modal();

                                                setTimeout(function(){

                                                    $('[data-session-order="' + data.order.order_id + '"]').first().click();

                                                },200);

                                            }).fail( function(){

                                                // Payment not recorded

                                                book_a_session_hide_loader();

                                                book_a_session_reveal_fixed_notice( "Sorry, this payment could not be recorded. Please try again.", "error", "dismissable", "book-a-session-icon-banknote", 5000 );

                                            });

                                        } else {

                                            // Payment less than 0.01

                                            book_a_session_reveal_fixed_notice( "The amount must be more than 0.01", "error", "dismissable", "book-a-session-icon-banknote", 5000 );

                                            $('.book-a-session-record-payment input[type="number"]').val();

                                        }

                                    });

                                }
                                
                            } else if ( data.order.payment_method_id == 3 ) {

                                // Vodafone Cash

                                if ( data.order.current_user_is_prac ) {

                                    // If the current user is the order's practitioner or an administrator, show a form allowing them to record a received Vodafone Cash payment
                                    
                                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-notice')
                                    .after("<div class='book-a-session-notice book-a-session-record-payment-container book-a-session-card book-a-session-mini-card'><h3>Record a Vodafone Cash payment from " + data.order.vfc_mobile + "</h3><div class='book-a-session-record-payment book-a-session-card book-a-session-mini-card'></div></div>");

                                    $(".book-a-session-record-payment").append("<input type='number' step='.01' min='0.01' placeholder='Enter the amount only, i.e. " + data.order.total_due + "' value='" + data.order.total_due + "' class='book-a-session-animated'><span class='book-a-session-record-payment-currency'>" + data.order.currency_code.toUpperCase() + "</span><a href='javascript:void(0)' class='book-a-session-record-payment-button book-a-session-animated book-a-session-button book-a-session-big-button book-a-session-high-button book-a-session-button-icon-right'><div class='book-a-session-button-text'>Record</div><span class='book-a-session-icon book-a-session-icon-vodafone-cash-logo-icon'></span></a>");
                                    
                                    var vfc_receiving_number = book_a_session_api_order_authentication.vfc_receiving_number;
                                    var api_root = book_a_session_api_order_authentication.api_root;

                                    $('.book-a-session-record-payment a').on("click", function(){

                                        if ( $('.book-a-session-record-payment input[type="number"]').val() > 0.00 ) {

                                            book_a_session_reveal_loader();

                                            var date = new Date();
                                            var iso = date.toISOString().match(/(\d{4}\-\d{2}\-\d{2})T(\d{2}:\d{2}:\d{2})/);
                                            var note_json = { 

                                                "user_id": data.order.current_user_id,
                                                "user_username": data.order.current_user_username,
                                                "user_email": data.order.current_user_email,
                                                "date": iso[1],
                                                "time": iso[2],
                                                "datetime": iso[1] + " " + iso[2],

                                            }

                                            $.ajax({

                                                url: api_root + "orders/" + data.order.order_id + "/payment/",
                                                method: 'POST',
                                                data: {

                                                    "payment_method_id": data.order.payment_method_id,
                                                    "payment_id": "Vodafone Cash Payment",
                                                    "invoice_id": data.invoices[0].id,
                                                    "total": $('.book-a-session-record-payment input[type="number"]').val(),
                                                    "success": 1,
                                                    "note": JSON.stringify( note_json ),
                                                    
                                                },
                                                beforeSend: function ( xhr ) {

                                                    xhr.setRequestHeader( 'X-WP-Nonce', api_nonce );

                                                },
                    
                                            }).done( function( record_payment_success ) {
                                                
                                                // Payment recorded

                                                book_a_session_reveal_fixed_notice( "Payment recorded successfully", "success", "dismissable", "book-a-session-icon-vodafone-cash-logo-icon", 5000 );

                                                book_a_session_hide_and_clear_modal();

                                                setTimeout(function(){

                                                    $('[data-session-order="' + data.order.order_id + '"]').first().click();

                                                },200);

                                            }).fail( function( record_payment_fail ){

                                                // Payment not recorded

                                                book_a_session_hide_loader();

                                                book_a_session_reveal_fixed_notice( "Sorry, this payment could not be recorded. Please try again.", "error", "dismissable", "book-a-session-icon-vodafone-cash-logo-icon", 5000 );

                                            });

                                        } else {

                                            // Payment less than 0.01

                                            book_a_session_reveal_fixed_notice( "The amount must be more than 0.01", "error", "dismissable", "book-a-session-icon-vodafone-cash-logo-icon", 5000 );

                                            $('.book-a-session-record-payment input[type="number"]').val();

                                        }

                                    });

                                } else {

                                    // If the current user is not the practitioner or the administrator, get the due date via AJAX for payment instructions

                                    book_a_session_reveal_loader();
                                
                                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-notice').css({ "flex-direction": "column" }).append("<h3 class='book-a-session-animated' style='padding-top:0.75em;font-size:1.2em;'>Loading...</h3>");
    
                                    var vfc_receiving_number = book_a_session_api_order_authentication.vfc_receiving_number;
                                    var api_root = book_a_session_api_order_authentication.api_root;
    
                                    // Prepare session times and date values to calculate the due date for VFC payment on the server's side
    
                                    var session_date_time_array = [];
    
                                    $.each( data.sessions, function( session_array_key, session_array_value ) {
    
                                        session_date_time_array.push({ 
    
                                            date: session_array_value.date,
                                            schedule_id: session_array_value.schedule_id,
        
                                        });
    
                                    });
                                    
                                    $.ajax({
    
                                        url: api_root + "invoices/due_date",
                                        method: 'POST',
                                        data: {
    
                                            region_id:        data.order.region_id,
                                            session_array:    session_date_time_array,
                                            
                                        },

                                    }).done( function( vfc_due_date_success ) {
    
                                        // Print VFC Due Date
    
                                        setTimeout( function(){
    
                                            $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-notice h3').addClass("book-a-session-hidden");
    
                                            setTimeout(function(){
    
                                                $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-notice h3')
                                                .html( 'Please make your payment to ' + vfc_receiving_number + ' by ' + vfc_due_date_success.date + ' at ' + vfc_due_date_success.time + '.' );
        
                                                $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-notice h3').removeClass('book-a-session-hidden');
        
                                            },200);
    
                                        }, 200 );
    
                                        book_a_session_hide_loader();
    
                                    }).fail( function( vfc_due_date_fail ){
    
                                        // Can't get VFC Due Date
    
                                        $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-notice h3').addClass("book-a-session-hidden");
    
                                        setTimeout(function(){
    
                                            $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-notice h3')
                                            .html( 'Please make your payment to the number in your invoice by the due date provided.' );
    
                                            $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-notice h3').removeClass('book-a-session-hidden');
    
                                        },200);
    
                                        book_a_session_hide_loader();
    
                                    });
    
                                }

                            } else if ( data.order.payment_method_id == 4 ) {

                                var payment_method_icon_html = "<span class='book-a-session-icon book-a-session-icon-shopping-basket'></span>";

                                // Payment button

                                $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-notice')
                                .append( '<div id="stripe-button-container" class="book-a-session-payment-button-container">' );
 
                                if ( $('#stripe-button-container').length != 0 ) {

                                    var api_root = book_a_session_api_order_authentication.api_root;
 
                                    // Stripe payment button

                                    $('#stripe-button-container').append( "<a href='javascript:void(0)' class='book-a-session-animated book-a-session-button book-a-session-big-button book-a-session-high-button'><div class='book-a-session-price-tag'><span class='book-a-session-price-text'>Pay now by card</span></div>" + payment_method_icon_html + "</a>" );

                                    // Stripe key

                                    var test_live = book_a_session_api_order_authentication.payment_method_options_array.test_live_4;
                                    var test_key = book_a_session_api_order_authentication.payment_method_options_array.test_pub_key_4;
                                    var live_key = book_a_session_api_order_authentication.payment_method_options_array.live_pub_key_4;

                                    var stripe_key = test_live == "live" ? live_key : test_key;

                                    var logo_image_square = book_a_session_api_order_authentication.logo_square_url[0];


                                    // Stripe handler

                                    var handler = StripeCheckout.configure({

                                        key: stripe_key,
                                        image: logo_image_square ? logo_image_square : 'https://stripe.com/img/documentation/checkout/marketplace.png',
                                        locale: 'auto',

                                        token: function( token ) {

                                            book_a_session_reveal_loader();

                                            // Charge Stripe
                                            
                                            $.ajax({

                                                url: api_root + "payment/stripe",
                                                method: 'POST',
                                                data: {

                                                    "stripeToken": token.id,
                                                    "stripeEmail": token.email,
                                                    "total": Math.round( data.order.total_due * 100 ),
                                                    "currency_code": data.order.currency_code,
                                                    
                                                },
                                                beforeSend: function ( xhr ) {

                                                    xhr.setRequestHeader( 'X-WP-Nonce', api_nonce );

                                                },
                    
                                            }).done( function( charge_stripe_success ) {

                                                // Charged successfully!

                                                book_a_session_reveal_fixed_notice( "Thanks! Your payment was successful, we're just working on creating your order. Your payment ID is " + charge_stripe_success.charge.id + ".", "success", "dismissable" );

                                                $.ajax({

                                                    url: api_root + "orders/" + data.order.order_id + "/payment/",
                                                    method: 'POST',
                                                    data: {

                                                        "payment_method_id": data.order.payment_method_id,
                                                        "invoice_id": data.invoices[0].id,
                                                        "payment_id": charge_stripe_success.charge.id,
                                                        "total": data.order.total_due,
                                                        "success": 1,
                                                        "note": JSON.stringify( token ),
                                                        
                                                    },
                                                    beforeSend: function ( xhr ) {

                                                        xhr.setRequestHeader( 'X-WP-Nonce', api_nonce );

                                                    },
                        
                                                }).done( function( record_payment_success ) {

                                                    // Payment recorded

                                                    // PayPal thank you page

                                                    book_a_session_hide_and_clear_modal();

                                                    setTimeout(function(){
                                                        
                                                        // Thanks page container

                                                        $('.book-a-session-modal .book-a-session-modal-inner:not(.book-a-session-dashboard-week-timetable-inner)').append('<div class="book-a-session-animated book-a-session-thanks-page"></div').addClass('book-a-session-modal-thanks');

                                                        // Thanks page text

                                                        $('.book-a-session-thanks-page').append('<h1 class="book-a-session-thanks-title">Thanks!</h1>')
                                                        .append('<h3 class="book-a-session-thanks-subtitle">Your payment was successful.</h3>')
                                                        .append('<p>Your Stripe charge ID is ' + charge_stripe_success.charge.id + '. Return to the dashboard and open this order again to review it.</p>');

                                                        // Order link

                                                        $('.book-a-session-thanks-page').append('<a href="javascript:void(0)" class="book-a-session-button-icon-right book-a-session-button-icon book-a-session-animated book-a-session-button book-a-session-high-button">Dashboard<span class="book-a-session-icon book-a-session-icon-dashboard"></span></a>');

                                                        $('.book-a-session-thanks-page a').on("click", function(){

                                                            book_a_session_hide_and_clear_modal();

                                                            setTimeout(function(){

                                                                $('.book-a-session-modal .book-a-session-modal-inner:not(.book-a-session-dashboard-week-timetable-inner)').removeClass('book-a-session-modal-thanks');
                                                                $('[data-session-order="' + data.order.order_id + '"]').first().click();

                                                            },200);

                                                        });

                                                        book_a_session_reveal_modal();

                                                        book_a_session_hide_loader();

                                                    },200);

                                                }).fail( function( record_payment_fail ){

                                                    // Payment not recorded

                                                    book_a_session_hide_and_clear_modal();

                                                    setTimeout(function(){
                                                        
                                                        // Thanks page container

                                                        $('.book-a-session-modal .book-a-session-modal-inner:not(.book-a-session-dashboard-week-timetable-inner)').append('<div class="book-a-session-animated book-a-session-thanks-page"></div').addClass('book-a-session-modal-thanks');

                                                        // Thanks page text

                                                        $('.book-a-session-thanks-page').append('<h1 class="book-a-session-thanks-title">Thanks!</h1>')
                                                        .append('<h3 class="book-a-session-thanks-subtitle">Your payment was successful.</h3>')
                                                        .append('<p>Your Stripe charge ID is ' + charge_stripe_success.charge.id + '. Return to the dashboard and open this order again to review it.</p>');

                                                        // Order link

                                                        $('.book-a-session-thanks-page').append('<a href="javascript:void(0)" class="book-a-session-button-icon-right book-a-session-button-icon book-a-session-animated book-a-session-button book-a-session-high-button">Dashboard<span class="book-a-session-icon book-a-session-icon-dashboard"></span></a>');

                                                        $('.book-a-session-thanks-page a').on("click", function(){

                                                            book_a_session_hide_and_clear_modal();

                                                            setTimeout(function(){

                                                                $('.book-a-session-modal .book-a-session-modal-inner:not(.book-a-session-dashboard-week-timetable-inner)').removeClass('book-a-session-modal-thanks');
                                                                $('[data-session-order="' + data.order.order_id + '"]').first().click();

                                                            },200);

                                                        });

                                                        book_a_session_reveal_modal();

                                                        book_a_session_hide_loader();

                                                        book_a_session_reveal_fixed_notice( "Thanks! Your payment was successful, but we couldn't update your order to reflect that. Your payment ID is " + charge_stripe_success.charge.id + ". Contact your " + data.order.practitioner_title.toLowerCase() + " to let them know.", "warning", "dismissable" );

                                                    },200);

                                                });

                                            }).fail( function( charge_stripe_fail ){

                                                // Charge could not be request
                                                
                                                book_a_session_hide_loader();

                                                book_a_session_reveal_fixed_notice( "Sorry, your payment could not be processed. Please try another card, or another payment method.", "error", "dismissable", 5000 );

                                            });                                        

                                        }

                                    });

                                    $('#stripe-button-container a').on( "click", function( e ) {

                                        book_a_session_reveal_inline_loader( $( e.currentTarget ) );

                                        // Open Checkout with further options:

                                        var stripe_description = data.order.quantity > 1 ? data.order.quantity +  " sessions of " + data.order.service_name + " - " + data.order.service_name + "." : data.order.service_name + " - " + data.order.service_name + ".";
                                        var site_name = book_a_session_api_order_authentication.site_name;

                                        handler.open({

                                            name: site_name,
                                            description: stripe_description,
                                            zipCode: true,
                                            currency: data.order.currency_code,

                                            // Amount needs to be an integer, so we add two extra zeroes to charge accuarately

                                            amount: Math.round( data.order.total_due * 100 ),

                                        });

                                        e.preventDefault();

                                        book_a_session_hide_inline_loader( $( e.currentTarget ) );


                                    });
                                    
                                    // Close Checkout on page navigation:

                                    window.addEventListener('popstate', function() {

                                        handler.close();

                                    });

                                    $('.book-a-session-modal:not(.book-a-session-modal-week-timetable) .book-a-session-icon-close, .book-a-session-modal').click( function( e ) {

                                        // If the event's true target wasn't clicked ( the close button or the actual modal background ), don't do anything, that is, clicking the modal inner shouldn't work just because it's a modal child
                                
                                        if ( e.target !== e.currentTarget ) return;
                                
                                        handler.close();
                                
                                    });

                                }

                            }

                        }

                    // First / next / last session notice or project milestone notice (in a future version) if there's no online or VFC payment due

                    } else {

                        if ( data.sessions ) {

                            // If this is a session order and there are multiple sessions

                            if ( data.order.quantity > 1 ) {

                                var $match = false;

                                $.each( data.sessions, function( key, value ) {
                                
                                    // If the session is in the future

                                    if ( value.session_time_converted_array.upcoming ) {

                                        if ( key === 0 ) {

                                            // If it's the first session in the order

                                            $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-notice h2')
                                            .html( "Your first session is on " + value.session_time_converted_array.start_date_formatted + " for " + value.session_time_converted_array.time_string + "." );    

                                        } else {

                                            // If it's not the first, it's the next session instead

                                            $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-notice h2')
                                            .html( "Your next session is on " + value.session_time_converted_array.start_date_formatted + " for " + value.session_time_converted_array.time_string + "." );    

                                        }

                                        // Break the loop

                                        $match = true;

                                        return false;

                                    }

                                    // If this is the last session of the array, that means all the sessions are in the past, so print the date of their last session and a thank you

                                    if ( key + 1 === data.sessions.length ) {

                                        $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-notice h2')
                                        .html( "Your last session was on " + value.session_time_converted_array.start_date_formatted + " for " + value.session_time_converted_array.time_string + ". We hope you enjoyed it!" ); 

                                    }

                                });

                            } else {

                                // In the case of just a single session in the order, check if upcoming or not

                                $.each( data.sessions, function( key, value ) {

                                    if ( value.session_time_converted_array.upcoming ) {

                                        // The session hasn't started

                                        $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-notice h2')
                                        .html( "Your session is on " + value.session_time_converted_array.start_date_formatted + " for " + value.session_time_converted_array.time_string + "." ); 

                                    } else {

                                        // The session was completed

                                        $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-notice h2')
                                        .html( "Your session was on " + value.session_time_converted_array.start_date_formatted + " for " + value.session_time_converted_array.time_string + ". We hope you enjoyed it!" ); 

                                    }

                                });

                            }

                        } else {

                            // Projects?

                        }

                    }

                    // Summary tab content group container (groups the groups)

                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary').append( '<div class="book-a-session-content-group-container"></div>' )

                    // Details Title
                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container')
                    .append('<div class="book-a-session-tab-content-group book-a-session-tab-content-group-details"><h5 class="book-a-session-subtitle">Details</h5></div>');

                    // Details mini card container
                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container .book-a-session-tab-content-group-details')
                    .append('<div class="book-a-session-card book-a-session-mini-card book-a-session-tab-content-summary-details"></div>');

                    // Quantity
                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container .book-a-session-tab-content-group-details .book-a-session-tab-content-summary-details')
                    .append( '<span class="book-a-session-flex-table-key">Sessions</span><span class="book-a-session-flex-table-value">' + 
                    data.order.quantity + '</span>' );

                    // Service type name
                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container .book-a-session-tab-content-group-details .book-a-session-tab-content-summary-details')
                    .append( '<span class="book-a-session-flex-table-key">Service Type</span><span class="book-a-session-flex-table-value">' + 
                    data.order.service_type_name + '</span>');

                    // Service name
                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container .book-a-session-tab-content-group-details .book-a-session-tab-content-summary-details')
                    .append( '<span class="book-a-session-flex-table-key">Service</span><span class="book-a-session-flex-table-value">' + 
                    data.order.service_name + '</span>');

                    // Location
                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container .book-a-session-tab-content-group-details .book-a-session-tab-content-summary-details')
                    .append( '<span class="book-a-session-flex-table-key">Location</span><span class="book-a-session-flex-table-value">' + 
                    data.order.location_name + '</span>' );

                    // Print both client name and practitioner name if it's an admin (as they're the only type of user who can view the order if they're not the client nor the practitioner)

                    if ( data.order.current_user_is_admin ) {

                        // Client Name
                        $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container .book-a-session-tab-content-group-details .book-a-session-tab-content-summary-details')
                        .append( '<span class="book-a-session-flex-table-key">Client</span><span class="book-a-session-flex-table-value">' + 
                        data.order.client_name + '</span>' );

                        // Practitioner Name
                        $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container .book-a-session-tab-content-group-details .book-a-session-tab-content-summary-details')
                        .append( '<span class="book-a-session-flex-table-key">' + data.order.practitioner_title.charAt(0).toUpperCase() + data.order.practitioner_title.slice(1) + '</span><span class="book-a-session-flex-table-value">' + 
                        data.order.practitioner_name + '</span>' );

                    } else {

                        // If not an admin, check if they're a practitioner or a client. Practitioners are shown the client name only, and clients are shown the practitioner name only

                        if ( data.order.current_user_is_prac ) {

                            // Client Name
                            $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container .book-a-session-tab-content-group-details .book-a-session-tab-content-summary-details')
                            .append( '<span class="book-a-session-flex-table-key">Client</span><span class="book-a-session-flex-table-value">' + 
                            data.order.client_name + '</span>' );
    
                        } else {
    
                            // Practitioner Name
                            $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container .book-a-session-tab-content-group-details .book-a-session-tab-content-summary-details')
                            .append( '<span class="book-a-session-flex-table-key">' + data.order.practitioner_title.charAt(0).toUpperCase() + data.order.practitioner_title.slice(1) + '</span><span class="book-a-session-flex-table-value">' + 
                            data.order.practitioner_name + '</span>' );
    
                        } 

                    }
                    
                    // Payment Method
                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container .book-a-session-tab-content-group-details .book-a-session-tab-content-summary-details')
                    .append( '<span class="book-a-session-flex-table-key">Payment Method</span><span class="book-a-session-flex-table-value">' + 
                    data.order.payment_method_name + '</span>' );                
                    
                    // Price Breakdown Title
                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container').append('<div class="book-a-session-tab-content-group book-a-session-tab-content-group-price"><h5 class="book-a-session-subtitle">Price</h5></div>');

                    // Price mini card container
                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container .book-a-session-tab-content-group-price').append('<div class="book-a-session-card book-a-session-mini-card book-a-session-tab-content-summary-price"></div>');
                    
                    // Base Price
                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container .book-a-session-tab-content-group-price .book-a-session-tab-content-summary-price')
                    .append( '<span class="book-a-session-flex-table-key">Subtotal</span><span class="book-a-session-flex-table-value">' + 
                    ( data.order.base_price ).toFixed(2) + '</span>');
                    
                    // Location Charge
                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container .book-a-session-tab-content-group-price .book-a-session-tab-content-summary-price')
                    .append( '<span class="book-a-session-flex-table-key">Location</span><span class="book-a-session-flex-table-value">' + 
                    ( data.order.location_charge ).toFixed(2) + '</span>' );
                    
                    // Discount 
                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container .book-a-session-tab-content-group-price .book-a-session-tab-content-summary-price')
                    .append( '<span class="book-a-session-flex-table-key">Discount</span><span class="book-a-session-flex-table-value">-' + 
                    ( data.order.discount ).toFixed(2) + '</span>' );
                    
                    // Total 
                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container .book-a-session-tab-content-group-price .book-a-session-tab-content-summary-price').
                    append( '<span class="book-a-session-flex-table-key">Total</span><span class="book-a-session-flex-table-value">' + 
                    ( data.order.total ).toFixed(2) + '</span>' );
                    
                    // Amount Paid 
                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container .book-a-session-tab-content-group-price .book-a-session-tab-content-summary-price')
                    .append( '<span class="book-a-session-flex-table-key">Paid</span><span class="book-a-session-flex-table-value">-' + 
                    data.order.amount_paid.toFixed(2) + '</span>' );
                    
                    // Total 
                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container .book-a-session-tab-content-group-price .book-a-session-tab-content-summary-price')
                    .append( '<span class="book-a-session-flex-table-key book-a-session-total-due">Total due</span><span class="book-a-session-flex-table-value book-a-session-total-due">' + 
                    data.order.total_due_tag + '</span>' );

                    // Location Title
                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container').append('<div class="book-a-session-tab-content-group book-a-session-tab-content-group-location"><h5 class="book-a-session-subtitle">Location</h5></div>');

                    // Location mini card container
                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container .book-a-session-tab-content-group-location').append('<div class="book-a-session-card book-a-session-mini-card book-a-session-tab-content-summary-location"></div>');
                    
                    // Location name
                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container .book-a-session-tab-content-group-location .book-a-session-tab-content-summary-location')
                    .append( '<span class="book-a-session-flex-table-key">Name</span><span class="book-a-session-flex-table-value">' + 
                    ( data.order.location_name ) + '</span>');

                    // Location address
                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container .book-a-session-tab-content-group-location .book-a-session-tab-content-summary-location')
                    .append( '<span class="book-a-session-flex-table-key">Address</span><span class="book-a-session-flex-table-value"></span>');

                    // If there's no address, notify

                    if ( ! data.order.location_address_line_1 && ! data.order.location_address_line_2 && ! data.order.location_address_line_3 && ! data.order.location_city && ! data.order.location_postcode_zipcode && ! data.order.location_country ) {

                        $('.book-a-session-order-viewer .book-a-session-tab-content-summary-location .book-a-session-flex-table-value').last().append( "Unavailable. Please message your " + data.order.practitioner_title.toLowerCase() + " for more information." );

                    } else {

                        // Print address

                        if ( data.order.location_address_line_1 ) $('.book-a-session-order-viewer .book-a-session-tab-content-summary-location .book-a-session-flex-table-value').last().append( data.order.location_address_line_1 + "<br>" );
                        if ( data.order.location_address_line_2 ) $('.book-a-session-order-viewer .book-a-session-tab-content-summary-location .book-a-session-flex-table-value').last().append( data.order.location_address_line_2 + "<br>" );
                        if ( data.order.location_address_line_3 ) $('.book-a-session-order-viewer .book-a-session-tab-content-summary-location .book-a-session-flex-table-value').last().append( data.order.location_address_line_3 + "<br>" );
                        if ( data.order.location_city ) $('.book-a-session-order-viewer .book-a-session-tab-content-summary-location .book-a-session-flex-table-value').last().append( data.order.location_city + "<br>" );
                        if ( data.order.location_postcode_zipcode ) $('.book-a-session-order-viewer .book-a-session-tab-content-summary-location .book-a-session-flex-table-value').last().append( data.order.location_postcode_zipcode + "<br>" );
                        if ( data.order.location_country ) $('.book-a-session-order-viewer .book-a-session-tab-content-summary-location .book-a-session-flex-table-value').last().append( data.order.location_country + "<br>" );
    
                        // Remove the last break line tag
    
                        $('.book-a-session-order-viewer .book-a-session-tab-content-summary-location .book-a-session-flex-table-value').last().find("br").last().remove();

                    }
                    
                    // Messages Title
                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container').append('<div class="book-a-session-tab-content-group book-a-session-tab-content-group-messages"><h5 class="book-a-session-subtitle">Latest message</h5></div>');

                    // Messages mini card container
                    $('.book-a-session-order-viewer .book-a-session-tab-content-summary .book-a-session-content-group-container .book-a-session-tab-content-group-messages').append('<div class="book-a-session-card book-a-session-mini-card book-a-session-tab-content-summary-messages"></div>');
                    
                    if ( data.messages[data.messages.length-1] ) { 
                        
                        if ( data.order.current_user_is_prac ) {

                            // Practitioner

                            if ( data.messages[data.messages.length-1].user_id == data.order.user_id ) {

                                $('.book-a-session-tab-content-group-messages .book-a-session-tab-content-summary-messages').append( "<div class='book-a-session-card book-a-session-mini-card book-a-session-message book-a-session-message-other'>" + 
                                "<div class='book-a-session-message-content'>" + 
                                data.messages[data.messages.length-1].content + "</div>" + 
                                "<div class='book-a-session-message-meta'><span class='book-a-session-message-meta-name'>" + 
                                data.messages[data.messages.length-1].user_name + "</span>" +
                                "<span class='book-a-session-message-meta-time'>" + data.messages[data.messages.length-1].time_converted_formatted  + "</span>" + 
                                "<span class='book-a-session-message-meta-date'>" + data.messages[data.messages.length-1].date_converted_formatted  + "</span>" + 
                                "</div></div>" );;
    
                            } else if ( data.messages[data.messages.length-1].user_id == data.order.practitioner_id ) {
    
                                $('.book-a-session-tab-content-group-messages .book-a-session-tab-content-summary-messages').append( "<div class='book-a-session-card book-a-session-mini-card book-a-session-message book-a-session-message-user'>" + 
                                "<div class='book-a-session-message-content'>" + 
                                data.messages[data.messages.length-1].content + "</div>" +
                                "<div class='book-a-session-message-meta'><span class='book-a-session-message-meta-name'>" + 
                                data.messages[data.messages.length-1].user_name + "</span>" +
                                "<span class='book-a-session-message-meta-time'>" + data.messages[data.messages.length-1].time_converted_formatted  + "</span>" + 
                                "<span class='book-a-session-message-meta-date'>" + data.messages[data.messages.length-1].date_converted_formatted  + "</span>" + 
                                "</div></div>" );
    
                            }

                        } else {

                            // Client

                            if ( data.messages[data.messages.length-1].user_id == data.order.user_id ) {

                                $('.book-a-session-tab-content-group-messages .book-a-session-tab-content-summary-messages').append( "<div class='book-a-session-card book-a-session-mini-card book-a-session-message book-a-session-message-user'>" + 
                                "<div class='book-a-session-message-content'>" + 
                                data.messages[data.messages.length-1].content + "</div>" + 
                                "<div class='book-a-session-message-meta'><span class='book-a-session-message-meta-name'>" + 
                                data.messages[data.messages.length-1].user_name + "</span>" +
                                "<span class='book-a-session-message-meta-time'>" + data.messages[data.messages.length-1].time_converted_formatted  + "</span>" + 
                                "<span class='book-a-session-message-meta-date'>" + data.messages[data.messages.length-1].date_converted_formatted  + "</span>" + 
                                "</div></div>" );;
    
                            } else if ( data.messages[data.messages.length-1].user_id == data.order.practitioner_id ) {
    
                                $('.book-a-session-tab-content-group-messages .book-a-session-tab-content-summary-messages').append( "<div class='book-a-session-card book-a-session-mini-card book-a-session-message book-a-session-message-other'>" + 
                                "<div class='book-a-session-message-content'>" + 
                                data.messages[data.messages.length-1].content + "</div>" +
                                "<div class='book-a-session-message-meta'><span class='book-a-session-message-meta-name'>" + 
                                data.messages[data.messages.length-1].user_name + "</span>" +
                                "<span class='book-a-session-message-meta-time'>" + data.messages[data.messages.length-1].time_converted_formatted  + "</span>" + 
                                "<span class='book-a-session-message-meta-date'>" + data.messages[data.messages.length-1].date_converted_formatted  + "</span>" + 
                                "</div></div>" );
    
                            }

                        }


        
                    } else {

                        $('.book-a-session-tab-content-group-messages .book-a-session-tab-content-summary-messages').append( "<p>Nothing yet!</p>" );
        
                    }

                    // Sessions

                    if ( data.sessions ) {

                        // Tab button
                        $('.book-a-session-order-viewer .book-a-session-tab-button-container')
                        .append( "<a href='javascript:void(0)' data-tab-content='sessions' class='book-a-session-animated book-a-session-tab-button'><span class='book-a-session-icon book-a-session-icon-calendar'></span>Sessions</a>" );

                        // Content
                        $('.book-a-session-order-viewer .book-a-session-tab-content-container')
                        .append( "<div class='book-a-session-animated book-a-session-tab-content book-a-session-tab-content-sessions book-a-session-hidden'><h2 class='book-a-session-tab-content-title'>Sessions</h2></div>" );

                        // Table
                        $('.book-a-session-order-viewer .book-a-session-tab-content-sessions').append( "<table class='book-a-session-table book-a-session-card book-a-session-mini-card'><thead><tr></tr></thead><tbody></tbody></table>" );

                        // Table header
                        $('.book-a-session-order-viewer .book-a-session-tab-content-sessions table.book-a-session-table thead tr')
                        .append( "<th scope='col'>Date</th>" )
                        .append( "<th scope='col'>Time</th>" )
                        .append( "<th scope='col'>Status</th>" );
                    
                        // Table rows
                        $.each( data.sessions, function( key, value ) {

                            if ( data.order.booking_status === "Booked" ) {

                                $status = value.session_time_converted_array.upcoming ? "Upcoming" : "Done";

                            } else $status = data.order.booking_status;

                            $('.book-a-session-order-viewer .book-a-session-tab-content-sessions table.book-a-session-table tbody').append( "<tr>" );

                            $('.book-a-session-order-viewer .book-a-session-tab-content-sessions table.book-a-session-table tbody tr').last()
                            .append( "<td>" + value.session_time_converted_array.start_date_formatted + "</td>" )
                            .append( "<td>" + value.session_time_converted_array.time_string + "</td>" )
                            .append( "<td>" + $status + "</td>" );

                            $('.book-a-session-order-viewer .book-a-session-tab-content-sessions table.book-a-session-table tbody tr').last().addClass( "book-a-session-session-" + $status.replace( " ", "-", ).toLowerCase() );
                        
                        });
                        
                    }

                    // Messages tab

                    // Tab button
                    $('.book-a-session-order-viewer .book-a-session-tab-button-container')
                    .append( "<a href='javascript:void(0)' data-tab-content='messages' class='book-a-session-animated book-a-session-tab-button'><span class='book-a-session-icon book-a-session-icon-paper-plane'></span>Messages</a>" );

                    // Content
                    $('.book-a-session-order-viewer .book-a-session-tab-content-container')
                    .append( "<div class='book-a-session-animated book-a-session-tab-content book-a-session-tab-content-messages book-a-session-hidden'><div class='book-a-session-tab-content-messages-viewer'><h2 class='book-a-session-tab-content-title'>Messages</h2></div></div>" );

                    // Messages 
                    if ( data.messages ) {

                        $.each( data.messages, function(  key, value ) {

                            if ( data.order.current_user_is_prac ) {

                                // Practitioner

                                if ( value.user_id == data.order.user_id ) {

                                    $('.book-a-session-tab-content-container .book-a-session-tab-content-messages .book-a-session-tab-content-messages-viewer').append( "<div id='book-a-session-message-" + value.id + "' class='book-a-session-card book-a-session-mini-card book-a-session-message book-a-session-message-other'>" + 
                                    "<div class='book-a-session-message-content'>" + 
                                    value.content + "</div>" +
                                    "<div class='book-a-session-message-meta'><span class='book-a-session-message-meta-name'>" + 
                                    value.user_name + "</span>" +
                                    "<span class='book-a-session-message-meta-time'>" + value.time_converted_formatted  + "</span>" + 
                                    "<span class='book-a-session-message-meta-date'>" + value.date_converted_formatted  + "</span>" + 
                                    "</div></div>" );
    
                                } else if ( value.user_id == data.order.practitioner_id ) {
    
                                    $('.book-a-session-tab-content-container .book-a-session-tab-content-messages .book-a-session-tab-content-messages-viewer').append( "<div id='book-a-session-message-" + value.id + "' class='book-a-session-card book-a-session-mini-card book-a-session-message book-a-session-message-user'>" + 
                                    "<div class='book-a-session-message-content'>" + 
                                    value.content + "</div>" +
                                    "<div class='book-a-session-message-meta'><span class='book-a-session-message-meta-name'>" + 
                                    value.user_name + "</span>" +
                                    "<span class='book-a-session-message-meta-time'>" + value.time_converted_formatted  + "</span>" + 
                                    "<span class='book-a-session-message-meta-date'>" + value.date_converted_formatted  + "</span>" + 
                                    "</div></div>" );
    
                                }

                            } else {

                                // Client

                                if ( value.user_id == data.order.user_id ) {

                                    $('.book-a-session-tab-content-container .book-a-session-tab-content-messages .book-a-session-tab-content-messages-viewer').append( "<div id='book-a-session-message-" + value.id + "' class='book-a-session-card book-a-session-mini-card book-a-session-message book-a-session-message-user'>" + 
                                    "<div class='book-a-session-message-content'>" + 
                                    value.content + "</div>" +
                                    "<div class='book-a-session-message-meta'><span class='book-a-session-message-meta-name'>" + 
                                    value.user_name + "</span>" +
                                    "<span class='book-a-session-message-meta-time'>" + value.time_converted_formatted  + "</span>" + 
                                    "<span class='book-a-session-message-meta-date'>" + value.date_converted_formatted  + "</span>" + 
                                    "</div></div>" );
    
                                } else if ( value.user_id == data.order.practitioner_id ) {
    
                                    $('.book-a-session-tab-content-container .book-a-session-tab-content-messages .book-a-session-tab-content-messages-viewer').append( "<div id='book-a-session-message-" + value.id + "' class='book-a-session-card book-a-session-mini-card book-a-session-message book-a-session-message-other'>" + 
                                    "<div class='book-a-session-message-content'>" + 
                                    value.content + "</div>" +
                                    "<div class='book-a-session-message-meta'><span class='book-a-session-message-meta-name'>" + 
                                    value.user_name + "</span>" +
                                    "<span class='book-a-session-message-meta-time'>" + value.time_converted_formatted  + "</span>" + 
                                    "<span class='book-a-session-message-meta-date'>" + value.date_converted_formatted  + "</span>" + 
                                    "</div></div>" );
    
                                }

                            }

                        });
        
                    } else {

                        $('.book-a-session-tab-content-container .book-a-session-tab-content-messages .book-a-session-tab-content-messages-viewer').append( "<p>Nothing yet!</p>" );
        
                    }
                    
                    // Send message

                    $('.book-a-session-order-viewer .book-a-session-tab-content-container .book-a-session-tab-content-messages')
                    .append( "<div class='book-a-session-send-message'><textarea name='send_message' placeholder='Type here to send a message...'></textarea><a href='javascript:void(0)' class='book-a-session-animated book-a-session-button book-a-session-high-button book-a-session-big-button book-a-session-send-message-button'><span class='book-a-session-icon book-a-session-icon-send'></span>Send</a></div>" );

                    $('.book-a-session-order-viewer .book-a-session-send-message-button').click( function(e) {

                        book_a_session_reveal_fixed_notice( "Sending...", "", "dismissable", "book-a-session-icon-hourglass-end" );

                        $content = $('.book-a-session-order-viewer .book-a-session-send-message textarea').val();

                        if ( $content.replace(/ /g,'').length > 0 ) {

                            var client_or_practitioner_id = data.order.current_user_is_prac ? data.order.practitioner_id : data.order.user_id;

                            if ( data.order.current_user_is_admin ) client_or_practitioner_id = data.order.current_user_id;

                            $.ajax({

                                url: order_api_root + $order_id + "/messages/" + client_or_practitioner_id,
                                method: 'POST',
                                data : $content,
                                contentType: 'applcation/json',
                                beforeSend: function ( xhr ) {
                                    xhr.setRequestHeader( 'X-WP-Nonce', api_nonce );
                                },
    
                            }).done( function() { 
    
                                // Get latest messages (0 is passed instead of the user id to denote that we want all messages from all users)
    
                                $.ajax({
    
                                    url: order_api_root + $order_id + "/messages/0",
                                    method: 'GET',
                                    beforeSend: function ( xhr ) {
                                        xhr.setRequestHeader( 'X-WP-Nonce', api_nonce );
                                    },
            
                                }).done( function( $messages ) { 
    
                                    $.each( $messages, function( key, value ) {
    
                                        // Check if the message is already in the DOM, if it isn't append it
    
                                        if ( $('.book-a-session-message#book-a-session-message-' + value.id ).length === 0 ) {

                                            if ( data.order.current_user_is_prac ) {

                                                // For practitioners or admins

                                                if ( value.user_id == data.order.user_id ) {
    
                                                    $('.book-a-session-tab-content-container .book-a-session-tab-content-messages .book-a-session-tab-content-messages-viewer').append( "<div id='book-a-session-message-" + value.id + "' class='book-a-session-animated book-a-session-hidden book-a-session-card book-a-session-mini-card book-a-session-message book-a-session-message-other'>" + 
                                                    "<div class='book-a-session-message-content'>" + 
                                                    value.content + "</div>" +
                                                    "<div class='book-a-session-message-meta'><span class='book-a-session-message-meta-name'>" + 
                                                    value.user_name + "</span>" +
                                                    "<span class='book-a-session-message-meta-time'>" + value.time_converted_formatted  + "</span>" + 
                                                    "<span class='book-a-session-message-meta-date'>" + value.date_converted_formatted  + "</span>" + 
                                                    "</div></div>" );
                        
                                                } else if ( value.user_id == data.order.practitioner_id ) {
                        
                                                    $('.book-a-session-tab-content-container .book-a-session-tab-content-messages .book-a-session-tab-content-messages-viewer').append( "<div id='book-a-session-message-" + value.id + "' class='book-a-session-animated book-a-session-hidden book-a-session-card book-a-session-mini-card book-a-session-message book-a-session-message-user'>" + 
                                                    "<div class='book-a-session-message-content'>" + 
                                                    value.content + "</div>" + 
                                                    "<div class='book-a-session-message-meta'><span class='book-a-session-message-meta-name'>" + 
                                                    value.user_name + "</span>" +
                                                    "<span class='book-a-session-message-meta-time'>" + value.time_converted_formatted  + "</span>" + 
                                                    "<span class='book-a-session-message-meta-date'>" + value.date_converted_formatted  + "</span>" + 
                                                    "</div></div>" );
                        
                                                }

                                            } else {

                                                // For clients

                                                if ( value.user_id == data.order.user_id ) {
    
                                                    $('.book-a-session-tab-content-container .book-a-session-tab-content-messages .book-a-session-tab-content-messages-viewer').append( "<div id='book-a-session-message-" + value.id + "' class='book-a-session-animated book-a-session-hidden book-a-session-card book-a-session-mini-card book-a-session-message book-a-session-message-user'>" + 
                                                    "<div class='book-a-session-message-content'>" + 
                                                    value.content + "</div>" +
                                                    "<div class='book-a-session-message-meta'><span class='book-a-session-message-meta-name'>" + 
                                                    value.user_name + "</span>" +
                                                    "<span class='book-a-session-message-meta-time'>" + value.time_converted_formatted  + "</span>" + 
                                                    "<span class='book-a-session-message-meta-date'>" + value.date_converted_formatted  + "</span>" + 
                                                    "</div></div>" );
                        
                                                } else if ( value.user_id == data.order.practitioner_id ) {
                        
                                                    $('.book-a-session-tab-content-container .book-a-session-tab-content-messages .book-a-session-tab-content-messages-viewer').append( "<div id='book-a-session-message-" + value.id + "' class='book-a-session-animated book-a-session-hidden book-a-session-card book-a-session-mini-card book-a-session-message book-a-session-message-other'>" + 
                                                    "<div class='book-a-session-message-content'>" + 
                                                    value.content + "</div>" + 
                                                    "<div class='book-a-session-message-meta'><span class='book-a-session-message-meta-name'>" + 
                                                    value.user_name + "</span>" +
                                                    "<span class='book-a-session-message-meta-time'>" + value.time_converted_formatted  + "</span>" + 
                                                    "<span class='book-a-session-message-meta-date'>" + value.date_converted_formatted  + "</span>" + 
                                                    "</div></div>" );
                        
                                                }

                                            }
    
                                            // Reveal message
    
                                            $('.book-a-session-tab-content-messages .book-a-session-tab-content-messages-viewer .book-a-session-message').last().removeClass('book-a-session-hidden');
    
                                            // Remove previous latest message from summary tab
    
                                            $('.book-a-session-tab-content-summary .book-a-session-tab-content-summary-messages .book-a-session-message').remove();
    
                                            // Append latest message

                                            if ( data.order.current_user_is_prac ) {

                                                // For practitioners

                                                if ( value.user_id == data.order.user_id ) {
    
                                                    $('.book-a-session-tab-content-group-messages .book-a-session-tab-content-summary-messages').append( "<div class='book-a-session-card book-a-session-mini-card book-a-session-message book-a-session-message-other'>" + 
                                                    "<div class='book-a-session-message-content'>" + 
                                                    value.content + "</div>" + 
                                                    "<div class='book-a-session-message-meta'><span class='book-a-session-message-meta-name'>" + 
                                                    value.user_name + "</span>" +
                                                    "<span class='book-a-session-message-meta-time'>" + value.time_converted_formatted  + "</span>" + 
                                                    "<span class='book-a-session-message-meta-date'>" + value.date_converted_formatted  + "</span>" + 
                                                    "</div></div>" );
        
                                                } else if ( value.user_id == data.order.practitioner_id ) {
        
                                                    $('.book-a-session-tab-content-group-messages .book-a-session-tab-content-summary-messages').append( "<div class='book-a-session-card book-a-session-mini-card book-a-session-message book-a-session-message-user'>" + 
                                                    "<div class='book-a-session-message-content'>" + 
                                                    value.content + "</div>" + 
                                                    "<div class='book-a-session-message-meta'><span class='book-a-session-message-meta-name'>" + 
                                                    value.user_name + "</span>" +
                                                    "<span class='book-a-session-message-meta-time'>" + value.time_converted_formatted  + "</span>" + 
                                                    "<span class='book-a-session-message-meta-date'>" + value.date_converted_formatted  + "</span>" + 
                                                    "</div></div>" );
        
                                                }
                                    
                                            } else {

                                                // For clients

                                                if ( value.user_id == data.order.user_id ) {
    
                                                    $('.book-a-session-tab-content-group-messages .book-a-session-tab-content-summary-messages').append( "<div class='book-a-session-card book-a-session-mini-card book-a-session-message book-a-session-message-user'>" + 
                                                    "<div class='book-a-session-message-content'>" + 
                                                    value.content + "</div>" + 
                                                    "<div class='book-a-session-message-meta'><span class='book-a-session-message-meta-name'>" + 
                                                    value.user_name + "</span>" +
                                                    "<span class='book-a-session-message-meta-time'>" + value.time_converted_formatted  + "</span>" + 
                                                    "<span class='book-a-session-message-meta-date'>" + value.date_converted_formatted  + "</span>" + 
                                                    "</div></div>" );
        
                                                } else if ( value.user_id == data.order.practitioner_id ) {
        
                                                    $('.book-a-session-tab-content-group-messages .book-a-session-tab-content-summary-messages').append( "<div class='book-a-session-card book-a-session-mini-card book-a-session-message book-a-session-message-other'>" + 
                                                    "<div class='book-a-session-message-content'>" + 
                                                    value.content + "</div>" + 
                                                    "<div class='book-a-session-message-meta'><span class='book-a-session-message-meta-name'>" + 
                                                    value.user_name + "</span>" +
                                                    "<span class='book-a-session-message-meta-time'>" + value.time_converted_formatted  + "</span>" + 
                                                    "<span class='book-a-session-message-meta-date'>" + value.date_converted_formatted  + "</span>" + 
                                                    "</div></div>" );
        
                                                }
                                    

                                            }
    

                                        }
    
                                    });
                                    
                                    // Scroll the message viewer to the bottom
    
                                    $('.book-a-session-tab-content-messages-viewer').scrollTop($('.book-a-session-tab-content-messages-viewer')[0].scrollHeight);
    
                                    // Empty the textarea
    
                                    $('.book-a-session-order-viewer .book-a-session-send-message textarea').val("");

                                    setTimeout(function(){

                                        book_a_session_reveal_fixed_notice( "Sent", "success", "dismissable", "book-a-session-icon-paper-plane", 3000 );
                                    
                                    },200);

                                }).fail( function() {});
    
                            }).fail( function() {

                                // Error

                                book_a_session_reveal_fixed_notice( "Your message couldn't be sent: <i>" + $content + "</i>.", "error", "dismissable", 5000 );
                                book_a_session_hide_loader();

                            });

                        } else {

                            // Empty message. Append error message into message viewer

                            $('.book-a-session-tab-content-messages .book-a-session-send-message textarea').val("");

                            book_a_session_reveal_fixed_notice( "Your message is empty. Please type something and try again.", "error", "dismissable", 5000 );

                        }

                    });

                    checkForMessages = setInterval( function() {

                        // Every 10 seconds (for 2 minutes), check for latest messages and append them if they're new (0 is passed instead of the user id to denote that we want all messages from all users in this order)

                        $.ajax({

                            url: order_api_root + $order_id + "/messages/0",
                            method: 'GET',
                            beforeSend: function ( xhr ) {
                                xhr.setRequestHeader( 'X-WP-Nonce', api_nonce );
                            },

                        }).done( function( $messages ) { 

                            $.each( $messages, function( key, value ) {

                                // Check if the message is already in the DOM, if it isn't, append it

                                if ( $('.book-a-session-message#book-a-session-message-' + value.id ).length === 0 ) {

                                    if ( data.order.current_user_is_prac ) {

                                        // For practitioners

                                        if ( value.user_id == data.order.user_id ) {

                                            $('.book-a-session-tab-content-container .book-a-session-tab-content-messages .book-a-session-tab-content-messages-viewer').append( "<div id='book-a-session-message-" + value.id + "' class='book-a-session-animated book-a-session-hidden book-a-session-card book-a-session-mini-card book-a-session-message book-a-session-message-other'>" + 
                                            "<div class='book-a-session-message-content'>" + 
                                            value.content + "</div>" + 
                                            "<div class='book-a-session-message-meta'><span class='book-a-session-message-meta-name'>" + 
                                            value.user_name + "</span>" +
                                            "<span class='book-a-session-message-meta-time'>" + value.time_converted_formatted  + "</span>" + 
                                            "<span class='book-a-session-message-meta-date'>" + value.date_converted_formatted  + "</span>" + 
                                            "</div></div>" );
                
                                        } else if ( value.user_id == data.order.practitioner_id ) {
                
                                            $('.book-a-session-tab-content-container .book-a-session-tab-content-messages .book-a-session-tab-content-messages-viewer').append( "<div id='book-a-session-message-" + value.id + "' class='book-a-session-animated book-a-session-hidden book-a-session-card book-a-session-mini-card book-a-session-message book-a-session-message-user'>" + 
                                            "<div class='book-a-session-message-content'>" + 
                                            value.content + "</div>" + 
                                            "<div class='book-a-session-message-meta'><span class='book-a-session-message-meta-name'>" + 
                                            value.user_name + "</span>" +
                                            "<span class='book-a-session-message-meta-time'>" + value.time_converted_formatted  + "</span>" + 
                                            "<span class='book-a-session-message-meta-date'>" + value.date_converted_formatted  + "</span>" + 
                                            "</div></div>" );
                
                                        }

                                    } else {

                                        // For clients
     
                                        if ( value.user_id == data.order.user_id ) {

                                            $('.book-a-session-tab-content-container .book-a-session-tab-content-messages .book-a-session-tab-content-messages-viewer').append( "<div id='book-a-session-message-" + value.id + "' class='book-a-session-animated book-a-session-hidden book-a-session-card book-a-session-mini-card book-a-session-message book-a-session-message-user'>" + 
                                            "<div class='book-a-session-message-content'>" + 
                                            value.content + "</div>" + 
                                            "<div class='book-a-session-message-meta'><span class='book-a-session-message-meta-name'>" + 
                                            value.user_name + "</span>" +
                                            "<span class='book-a-session-message-meta-time'>" + value.time_converted_formatted  + "</span>" + 
                                            "<span class='book-a-session-message-meta-date'>" + value.date_converted_formatted  + "</span>" + 
                                            "</div></div>" );
                
                                        } else if ( value.user_id == data.order.practitioner_id ) {
                
                                            $('.book-a-session-tab-content-container .book-a-session-tab-content-messages .book-a-session-tab-content-messages-viewer').append( "<div id='book-a-session-message-" + value.id + "' class='book-a-session-animated book-a-session-hidden book-a-session-card book-a-session-mini-card book-a-session-message book-a-session-message-other'>" + 
                                            "<div class='book-a-session-message-content'>" + 
                                            value.content + "</div>" + 
                                            "<div class='book-a-session-message-meta'><span class='book-a-session-message-meta-name'>" + 
                                            value.user_name + "</span>" +
                                            "<span class='book-a-session-message-meta-time'>" + value.time_converted_formatted  + "</span>" + 
                                            "<span class='book-a-session-message-meta-date'>" + value.date_converted_formatted  + "</span>" + 
                                            "</div></div>" );
                
                                        }

                                    }

                                    // Reveal message

                                    $('.book-a-session-tab-content-messages .book-a-session-tab-content-messages-viewer .book-a-session-message').last().removeClass('book-a-session-hidden');

                                    // Scroll the message viewer to the bottom

                                    $('.book-a-session-tab-content-messages-viewer').scrollTop($('.book-a-session-tab-content-messages-viewer')[0].scrollHeight);

                                    // Remove previous latest message from summary tab

                                    $('.book-a-session-tab-content-summary .book-a-session-tab-content-summary-messages .book-a-session-message').remove();

                                    // Append latest message to message summary section

                                    if ( data.order.current_user_is_prac ) {

                                        if ( value.user_id == data.order.user_id ) {

                                            $('.book-a-session-tab-content-group-messages .book-a-session-tab-content-summary-messages').append( "<div class='book-a-session-card book-a-session-mini-card book-a-session-message book-a-session-message-other'>" + 
                                            "<div class='book-a-session-message-content'>" + 
                                            value.content + "</div>" + 
                                            "<div class='book-a-session-message-meta'><span class='book-a-session-message-meta-name'>" + 
                                            value.user_name + "</span>" +
                                            "<span class='book-a-session-message-meta-time'>" + value.time_converted_formatted  + "</span>" + 
                                            "<span class='book-a-session-message-meta-date'>" + value.date_converted_formatted  + "</span>" + 
                                            "</div></div>" );
    
                                        } else if ( value.user_id == data.order.practitioner_id ) {
    
                                            $('.book-a-session-tab-content-group-messages .book-a-session-tab-content-summary-messages').append( "<div class='book-a-session-card book-a-session-mini-card book-a-session-message book-a-session-message-user'>" + 
                                            "<div class='book-a-session-message-content'>" + 
                                            value.content + "</div>" + 
                                            "<div class='book-a-session-message-meta'><span class='book-a-session-message-meta-name'>" + 
                                            value.user_name + "</span>" +
                                            "<span class='book-a-session-message-meta-time'>" + value.time_converted_formatted  + "</span>" + 
                                            "<span class='book-a-session-message-meta-date'>" + value.date_converted_formatted  + "</span>" + 
                                            "</div></div>" );
    
                                        }

                                    } else {

                                        if ( value.user_id == data.order.user_id ) {

                                            $('.book-a-session-tab-content-group-messages .book-a-session-tab-content-summary-messages').append( "<div class='book-a-session-card book-a-session-mini-card book-a-session-message book-a-session-message-user'>" + 
                                            "<div class='book-a-session-message-content'>" + 
                                            value.content + "</div>" + 
                                            "<div class='book-a-session-message-meta'><span class='book-a-session-message-meta-name'>" + 
                                            value.user_name + "</span>" +
                                            "<span class='book-a-session-message-meta-time'>" + value.time_converted_formatted  + "</span>" + 
                                            "<span class='book-a-session-message-meta-date'>" + value.date_converted_formatted  + "</span>" + 
                                            "</div></div>" );
    
                                        } else if ( value.user_id == data.order.practitioner_id ) {
    
                                            $('.book-a-session-tab-content-group-messages .book-a-session-tab-content-summary-messages').append( "<div class='book-a-session-card book-a-session-mini-card book-a-session-message book-a-session-message-other'>" + 
                                            "<div class='book-a-session-message-content'>" + 
                                            value.content + "</div>" + 
                                            "<div class='book-a-session-message-meta'><span class='book-a-session-message-meta-name'>" + 
                                            value.user_name + "</span>" +
                                            "<span class='book-a-session-message-meta-time'>" + value.time_converted_formatted  + "</span>" + 
                                            "<span class='book-a-session-message-meta-date'>" + value.date_converted_formatted  + "</span>" + 
                                            "</div></div>" );
    
                                        }

                                    }

                                    book_a_session_reveal_fixed_notice( "You've got a new message from " + value.user_name + ".", "", "dismissable" );

                                }

                            });

                        });

                    }, 1000 * 10 );

                    // After 2 minutes, idle out and stop checking for new messages

                    setTimeout( function() {

                        clearInterval( checkForMessages );

                    }, 1000 * 60 * 2 )

                    // Tab button interactivity

                    $('.book-a-session-order-viewer .book-a-session-tab-button-container a[data-tab-content].book-a-session-tab-button').click( function() {

                        $data = $(this).attr('data-tab-content');
                        $content = $( '.book-a-session-tab-content-' + $data  ); 
            
                        // Hide all content divs at first
            
                        $( '.book-a-session-tab-content' ).addClass( 'book-a-session-hidden' );
            
                        // Reveal the content div associated with the clicked link
            
                        $content.removeClass( 'book-a-session-hidden' );
            
                        // Change the state of all links from current to a regular state, removing the CSS highlighting the link
            
                        $('.book-a-session-order-viewer .book-a-session-tab-button-container .book-a-session-tab-button').removeClass('book-a-session-tab-button-current');
                        $('a[data-tab-content]').removeClass('book-a-session-tab-button-current');
            
                        // Add the current state to the correct link
            
                        $(this).addClass('book-a-session-tab-button-current');
                        $('a[data-tab-content="' + $data + '"').addClass('book-a-session-tab-button-current');
                        
                    });

                    // Style tabs

                    $('.book-a-session-order-viewer .book-a-session-tab-button-container a[data-tab-content].book-a-session-tab-button').css({"width": "calc( 100% / " + $('.book-a-session-order-viewer .book-a-session-tab-button-container a[data-tab-content].book-a-session-tab-button').length + " )" });

                    book_a_session_reveal_modal();


                }).fail( function() {

                    // If it goes wrong show an error

                    book_a_session_hide_loader();
                    book_a_session_reveal_fixed_notice( "We couldn't load your order. Please try again.", "error", "dismissable", 5000 );

                });

            });

        }

        $('.book-a-session-modal-week-timetable .book-a-session-icon-close').on("click", function(){

            $('.book-a-session-modal-week-timetable.book-a-session-modal').addClass('book-a-session-hidden');

        });

    }

    function book_a_session_frontend() {

        if ( $('.book-a-session-frontend').length !== 0 ) {

            var subtotal_single = 0.00; 
            var subtotal_total = 0.00; 
            var location_charge_single = 0.00; 
            var location_charge_total = 0.00; 
            var discount_single = 0.00; 
            var discount_total = 0.00; 
            var total_single = 0.00; 
            var total_total = 0.00;  

            var chosen_service = 0;
            var chosen_quantity = 1;
            var chosen_location = 0;
            var chosen_practitioner = 0;
            var chosen_payment_method = 0;
            var chosen_location_name = "";

            var api_root =  book_a_session_frontend_authentication.api_root;
            var api_nonce = book_a_session_frontend_authentication.api_nonce;
            var service_price = book_a_session_frontend_authentication.calculated_service_price_array;
            var service_type = book_a_session_frontend_authentication.service_type_array;            
            var payment_method = book_a_session_frontend_authentication.payment_method_array;
            var accepted_payment_method =  book_a_session_frontend_authentication.accepted_payment_method_array;
            var payment_method_options =  book_a_session_frontend_authentication.payment_method_options_array;
            var user_id = book_a_session_frontend_authentication.user_id;
            var region_id = book_a_session_frontend_authentication.user_region_id;
            var no_region = book_a_session_frontend_authentication.no_region;
            var currency_id = book_a_session_frontend_authentication.currency_id;
            var savings_tag = book_a_session_frontend_authentication.savings_tag;
            var quantity_top = book_a_session_frontend_authentication.quantity_top;
            var quantity_bottom = book_a_session_frontend_authentication.quantity_bottom;
            var location_top = book_a_session_frontend_authentication.location_top;
            var location_bottom = book_a_session_frontend_authentication.location_bottom;
            var online_payable = payment_method_options.online_payable;

            // Remove login and register buttons in the login registration modal, and move the forgot password button outside into the modal inner rather than the form.

            $('.book-a-session-modal-login-register a.book-a-session-register').remove();
            $('.book-a-session-modal-login-register a.book-a-session-login').remove();
            $('.book-a-session-modal-login-register #book-a-session-register a.book-a-session-forgot-password').remove();

            $('.book-a-session-frontend .book-a-session-service a[data-frontend-service]').click( function() {

                book_a_session_reveal_loader();

                if ( ! user_id ) {

                    // If the user isn't logged in, show them the login / registration form. If logged in, reload the page.

                    book_a_session_reveal_login_registration_modal();

                } else {

                    // If the user is logged in, try to get the service information and create the form
    
                    $service_id = $(this).attr("data-frontend-service");
                    $region_id = $(this).attr("data-frontend-region");
                    $currency_id = $(this).attr("data-frontend-currency");
                    chosen_service = $service_id;
        
                    $.ajax({
        
                        url: api_root + "services/" + $service_id,
                        method: 'GET',
        
                    }).done( function( service ) {
        
                        subtotal_single = service_price[ $service_id ].price;
                        subtotal_total = subtotal_single * chosen_quantity;
                        total_single = ( subtotal_single + location_charge_single - discount_single );
                        total_total = total_single * chosen_quantity;
    
                        // Add card container class to remove padding and frontend modal container class for further modifications
    
                        $('.book-a-session-modal-inner:not(.book-a-session-dashboard-week-timetable-inner)').addClass('book-a-session-card-container').addClass('book-a-session-frontend-modal-container');
    
                        // Frontend modal container
    
                        $('.book-a-session-modal-inner:not(.book-a-session-dashboard-week-timetable-inner)').append('<form class="book-a-session-animated" id="book-a-session-frontend-form"><div class="book-a-session-frontend-modal"></div></form>');
    
                        // Service Image Container
    
                        $('.book-a-session-frontend-modal').append("<div class='book-a-session-service-image'></div>");
    
                        // Service Background Image
    
                        $('.book-a-session-frontend-modal .book-a-session-service-image').css({ "background-image": "url(" + service.image_full_url + ")" });
    
                        // Scrim
    
                        $('.book-a-session-frontend-modal .book-a-session-service-image').append("<div class='book-a-session-scrim'></div>");
    
                        // Service Name and Price
    
                        $('.book-a-session-frontend-modal .book-a-session-service-image .book-a-session-scrim').append("<h2 class='book-a-session-animated'>" + service.name + "</h2>").append("<h2 class='book-a-session-animated'>" + service_price[ $service_id ].price_tag_html + "</h2>");
    
                        // Tab Button Container
    
                        $('.book-a-session-frontend-modal').append("<div class='book-a-session-tab-button-container book-a-session-progress-tab-container'></div>");
    
                        // Section Container
    
                        $('.book-a-session-frontend-modal').append("<div class='book-a-session-frontend-section-container book-a-session-animated'></div>");
    
                        // Get Quantity
    
                        $.ajax({
    
                            url: api_root + "bundles",
                            method: 'GET',
    
                        }).done( function( bundles ){
    
                            // Quantity tab button
    
                            $('.book-a-session-frontend-modal .book-a-session-progress-tab-container')
                            .append( "<a href='javascript:void(0)' data-progress-tab-content='quantity' class='book-a-session-animated book-a-session-tab-button book-a-session-tab-button-current'><span class='book-a-session-icon book-a-session-icon-plus-square'></span>Quantity</a>" );
    
                            // Quantity tab content
    
                            $('.book-a-session-frontend-modal .book-a-session-frontend-section-container').append("<div class='book-a-session-frontend-section book-a-session-datetime-picker-prerequisite book-a-session-frontend-section-quantity'><p class='book-a-session-frontend-section-question'>" + quantity_top + "</p><div class='book-a-session-flex-row'></div></div>");
    
    
                            // Quantity inputs
    
                            $.each( bundles.bundles, function( key, value ) {
    
                                // Quantity container
    
                                $('.book-a-session-frontend-modal .book-a-session-frontend-section-quantity .book-a-session-flex-row').append("<div class='book-a-session-animated book-a-session-frontend-option book-a-session-frontend-quantity'>");
    
                                // Label and radio
    
                                $('.book-a-session-frontend-modal .book-a-session-frontend-quantity').last()
                                .append("<input type='radio' name='quantity' id='quantity_" + value.quantity + "' value='" + value.quantity + "'>")
                                .append("<label for='quantity_" + value.quantity + "' class='book-a-session-animated book-a-session-card'><span class='book-a-session-frontend-option-title'>" + value.quantity + "</span></label>");
    
                                // Discounted price
    
                                $('.book-a-session-frontend-modal .book-a-session-frontend-quantity').last().find("label")
                                .append("<span class='book-a-session-animated book-a-session-frontend-option-subtitle'>" + service_price[ chosen_service ].discounts[ value.quantity ].discounted_single_price_tag_html + "<span class='book-a-session-price-each'>each</span></span>")
                                .children().each( function(){ $(this).addClass('book-a-session-animated') });
    
                                if ( service_price[ $service_id ].discounts[ value.quantity ].discount > 0.00 && savings_tag > 0 ) {
    
                                    $('.book-a-session-frontend-modal .book-a-session-frontend-quantity').last().find("label")
                                    .append("<span class='book-a-session-frontend-tag'>- " + service_price[ chosen_service ].discounts[ value.quantity ].discount_total_tag_html + "</span>");
    
                                }
    
                                $('.book-a-session-frontend-modal .book-a-session-frontend-quantity').last().find("input[type='radio']").change( function() {
    
                                    chosen_quantity = value.quantity;
                                    subtotal_total = subtotal_single * chosen_quantity;
                                    location_charge_total = location_charge_single * chosen_quantity;
                                    discount_single = parseFloat(service_price[ chosen_service ].discounts[ chosen_quantity ].discount).toFixed(2);
                                    discount_total = ( discount_single * chosen_quantity ).toFixed(2);
                                    total_single = ( subtotal_single + location_charge_single - discount_single );
                                    total_total = total_single * chosen_quantity;
    
                                    $('.book-a-session-frontend-modal .book-a-session-scrim h2').addClass('book-a-session-hidden');
    
                                    setTimeout(function(){ 
    
                                        var session_string = value.quantity > 1 ? " sessions" : " session";
    
                                        $('.book-a-session-frontend-modal .book-a-session-scrim h2').first().html( service.name + " - " + value.quantity + session_string ).removeClass('book-a-session-hidden');
                                        $('.book-a-session-frontend-modal .book-a-session-scrim h2').last().find('.book-a-session-price-amount').html( total_total.toFixed(2) );
                                        $('.book-a-session-frontend-modal .book-a-session-scrim h2').last().removeClass('book-a-session-hidden');
    
                                    }, 200);
    
                                });
    
                            });
    
                            // Set quantity option widths
    
                            var quantity_count = $('.book-a-session-frontend-modal .book-a-session-frontend-quantity').length;
                            var quantity_separator = quantity_count-1;
    
                            $('.book-a-session-frontend-modal .book-a-session-frontend-quantity').css({ "width": "calc( ( ( 100% - ( 17.5px * " + quantity_separator + " ) ) /  " + quantity_count + " ) )" });
                            $('.book-a-session-frontend-quantity:nth-child(' + quantity_count + 'n+' + quantity_count + ')').css({ "margin-right": 0 });
    
                            // Bottom bar
    
                            $('.book-a-session-frontend-modal .book-a-session-frontend-section-quantity').append("<div class='book-a-session-frontend-bottom-bar'></div>");
    
                            $('.book-a-session-frontend-modal .book-a-session-frontend-section-quantity .book-a-session-frontend-bottom-bar')
                            .append("<p>" + quantity_bottom + "</p>")
                            .append("<a href='javascript:void(0)' class='book-a-session-animated book-a-session-button book-a-session-circle-button'><span class='book-a-session-icon book-a-session-icon-chevron-right'></span>Next</a>");
    
                            // Section container height to Quantity height
    
                            $('.book-a-session-frontend-modal .book-a-session-frontend-section-container').css({ "height":  $('.book-a-session-frontend-modal .book-a-session-frontend-section-quantity').outerHeight() + "px" })
    
                            // Get Locations
    
                            var location_array = book_a_session_frontend_authentication.available_location_array;
    
                            if ( location_array.length !== 0 ) {
    
                                // Location tab button
    
                                $('.book-a-session-frontend-modal .book-a-session-progress-tab-container')
                                .append( "<a href='javascript:void(0)' data-progress-tab-content='location' class='book-a-session-animated book-a-session-tab-button book-a-session-tab-button-inactive'><span class='book-a-session-icon book-a-session-icon-map-marker'></span>Location</a>" );
    
                                // Location tab content
    
                                $('.book-a-session-frontend-modal .book-a-session-frontend-section-container').append("<div class='book-a-session-hidden book-a-session-datetime-picker-prerequisite book-a-session-frontend-section book-a-session-frontend-section-location'></div>");
    
                                $('.book-a-session-frontend-modal .book-a-session-frontend-section-location').append("<p class='book-a-session-frontend-section-question'>" + location_top + "</p>");
    
                                // Location wrapper
    
                                $('.book-a-session-frontend-modal .book-a-session-frontend-section-location').append("<div class='book-a-session-flex-row'></div>");
    
                                $.each( location_array[ $service_id ], function( key, value ) {
    
                                    // Loctaion container
        
                                    $('.book-a-session-frontend-modal .book-a-session-frontend-section-location .book-a-session-flex-row').append("<div class='book-a-session-animated book-a-session-frontend-option book-a-session-frontend-location'>");
        
                                    // Label and radio
        
                                    $('.book-a-session-frontend-modal .book-a-session-frontend-location').last()
                                    .append("<input type='radio' name='location' id='location_" + value.id + "' value='" + value.id + "'>")
                                    .append("<label for='location_" + value.id + "' class='book-a-session-animated book-a-session-card'><span class='book-a-session-frontend-option-title'>" + value.name + "</span></label>");
    
                                    var location_charge_string;
    
                                    if ( value.location_charge > 0.00 ) { location_charge_string = "<span class='book-a-session-price-plus'>+</span>" + value.location_charge_html; } else { location_charge_string = "Free"; }
                                    $('.book-a-session-frontend-modal .book-a-session-frontend-location').last().find('label').append("<span class='book-a-session-frontend-option-subtitle'>" + location_charge_string + "</span>");
    
                                    if ( value.tag ) { $('.book-a-session-frontend-modal .book-a-session-frontend-location').last().find('label').append("<span class='book-a-session-frontend-tag'>" + value.tag + "</span>"); }
    
                                    
                                    if ( value.public_address == 1 ) {
    
                                        if ( value.address_line_1 || value.address_line_2 || value.address_line_3 || value.city || value.country || value.postcode_zipcode ) { 
    
                                            $('.book-a-session-frontend-modal .book-a-session-frontend-location').last().find('label').addClass('book-a-session-container-tooltip')
                                            .append("<span class='book-a-session-animated book-a-session-label-tooltip'></span>");
                                            
                                            $('.book-a-session-frontend-modal .book-a-session-frontend-location').last().find('.book-a-session-label-tooltip')
                                            .append("<header>Address</header><ul></ul>");
    
                                            if ( value.address_line_1 ) { $('.book-a-session-frontend-modal .book-a-session-frontend-location').last().find('.book-a-session-label-tooltip ul').append("<li>" + value.address_line_1 + "</li>"); }
                                            if ( value.address_line_2 ) { $('.book-a-session-frontend-modal .book-a-session-frontend-location').last().find('.book-a-session-label-tooltip ul').append("<li>" + value.address_line_2 + "</li>"); }
                                            if ( value.address_line_3 ) { $('.book-a-session-frontend-modal .book-a-session-frontend-location').last().find('.book-a-session-label-tooltip ul').append("<li>" + value.address_line_3 + "</li>"); }
                                            if ( value.city ) { $('.book-a-session-frontend-modal .book-a-session-frontend-location').last().find('.book-a-session-label-tooltip ul').append("<li>" + value.city + "</li>"); }
                                            if ( value.country ) { $('.book-a-session-frontend-modal .book-a-session-frontend-location').last().find('.book-a-session-label-tooltip ul').append("<li>" + value.country + "</li>"); }
                                            if ( value.postcode_zipcode ) { $('.book-a-session-frontend-modal .book-a-session-frontend-location').last().find('.book-a-session-label-tooltip ul').append("<li>" + value.postcode_zipcode + "</li>"); }
    
                                        }
    
                                    } else if ( value.address_line_1 || value.address_line_2 || value.address_line_3 || value.city || value.country || value.postcode_zipcode ) {

                                        $('.book-a-session-frontend-modal .book-a-session-frontend-location').last().find('label').addClass('book-a-session-container-tooltip')
                                        .append("<span class='book-a-session-animated book-a-session-label-tooltip'></span>");

                                        if ( value.country ) var address_preview = value.country;
                                        if ( value.city ) var address_preview = value.city;
                                         
                                        if ( address_preview ) {

                                            $('.book-a-session-frontend-modal .book-a-session-frontend-location').last().find('.book-a-session-label-tooltip')
                                            .append("<header>Address</header><ul><li>" + address_preview + "...</li><li>We'll show you the full address once you've placed your order.</li></ul>");

                                        } else {

                                            $('.book-a-session-frontend-modal .book-a-session-frontend-location').last().find('.book-a-session-label-tooltip')
                                            .append("<header>Address</header><ul><li>You'll get the full address once you've placed your order.</li></ul>");

                                        }

                                    }
                                    
                                    $('.book-a-session-frontend-modal .book-a-session-frontend-location').last().find("input[type='radio']").change( function() {
    
                                        chosen_location = value.id;
                                        chosen_location_name = value.name;
                                        subtotal_total = subtotal_single * chosen_quantity;
                                        location_charge_single = parseFloat(value.location_charge);
                                        location_charge_total = location_charge_single * chosen_quantity;
                                        total_single = ( subtotal_single + location_charge_single - discount_single );
                                        total_total = total_single * chosen_quantity;
    
                                        $('.book-a-session-frontend-modal .book-a-session-scrim h2').last().addClass('book-a-session-hidden');
        
                                        setTimeout(function(){ 
        
                                            $('.book-a-session-frontend-modal .book-a-session-scrim h2').last().find('.book-a-session-price-amount').html( total_total.toFixed(2) );
                                            $('.book-a-session-frontend-modal .book-a-session-scrim h2').last().removeClass('book-a-session-hidden');
        
                                        }, 200);
        
                                    });
        
                                });
    
                                // Set location option widths
    
                                var location_count = $('.book-a-session-frontend-modal .book-a-session-frontend-location').length;
                                var location_separator = location_count-1;
                                $('.book-a-session-frontend-modal .book-a-session-frontend-location').css({ "width": "calc( ( ( 100% - ( 17.5px * " + location_separator + " ) ) /  " + location_count + " ) )" });
                                $('.book-a-session-frontend-location:nth-child(' + location_count + 'n+' + location_count + ')').css({ "margin-right": 0 });
    
                                // Bottom bar
    
                                $('.book-a-session-frontend-modal .book-a-session-frontend-section-location').append("<div class='book-a-session-frontend-bottom-bar'></div>");
    
                                $('.book-a-session-frontend-modal .book-a-session-frontend-section-location .book-a-session-frontend-bottom-bar')
                                .append("<p>" + location_bottom + "</p>")
                                .append("<a href='javascript:void(0)' class='book-a-session-animated book-a-session-button book-a-session-circle-button'><span class='book-a-session-icon book-a-session-icon-chevron-right'></span>Next</a>");
    
    
                            }
    
                            // Get Practitioners
    
                            var practitioner_array = book_a_session_frontend_authentication.practitioner_array;
                            var practitioner_top = book_a_session_frontend_authentication.practitioner_top;
                            var practitioner_bottom = book_a_session_frontend_authentication.practitioner_bottom;
                            var practitioner_title_option = book_a_session_frontend_authentication.practitioner_title;
    
                            var practitioner_title = "Practitioner";
    
                            if ( practitioner_title_option !== 0 ) {
    
                                $.each( service_type, function( key, value ) {
    
                                    if ( value.id == service.service_type_id ) {
    
                                        practitioner_title = service_type[ key ].practitioner_title;
                                        practitioner_top = practitioner_top.replace(/practitioner/gi, service_type[ key ].practitioner_title.toLowerCase() );
                                        practitioner_bottom = practitioner_bottom.replace(/practitioner/gi, service_type[ key ].practitioner_title.toLowerCase() );
    
                                    }
    
                                });
    
                            }
    
                            if ( practitioner_array.length > 1 ) {
    
                                // Practitioner tab button
    
                                $('.book-a-session-frontend-modal .book-a-session-progress-tab-container')
                                .append( "<a href='javascript:void(0)' data-progress-tab-content='practitioner' class='book-a-session-animated book-a-session-tab-button book-a-session-tab-button-inactive'><span class='book-a-session-icon book-a-session-icon-person'></span>" + practitioner_title + "</a>" );
    
                                // Practitioner tab content
    
                                $('.book-a-session-frontend-modal .book-a-session-frontend-section-container').append("<div class='book-a-session-hidden book-a-session-datetime-picker-prerequisite book-a-session-frontend-section book-a-session-frontend-section-practitioner'></div>");
    
                                $('.book-a-session-frontend-modal .book-a-session-frontend-section-practitioner').append("<p class='book-a-session-frontend-section-question'>" + practitioner_top + "</p>");
    
                                // Practitioner wrapper
    
                                $('.book-a-session-frontend-modal .book-a-session-frontend-section-practitioner').append("<div class='book-a-session-flex-row'></div>");
    
                                $.each( practitioner_array, function( key, value ) {
    
                                    // Practitioner container
        
                                    $('.book-a-session-frontend-modal .book-a-session-frontend-section-practitioner .book-a-session-flex-row').append("<div class='book-a-session-animated book-a-session-frontend-option book-a-session-frontend-practitioner'>");
        
                                    // Label and radio
        
                                    $('.book-a-session-frontend-modal .book-a-session-frontend-practitioner').last()
                                    .append("<input type='radio' name='practitioner' id='practitioner_" + value.ID + "' value='" + value.ID + "'>")
                                    .append("<label for='practitioner_" + value.ID + "' class='book-a-session-animated book-a-session-card'><span class='book-a-session-frontend-option-title'>" + value.data.display_name + "</span></label>");
    
                                    $('.book-a-session-frontend-modal .book-a-session-frontend-practitioner').last().find('label').append("<span class='book-a-session-frontend-option-subtitle'>" + practitioner_title + "</span>");
    
                                    $('.book-a-session-frontend-modal .book-a-session-frontend-practitioner').last().find("input[type='radio']").change( function() {
    
                                        chosen_practitioner = value.ID;
        
                                    });
            
                                });
    
                                // Set practitioner option widths
    
                                var practitioner_count = $('.book-a-session-frontend-modal .book-a-session-frontend-practitioner').length;
                                var practitioner_separator = practitioner_count-1;
    
                                $('.book-a-session-frontend-modal .book-a-session-frontend-practitioner').css({ "width": "calc( ( ( 100% - ( 17.5px * " + practitioner_separator + " ) ) /  " + practitioner_count + " ) )" });
                                $('.book-a-session-frontend-practitioner:nth-child(' + practitioner_count + 'n+' + practitioner_count + ')').css({ "margin-right": 0 });
    
                                // Bottom bar
    
                                $('.book-a-session-frontend-modal .book-a-session-frontend-section-practitioner').append("<div class='book-a-session-frontend-bottom-bar'></div>");
    
                                $('.book-a-session-frontend-modal .book-a-session-frontend-section-practitioner .book-a-session-frontend-bottom-bar')
                                .append("<p>" + practitioner_bottom + "</p>")
                                .append("<a href='javascript:void(0)' class='book-a-session-animated book-a-session-button book-a-session-circle-button book-a-session-load-datetime-picker'><span class='book-a-session-icon book-a-session-icon-chevron-right'></span>Next</a>");
    
                            } else {
    
                                chosen_practitioner = practitioner_array[0].ID;
    
                                $('.book-a-session-frontend-section-location .book-a-session-frontend-bottom-bar .book-a-session-circle-button').addClass('book-a-session-load-datetime-picker');
    
                            }
    
                            //  Datetime picker tab button
    
                            $('.book-a-session-frontend-modal .book-a-session-progress-tab-container')
                            .append( "<a href='javascript:void(0)' data-progress-tab-content='datetime' class='book-a-session-animated book-a-session-tab-button book-a-session-tab-button-inactive book-a-session-load-datetime-picker'><span class='book-a-session-icon book-a-session-icon-calendar'></span>Date & Time</a>" );
    
                            // Datetime picker section content
    
                            $('.book-a-session-frontend-modal .book-a-session-frontend-section-container').append("<div class='book-a-session-hidden book-a-session-frontend-section book-a-session-frontend-section-datetime'><div class='book-a-session-frontend-bottom-bar'><p class='book-a-session-animated'></p><a href='javascript:void(0)' class='book-a-session-animated book-a-session-button book-a-session-circle-button'><span class='book-a-session-icon book-a-session-icon-chevron-right'></span>Next</a></div></div>");
    
                            // Payment Tab
    
                            $('.book-a-session-frontend-modal .book-a-session-progress-tab-container')
                            .append( "<a href='javascript:void(0)' data-progress-tab-content='payment' class='book-a-session-animated book-a-session-tab-button book-a-session-tab-button-inactive'><span class='book-a-session-icon book-a-session-icon-shopping-basket'></span>Checkout</a>" );
    
                            // Payment Content
    
                            var payment_method_top = book_a_session_frontend_authentication.payment_method_top;
                            var payment_method_bottom = book_a_session_frontend_authentication.payment_method_bottom;
    
                            $('.book-a-session-frontend-modal .book-a-session-frontend-section-container').append("<div class='book-a-session-hidden book-a-session-frontend-section book-a-session-frontend-section-payment'><p class='book-a-session-frontend-section-question'>" + payment_method_top + "</p><div class='book-a-session-flex-row'></div><div class='book-a-session-frontend-bottom-bar'><p>" + payment_method_bottom + "</p><a href='javascript:void(0)' class='book-a-session-animated book-a-session-button book-a-session-circle-button'>Next</a></div>")
    
                            // Get possible payment methods according to bundle and location chosen
    
                            var currency = book_a_session_frontend_authentication.currency;
    
                            // Initialise payment method data requirements object for checking whether or not the user picked an option that might require us to load a different set of payment methods. The values all start at 0 because we definitely want it to load the first time.
    
                            var payment_method_requirements = { 
    
                                quantity: 0,
                                location: 0,
    
                             }
    
                            function book_a_session_update_payment_methods( session_values ) {
    
                                // If any of the requirements have changed since last time, go ahead
    
                                if ( payment_method_requirements.quantity != chosen_quantity || payment_method_requirements.location != chosen_location ) {
    
                                    // Update the payment method requirements object
    
                                    payment_method_requirements.quantity = chosen_quantity;
                                    payment_method_requirements.location = chosen_location;
    
                                    // Remove old payment methods
    
                                    $('.book-a-session-frontend-section-payment .book-a-session-frontend-payment-method').remove();
                                    $('.book-a-session-frontend-section-checkout-payment-method').remove();
    
                                    // Loop through all payment methods known
            
                                    $.each( payment_method, function( payment_method_key, payment_method_value ){
    
                                        // Check if this payment method is currently enabled for use in the frontend
    
                                        var payment_method_accepted = accepted_payment_method[ "accept" + payment_method_value.id ];
    
                                        if ( payment_method_accepted == 1 ) {
    
                                            // And for each, also loop through bundle payment methods to determine if we find a match
                
                                            $.each( bundles.bundle_payment_methods, function( bundle_payment_method_key, bundle_payment_method_value ){
    
                                                // If we do find a match, also check to see if the matched values can be found in the location array to determine if this payment method is accepted for both the selected quantity and location
    
                                                if ( bundle_payment_method_value.quantity == chosen_quantity && bundle_payment_method_value.payment_method_id == payment_method_value.id ) {
    
                                                    $.each( location_array[ chosen_service ], function( location_key, location_value ) {
    
                                                        if ( location_value.id == chosen_location ) {
    
                                                            // Now comb through the location's accepted payment methods.
    
                                                            $.each( location_value.payment_method, function( location_payment_method_key, location_payment_method_value ){
    
                                                                if ( location_payment_method_value.payment_method_id == payment_method_value.id ) {
    
                                                                    // We found a match! Whew these loops are a tad too deep for my liking. The payment_method_value.id that turns up here is accepted by the system, the chosen quantity and the chosen location.
    
                                                                    $('.book-a-session-frontend-section-payment .book-a-session-flex-row').append("<div class='book-a-session-animated book-a-session-frontend-option book-a-session-frontend-payment-method'><input type='radio' name='payment_method' id='payment_method_" + payment_method_value.id + "' value='" + payment_method_value.id + "'><label for='payment_method_" + payment_method_value.id + "' class='book-a-session-animated book-a-session-card'><span class='book-a-session-frontend-option-title'></span><span class='book-a-session-frontend-option-subtitle'></span></label></div>");
    
                                                                    var payment_method_icon = payment_method_options[ "frontend_icon_" + payment_method_value.id ];
                                                                    var payment_method_title = payment_method_options[ "frontend_title_" + payment_method_value.id ];
                                                                    var payment_method_subtitle = payment_method_options[ "frontend_subtitle_" + payment_method_value.id ];
                                                                    var payment_method_tag = payment_method_options[ "frontend_tag_" + payment_method_value.id ];
                                                                    var payment_method_single_top = payment_method_options[ "frontend_top_" + payment_method_value.id ];
                                                                    var payment_method_single_bottom = payment_method_options[ "frontend_bottom_" + payment_method_value.id ];
    
                                                                    // Prepare icon if enabled in the admin area
    
                                                                    if ( payment_method_icon ) {
    
                                                                        if ( payment_method_value.id == 1 ) {
    
                                                                            // PayPal
    
                                                                            var payment_method_icon_html = "<div class='book-a-session-icon-container'><span class='book-a-session-icon book-a-session-icon-paypal'></span></div>";
    
                                                                        } else if ( payment_method_value.id == 2 ) {
    
                                                                            // Cash
    
                                                                            var payment_method_icon_html = "<div class='book-a-session-icon-container'><span class='book-a-session-icon book-a-session-icon-banknote'></span></div>";
    
                                                                        } else if ( payment_method_value.id == 3 ) {
    
                                                                            // Vodafone Cash
    
                                                                            var payment_method_icon_html = "<div class='book-a-session-icon-container'><span class='book-a-session-icon book-a-session-icon-vodafone-cash-combined-logo-alternative'></span></div>";
    
                                                                        } else if ( payment_method_value.id == 4 ) {
    
                                                                            // Credit / Debit Card (Stripe)
    
                                                                            var payment_method_icon_html = "<div class='book-a-session-icon-container'><div class='book-a-session-icon-container'><span class='book-a-session-icon book-a-session-icon-cc-visa'></span><span class='book-a-session-icon book-a-session-icon-cc-mastercard'></span></div><div class='book-a-session-icon-container'><span class='book-a-session-icon book-a-session-icon-cc-amex'></span><span class='book-a-session-icon book-a-session-icon-cc-stripe'></span></div></div>";
    
                                                                        }
    
                                                                        $('.book-a-session-frontend-section-payment .book-a-session-flex-row .book-a-session-frontend-payment-method').last().find(".book-a-session-frontend-option-title").append( payment_method_icon_html );
    
                                                                    }
    
                                                                    // Prepare text if each type of text is enabled in the admin area
    
                                                                    if ( payment_method_title ) {
    
                                                                        $('.book-a-session-frontend-section-payment .book-a-session-flex-row .book-a-session-frontend-payment-method').last().find(".book-a-session-frontend-option-title").append( payment_method_title );
    
                                                                    }
                                                                    if ( payment_method_subtitle ) {
    
                                                                        $('.book-a-session-frontend-section-payment .book-a-session-flex-row .book-a-session-frontend-payment-method').last().find(".book-a-session-frontend-option-subtitle").append( payment_method_subtitle );
    
                                                                    }
                                                                    if ( payment_method_tag ) {
    
                                                                        $('.book-a-session-frontend-section-payment .book-a-session-flex-row .book-a-session-frontend-payment-method label').last().append( '<span class="book-a-session-frontend-tag">' + payment_method_tag + '</span>' );
    
                                                                    }
    
                                                                    // Create price breakdown table
    
                                                                    var price_tag_html_derived = $('.book-a-session-frontend-modal .book-a-session-service-image .book-a-session-scrim h2').last().html();
                                                                    
                                                                    // Quantity
                                                                    var checkout_price_table = '<div class="book-a-session-card book-a-session-mini-card book-a-session-price-table"><span class="book-a-session-flex-table-key book-a-session-quantity">Quantity</span><span class="book-a-session-flex-table-value book-a-session-quantity">' + 
                                                                    chosen_quantity + '</span>';
    
                                                                    // Base Price
                                                                    var checkout_price_table = '<div class="book-a-session-card book-a-session-mini-card book-a-session-price-table"><span class="book-a-session-flex-table-key book-a-session-base-price">Subtotal</span><span class="book-a-session-flex-table-value book-a-session-base-price">' + 
                                                                    subtotal_total + '</span>';
                                                                                                                                
                                                                    // Location Charge
                                                                    checkout_price_table += '<span class="book-a-session-flex-table-key book-a-session-location-charge">Location</span><span class="book-a-session-flex-table-value book-a-session-location-charge">' + 
                                                                    location_charge_total + '</span>';
                                                                    
                                                                    // Discount 
                                                                    checkout_price_table += '<span class="book-a-session-flex-table-key book-a-session-discount">Discount</span><span class="book-a-session-flex-table-value book-a-session-discount">-' + 
                                                                    discount_total + '</span>';
                                                                    
                                                                    // Total 
                                                                    checkout_price_table += '<span class="book-a-session-flex-table-key book-a-session-flex-table-large book-a-session-grand-total">Total</span><span class="book-a-session-flex-table-value book-a-session-flex-table-large book-a-session-grand-total">' + 
                                                                    total_total + '</span></div>';
    
                                                                    if ( payment_method_value.id == 1 ) {
    
                                                                        // PayPal
    
                                                                        var sandbox_live = payment_method_options[ "sandbox_live_" + payment_method_value.id ];
                                                                        var sandbox_id = payment_method_options[ "sandbox_id_" + payment_method_value.id ];
                                                                        var live_id = payment_method_options[ "live_id_" + payment_method_value.id ];
                                                                        var checkout_html = "<div class='book-a-session-animated book-a-session-hidden book-a-session-frontend-section book-a-session-frontend-section-checkout book-a-session-frontend-section-checkout-payment-method book-a-session-frontend-section-checkout-paypal book-a-session-frontend-section-payment-method-1'><p class='book-a-session-frontend-section-question'>" + payment_method_single_top + "</p><div class='book-a-session-flex-row'>" + checkout_price_table + "<div id='paypal-button-container' class='book-a-session-payment-button-container'></div></div></div>";
    
                                                                        $('.book-a-session-frontend-section-container').append( checkout_html );
    
                                                                        if ( $('#paypal-button-container').length != 0 ) {
    
                                                                            paypal.Button.render({
                                                                            
                                                                                // Set your environment
                                                                        
                                                                                env: sandbox_live, // sandbox | production
                                                                        
                                                                                // Specify the style of the button
                                                                        
                                                                                style: {
                                                                                    layout: 'vertical', // horizontal | vertical
                                                                                    label:  'checkout',  // checkout | credit | pay | buynow | generic
                                                                                    size:   'responsive', // small | medium | large | responsive
                                                                                    shape:  'rect',   // pill | rect
                                                                                    color:  'gold'   // gold | blue | silver | black
                                                                                },
                                                                        
                                                                                // PayPal Client IDs - replace with your own
                                                                                // Create a PayPal app: https://developer.paypal.com/developer/applications/create
                                                                        
                                                                                client: {
                                                                                    sandbox:    sandbox_id,
                                                                                    production: live_id
                                                                                },
                                                                        
                                                                                // Wait for the PayPal button to be clicked
                                                                        
                                                                                payment: function(data, actions) {
                                                                                    return actions.payment.create({
                                                                                        payment: {
                                                                                            transactions: [
                                                                                                {
                                                                                                    amount: { 
                                                                                                        total: total_total, 
                                                                                                        currency: currency[ "code" ],
                                                                                                    },
                                                                                                    description: chosen_quantity > 1 ? chosen_quantity + " sessions of " + service.name + " - " + chosen_location_name + "." : service.name + " - " + chosen_location_name + ".",
                                                                                                    item_list: {
                                                                                                        items: [
                                                                                                            {
                                                                                                                name: service.name,
                                                                                                                description: service.name + " - " + chosen_location_name,
                                                                                                                quantity: chosen_quantity.toString(),
                                                                                                                price: total_single.toString(),
                                                                                                                sku: ' SERVICE-ID-' + chosen_service,
                                                                                                                currency: currency[ "code" ],
                                                                                                            },
                                                                                                        ]
                                                                                                    }
    
                                                                                                }
                                                                                            ],
                                                                                            note_to_payer: 'You can message your ' + practitioner_title.toLowerCase() + ' for any questions about your order once it has been placed.'
                                                                                        }
                                                                                    });
                                                                                },
                                                                        
                                                                                // Wait for the payment to be authorized by the customer
                                                                        
                                                                                onAuthorize: function(data, actions) {
    
                                                                                    return actions.payment.execute().then(function() {
    
                                                                                        // Prepare payment information
    
                                                                                        var intent = data.intent ? data.intent : "N/A";
                                                                                        var paypal_orderID = data.orderID ? data.orderID : "N/A";
                                                                                        var payerID = data.payerID ? data.payerID : "N/A";
                                                                                        var paymentID = data.paymentID ? data.paymentID : "N/A";
                                                                                        var paymentToken = data.paymentToken ? data.paymentToken : "N/A";
    
                                                                                        var note_json = {
    
                                                                                            intent: intent,
                                                                                            orderID: paypal_orderID,
                                                                                            payerID: payerID,
                                                                                            paymentID: paymentID,
                                                                                            paymentToken: paymentToken,
    
                                                                                        }
    
                                                                                        // Prepare session times and date values
    
                                                                                        var session_date_array = [];
                                                                                        var session_time_array = [];
    
                                                                                        if ( chosen_quantity > 1 && session_values && $('[data-session-tab]').length == chosen_quantity ) {
    
                                                                                            // For multiple sessions, get the session time and dates stored in the session values object and push them to their arrays
    
                                                                                            $('[data-session-tab]').each(function(){
    
                                                                                                var session_tab_number = $(this).attr("data-session-tab");
    
                                                                                                session_date_array.push( session_values[ session_tab_number ].date );
                                                                                                session_time_array.push( session_values[ session_tab_number ].schedule );
    
                                                                                            });
    
                                                                                        } else {
    
                                                                                            // For just a single session, get the values of checked radios and push them as single entries in their arrays
    
                                                                                            session_date_array.push( $('input[name="date"]:checked').attr("data-frontend-date") );
                                                                                            session_time_array.push( $('input[name="schedule_id"]:checked').attr("value") );
    
                                                                                        }
    
                                                                                        // Create an order via WP REST API
    
                                                                                        $.ajax({
    
                                                                                            url: api_root + "orders/",
                                                                                            method: 'POST',
                                                                                            data: {
    
                                                                                                "session_project":   "session",
                                                                                                "session_time":      session_time_array,
                                                                                                "session_date":      session_date_array,
                                                                                                "quantity":          chosen_quantity,
                                                                                                "service_id":        chosen_service,
                                                                                                "region_id":         region_id,
                                                                                                "practitioner_id":   chosen_practitioner,
                                                                                                "user_id":           user_id,
                                                                                                "location_id":       chosen_location,
                                                                                                "payment_method_id": payment_method_value.id,
                                                                                                "currency_id":       currency_id,
                                                                                                "base_price":        subtotal_total,
                                                                                                "location_charge":   location_charge_total,
                                                                                                "discount":          discount_total,
                                                                                                "total":             total_total,
                                                                                                "amount_paid":       0.00,
                                                                                                "total_due":         total_total,
                                                                                                "booking_status":    "Pending",
                                                                                                "payment_status":    "Online payment due",
                                                                                                "vfc_mobile":        null,
                                                                                                "note":              null,
                                                                                                
                                                                                            },
                                                                                            beforeSend: function ( xhr ) {
                                                                                                xhr.setRequestHeader( 'X-WP-Nonce', api_nonce );
                                                                                            },
                                                                
                                                                                        }).done( function( create_order_success ) {
    
                                                                                            // Order created
    
                                                                                            book_a_session_reveal_fixed_notice( "Thanks! Your payment was successful and your order has been placed. Your payment ID is " + paymentID + ".", "success", "dismissable" );
    
                                                                                            // Now record payment
    
                                                                                            $.ajax({
    
                                                                                                url: api_root + "orders/" + create_order_success.order_id + "/payment/",
                                                                                                method: 'POST',
                                                                                                data: {
    
                                                                                                    "payment_method_id": payment_method_value.id,
                                                                                                    "invoice_id": create_order_success.invoice_id,
                                                                                                    "payment_id": data.paymentID,
                                                                                                    "total": total_total,
                                                                                                    "success": 1,
                                                                                                    "note": JSON.stringify( note_json ),
                                                                                                    
                                                                                                },
                                                                                                beforeSend: function ( xhr ) {
    
                                                                                                    xhr.setRequestHeader( 'X-WP-Nonce', api_nonce );
    
                                                                                                },
                                                                    
                                                                                            }).done( function( record_payment_success ) {
    
                                                                                                // Payment recorded
    
                                                                                                // PayPal thank you page
    
                                                                                                book_a_session_hide_and_clear_modal();
    
                                                                                                setTimeout(function(){
                                                                                                    
                                                                                                    // Thanks page container
    
                                                                                                    $('.book-a-session-modal .book-a-session-modal-inner:not(.book-a-session-dashboard-week-timetable-inner)').append('<div class="book-a-session-animated book-a-session-thanks-page"></div').addClass('book-a-session-modal-thanks');
    
                                                                                                    // Thanks page text
    
                                                                                                    var thanks_page_subtitle_session = chosen_quantity > 1 ? "sessions" : "session";
                                                                                                    var thanks_page_subtitle_has = chosen_quantity > 1 ? "have" : "has";
    
                                                                                                    $('.book-a-session-thanks-page').append('<h1 class="book-a-session-thanks-title">Thanks!</h1>')
                                                                                                    .append('<h3 class="book-a-session-thanks-subtitle">Your ' + thanks_page_subtitle_session + ' ' + thanks_page_subtitle_has + ' been booked.</h3>')
                                                                                                    .append('<p>Keep an eye out for an email we\'ll send you containing all the instructions and directions you might need to get to your session. You can also find everything you need in your dashboard. See you soon!</p>');
    
                                                                                                    // Thanks page session table
    
                                                                                                    $('.book-a-session-thanks-page').append("<table class='book-a-session-table'><thead><th class='book-a-session-table-session'>Session</th><th class='book-a-session-table-date'>Date</th><th class='book-a-session-table-time'>Time</th></thead><tbody></tbody></table>");
    
                                                                                                    // Loop through sessions and append dates and times to table
    
                                                                                                    $.each( create_order_success.order.sessions, function( session_key, session_value ){
    
                                                                                                        // Table Row
    
                                                                                                        $('.book-a-session-thanks-page .book-a-session-table tbody').append("<tr></tr>");
    
                                                                                                        // Session number
    
                                                                                                        $('.book-a-session-thanks-page .book-a-session-table tbody tr').last()
                                                                                                        .append("<td class='book-a-session-table-session'>" + ( session_key + 1 ) + "</td>");
    
                                                                                                        // Date
    
                                                                                                        $('.book-a-session-thanks-page .book-a-session-table tbody tr').last()
                                                                                                        .append("<td class='book-a-session-table-date'>" + session_value.session_time_converted_array.start_date_formatted + "</td>");
    
                                                                                                        // Time
    
                                                                                                        $('.book-a-session-thanks-page .book-a-session-table tbody tr').last()
                                                                                                        .append("<td class='book-a-session-table-time'>" + session_value.session_time_converted_array.time_string + "</td>");
    
                                                                                                    });
    
                                                                                                    var dashboard_url = book_a_session_frontend_authentication.dashboard_url;
    
                                                                                                    if ( dashboard_url ) {
    
                                                                                                        // Dashboard link
    
                                                                                                        $('.book-a-session-thanks-page').append('<a href="' + dashboard_url + '" class="book-a-session-button-icon-right book-a-session-button-icon book-a-session-animated book-a-session-button book-a-session-high-button">Dashboard<span class="book-a-session-icon book-a-session-icon-dashboard"></span></a>');
    
                                                                                                    }
    
                                                                                                    book_a_session_reveal_modal();
    
                                                                                                    book_a_session_hide_loader();
    
                                                                                                },200);
                                                                                
                                                                                            }).fail( function( record_payment_fail ){
    
                                                                                                // Payment not recorded
    
                                                                                                book_a_session_reveal_fixed_notice( "Thanks! Your payment was successful, but your order was not updated to reflect it. Your PayPal payment ID is " + data.paymentID + ". Keep note of this and contact your " + data.order.practitioner_title.toLowerCase() + ".", "warning", "dismissable" );
                                                                                                
                                                                                            });
    
                                                                                        }).fail( function( create_order_fail ) {
    
                                                                                            // Order not created
    
                                                                                            book_a_session_reveal_fixed_notice( "Thanks! Your payment was successful, but your order could not be created. Your PayPal payment ID is " + data.paymentID + ". Keep note of this and contact us with this message.", "warning", "dismissable" );
    
                                                                                        });
    
    
                                                                                    });
    
                                                                                }
                                                                            
                                                                            }, '#paypal-button-container');
                                                                    
                                                                        }    
    
                                                                    } else if ( payment_method_value.id == 2 ) {
    
                                                                        // Cash
    
                                                                        var checkout_html = "<div class='book-a-session-animated book-a-session-hidden book-a-session-frontend-section book-a-session-frontend-section-checkout book-a-session-frontend-section-checkout-payment-method book-a-session-frontend-section-checkout-cash book-a-session-frontend-section-payment-method-2'><p class='book-a-session-frontend-section-question'>" + payment_method_single_top + "</p><div class='book-a-session-flex-row'>" + checkout_price_table + "<div id='cash-button-container' class='book-a-session-payment-button-container'></div></div></div>";
    
                                                                        $('.book-a-session-frontend-section-container').append( checkout_html );
    
                                                                        // Place order button
    
                                                                        $('#cash-button-container').append( "<a href='javascript:void(0)' class='book-a-session-animated book-a-session-button book-a-session-big-button book-a-session-high-button'><div class='book-a-session-price-tag'><span class='book-a-session-price-text'>Book now & pay in person</span></div><span class='book-a-session-icon book-a-session-icon-banknote'></span></a>" );
    
                                                                        $('#cash-button-container').on("click", function(e){
    
                                                                            book_a_session_reveal_loader();
    
                                                                            // Prepare session times and date values
    
                                                                            var session_date_array = [];
                                                                            var session_time_array = [];
    
                                                                            if ( chosen_quantity > 1 && session_values && $('[data-session-tab]').length == chosen_quantity ) {
    
                                                                                // For multiple sessions, get the session time and dates stored in the session values object and push them to their arrays
    
                                                                                $('[data-session-tab]').each(function(){
    
                                                                                    var session_tab_number = $(this).attr("data-session-tab");
    
                                                                                    session_date_array.push( session_values[ session_tab_number ].date );
                                                                                    session_time_array.push( session_values[ session_tab_number ].schedule );
    
                                                                                });
    
                                                                            } else {
    
                                                                                // For just a single session, get the values of checked radios and push them as single entries in their arrays
    
                                                                                session_date_array.push( $('input[name="date"]:checked').attr("data-frontend-date") );
                                                                                session_time_array.push( $('input[name="schedule_id"]:checked').attr("value") );
    
                                                                            }
                                                                            
                                                                            // Create an order via WP REST API
    
                                                                            $.ajax({
    
                                                                                url: api_root + "orders/",
                                                                                method: 'POST',
                                                                                data: {
    
                                                                                    "session_project":   "session",
                                                                                    "session_time":      session_time_array,
                                                                                    "session_date":      session_date_array,
                                                                                    "quantity":          chosen_quantity,
                                                                                    "service_id":        chosen_service,
                                                                                    "region_id":         region_id,
                                                                                    "practitioner_id":   chosen_practitioner,
                                                                                    "user_id":           user_id,
                                                                                    "location_id":       chosen_location,
                                                                                    "payment_method_id": payment_method_value.id,
                                                                                    "currency_id":       currency_id,
                                                                                    "base_price":        subtotal_total,
                                                                                    "location_charge":   location_charge_total,
                                                                                    "discount":          discount_total,
                                                                                    "total":             total_total,
                                                                                    "amount_paid":       0.00,
                                                                                    "total_due":         total_total,
                                                                                    "booking_status":    "Booked",
                                                                                    "payment_status":    "Cash payment due",
                                                                                    "vfc_mobile":        null,
                                                                                    "note":              null,
                                                                                    
                                                                                },
                                                                                beforeSend: function ( xhr ) {
    
                                                                                    xhr.setRequestHeader( 'X-WP-Nonce', api_nonce );
    
                                                                                },
                                                    
                                                                            }).done( function( create_order_success ) {
    
                                                                                // Order created
    
                                                                                book_a_session_reveal_fixed_notice( "Your order has been placed. Please wait while we send out your invoice.", "success", "dismissable" );
    
                                                                                // Send invoice
    
                                                                                $.ajax({
    
                                                                                    url: api_root + "invoices/" + create_order_success.invoice_id + "/send",
                                                                                    method: 'POST',
                                                                                    data: {
    
                                                                                        "order_id": create_order_success.order_id,
                                                                                        "reminder": false,
                                                                                        "recipients":      {
    
                                                                                            // Send to the client and the practitioner
    
                                                                                            "client": true,
                                                                                            "practitioner": true,
                                                                                            "self": false,
    
                                                                                        },
                                                                                        "preview": false,
                                                                                        
                                                                                    },
                                                                                    beforeSend: function ( xhr ) {
    
                                                                                        xhr.setRequestHeader( 'X-WP-Nonce', api_nonce );
    
                                                                                    },
                                                        
                                                                                }).done( function( send_invoice_success ) {
    
                                                                                    book_a_session_reveal_fixed_notice( "Your invoice has been sent.", "success", "dismissable", "", 5000 );
    
                                                                                    // Cash thank you page
    
                                                                                    book_a_session_hide_and_clear_modal();
    
                                                                                    setTimeout(function(){
                                                                                        
                                                                                        // Thanks page container
    
                                                                                        $('.book-a-session-modal .book-a-session-modal-inner:not(.book-a-session-dashboard-week-timetable-inner)').append('<div class="book-a-session-animated book-a-session-thanks-page"></div').addClass('book-a-session-modal-thanks');
    
                                                                                        // Thanks page text
    
                                                                                        var thanks_page_subtitle_session = chosen_quantity > 1 ? "sessions" : "session";
                                                                                        var thanks_page_subtitle_has = chosen_quantity > 1 ? "have" : "has";
    
                                                                                        $('.book-a-session-thanks-page').append('<h1 class="book-a-session-thanks-title">Thanks!</h1>')
                                                                                        .append('<h3 class="book-a-session-thanks-subtitle">Your ' + thanks_page_subtitle_session + ' ' + thanks_page_subtitle_has + ' been booked.</h3>')
                                                                                        .append('<p>We\'ve sent you your invoice. If you don\'t receive it soon, you can also find it in your dashboard. See you then!</p>');
    
                                                                                        // Thanks page session table
    
                                                                                        $('.book-a-session-thanks-page').append("<table class='book-a-session-table'><thead><th class='book-a-session-table-session'>Session</th><th class='book-a-session-table-date'>Date</th><th class='book-a-session-table-time'>Time</th></thead><tbody></tbody></table>");
    
                                                                                        // Loop through sessions and append dates and times to table
    
                                                                                        $.each( create_order_success.order.sessions, function( session_key, session_value ){
    
                                                                                            // Table Row
    
                                                                                            $('.book-a-session-thanks-page .book-a-session-table tbody').append("<tr></tr>");
    
                                                                                            // Session number
    
                                                                                            $('.book-a-session-thanks-page .book-a-session-table tbody tr').last()
                                                                                            .append("<td class='book-a-session-table-session'>" + ( session_key + 1 ) + "</td>");
    
                                                                                            // Date
    
                                                                                            $('.book-a-session-thanks-page .book-a-session-table tbody tr').last()
                                                                                            .append("<td class='book-a-session-table-date'>" + session_value.session_time_converted_array.start_date_formatted + "</td>");
    
                                                                                            // Time
    
                                                                                            $('.book-a-session-thanks-page .book-a-session-table tbody tr').last()
                                                                                            .append("<td class='book-a-session-table-time'>" + session_value.session_time_converted_array.time_string + "</td>");
    
                                                                                        });
    
                                                                                        var dashboard_url = book_a_session_frontend_authentication.dashboard_url;
    
                                                                                        if ( dashboard_url ) {
    
                                                                                            // Dashboard link
    
                                                                                            $('.book-a-session-thanks-page').append('<a href="' + dashboard_url + '" class="book-a-session-button-icon-right book-a-session-button-icon book-a-session-animated book-a-session-button book-a-session-high-button">Dashboard<span class="book-a-session-icon book-a-session-icon-dashboard"></span></a>');
    
                                                                                        }
    
                                                                                        book_a_session_reveal_modal();
    
                                                                                        book_a_session_hide_loader();
    
                                                                                    },200);
    
    
                                                                                }).fail( function(){
    
                                                                                    // Cash thank you page
    
                                                                                    book_a_session_hide_and_clear_modal();
    
                                                                                    setTimeout(function(){
                                                                                        
                                                                                        // Thanks page container
    
                                                                                        $('.book-a-session-modal .book-a-session-modal-inner:not(.book-a-session-dashboard-week-timetable-inner)').append('<div class="book-a-session-animated book-a-session-thanks-page"></div').addClass('book-a-session-modal-thanks');
    
                                                                                        // Thanks page text
    
                                                                                        var thanks_page_subtitle_session = chosen_quantity > 1 ? "sessions" : "session";
                                                                                        var thanks_page_subtitle_has = chosen_quantity > 1 ? "have" : "has";
    
                                                                                        $('.book-a-session-thanks-page').append('<h1 class="book-a-session-thanks-title">Thanks!</h1>')
                                                                                        .append('<h3 class="book-a-session-thanks-subtitle">Your ' + thanks_page_subtitle_session + ' ' + thanks_page_subtitle_has + ' been booked.</h3>')
                                                                                        .append('<p>Go to your dashboard to find your invoice under the Invoices section, and review your order under the Order section, where you can also message your ' + create_order_success.order.order.practitioner_title.toLowerCase() + '. See you then!</p>');
    
                                                                                        // Thanks page session table
    
                                                                                        $('.book-a-session-thanks-page').append("<table class='book-a-session-table'><thead><th class='book-a-session-table-session'>Session</th><th class='book-a-session-table-date'>Date</th><th class='book-a-session-table-time'>Time</th></thead><tbody></tbody></table>");
    
                                                                                        // Loop through sessions and append dates and times to table
    
                                                                                        $.each( create_order_success.order.sessions, function( session_key, session_value ){
    
                                                                                            // Table Row
    
                                                                                            $('.book-a-session-thanks-page .book-a-session-table tbody').append("<tr></tr>");
    
                                                                                            // Session number
    
                                                                                            $('.book-a-session-thanks-page .book-a-session-table tbody tr').last()
                                                                                            .append("<td class='book-a-session-table-session'>" + ( session_key + 1 ) + "</td>");
    
                                                                                            // Date
    
                                                                                            $('.book-a-session-thanks-page .book-a-session-table tbody tr').last()
                                                                                            .append("<td class='book-a-session-table-date'>" + session_value.session_time_converted_array.start_date_formatted + "</td>");
    
                                                                                            // Time
    
                                                                                            $('.book-a-session-thanks-page .book-a-session-table tbody tr').last()
                                                                                            .append("<td class='book-a-session-table-time'>" + session_value.session_time_converted_array.time_string + "</td>");
    
                                                                                        });
    
                                                                                        var dashboard_url = book_a_session_frontend_authentication.dashboard_url;
    
                                                                                        if ( dashboard_url ) {
    
                                                                                            // Dashboard link
    
                                                                                            $('.book-a-session-thanks-page').append('<a href="' + dashboard_url + '" class="book-a-session-button-icon-right book-a-session-button-icon book-a-session-animated book-a-session-button book-a-session-high-button">Dashboard<span class="book-a-session-icon book-a-session-icon-dashboard"></span></a>');
    
                                                                                        }
    
                                                                                        book_a_session_reveal_modal();
    
                                                                                        book_a_session_hide_loader();
    
                                                                                    },200);
    
                                                                                });
    
                                                                            }).fail( function( create_order_fail ) {
    
                                                                                // Order not created
    
                                                                                book_a_session_reveal_fixed_notice( "Sorry, your order could not be placed. Please try again.", "error", "dismissable" );
    
                                                                                book_a_session_hide_loader();
    
                                                                            });
                                                                            
    
                                                                        });
    
                                                                    } else if ( payment_method_value.id == 3 ) {
    
                                                                        // Vodafone Cash
    
                                                                        var checkout_html = "<div class='book-a-session-animated book-a-session-hidden book-a-session-frontend-section book-a-session-frontend-section-checkout book-a-session-frontend-section-checkout-payment-method book-a-session-frontend-section-checkout-vfc book-a-session-frontend-section-payment-method-3'><p class='book-a-session-frontend-section-question'>" + payment_method_single_top + "</p><div class='book-a-session-flex-row'>" + checkout_price_table + "<div id='vfc-button-container' class='book-a-session-payment-button-container'></div></div></div>";
    
                                                                        $('.book-a-session-frontend-section-container').append( checkout_html );
    
                                                                        // Prepare session times and date values to calculate the due date for VFC payment on the server's side
    
                                                                        var session_date_time_array = [];
    
                                                                        if ( chosen_quantity > 1 && session_values && $('[data-session-tab]').length == chosen_quantity ) {
    
                                                                            // For multiple sessions, get the session time and dates stored in the session values object and push them to their arrays
    
                                                                            $('[data-session-tab]').each(function(){
    
                                                                                var session_tab_number = $(this).attr("data-session-tab");
    
                                                                                session_date_time_array.push({ 
    
                                                                                    date: session_values[ session_tab_number ].date,
                                                                                    schedule_id: session_values[ session_tab_number ].schedule,
    
                                                                                });
    
                                                                            });
    
                                                                        } else {
    
                                                                            // For just a single session, get the values of checked radios and push them as single entries in their arrays
                                                                            
                                                                            session_date_time_array.push({ 
    
                                                                                date: $('input[name="date"]:checked').attr("data-frontend-date"),
                                                                                schedule_id: $('input[name="schedule_id"]:checked').attr("value"),
    
                                                                            });
    
                                                                        }
    
                                                                        $.ajax({
    
                                                                            url: api_root + "invoices/due_date",
                                                                            method: 'POST',
                                                                            data: {
    
                                                                                region_id:        $region_id,
                                                                                session_array:    session_date_time_array,
                                                                                
                                                                            },
                                                                            beforeSend: function ( xhr ) {
    
                                                                                xhr.setRequestHeader( 'X-WP-Nonce', api_nonce );
                                                                                
                                                                            },
                                                
                                                                        }).done( function( vfc_due_date_success ) {
    
                                                                            // Place order button
    
                                                                            $('.book-a-session-frontend-section-checkout-vfc .book-a-session-frontend-section-question').prepend("Your payment will be due by <strong>" + vfc_due_date_success.date + "</strong> at <strong>" + vfc_due_date_success.time + "</strong>.<br><br>" );
    
                                                                            $('#vfc-button-container').append( "<input type='text' name='vfc_mobile' id='vfc_mobile' placeholder='Enter the number you will send your payment from'>" );
    
                                                                            $('#vfc-button-container').append( "<a href='javascript:void(0)' class='book-a-session-animated book-a-session-button book-a-session-big-button book-a-session-high-button'><div class='book-a-session-price-tag'><span class='book-a-session-price-text'>Book now & pay later</span></div><span class='book-a-session-icon book-a-session-icon-vodafone-cash-logo-icon'></span></a>" );
    
                                                                            $('#vfc-button-container a').on("click", function(e){
    
                                                                                book_a_session_reveal_loader();
    
                                                                                $vfc_mobile_number =  $('input[name="vfc_mobile"]').val();
    
                                                                                $vfc_mobile_number = $vfc_mobile_number.replace(/ /g,'');
    
                                                                                $minimum_number_of_digits = 8;
    
                                                                                if ( $vfc_mobile_number.length >= $minimum_number_of_digits ) {
    
                                                                                    // Prepare session times and date values
    
                                                                                    var session_date_array = [];
                                                                                    var session_time_array = [];
    
                                                                                    if ( chosen_quantity > 1 && session_values && $('[data-session-tab]').length == chosen_quantity ) {
    
                                                                                        // For multiple sessions, get the session time and dates stored in the session values object and push them to their arrays
    
                                                                                        $('[data-session-tab]').each(function(){
    
                                                                                            var session_tab_number = $(this).attr("data-session-tab");
    
                                                                                            session_date_array.push( session_values[ session_tab_number ].date );
                                                                                            session_time_array.push( session_values[ session_tab_number ].schedule );
    
                                                                                        });
    
                                                                                    } else {
    
                                                                                        // For just a single session, get the values of checked radios and push them as single entries in their arrays
    
                                                                                        session_date_array.push( $('input[name="date"]:checked').attr("data-frontend-date") );
                                                                                        session_time_array.push( $('input[name="schedule_id"]:checked').attr("value") );
    
                                                                                    }
                                                                                    
                                                                                    // Create an order via WP REST API
    
                                                                                    $.ajax({
    
                                                                                        url: api_root + "orders/",
                                                                                        method: 'POST',
                                                                                        data: {
    
                                                                                            "session_project":   "session",
                                                                                            "session_time":      session_time_array,
                                                                                            "session_date":      session_date_array,
                                                                                            "quantity":          chosen_quantity,
                                                                                            "service_id":        chosen_service,
                                                                                            "region_id":         region_id,
                                                                                            "practitioner_id":   chosen_practitioner,
                                                                                            "user_id":           user_id,
                                                                                            "location_id":       chosen_location,
                                                                                            "payment_method_id": payment_method_value.id,
                                                                                            "currency_id":       currency_id,
                                                                                            "base_price":        subtotal_total,
                                                                                            "location_charge":   location_charge_total,
                                                                                            "discount":          discount_total,
                                                                                            "total":             total_total,
                                                                                            "amount_paid":       0.00,
                                                                                            "total_due":         total_total,
                                                                                            "booking_status":    "Pending",
                                                                                            "payment_status":    "Vodafone Cash payment due",
                                                                                            "vfc_mobile":        $('input[name="vfc_mobile"]').val(),
                                                                                            "note":              null,
                                                                                            
                                                                                        },
                                                                                        beforeSend: function ( xhr ) {
    
                                                                                            xhr.setRequestHeader( 'X-WP-Nonce', api_nonce );
    
                                                                                        },
                                                            
                                                                                    }).done( function( create_order_success ) {
    
                                                                                        // Order created
    
                                                                                        book_a_session_reveal_fixed_notice( "Thanks! Your order has been placed. Please wait while we send you your invoice.", "success", "dismissable" );
                                                                                        
                                                                                        // Send invoice
    
                                                                                        $.ajax({
    
                                                                                            url: api_root + "invoices/" + create_order_success.invoice_id + "/send",
                                                                                            method: 'POST',
                                                                                            data: {
        
                                                                                                "order_id": create_order_success.order_id,
                                                                                                "reminder": false,
                                                                                                "recipients":      {
    
                                                                                                    // Send to the client and the practitioner
    
                                                                                                    "client": true,
                                                                                                    "practitioner": true,
                                                                                                    "self": false,
    
                                                                                                },
                                                                                                "preview": false,
                                                                                                
                                                                                            },
                                                                                            beforeSend: function ( xhr ) {
        
                                                                                                xhr.setRequestHeader( 'X-WP-Nonce', api_nonce );
        
                                                                                            },
                                                                
                                                                                        }).done( function( send_invoice_success ) {
    
                                                                                            book_a_session_reveal_fixed_notice( "Thanks! Your order has been placed, and your invoice is on the way.", "success", "dismissable" );
    
                                                                                            // Vodafone Cash thank you page
    
                                                                                            book_a_session_hide_and_clear_modal();
    
                                                                                            setTimeout(function(){
                                                                                                
                                                                                                // Thanks page container
    
                                                                                                $('.book-a-session-modal .book-a-session-modal-inner:not(.book-a-session-dashboard-week-timetable-inner)').append('<div class="book-a-session-animated book-a-session-thanks-page"></div').addClass('book-a-session-modal-thanks');
    
                                                                                                // Thanks page text
    
                                                                                                var thanks_page_subtitle_session = chosen_quantity > 1 ? "sessions" : "session";
                                                                                                var thanks_page_subtitle_has = chosen_quantity > 1 ? "have" : "has";
    
                                                                                                var vfc_receiving_number = book_a_session_frontend_authentication.vfc_receiving_number ? book_a_session_frontend_authentication.vfc_receiving_number : "the number we'll send you";
    
                                                                                                $('.book-a-session-thanks-page').append('<h1 class="book-a-session-thanks-title">Thanks!</h1>')
                                                                                                .append('<h3 class="book-a-session-thanks-subtitle">Your ' + thanks_page_subtitle_session + ' ' + thanks_page_subtitle_has + ' been booked, we\'re just waiting for your payment.</h3>')
                                                                                                .append('<p>You\'ll need to make your payment of <strong>' + price_tag_html_derived + '</strong> to <strong>' + vfc_receiving_number + '</strong> by <strong>' + vfc_due_date_success.date + "</strong> at <strong>" + vfc_due_date_success.time + '</strong>. We\'ve sent you your invoice containing a breakdown of your order and instructions you might need to make your payment. If it doesn\'t arrive soon, you can also find your invoices and orders in your dashboard. See you then!</p>');
    
                                                                                                // Thanks page session table
    
                                                                                                $('.book-a-session-thanks-page').append("<table class='book-a-session-table'><thead><th class='book-a-session-table-session'>Session</th><th class='book-a-session-table-date'>Date</th><th class='book-a-session-table-time'>Time</th></thead><tbody></tbody></table>");
    
                                                                                                // Loop through sessions and append dates and times to table
    
                                                                                                $.each( create_order_success.order.sessions, function( session_key, session_value ){
    
                                                                                                    // Table Row
    
                                                                                                    $('.book-a-session-thanks-page .book-a-session-table tbody').append("<tr></tr>");
    
                                                                                                    // Session number
    
                                                                                                    $('.book-a-session-thanks-page .book-a-session-table tbody tr').last()
                                                                                                    .append("<td class='book-a-session-table-session'>" + ( session_key + 1 ) + "</td>");
    
                                                                                                    // Date
    
                                                                                                    $('.book-a-session-thanks-page .book-a-session-table tbody tr').last()
                                                                                                    .append("<td class='book-a-session-table-date'>" + session_value.session_time_converted_array.start_date_formatted + "</td>");
    
                                                                                                    // Time
    
                                                                                                    $('.book-a-session-thanks-page .book-a-session-table tbody tr').last()
                                                                                                    .append("<td class='book-a-session-table-time'>" + session_value.session_time_converted_array.time_string + "</td>");
    
                                                                                                });
    
                                                                                                var dashboard_url = book_a_session_frontend_authentication.dashboard_url;
    
                                                                                                if ( dashboard_url ) {
    
                                                                                                    // Dashboard link
    
                                                                                                    $('.book-a-session-thanks-page').append('<a href="' + dashboard_url + '" class="book-a-session-button-icon-right book-a-session-button-icon book-a-session-animated book-a-session-button book-a-session-high-button">Dashboard<span class="book-a-session-icon book-a-session-icon-dashboard"></span></a>');
    
                                                                                                }
    
                                                                                                book_a_session_reveal_modal();
    
                                                                                                book_a_session_hide_loader();
    
                                                                                            },200);
    
    
                                                                                        }).fail( function( send_invoice_fail ){
    
                                                                                            book_a_session_reveal_fixed_notice( "Thanks! Your order has been placed, but your invoice was not sent. You can find a copy of your invoice in your dashboard.", "info", "dismissable" );
    
                                                                                            // Vodafone Cash thank you page
    
                                                                                            book_a_session_hide_and_clear_modal();
    
                                                                                            setTimeout(function(){
                                                                                                
                                                                                                // Thanks page container
    
                                                                                                $('.book-a-session-modal .book-a-session-modal-inner:not(.book-a-session-dashboard-week-timetable-inner)').append('<div class="book-a-session-animated book-a-session-thanks-page"></div').addClass('book-a-session-modal-thanks');
    
                                                                                                // Thanks page text
    
                                                                                                var thanks_page_subtitle_session = chosen_quantity > 1 ? "sessions" : "session";
                                                                                                var thanks_page_subtitle_has = chosen_quantity > 1 ? "have" : "has";
    
                                                                                                var vfc_receiving_number = book_a_session_frontend_authentication.vfc_receiving_number ? book_a_session_frontend_authentication.vfc_receiving_number : "the number we'll send you";
    
                                                                                                $('.book-a-session-thanks-page').append('<h1 class="book-a-session-thanks-title">Thanks!</h1>')
                                                                                                .append('<h3 class="book-a-session-thanks-subtitle">Your ' + thanks_page_subtitle_session + ' ' + thanks_page_subtitle_has + ' been booked, we\'re just waiting for your payment.</h3>')
                                                                                                .append('<p>You\'ll need to make your payment of <strong>' + price_tag_html_derived + '</strong> to <strong>' + vfc_receiving_number + '</strong> by <strong>' + vfc_due_date_success.date + "</strong> at <strong>" + vfc_due_date_success.time + '</strong>. Head over to your dashboard to find your invoice and order where you can find your payment instructions and other things. See you then!</p>');
    
                                                                                                // Thanks page session table
    
                                                                                                $('.book-a-session-thanks-page').append("<table class='book-a-session-table'><thead><th class='book-a-session-table-session'>Session</th><th class='book-a-session-table-date'>Date</th><th class='book-a-session-table-time'>Time</th></thead><tbody></tbody></table>");
    
                                                                                                // Loop through sessions and append dates and times to table
    
                                                                                                $.each( create_order_success.order.sessions, function( session_key, session_value ){
    
                                                                                                    // Table Row
    
                                                                                                    $('.book-a-session-thanks-page .book-a-session-table tbody').append("<tr></tr>");
    
                                                                                                    // Session number
    
                                                                                                    $('.book-a-session-thanks-page .book-a-session-table tbody tr').last()
                                                                                                    .append("<td class='book-a-session-table-session'>" + ( session_key + 1 ) + "</td>");
    
                                                                                                    // Date
    
                                                                                                    $('.book-a-session-thanks-page .book-a-session-table tbody tr').last()
                                                                                                    .append("<td class='book-a-session-table-date'>" + session_value.session_time_converted_array.start_date_formatted + "</td>");
    
                                                                                                    // Time
    
                                                                                                    $('.book-a-session-thanks-page .book-a-session-table tbody tr').last()
                                                                                                    .append("<td class='book-a-session-table-time'>" + session_value.session_time_converted_array.time_string + "</td>");
    
                                                                                                });
    
                                                                                                var dashboard_url = book_a_session_frontend_authentication.dashboard_url;
    
                                                                                                if ( dashboard_url ) {
    
                                                                                                    // Dashboard link
    
                                                                                                    $('.book-a-session-thanks-page').append('<a href="' + dashboard_url + '" class="book-a-session-button-icon-right book-a-session-button-icon book-a-session-animated book-a-session-button book-a-session-high-button">Dashboard<span class="book-a-session-icon book-a-session-icon-dashboard"></span></a>');
    
                                                                                                }
    
                                                                                                book_a_session_reveal_modal();
    
                                                                                                book_a_session_hide_loader();
    
                                                                                            },200);
    
    
                                                                                        });
    
    
                                                                                    }).fail( function() {
    
                                                                                        // Order not created
    
                                                                                        book_a_session_reveal_fixed_notice( "Sorry, your order could not be placed, please try again.", "error", "dismissable" );
    
                                                                                        book_a_session_hide_loader();
    
                                                                                    });
    
                                                                                }                                                                        
    
                                                                            });
    
                                                                        }).fail( function(){
    
                                                                            // Can't get VFC Due Date
    
                                                                            book_a_session_reveal_fixed_notice( "We couldn't get an accurate due date. Please check your invoice.", "info", "dismissable", "book-a-session-icon-vodafone-cash-logo-icon" );
    
                                                                        });
                                                                                                                                                    
                                                                    } else if ( payment_method_value.id == 4 ) {
    
                                                                        // Credit / Debit Card (Stripe)
    
                                                                        var checkout_html = "<div class='book-a-session-animated book-a-session-hidden book-a-session-frontend-section book-a-session-frontend-section-checkout book-a-session-frontend-section-checkout-payment-method book-a-session-frontend-section-checkout-stripe book-a-session-frontend-section-payment-method-4'><p class='book-a-session-frontend-section-question'>" + payment_method_single_top + "</p><div class='book-a-session-flex-row'>" + checkout_price_table + "<div id='stripe-button-container' class='book-a-session-payment-button-container'></div></div></div>";
    
                                                                        $('.book-a-session-frontend-section-container').append( checkout_html );
    
                                                                        if ( payment_method_icon ) {
    
                                                                            var payment_method_icon_html = "<span class='book-a-session-icon book-a-session-icon-shopping-basket'></span>";
                                                                            
                                                                        }
        
                                                                        // Stripe payment button
    
                                                                        $('#stripe-button-container').append( "<a href='javascript:void(0)' class='book-a-session-animated book-a-session-button book-a-session-big-button book-a-session-high-button'><div class='book-a-session-price-tag'><span class='book-a-session-price-text'>Pay now by card</span></div>" + payment_method_icon_html + "</a>" );
    
                                                                        // Stripe icons
    
                                                                        var stripe_cc_img_url = book_a_session_frontend_authentication.stripe_cc_img_url;
    
                                                                        $('#stripe-button-container').append("<img src='" + stripe_cc_img_url + "' alt='Credit / Debit Card Logos'>");
    
                                                                        // Stripe key
    
                                                                        var test_live = book_a_session_frontend_authentication.payment_method_options_array.test_live_4;
                                                                        var test_key = book_a_session_frontend_authentication.payment_method_options_array.test_pub_key_4;
                                                                        var live_key = book_a_session_frontend_authentication.payment_method_options_array.live_pub_key_4;
    
                                                                        var stripe_key = test_live == "live" ? live_key : test_key;
    
                                                                        var logo_image_square = book_a_session_frontend_authentication.logo_square_url[0];
    
                                                                        // Stripe handler
    
                                                                        var handler = StripeCheckout.configure({
    
                                                                            key: stripe_key,
                                                                            image: logo_image_square ? logo_image_square : 'https://stripe.com/img/documentation/checkout/marketplace.png',
                                                                            locale: 'auto',
    
                                                                            token: function( token ) {
    
                                                                                book_a_session_reveal_loader();
    
                                                                                // Charge Stripe
                                                                                
                                                                                $.ajax({
    
                                                                                    url: api_root + "payment/stripe",
                                                                                    method: 'POST',
                                                                                    data: {
    
                                                                                        "stripeToken": token.id,
                                                                                        "stripeEmail": token.email,
                                                                                        "total": Math.round( total_total * 100 ),
                                                                                        "currency_code": currency["code"],
                                                                                        
                                                                                    },
                                                                                    beforeSend: function ( xhr ) {
    
                                                                                        xhr.setRequestHeader( 'X-WP-Nonce', api_nonce );
    
                                                                                    },
                                                        
                                                                                }).done( function( charge_stripe_success ) {
    
                                                                                    // Charged successfully!
    
                                                                                    book_a_session_reveal_fixed_notice( "Thanks! Your payment was successful, we're just working on creating your order. Your payment ID is " + charge_stripe_success.charge.id + ".", "success", "dismissable" );
    
                                                                                    // Prepare session times and date values
    
                                                                                    var session_date_array = [];
                                                                                    var session_time_array = [];
    
                                                                                    if ( chosen_quantity > 1 && session_values && $('[data-session-tab]').length == chosen_quantity ) {
    
                                                                                        // For multiple sessions, get the session time and dates stored in the session values object and push them to their arrays
    
                                                                                        $('[data-session-tab]').each(function(){
    
                                                                                            var session_tab_number = $(this).attr("data-session-tab");
    
                                                                                            session_date_array.push( session_values[ session_tab_number ].date );
                                                                                            session_time_array.push( session_values[ session_tab_number ].schedule );
    
                                                                                        });
    
                                                                                    } else {
    
                                                                                        // For just a single session, get the values of checked radios and push them as single entries in their arrays
    
                                                                                        session_date_array.push( $('input[name="date"]:checked').attr("data-frontend-date") );
                                                                                        session_time_array.push( $('input[name="schedule_id"]:checked').attr("value") );
    
                                                                                    }
                                                                                    
                                                                                    // Create an order via WP REST API
    
                                                                                    $.ajax({
    
                                                                                        url: api_root + "orders/",
                                                                                        method: 'POST',
                                                                                        data: {
    
                                                                                            "session_project":   "session",
                                                                                            "session_time":      session_time_array,
                                                                                            "session_date":      session_date_array,
                                                                                            "quantity":          chosen_quantity,
                                                                                            "service_id":        chosen_service,
                                                                                            "region_id":         region_id,
                                                                                            "practitioner_id":   chosen_practitioner,
                                                                                            "user_id":           user_id,
                                                                                            "location_id":       chosen_location,
                                                                                            "payment_method_id": payment_method_value.id,
                                                                                            "currency_id":       currency_id,
                                                                                            "base_price":        subtotal_total,
                                                                                            "location_charge":   location_charge_total,
                                                                                            "discount":          discount_total,
                                                                                            "total":             total_total,
                                                                                            "amount_paid":       0.00,
                                                                                            "total_due":         total_total,
                                                                                            "booking_status":    "Pending",
                                                                                            "payment_status":    "Online payment due",
                                                                                            "vfc_mobile":        null,
                                                                                            "note":              null,
                                                                                            
                                                                                        },
                                                                                        beforeSend: function ( xhr ) {
    
                                                                                            xhr.setRequestHeader( 'X-WP-Nonce', api_nonce );
    
                                                                                        },
                                                            
                                                                                    }).done( function( create_order_success ) {
    
                                                                                        // Order created
    
                                                                                        book_a_session_reveal_fixed_notice( "Your order has been placed. Your payment ID is " + charge_stripe_success.charge.id + ".", "success", "dismissable" );
    
                                                                                        // Now record payment
    
                                                                                        $.ajax({
    
                                                                                            url: api_root + "orders/" + create_order_success.order_id + "/payment/",
                                                                                            method: 'POST',
                                                                                            data: {
    
                                                                                                "payment_method_id": payment_method_value.id,
                                                                                                "invoice_id": create_order_success.invoice_id,
                                                                                                "payment_id": charge_stripe_success.charge.id,
                                                                                                "total": total_total,
                                                                                                "success": 1,
                                                                                                "note": JSON.stringify( token ),
                                                                                                
                                                                                            },
                                                                                            beforeSend: function ( xhr ) {
    
                                                                                                xhr.setRequestHeader( 'X-WP-Nonce', api_nonce );
    
                                                                                            },
                                                                
                                                                                        }).done( function( record_payment_success ) {
    
                                                                                            // Payment recorded
    
                                                                                            // Stripe thank you page
    
                                                                                            book_a_session_hide_and_clear_modal();
    
                                                                                            setTimeout(function(){
                                                                                                
                                                                                                // Thanks page container
    
                                                                                                $('.book-a-session-modal .book-a-session-modal-inner:not(.book-a-session-dashboard-week-timetable-inner)').append('<div class="book-a-session-animated book-a-session-thanks-page"></div').addClass('book-a-session-modal-thanks');
    
                                                                                                // Thanks page text
    
                                                                                                var thanks_page_subtitle_session = chosen_quantity > 1 ? "sessions" : "session";
                                                                                                var thanks_page_subtitle_has = chosen_quantity > 1 ? "have" : "has";
    
                                                                                                $('.book-a-session-thanks-page').append('<h1 class="book-a-session-thanks-title">Thanks!</h1>')
                                                                                                .append('<h3 class="book-a-session-thanks-subtitle">Your payment was successful, and your ' + thanks_page_subtitle_session + ' ' + thanks_page_subtitle_has + ' been booked.</h3>')
                                                                                                .append('<p>Head over to your dashboard where you can review your order and message your ' + create_order_success.order.order.practitioner_title.toLowerCase() + '. See you then!</p>');
    
                                                                                                // Thanks page session table
    
                                                                                                $('.book-a-session-thanks-page').append("<table class='book-a-session-table'><thead><th class='book-a-session-table-session'>Session</th><th class='book-a-session-table-date'>Date</th><th class='book-a-session-table-time'>Time</th></thead><tbody></tbody></table>");
    
                                                                                                // Loop through sessions and append dates and times to table
    
                                                                                                $.each( create_order_success.order.sessions, function( session_key, session_value ){
    
                                                                                                    // Table Row
    
                                                                                                    $('.book-a-session-thanks-page .book-a-session-table tbody').append("<tr></tr>");
    
                                                                                                    // Session number
    
                                                                                                    $('.book-a-session-thanks-page .book-a-session-table tbody tr').last()
                                                                                                    .append("<td class='book-a-session-table-session'>" + ( session_key + 1 ) + "</td>");
    
                                                                                                    // Date
    
                                                                                                    $('.book-a-session-thanks-page .book-a-session-table tbody tr').last()
                                                                                                    .append("<td class='book-a-session-table-date'>" + session_value.session_time_converted_array.start_date_formatted + "</td>");
    
                                                                                                    // Time
    
                                                                                                    $('.book-a-session-thanks-page .book-a-session-table tbody tr').last()
                                                                                                    .append("<td class='book-a-session-table-time'>" + session_value.session_time_converted_array.time_string + "</td>");
    
                                                                                                });
    
                                                                                                var dashboard_url = book_a_session_frontend_authentication.dashboard_url;
    
                                                                                                if ( dashboard_url ) {
    
                                                                                                    // Dashboard link
    
                                                                                                    $('.book-a-session-thanks-page').append('<a href="' + dashboard_url + '" class="book-a-session-button-icon-right book-a-session-button-icon book-a-session-animated book-a-session-button book-a-session-high-button">Dashboard<span class="book-a-session-icon book-a-session-icon-dashboard"></span></a>');
    
                                                                                                }
    
                                                                                                book_a_session_reveal_modal();
    
                                                                                                book_a_session_hide_loader();
    
                                                                                            },200);
    
    
                                                                                        }).fail( function( record_payment_fail ){
    
                                                                                            // Payment not recorded
    
                                                                                            book_a_session_reveal_fixed_notice( "Thanks! Your payment was successful, but your order was not updated to reflect it. Your payment ID is " + charge_stripe_success.charge.id + ". Keep note of this and contact your " + data.order.practitioner_title.toLowerCase() + ".", "warning", "dismissable" );
                                                                                            
                                                                                            book_a_session_hide_loader();
    
                                                                                        });
    
                                                                                    }).fail( function( create_order_fail ) {
    
                                                                                        // Order not created
    
                                                                                        book_a_session_reveal_fixed_notice( "Thanks! Your payment was successful, but your order could not be created. Your payment ID is " + charge_stripe_success.charge.id + ". Keep note of this and contact us.", "error", "dismissable" );
    
                                                                                        book_a_session_hide_loader();
    
                                                                                    });
    
                                                                                }).fail( function( charge_stripe_fail ){
    
                                                                                    // Charge could not be request
    
                                                                                    book_a_session_reveal_fixed_notice( "Your payment could not be processed. Please try again or try a different payment method", "error", "dismissable" );
    
                                                                                    book_a_session_hide_loader();
    
                                                                                });
    
                                                                                
    
                                                                            }
    
                                                                        });
    
                                                                        $('#stripe-button-container a').on( "click", function( e ) {
    
                                                                            book_a_session_reveal_inline_loader( $( e.currentTarget ) );
    
                                                                            // Open Checkout with further options:
    
                                                                            var stripe_description = chosen_quantity > 1 ? chosen_quantity +  " sessions of " + service.name + " - " + chosen_location_name + "." : service.name + " - " + chosen_location_name + ".";
                                                                            var site_name = book_a_session_frontend_authentication.site_name;
    
                                                                            handler.open({
    
                                                                                name: site_name,
                                                                                description: stripe_description,
                                                                                zipCode: true,
                                                                                currency: currency["code"],
    
                                                                                // Amount needs to be an integer, so we add two extra zeroes to charge accuarately
    
                                                                                amount: Math.round( total_total * 100 ),
    
                                                                            });
    
                                                                            e.preventDefault();
    
                                                                            book_a_session_hide_inline_loader( $( e.currentTarget ) );
    
    
                                                                        });
                                                                        
                                                                        // Close Checkout on page navigation:
    
                                                                        window.addEventListener('popstate', function() {
    
                                                                            handler.close();
    
                                                                        });
    
                                                                        $('.book-a-session-modal:not(.book-a-session-modal-week-timetable) .book-a-session-icon-close, .book-a-session-modal:not(.book-a-session-modal-week-timetable)').click( function( e ) {
    
                                                                            // If the event's true target wasn't clicked ( the close button or the actual modal background ), don't do anything, that is, clicking the modal inner shouldn't work just because it's a modal child
                                                                    
                                                                            if ( e.target !== e.currentTarget ) return;
                                                                    
                                                                            handler.close();
                                                                    
                                                                        });
    
                                                                    }
    
                                                                    $('.book-a-session-frontend-section-payment-method-' + payment_method_value.id + ' .book-a-session-flex-table-value.book-a-session-base-price').html( price_tag_html_derived ).find(".book-a-session-price-amount").html( subtotal_total.toFixed(2) );
                                                                    $('.book-a-session-frontend-section-payment-method-' + payment_method_value.id + ' .book-a-session-flex-table-value.book-a-session-location-charge').html( price_tag_html_derived ).find(".book-a-session-price-amount").html( location_charge_total.toFixed(2) );
                                                                    $('.book-a-session-frontend-section-payment-method-' + payment_method_value.id + ' .book-a-session-flex-table-value.book-a-session-discount').html( "<span class='book-a-session-minus'>-</span>" + price_tag_html_derived ).find(".book-a-session-price-amount").html( discount_total );
                                                                    $('.book-a-session-frontend-section-payment-method-' + payment_method_value.id + ' .book-a-session-flex-table-value.book-a-session-grand-total').html( price_tag_html_derived ).find(".book-a-session-price-amount").html( total_total.toFixed(2) );
    
                                                                    $('.book-a-session-frontend-section-payment-method-' + payment_method_value.id ).append('<div class="book-a-session-frontend-bottom-bar"><p>' + payment_method_single_bottom + '</p><a href="javascript:void(0)" class="book-a-session-animated book-a-session-button book-a-session-circle-button book-a-session-hidden"><span class="book-a-session-icon book-a-session-icon-chevron-right"></span>Next</a></div>')
    
                                                                }
    
                                                            });
    
                                                        }
                
                                                    });
                
                                                }
    
                                            });
    
                                        }
            
                                    });
    
                                    // Set payment method label container widths
    
                                    $('.book-a-session-frontend-section-payment .book-a-session-frontend-payment-method').css({"width": "calc( ( ( 100% - ( 17.5px * " + ( $('.book-a-session-frontend-section-payment .book-a-session-frontend-payment-method').length - 1 ) + " ) ) / " + ( $('.book-a-session-frontend-section-payment .book-a-session-frontend-payment-method').length ) + " ) )" });
                                    $('.book-a-session-frontend-section-payment .book-a-session-frontend-payment-method').last().css({ "margin-right": 0 });
    
                                    // Update payment method when one is selected
    
                                    $('input[name="payment_method"]').on("change", function(){
    
                                        chosen_payment_method = $(this).attr("value");
    
                                    });
    
                                }
        
                            }
    
                            $('[data-progress-tab-content="payment"]').on("click", function(e){
    
                                if ( ! $(this).hasClass("book-a-session-tab-button-inactive") ) {
    
                                    if ( ! session_values ) var session_values = {};
    
                                    book_a_session_update_payment_methods( session_values );
    
                                    $content = $(this).attr("data-progress-tab-content");
    
                                    $('.book-a-session-frontend-modal [data-progress-tab-content]').removeClass('book-a-session-tab-button-current');
        
                                    $(this).addClass('book-a-session-tab-button-current');
        
                                    $('.book-a-session-frontend-modal .book-a-session-frontend-section').addClass('book-a-session-hidden');
                                    $('.book-a-session-frontend-modal .book-a-session-frontend-section-' + $content).removeClass('book-a-session-hidden');
    
                                    $('.book-a-session-frontend-modal .book-a-session-frontend-section-container').css({ "height":  $(window).width() >= 768 ? $('.book-a-session-frontend-modal .book-a-session-frontend-section-' + $content).outerHeight() + "px" : "auto" });
    
                                }
    
                            });
    
                            // Payment Next button
    
                            $('.book-a-session-frontend-section-payment .book-a-session-frontend-bottom-bar a.book-a-session-circle-button').on("click", function(){
    
                                if ( $('input[name="payment_method"]:checked').length !== 0 && chosen_payment_method > 0 ) {
    
                                    // Hide current section and reveal next section
    
                                    $('.book-a-session-frontend-modal .book-a-session-frontend-section').addClass('book-a-session-hidden');
    
                                    $( '.book-a-session-frontend-section-payment-method-' + chosen_payment_method ).removeClass('book-a-session-hidden');
                                    
                                    $('.book-a-session-frontend-modal .book-a-session-frontend-section-container').css({ "height":  $(window).width() >= 768 ? $( '.book-a-session-frontend-section-payment-method-' + chosen_payment_method ).outerHeight() + "px" : "auto" });
    
                                }
    
                            });
    
                            // Next buttons (except for datetime picker loaders which will be done separately)
    
                            $('.book-a-session-frontend-modal .book-a-session-frontend-section:not(.book-a-session-frontend-section-datetime):not(.book-a-session-frontend-section-payment) .book-a-session-frontend-bottom-bar .book-a-session-circle-button:not(.book-a-session-load-datetime-picker)').click(function() {
    
                                if ( $(this).parents(".book-a-session-frontend-section").find("input[type='radio']:checked + label").length > 0 && ! $(this).parents(".book-a-session-frontend-section").hasClass('book-a-session-frontend-section-datetime') )  {
    
                                    // Hide current section and reveal next section
                                    $('.book-a-session-frontend-modal .book-a-session-frontend-section').addClass('book-a-session-hidden');
                                    $(this).parents('.book-a-session-frontend-section').next('.book-a-session-frontend-section').removeClass('book-a-session-hidden');
    
                                    $('.book-a-session-progress-tab-container.book-a-session-tab-button-container .book-a-session-tab-button.book-a-session-tab-button-current').removeClass('book-a-session-tab-button-current')
                                    .next('.book-a-session-tab-button').addClass('book-a-session-tab-button-current').removeClass('book-a-session-tab-button-inactive');
                                    
                                    $('.book-a-session-frontend-modal .book-a-session-frontend-section-container').css({ "height":  $(window).width() >= 768 ? $(this).parents('.book-a-session-frontend-section').next('.book-a-session-frontend-section').outerHeight() + "px" : "auto" })
    
                                }
    
                            });
    
                            // Add animated class to sections 
    
                            $('.book-a-session-frontend-modal .book-a-session-frontend-section').addClass('book-a-session-animated');
    
                            // Tab button width
    
                            $('.book-a-session-progress-tab-container.book-a-session-tab-button-container .book-a-session-tab-button').css({ "width": "calc( 100% / " + ( $(this).length ) + " )" });
      
                            // Tab button interactivity except for datetime picker loaders which will be done separately
    
                            $('.book-a-session-frontend-modal [data-progress-tab-content]:not(.book-a-session-load-datetime-picker):not([data-progress-tab-content="payment"]').click(function() {
    
                                if ( ! $(this).hasClass('book-a-session-tab-button-inactive') ) {
    
                                    $content = $(this).attr("data-progress-tab-content");
    
                                    $('.book-a-session-frontend-modal [data-progress-tab-content]').removeClass('book-a-session-tab-button-current');
        
                                    $(this).addClass('book-a-session-tab-button-current');
        
                                    $('.book-a-session-frontend-modal .book-a-session-frontend-section').addClass('book-a-session-hidden');
                                    $('.book-a-session-frontend-modal .book-a-session-frontend-section-' + $content).removeClass('book-a-session-hidden');
    
                                    $('.book-a-session-frontend-modal .book-a-session-frontend-section-container').css({ "height":   $(window).width() >= 768 ? $('.book-a-session-frontend-modal .book-a-session-frontend-section-' + $content).outerHeight() + "px" : "auto" });
        
                                    

                                }
    
                            });
    
                            // Initialise datetime picker data, used to check if we need to get new data from the REST API
    
                            $('.book-a-session-frontend-section-datetime').data( "getParameters", { quantity: 0, location: 0, practitioner: 0 } );
    
                            // Datetime picker
                                
                            $('.book-a-session-load-datetime-picker').click(function(e) {
    
                                // Check to see if we should be getting a new datetime picker. If so, we start a GET request and put it in the DOM. Otherwise, if there's already a datepicker present, we display the section as usual.
    
                                if ( ! $(e.currentTarget).hasClass('book-a-session-tab-button-inactive') && chosen_quantity != 0 && chosen_location != 0 && chosen_practitioner != 0 ) {
    
                                    // If a user has changed a choice that would require a new set of dates to be loaded, do that
                                    
                                    if ( $('.book-a-session-frontend-section-datetime').data( "getParameters" ).quantity != chosen_quantity || $('.book-a-session-frontend-section-datetime').data( "getParameters" ).location != chosen_location || $('.book-a-session-frontend-section-datetime').data( "getParameters" ).practitioner != chosen_practitioner ) {
    
                                        book_a_session_reveal_inline_loader( $(e.currentTarget) );
    
                                        $.ajax({
            
                                            url: api_root + "datetime_pickers/region_id=" + $region_id + "/quantity=" + chosen_quantity + "/location_id=" + chosen_location + "/practitioner_id=" + chosen_practitioner + "/user_id=" + user_id,
                                            method: 'GET',
            
                                        }).done( function( datetime_picker ){
    
                                            // Save the GET parameters sent by this AJAX GET as jQuery data to the datetime frontend section element, which we can use to check if we need to get a new datetime picker
    
                                            $('.book-a-session-frontend-section-datetime').data( "getParameters", { quantity: chosen_quantity, location: chosen_location, practitioner: chosen_practitioner } );
                                        
                                            // Remove old stuff
    
                                            $('.book-a-session-datetime-picker-container').remove();
                                            $('.book-a-session-datetime-picker-container-schedule-wrapper').remove();
                                            $('.book-a-session-session-tabs').remove();
                                            $('.book-a-session-frontend-section-datetime .book-a-session-frontend-bottom-bar a.book-a-session-circle-button').remove();
    
                                            // Add new circle button as the old one may have had old events
    
                                            $('.book-a-session-frontend-section-datetime .book-a-session-frontend-bottom-bar').append('<a href="javascript:void(0)" class="book-a-session-animated book-a-session-button book-a-session-circle-button"><span class="book-a-session-icon book-a-session-icon-chevron-right"></span>Next</a>')
    
                                            // Invalidate tab buttons after the datetime picker if they were previously valid
    
                                            $('.book-a-session-progress-tab-container .book-a-session-tab-button').each(function(){
    
                                                // Get the position of each button. If it appears after the load datetime picker tab (as in it's after the Date & Time tab button), give it an inactive class if it doesn't already have it
    
                                                var tab_position = $(this).index();
    
                                                if ( tab_position > $('.book-a-session-progress-tab-container .book-a-session-tab-button').index( $('.book-a-session-tab-button.book-a-session-load-datetime-picker') ) && ! $(this).hasClass('book-a-session-tab-button-inactive') ) {
    
                                                    $(this).addClass('book-a-session-tab-button-inactive');
    
                                                }
    
                                            });
    
                                            // Session tabs for quantities higher than 1 
    
                                            if ( chosen_quantity > 1 ) {
    
                                                // Container
    
                                                $('.book-a-session-frontend-section-datetime .book-a-session-frontend-bottom-bar').before("<div class='book-a-session-tab-button-container book-a-session-session-tabs'></div>");
                        
                                                // Buttons
    
                                                for ( $i = 1; $i <= chosen_quantity; $i++ ) {
                        
                                                    $('.book-a-session-frontend-section-datetime .book-a-session-session-tabs').append("<a href='javascript:void(0)' data-session-tab='" + $i + "' class='book-a-session-animated book-a-session-invalid book-a-session-button book-a-session-tab-button'><span class='book-a-session-inline-anchor'>" + $i + "</span></a>")
                                                    .find(".book-a-session-tab-button").last().find(".book-a-session-inline-anchor").append("<svg class='book-a-session-animated book-a-session-hidden' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 52 52'><circle class='checkmark__circle' cx='26' cy='26' r='25' fill='none'/><path class='checkmark__check' fill='none' d='M14.1 27.2l7.1 7.2 16.7-16.8'/></svg>")
                        
                                                }
    
                                                // Button styles
    
                                                $('.book-a-session-session-tabs a:first-of-type').addClass('book-a-session-tab-button-current');
                                                $('.book-a-session-session-tabs .book-a-session-tab-button').css({ "width": "calc( 100% / " + ( $('.book-a-session-session-tabs .book-a-session-tab-button').length ) + " )" })  
                                                
                                                // Datetime picker
    
                                                $('.book-a-session-frontend-section-datetime .book-a-session-session-tabs').before( datetime_picker.dates );
    
                                                // Initialise button data
    
                                                var session_values = {};
                                                var $next_available_date = $('.book-a-session-datetime-picker-date-future:not(.book-a-session-datetime-picker-date-today):not(.book-a-session-date-unavailable)').first().find("input").attr("data-frontend-date");
    
                                                $('[data-session-tab]').each(function(){
    
                                                    session_values[ $(this).attr("data-session-tab") ] = { date: $next_available_date, schedule: "0" };
    
                                                });
    
                                            } else {
    
                                                $('.book-a-session-frontend-section-datetime .book-a-session-frontend-bottom-bar').before( datetime_picker.dates );
    
                                            }
    
                                            $('.book-a-session-datetime-picker-container').after("<div class='book-a-session-datetime-picker-container-schedule-wrapper book-a-session-animated'><div class='book-a-session-datetime-picker-container-schedule book-a-session-flex-row'></div>");
    
                                            // Get the first date's schedule in the timetable and use that as a template, whose elements will change as users choose a different date depending on timetable entries for each date
    
                                            $.each( datetime_picker.timetable_array[ $('.book-a-session-datetime-picker-date-future:not(.book-a-session-datetime-picker-date-today):not(.book-a-session-date-unavailable)').first().find('[data-frontend-date]').attr("data-frontend-date") ], function( schedule_id_key, entry_array ){
    
                                                $('.book-a-session-datetime-picker-container-schedule').append("<div class='book-a-session-animated book-a-session-frontend-option book-a-session-session-time book-a-session-session-time-available'><input type='radio' name='schedule_id' value='" + schedule_id_key + "' id='schedule_" + schedule_id_key + "'></div>");
                                                $('.book-a-session-session-time').last().append("<label for='schedule_" + schedule_id_key + "' class='book-a-session-animated book-a-session-card'><span class='book-a-session-frontend-option-title'><span class='book-a-session-start-time'>" + entry_array.time_string_array[0] + "</span>" + "<span class='book-a-session-time-suffix'>" + entry_array.time_string_array[1] + "</span><span class='book-a-session-hyphen'>-</span>" + "<span class='book-a-session-end-time'>" + entry_array.time_string_array[2] + "</span>" + "<span class='book-a-session-time-suffix'>" + entry_array.time_string_array[3] + "</span>" + "</span></label>");
    
                                                var session_time_accepted = false;
    
                                                // Loop through the location array, which has an entry for each service indexed by the service ID, and for each service, an indexed array with JSON objects, each for a location that is accepted for the service. One of those object properties is called schedule, which is either an empty indexed array (which means no session times are accepted for this location) or an indexed array of JSON entries, each with a schedule ID property and value. If, say, location_array[1][0].schedule[0].schedule_id === "2", a schedule id 2 is accepted. If there isn't a schedule[i].schedule_id which equals "2" in the array, that scheduled time is not accepted by the location
                                                
                                                $.each( location_array[ chosen_service ], function( key, value ){
    
                                                    $.each( value.schedule, function( accepted_schedule_key, accepted_schedule_value ) {
    
                                                        if ( value.id == chosen_location && accepted_schedule_value.schedule_id == schedule_id_key ) {
    
                                                            session_time_accepted = true;
    
                                                            return false;
    
                                                        }
    
                                                    });
    
                                                    if ( session_time_accepted ) {
    
                                                        return false;
    
                                                    }
    
                                                });
    
                                                if ( entry_array.unavailable || ! session_time_accepted ) {
    
                                                    $('.book-a-session-session-time').last().removeClass('book-a-session-session-time-available').addClass('book-a-session-session-time-unavailable').find("input[type='radio']").prop("disabled", true).change();
    
                                                } else if ( entry_array.order_id ) {
    
                                                    $('.book-a-session-session-time').last().removeClass('book-a-session-session-time-available').addClass('book-a-session-session-time-unavailable').find("input[type='radio']").prop("disabled", true).change();
    
                                                }
    
    
                                            });
    
                                            // Apply schedule option card widths so that all of them fit in a single row
                                            
                                            $('.book-a-session-session-time').css({ "width": "calc( ( ( 100% - ( 17.5px * " + ( $('.book-a-session-datetime-picker-container-schedule-wrapper .book-a-session-session-time').length - 1 ) + " ) ) / " + ( $('.book-a-session-datetime-picker-container-schedule-wrapper .book-a-session-session-time').length ) + " ) )" });
                                            $('.book-a-session-session-time').last().css({ "margin-right": 0 });
    
                                            // Hide all weeks by default, the first will be revealed when it's time to load the section
    
                                            $('.book-a-session-datetime-picker-container-week').addClass('book-a-session-hidden');
    
                                            // Disable radios that are deemed to be invalid choices
    
                                            var location_found = false;
    
                                            // Start by checking the localised accepted location array for the location row's accept_day (e.g. accept_mon) columns. If they are === "1", they are accepted, "0" means not accepted. If not accepted, we'll add date-unavailable classes to all dates falling on that day of the week and disable their radios.
    
                                            $.each( location_array[ chosen_service ], function( key, value ){
    
                                                if ( value.id == chosen_location ) {
    
                                                    location_found = true;
    
                                                    if ( value.accept_mon === "0" ) $('.book-a-session-datetime-picker-day-mon').addClass('book-a-session-date-unavailable');
                                                    if ( value.accept_tue === "0" ) $('.book-a-session-datetime-picker-day-tue').addClass('book-a-session-date-unavailable');
                                                    if ( value.accept_wed === "0" ) $('.book-a-session-datetime-picker-day-wed').addClass('book-a-session-date-unavailable');
                                                    if ( value.accept_thu === "0" ) $('.book-a-session-datetime-picker-day-thu').addClass('book-a-session-date-unavailable');
                                                    if ( value.accept_fri === "0" ) $('.book-a-session-datetime-picker-day-fri').addClass('book-a-session-date-unavailable');
                                                    if ( value.accept_sat === "0" ) $('.book-a-session-datetime-picker-day-sat').addClass('book-a-session-date-unavailable');
                                                    if ( value.accept_sun === "0" ) $('.book-a-session-datetime-picker-day-sun').addClass('book-a-session-date-unavailable');
    
                                                    return false;
    
                                                }
    
                                                if ( location_found ) {
    
                                                    return false;
    
                                                }
    
                                            });
    
                                            // Disable invalid radios
    
                                            $('.book-a-session-datetime-picker-date-today input[type="radio"]').prop("disabled", true);
                                            $('.book-a-session-datetime-picker-date-past input[type="radio"]').prop("disabled", true);
                                            $('.book-a-session-session-time-unavailable input[type="radio"]').prop("disabled", true);
                                            $('.book-a-session-date-unavailable input[type="radio"]').prop("disabled", true);
    
                                            // When a date radio is changed, show that date's schedule by checking the timetable array received via AJAX GET, changing the radio container classes and radio states depending on the data in the array
    
                                            $('input[data-frontend-date]').on("change", function() {
    
                                                $date = $(this).attr("data-frontend-date");
    
                                                // Hide schedule wrapper to show the user that a new schedule will be appearing
    
                                                $('.book-a-session-datetime-picker-container-schedule-wrapper').addClass('book-a-session-hidden');
    
                                                // Remove animation class so changes to input[type="radio"] can be made instantly while they're hidden, as the animation class has a .2s transition
    
                                                $('.book-a-session-session-time label').removeClass('book-a-session-animated');
    
                                                // Once the hiding animation has been completed (after 200ms) make changes while the schedule is invisible
    
                                                $('.book-a-session-frontend-section-datetime .book-a-session-frontend-bottom-bar p').addClass("book-a-session-hidden");
    
                                                setTimeout(function() {
    
                                                    // Start by getting timetable entries for the chosen date
    
                                                    var schedule = datetime_picker.timetable_array[ $date ];
    
                                                    // Change date title by looking for the date property of any of the timetable entries, we just get the first one here.
    
                                                    $date_string = schedule[ $('input[name="schedule_id"]').first().attr("value") ].date;
                                                    
                                                    $('.book-a-session-datetime-picker-container-schedule .book-a-session-datetime-picker-label-schedule').html('Pick a time for <span class="book-a-session-datetime-picker-label book-a-session-datetime-picker-label-date">' + $date_string + "</span>");
                                                    
                                                    // Undo current tab checkmark if it exists, this is because once a new date has been picked, it's not granted that the previously chosen session time, if one was chosen, will be available for the chosen date, so once a new date has been chosen, the session time is cleared and thus the session tab is made invalid until another valid session time has been chosen
    
                                                    if ( chosen_quantity > 1 && $('[data-session-tab].book-a-session-tab-button-current').length != 0 ) {
    
                                                        $('[data-session-tab].book-a-session-tab-button-current svg').addClass('book-a-session-hidden').removeClass('checkmark');
                                                        $('[data-session-tab].book-a-session-tab-button-current').addClass('book-a-session-invalid').removeClass('book-a-session-valid');
    
                                                        // Also remove the session values session time
    
                                                        session_values[ $('[data-session-tab].book-a-session-tab-button-current').attr("data-session-tab") ].schedule = "0";
    
                                                        // Invalidate tab buttons after the datetime picker if they were previously valid
    
                                                        $('.book-a-session-progress-tab-container .book-a-session-tab-button').each(function(){
    
                                                            // Get the position of each button. If it appears after the load datetime picker tab (as in it's after the Date & Time tab button), give it an inactive class if it doesn't already have it
    
                                                            var tab_position = $(this).index();
    
                                                            if ( tab_position > $('.book-a-session-progress-tab-container .book-a-session-tab-button').index( $('.book-a-session-tab-button.book-a-session-load-datetime-picker') ) && ! $(this).hasClass('book-a-session-tab-button-inactive') ) {
    
                                                                $(this).addClass('book-a-session-tab-button-inactive');
    
                                                            }
    
                                                        });
    
    
                                                    }
            
                                                    // Change radio states by going through each radio and checking its corresponding entry in the timetable array
    
                                                    $('input[name="schedule_id"]').each(function(){
    
                                                        // Start by unchecking each one and resetting any added classes to its parents denoting radio states, currently that is only one class.
    
                                                        $(this).prop("checked", false);
                                                        $(this).parent().removeClass('book-a-session-session-time-unavailable-selected');
    
                                                        // Get this schedule_id and the timetable entry row in the timetable array
    
                                                        var this_schedule = $(this).attr("value");
                                                        var entry = schedule[ $(this).attr("value") ];
    
                                                        var session_time_accepted = false;
                                                        
                                                        // Loop through the location array, which has an entry for each service indexed by the service ID, and for each service, an indexed array with JSON objects, each for a location that is accepted for the service. One of those object properties is called schedule, which is either an empty indexed array (which means no session times are accepted for this location) or an indexed array of JSON entries, each with a schedule ID property and value. If, say, location_array[1][0].schedule[0].schedule_id === "2", a schedule id 2 is accepted. If there isn't a schedule[i].schedule_id which equals "2" in the array, that scheduled time is not accepted by the location
                                                        
                                                        $.each( location_array[ chosen_service ], function( key, value ){
    
                                                            $.each( value.schedule, function( accepted_schedule_key, accepted_schedule_value ) {
    
                                                                if ( value.id == chosen_location && accepted_schedule_value.schedule_id == this_schedule ) {
    
                                                                    session_time_accepted = true;
    
                                                                    return false;
    
                                                                }
    
                                                            });
    
                                                            if ( session_time_accepted ) {
    
                                                                return false;
    
                                                            }
    
                                                        });
    
                                                        if ( ! entry.unavailable && ! entry.order_id && session_time_accepted ) {
    
                                                            // Looking at the timetable array, if this session time on this  has not been made unavailable
    
                                                            $(this).parent().addClass('book-a-session-session-time-available').removeClass('book-a-session-session-time-unavailable');
                                                            $(this).prop("disabled", false);
            
                                                        } else if ( entry.unavailable || ! session_time_accepted ) {
            
                                                            $(this).parent().removeClass('book-a-session-session-time-available').addClass('book-a-session-session-time-unavailable');
                                                            $(this).prop("disabled", true);
            
                                                        } else if ( entry.order_id ) {
            
                                                            $(this).parent().removeClass('book-a-session-session-time-available').addClass('book-a-session-session-time-unavailable');
                                                            $(this).prop("disabled", true);
            
                                                        }
    
                                                        if ( chosen_quantity > 1 && session_values ) {
    
                                                            var $schedule = session_values[ $('[data-session-tab].book-a-session-tab-button-current').attr("data-session-tab") ].schedule !== "0" ? session_values[ $('[data-session-tab].book-a-session-tab-button-current').attr("data-session-tab") ].schedule : false;
    
                                                            // Check other session values
    
                                                            $.each( session_values, function( key, value ) {
    
                                                                if ( $date === value.date && this_schedule === value.schedule && value.schedule !== $schedule ) {
    
                                                                    $('input[name="schedule_id"][value="' + value.schedule + '"]').parent().removeClass('book-a-session-session-time-available').addClass('book-a-session-session-time-unavailable').addClass('book-a-session-session-time-unavailable-selected');
                                                                    $('input[name="schedule_id"][value="' + value.schedule + '"]').prop("disabled", true);
    
                                                                }
    
                                                            });
    
                                                        }    
                                                                                                                                                
                                                    });          
    
                                                    // Update bottom bar text for date only
    
                                                    var date_string = datetime_picker.timetable_array[ $('input[data-frontend-date]:checked').attr("data-frontend-date") ][ $('input[name="schedule_id"]').first().attr("value") ].date;
                                                    var current_session_tab = $('[data-session-tab].book-a-session-tab-button-current').attr("data-session-tab");
    
                                                    if ( chosen_quantity > 1 ) {
    
                                                        $('.book-a-session-frontend-section-datetime .book-a-session-frontend-bottom-bar p')
                                                        .html( "Session " + current_session_tab + " of " + chosen_quantity + " will be for <span class='book-a-session-datetime-picker-label book-a-session-datetime-picker-label-date'>" + date_string + "</span>, but at what time?" );    
    
                                                    } else {
    
                                                        $('.book-a-session-frontend-section-datetime .book-a-session-frontend-bottom-bar p')
                                                        .html( "Your session will be for <span class='book-a-session-datetime-picker-label book-a-session-datetime-picker-label-date'>" + date_string + "</span>, but at what time?" );
        
                                                    }
    
                                                    $('.book-a-session-frontend-section-datetime .book-a-session-frontend-bottom-bar p').removeClass('book-a-session-hidden');
    
                                                    $('.book-a-session-datetime-picker-container-schedule-wrapper').removeClass('book-a-session-hidden');
    
                                                    setTimeout(function(){
    
                                                        // Replace the animation class afterwards for continued transitions as usual
    
                                                        $('.book-a-session-session-time label').addClass('book-a-session-animated');
    
                                                    },200);
    
                                                },199);
    
                                            });
    
                                            // Set new default dates for session tabs now that the previously chosen default date may have become unavailable
    
                                            if ( chosen_quantity > 1 ) {
    
                                                var $next_available_date = $('.book-a-session-datetime-picker-date-future:not(.book-a-session-datetime-picker-date-today):not(.book-a-session-date-unavailable)').first().find("input").attr("data-frontend-date");
    
                                                $('[data-session-tab]').each(function(){
        
                                                    session_values[ $(this).attr("data-session-tab") ] = { date: $next_available_date, schedule: "0" };
        
                                                });
        
                                            } else {
    
                                                // Change bottom bar text for schedule radios only when there's one session in the bag. This is done separately below for quantities higher than one.
    
                                                $('input[name="schedule_id"]').on("change", function(){
    
                                                    $('.book-a-session-frontend-section-datetime .book-a-session-frontend-bottom-bar p').addClass("book-a-session-hidden");
    
                                                    var date_string = datetime_picker.timetable_array[ $('input[data-frontend-date]:checked').attr("data-frontend-date") ][ $('input[name="schedule_id"]').first().attr("value") ].date;
                                                    var time_string = datetime_picker.timetable_array[ $('input[data-frontend-date]:checked').attr("data-frontend-date") ][ $('input[name="schedule_id"]:checked').attr("value") ].time;
        
                                                    setTimeout(function(){
        
                                                        $('.book-a-session-frontend-section-datetime .book-a-session-frontend-bottom-bar p')
                                                        .html( "Your session will be for <span class='book-a-session-datetime-picker-label book-a-session-datetime-picker-label-date'>" + date_string + "</span> at <span class='book-a-session-datetime-picker-label book-a-session-datetime-picker-label-time'>" + time_string + "</span>. Click next to continue." ).removeClass('book-a-session-hidden');
        
                                                    },200);
        
                                                });
    
                                            }
    
                                            // Find the first available date, check the radio, reveal its parent week container
    
                                            $('.book-a-session-datetime-picker-date-future:not(.book-a-session-datetime-picker-date-today):not(.book-a-session-date-unavailable)').first().find("input[type='radio']").prop("checked", true).change().parents('.book-a-session-datetime-picker-container-week').removeClass('book-a-session-hidden');
    
                                            var $date_string = datetime_picker.timetable_array[ $('input[data-frontend-date]:checked').attr("data-frontend-date") ][ $('input[name="schedule_id"]').first().attr("value") ].date;
    
                                            $('.book-a-session-datetime-picker-container-schedule').prepend('<div class="book-a-session-datetime-picker-label book-a-session-datetime-picker-label-schedule">Pick a time for <span class="book-a-session-datetime-picker-label book-a-session-datetime-picker-label-date">' + $date_string + '</span></div>');
                                            
                                            $('.book-a-session-frontend-section-datetime .book-a-session-bottom-bar .datetime-bottom');
    
                                            // Session tab events
    
                                            if ( chosen_quantity > 1 ) {
    
                                                // If a date is selected
    
                                                $('input[data-frontend-date]').on("change", function(){ 
    
                                                    // Check if both a date and a session time are selected (they shouldn't be by design, take a look at this later)
    
                                                    if ( $('input[data-frontend-date]:checked').length != 0 && $('input[name="schedule_id"]:checked').length != 0 ) {
    
                                                        // If so, show the checkmark animation, save the date and time in the session values array and remove the invalid class from the session tab button
    
                                                        $('[data-session-tab].book-a-session-tab-button-current svg').addClass('checkmark').removeClass('book-a-session-hidden');
    
                                                        session_values[ $('[data-session-tab].book-a-session-tab-button-current').attr("data-session-tab") ] = { date: $('input[data-frontend-date]:checked').attr("data-frontend-date"), schedule: $('input[name="schedule_id"]:checked').attr("value") }
    
                                                        $('[data-session-tab].book-a-session-tab-button-current').addClass('book-a-session-valid').removeClass('book-a-session-invalid');
    
                                                    } else {
    
                                                        // Otherwise save the date only and ensure the session tab button is still invalid
    
                                                        session_values[ $('[data-session-tab].book-a-session-tab-button-current').attr("data-session-tab") ].date = $('input[data-frontend-date]:checked').attr("data-frontend-date");
    
                                                        $('[data-session-tab].book-a-session-tab-button-current').addClass('book-a-session-invalid').removeClass('book-a-session-valid');
    
                                                    }
    
                                                    // Update bottom bar text for date only
    
                                                    $('.book-a-session-frontend-section-datetime .book-a-session-frontend-bottom-bar p').addClass("book-a-session-hidden");
    
                                                    var date_string = datetime_picker.timetable_array[ $('input[data-frontend-date]:checked').attr("data-frontend-date") ][ $('input[name="schedule_id"]').first().attr("value") ].date;
                                                    var current_session_tab = $('[data-session-tab].book-a-session-tab-button-current').attr("data-session-tab");
    
                                                    setTimeout(function(){
    
                                                        $('.book-a-session-frontend-section-datetime .book-a-session-frontend-bottom-bar p')
                                                        .html( "Session " + current_session_tab + " of " + chosen_quantity + " will be for <span class='book-a-session-datetime-picker-label book-a-session-datetime-picker-label-date'>" + date_string + "</span>, but at what time?" ).removeClass('book-a-session-hidden');
    
                                                    },200);
    
                                                });
    
                                                // If a session time is selected
    
                                                $('input[name="schedule_id"]').on("change", function(){ 
    
                                                    // Check if both a date and a session time are selected
    
                                                    if ( $('input[data-frontend-date]:checked').length != 0 && $('input[name="schedule_id"]:checked').length != 0 ) {
    
                                                        // If so, show the checkmark animation, save the date and time in the session values array and remove the invalid class from the session tab button
    
                                                        $('[data-session-tab].book-a-session-tab-button-current svg').addClass('checkmark').removeClass('book-a-session-hidden');
    
                                                        session_values[ $('[data-session-tab].book-a-session-tab-button-current').attr("data-session-tab") ] = { date: $('input[data-frontend-date]:checked').attr("data-frontend-date"), schedule: $('input[name="schedule_id"]:checked').attr("value") }
    
                                                        $('[data-session-tab].book-a-session-tab-button-current').addClass('book-a-session-valid').removeClass('book-a-session-invalid');
                                                        
                                                        // Update bottom bar text for date and time
    
                                                        $('.book-a-session-frontend-section-datetime .book-a-session-frontend-bottom-bar p').addClass("book-a-session-hidden");
    
                                                        var date_string = datetime_picker.timetable_array[ $('input[data-frontend-date]:checked').attr("data-frontend-date") ][ $('input[name="schedule_id"]').first().attr("value") ].date;
                                                        var time_string = datetime_picker.timetable_array[ $('input[data-frontend-date]:checked').attr("data-frontend-date") ][ $('input[name="schedule_id"]:checked').attr("value") ].time;
                                                        var current_session_tab = $('[data-session-tab].book-a-session-tab-button-current').attr("data-session-tab");
    
                                                        setTimeout(function(){
    
                                                            $('.book-a-session-frontend-section-datetime .book-a-session-frontend-bottom-bar p')
                                                            .html( "Session " + current_session_tab + " of " + chosen_quantity + " is for <span class='book-a-session-datetime-picker-label book-a-session-datetime-picker-label-date'>" + date_string + "</span> at <span class='book-a-session-datetime-picker-label book-a-session-datetime-picker-label-time'>" + time_string + "</span>. Click next to continue." ).removeClass('book-a-session-hidden');
    
                                                        },200);
    
                                                    } else {
    
                                                        // Though this shouldn't really happen, for consistency, this code block is present. If a session time has been picked but a date hasn't, save the session time only and ensure the session tab button is still invalid.
    
                                                        session_values[ $('[data-session-tab].book-a-session-tab-button-current').attr("data-session-tab") ].schedule = $('input[name="schedule_id"]:checked').attr("value");
    
                                                        $('[data-session-tab].book-a-session-tab-button-current').addClass('book-a-session-invalid').removeClass('book-a-session-valid');
    
                                                    }
    
                                                });
    
                                                $('.book-a-session-session-tabs a[data-session-tab]').click(function(){
    
                                                    var $date = session_values[ $(this).attr("data-session-tab") ].date;
                                                    var $schedule = session_values[ $(this).attr("data-session-tab") ].schedule !== "0" ? session_values[ $(this).attr("data-session-tab") ].schedule : false;
    
                                                    $content = $(this).attr("data-session-tab");
        
                                                    $('.book-a-session-frontend-modal [data-session-tab]').removeClass('book-a-session-tab-button-current');
                                                    
                                                    $(this).addClass('book-a-session-tab-button-current');
                                                    
                                                    $('.book-a-session-datetime-picker-container-schedule-wrapper').addClass('book-a-session-hidden');
                                                    $('.book-a-session-datetime-picker-container').addClass('book-a-session-hidden');
                                                    $('.book-a-session-frontend-section-datetime .book-a-session-frontend-bottom-bar p').addClass("book-a-session-hidden");
    
                                                    $('.book-a-session-frontend-date label').removeClass('book-a-session-animated');
                                                    $('.book-a-session-session-time label').removeClass('book-a-session-animated');
        
                                                    setTimeout(function(){ 
    
                                                        // Change radio states
    
                                                        if ( $date ) { 
    
                                                            $('input[data-frontend-date="' + $date + '"]').prop("checked", true);
    
                                                            // Get the correct schedule, making changes immediately
                
                                                            var schedule = datetime_picker.timetable_array[ $date ];
            
                                                            // Change date title
            
                                                            $date_string = schedule[ $('input[name="schedule_id"]').first().attr("value") ].date;
                                                            
                                                            $('.book-a-session-datetime-picker-container-schedule .book-a-session-datetime-picker-label-schedule').html('Pick a time for <span class="book-a-session-datetime-picker-label book-a-session-datetime-picker-label-date">' + $date_string + "</span>");
                                                            
                                                            // Change radio states
            
                                                            $('input[name="schedule_id"]').each(function(){
    
                                                                var this_schedule = $(this).attr("value");
                                                                var entry = schedule[ $(this).attr("value") ];
    
                                                                // Loop through the location array, which has an entry for each service indexed by the service ID, and for each service, an indexed array with JSON objects, each for a location that is accepted for the service. One of those object properties is called schedule, which is either an empty indexed array (which means no session times are accepted for this location) or an indexed array of JSON entries, each with a schedule ID property and value. If, say, location_array[1][0].schedule[0].schedule_id === "2", a schedule id 2 is accepted. If there isn't a schedule[i].schedule_id which equals "2" in the array, that scheduled time is not accepted by the location
                                                                
                                                                var session_time_accepted = false;
    
                                                                $.each( location_array[ chosen_service ], function( key, value ){
    
                                                                    $.each( value.schedule, function( accepted_schedule_key, accepted_schedule_value ) {
    
                                                                        if ( value.id == chosen_location && accepted_schedule_value.schedule_id == this_schedule ) {
    
                                                                            session_time_accepted = true;
    
                                                                            return false;
    
                                                                        }
    
                                                                    });
    
                                                                    if ( session_time_accepted ) {
    
                                                                        return false;
    
                                                                    }
    
                                                                });
    
                                                                $(this).parent().removeClass('book-a-session-session-time-unavailable-selected');
            
                                                                if ( ! entry.unavailable && ! entry.order_id && session_time_accepted ) {
            
                                                                    $(this).parent().addClass('book-a-session-session-time-available').removeClass('book-a-session-session-time-unavailable');
                                                                    $(this).prop("disabled", false);
                    
                                                                } else if ( entry.unavailable || ! session_time_accepted ) {
                    
                                                                    $(this).parent().removeClass('book-a-session-session-time-available').addClass('book-a-session-session-time-unavailable');
                                                                    $(this).prop("disabled", true);
                    
                                                                } else if ( entry.order_id ) {
                    
                                                                    $(this).parent().removeClass('book-a-session-session-time-available').addClass('book-a-session-session-time-unavailable');
                                                                    $(this).prop("disabled", true);
                    
                                                                }
    
                                                                // Check other session values
    
                                                                $.each( session_values, function( key, value ) {
    
                                                                    if ( $date === value.date && this_schedule === value.schedule && value.schedule !== $schedule ) {
    
                                                                        $('input[name="schedule_id"][value="' + value.schedule + '"]').parent().removeClass('book-a-session-session-time-available').addClass('book-a-session-session-time-unavailable').addClass('book-a-session-session-time-unavailable-selected');
                                                                        $('input[name="schedule_id"][value="' + value.schedule + '"]').prop("disabled", true);
    
                                                                    }
    
                                                                });
                    
                                                            });   
                                                            
                                                        } else {
    
                                                            $('input[data-frontend-date]').prop("checked", false);
                                                            $('.book-a-session-datetime-picker-date-future:not(.book-a-session-datetime-picker-date-today):not(.book-a-session-date-unavailable)').first().find("input[data-frontend-date]").prop("checked", true);
    
                                                        }
                                                        
                                                        if ( $schedule ) {
    
                                                            $('input#schedule_' + $schedule).prop("checked", true);
    
                                                        } else {
    
                                                            $('input[name="schedule_id"]').prop("checked", false);
    
                                                        }
    
                                                        if ( $date && $schedule ) {
    
                                                            // Update bottom bar text for date and time
    
                                                            var date_string = datetime_picker.timetable_array[ $('input[data-frontend-date]:checked').attr("data-frontend-date") ][ $('input[name="schedule_id"]').first().attr("value") ].date;
                                                            var time_string = datetime_picker.timetable_array[ $('input[data-frontend-date]:checked').attr("data-frontend-date") ][ $('input[name="schedule_id"]:checked').attr("value") ].time;
                                                            var current_session_tab = $('[data-session-tab].book-a-session-tab-button-current').attr("data-session-tab");
    
                                                            $('.book-a-session-frontend-section-datetime .book-a-session-frontend-bottom-bar p')
                                                            .html( "Session " + current_session_tab + " of " + chosen_quantity + " is for " + date_string + " at " + time_string + ". Click next to continue." ).removeClass('book-a-session-hidden');
    
                                                        } else if ( $date ) {
    
                                                            // Update bottom bar text for date only
    
                                                            var date_string = datetime_picker.timetable_array[ $('input[data-frontend-date]:checked').attr("data-frontend-date") ][ $('input[name="schedule_id"]').first().attr("value") ].date;
                                                            var current_session_tab = $('[data-session-tab].book-a-session-tab-button-current').attr("data-session-tab");
    
                                                            $('.book-a-session-frontend-section-datetime .book-a-session-frontend-bottom-bar p')
                                                            .html( "Session " + current_session_tab + " of " + chosen_quantity + " will be for <span class='book-a-session-datetime-picker-label book-a-session-datetime-picker-label-date'>" + date_string + "</span>, but at what time?" ).removeClass('book-a-session-hidden');
    
                                                        }
    
                                                        setTimeout(function(){
    
                                                            // Replace the animation class afterwards for continued transitions as usual
    
                                                            $('.book-a-session-frontend-date label').addClass('book-a-session-animated');
                                                            $('.book-a-session-session-time label').addClass('book-a-session-animated');
    
                                                        },200);
    
                                                        $('.book-a-session-datetime-picker-container-schedule-wrapper').removeClass('book-a-session-hidden');
                                                        $('.book-a-session-datetime-picker-container').removeClass('book-a-session-hidden');
    
                                                    },200);
                                                
                                                });    
    
                                                $('.book-a-session-frontend-section-datetime .book-a-session-frontend-bottom-bar a.book-a-session-circle-button').on("click", function(){
    
                                                    if ( $("[data-session-tab].book-a-session-invalid").length > 0 ) {
    
                                                        if ( ! $("[data-session-tab].book-a-session-invalid").first().hasClass('book-a-session-tab-button-current') ) {
    
                                                            $("[data-session-tab].book-a-session-invalid").first().click();
    
                                                        } else {
    
                                                            // Staggered shake animation to tell the user to pick the date and time
    
                                                            $('.book-a-session-datetime-picker-container-week:not(.book-a-session-hidden) .book-a-session-datetime-picker-label.book-a-session-datetime-picker-label-month').addClass('book-a-session-animated')
                                                            .css({ "transform": "translateX(15px)" });
    
                                                            setTimeout(function(){
    
                                                                $('.book-a-session-datetime-picker-label.book-a-session-datetime-picker-label-schedule').addClass('book-a-session-animated')
                                                                .css({ "transform": "translateX(15px)" });    
    
                                                            },100);
    
                                                            setTimeout(function(){
    
                                                                $('.book-a-session-datetime-picker-container-week:not(.book-a-session-hidden) .book-a-session-datetime-picker-label.book-a-session-datetime-picker-label-month')
                                                                .css({ "transform": "translateX(0px)" });
    
                                                                setTimeout(function(){
    
                                                                    $('.book-a-session-datetime-picker-label.book-a-session-datetime-picker-label-schedule')
                                                                    .css({ "transform": "translateX(0px)" });
                                                                
                                                                },100);
    
                                                            },100);
    
                                                        }
    
                                                    } else {
    
                                                        // Go to payment section
                                
                                                        book_a_session_reveal_inline_loader( $(e.currentTarget) );
    
                                                        book_a_session_update_payment_methods( session_values );
                        
                                                        $('.book-a-session-frontend-modal .book-a-session-frontend-section').addClass('book-a-session-hidden');
                                                        $(this).parents('.book-a-session-frontend-section').next('.book-a-session-frontend-section').removeClass('book-a-session-hidden');
                        
                                                        $('.book-a-session-progress-tab-container.book-a-session-tab-button-container .book-a-session-tab-button.book-a-session-tab-button-current').removeClass('book-a-session-tab-button-current')
                                                        .next('.book-a-session-tab-button').addClass('book-a-session-tab-button-current').removeClass('book-a-session-tab-button-inactive');
                                                        
                                                        $('.book-a-session-frontend-modal .book-a-session-frontend-section-container').css({ "height":  $(this).parents('.book-a-session-frontend-section').next('.book-a-session-frontend-section').outerHeight() + "px" })
    
                                                        book_a_session_hide_inline_loader( $(e.currentTarget) );
    
                                                    }
    
                                                });
    
                                            } else {
    
                                                // When there's just one session in the bag (denoted by the selector's schedule-wrapper being directly before the bottom bar, where otherwise a session tab element would be there) and the datetime next button was clicked, check to see if both date and time have been picked before moving to the next section
    
                                                $('.book-a-session-frontend-section-datetime .book-a-session-datetime-picker-container-schedule-wrapper + .book-a-session-frontend-bottom-bar a.book-a-session-circle-button').on("click", function(){
    
                                                    if ( $("input[name='date']:checked").length > 0 && $("input[name='schedule_id']:checked").length > 0 )  {
    
                                                        // Go to payment section
                                                        
                                                        if ( ! session_values ) var session_values = {};
    
                                                        book_a_session_update_payment_methods( session_values );
                        
                                                        $('.book-a-session-frontend-modal .book-a-session-frontend-section').addClass('book-a-session-hidden');
                                                        $(this).parents('.book-a-session-frontend-section').next('.book-a-session-frontend-section').removeClass('book-a-session-hidden');
                        
                                                        $('.book-a-session-progress-tab-container.book-a-session-tab-button-container .book-a-session-tab-button.book-a-session-tab-button-current').removeClass('book-a-session-tab-button-current')
                                                        .next('.book-a-session-tab-button').addClass('book-a-session-tab-button-current').removeClass('book-a-session-tab-button-inactive');
                                                        
                                                        $('.book-a-session-frontend-modal .book-a-session-frontend-section-container').css({ "height":  $(this).parents('.book-a-session-frontend-section').next('.book-a-session-frontend-section').outerHeight() + "px" })
    
                                                    } else {
    
                                                        // Staggered shake animation to tell the user to pick the date and time
    
                                                        $('.book-a-session-datetime-picker-container-week:not(.book-a-session-hidden) .book-a-session-datetime-picker-label.book-a-session-datetime-picker-label-month').addClass('book-a-session-animated')
                                                        .css({ "transform": "translateX(15px)" });
    
                                                        setTimeout(function(){
    
                                                            $('.book-a-session-datetime-picker-label.book-a-session-datetime-picker-label-schedule').addClass('book-a-session-animated')
                                                            .css({ "transform": "translateX(15px)" });    
    
                                                        },100);
    
                                                        setTimeout(function(){
    
                                                            $('.book-a-session-datetime-picker-container-week:not(.book-a-session-hidden) .book-a-session-datetime-picker-label.book-a-session-datetime-picker-label-month')
                                                            .css({ "transform": "translateX(0px)" });
    
                                                            setTimeout(function(){
    
                                                                $('.book-a-session-datetime-picker-label.book-a-session-datetime-picker-label-schedule')
                                                                .css({ "transform": "translateX(0px)" });
                                                            
                                                            },100);
    
                                                        },100);
    
                                                    }
                                            
                                                });
                        
                                            }
    
                                            // Initialise the date scroller. First we find the first available date and find out whether or not it's in the first week. Then we get the width we need to scroll and the number of weeks to power the scrolling mechanism.
    
                                            var starting_week = $('.book-a-session-datetime-picker-date-future:not(.book-a-session-datetime-picker-date-today):not(.book-a-session-date-unavailable)').first().parents('.book-a-session-datetime-picker-container-week').index();
                                            var $datetime_week_width = $('.book-a-session-datetime-picker-container-week').eq(0).outerWidth();
                                            var $datetime_week_count = $('.book-a-session-datetime-picker-container-week').length;                                        
    
                                            // Set the data on the DOM element that holds all the week containers. jQuery .data() was chosen because it provided a straightforward way of linking data to DOM elements, so in the case we would have gone forward with having multiple date scrollers (we chose to only use one for performance reasons), each would have its scrolling data attached to it. This can be safely moved to a variable though that would be unnecessary now. 
    
                                            $('.book-a-session-datetime-picker-week-container').data( "scroll", { weekCounter: ( starting_week + 1 ), scrollTracker: -Math.abs( starting_week * $datetime_week_width  ) } );
    
                                            // All dates are loaded via PHP which are converted to the user's GEOIP determined region timezone, but dates are not checked against the location's accepted daily and weekly schedule, that's done earlier in this script asynchronously.
    
                                            if ( starting_week > 0 ) {
    
                                                // So now that we have the true first available date, and it turns out not to be in the first week loaded via PHP, here we scroll to the correct week and act as though the weeks before the first available date don't exist, though they're not removed from the DOM.
    
                                                $('.book-a-session-datetime-picker-week-container').css({ "transform": "translateX( " + ( -Math.abs( $('.book-a-session-datetime-picker-week-container').data( "scroll" ).scrollTracker ) ) + "px )" })
    
                                            }
    
                                            // When we scroll forward through the weeks, the container holding the weeks is translated to move to the left (minus X pixels), and the week we're looking for is revealed while others are left hidden.
    
                                            $('.book-a-session-datetime-picker-container .book-a-session-next-button').click(function(){
    
                                                // Get correct data
    
                                                $datetime_week_container = $(this).prev('.book-a-session-datetime-picker-wrap').find('.book-a-session-datetime-picker-week-container');
                                                $datetime_date_container = $datetime_week_container.find('.book-a-session-datetime-picker-container-week');
                                                $datetime_scroll_tracker = $datetime_week_container.data( "scroll" ).scrollTracker;
                                                $datetime_week_counter = $datetime_week_container.data( "scroll" ).weekCounter;
    
                                                // Reduce scroll tracker by the container width, thus giving us a value to apply for transform: translateX(), and increment the week counter to refer to the correct week we want to reveal
    
                                                $datetime_scroll_tracker -= $datetime_week_width;
                                                $datetime_week_counter++;
    
                                                // Move the container
                                                
                                                $datetime_week_container.css({ "transform": "translateX( " + (  $datetime_scroll_tracker ) + "px )" });
    
                                                // Hide all weeks and only show the next week
    
                                                $datetime_date_container.addClass('book-a-session-hidden');
                                                $datetime_week_container.find('.book-a-session-datetime-picker-week-' + $datetime_week_counter ).removeClass('book-a-session-hidden');
    
                                                // Update the data to the DOM element
    
                                                $datetime_week_container.data( "scroll", { weekCounter: $datetime_week_counter, scrollTracker: $datetime_scroll_tracker } );
    
                                                // Reveal prev button when needed. The expression -Math.abs( starting_week * $datetime_week_width ) ensures the previous button is hidden when there are no previous weeks to show and when we don't want to show any weeks loaded before the first available date 
    
                                                if ( $datetime_scroll_tracker <= -Math.abs( starting_week * $datetime_week_width ) ) $(this).siblings('.book-a-session-prev-button').removeClass( 'book-a-session-hidden' );
    
                                                // Hide next button when the scroll tracker is as far as it should go (or further, in the case of a bug)
    
                                                if ( $datetime_scroll_tracker <= -$datetime_week_width * ( $datetime_week_count - 1 ) ) $(this).addClass( 'book-a-session-hidden' );
    
                                            });
    
                                            $('.book-a-session-datetime-picker-container .book-a-session-prev-button').click(function(){
    
                                                // Get correct data
    
                                                $datetime_week_container = $(this).next('.book-a-session-datetime-picker-wrap').find('.book-a-session-datetime-picker-week-container');
                                                $datetime_date_container = $datetime_week_container.find('.book-a-session-datetime-picker-container-week');
                                                $datetime_scroll_tracker = $datetime_week_container.data( "scroll" ).scrollTracker;
                                                $datetime_week_counter = $datetime_week_container.data( "scroll" ).weekCounter;
    
                                                // Increase scroll tracker by the container width, thus giving us a value to apply for transform: translateX(), and increment the week counter to refer to the correct week we want to reveal
    
                                                $datetime_scroll_tracker += $datetime_week_width;
                                                $datetime_week_counter--;
    
                                                // Move the container
                                                
                                                $datetime_week_container.css({ "transform": "translateX( " + (  $datetime_scroll_tracker ) + "px )" });
    
                                                // Hide all weeks and only show the previous week
    
                                                $datetime_date_container.addClass('book-a-session-hidden');
                                                $datetime_week_container.find('.book-a-session-datetime-picker-week-' + $datetime_week_counter ).removeClass('book-a-session-hidden');
    
                                                // Update the data to the DOM element
    
                                                $datetime_week_container.data( "scroll", { weekCounter: $datetime_week_counter, scrollTracker: $datetime_scroll_tracker } );
    
                                                // Reveal next button when it was hidden because we've shown the final week, and we've moved away from the final week
    
                                                if ( $datetime_scroll_tracker >= - $datetime_week_width * $datetime_week_count ) $(this).siblings('.book-a-session-next-button').removeClass( 'book-a-session-hidden' );
    
                                                // Hide prev button when there are no previous weeks to show and when we don't want to show any weeks loaded before the first available date
    
                                                if ( $datetime_scroll_tracker >= -Math.abs( starting_week * $datetime_week_width  ) ) $(this).addClass( 'book-a-session-hidden' );
    
                                            });
    
                                            // Invalidate tab button progress if the a new choice was made that would need to load a new date time picker, or revalidate if the previous choice was made again and datetime picker radios are still checked and valid
    
                                            $('.book-a-session-datetime-picker-prerequisite input[type="radio"]').on("change", function(){
    
                                                if ( $('.book-a-session-frontend-section-datetime').data( "getParameters" ).quantity != chosen_quantity || $('.book-a-session-frontend-section-datetime').data( "getParameters" ).location != chosen_location || $('.book-a-session-frontend-section-datetime').data( "getParameters" ).practitioner != chosen_practitioner ) {
                                                
                                                    $('.book-a-session-progress-tab-container .book-a-session-tab-button').each(function(){
    
                                                        // Get the position of each button. If it appears after the load datetime picker tab (as in it's after the Date & Time tab button), give it an inactive class if it doesn't already have it
    
                                                        var tab_position = $(this).index();
    
                                                        if ( tab_position > $('.book-a-session-progress-tab-container .book-a-session-tab-button').index( $('.book-a-session-tab-button.book-a-session-load-datetime-picker') ) && ! $(this).hasClass('book-a-session-tab-button-inactive') ) {
    
                                                            $(this).addClass('book-a-session-tab-button-inactive');
    
                                                        }
    
                                                    });
                                                
                                                } else if ( $('.book-a-session-frontend-section-datetime').data( "getParameters" ).quantity     == chosen_quantity      && 
                                                            $('.book-a-session-frontend-section-datetime').data( "getParameters" ).location     == chosen_location      && 
                                                            $('.book-a-session-frontend-section-datetime').data( "getParameters" ).practitioner == chosen_practitioner  && 
                                                            $('[data-session-tab].book-a-session-invalid').length                               == 0                    &&
                                                            $('input[name="date"]:checked').length                                              != 0                    &&
                                                            $('input[name="schedule_id"]:checked').length                                       != 0                    ){
        
                                                    $('.book-a-session-progress-tab-container .book-a-session-tab-button').each(function(){
    
                                                        // Get the position of each button. If it appears after the load datetime picker tab (as in it's after the Date & Time tab button), remove its inactive class if it has one
    
                                                        var tab_position = $(this).index();
    
                                                        if ( tab_position > $('.book-a-session-progress-tab-container .book-a-session-tab-button').index( $('.book-a-session-tab-button.book-a-session-load-datetime-picker') ) && $(this).hasClass('book-a-session-tab-button-inactive') ) {
    
                                                            $(this).removeClass('book-a-session-tab-button-inactive');
    
                                                        }
    
                                                    });
    
                                                }
                                            });
    
                                            // Reveal datetime picker
    
                                            if ( $(e.currentTarget).hasClass('book-a-session-tab-button') && $(e.currentTarget).attr("data-progress-tab-content") ) {
    
                                                // Via tab button
    
                                                $content = $(e.currentTarget).attr("data-progress-tab-content");
    
                                                $('.book-a-session-frontend-modal [data-progress-tab-content]').removeClass('book-a-session-tab-button-current');
                    
                                                $(e.currentTarget).addClass('book-a-session-tab-button-current');
                    
                                                $('.book-a-session-frontend-modal .book-a-session-frontend-section').addClass('book-a-session-hidden');
                                                $('.book-a-session-frontend-modal .book-a-session-frontend-section-' + $content).removeClass('book-a-session-hidden');
                
                                                $('.book-a-session-frontend-modal .book-a-session-frontend-section-container').css({ "height":  $('.book-a-session-frontend-modal .book-a-session-frontend-section-' + $content).outerHeight() + "px" });
    
                                            } else if ( $(e.currentTarget).hasClass('book-a-session-circle-button') ) {
    
                                                // Via next button
    
                                                $('.book-a-session-frontend-modal .book-a-session-frontend-section').addClass('book-a-session-hidden');
                                                $(e.currentTarget).parents('.book-a-session-frontend-section').next('.book-a-session-frontend-section').removeClass('book-a-session-hidden');
                
                                                $('.book-a-session-progress-tab-container.book-a-session-tab-button-container .book-a-session-tab-button.book-a-session-tab-button-current').removeClass('book-a-session-tab-button-current')
                                                .next('.book-a-session-tab-button').addClass('book-a-session-tab-button-current').removeClass('book-a-session-tab-button-inactive');
                                                
                                                $('.book-a-session-frontend-modal .book-a-session-frontend-section-container').css({ "height":  $(e.currentTarget).parents('.book-a-session-frontend-section').next('.book-a-session-frontend-section').outerHeight() + "px" })                            
                                                
                                            }
    
                                            setTimeout(function(){
    
                                                book_a_session_hide_inline_loader( $(e.currentTarget) );
    
                                            },200);
    
                                        }).fail( function(){
                                                    
                                            book_a_session_reveal_fixed_notice( "Sorry, we couldn't load any dates and times for this combination of choices. Please try again.", "error", "dismissable" );
    
                                            book_a_session_hide_inline_loader( $(e.currentTarget) );
    
            
                                        });
    
                                    } else if ( ! $(e.currentTarget).hasClass('book-a-session-tab-button-inactive') ) {
    
                                        // Reveal a previous datetime picker without loading new data
    
                                        if ( $(e.currentTarget).hasClass('book-a-session-tab-button') && $(e.currentTarget).attr("data-progress-tab-content") ) {
    
                                            // Via tab button
    
                                            $content = $(e.currentTarget).attr("data-progress-tab-content");
    
                                            $('.book-a-session-frontend-modal [data-progress-tab-content]').removeClass('book-a-session-tab-button-current');
                
                                            $(e.currentTarget).addClass('book-a-session-tab-button-current');
                
                                            $('.book-a-session-frontend-modal .book-a-session-frontend-section').addClass('book-a-session-hidden');
                                            $('.book-a-session-frontend-modal .book-a-session-frontend-section-' + $content).removeClass('book-a-session-hidden');
    
                                            $('.book-a-session-frontend-modal .book-a-session-frontend-section-container').css({ "height":  $('.book-a-session-frontend-modal .book-a-session-frontend-section-' + $content).outerHeight() + "px" });
            
                                        } else if ( $(e.currentTarget).hasClass('book-a-session-circle-button') ) {
    
                                            // Via next button
    
                                            $('.book-a-session-frontend-modal .book-a-session-frontend-section').addClass('book-a-session-hidden');
                                            $(e.currentTarget).parents('.book-a-session-frontend-section').next('.book-a-session-frontend-section').removeClass('book-a-session-hidden');
            
                                            $('.book-a-session-progress-tab-container.book-a-session-tab-button-container .book-a-session-tab-button.book-a-session-tab-button-current').removeClass('book-a-session-tab-button-current')
                                            .next('.book-a-session-tab-button').addClass('book-a-session-tab-button-current').removeClass('book-a-session-tab-button-inactive');
    
                                            $('.book-a-session-frontend-modal .book-a-session-frontend-section-container').css({ "height":  $(e.currentTarget).parents('.book-a-session-frontend-section').next('.book-a-session-frontend-section').outerHeight() + "px" })                            
    
                                        }
    
                                    }
    
                                }
    
                            });
    
                            // Check first radios by default
    
                            $('.book-a-session-frontend-modal .book-a-session-frontend-option:first-of-type input[type="radio"]').prop("checked", true).change();
    
                            book_a_session_hide_loader();
                            book_a_session_reveal_modal();
    
                        }).fail( function() {
    
                            book_a_session_hide_loader();
                            book_a_session_reveal_fixed_notice( "Sorry, we couldn't load our bundles.", "error", "dismissable" );
    
                        }); // Bundles AJAX Auto
        
                    }).fail( function() {
    
                        book_a_session_hide_loader();
                        book_a_session_reveal_fixed_notice( "Sorry, we couldn't load this service.", "error", "dismissable" );
    
    
                    }); // Service AJAX Click

                }
    
            });
    
        }

    }

    book_a_session_frontend();
    book_a_session_dashboard();
    book_a_session_week_timetable();
    book_a_session_calendar_timetable();

    /*
    $(window).resize( function() {

        book_a_session_week_timetable();
        book_a_session_calendar_timetable();
    
    });
    */

});