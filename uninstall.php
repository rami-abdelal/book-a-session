<?php // Book A Session - Uninstall. Delete all custom post types, tables, and such.

// Exit if file is called directly

if ( ! defined( 'ABSPATH' ) ) {
    
    exit;
    
}

// Delete Region Table

    // ID
    // International
    // Name
    // Description
    // Image

// Delete Service Type Table

    // ID
    // Service Type Name: 'Coaching'
    // Service Type slug: 'coaching'
    // Service Practitioner Title: 'Coach'
    // Service Practitioner Slug: 'coach' 

// Delete Payment Method Table

    // ID
    // Name

// Delete Practitioners

function book_a_session_remove_practitioner_role() {
    
    // remove_role( $role, $display_name, $capabilities );
    
    // Remove Custom Field Service Type
    
    // Remove Custom Field Description
    
    // Remove Custom Option Accept Cash
    
    // Remove Custom Option Accept PayPal
    
    // Remove Custom Option Accept VFC
    
}

// Delete Custom Post Types

    // Delete Locations Custom Post Type

        // ID
        // Region ID
        // Name
        // Address Line 1
        // Address Line 2
        // Address Line 3
        // City
        // Country
        // Postcode
        // Coordinates
        // Public Address: 0, 1
        // Accept Cash: 0, 1
        // Accept PayPal: 0, 1
        // Accept VFC: 0, 1

    // Delete Services Custom Post Types

        // ID
        // Service Type ID
        // Title
        // Description
        // Image

// Delete Schedule Table

    // ID
    // Start Time: 11:00
    // End Time: 12:00

// Delete Currency Table

    // ID
    // Currency Code: EGP
    // Currency Name: Egyptian Pounds
    // Currency Symbol: £
    // Label After: 0, 1
    // Use Alternative Label: 0,1
    // Alternative Label: 'LE'
    // Currency Active: 0, 1
    // Accept Cash: 0, 1
    // Accept PayPal: 0, 1
    // Accept VFC: 0, 1

// Delete Service Price Table

    // Service ID
    // Location ID
    // Bundle Quantity : 1,4,6,8
    // Currency ID:
    // Price per Session
    // Accept Cash: 0, 1
    // Accept PayPal: 0, 1
    // Accept VFC: 0, 1
    
// Delete Pracitioner Location Table

    // Practitioner ID
    // Location ID
    // Accepted: 0, 1

// Delete Location Schedule Table

    // Location ID
    // Schedule ID
        
// Delete Location Timetable Table

    // Sesssion ID
    // Date
    // Location Schedule ID
    // Availability: 'Available', 'Unavailable'
    // Currency ID
    // Cash Due: Amount
    // VFC Due: Amount

// Delete Practitioner Schedule Table

    // Practitioner ID
    // Schedule ID

// Delete Practitioner Timetable Table
    
    // Session ID
    // Date
    // Practitioner Schedule ID
    // Availability: 'Available', 'Unavailable'
    // Currency ID
    // Cash Due: Amount
    // VFC Due: Amount

// Delete Booking Table

    // Client Date
    // Client Time
    // Session Date
    // Schedule ID
    // Booking ID: Client Date + Client Time + User ID
    // Session ID: Booking ID + Session Date + Schedule ID 
    // Service ID
    // Service Price ID
    // Practitioner ID
    // User ID
    // Location ID
    // Location Price ID
    // Session Total
    // Payment method: PP, DC/CC, VFC, Cash
    // VFC Mobile Number: If VFC
    // Booked: 0, 1
    // Completed: 0, 1
    // No Show Client: 0, 1
    // No Show Practitioner: 0, 1
    // Cash Paid: 0, 1
    // VFC Paid: 0, 1
    // Note

// Delete Invoice Table

    // ID
    // Booking ID
    // Type: Cash Invoice, VFC Invoice
    // VFC Mobile Number
    // Total Due
    // Grand Total

// Delete Receipt Table
    
    // Invoice ID
    // Payment ID
    // Bundle ID
    // Total
    
// Delete Payment Table

