<?php
/*
Plugin Name: Book A Session
Description: order, payments and invoicing. Add, remove and modify service types, services, practitioners, locations, session times, currencies, payment methods and more to generate orders, timetables, payments and invoices.
Plugin URI: https://rami-abdelal.co.uk
Author: Rami Abdelal
Version: 1.0.1
*/

// Exit if file is called directly

if ( ! defined( 'ABSPATH' ) ) {
    
    exit;
    
}

// Intitialise main functionality

require_once plugin_dir_path( __FILE__ ) . 'includes/activation.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/default-options.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/core-functionality.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/rest-api.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/shortcodes.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/dashboard.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/frontend.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/time.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/timetable.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/php-mailer.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/invoices.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/paypal.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/stripe.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/create-order.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/login-register.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/services.php';

function book_a_session_init_admin_scripts() {

    wp_enqueue_script( 'book_a_session_admin_js', plugin_dir_url( __FILE__ ) . 'admin/js/script.js', array( 'jquery' ), null, true );
    wp_register_style( 'book_a_session_admin_css',  plugin_dir_url( __FILE__ ) . 'admin/css/style.css');
    wp_enqueue_style( 'book_a_session_admin_css');

}

function book_a_session_init() {

    // Initialise admin menu and settings page if user is an admin

    if ( is_admin() ) {
        
        // Include dependencies
        
        require_once plugin_dir_path( __FILE__ ) . 'admin/admin-menu.php';
        require_once plugin_dir_path( __FILE__ ) . 'admin/admin-notices.php';
        require_once plugin_dir_path( __FILE__ ) . 'admin/settings-register.php';
        require_once plugin_dir_path( __FILE__ ) . 'admin/settings-callbacks.php';
        require_once plugin_dir_path( __FILE__ ) . 'admin/settings-orders.php';
        require_once plugin_dir_path( __FILE__ ) . 'admin/settings-currencies.php';
        require_once plugin_dir_path( __FILE__ ) . 'admin/settings-general.php';
        require_once plugin_dir_path( __FILE__ ) . 'admin/settings-invoices.php';
        require_once plugin_dir_path( __FILE__ ) . 'admin/settings-locations.php';
        require_once plugin_dir_path( __FILE__ ) . 'admin/settings-payment-methods.php';
        require_once plugin_dir_path( __FILE__ ) . 'admin/settings-payments.php';
        require_once plugin_dir_path( __FILE__ ) . 'admin/settings-practitioners.php';
        require_once plugin_dir_path( __FILE__ ) . 'admin/settings-regions.php';
        require_once plugin_dir_path( __FILE__ ) . 'admin/settings-register.php';
        require_once plugin_dir_path( __FILE__ ) . 'admin/settings-schedule.php';
        require_once plugin_dir_path( __FILE__ ) . 'admin/settings-services.php';
        require_once plugin_dir_path( __FILE__ ) . 'admin/settings-bundles.php';
        require_once plugin_dir_path( __FILE__ ) . 'admin/settings-service-types.php';
        // require_once plugin_dir_path( __FILE__ ) . 'admin/settings-bundle-currencies.php';
    
    }

    // Initialise PHPMailer
    
    book_a_session_php_mailer();

    // Actions

    add_action( "lost_password", "book_a_session_redirect_forgot_password" );
    add_action( "lost_password", "book_a_session_recover_password" );

    // Initialise core scripts for admin and everyone

    wp_enqueue_script( 'book_a_session_jquery_waypoints', plugin_dir_url( __FILE__ ) . 'public/js/jquery.waypoints.min.js', array( 'jquery' ), null, true );
    wp_enqueue_script( 'book_a_session_waypoints_sticky', plugin_dir_url( __FILE__ ) . 'public/js/sticky.min.js', array( 'jquery' ), null, true );
    
}

// Initialise

add_action( 'admin_enqueue_scripts', 'book_a_session_init_admin_scripts' );
add_action( 'init', 'book_a_session_init' );

// Activation

function book_a_session_activate() {
    
    if ( ! current_user_can ( 'activate_plugins' ) ) return;
    
    book_a_session_add_practitioner_roles();
    book_a_session_create_bundle_table();
    book_a_session_create_region_table();
    book_a_session_create_location_table();
    book_a_session_create_service_type_table();
    book_a_session_create_service_table();
    book_a_session_create_payment_method_table();
    book_a_session_create_schedule_table();
    book_a_session_create_currency_table();
    book_a_session_create_practitioner_location_table();
    book_a_session_create_location_schedule_table();
    book_a_session_create_practitioner_schedule_table();
    book_a_session_create_timetable_table();
    book_a_session_create_order_table();
    book_a_session_create_invoice_table();
    book_a_session_create_payment_table();
    book_a_session_create_location_charge_table();
    book_a_session_create_bundle_currency_table();
    book_a_session_create_bundle_payment_method_table();
    book_a_session_create_location_payment_method_table();
    book_a_session_create_service_price_table();
    book_a_session_create_service_location_table();
    book_a_session_create_country_table();
    book_a_session_create_region_country_table();
    book_a_session_create_order_table();
    book_a_session_create_session_table();
    book_a_session_create_project_table();
    book_a_session_create_message_table();

    // Populate timetable temporary helper function

    //book_a_session_populate_timetable();

}


register_activation_hook( __FILE__, 'book_a_session_activate' );

// Deactivation

function book_a_session_deactivate() {
    
    if ( ! current_user_can( 'activate_plugins' ) ) return;
    
}

register_deactivation_hook( __FILE__, 'book_a_session_deactivate' );

// Uninstall 

function book_a_session_uninstall() {
    
    if ( ! current_user_can( 'activate_plugins' ) ) return;
    
}

register_uninstall_hook( __FILE__, 'book_a_session_uninstall' );

