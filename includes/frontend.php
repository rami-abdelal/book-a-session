<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Outputs services and booking form used to select, configure and create a booking for services.

 * @return  string  $output     The frontend form.
 * 
 */

function book_a_session_frontend( $format = false, $order = false, $limit = false ) {

    $services = $format === "services" ? true: false;
    $order = $services && ! empty( $order ) ? $order : false;
    $limit = $services && intval( $limit ) > 0 ? intval( $limit ) : false;

    book_a_session_print_login_register_form( "modal" );

    // Check if logged in and has user meta data region id, if so, use that, otherwise, get region via geoip

    $user_id = false;
    $user_region_id = false;

    if ( is_user_logged_in() ) {

        $user_id = get_current_user_id();

        if ( $user_id ) {

            // Check for region id in user meta

            $user_region_id = ! empty( get_user_meta( $user_id, "region_id", true ) ) ? get_user_meta( $user_id, "region_id", true ): false;

            // If it hasn't been set, find the appropriate region for them by checking their GeoIP via book_a_session_get_country_code() and the region_country table populated by the plugin user

            if ( ! $user_region_id ) {

                $user_country_code = book_a_session_get_country_code();

                if ( ! empty( $user_country_code ) ) {

                    $region_country = book_a_session_get_table_array( "region_country", false, false, "*", array( "country_code", "=", "'" . $user_country_code . "'" ), 1 )[0];

                    if ( ! empty( $region_country ) ) {
                        
                        // If a region exists with the user's country specified in the region_country table, use that region 

                        $user_region_id = $region_country->region_id;

                        update_user_meta( $user_id, "region_id", $user_region_id );

                    }

                }

            }

        }

    } else {

        // Simply do the above code block for non logged in users to determine their region, but of course we won't update user meta until we get them logged in or registered

        $user_country_code = book_a_session_get_country_code();

        if ( ! empty( $user_country_code ) ) {

            $region_country = book_a_session_get_table_array( "region_country", false, false, "*", array( "country_code", "=", "'" . $user_country_code . "'" ), 1 )[0];

            if ( ! empty( $region_country ) ) {
                
                // If a region exists with the user's country specified in the region_country table, use that region 

                $user_region_id = $region_country->region_id;

            }

        }

    }

    // Otherwise, flag that their region could not be found automatically, which will be used to determine whether or not we should ask the user to choose their desired region directly

    $no_region = intval( $user_region_id ) ? false : true;

    $region_array = book_a_session_get_table_array( "region" );

    // Check for an international region, if one exists, use that by default

    if ( $region_array && $no_region ) {

        $match = false;

        foreach ( $region_array as $region ) {

            // Use the international region if found

            if ( $region->international ) {
                
                $user_region_id = $region->id;
                $match = true;
                break;
    
            }

        }

        if ( ! $match ) {

            // Otherwise, default to the first region

            $user_region_id = $region_array[0]->id;

        }

    }

    // Clear this variable out for use below as a single db row associative array

    unset( $region_array );
    unset( $match );

    // Get services

    if ( $user_region_id ) {

        // Output all services if on the frontend or services page. Otherwise, if order and limit were passed, restrict the number of printed services and order the services by the given order (ASC | DESC) 

        $service_array = book_a_session_get_table_array( "service", $order ? "name" : false, $order ? $order : false, "*", false, $limit ? $limit : false );

        if ( $service_array ) {

            // Get region and currency to determine prices

            $region_input_array = array( "id" => $user_region_id );
            $region_array = book_a_session_get_region_by_id( $region_input_array );
            $currency_id = $region_array["currency_id"];
            $calculated_service_price_array = array();
            $available_location_array = array();

            if ( ! empty( $currency_id ) ) {

                // Output services grid. If an order or a limit were passed, output services as a widget. Otherwise, if services format was passed, output services for the services page. Otherwise, output for the conventional frontend form page
                
                if ( $order || $limit ) echo "<div class='book-a-session-services-widget book-a-session-services book-a-session-frontend'>";
                elseif ( $services ) echo "<div class='book-a-session-services book-a-session-frontend'>";
                else echo "<div class='book-a-session-frontend'>";

                foreach ( $service_array as $service ) :

                    // Check service locations to ensure the service can be practiced in at least one location in this region

                    $service_location_array = book_a_session_get_table_array( "service_location", false, false, "*", array( "service_id", "=", $service->id ) );

                    if ( $service_location_array ) {

                        $match = false;

                        foreach ( $service_location_array as $service_location ) :

                            $location_region_id = book_a_session_get_table_array( "location", false, false, "region_id", array( "id", "=", $service_location->location_id ), 1 )[0]->region_id;

                            if ( ! empty( $location_region_id ) ) {

                                if ( $location_region_id == $user_region_id ) {

                                    $match = true;

                                    if ( $service_location->service_id == $service->id ) {

                                        $location = (array) book_a_session_get_table_array( "location", false, false, "*", array( "id", "=", $service_location->location_id ), 1 )[0];
                                        $location_charge = book_a_session_get_table_array( "location_charge", false, false, "*", array( "location_id", "=", $service_location->location_id, "currency_id", "=", $currency_id ), 1 ) ? book_a_session_get_table_array( "location_charge", false, false, "*", array( "location_id", "=", $service_location->location_id, "currency_id", "=", $currency_id ), 1 )[0]->addition_charge : null;
                                        $location_schedule = book_a_session_get_table_array( "location_schedule", false, false, "schedule_id", array( "location_id", "=", $service_location->location_id ) ) ? book_a_session_get_table_array( "location_schedule", false, false, "schedule_id", array( "location_id", "=", $service_location->location_id ) ) : null;
                                        $location_payment_method = book_a_session_get_table_array( "location_payment_method", false, false, "payment_method_id", array( "location_id", "=", $service_location->location_id ) ) ? book_a_session_get_table_array( "location_payment_method", false, false, "payment_method_id", array( "location_id", "=", $service_location->location_id ) ) : null;

                                        $location["location_charge"] = ! empty( $location_charge ) ? number_format( $location_charge, 2 ) : "0.00";
                                        $location["location_charge_plain"] = ! empty( $location_charge ) ? book_a_session_get_price_tag( $location_charge, $currency_id ) : "0.00";
                                        $location["location_charge_html"] = ! empty( $location_charge ) ? book_a_session_get_price_tag( $location_charge, $currency_id, true ) : "0.00";
                                        $location["schedule"] = ! empty( $location_schedule ) ? $location_schedule : array();
                                        $location["payment_method"] = ! empty( $location_payment_method ) ? $location_payment_method : array();

                                        if ( ! empty( $location ) ) $available_location_array[ $service_location->service_id ][] = $location;

                                    }

                                } else {

                                    $location_region_international = book_a_session_get_table_array( "region", false, false, "international", array( "id", "=", $location_region_id ), 1 )[0]->international;

                                    if ( ! empty( $location_region_international ) ) {

                                        $match = true;

                                        if ( $service_location->service_id == $service->id ) {

                                            $location = (array) book_a_session_get_table_array( "location", false, false, "*", array( "id", "=", $service_location->location_id ), 1 )[0];
                                            $location_charge = book_a_session_get_table_array( "location_charge", false, false, "*", array( "location_id", "=", $service_location->location_id, "currency_id", "=", $currency_id ), 1 ) ? book_a_session_get_table_array( "location_charge", false, false, "*", array( "location_id", "=", $service_location->location_id, "currency_id", "=", $currency_id ), 1 )[0]->addition_charge : null;
                                            $location_schedule = book_a_session_get_table_array( "location_schedule", false, false, "schedule_id", array( "location_id", "=", $service_location->location_id ) ) ? book_a_session_get_table_array( "location_schedule", false, false, "schedule_id", array( "location_id", "=", $service_location->location_id ) ) : null;
                                            $location_payment_method = book_a_session_get_table_array( "location_payment_method", false, false, "payment_method_id", array( "location_id", "=", $service_location->location_id ) ) ? book_a_session_get_table_array( "location_payment_method", false, false, "payment_method_id", array( "location_id", "=", $service_location->location_id ) ) : null;
    
                                            $location["location_charge"] = ! empty( $location_charge ) ? number_format( $location_charge, 2 ) : "0.00";
                                            $location["location_charge_plain"] = ! empty( $location_charge ) ? book_a_session_get_price_tag( $location_charge, $currency_id ) : "0.00";
                                            $location["location_charge_html"] = ! empty( $location_charge ) ? book_a_session_get_price_tag( $location_charge, $currency_id, true ) : "0.00";
                                            $location["schedule"] = ! empty( $location_schedule ) ? $location_schedule : array();
                                            $location["payment_method"] = ! empty( $location_payment_method ) ? $location_payment_method : array();
    
                                            if ( ! empty( $location ) ) $available_location_array[ $service_location->service_id ][] = $location;
                
                                        }

                                    } 

                                }

                            }

                        endforeach;

                        if ( $match ) {

                            // Check for a unique price for this service as the highest priority

                            $service_price_input_array = array( "service_id", "=", $service->id, "currency_id", "=", $currency_id );
                            $service_price_result = book_a_session_get_table_array( "service_price", false, false, "*", $service_price_input_array );
                            $service_price_result ? $service_price = $service_price_result[0] : $service_price = false;

                            if ( ! empty( $service_price ) ) {

                                // Use this price primarily

                                (float)$price = (float)$service_price->session_price;

                            } else {

                                // If a service price isn't found, fall back to the default session price determined by region

                                (float)$price = (float)$region_array["session_price"];

                            }

                            // Get Discounts

                            $discounts = book_a_session_get_table_array( "bundle_currency", "quantity", "ASC", "*", array( "currency_id", "=", $currency_id ) );

                            // Get base price tags

                            $price_tag_html = book_a_session_get_price_tag( $price, $currency_id, true );
                            $price_tag_plain = book_a_session_get_price_tag( $price, $currency_id );

                            // Insert them into an array to localise and use later

                            $calculated_service_price_array[ (string)$service->id ] = array(

                                "price"             => (float)number_format( (float)$price, 2 ),
                                "currency_id"       => $currency_id,
                                "price_tag_html"    => $price_tag_html,
                                "price_tag_plain"   => $price_tag_plain

                            );

                            // Loop through discounts and include them in the above array for use on the frontend. Array should look like this: $prices["1"]["discounts"]["4"]["discounted_price_tag_plain"] = "£10.00";

                            if ( $discounts ) :

                                foreach ( $discounts as $discount ) :

                                    $calculated_service_price_array[ (string)$service->id ]["discounts"][ (string)$discount->quantity ] = array(

                                        "discount"                              => number_format( (float)$discount->discount, 2 ),
                                        "discount_single_tag_plain"             => book_a_session_get_price_tag( (float)$discount->discount, $currency_id ),
                                        "discount_single_tag_html"              => book_a_session_get_price_tag( (float)$discount->discount, $currency_id, true ),
                                        "discounted_single_price_tag_plain"     => book_a_session_get_price_tag( ( (float)$price - (float)$discount->discount ), $currency_id ),
                                        "discounted_single_price_tag_html"      => book_a_session_get_price_tag( ( (float)$price - (float)$discount->discount ), $currency_id, true ),
                                        "discount_total_tag_plain"              => book_a_session_get_price_tag( ( (float)$discount->discount * $discount->quantity ), $currency_id ),
                                        "discount_total_tag_html"               => book_a_session_get_price_tag( ( (float)$discount->discount * $discount->quantity ), $currency_id, true ),
                                        "discounted_total_price_tag_plain"      => book_a_session_get_price_tag( ( ( (float)$price - (float)$discount->discount ) * $discount->quantity ), $currency_id ),
                                        "discounted_total_price_tag_html"       => book_a_session_get_price_tag( ( ( (float)$price - (float)$discount->discount ) * $discount->quantity ), $currency_id, true ),

                                    );

                                endforeach;

                            endif;

                            // Service container

                            if ( $services ) {

                                echo '<div class="book-a-session-card-large book-a-session-card book-a-session-animated book-a-session-service"';
                                echo $limit ? ' style="width: calc( ( 100% - ( 17.5px * ' . ( $limit - 1 ) . ' ) ) / ' . $limit . ' );">' : '>';

                            } else echo "<div class='book-a-session-card book-a-session-service'>";

                            // Image

                            echo "<div class='book-a-session-service-image'";
                            
                            if ( $service->image_id ) echo " style='background-image:url(" . wp_get_attachment_image_src( $service->image_id, "medium" )[0] . ");'";

                            echo ">";

                            // Scrim

                            echo "<div class='book-a-session-scrim'>";

                            // Name

                            echo "<h2>" . $service->name . "</h2>";

                            // End scrim and image

                            echo "</div></div>";

                            if ( $services ) {

                                // Description if services page

                                echo '<div class="book-a-session-service-description">';

                                if ( ! $limit && ! $order ) echo '<p>' . $service->description . '</p>';

                                // Price as well

                                echo "<div class='book-a-session-service-price'>";
                                echo "<div class='book-a-session-service-price-tag'>" . $price_tag_html . "</div>";
                                
                                // And button

                                echo "<a data-frontend-service='" . $service->id . "' ";
                                echo "data-frontend-region='" . $user_region_id . "' ";
                                echo "data-frontend-currency='" . $currency_id . "' ";
                                echo "href='javascript:void(0)' class='book-a-session-animated book-a-session-button book-a-session-circle-button'><span class='book-a-session-icon book-a-session-icon-shopping-cart'></span></a>";


                                echo "</div>";

                            } else {

                                // Price

                                echo "<div class='book-a-session-service-price'>";
                                echo "<div class='book-a-session-service-price-tag'>" . $price_tag_html . "</div>";

                                // Button

                                echo "<a data-frontend-service='" . $service->id . "' ";
                                echo "data-frontend-region='" . $user_region_id . "' ";
                                echo "data-frontend-currency='" . $currency_id . "' ";
                                echo "href='javascript:void(0)' class='book-a-session-animated book-a-session-button book-a-session-circle-button'><span class='book-a-session-icon book-a-session-icon-shopping-cart'></span></a>";

                            }

                            // End price and service container

                            echo "</div></div>";

                        }

                    }

                endforeach;

                $savings_tag           = isset( get_option( "book_a_session_options_general" )["savings_tag"] )         ? get_option( "book_a_session_options_general" )["savings_tag"]           : book_a_session_options_default( "general", "savings_tag" );
                $quantity_top          = isset( get_option( "book_a_session_options_general" )["quantity_top"] )        ? get_option( "book_a_session_options_general" )["quantity_top"]          : book_a_session_options_default( "general", "quantity_top" );
                $quantity_bottom       = isset( get_option( "book_a_session_options_general" )["quantity_bottom"] )     ? get_option( "book_a_session_options_general" )["quantity_bottom"]       : book_a_session_options_default( "general", "quantity_bottom" );
                $location_top          = isset( get_option( "book_a_session_options_general" )["location_top"] )        ? get_option( "book_a_session_options_general" )["location_top"]          : book_a_session_options_default( "general", "location_top" );
                $location_bottom       = isset( get_option( "book_a_session_options_general" )["location_bottom"] )     ? get_option( "book_a_session_options_general" )["location_bottom"]       : book_a_session_options_default( "general", "location_bottom" );
                $practitioner_top      = isset( get_option( "book_a_session_options_general" )["practitioner_top"] )    ? get_option( "book_a_session_options_general" )["practitioner_top"]      : book_a_session_options_default( "general", "practitioner_top" );
                $practitioner_bottom   = isset( get_option( "book_a_session_options_general" )["practitioner_bottom"] ) ? get_option( "book_a_session_options_general" )["practitioner_bottom"]   : book_a_session_options_default( "general", "practitioner_bottom" );
                $practitioner_title    = isset( get_option( "book_a_session_options_general" )["practitioner_title"] )  ? get_option( "book_a_session_options_general" )["practitioner_title"]    : book_a_session_options_default( "general", "practitioner_title" );
                $payment_method_top    = isset( get_option( "book_a_session_options_general" )["practitioner_top"] )    ? get_option( "book_a_session_options_general" )["payment_method_top"]    : book_a_session_options_default( "general", "payment_method_top" );
                $payment_method_bottom = isset( get_option( "book_a_session_options_general" )["practitioner_bottom"] ) ? get_option( "book_a_session_options_general" )["payment_method_bottom"] : book_a_session_options_default( "general", "payment_method_bottom" );

                $practitioner_array = book_a_session_get_practitioners();
                $service_type_array = book_a_session_get_service_types();
                $payment_method_array = book_a_session_get_payment_methods();

                $accepted_payment_method_array = array(

                    'accept1' => isset( get_option( "book_a_session_options_paymentmethods" )["accept1"] ) ? get_option( "book_a_session_options_paymentmethods" )["accept1"] : book_a_session_options_default( "paymentmethods", "accept1" ),
                    'accept2' => isset( get_option( "book_a_session_options_paymentmethods" )["accept2"] ) ? get_option( "book_a_session_options_paymentmethods" )["accept2"] : book_a_session_options_default( "paymentmethods", "accept2" ),
                    'accept3' => isset( get_option( "book_a_session_options_paymentmethods" )["accept3"] ) ? get_option( "book_a_session_options_paymentmethods" )["accept3"] : book_a_session_options_default( "paymentmethods", "accept3" ),
                    'accept4' => isset( get_option( "book_a_session_options_paymentmethods" )["accept4"] ) ? get_option( "book_a_session_options_paymentmethods" )["accept4"] : book_a_session_options_default( "paymentmethods", "accept4" ),

                );

                $payment_method_options_array = array(

                    'frontend_icon_1'     => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_icon_1"] )     ? get_option( "book_a_session_options_paymentmethods" )["frontend_icon_1"]     : book_a_session_options_default( "paymentmethods", "frontend_icon_1" ),
                    'frontend_icon_2'     => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_icon_2"] )     ? get_option( "book_a_session_options_paymentmethods" )["frontend_icon_2"]     : book_a_session_options_default( "paymentmethods", "frontend_icon_2" ),
                    'frontend_icon_3'     => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_icon_3"] )     ? get_option( "book_a_session_options_paymentmethods" )["frontend_icon_3"]     : book_a_session_options_default( "paymentmethods", "frontend_icon_3" ),
                    'frontend_icon_4'     => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_icon_4"] )     ? get_option( "book_a_session_options_paymentmethods" )["frontend_icon_4"]     : book_a_session_options_default( "paymentmethods", "frontend_icon_4" ),
                    'frontend_title_1'    => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_title_1"] )    ? get_option( "book_a_session_options_paymentmethods" )["frontend_title_1"]    : book_a_session_options_default( "paymentmethods", "frontend_title_1" ),
                    'frontend_title_2'    => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_title_2"] )    ? get_option( "book_a_session_options_paymentmethods" )["frontend_title_2"]    : book_a_session_options_default( "paymentmethods", "frontend_title_2" ),
                    'frontend_title_3'    => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_title_3"] )    ? get_option( "book_a_session_options_paymentmethods" )["frontend_title_3"]    : book_a_session_options_default( "paymentmethods", "frontend_title_3" ),
                    'frontend_title_4'    => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_title_4"] )    ? get_option( "book_a_session_options_paymentmethods" )["frontend_title_4"]    : book_a_session_options_default( "paymentmethods", "frontend_title_4" ),
                    'frontend_subtitle_1' => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_subtitle_1"] ) ? get_option( "book_a_session_options_paymentmethods" )["frontend_subtitle_1"] : book_a_session_options_default( "paymentmethods", "frontend_subtitle_1" ),
                    'frontend_subtitle_2' => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_subtitle_2"] ) ? get_option( "book_a_session_options_paymentmethods" )["frontend_subtitle_2"] : book_a_session_options_default( "paymentmethods", "frontend_subtitle_2" ),
                    'frontend_subtitle_3' => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_subtitle_3"] ) ? get_option( "book_a_session_options_paymentmethods" )["frontend_subtitle_3"] : book_a_session_options_default( "paymentmethods", "frontend_subtitle_3" ),
                    'frontend_subtitle_4' => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_subtitle_4"] ) ? get_option( "book_a_session_options_paymentmethods" )["frontend_subtitle_4"] : book_a_session_options_default( "paymentmethods", "frontend_subtitle_4" ),
                    'frontend_tag_1'      => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_tag_1"] )      ? get_option( "book_a_session_options_paymentmethods" )["frontend_tag_1"]      : book_a_session_options_default( "paymentmethods", "frontend_tag_1" ),
                    'frontend_tag_2'      => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_tag_2"] )      ? get_option( "book_a_session_options_paymentmethods" )["frontend_tag_2"]      : book_a_session_options_default( "paymentmethods", "frontend_tag_2" ),
                    'frontend_tag_3'      => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_tag_3"] )      ? get_option( "book_a_session_options_paymentmethods" )["frontend_tag_3"]      : book_a_session_options_default( "paymentmethods", "frontend_tag_3" ),
                    'frontend_tag_4'      => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_tag_4"] )      ? get_option( "book_a_session_options_paymentmethods" )["frontend_tag_4"]      : book_a_session_options_default( "paymentmethods", "frontend_tag_4" ),
                    'frontend_top_1'      => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_top_1"] )      ? get_option( "book_a_session_options_paymentmethods" )["frontend_top_1"]      : book_a_session_options_default( "paymentmethods", "frontend_top_1" ),
                    'frontend_top_2'      => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_top_2"] )      ? get_option( "book_a_session_options_paymentmethods" )["frontend_top_2"]      : book_a_session_options_default( "paymentmethods", "frontend_top_2" ),
                    'frontend_top_3'      => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_top_3"] )      ? get_option( "book_a_session_options_paymentmethods" )["frontend_top_3"]      : book_a_session_options_default( "paymentmethods", "frontend_top_3" ),
                    'frontend_top_4'      => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_top_4"] )      ? get_option( "book_a_session_options_paymentmethods" )["frontend_top_4"]      : book_a_session_options_default( "paymentmethods", "frontend_top_4" ),
                    'frontend_bottom_1'   => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_bottom_1"] )   ? get_option( "book_a_session_options_paymentmethods" )["frontend_bottom_1"]   : book_a_session_options_default( "paymentmethods", "frontend_bottom_1" ),
                    'frontend_bottom_2'   => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_bottom_2"] )   ? get_option( "book_a_session_options_paymentmethods" )["frontend_bottom_2"]   : book_a_session_options_default( "paymentmethods", "frontend_bottom_2" ),
                    'frontend_bottom_3'   => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_bottom_3"] )   ? get_option( "book_a_session_options_paymentmethods" )["frontend_bottom_3"]   : book_a_session_options_default( "paymentmethods", "frontend_bottom_3" ),
                    'frontend_bottom_4'   => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_bottom_4"] )   ? get_option( "book_a_session_options_paymentmethods" )["frontend_bottom_4"]   : book_a_session_options_default( "paymentmethods", "frontend_bottom_4" ),
                    'sandbox_live_1'      => isset( get_option( "book_a_session_options_paymentmethods" )["sandbox_live_1"] )      ? get_option( "book_a_session_options_paymentmethods" )["sandbox_live_1"]      : book_a_session_options_default( "paymentmethods", "sandbox_live_1" ),
                    'sandbox_id_1'        => isset( get_option( "book_a_session_options_paymentmethods" )["sandbox_id_1"] )        ? get_option( "book_a_session_options_paymentmethods" )["sandbox_id_1"]        : book_a_session_options_default( "paymentmethods", "sandbox_id_1" ),
                    'live_id_1'           => isset( get_option( "book_a_session_options_paymentmethods" )["live_id_1"] )           ? get_option( "book_a_session_options_paymentmethods" )["live_id_1"]           : book_a_session_options_default( "paymentmethods", "live_id_1" ),
                    'test_live_4'         => isset( get_option( "book_a_session_options_paymentmethods" )["test_live_4"] )         ? get_option( "book_a_session_options_paymentmethods" )["test_live_4"]         : book_a_session_options_default( "paymentmethods", "test_live_4" ),
                    'test_pub_key_4'      => isset( get_option( "book_a_session_options_paymentmethods" )["test_pub_key_4"] )      ? get_option( "book_a_session_options_paymentmethods" )["test_pub_key_4"]      : book_a_session_options_default( "paymentmethods", "test_pub_key_4" ),
                    'live_pub_key_4'      => isset( get_option( "book_a_session_options_paymentmethods" )["live_pub_key_4"] )      ? get_option( "book_a_session_options_paymentmethods" )["live_pub_key_4"]      : book_a_session_options_default( "paymentmethods", "live_pub_key_4" ),
                    'online_payable'      => isset( get_option( "book_a_session_options_paymentmethods" )["online_payable"] )      ? get_option( "book_a_session_options_paymentmethods" )["online_payable"]      : book_a_session_options_default( "paymentmethods", "online_payable" ),

                );

                wp_localize_script( 'book-a-session-js', 'book_a_session_frontend_authentication', array(

                    'api_root'                          => esc_url_raw( rest_url() . 'book-a-session/v1/' ),
                    'api_nonce'                         => wp_create_nonce( 'wp_rest' ),
                    'currency'                          => book_a_session_get_currency_by_id( array( "id" => $currency_id ) ),
                    'stripe_cc_img_url'                 => plugin_dir_url( dirname( __FILE__ ) ) . 'public/img/stripe-logo.png',
                    'logo_square_url'                   => ! empty( get_option( "book_a_session_options_general" )["image_logo_square"] ) ? wp_get_attachment_image_src( get_option( "book_a_session_options_general" )["image_logo_square"], "full" ) : null,
                    'dashboard_url'                     => isset( get_option( "book_a_session_options_general" )["dashboard_page_id"] ) ? get_permalink( get_option( "book_a_session_options_general" )["dashboard_page_id"] ) : null,
                    'vfc_receiving_number'              => isset( get_option( "book_a_session_options_invoices" )["vfc_receiving_mobile_number"] ) ? get_option( "book_a_session_options_invoices" )["vfc_receiving_mobile_number"] : null,
                    'redirect_url'                      => ! empty( get_permalink( get_option( "book_a_session_options_general" )["frontend_page_id"] ) ) ? get_permalink( get_option( "book_a_session_options_general" )["frontend_page_id"] ) : home_url(),
                    'site_name'                         => get_bloginfo( "name" ),
                    'calculated_service_price_array'    => $calculated_service_price_array,
                    'available_location_array'          => $available_location_array,
                    'practitioner_array'                => $practitioner_array,
                    'service_type_array'                => $service_type_array,
                    'payment_method_array'              => $payment_method_array,
                    'accepted_payment_method_array'     => $accepted_payment_method_array,
                    'payment_method_options_array'      => $payment_method_options_array,
                    'currency_id'                       => $currency_id,
                    'user_id'                           => $user_id,
                    'user_region_id'                    => $user_region_id,
                    'no_region'                         => $no_region ? 1 : 0,
                    'savings_tag'                       => $savings_tag ? 1 : 0,
                    'practitioner_title'                => $practitioner_title ? 1 : 0,
                    'quantity_top'                      => $quantity_top,
                    'quantity_bottom'                   => $quantity_bottom,
                    'location_top'                      => $location_top,
                    'location_bottom'                   => $location_bottom,
                    'practitioner_top'                  => $practitioner_top,
                    'practitioner_bottom'               => $practitioner_bottom,                    
                    'payment_method_top'                => $payment_method_top,
                    'payment_method_bottom'             => $payment_method_bottom,
                    'ajax_url'                          => admin_url( 'admin-ajax.php' ),

                ) );

            }

            echo "</div>"; // End book-a-session-frontend

        }

    }

}