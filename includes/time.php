<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Sets default timezone for PHP and MySQL. Passing no arguments sets the timezone to default. 
 * This can be used to change MySQLs default timezone before a query like so: 
 * book_a_session_set_default_timezone_mysql( $region_id );
 * book_a_session_mysql_time_query();
 * book_a_session_set_default_timezone_mysql(); 
 * 
 * @param   int     $region_id          ID of the region used to find a timezone string used to set PHP time. MySQL time is changed based on PHP time offset.
 * @param   string  $timezone_string    Timezone string such as "Africa/Cairo" which can be used to set PHP time. MySQL time is changed based on PHP time offset.
 * @return  void    void                Nothing to return.
 * 
 */

function book_a_session_set_default_timezone_mysql( $region_id = false, $timezone_string = false ) {

    // If supplied region id, use that to find the region's timezone string and set that as PHP's default timezone

    if ( intval( $region_id ) ) {

        global $wpdb;
        $table_name_region = $wpdb->prefix . "book_a_session_region";

        $region_timezone = $wpdb->get_row(

            "
            SELECT timezone
            FROM $table_name_region
            WHERE id = $region_id
            "

        );

        if ( $region_timezone ) {
            
            $region_timezone_string = $region_timezone->timezone;

            date_default_timezone_set( $region_timezone_string );

        }

    } elseif ( $timezone_string ) {

        // If passed timezone strig, use that to set the PHP timezone

        date_default_timezone_set( $timezone_string );

    } else {

        // Otherwise, set the PHP default timezone according to user options or default

        book_a_session_set_default_timezone();

    }

    // With PHP timezones now set, use PHP time to get offset and use that to set MySQL server timezone
    
    $now = new DateTime();
    $mins = $now->getOffset() / 60;
    $sgn = ($mins < 0 ? -1 : 1);
    $mins = abs($mins);
    $hrs = floor($mins / 60);
    $mins -= $hrs * 60;
    $offset = sprintf('%+d:%02d', $hrs*$sgn, $mins);

    global $wpdb;
    $wpdb->query("SET time_zone='$offset';");

}


// Set default PHP timezone from user options. If we can't find a timezone set in Book A Session Schedule settings, we'll use the default WordPress timezone string option.
// Currently takes region_id, but does not apply it yet.

function book_a_session_set_default_timezone( $region_id = false ) {

    $region_id = intval( $region_id ) ? intval( $region_id ) : false;

    $default_timezone = get_option( 'book_a_session_options_schedule' )['timezone'] ? get_option( 'book_a_session_options_schedule' )['timezone'] : get_option( 'timezone_string' );   

    if ( $default_timezone ) {
        
        if ( date_default_timezone_get() !== $default_timezone ) {

            date_default_timezone_set( $default_timezone );

            if ( date_default_timezone_get() === $default_timezone ) {

                return true;

            } else {

                return false;

            }

        } else {

            return true;

        }

    } elseif ( $region_id ) {

        global $wpdb;
        $table_name_region = $wpdb->prefix . "book_a_session_region";

        $region_timezone = $wpdb->get_row(

            "
            SELECT timezone
            FROM $table_name_region
            WHERE id = $region_id
            "

        );

        if ( $region_timezone ) {
            
            $region_timezone_string = $region_timezone->timezone;
            date_default_timezone_set( $region_timezone_string );

        }

    } else {

        add_action( 'admin_notices', 'book_a_session_admin_notice_option_info' );

    }

}

book_a_session_set_default_timezone_mysql();


/** 
 * Book A Session Convert DateTime book_a_session_convert_datetime()
 * Convert any date time object to a supplied timezone
 * 
 * @param   object          $datetime_object            The datetime to convert
 * @param   string          $timezone_string            The desired timezone to which we'll convert the supplied datetime object
 * @param   int/string      $region_id                  Alternatively, a region ID we can use to refer to the database to get the desired timezone string
 * @return  object          $converted_datetime_object  Converted datetime 
 * @return  object          $datetime_object            Returns passed datetime object unconverted on error 
 * 
 */

function book_a_session_convert_datetime( $datetime_object, $timezone_string = false, $region_id = false ) {

    // Start by setting default timezone

    book_a_session_set_default_timezone_mysql();

    // If a timezone string was supplied, use that to return a datetime object converted to the supplied timezone

    if ( $timezone_string ) {

        $timezone_object = new DateTimeZone( $timezone_string );
        $converted_datetime_object = $datetime_object->setTimezone( $timezone_object );
        book_a_session_set_default_timezone_mysql();
        return $converted_datetime_object;

    } elseif ( $region_id ) {

        // Otherwise if a region id was supplied, use the id to get the timezone string from the database and use that instead to return a datetime object converted to the supplied region's timezone

        global $wpdb;
        $table_name_region = $wpdb->prefix . "book_a_session_region";

        $region_timezone = $wpdb->get_row(

            "
            SELECT timezone
            FROM $table_name_region
            WHERE id = $region_id
            "

        );

        if ( $region_timezone ) {
            
            $region_timezone_object = new DateTimeZone( $region_timezone->timezone );
            $converted_datetime_object = $datetime_object->setTimezone( $region_timezone_object );
            book_a_session_set_default_timezone_mysql();
            return $converted_datetime_object;
        
        } else {

            return $datetime_object;

        }

    } else {

        return $datetime_object;

    }

 }

/*  Get Session Time By Schedule ID book_a_session_get_session_time()
*   Takes the numbered database ID of a session time recorded in the schedule database, 
*   checks it against the database, gets the right record, processes it, and returns a
*   prettified string e.g. "11am-12pm". Optionally include a region id for timezone changing.
*/

function book_a_session_get_session_time( $id_number, $region_id = false, $date = false ) {
    
    global $wpdb;
    $table_name = $wpdb->prefix . "book_a_session_schedule";

    $result = $wpdb->get_results(
        "
        SELECT *
        FROM $table_name
        WHERE id = $id_number        
        "
    );

    if ( $region_id ) {

        // Get region's timezone

        $table_name_region = $wpdb->prefix . "book_a_session_region";

        $region_timezone = $wpdb->get_row(
            "
            SELECT timezone
            FROM $table_name_region
            WHERE id = $region_id
            "
        );

        // Get default timezone

        $schedule_timezone = get_option( 'book_a_session_options_schedule' )['timezone'] ? get_option( 'book_a_session_options_schedule' )['timezone'] : get_option( 'timezone_string' );

    }

    if ( $result ) {

        // Start by turning time strings into datetime objects, and initialising variables that may be returned
        
        $start_time = new DateTime( $result[0]->start_time );
        $end_time = new DateTime( $result[0]->end_time );
        $start_datetime_object = $date ? new DateTime( $date . " " . $result[0]->start_time ) : false;
        $end_datetime_object = $date ? new DateTime( $date . " " . $result[0]->end_time ) : false;

        // If everything needed to convert timezones is present

        if ( ! empty( $region_timezone ) && ! empty( $schedule_timezone ) && ! empty( $date ) ) {

            // Start by setting the default timezone

            $original_timezone = date_default_timezone_get();
            book_a_session_set_default_timezone_mysql( false, $schedule_timezone );

            // Combine date and time into new DateTime objects

            $start_datetime = new DateTime( $date . " " . $result[0]->start_time );
            $end_datetime = new DateTime( $date . " " . $result[0]->end_time );

            // Prepare DateTimeZone object

            $region_timezone_object = new DateTimeZone( $region_timezone->timezone );

            // Use that to convert DateTime to the supplied timezone

            $start_datetime->setTimezone( $region_timezone_object );
            $end_datetime->setTimezone( $region_timezone_object );

            // Assign converted object to times that will be processed into a string to be returned just as a non timezone converted time would be returned

            $start_time = $start_datetime;
            $end_time = $end_datetime;

            // Assign datetime objects separately to be returned for full context

            $start_datetime_object = $start_time;
            $end_datetime_object = $end_time;

            // Revert default timezone

            book_a_session_set_default_timezone_mysql( false, $original_timezone );

        }

        // Process as a string normally
        $start_time = $start_time->format("H:i:s");
        $end_time = $end_time->format("H:i:s");
        $time = array( $start_time, $end_time );

        // To Do: Set up option to choose between 12 hour or 24 hour time formats. Right now we only output 12 hour.

        // Turn associative array of returned record object into indexed array split by colons for easier calculations
        $start_time_array = explode( ":", $time[0] );
        $end_time_array = explode( ":", $time[1] );
        $start_time_suffix;
        $end_time_suffix;
        // Check if am or pm
        (int)$start_time_array[0] >= 12 ? $start_time_suffix = "pm" : $start_time_suffix = "am";
        (int)$end_time_array[0] >= 12 ? $end_time_suffix = "pm" : $end_time_suffix = "am";
        // If 13:00 or later, minus 12 to get 12hr time format 
        if ( (int)$start_time_array[0] > 12 ) { (int)$start_time_array[0] -= 12;  }
        if ( (int)$end_time_array[0] > 12 ) { (int)$end_time_array[0] -= 12;  }
        // Remove leading zeroes from hours
        $start_time_array[0] = ltrim( $start_time_array[0], '0' );
        $end_time_array[0] = ltrim( $end_time_array[0], '0' );
        // Determine if minutes are 0, so the time is precisely hourly. If so, we can get rid of the extra zeros.
        (int)$start_time_array[1] === 00 ? $start_time_hourly = true : $start_time_hourly = false;
        (int)$end_time_array[1] === 00 ? $end_time_hourly = true : $end_time_hourly = false;
        // So if it's precisely hourly, shorten it and add the suffix. Otherwise, put the hours and minutes back together into a string without the seconds and add am or pm.
        $start_time_hourly ? $start_time = $start_time_array[0] : $start_time = $start_time_array[0] . ":" . $start_time_array[1];
        $end_time_hourly ? $end_time = $end_time_array[0]: $end_time = $end_time_array[0] . ":" . $end_time_array[1];

        $full_time = $start_time . $start_time_suffix . "-" . $end_time . $end_time_suffix;
        
        return array( 

            "time_string"           => $full_time, 
            "start_time"            => $start_time . $start_time_suffix, 
            "end_time"              => $end_time . $end_time_suffix, 
            "start_datetime_object" => $start_datetime_object, 
            "end_datetime_object"   => $end_datetime_object , 
            "start_date_formatted"  => $start_datetime_object ? $start_datetime_object->format( get_option( 'date_format' ) ) : false, 
            "end_date_formatted"    => $end_datetime_object ? $end_datetime_object->format( get_option( 'date_format' ) ) : false, 
            "time_string_array"     => array( 
                $start_time, 
                $start_time_suffix, 
                $end_time, 
                $end_time_suffix 
            ) );

    } else {

        return false;

    }

}

function book_a_session_get_due_date( $session_array, $region_id = false ) {

    $region_id = $region_id ? $region_id : false;

    if ( is_array( $session_array ) ) {

        // Sort session array by soonest to latest if there are multiple sessions

        if ( count( $session_array ) > 1 ) {

            $sorted_array = array();

            foreach( $session_array as $key => $value ) {

                $sorted_array[] = strtotime( book_a_session_get_session_time( $value["schedule_id"], $region_id, $value["date"] )["start_datetime_object"]->format( "Y-m-d H:i:s" ) );

            }

            array_multisort( $sorted_array, SORT_ASC, $session_array );

        }

        // Get Vodafone Cash invoice option values

        $due_date_offset_measurement = isset( get_option( 'book_a_session_options_invoices' )['due_date_offset_measurement'] ) ? get_option( 'book_a_session_options_invoices' )['due_date_offset_measurement'] : book_a_session_options_default( "invoices", "due_date_offset_measurement" );
        $due_date_offset_anchor      = isset( get_option( 'book_a_session_options_invoices' )['due_date_offset_anchor'] )      ? get_option( 'book_a_session_options_invoices' )['due_date_offset_anchor']      : book_a_session_options_default( "invoices", "due_date_offset_anchor" );
        $due_date_offset_value       = isset( get_option( 'book_a_session_options_invoices' )['due_date_offset_value'] )       ? get_option( 'book_a_session_options_invoices' )['due_date_offset_value']       : book_a_session_options_default( "invoices", "due_date_offset_value" );

        if ( (int)$due_date_offset_value === 1 ) {

            // Remove 's' at end of string (minutes, hours, days) when it's only one minute, hour, or day.

            $due_date_offset_measurement = preg_replace( "/s$/", "", $due_date_offset_measurement );

        }

        // If due date options have been entered and retrieved correctly

        if ( ! empty( $due_date_offset_anchor ) && ! empty( $due_date_offset_measurement ) && ! empty( $due_date_offset_value ) ) {

            // Calculate due date if the period is to be measured from the issue date, i.e. issue date +1 day

            if ( $due_date_offset_anchor === 'issue_date' ) {

                $issue_date = book_a_session_convert_datetime( new DateTime( date("Y-m-d H:i:s") ), false, $region_id )->format("Y-m-d H:i:s");
                $str = '+' . $due_date_offset_value . ' ' . $due_date_offset_measurement;
                $due_by_date = date( "Y-m-d H:i:s", strtotime( $str, strtotime( $issue_date ) ) );

            // Calculate due date if the period is to be measured from the session date, by getting the first session date, its time, putting them together as a datetime object, and modifying it according to user options, i.e. session date -1 day.

            } elseif ( $due_date_offset_anchor === 'session_date' ) {

                $session_date =  $session_array[0]["date"];
                $session_time = book_a_session_get_table_array( "schedule", false, false, "start_time", array( "id", "=", $session_array[0]["schedule_id"] ) )[0]->start_time;
                $session_date = date( $session_date . " " . $session_time );
                $str = '-' . $due_date_offset_value . ' ' . $due_date_offset_measurement;
                $due_by_date = date( "Y-m-d H:i:s", strtotime( $str, strtotime( $session_date ) ) );

            }

        }

        if ( $due_by_date ) {

            return array(

                "date" => book_a_session_convert_datetime( new DateTime( $due_by_date ), false, $region_id )->format( get_option( "date_format" ) ),
                "time" => book_a_session_convert_datetime( new DateTime( $due_by_date ), false, $region_id )->format( get_option( "time_format" ) ),

            );

        } else return false;

    } else return false;

}