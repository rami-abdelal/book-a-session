<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Initialises Book A Session shortcode and all of its assets depending on the shortcode
 * 
 */

function book_a_session_register_shortcodes() {

    add_shortcode( 'book-a-session', 'book_a_session_do_shortcodes' );
    add_filter( 'the_posts', 'book_a_session_shortcode_scripts_styles' );

}

/**
 * Loads scripts and styles for specific shortcodes only when the correct shortcode is found.
 * This is used as a callback function for book_a_session_register_shortcodes().
 * 
 * @param   object  $posts  the_posts filter passes WP_Post objects which we used to find shortcodes
 * @return  object  $posts  Returns post objects for the_posts filter  
 * 
 */

function book_a_session_shortcode_scripts_styles( $posts ) {

    if ( empty( $posts ) ) return $posts;

    $shortcode_found = false;

    foreach ($posts as $post) {

        if ( stripos($post->post_content, '[book-a-session' ) !== false ) $shortcode_found = true; break;

    }

    if ( $shortcode_found ) {

        // All shortcodes found prompt the following scripts
        
        wp_register_script( "paypal_checkout", "https://www.paypalobjects.com/api/checkout.js" );
        wp_register_script( "stripe_checkout", "https://checkout.stripe.com/checkout.js" );
        
        wp_register_script( "book-a-session-js", plugin_dir_url( dirname( __FILE__ ) ) . 'public/js/script.js', array( 'jquery' ), null, true );


        // Enqueue scripts

        wp_enqueue_script( 'paypal_checkout' );
        wp_enqueue_script( 'stripe_checkout' );
        wp_enqueue_script( 'book-a-session-js' );

        wp_enqueue_style( 'book-a-session-css', plugin_dir_url( dirname( __FILE__ ) ) . 'public/css/style.css' );

    }

    return $posts;

}

/**
 * Finds and replaces shortcodes to output the main plugin functionality such as the fontend form 
 * where services are displayed and sessions are booked and paid for. This function is called by 
 * book_a_session_register_shortcodes() via the add_shortcode function.
 * 
 * @param   array   $atts           Indexed or associative array of attributes.
 * @param   string  $content        Any content inside the shortcode tag, e.g. [shortcode]Content[/shortcode].
 * @param   string  $shortcode_tag  Shortcode tag, e.g. [book-a-session].
 * 
 */

function book_a_session_do_shortcodes( $atts, $content, $shortcode_tag ) {

    // Check for boolean attributes e.g. [book-a-session frontend].

    $frontend           = in_array( "frontend", $atts, true )                        ? true : false;
    $services           = in_array( "services", $atts, true )                        ? true : false;
    $dashboard          = in_array( "dashboard", $atts, true )                       ? true : false;
    $login              = in_array( "login", $atts, true )                           ? true : false;
    $register           = in_array( "register", $atts, true )                        ? true : false;
    $forgot_password    = in_array( "forgot-password", $atts, true )                 ? true : false;

    // Check for a key value pair attributes, e.g. [book-a-session timetable="location_1"].

    $timetable          = ! empty( $atts["timetable"] )         ? $atts["timetable"]        : false;
    $timetable_format   = ! empty( $atts["timetable_format"] )  ? $atts["timetable_format"] : false;
    $timetable_order    = ! empty( $atts["timetable_order"] )   ? $atts["timetable_order"]  : false;
    $timetable_limit    = ! empty( $atts["timetable_limit"] )   ? $atts["timetable_limit"]  : false;    
    $timezone           = ! empty( $atts["timezone"] )          ? $atts["timezone"]         : false;

    $services_list      = ! empty( $atts["services_list"] )     ? $atts["services_list"]    : false;
    $services_limit     = ! empty( $atts["services_limit"] )    ? $atts["services_limit"]   : false;
    $services_order     = ! empty( $atts["services_order"] )    ? $atts["services_order"]   : "ASC";

    // Generate output depending on the values found above

    if ( $frontend ) return book_a_session_frontend();

    if ( $services ) return book_a_session_services();

    if ( $services_list ) return book_a_session_frontend( "services", $services_order, $services_limit );

    if ( $dashboard ) return book_a_session_dashboard();

    if ( $timetable ) return book_a_session_timetable( $timetable, $timetable_format, $timetable_order, $timetable_limit, $timezone );

    if ( $login ) {
        
        if ( is_user_logged_in() ) return book_a_session_dashboard();
        else return book_a_session_print_login_form();

    }

    if ( $register ) {
        
        if ( is_user_logged_in() ) return book_a_session_dashboard();
        else return book_a_session_print_register_form();

    }

    if ( $forgot_password ) {
        
        if ( is_user_logged_in() ) return book_a_session_dashboard();
        else return book_a_session_print_forgot_password_form();
    }

}

book_a_session_register_shortcodes();


