<?php // Book A Session - Core Functionality

// Exit if file is called directly

if ( ! defined( 'ABSPATH' ) ) {
    
    exit;
    
}

/**
 * 
 * book_a_session_get_table_array() is the primary getter in this plugin.
 * 
 * Because Book A Session stores its resources (such as locations, services, and orders)
 * in separate tables from the tables WordPress uses, also storing them in different ways, we
 * use this function to get our resources, instead of using the WordPress loop.
 * 
 * This function works by extending and specialising WordPress's own SQL function, $wpdb->get_results();
 * 
 * No user input should be passed into this function, as it doesn't escape anything it receives.
 * 
 * To get a resource such as orders, simply call book_a_session_get_table_array( "order" ) to receive all orders in object format.
 * 
 * Pass more arguments to restrict the returned results, e.g: 
 * book_a_session_get_table_array( "order", "created", "DESC", "booking_status, payment_status", array( "total", ">", 50.00, "currency_id", "=", 12 ), 3 )
 * This gets booking_status and payment_status columns for 3 of the latest orders, where the total is more than 50.00, and the currency_id is 12.
 * 
 * @param   $table_name     string      Required        The name of the resource as found after "book_a_session_" in the table name in the database, such as "payment_method" or "order".
 * 
 * @param   $orderby        string      Optional        Column name to order the results by.
 * 
 * @param   $order          string      Optional        ASC | DESC.
 * 
 * @param   $column_names   string      Optional        Defaults to "*", which gets all columns. Enter a comma separated list of column names to restrict your results to those columns.
 * 
 * @param   $where          array       Optional        Enter an indexed array of values and operators to restrict the resources in a certain way. The first item must be the column name,
 *                                                      such as "total", the second must be the operator, such as "=" or "<", and the third must be the value, such as 50.00. Enter a
 *                                                      second set of three values in this array, and you can restrict the results further by placing another constraint. The first three
 *                                                      are strung together by default with AND, so the SQL query ends up like this: WHERE total > 50.00 AND currency_id = 12.
 *                                                      However, if you need to use OR instead of AND, place the string "OR" as the very first item in the indexed array. For example:
 *                                                      array( "OR", "booking_status", "=", "Booked", "booking_status", "=", "Pending" ). This will build the following query:
 *                                                      WHERE booking_status = "Booked" OR booking_status = "Pending". In both cases, (with or without OR), you can include as many items
 *                                                      in the $where array as you like, but the items must be in the correct order, and the total number of entries must be divisble by 3,
 *                                                      excluding the initial "OR" when included. So if you want to use three or more constraints, each will be strung together with OR or AND.
 * 
 * @param   $limit          int|string  Optional        Enter an integer here to limit the number of results. For example, if you have 10 orders, and you want to get 5, enter 5 and you shall receive.
 *                                                      Entering a string such as "5,10" to retrieve only those rows, best used with order and orderby for pagination, in conjunction with
 *                                                      book_a_session_count_table().
 *                                                                       
 * 
 */

function book_a_session_get_table_array( $table_name, $orderby = NULL, $order = NULL,  $column_names = "*", $where = NULL, $limit = NULL, $debug = NULL ) {

    global $wpdb;
    $full_table_name = $wpdb->prefix . "book_a_session_" . $table_name;
    $sql  = "SELECT ";
    $sql .= $column_names;
    $sql .= " FROM $full_table_name";

    // Append where statements as long as they are provided and it's an array

    if ( ! empty ( $where ) ) {

        if ( is_array( $where ) ) {

            if ( $where[0] === "OR" ) {

                for ( $i = 1; $i < count( $where ) ; $i++ ) {

                    if ( $i === 1 ) { 
        
                        $sql .= " WHERE ";
        
                     } else {
        
                        $sql .= " OR ";
        
                    }
        
                    // Each three consecutive values are associated with eachother, e.g. array( "date", ">=", "CURDATE();" ). 
        
                    $sql .= $where[ $i ] . " " . $where[ $i + 1 ] . " " . $where[ $i + 2 ];
        
                    // Skip two and move on to the next three if they exist.
                    
                    $i = $i + 2;
        
                }

            } else {

                for ( $i = 0; $i < count( $where ); $i++ ) {

                    if ( $i === 0 ) { 
        
                        $sql .= " WHERE ";
        
                     } else {
        
                        $sql .= " AND ";
        
                    }
        
                    // Each three consecutive values are associated with eachother, e.g. array( "date", ">=", "CURDATE();" ). 
        
                    $sql .= $where[ $i ] . " " . $where[ $i + 1 ] . " " . $where[ $i + 2 ];
        
                    // Skip two and move on to the next three if they exist.
                    
                    $i = $i + 2;
        
                }

            }

        }

    }

    if ( ! empty ( $orderby ) && ! empty( $order ) && ! is_array( $orderby ) ) {

        // If both orderby and order are included, and orderby isn't an array, we'll use orderby as the column name, and order as the order type, e.g. ASC or DESC

        $sql .= " ORDER BY $orderby $order";

    } elseif ( ! empty ( $orderby ) && ! is_array( $orderby ) ) {

        // If we only have orderby and it's not an array, just spit out ORDER BY column name to use the default ordering which should be ASC or whatever it is

        $sql .= " ORDER BY $orderby";

    } elseif ( is_array( $orderby ) ) {

        // If it's an array, we'll assume it's indexed and goes something like this: array( "column_1", "ASC", "column_2", "DESC" ). With that we will go through and put them together into the query

        for ( $i = 0; $i < count( $orderby ); $i++ ) {

            if ( $i == 0 ) {

                $sql .= " ORDER BY ";

            } else {

                $sql .= ", ";
                 
            }

            $sql .= $orderby[ $i ] . " " . $orderby[ $i + 1 ];

            $i++;

        }

    }

    if ( $limit ) {

        $sql .= " LIMIT " . $limit;

    }
    
    $result = $wpdb->get_results( $sql );

    // If the final argument, debug, is supplied (it can be any truthy value), return the query instead of the result.

    if ( ! $debug ) return $result;
    else return $sql;

}

/**
 * Count rows in a table provided, optionally only count them where they meet conditions determined by the values in a provided indexed array
 * 
 * The array works like the book_a_session_get_table_array() where parameter.
 * 
 * It should look something like this for one conditon: array( "date", ">", "CURDATE()" )
 * For multiple AND conditions (e.g. Where user id = 1 and total > 19.99 ): array( "user_id", "=", 1, "total", ">", "19.99" )
 * 
 * For multiple OR conditions, you would include "OR" as the first item in the array, then it would work just like the AND conditions above, e.g. 
 * (Where user id = 10 or practitioner id = 20): array( "OR", "user_id", "=", 10, "practitioner_id", "=", 20 )
 * 
 * @param   $table_name     string      Required string determines which table's rows we wish to count  
 * @param   $where          array       Optional array used to restrict the count using the SQL WHERE clause
 * 
 */

function book_a_session_count_table( $table_name, $where = false ) {

    if ( ! empty( $table_name ) ) {

        global $wpdb;
        $full_table_name = $wpdb->prefix . "book_a_session_" . $table_name;

        $sql = "SELECT COUNT(*) FROM $full_table_name";

        if ( $where ) {

            if ( is_array( $where ) ) {
   
                if ( $where[0] === "OR" ) {

                    for ( $i = 1; $i < count( $where ) ; $i++ ) {

                        if ( $i === 1 ) { 
            
                            $sql .= " WHERE ";
            
                        } else {
            
                            $sql .= " OR ";
            
                        }
            
                        // Each three consecutive values are associated with eachother, e.g. array( "date", ">=", "CURDATE();" ). 
            
                        $sql .= $where[ $i ] . " " . $where[ $i + 1 ] . " " . $where[ $i + 2 ];
            
                        // Skip two and move on to the next three if they exist.
                        
                        $i = $i + 2;
            
                    }

                } else {

                    for ( $i = 0; $i < count( $where ); $i++ ) {

                        if ( $i === 0 ) { 
            
                            $sql .= " WHERE ";
            
                        } else {
            
                            $sql .= " AND ";
            
                        }
            
                        // Each three consecutive values are associated with eachother, e.g. array( "date", ">=", "CURDATE();" ). 
            
                        $sql .= $where[ $i ] . " " . $where[ $i + 1 ] . " " . $where[ $i + 2 ];
            
                        // Skip two and move on to the next three if they exist.
                        
                        $i = $i + 2;
            
                    }

                }

            }

        }

        $count = $wpdb->get_var( $sql );

        if ( $count ) return $count;
        else return $sql;

    } else return false;

}

function book_a_session_get_practitioner_array() {

    $current_practitioners = get_users( array(
        'role' => 'practitioner'
    ) );
    $current_admin_practitioners = get_users( array(
        'role' => 'admin_practitioner'
    ) );

    $practitioner_array = array_merge( $current_admin_practitioners, $current_practitioners );

    return $practitioner_array;

}
/*  Get Practitioner Name By User ID book_a_session_get_practitioner_name()
*   Alias of book_a_session_get_user_name(), 
*/

function book_a_session_get_practitioner_name( $id_number ) {
    
    return get_userdata( $id_number )->display_name;

}

/*  Get User Name By User ID book_a_session_get_user_name()
*   Takes user id int, uses that to get WP User object, and returns a first and last name string
*/

function book_a_session_get_user_name( $id_number ) {
    
    return get_userdata( $id_number )->display_name;

}

/*  Get Location Name By Location ID book_a_session_get_location_name()
*   
*/

function book_a_session_get_location_name( $id_number ) {
    
    global $wpdb;
    $table_name = $wpdb->prefix . "book_a_session_location";

    $result = $wpdb->get_results(
        "
        SELECT name
        FROM $table_name
        WHERE ID = $id_number
        LIMIT 1        
        "
    );
	return $result[0]->name;
}

/**
 * $amount = float
 * $id = currency_id
 */
function book_a_session_get_price_tag( $amount, $id_number, $html = false ) {
    
    global $wpdb;
    $table_name = $wpdb->prefix . "book_a_session_currency";

    $result = $wpdb->get_results(
        "
        SELECT *
        FROM $table_name
        WHERE ID = $id_number
        LIMIT 1        
        "
    );

    // Add commas to large prices, also ensure it looks like a price, with 2 decimal places

    $amount = number_format( $amount, 2 );

    $price_label;

    if ( ! empty( $result[0] ) ) {

        if ( ! $html ) {

            if ( $result[0]->symbol_after == 1 ) {

                $result[0]->use_alternative_symbol ? $price_label = $amount . $result[0]->symbol : $price_label = $amount . " " . $result[0]->code; 
        
            } else {
        
                $result[0]->use_alternative_symbol ? $price_label = $result[0]->symbol . $amount : $price_label = $result[0]->code . " " . $amount ; 
        
            }    

        } else {

            if ( $result[0]->symbol_after == 1 ) {

                $result[0]->use_alternative_symbol ? $price_label = "<span class='book-a-session-price-amount'>" . $amount . "</span>" . "<span class='book-a-session-price-symbol'>" . $result[0]->symbol . "</span>" : $price_label = "<span class='book-a-session-price-amount'>" . $amount . "</span>" . "<span class='book-a-session-price-code'>" . $result[0]->code. "</span>"; 
        
            } else {
        
                $result[0]->use_alternative_symbol ? $price_label = "<span class='book-a-session-price-symbol'>" . $result[0]->symbol . "</span>" . "<span class='book-a-session-price-amount'>" . $amount . "</span>" : $price_label = "<span class='book-a-session-price-code'>" . $result[0]->code . "</span>" . "<span class='book-a-session-price-amount'>" . $amount. "</span>"; 
        
            }
    
        }

    } else return $amount;

    return $price_label;

}

/*  Get Payment Method Name By Payment Method ID book_a_session_get_payment_method()
*   
*/

function book_a_session_get_payment_method( $id_number ) {
    
    global $wpdb;
    $table_name = $wpdb->prefix . "book_a_session_payment_method";

    $result = $wpdb->get_results(
        "
        SELECT *
        FROM $table_name
        WHERE ID = $id_number
        LIMIT 1
        "
    );

    $payment_method_name = $result[0]->name;

    return $payment_method_name;

}

/*  Get Service Name By Service ID book_a_session_get_service_name()
*   
*/

function book_a_session_get_service_name( $id_number ) {
    
    global $wpdb;
    $table_name = $wpdb->prefix . "book_a_session_service";

    $result = $wpdb->get_results(
        "
        SELECT name
        FROM $table_name
        WHERE ID = $id_number
        LIMIT 1
        "
    );

    $service_name = $result[0]->name;

    return $service_name;

}

function book_a_session_generate_order_id() {

    return random_int( 1000000000, 9999999999 );

}

function book_a_session_all_truthy_or_falsey( $array ) {
    if ( count( array_unique( $array ) ) === 1 ) {
        return current( $array );
    } else {
        return;
    }
}

// Normalise payment methods if changed. Deprecated and discontinued.
/*

function book_a_session_payment_methods() {

    $payment_methods = array(
        array( 'id' => 1, 'name' => 'PayPal' ),
        array( 'id' => 2, 'name' => 'Cash' ),
        array( 'id' => 3, 'name' => 'Vodafone Cash' ),
        array( 'id' => 4, 'name' => 'Credit / Debit Card (Stripe)' ),
    );

    global $wpdb; 
    $table_name = $wpdb->prefix . "book_a_session_payment_method";

    // Check payment methods
    $sql = "SELECT
    COUNT(*) 
    FROM " . 
    $table_name;

    $count = $wpdb->get_var($sql);


    if ( $count !== count( $payment_methods ) ) {
        
        // Delete everything in the table if the number of payment methods have changed
        $delete_payment_methods_db_result = $wpdb->query("TRUNCATE TABLE $table_name");

        // If that worked
        if ( $delete_payment_methods_db_result ) {

            // Loop through the master payment method values found at the top of this function
            for ( $i = 0; $i < count( $payment_methods ); $i++ ) {

                // Insert payment methods
                $wpdb->insert(
                    $table_name,
                    array(
                        'id'    => $payment_methods[$i]["id"],    
                        'name'  => $payment_methods[$i]["name"]    
                    ),
                    array(
                        '%d',
                        '%s'
                    )
                );

            }

        }

    }

}

*/

function book_a_session_get_ip() {

    if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {

        $ip = $_SERVER['HTTP_CLIENT_IP'];

    } elseif ( ! empty($_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {

        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

    } else {

        $ip = $_SERVER['REMOTE_ADDR'];

    }

    return $ip;

}

function book_a_session_get_country_code() {

    // Set PHP to allow getting from URLs openly if it isn't allowed already
    ini_set("allow_url_fopen", 1);

    // Get current user's IP address
    $ip = book_a_session_get_ip(); 

    // Send that to Geoplugin.net to get back some JSON with the IP address's likely country code (and more)
    $response = file_get_contents( "http://www.geoplugin.net/json.gp?ip=" . $ip );

    // Decode it for PHP use
    $json = json_decode( $response );

    // Return if it worked
    if ( $json->geoplugin_countryCode ) return $json->geoplugin_countryCode;
    
    // Return false on falure
    else return false;

}

function book_a_session_get_countries() {

    $source = "Afghanistan;AF
    Åland Islands;AX
    Albania;AL
    Algeria;DZ
    American Samoa;AS
    Andorra;AD
    Angola;AO
    Anguilla;AI
    Antarctica;AQ
    Antigua And Barbuda;AG
    Argentina;AR
    Armenia;AM
    Aruba;AW
    Australia;AU
    Austria;AT
    Azerbaijan;AZ
    Bahamas;BS
    Bahrain;BH
    Bangladesh;BD
    Barbados;BB
    Belarus;BY
    Belgium;BE
    Belize;BZ
    Benin;BJ
    Bermuda;BM
    Bhutan;BT
    Bolivia, Plurinational State Of;BO
    Bonaire, Sint Eustatius And Saba;BQ
    Bosnia And Herzegovina;BA
    Botswana;BW
    Bouvet Island;BV
    Brazil;BR
    British Indian Ocean Territory;IO
    Brunei Darussalam;BN
    Bulgaria;BG
    Burkina Faso;BF
    Burundi;BI
    Cambodia;KH
    Cameroon;CM
    Canada;CA
    Cape Verde;CV
    Cayman Islands;KY
    Central African Republic;CF
    Chad;TD
    Chile;CL
    China;CN
    Christmas Island;CX
    Cocos (Keeling) Islands;CC
    Colombia;CO
    Comoros;KM
    Congo;CG
    Congo, The Democratic Republic Of The;CD
    Cook Islands;CK
    Costa Rica;CR
    CÔte D'ivoire;CI
    Croatia;HR
    Cuba;CU
    CuraÇao;CW
    Cyprus;CY
    Czech Republic;CZ
    Denmark;DK
    Djibouti;DJ
    Dominica;DM
    Dominican Republic;DO
    Ecuador;EC
    Egypt;EG
    El Salvador;SV
    Equatorial Guinea;GQ
    Eritrea;ER
    Estonia;EE
    Ethiopia;ET
    Falkland Islands (Malvinas);FK
    Faroe Islands;FO
    Fiji;FJ
    Finland;FI
    France;FR
    French Guiana;GF
    French Polynesia;PF
    French Southern Territories;TF
    Gabon;GA
    Gambia;GM
    Georgia;GE
    Germany;DE
    Ghana;GH
    Gibraltar;GI
    Greece;GR
    Greenland;GL
    Grenada;GD
    Guadeloupe;GP
    Guam;GU
    Guatemala;GT
    Guernsey;GG
    Guinea;GN
    Guinea-Bissau;GW
    Guyana;GY
    Haiti;HT
    Heard Island And McDonald Islands;HM
    Holy See (Vatican City State);VA
    Honduras;HN
    Hong Kong;HK
    Hungary;HU
    Iceland;IS
    India;IN
    Indonesia;ID
    Iran, Islamic Republic Of;IR
    Iraq;IQ
    Ireland;IE
    Isle Of Man;IM
    Israel;IL
    Italy;IT
    Jamaica;JM
    Japan;JP
    Jersey;JE
    Jordan;JO
    Kazakhstan;KZ
    Kenya;KE
    Kiribati;KI
    Korea, Democratic People's Republic Of;KP
    Korea, Republic Of;KR
    Kuwait;KW
    Kyrgyzstan;KG
    Lao People's Democratic Republic;LA
    Latvia;LV
    Lebanon;LB
    Lesotho;LS
    Liberia;LR
    Libya;LY
    Liechtenstein;LI
    Lithuania;LT
    Luxembourg;LU
    Macao;MO
    Macedonia, The Former Yugoslav Republic Of;MK
    Madagascar;MG
    Malawi;MW
    Malaysia;MY
    Maldives;MV
    Mali;ML
    Malta;MT
    Marshall Islands;MH
    Martinique;MQ
    Mauritania;MR
    Mauritius;MU
    Mayotte;YT
    Mexico;MX
    Micronesia, Federated States Of;FM
    Moldova, Republic Of;MD
    Monaco;MC
    Mongolia;MN
    Montenegro;ME
    Montserrat;MS
    Morocco;MA
    Mozambique;MZ
    Myanmar;MM
    Namibia;NA
    Nauru;NR
    Nepal;NP
    Netherlands;NL
    New Caledonia;NC
    New Zealand;NZ
    Nicaragua;NI
    Niger;NE
    Nigeria;NG
    Niue;NU
    Norfolk Island;NF
    Northern Mariana Islands;MP
    Norway;NO
    Oman;OM
    Pakistan;PK
    Palau;PW
    Palestine, State Of;PS
    Panama;PA
    Papua New Guinea;PG
    Paraguay;PY
    Peru;PE
    Philippines;PH
    Pitcairn;PN
    Poland;PL
    Portugal;PT
    Puerto Rico;PR
    Qatar;QA
    RÉunion;RE
    Romania;RO
    Russian Federation;RU
    Rwanda;RW
    Saint BarthÉlemy;BL
    Saint Helena, Ascension And Tristan Da Cunha;SH
    Saint Kitts And Nevis;KN
    Saint Lucia;LC
    Saint Martin (French Part);MF
    Saint Pierre And Miquelon;PM
    Saint Vincent And The Grenadines;VC
    Samoa;WS
    San Marino;SM
    Sao Tome And Principe;ST
    Saudi Arabia;SA
    Senegal;SN
    Serbia;RS
    Seychelles;SC
    Sierra Leone;SL
    Singapore;SG
    Sint Maarten (Dutch Part);SX
    Slovakia;SK
    Slovenia;SI
    Solomon Islands;SB
    Somalia;SO
    South Africa;ZA
    South Georgia And The South Sandwich Islands;GS
    South Sudan;SS
    Spain;ES
    Sri Lanka;LK
    Sudan;SD
    Suriname;SR
    Svalbard And Jan Mayen;SJ
    Swaziland;SZ
    Sweden;SE
    Switzerland;CH
    Syrian Arab Republic;SY
    Taiwan, Province Of China;TW
    Tajikistan;TJ
    Tanzania, United Republic Of;TZ
    Thailand;TH
    Timor-Leste;TL
    Togo;TG
    Tokelau;TK
    Tonga;TO
    Trinidad And Tobago;TT
    Tunisia;TN
    Turkey;TR
    Turkmenistan;TM
    Turks And Caicos Islands;TC
    Tuvalu;TV
    Uganda;UG
    Ukraine;UA
    United Arab Emirates;AE
    United Kingdom;GB
    United States;US
    United States Minor Outlying Islands;UM
    Uruguay;UY
    Uzbekistan;UZ
    Vanuatu;VU
    Venezuela, Bolivarian Republic Of;VE
    Viet Nam;VN
    Virgin Islands, British;VG
    Virgin Islands, U.S.;VI
    Wallis And Futuna;WF
    Western Sahara;EH
    Yemen;YE
    Zambia;ZM
    Zimbabwe;ZW";

    // Remove tabs
    $source = trim( preg_replace('/\t+/', '', $source ) );
    // Convert to array
    $source_array = explode( "\n", $source );

    $country_array = array();

    // Split into country code and country name, also stripping tabs and spaces, and compile into a new indexed array of associative arrays
    foreach ( $source_array as $source_country ) {

        $source_country_array = explode( ";", $source_country );
        $name = trim( preg_replace('/\t+/', '', $source_country_array[0] ) );
        $code = trim( preg_replace('/\s+/', '', $source_country_array[1] ) );

        $country_array[] = array( "code" => $code, "name" => $name );

    }

    return $country_array;

}

// Replaces countries in the country table if there aren't as many countries as there should be. This is deprecated and discontinued currently.
/*
function book_a_session_countries() {

    // Get source countries
    $countries = book_a_session_get_countries();
    $count_entries = 0;
    $entries = array();
    global $wpdb; 
    $table_name = $wpdb->prefix . "book_a_session_country";

    // Count table countries
    $sql = "SELECT
    COUNT(*) 
    FROM " . 
    $table_name;

    $count = $wpdb->get_var($sql);
    $insertion_count = 0;

    // If the number of source countries isn't the same as countries in our table
    if ( (int)$count !== (int)count( $countries ) ) {
        
        // Delete everything in the table if the number of countries have changed
        $delete_countries_db_result = $wpdb->query("TRUNCATE TABLE $table_name");

        // If that worked
        if ( $delete_countries_db_result ) {

            // Loop through the source country values
            foreach ( $countries as $country ) {

                // Insert countries into the table
                $result = $wpdb->insert(
                    $table_name,
                    array(
                        'code'  => $country["code"],    
                        'name'  => $country["name"]    
                    ),
                    array(
                        '%s',
                        '%s'
                    )
                );

                if ( ! empty( $result ) ) {
                    $insertion_count++;
                }

            }

        }

    }

    return array( "table_count" => $count, "source_count" => count( $countries ), "insertion_count" => $insertion_count ) ;

}
*/

// Gets top level domain from the URL which can be used for email addresses.

function book_a_session_get_top_level_url() {

    $site_url = str_replace( "www.", "", str_replace( "http://", "", str_replace( "https://", "", site_url() ) ) );
    $site_url = strpos( $site_url, "/" ) ? explode( "/", $site_url )[0] : $site_url;
    
    if ( ! empty( $site_url ) ) {
        return $site_url;
    } else {
        return false;
    }

}


// Create a page for the dashboard



// Booking a session

// Region checking and cookie checking may have to happen directly on the page to have correct information on user. 

// If no region database option exists for logged in user or cookie exists for logged out user, present available regions. Then store region as user option if logged in or cookie if logged out

// Service grid
    
    // Name
    // Image URL
    // Price range

// Store selected service

// Bundle quantities from chosen service

    // If settings allow, calculate savings for each quantity and display

// Locations for region

//  If more than one practitioner, show practitioners

// Present dates starting from the beginning of the client month and one year from current date. Mark dates preceding the current date, also marking the current date, to be used for the calendar.

// Present schedule data, marking sessions that are booked either on the chosen location timetable or the chosen practitioner timetable. Change times according to selected region's timezone.

// Repeat for each session in bundle if chosen bundle quantity is higher than 1, marking session times that were selected previously

// Present payment methods available depending on everything chosen

// If cash is applicable and chosen, go straight to thank you page and display details of the booked sessions

// If vfc is applicable and chosen, ask for vfc mobile number. Once given, go to vfc pending payment page informing customer on how to make vfc. Record order as vfc due and pending payment. Send email to chosen practitioner with invoice and link to report received payment. Once the link is followed, recordi the payment in payment table and change order vfc due and status paid, and booked.

// Otherwise present form to capture payment details according to the chosen configuration and payment method.

// Request payment if online payment method was chosen, and record payment attempt in database. If it's successful, change the record to success and move to thank you page containing booked sessions.

// Record receipt, and email receipt to user and practitioner as long as email settings allow

// Build dashboard
    
    // Upcoming sessions table UI
    // Session history table UI
    // Invoices
    // Email preferences
    // Location directions
    // Online sessions

// Build calendar timetable with box of sessions for admin area

// Build admin settings page

    // Schedule
    // Currency
    // Regions
    // Locations
    // Location Schedule
    // Location Timetable
    // Service Types
    // Services
    // Service Price
    // Practitioner Location
    // Practitioner Schedule
    // Practitioner Timetable
    // Payment Method
    // Payment
    // Invoice
    // Receipt

