<?php // Book A Session - Login

// Exit if file is called directly

if ( ! defined( 'ABSPATH' ) ) exit;

function book_a_session_login( $data ){

    if ( wp_verify_nonce( $data["nonce"], "book-a-session-ajax-login-nonce" ) && ! empty( $data["user_login"] ) && ! empty( $data["user_password"] ) ) {

        $info = array(

            "user_login"    => esc_attr( $data["user_login"] ),
            "user_password" => esc_attr( $data["user_password"] ),
            "remember"      => $data["remember"] == "true" ? true : false,

        );

        $user_signon = wp_signon( $info, is_ssl() );

        if ( ! is_wp_error( $user_signon ) ) {
    
            $user_id = $user_signon->ID;

            wp_set_current_user( $user_id, $info["user_login"] );
            wp_set_auth_cookie( $user_id, $info["remember"], is_ssl(), false );
            do_action( "wp_login", $info["user_login"] );

            return rest_ensure_response(
    
                array(
    
                    "loggedin" => true, 
                    "user_id" => $user_id,
    
                )
            );
    
        } else {
    
            return rest_ensure_response( new WP_Error( __( "We couldn't log you in with those details.", "book_a_session" ) ) );
    
        }

    } else return rest_ensure_response( new WP_Error( __( "We've run into a security error. Please reload and try again.", "book_a_session" ) ) );

}

add_action( 'rest_api_init', function () {

    register_rest_route( 'book-a-session/v1', '/login', array(

        'methods'               => 'POST',
        'callback'              => 'book_a_session_login',

    ) );

} );

function book_a_session_register( $data ){

    if ( wp_verify_nonce( $data["nonce"], "book-a-session-ajax-register-nonce" ) && ! empty( $data["firstname"] ) && ! empty( $data["lastname"] ) && ! empty( $data["email"] ) && ! empty( $data["region_id"] ) && ! empty( $data["password"] ) ) {

        // Check if someone with the same first and last name 

        $info = array(

            "user_pass" => esc_attr( $data["password"] ),
            "user_login" => esc_attr( $data["email"]),
            "user_email"  => esc_attr( $data["email"]),
            "display_name" => esc_attr( $data["firstname"] ) . " " . esc_attr( $data["lastname"] ),
            "first_name" => esc_attr( $data["firstname"] ),
            "last_name" => esc_attr( $data["lastname"] ),
            'show_admin_bar_front' => "false", // For some reason this has to be a string, doesn't accept boolean.

        );

        $register_user = wp_insert_user( $info );

        if ( ! is_wp_error( $register_user ) ) {

            $creds = array(

                "user_login"    => esc_attr( $info["user_login"] ),
                "user_password" => esc_attr( $info["user_pass"] ),
    
            );

            $user_signon = wp_signon( $creds, is_ssl() );

            if ( ! is_wp_error( $user_signon ) ) {
    
                // Get user id

                $user_id = $user_signon->ID;

                // Set their region

                update_user_meta( $user_id, "region_id", intval( $data["region_id"] ) );
    
                wp_set_current_user( $user_id, $creds["user_login"] );
                wp_set_auth_cookie( $user_id, false, is_ssl(), false );
                do_action( "wp_login", $creds["user_login"] );
    
                return rest_ensure_response(
        
                    array(
        
                        "registered" => true,
                        "loggedin" => true, 
                        "user_id" => $user_id,
        
                    )

                );
        
            } else {
        
                return rest_ensure_response( array( "registered" => true, "loggedin" => false, "user_id" => 0 ) );
        
            }

        } else return rest_ensure_response( new WP_Error( __( "We couldn't create your account. Please try again.", "book_a_session" ) ) );

    } else return rest_ensure_response( new WP_Error( __( "We've run into a security error. Please reload and try again.", "book_a_session" ) ) );

}

add_action( 'rest_api_init', function () {

    register_rest_route( 'book-a-session/v1', '/register', array(

        'methods'               => 'POST',
        'callback'              => 'book_a_session_register',

    ) );

} );

function book_a_session_print_login_form( $type = false ){

    $forgot_password_url = isset( get_option( "book_a_session_options_general" )["forgot_password_page_id"] ) ? get_permalink( get_option( "book_a_session_options_general" )["forgot_password_page_id"] ) : false;
    $register_url = isset( get_option( "book_a_session_options_general" )["register_page_id"] ) ? get_permalink( get_option( "book_a_session_options_general" )["register_page_id"] ) : false;

    if ( $type != "modal" ) :

    wp_localize_script( 'book-a-session-js', 'book_a_session_api', array(

        'api_root'                      => esc_url_raw( rest_url() . 'book-a-session/v1/' ),

    ) );

    echo "<div class='book-a-session-login book-a-session-card book-a-session-card-large'>";

    endif;

    ?>

    <form id="book-a-session-login" action="book_a_session_login" method="post">


        <div class="book-a-session-form-fields">

            <h2>Login</h2>

            <fieldset class="book-a-session-login-username">
                <label for="book-a-session-login-username">Email or username</label>
                <input class="book-a-session-animated" id="book-a-session-login-username" type="text" name="book_a_session_login_username">
            </fieldset>

            <fieldset class="book-a-session-login-password">
                <label for="book-a-session-login-password">Password</label>
                <input class="book-a-session-animated" id="book-a-session-login-password" type="password" name="book_a_session_login_password">
            </fieldset>

            <div class='book-a-session-switch-field'>
                <label class='book-a-session-switch'>
                    <input class="book-a-session-animated" type="checkbox" id="book-a-session-login-remember" name="book_a_session_login_remember" value="1">
                    <span class='book-a-session-animated book-a-session-slider'></span>
                </label>
                <label for="book-a-session-login-remember">Remember me</label>
            </div>

        </div>

        <div class="book-a-session-button-row">

            <input class="book-a-session-animated book-a-session-submit-button book-a-session-button-login" type="submit" value="Login" name="book_a_session_login_submit">

            <div class="book-a-session-button-container">
                <a class="book-a-session-animated book-a-session-button book-a-session-low-button book-a-session-button-icon-right book-a-session-forgot-password" href="<?php echo $forgot_password_url; ?>">Forgot password<span class="book-a-session-icon book-a-session-icon-vpn_key"></span></a>
                <a class="book-a-session-animated book-a-session-button book-a-session-low-button book-a-session-button-icon-right book-a-session-register" href="<?php echo $register_url ? $register_url : wp_registration_url(); ?>">Register<span class="book-a-session-icon book-a-session-icon-person_add"></span></a>
            </div>

        </div>

        <?php wp_nonce_field( 'book-a-session-ajax-login-nonce', 'book_a_session_login_security' ); ?>

    </form>     

    <?php
    
    if ( $type != "modal" ) echo "</div>";

}

function book_a_session_print_register_form( $type = false ) {

    $forgot_password_url = isset( get_option( "book_a_session_options_general" )["forgot_password_page_id"] ) ? get_permalink( get_option( "book_a_session_options_general" )["forgot_password_page_id"] ) : false;
    $login_url = isset( get_option( "book_a_session_options_general" )["login_page_id"] ) ? get_permalink( get_option( "book_a_session_options_general" )["login_page_id"] ) : false;
    $dashboard_url = isset( get_option( "book_a_session_options_general" )["dashboard_page_id"] ) ? get_permalink( get_option( "book_a_session_options_general" )["dashboard_page_id"] ) : false;

    if ( $type != "modal" ) :

        wp_localize_script( 'book-a-session-js', 'book_a_session_api', array(

            'api_root' => esc_url_raw( rest_url() . 'book-a-session/v1/' ),
    
        ) );    

        echo "<div class='book-a-session-register book-a-session-card book-a-session-card-large'>";

    endif;

    ?>

    <form id="book-a-session-register" action="book_a_session_register" method="post">

        <div class="book-a-session-form-fields">

            <h2>Register</h2>

            <fieldset class="book-a-session-register-first-name">
                <label for="book-a-session-register-first-name">First name</label>
                <input class="book-a-session-animated" id="book-a-session-register-first-name" type="text" name="book_a_session_register_first_name">
            </fieldset>

            <fieldset class="book-a-session-register-last-name">
                <label for="book-a-session-register-last-name">Last name</label>
                <input class="book-a-session-animated" id="book-a-session-register-last-name" type="text" name="book_a_session_register_last_name">
            </fieldset>
            
            <fieldset class="book-a-session-register-email">
                <label for="book-a-session-register-email">Email</label>
                <input class="book-a-session-animated" id="book-a-session-register-email" type="email" name="book_a_session_register_email">
            </fieldset>

            <?php 

                // Region label and outer select markup

                echo "<fieldset class='book-a-session-register-region'>";
                echo "<label for='book-a-session-register-region'>Region</label>";
                echo "<select name='book_a_session_register_region' id='book-a-session-register-region' required>";

                // Get region array

                $region_array = book_a_session_get_table_array( "region", array( "international", "ASC", "name", "ASC" ) );

                // Get their country code via GeoIP API and use that to select the best region for them

                $country_code = book_a_session_get_country_code();

                // Options

                if ( ! empty( $region_array ) ) {

                    echo "<option disabled>Select your region</option>";

                    // Loop through regions

                    foreach ( $region_array as $region ) {

                        echo "<option value='" . $region->id . "'";

                        if ( ! empty( $country_code ) ) {

                            $region_country = book_a_session_get_table_array( "region_country", false, false, "*", array( "region_id", "=", intval( $region->id ), "country_code", "=", "'" . $country_code . "'" ) );

                            if ( ! empty( $region_country ) ) echo " selected";

                        }

                        if ( ! $region->international ) echo ">" . $region->name . "</option>";
                        else echo ">Somewhere else (" . $region->name . ")</option>";

                    }

                    echo "</select>";
                    echo "</fieldset>";

                }

                ?>


            <fieldset class="book-a-session-register-password">

                <label for="book-a-session-register-password">Password</label>
                <input class="book-a-session-animated" id="book-a-session-register-password" type="password" name="book_a_session_register_password">

            </fieldset> 

            <fieldset class="book-a-session-register-confirm-password">

                <label for="book-a-session-register-confirm-password">Confirm password</label>
                <input class="book-a-session-animated" id="book-a-session-register-confirm-password" type="password" name="book_a_session_register_confirm_password">

            </fieldset> 

        </div>

        <div class="book-a-session-button-row">

            <input class="book-a-session-animated book-a-session-submit-button book-a-session-button-register" type="submit" value="Register" name="book_a_session_register_submit">

            <div class="book-a-session-button-container">
                <a class="book-a-session-animated book-a-session-button book-a-session-low-button book-a-session-button-icon-right book-a-session-forgot-password" href="<?php echo $forgot_password_url; ?>">Forgot password<span class="book-a-session-icon book-a-session-icon-vpn_key"></span></a>
                <a class="book-a-session-animated book-a-session-button book-a-session-low-button book-a-session-button-icon-right book-a-session-login" href="<?php echo $login_url ? $login_url : $dashboard_url; ?>">Login<span class="book-a-session-icon book-a-session-icon-person_add"></span></a>
            </div>

        </div>

        <?php wp_nonce_field( 'book-a-session-ajax-register-nonce', 'book_a_session_register_security' ); ?>

    </form>

    <?php

    if ( $type != "modal" ) echo "</div>";

}

function book_a_session_print_login_register_form( $type ){

    wp_localize_script( 'book-a-session-js', 'book_a_session_api', array(

        'api_root'                      => esc_url_raw( rest_url() . 'book-a-session/v1/' ),

    ) );

    if ( $type === "modal" ) :

    ?>
    
    <div class='book-a-session-modal-login-register book-a-session-animated book-a-session-hidden'>

        <div class='book-a-session-modal-inner-login-register book-a-session-card book-a-session-animated book-a-session-hidden'>

            <div class="book-a-session-flex-row">

                <?php book_a_session_print_login_form( "modal" ); ?>

                <?php book_a_session_print_register_form( "modal" ); ?>

            </div>

            <div class='book-a-session-icon-close book-a-session-modal-ui book-a-session-icon book-a-session-animated'></div>

        </div>

    </div>

    <?php else : ?>

        <div class='book-a-session-login-register book-a-session-card book-a-session-animated'>

            <div class="book-a-session-flex-row">

                <?php book_a_session_print_login_form( "modal" ); ?>

                <?php book_a_session_print_register_form( "modal" ); ?>

            </div>

        </div>

    <?php

    endif;

}

/**
 * Redirects the user to the Book A Session Forgot Password page, as long as the page has been selected in the Book A Session Admin settings area,
 * instead of wp-login.php?action=lostpassword.
 */

function book_a_session_redirect_forgot_password(){

    if ( 'GET' == $_SERVER['REQUEST_METHOD'] ) {

        if ( is_user_logged_in() ) {

            $dashboard_url = isset( get_option( "book_a_session_options_general" )["dashboard_page_id"] ) ? get_permalink( get_option( "book_a_session_options_general" )["dashboard_page_id"] ) : false;

            wp_redirect( $dashboard_url ? $dashboard_url : home_url() );

            exit;

        }
 
        $forgot_password_url = isset( get_option( "book_a_session_options_general" )["forgot_password_page_id"] ) ? get_permalink( get_option( "book_a_session_options_general" )["forgot_password_page_id"] ) : false;

        wp_redirect( $forgot_password_url ? $forgot_password_url : home_url() );

        exit;

    }

}

/**
 * Prints the forgot password form
 */

function book_a_session_print_forgot_password_form(){

    // Request password reset

    wp_localize_script( 'book-a-session-js', 'book_a_session_api', array(

        'api_root' => esc_url_raw( rest_url() . 'book-a-session/v1/' ),

    ) );    

    // Forgot Password Form

    $login_url = isset( get_option( "book_a_session_options_general" )["login_page_id"] ) ? get_permalink( get_option( "book_a_session_options_general" )["login_page_id"] ) : false;
    $register_url = isset( get_option( "book_a_session_options_general" )["register_page_id"] ) ? get_permalink( get_option( "book_a_session_options_general" )["register_page_id"] ) : false;

    ?>

    <div class="book-a-session-forgot-password book-a-session-card book-a-session-card-large">

        <form id="book-a-session-forgot-password" action="<?php echo wp_lostpassword_url(); ?>" method="post">

            <div class="book-a-session-form-fields">

                <h2>Enter your email to reset your password</h2>

                <fieldset class="book-a-session-forgot-password-email">

                    <label for="book-a-session-forgot-password-email">Email</label>
                    <input class="book-a-session-animated" id="book-a-session-forgot-password-email" type="email" name="book_a_session_forgot_password_email">

                </fieldset>


                <div class="book-a-session-button-row">
                    
                    <input class="book-a-session-animated book-a-session-submit-button book-a-session-button-forgot-password" type="submit" value="Send verification link" name="book_a_session_forgot_password_submit">

                    <div class="book-a-session-button-container">
                        <?php if ( $register_url ) echo '<a class="book-a-session-animated book-a-session-button book-a-session-low-button book-a-session-button-icon-right book-a-session-register" href="' . $register_url .'">Register<span class="book-a-session-icon book-a-session-icon-person_add"></span></a>'; ?>
                        <?php if ( $login_url ) echo '<a class="book-a-session-animated book-a-session-button book-a-session-low-button book-a-session-button-icon-right book-a-session-login" href="' . $login_url . '">Login<span class="book-a-session-icon book-a-session-icon-person_add"></span></a>'; ?>
                    </div>

                </div>

            </div>

        </form>

    </div>

    <?php

}

function book_a_session_recover_password( $user_id ) {

    $user_data = get_userdata( $user_id );
 
    // Redefining user_login ensures we return the right case in the email.
    $user_login = $user_data->user_login;
    $user_email = $user_data->user_email;
    $key = get_password_reset_key( $user_data );
 
    if ( is_wp_error( $key ) ) {
        return false;
    }
 
    if ( is_multisite() ) {
        $site_name = get_network()->site_name;
    } else {
        /*
         * The blogname option is escaped with esc_html on the way into the database
         * in sanitize_option we want to reverse this for the plain text arena of emails.
         */
        $site_name = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );
    }

    // Get dashboard URL, with the last forward slash removed
    
    $dashboard_url = preg_replace('{/$}', '', get_permalink( get_option( "book_a_session_options_general" )["dashboard_page_id"] ) );

    $message = __( 'Someone has requested a password reset for your account: ' . $user_email . '.' ) . "\r\n\r\n";
    $message .= __( 'If this was a mistake, just ignore this email and nothing will happen.' ) . "\r\n\r\n";
    $message .= __( 'To reset your password, visit the following address:' ) . "\r\n\r\n";
    $message .= $dashboard_url . "?reset=link&key=$key&login=" . rawurlencode( $user_login ) . "\r\n";
 
    /* translators: Password reset email subject. %s: Site name */
    $title = sprintf( __( '[%s] Password Reset' ), $site_name );
 
    /**
     * Filters the subject of the password reset email.
     *
     * @since 2.8.0
     * @since 4.4.0 Added the `$user_login` and `$user_data` parameters.
     *
     * @param string  $title      Default email title.
     * @param string  $user_login The username for the user.
     * @param WP_User $user_data  WP_User object.
     */
    $title = apply_filters( 'retrieve_password_title', $title, $user_login, $user_data );
 
    /**
     * Filters the message body of the password reset mail.
     *
     * If the filtered message is empty, the password reset email will not be sent.
     *
     * @since 2.8.0
     * @since 4.1.0 Added `$user_login` and `$user_data` parameters.
     *
     * @param string  $message    Default mail message.
     * @param string  $key        The activation key.
     * @param string  $user_login The username for the user.
     * @param WP_User $user_data  WP_User object.
     */
    $message = apply_filters( 'retrieve_password_message', $message, $key, $user_login, $user_data );
 
    if ( $message && !wp_mail( $user_email, wp_specialchars_decode( $title ), $message ) ) {

        return false;

    }
 
    return true;

}

function book_a_session_recover_password_rest( $data ){

    $user_id = get_user_by( "email", $data["email"] )->ID;

    if ( intval( $user_id ) > 0 ) {

        $recover_password = book_a_session_recover_password( $user_id );

        if ( $recover_password ) return rest_ensure_response( __( "Success", "book_a_session" ) );
        else return rest_ensure_response( new WP_Error( __( "Sorry, something went wrong. Please contact the site administrator.", "book_a_session" ) ) );
    
    } else return rest_ensure_response( new WP_Error( __( "Email address may not belong to an account.", "book_a_session" ) ) );

}

add_action( 'rest_api_init', function () {

    register_rest_route( 'book-a-session/v1', '/password', array(

        'methods'               => 'POST',
        'callback'              => 'book_a_session_recover_password_rest',

    ) );

} );

/**
 * Prints their name, a link to their dashboard and a logout link if they're logged in, or the login_register_form if they're logged out
 * 
 * @param   $type   string      Optional parameter to determine whether or not the login_register_form will be used in a modal, which doesn't need
 *                              wp_localize_script to print the Book A Session WP REST API root and nonce for logging in via ajax. If no parameter
 *                              is passed, the API root and nonce will be included, along with a .book-a-session-card container for display on a page.
 * 
 */

function book_a_session_print_logged_in( $type = null ){

    if ( is_user_logged_in() ) {

        $dashboard_url = isset( get_option( "book_a_session_options_general" )["dashboard_page_id"] ) ? get_permalink( get_option( "book_a_session_options_general" )["dashboard_page_id"] ) : false;
        
        echo "<div class='book-a-session-card book-a-session-card-large'>";
        echo "<h4>You're logged in as ";

        if ( $dashboard_url ) echo "<a href='" . $dashboard_url . "' class='book-a-session-animated book-a-session-inline-link'>" . get_userdata( get_current_user_id() )->display_name . "</a>.</h4>";
        else  echo get_userdata( get_current_user_id() )->display_name . ".</h4>";

        echo '<a class="book-a-session-animated book-a-session-button book-a-session-low-button book-a-session-button-icon-right book-a-session-logout" href="' . wp_logout_url( get_permalink() ? get_permalink() : home_url() ) . '">Logout<span class="book-a-session-icon book-a-session-icon-exit_to_app"></span></a>';

        echo "</div>";
        
    } else {

        book_a_session_print_login_register_form( $type );

    }

}

