<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Outputs a timetable displaying any or all sessions with multiple possible criteria.
 * 
 * @param   string  $timetable          Determines the type of timetable and its ID to output. Choices are all | location_(int) | client_(int) | practitioner_(int) | service_(int).
 * 
 * @param   string  $format             Optional string to request different formats for the timetable.
 *                                      Choices are week | calendar | simple | simple_upcoming | simple_history |  simple_current | simple_week | simple_month | simple_year
 *                                      If passed week, returns all upcoming sessions in the current week and the next 11 weeks in a weekly timetable format, with weeks traversable by previous week and next week buttons.
 *                                      If passed calendar, returns all upcoming sessions in the current month and the next 11 months in a classic calendar format, traversable by previous month and next month buttons. 
 *                                      If passed simple_all, returns all sessions, past, present and future, in a simple table format.
 *                                      If passed simple_upcoming, returns all upcoming sessions in a simple table format. 
 *                                      If passed simple_history, returns all past sessions in a simple table format, starting with the latest and ending with the earliest.
 *                                      If passed simple_current, returns sessions currently happening in a simple table format.
 *                                      If passed simple_week, returns upcoming sessions for 7 days from the time requested in a simple table format.
 *                                      If passed simple_month, returns upcoming sessions occuring a month from the time requested in a simple table format.
 *                                      If passed simple_year, returns all upcoming sessions to occur one year from the time requested, in a simple table format.
 * 
 * @param   string  $order              Optional string to denote order of sessions for simple timetables. Choices are ASC | DESC.
 *                                      Week and calendar timetables will always be in the chronological order despite this value. 
 *                                      If passed ASC, displays earliest sessions first and latest sessions last.
 *                                      If passed DESC, orders latest sessions first and earliest sessions last.
 * 
 * @param   int     $limit              Optional integer used to limit the total number of entries to load, for example, passing 10 would return up to 10 sessions.
 *                                      Currently not recommended to be used with Week or Calendar timetables. It would be better to change the number of weeks they load instead. 
 * 
 * @param   string  $timezone           Optional string for simple timetables used to determine which timezone all dates and times should be converted to. Choices are default | region.
 *                                      If passed default, or nothing, times will remain unconverted, reflecting the default timezone entered in Schedule settings or the server's default timezone.
 *                                      If passed region, each order's dates and times will be converted to the local timezone of each order's region.
 * 
 * @return  string  $output             Timetable markup
 * 
 */

function book_a_session_timetable( $timetable = "all", $format = "week", $order = "ASC", $limit = null, $timezone = "default", $region_id = false ) {

    // Prepare to extract timetable type and id if passed a string with the underscore delimiter

    $timetable_string_array = strpos( $timetable, "_" ) ? explode( "_", $timetable ) : false;
    $format_string_array = strpos( $format, "_" ) ? explode( "_", $format ) : array( $format );
    $where_array = null;

    // Default order is ORDERBY start_datetime ASC

    $orderby = "start_datetime";
    $order = ! empty( $order ) && strtoupper( $order ) === "DESC" ? "DESC" : "ASC";

    // Get default limit. Limit only used by simple timetables, so $limit turns to false if it's not a simple timetable.

    $default_limit = get_option( "book_a_session_options_timetables" )["result_limit"] ? get_option( "book_a_session_options_timetables" )["result_limit"] : false;
    $limit = intval( $limit ) ? intval( $limit ) : $default_limit;

    // Boolean timetable attributes used later in the markup output

    $upcoming = false;
    $simple = false;
    $current = false;
    $timecalc = false;
    $history = false;
    $week = $format === "week" ? true : false;
    $calendar = $format === "calendar" ? true : false;
    $datetime_picker = $format === "datetime_picker" ? true : false;
    $timezone = ! empty( $timezone ) && $timezone === "region" ? "region" : "default";
    $region = $timezone === "region" ? true : false;

    // Initialise our return value which will contain the timetable markup

    $output = "";

    // Change the database query if the format string passed contains restrictions.

    if ( ! empty( $format_string_array ) && ! $datetime_picker ) {

        if ( in_array( "simple", $format_string_array, true ) ) {

            // simple

            $simple = true;

            if ( in_array( "upcoming", $format_string_array, true ) ) {

                // simple_upcoming
    
                $where_array = array( "start_datetime", ">=", "NOW()" );
                $upcoming = true;
    
            } elseif ( in_array( "history", $format_string_array, true ) ) {
    
                // simple_history

                $history = true;
    
                if ( in_array( "week", $format_string_array, true ) ) $where_array = array( "start_datetime", ">=", "NOW() - INTERVAL 1 WEEK", "end_datetime", "<=", "NOW()" );
    
                elseif ( in_array( "month", $format_string_array, true ) ) $where_array = array( "start_datetime", ">=", "NOW() - INTERVAL 1 MONTH", "end_datetime", "<=", "NOW()" );
    
                elseif ( in_array( "year", $format_string_array, true ) )  $where_array = array( "start_datetime", ">=", "NOW() - INTERVAL 1 YEAR", "end_datetime", "<=", "NOW()" );
    
                else $where_array = array( "end_datetime", "<=", "NOW()" );
    
            } elseif ( in_array( "current", $format_string_array, true ) ) {

                // simple_current

                $where_array = array( "start_datetime", "<=", "NOW()", "end_datetime", ">=", "NOW()" );
                $current = true;
    
            } elseif ( in_array( "week", $format_string_array, true ) ) {
    
                // simple_week
    
                $where_array = array( "start_datetime", ">", "NOW()", "start_datetime", "<=", "NOW() + INTERVAL 1 WEEK" );
                $upcoming = true;
    
            } elseif ( in_array( "month", $format_string_array, true ) ) {
    
                // simple_month
    
                $where_array = array( "start_datetime", ">=", "NOW()", "start_datetime", "<=", "NOW() + INTERVAL 1 MONTH" );
                $upcoming = true;
    
            } elseif ( in_array( "year", $format_string_array, true ) ) {
    
                // simple_year
    
                $where_array = array( "start_datetime", ">=", "NOW()", "start_datetime", "<=", "NOW() + INTERVAL 1 YEAR" );
                $upcoming = true;
    
            }

        } else $limit = false;

    } else {

        // If $format_string_array is empty, that means there's no underscore, which should be present in all timetable_format attribute values for simple timetables, so we know it has to be either week or calendar, which doesn't use limits

        $limit = false;

    }

    // Get all timetable entries, for all types including locations, clients and practitioners.

    if ( $timetable === "all" ) { 

        $timetable_array = book_a_session_get_table_array( "timetable", $orderby, $order, "*", $where_array, $limit );

    // Get specific entries if the information by which we can find the entries is present

    } elseif ( $timetable_string_array && ! $datetime_picker ) {

        // Extract data from string

        $type = $timetable_string_array[0];
        $id = intval( $timetable_string_array[1] );

        // Get location row and location timetable entries

        if ( $type === "location" ) {

            $location = book_a_session_get_table_array( "location", false, false, "name", array( "id", "=", $id ), 1 )[0];
            $where_array = is_array( $where_array ) ? array_merge( array( "location_id", "=", $id ), $where_array ) : array( "location_id", "=", $id );

            if ( $location && $where_array ) {

                $timetable_array = book_a_session_get_table_array( "timetable", $orderby, $order, "*", $where_array, $limit );

            } else return false;

        // Get service row and service timetable entries

        } elseif ( $type === "service" ) {

            $service = book_a_session_get_table_array( "service", false, false, "name", array( "id", "=", $id ), 1 )[0];
            $where_array = is_array( $where_array ) ? array_merge( array( "service_id", "=", $id ), $where_array ) : array( "service_id", "=", $id );

            if ( $service && $where_array ) {

                $timetable_array = book_a_session_get_table_array( "timetable", $orderby, $order, "*", $where_array, $limit );

            } else return false;

        // Get client WP User Object and client timetable entries

        } elseif ( $type === "client" ) {

            $client = get_userdata( $id );
            $where_array = is_array( $where_array ) ? array_merge( array( "client_id", "=", $id ), $where_array ) : array( "client_id", "=", $id );

            if ( $client && $where_array ) {

                $timetable_array = book_a_session_get_table_array( "timetable", $orderby, $order, "*", $where_array, $limit );
    
            } else return false;

        // Get practitioner WP User Object and practitioner timetable entries

        } elseif ( $type === "practitioner" ) {

            $practitioner = is_array( get_userdata( $id )->roles ) && in_array( "practitioner", get_userdata( $id )->roles ) || is_array( get_userdata( $id )->roles ) && in_array( "admin_practitioner", get_userdata( $id )->roles ) ? get_userdata( $id ) : false;
            $where_array = is_array( $where_array ) ? array_merge( array( "practitioner_id", "=", $id ), $where_array ) : array( "practitioner_id", "=", $id );

            if ( $practitioner && $where_array ) {

                $timetable_array = book_a_session_get_table_array( "timetable", $orderby, $order, "*", $where_array, $limit );
    
            } else return false;

        }

    // Datetime picker timetables are used on the frontend form, if this is passed, we're looking for practitioner and location timetables combined

    } elseif ( $timetable_string_array && $datetime_picker ) {

        // If location was included in the timetable type value

        if ( in_array( "location", $timetable_string_array, true ) ) {

            // Look for the next value in the array which should be the ID of the location. Use that to build on the where array to be used to query the database

            $location_id = $timetable_string_array[ array_search( "location", $timetable_string_array ) + 1 ];
            $location = book_a_session_get_table_array( "location", false, false, "name", array( "id", "=", $location_id ), 1 )[0];
            $where_array = is_array( $where_array ) ? array_merge( array( "location_id", "=", $location_id ), $where_array ) : array( "location_id", "=", $location_id );

        }

        // If practitioner was included in the timetable type value

        if ( in_array( "practitioner", $timetable_string_array, true ) ) {

            // Look for the next value in the array which should be the ID of the location. Use that to build on the where array to be used to query the database

            $practitioner_id = $timetable_string_array[ array_search( "practitioner", $timetable_string_array ) + 1 ];
            $practitioner = is_array( get_userdata( $practitioner_id )->roles ) && in_array( "practitioner", get_userdata( $practitioner_id )->roles ) || is_array( get_userdata( $practitioner_id )->roles ) && in_array( "admin_practitioner", get_userdata( $practitioner_id )->roles ) ? get_userdata( $practitioner_id ) : false;
            $where_array = is_array( $where_array ) ? array_merge( array( "practitioner_id", "=", $practitioner_id ), $where_array ) : array( "practitioner_id", "=", $practitioner_id );

        }

        // If practitioner was included in the timetable type value

        if ( in_array( "user", $timetable_string_array, true ) ) {

            // Look for the next value in the array which should be the ID of the location. Use that to build on the where array to be used to query the database

            $user_id = $timetable_string_array[ array_search( "user", $timetable_string_array ) + 1 ];
            $user = get_userdata( $user_id );
            $where_array = is_array( $where_array ) ? array_merge( array( "OR", "client_id", "=", $user_id ), $where_array ) : array( "OR", "client_id", "=", $user_id );

        }

        // If the supplied data is valid and the where array is present, get the combined practitiioner location timetable

        if ( $location && $practitioner && $user && $where_array ) {

            $timetable_array = book_a_session_get_table_array( "timetable", $orderby, $order, "*", $where_array, $limit );

        } else return false;

    // Either "all" wasn't passed, or no underscore was found to denote the type of timetable requested and its ID, so the function will return false

    } else {

        return false;

    }

    // Now after having gotten the right data, create an array with timetable data and order data merged

    $merged_timetable_array = array();

    // Do not convert times if requested

    $timezone_default = $timezone !== "default" ? false : true;

    if ( ! empty( $timetable_array ) ) {

        // Loop through the original timetable array

        foreach ( $timetable_array as $entry ) {

            // Get order data for each timetable entry that has an order id

            $order = $entry->order_id ? book_a_session_get_table_array( "order", "created", "ASC", "*", array( "order_id", "=", $entry->order_id ), 1 )[0] : null;

            // If this entry relates to an existing order, the array entry will include all order details. As for other entries (such as unavailability entries that signify the system, a practitioner or a location is busy for that time), we include basic data

            if ( $order ) {

                // Use supplied region ID primarily, if that wasn't supplied, then check if they want to use the default timezone or the order's region's timezone, determining the correct region to use in each case

                if ( $region_id ) {

                    $region = $region_id;

                } elseif ( ! $timezone_default ) {

                    $region = $order->region_id;

                } else {

                    $region = false;

                }

                // Get timezone converted dates, times, and datetime objects

                $datetime = book_a_session_get_session_time( $entry->schedule_id, $region, $entry->date );
                
                // Check if session is upcoming

                $current_datetime = book_a_session_convert_datetime( new DateTime(), false, $region );

                $session_upcoming = $current_datetime < $datetime["end_datetime_object"] ? true : false;

                // Check if session is currently occuring

                $session_current = $current_datetime >= $datetime["start_datetime_object"] && $current_datetime <= $datetime["end_datetime_object"] ? true : false;

                // Compile the new merged array

                $merged_timetable_array[] = array( 

                    "date"                  => $datetime["start_datetime_object"]->format( get_option( 'date_format' ) ), 
                    "time"                  => $datetime["time_string"],
                    "start_datetime"        => $datetime["start_datetime_object"]->format( "Y-m-d H:i:s" ),
                    "end_datetime"          => $datetime["end_datetime_object"]->format( "Y-m-d H:i:s" ),
                    "time_string_array"     => $datetime["time_string_array"],
                    "start_datetime_object" => $datetime["start_datetime_object"],
                    "end_datetime_object"   => $datetime["end_datetime_object"],
                    "time_string_array"     => $datetime["time_string_array"],
                    "upcoming"              => $session_upcoming,
                    "current"               => $session_current,
                    "unavailable"           => $entry->unavailable,
                    "timetable_id"          => $entry->id,
                    "schedule_id"           => $entry->schedule_id,
                    "order_id"              => $entry->order_id,
                    "location_id"           => $entry->location_id,
                    "practitioner_id"       => $entry->practitioner_id,
                    "client_id"             => $entry->client_id,
                    "created"               => $order->created,
                    "quantity"              => $order->quantity,
                    "order_id"              => $order->order_id,
                    "service_id"            => $order->service_id,
                    "region_id"             => $order->region_id,
                    "currency_id"           => $order->currency_id,
                    "total_due"             => $order->total_due,
                    "amount_paid"           => $order->amount_paid,
                    "payment_method_id"     => $order->payment_method_id,
                    "vfc_mobile"            => $order->vfc_mobile,
                    "booking_status"        => $order->booking_status,
                    "payment_status"        => $order->payment_status,
                    "note"                  => $order->note

                );  

                // Otherwise, get unavailability data

            } elseif ( $entry->unavailable ) {

                // Get dates, times, and datetime objects

                $datetime = book_a_session_get_session_time( $entry->schedule_id, $region, $entry->date );

                $entry_service_id       = $entry->service_id        ? $entry->service_id        : null;
                $entry_location_id      = $entry->location_id       ? $entry->location_id       : null;
                $entry_practitioner_id  = $entry->practitioner_id   ? $entry->practitioner_id   : null;
                $entry_client_id        = $entry->client_id         ? $entry->client_id         : null;

                // Compile the new merged array

                $merged_timetable_array[] = array( 

                    "date"                  => $datetime["start_datetime_object"]->format( get_option( 'date_format' ) ), 
                    "time"                  => $datetime["time_string"],
                    "start_datetime"        => $datetime["start_datetime_object"]->format( "Y-m-d H:i:s" ),
                    "end_datetime"          => $datetime["end_datetime_object"]->format( "Y-m-d H:i:s" ),
                    "time_string_array"     => $datetime["time_string_array"],
                    "start_datetime_object" => $datetime["start_datetime_object"],
                    "end_datetime_object"   => $datetime["end_datetime_object"],
                    "time_string_array"     => $datetime["time_string_array"],
                    "schedule_id"           => $entry->schedule_id,
                    "service_id"            => $entry_service_id,
                    "location_id"           => $entry_location_id,
                    "practitioner_id"       => $entry_practitioner_id,
                    "client_id"             => $entry_client_id,
                    "unavailable"           => $entry->unavailable,

                );  

            }

        }

    }

    // Simple timetable

    if ( $simple ) {

        // Build table head, conditionally hiding any columns that would display obsolete information, e.g. if the timetable type is service, the service name column isn't printed to save horizontal space. 
    
        $output .= "<table class='book-a-session-table book-a-session-timetable book-a-session-simple-timetable'><thead>";
        $output .= "<tr>";
        $output .= "<th scope='col' class='book-a-session-column-date'>Date</th>";
        $output .= "<th scope='col' class='book-a-session-column-time'>Time</th>";
        $output .= ! empty( $type ) && $type === "location" ? "" : "<th scope='col' class='book-a-session-column-location'>Location</th>";
        $output .= ! empty( $type ) && $type === "service" ? "" : "<th scope='col' class='book-a-session-column-service'>Service</th>";
        $output .= ! empty( $type ) && $type === "practitioner" ? "" : "<th scope='col' class='book-a-session-column-practitioner'>Practitioner</th>";
        $output .= ! empty( $type ) && $type === "client" ? "" : "<th scope='col' class='book-a-session-column-client'>Client</th>";
        $output .= "</tr>";
        $output .= "</thead><tbody>";

        // Build table body and insert any entries we have.

        if ( ! empty( $merged_timetable_array ) ) {

            foreach ( $merged_timetable_array as $entry ) {

                $output .= "<tr>";
                $output .= "<td class='book-a-session-column-date'>" . $entry["date"] . "</td>";
                $output .= "<td class='book-a-session-column-time'>" . $entry["time"] . "</td>";
                $output .= ! empty( $type ) && $type === "location" ? "" : "<td class='book-a-session-column-location'>" . book_a_session_get_location_by_id( array( "id" => $entry["location_id"] ) )["name"] . "</td>";
                $output .= ! empty( $type ) && $type === "service" ? "" : "<td class='book-a-session-column-service'>" . book_a_session_get_service_by_id( array( "id" => $entry["service_id"] ) )["name"] . "</td>";
                $output .= ! empty( $type ) && $type === "practitioner" ? "" : "<td class='book-a-session-column-practitioner'>" . get_userdata( $entry["practitioner_id"] )->first_name . " " . get_userdata( $entry["practitioner_id"] )->last_name ."</td>";
                $output .= ! empty( $type ) && $type === "client" ? "" : "<td class='book-a-session-column-client'>" . get_userdata( $entry["client_id"] )->first_name . " " . get_userdata( $entry["client_id"] )->last_name ."</td>";
                $output .= "</tr>";    
    
            }

        } elseif ( $current ) {

            $output .= "<tr><td colspan='5'>No sessions currently underway.</td></tr>";

        } elseif ( $upcoming ) {

            $output .= "<tr><td colspan='5'>No upcoming sessions found.</td></tr>";

        } else {

            $output .= "<tr><td colspan='5'>No sessions found.</td></tr>";

        }
        
        $output .= "</tbody></table>";

    } else {

        if ( $week ) {

            // Get the user specified number of weeks to load for this kind of timetable, the default being 12 weeks.

            $total_weeks = intval( get_option( "book_a_session_options_timetables" )["week_load"] ) ? intval( get_option( "book_a_session_options_timetables" )["week_load"] ) : 12;

            // Set the start date as this week's monday, and set the end date for 12 weeks from then by default, or however many weeks the user has specified ( 7 days * 12 weeks = 84 days, minus 1 being today )

            $total_days = ( $total_weeks * 7 ) - 1;

            $date = date( "Y-m-d" );
            $date = date( "Y-m-d", strtotime( "this week monday", strtotime( $date ) ) );         
            $end_date = date( "Y-m-d", strtotime( "+$total_days days", strtotime( $date ) ) );

            // Get schedule

            $schedule_array = book_a_session_get_table_array( "schedule", "id", "ASC", "id" );

            // Card container, styles the timetable as a card, expanding from its parent container while the content still aligns with the rest of the page content

            $output .= "<div class='book-a-session-card book-a-session-card-expanded book-a-session-week-timetable-card book-a-session-timetable-start'>";

            // Scroll tracker top

            $output .= "<div class='book-a-session-scroll-tracker book-a-session-animated book-a-session-scroll-tracker-top' style='width: calc( 100% / $total_weeks );'></div>";

            // Previous button

            $output .= "<div class='book-a-session-circle-button book-a-session-prev-button book-a-session-animated book-a-session-hidden'>❮</div>";

            // Timtetable wrap, used to hide overflow

            $output .= "<div class='book-a-session-week-timetable-wrap'>";

            // Timetable container, contains all week groups, width being 100% * number of weeks

            $output .= "<div class='book-a-session-timetable book-a-session-animated book-a-session-week-timetable' style='width: calc( 100% * $total_weeks );'>";

            // Initialise counters

            $day_counter = 0;
            $day_of_week_counter = 0;
            $week_counter = 0;

            // Loop through days as long as we haven't gotten to the end date, by default, 83 days later ( 7 days * 12 weeks ) - 1 day being today = 83 days )

            while ( strtotime( $date ) <= strtotime( $end_date ) ) {

                // Check for start of the week by counting days and checking to see if the number divides by 7 cleanly

                if ( $day_counter % 7 === 0 ) {

                    // If it's the start of a week, create a week container and a month label

                    $week_counter++;
                    $day_of_week_counter = 0;
                    $month_at_weekend = date( "F", strtotime( "+7 days", strtotime( $date ) ) );
                    $year_at_weekend = date( "Y", strtotime( "+7 days", strtotime( $date ) ) );
                    $label_month = date( "F", strtotime( $date ) ) !== $month_at_weekend ? date( "F", strtotime( $date ) ) . " - " . $month_at_weekend : date( "F", strtotime( $date ) );
                    $label_month .= "<span class='book-a-session-week-timetable-label book-a-session-week-timetable-label-year'>" . date( "Y", strtotime( $date ) );
                    $label_month .= date( "Y", strtotime( $date ) ) !== $year_at_weekend ? " - " . $year_at_weekend . "</span>" : "</span>";
                    $output .= "<div class='book-a-session-week-timetable-container-week ";
                    $output .= $day_counter > 0 ? "book-a-session-hidden book-a-session-animated " : "book-a-session-animated ";
                    $output .= "book-a-session-week-timetable-week-$week_counter' style='width: calc( 100% / $total_weeks );'>";
                    $output .= "<div class='book-a-session-week-timetable-label book-a-session-week-timetable-label-month'>$label_month</div>";

                } 

                // Tick counters forward

                $day_counter++;
                $day_of_week_counter++;

                // Date container

                $output .= "<div class='book-a-session-week-timetable-container-date";
                $output .= strtotime( $date ) < strtotime( "today" ) && strtotime( $date ) !== strtotime( "today" ) ? " book-a-session-week-timetable-date-past" : " book-a-session-week-timetable-date-future";
                $output .= " book-a-session-week-timetable-month-" . strtolower( date( "M", strtotime( $date ) ) );
                $output .= " book-a-session-week-timetable-day-";
                $output .= strtolower( date( "D", strtotime( $date ) ) );
                $output .= strtotime( $date ) === strtotime( "today" ) ? " book-a-session-week-timetable-date-today'>" : "'>";

                // Date header container

                $output .= "<div class='book-a-session-week-timetable-container-date-header'>";

                // Day label

                $output .= "<span class='book-a-session-week-timetable-label book-a-session-week-timetable-label-day book-a-session-week-timetable-label-" . strtolower( date( "D", strtotime( $date ) ) ) ."'>" . date( "l", strtotime( $date ) ) . "</span>";
               
                // Date label

                $output .= "<span class='book-a-session-week-timetable-label book-a-session-week-timetable-label-date'>" . date( "j", strtotime( $date ) );

                // Date suffix label
                
                $output .= "<span class='book-a-session-week-timetable-label book-a-session-week-timetable-label-date-suffix'>" .date( "S", strtotime( $date ) ) . "</span></span>";

                // End date header container

                $output .= "</div>";

                // Loop through session times according to schedule

                if ( ! empty( $schedule_array ) ) {

                    // Schedule container

                    $output .= "<div class='book-a-session-week-timetable-container-schedule'>";

                    foreach ( $schedule_array as $session_time ) {

                        $matched_sessions = array();

                        // Loop through timetable array and look for any sessions occuring for this date and timeslot, adding them to an array if found

                        foreach ( $merged_timetable_array as $session ) {

                            if ( $session["schedule_id"] == $session_time->id && date( "Y-m-d", strtotime( $session["date"] ) ) == $date ) {

                                $matched_sessions[] = $session;

                            }

                        }

                        if ( ! empty( $matched_sessions ) ) {

                            // List the number of found sessions and a hover popup

                            $output .= "<div class='book-a-session-week-timetable-session-time book-a-session-animated book-a-session-container-tooltip ";
                            $output .= "book-a-session-week-timetable-session-time-" . $session_time->id . " ";
                            $output .= count( $matched_sessions ) > 1 ? "book-a-session-week-timetable-session-multiple'>" : "book-a-session-week-timetable-session-single'>";

                            // Session time label

                            $output .= "<span class='book-a-session-week-timetable-label book-a-session-week-timetable-label-session-time'>" . $matched_sessions[0]["time"] . "</span>";

                            // Session count label

                            $session_count_label = count( $matched_sessions ) > 1 ? count( $matched_sessions ) : "&#10003;";

                            $output .= "<span class='book-a-session-week-timetable-label book-a-session-week-timetable-label-session-count'>" . $session_count_label . "</span>";

                            // Tooltip

                            if ( strtotime( $date ) >= strtotime( "today" ) ) {

                                $match_count = count( $matched_sessions );

                                $output .= "<span class='book-a-session-animated book-a-session-label-tooltip'>";
                                $output .= $match_count > 1 ? "<header><strong>" . $match_count . " sessions</strong></header>" : "<header><strong>Session</strong></header>";
                                $output .= "<ul>";
    
                                $tooltip_session_counter = 0;
                                $tooltip_session_max = get_option( "book_a_session_options_timetables" )["tooltip_max"] ? get_option( "book_a_session_options_timetables" )["tooltip_max"] : 99;
    
                                foreach ( $matched_sessions as $matched_session ) {
    
                                    if ( $tooltip_session_counter >= $tooltip_session_max && $match_count >= $tooltip_session_max ) {

                                        $remaining = (int)$match_count - $tooltip_session_max;
    
                                        $output .= "<li>" . $remaining . " more...</li>";
                                        break;
    
                                    }
    
                                    $service_name = book_a_session_get_table_array( "service", false, false, "name", array( "id", "=", $matched_session["service_id"] ), 1 )[0]->name;
                                    $location_name = book_a_session_get_table_array( "location", false, false, "name", array( "id", "=", $matched_session["location_id"] ), 1 )[0]->name;
                                    $client_name = get_userdata( $matched_session["client_id"] )->first_name . " " . get_userdata( $matched_session["client_id"] )->last_name;
                                    $practitioner_name = get_userdata( $matched_session["practitioner_id"] )->first_name . " " . get_userdata( $matched_session["practitioner_id"] )->last_name;
    
                                    $output .= "<li>";
                                    $output .= "<span class='book-a-session-week-timetable-label book-a-session-label-location'>" . $location_name . "</span>";
                                    $output .= "<span class='book-a-session-week-timetable-label book-a-session-label-hyphen'> - </span>";
                                    $output .= "<span class='book-a-session-week-timetable-label book-a-session-label-service'>" . $service_name . "</span><br>";
                                    $output .= "<span class='book-a-session-week-timetable-label book-a-session-label-for'>For</span> ";
                                    $output .= "<span class='book-a-session-week-timetable-label book-a-session-label-clientname'>" . $client_name . "</span>";
                                    $output .= "<span class='book-a-session-week-timetable-label book-a-session-label-with'> with </span>";
                                    $output .= "<span class='book-a-session-week-timetable-label book-a-session-label-practitionername'>" . $practitioner_name . "</span>.<br>";
                                    $output .= "<span class='book-a-session-week-timetable-label book-a-session-label-due'>Due:</span> ";
                                    $output .= "<span class='book-a-session-week-timetable-label book-a-session-label-duevalue'>" . book_a_session_get_price_tag( $matched_session["total_due"], $matched_session["currency_id"] ) . "</span><br>";
                                    $output .= "<span class='book-a-session-week-timetable-label book-a-session-label-paid'>Paid:</span> ";
                                    $output .= "<span class='book-a-session-week-timetable-label book-a-session-label-paidvalue'>" . book_a_session_get_price_tag( $matched_session["amount_paid"], $matched_session["currency_id"] ) . "</span><br>";
                                    $output .= "<span class='book-a-session-week-timetable-label book-a-session-label-bookingstatus'>" . $matched_session["booking_status"] . "</span>";
                                    $output .= "<span class='book-a-session-week-timetable-label book-a-session-label-colon'>:</span> ";
                                    $output .= "<span class='book-a-session-week-timetable-label book-a-session-label-paymentstatus'>" . $matched_session["payment_status"] . "</span>.";
                                    $output .= "<br><a href='javascript:void(0)' data-session-order='" . $matched_session["order_id"] . "' class='book-a-session-button book-a-session-animated book-a-session-dark-button book-a-session-mini-button book-a-session-button-icon book-a-session-button-icon-right book-a-session-view-order'>Order #" . $matched_session["order_id"] . "<span class='book-a-session-icon book-a-session-icon-chevron-right'></span></a>";
                                    $output .= "</li>";
    
                                    $tooltip_session_counter++;
    
                                }
    
                                $output .= "</ul>";
                                $output .= "</span>";    
        
                            }

                            // End session time

                            $output .= "</div>";
    
                        } else {

                            // If no sessions are found complete the rest of the container
                            
                            $output .= "<div class='book-a-session-week-timetable-session-time ";
                            $output .= "book-a-session-week-timetable-session-time-" . $session_time->id . " ";
                            $output .= "book-a-session-week-timetable-session-none'>";

                            // Session time label

                            $output .= "<span class='book-a-session-week-timetable-label book-a-session-week-timetable-label-session-time'>" . book_a_session_get_session_time( $session_time->id )["time_string"] . "</span>";

                            // Session count label

                            $output .= " <span class='book-a-session-week-timetable-label book-a-session-week-timetable-label-session-count'>0</span></div>";

                        }

                    }

                    // End schedule container

                    $output .= "</div>";

                }

                // End date-container

                $output .= "</div>";

                // End week container

                if ( $day_counter % 7 === 0 ) $output .= "</div>";

                // Tick forward to the next day

                $date = date( "Y-m-d", strtotime( "+1 day", strtotime( $date ) ) );

            }

            // End timetable container

            $output .= "</div>";

            // End timetable wrap

            $output .= "</div>";

            // Next button

            $output .= "<div class='book-a-session-circle-button book-a-session-animated book-a-session-next-button'>❯</div>";


            // End card wrap

            $output .= "</div>";

        } elseif ( $calendar ) {

            // TO DO: Set up region mapping to practitioners and clients. Then if this timetable is for either a client or practitioner, check if their selected region has a sunday week or not, changing the column order accordingly.

            $days_of_week = array( "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" );
            $total_months = intval( get_option( "book_a_session_options_timetables" )["month_load"] ) ? intval( get_option( "book_a_session_options_timetables" )["month_load"] ) : 12;
            $tooltip_session_max = intval( get_option( "book_a_session_options_timetables" )["tooltip_max"] ) ? intval( get_option( "book_a_session_options_timetables" )["tooltip_max"] ) : 99;

            // Set the start date as the first day of the month, then set to the previous monday even if it starts on monday, so as to ensure there are always 6 rows of weeks for consistency between months regardless of the day of the week each month starts 

            $current_month = date( "F" );
            $current_year = date( "Y" );
            $start_date = date( "Y-m-d" );
            $date = date( "Y-m-d", strtotime( "first day of this month", strtotime( $start_date ) ) );   
            $date = date( "Y-m-d", strtotime( "last sunday", strtotime( $date ) ) );  
            $date = date( "Y-m-d", strtotime( "this monday", strtotime( $date ) ) );  

            
            // Set the end date as n months from then, then set to the last day of the month and then next sunday even if it starts on a sunday already, so there are always 6 weeks displayed for every month for consistency

            $end_date = date( "Y-m-d", strtotime( "+" . $total_months - 1 . " months", strtotime( $start_date ) ) );
            $end_date = date( "Y-m-d", strtotime( "last day of this month", strtotime( $end_date ) ) );
            $end_date = date( "Y-m-d", strtotime( "next monday", strtotime( $end_date ) ) );
            $end_date = date( "Y-m-d", strtotime( "this sunday", strtotime( $end_date ) ) );

            // Card container, styles the timetable as a card, expanding from its parent container while the content still aligns with the rest of the page content

            $output = "<div class='book-a-session-card book-a-session-card-expanded book-a-session-calendar-timetable-card book-a-session-timetable-start'>";
       
            // Scroll tracker top

            $output .= "<div class='book-a-session-scroll-tracker book-a-session-animated book-a-session-scroll-tracker-top' style='width: calc( 100% / $total_months );'></div>";

            // Timtetable wrap, used to hide overflow

            $output .= "<div class='book-a-session-calendar-timetable-wrap'>";

            // Timetable container, contains all week groups, width being 100% * 12 (number of weeks)

            $output .= "<div class='book-a-session-timetable book-a-session-animated book-a-session-calendar-timetable' style='width: calc( 100% * $total_months );'>";

            // Initialise counters

            $day_counter = 0;
            $day_of_week_counter = 0;
            $week_counter = 0;
            $month_counter = 0;

            // Loop through days as long as we haven't gotten to the end date, 83 days later ( 7 days * 12 weeks = 84 days )

            while ( strtotime( $date ) <= strtotime( $end_date ) ) {

                // Check if it's the start of a week

                if ( $day_counter % 7 === 0 ) {

                    // Check for start of the month (As explained above, each month will have 42 days (7 days in a week * 6 rows of weeks in a month), with the extra days filled by previous month days and next month days to ensure a consistent layout despite the day of the week a month starts)

                    if ( $day_counter % 42 === 0 ) {

                        $month_counter++;

                        if ( $month_counter > 1 ) {

                            // If we've somehow miscalculated and gone further than the number of months we were meant to (which happens on some months, but not all), just stop and close up the tags, we're done!

                            if ( $month_counter > $total_months ) break;

                            // Go back to the first of this month and the last monday for every month after the first, to fill in all week rows of each month

                            $date = date( "Y-m-d", strtotime( "first day of this month", strtotime( $date ) ) );   
                            $date = date( "Y-m-d", strtotime( "last monday", strtotime( $date ) ) );              

                        }

                        // Get name of previous month. If it's the first month, we need to use strtotime to go back one month. Otherwise, we can use the current month before we change it to the next month

                        $previous_month = $month_counter > 1 ? $current_month : date( "F", strtotime( "-1 month", strtotime( $start_date ) ) );

                        // Update current month. We use two ifs here for for plural handling, though that may not be necessary

                        if ( $month_counter === 2 ) $current_month = date( "F", strtotime( "+1 month", strtotime( $start_date ) ) );
                        if ( $month_counter > 2 ) $current_month = date( "F", strtotime( "+" . $month_counter-1 . " months", strtotime( $start_date ) ) );

                        // Get next month by calculating from the start date

                        $next_month =  date( "F", strtotime( "+" . $month_counter . " months", strtotime( $start_date ) ) );

                        // Change $current_year when necessary

                        if ( $current_month === "January" ) $current_year = $current_year === date( "Y", strtotime( "+" . $month_counter + 1 . " months", strtotime( $start_date ) ) ) ? $current_year : date( "Y", strtotime( "+" . $month_counter + 1 . " months", strtotime( $start_date ) ) );

                        // Create a month container
    
                        $output .= "<div class='book-a-session-calendar-timetable-container-month book-a-session-animated ";

                        // Month class

                        $output .= "book-a-session-calendar-timetable-month-" . strtolower( $current_month ) . " ";

                        // Hidden class by default if not the first month

                        if ( $month_counter > 1 ) $output .= "book-a-session-hidden ";

                        // Month number class

                        $output .= "book-a-session-calendar-timetable-month-$month_counter ";

                        // Width inline style

                        $output .= "' style='width: calc( 100% / $total_months );'>";

                        // Create month header, containing previous month button, month label, and next month button

                        $output .= "<div class='book-a-session-calendar-timetable-header-month book-a-session-calendar-timetable-header-month-" . strtolower( $current_month ) . "'>";

                        // Previous month button

                        $output .= "<div class='book-a-session-low-button book-a-session-button-icon book-a-session-button-icon-left book-a-session-button-icon-left-no-text ";

                        // Hidden class if it's the first month

                        if ( $month_counter === 1 ) $output .= "book-a-session-hidden ";
                        $output .= "book-a-session-animated book-a-session-previous-month-button'>";

                        // Left chevron emoji (are these causing Wordpress extra work by loading some Emoji handling script? Network tab in DevTools seems to be suggesting so.. look at this later, we can just use an Icomoon font later anyway)

                        $output .= "<span class='book-a-session-icon'>❮</span>";

                        // Previous month text

                        $output .= "</div>";

                        // Current month label

                        $output .= "<span class='book-a-session-calendar-timetable-label book-a-session-label-month'>" . $current_month . " ";
                        
                        // Year label, inside current month label
                        
                        $output .= "<span class='book-a-session-calendar-timetable-label book-a-session-label-year'>" . $current_year . "</span></span>";

                        // Next month button

                        $output .= "<div class='book-a-session-low-button book-a-session-button-icon book-a-session-button-icon-right book-a-session-button-icon-right-no-text ";

                        // Hidden class if it's the last month

                        if ( $month_counter === $total_months ) $output .= "book-a-session-hidden ";
                        $output .= "book-a-session-animated book-a-session-next-month-button'>";

                        // Next month text

                        //$output .= $next_month;

                        // Right chevron emoji (are these causing Wordpress extra work by loading some Emoji handling script? Network tab in DevTools seems to be suggesting so.. look at this later, we can just use an Icomoon font later anyway)

                        $output .= "<span class='book-a-session-icon'>❯</span></div>";

                        // End month header

                        $output .= "</div>";

                        // Create Week header with days of the week

                        $output .= "<div class='book-a-session-calendar-timetable-header-week'>";

                        // Loop through hard coded days

                        foreach ( $days_of_week as $day ) $output .= "<div class='book-a-session-calendar-timetable-label book-a-session-label-day'>" . $day . "</div>";

                        $output .= "</div>";
    
                    }

                    // If it's the start of a week, create a week container

                    $week_counter++;
                    $day_of_week_counter = 0;
                    $output .= "<div class='book-a-session-calendar-timetable-container-week ";
                    $output .= "book-a-session-calendar-timetable-week-$week_counter'>";

                } 

                // Tick counters forward

                $day_counter++;
                $day_of_week_counter++;

               
                $matched_sessions = array();

                // Loop through timetable array and look for any sessions occuring for this date, adding them to an array if found

                foreach ( $merged_timetable_array as $session ) {

                    if ( date( "Y-m-d", strtotime( $session["date"] ) ) == $date ) {

                        // TO DO: Look at improving performance by putting the below tooltip construction in here, to avoid an extra loop. It looks like we may lose out on getting
                        // a count of matched sessions, which will mean we will lose out on use of classes such as session-single and session-multiple, though there may be a workaround 
                        // we could figure out by storing that information somewhere without impacting performance as much as we do currently, though I'm not sure without experimenting.
                        // Despite the benefits this might bring, it may be a task for a later update.

                        $matched_sessions[] = $session;

                    }

                }

                if ( ! empty( $matched_sessions ) ) {

                    $match_count = count( $matched_sessions );

                    // Date container

                    $output .= "<div class='book-a-session-calendar-timetable-container-date book-a-session-animated book-a-session-container-tooltip ";

                    // Not month or this month class

                    $output .= $current_month !== date( "F", strtotime( $date ) ) ? "book-a-session-calendar-timetable-not-month " : "book-a-session-calendar-timetable-this-month ";

                    // Single or multiple session class

                    $output .=  $match_count > 1 ? "book-a-session-calendar-timetable-session-multiple" : "book-a-session-calendar-timetable-session-single";

                    // Past class

                    $output .= strtotime( $date ) < strtotime( "today" ) && strtotime( $date ) !== strtotime( "today" ) ? " book-a-session-calendar-timetable-date-past" : " book-a-session-calendar-timetable-date-future";

                    // Month class

                    $output .= " book-a-session-calendar-timetable-month-" . strtolower( date( "M", strtotime( $date ) ) );

                    // Day class

                    $output .= " book-a-session-calendar-timetable-day-";
                    $output .= strtolower( date( "D", strtotime( $date ) ) );

                    // Today class

                    $output .= strtotime( $date ) === strtotime( "today" ) ? " book-a-session-calendar-timetable-date-today'>" : "'>";

                    // Date label

                    $output .= "<div class='book-a-session-calendar-timetable-label book-a-session-calendar-timetable-label-date'>" . date( "j", strtotime( $date ) );

                    // Tooltip

                    if ( strtotime( $date ) >= strtotime( "today" ) && $current_month === date( "F", strtotime( $date ) ) ) {

                        // Tooltip container

                        $output .= "<span class='book-a-session-animated book-a-session-label-tooltip'>";

                        // Header

                        $output .= $match_count > 1 ? "<header><strong>" . $match_count . " sessions</strong></header>" : "<header><strong>Session</strong></header>";

                        // Session list

                        $output .= "<ul>";

                        $tooltip_session_counter = 0;

                        foreach ( $matched_sessions as $matched_session ) {

                            if ( $tooltip_session_counter >= $tooltip_session_max && $match_count >= $tooltip_session_max ) {

                                $remaining = (int)$match_count - $tooltip_session_max;

                                $output .= "<li>" . $remaining . " more...</li>";
                                break;

                            }

                            // Maybe cache at least service and location names by storing in some loaded array at the top, so we don't have to bother the db every time

                            $service_name = book_a_session_get_table_array( "service", false, false, "name", array( "id", "=", $matched_session["service_id"] ), 1 )[0]->name;
                            $location_name = book_a_session_get_table_array( "location", false, false, "name", array( "id", "=", $matched_session["location_id"] ), 1 )[0]->name;
                            $client_name = get_userdata( $matched_session["client_id"] )->first_name . " " . get_userdata( $matched_session["client_id"] )->last_name;
                            $practitioner_name = get_userdata( $matched_session["practitioner_id"] )->first_name . " " . get_userdata( $matched_session["practitioner_id"] )->last_name;

                            $output .= "<li>";
                            
                            $output .= "<div class='book-a-session-calendar-timetable-container-session-details'>";

                            $output .= "<span class='book-a-session-calendar-timetable-label book-a-session-label-location'>" . $location_name . "</span>";
                            $output .= "<span class='book-a-session-calendar-timetable-label book-a-session-label-hyphen'> - </span>";
                            $output .= "<span class='book-a-session-calendar-timetable-label book-a-session-label-service'>" . $service_name . "</span><br>";
                            $output .= "<span class='book-a-session-calendar-timetable-label book-a-session-label-for'>For</span> ";
                            $output .= "<span class='book-a-session-calendar-timetable-label book-a-session-label-clientname'>" . $client_name . "</span>";
                            $output .= "<span class='book-a-session-calendar-timetable-label book-a-session-label-with'> with </span>";
                            $output .= "<span class='book-a-session-calendar-timetable-label book-a-session-label-practitionername'>" . $practitioner_name . "</span>.<br>";
                            $output .= "<span class='book-a-session-calendar-timetable-label book-a-session-label-due'>Due:</span> ";
                            $output .= "<span class='book-a-session-calendar-timetable-label book-a-session-label-duevalue'>" . book_a_session_get_price_tag( $matched_session["total_due"], $matched_session["currency_id"] ) . "</span><br>";
                            $output .= "<span class='book-a-session-calendar-timetable-label book-a-session-label-paid'>Paid:</span> ";
                            $output .= "<span class='book-a-session-calendar-timetable-label book-a-session-label-paidvalue'>" . book_a_session_get_price_tag( $matched_session["amount_paid"], $matched_session["currency_id"] ) . "</span><br>";
                            $output .= "<span class='book-a-session-calendar-timetable-label book-a-session-label-bookingstatus'>" . $matched_session["booking_status"] . "</span>";
                            $output .= "<span class='book-a-session-calendar-timetable-label book-a-session-label-colon'>:</span> ";
                            $output .= "<span class='book-a-session-calendar-timetable-label book-a-session-label-paymentstatus'>" . $matched_session["payment_status"] . "</span>.";
                            $output .= "<br><a href='javascript:void(0)' data-session-order='" . $matched_session["order_id"] . "' class='book-a-session-button book-a-session-animated book-a-session-dark-button book-a-session-mini-button book-a-session-button-icon book-a-session-button-icon-right book-a-session-view-order'>Order #" . $matched_session["order_id"] . "<span class='book-a-session-icon book-a-session-icon-chevron-right'></span></a>";
                            
                            $output .= "</div>";

                            // Session time column
                            
                            $output .= "<div class='book-a-session-calendar-timetable-container-session-time'>";
                            $output .= "<span class='book-a-session-calendar-timetable-label book-a-session-label-starttime'>";
                            $output .= $matched_session["time_string_array"][0];
                            $output .= "</span>";
                            
                            $output .= "<span class='book-a-session-calendar-timetable-label book-a-session-label-suffix'>";
                            $output .= $matched_session["time_string_array"][1];
                            $output .= "</span>";
                            
                            $output .= "<span class='book-a-session-calendar-timetable-label book-a-session-label-hyphen'> - </span>";

                            $output .= "<span class='book-a-session-calendar-timetable-label book-a-session-label-endtime'>";
                            $output .= $matched_session["time_string_array"][2];
                            $output .= "</span>";

                            $output .= "<span class='book-a-session-calendar-timetable-label book-a-session-label-suffix'>";
                            $output .= $matched_session["time_string_array"][3];
                            $output .= "</span></div>";

                            // End details list 

                            $output .= "</li>";

                            $tooltip_session_counter++;

                        }

                        $output .= "</ul>";
                        $output .= "</span>";    

                    }

                    // End date-container and date label

                    $output .= "</div></div>";


                } else {

                    // Date container

                    $output .= "<div class='book-a-session-calendar-timetable-container-date book-a-session-animated book-a-session-calendar-timetable-session-none";
                    $output .= $current_month !== date( "F", strtotime( $date ) ) ? " book-a-session-calendar-timetable-not-month " : " book-a-session-calendar-timetable-this-month ";
                    $output .= strtotime( $date ) < strtotime( "today" ) && strtotime( $date ) !== strtotime( "today" ) ? " book-a-session-calendar-timetable-date-past" : " book-a-session-calendar-timetable-date-future";
                    $output .= " book-a-session-calendar-timetable-month-" . strtolower( date( "M", strtotime( $date ) ) );
                    $output .= " book-a-session-calendar-timetable-day-";
                    $output .= strtolower( date( "D", strtotime( $date ) ) );
                    $output .= strtotime( $date ) === strtotime( "today" ) ? " book-a-session-calendar-timetable-date-today'>" : "'>";

                    // Date label

                    $output .= "<div class='book-a-session-calendar-timetable-label book-a-session-calendar-timetable-label-date'>" . date( "j", strtotime( $date ) );

                    // End date-container and date label

                    $output .= "</div></div>";

                }

                // End week container

                if ( $day_counter % 7 === 0 ) $output .= "</div>";

                // End of month container, 

                if ( $day_counter % 42 === 0 ) $output .= "</div>";

                // Tick forward to the next day

                $date = date( "Y-m-d", strtotime( "+1 day", strtotime( $date ) ) );

            }

            // End timetable container

            $output .= "</div>";

            // End timetable wrap

            $output .= "</div>";

            // Scroll tracker bottom

            //$output .= "<div class='book-a-session-scroll-tracker book-a-session-animated book-a-session-scroll-tracker-bottom' style='width: calc( 100% / $total_months );'></div>";

            // End card wrap

            $output .= "</div>";

        } elseif ( $datetime_picker ) {

            // Get chosen location's capacity

            $capacity = book_a_session_get_table_array( "location", false, false, "capacity", array( "id", "=", $location_id ), 1 )[0]->capacity;
            
            // Set region timezone

            book_a_session_set_default_timezone_mysql( $region_id );

            // Initialise this array which will be used to map all possible dates and times according to the number of weeks the user wants to load, the schedule (converted to the supplied region's timezone if applicable), and any sessions or unavailable sessions in the timetable

            $mapped_and_merged_timetable_array = array();

            // Get the user specified number of weeks to load for this kind of timetable, the default being 12 weeks.

            $total_weeks = intval( get_option( "book_a_session_options_timetables" )["week_load"] ) ? intval( get_option( "book_a_session_options_timetables" )["week_load"] ) : 12;

            // Set the start date as this week's monday, and set the end date for 12 weeks from then by default, or however many weeks the user has specified ( 7 days * 12 weeks = 84 days, minus 1 being today ). We also add 1 day to the starting date to account for the fact that we don't allow bookings for the same day

            $total_days = ( $total_weeks * 7 ) - 1;

            $date = date( "Y-m-d" );
            $date = date( "Y-m-d", strtotime( "+1 day", strtotime( $date ) ) );
            $date = date( "Y-m-d", strtotime( "this week monday", strtotime( $date ) ) );
            $end_date = date( "Y-m-d", strtotime( "+$total_days days", strtotime( $date ) ) );

            // Get schedule

            $schedule_array = book_a_session_get_table_array( "schedule", "id", "ASC", "id" );

            // Initialise schedule output. While dates and session times are generated in the same loop, session times must be output into a different element from the dates on the frontend, so the outputs must be separated

            $output_schedule = "";

            // Container

            $output .= "<div class='book-a-session-datetime-picker-container book-a-session-flex-row book-a-session-animated'>";

            // Previous button

            $output .= "<div class='book-a-session-circle-button book-a-session-prev-button book-a-session-animated book-a-session-hidden'>❮</div>";

            // Wrap, used to hide overflow

            $output .= "<div class='book-a-session-datetime-picker-wrap'>";

            // Week container, contains all week groups, width being 100% * number of weeks

            $output .= "<div class='book-a-session-animated book-a-session-flex-row book-a-session-datetime-picker-week-container' style='width: calc( 100% * $total_weeks );'>";

            // Initialise counters

            $day_counter = 0;
            $day_of_week_counter = 0;
            $week_counter = 0;

            // Loop through days as long as we haven't gotten to the end date, by default, 83 days later ( 7 days * 12 weeks ) - 1 day being today = 83 days )

            while ( strtotime( $date ) <= strtotime( $end_date ) ) {

                // Check for start of the week by counting days and checking to see if the number divides by 7 cleanly

                if ( $day_counter % 7 === 0 ) {

                    // If it's the start of a week, create a week container and a month label

                    $week_counter++;
                    $day_of_week_counter = 0;
                    $month_at_weekend = date( "F", strtotime( "+7 days", strtotime( $date ) ) );
                    $year_at_weekend = date( "Y", strtotime( "+7 days", strtotime( $date ) ) );
                    $label_month = date( "F", strtotime( $date ) ) !== $month_at_weekend ? "<span class='book-a-session-datetime-picker-label book-a-session-datetime-picker-label-date'>" . date( "F", strtotime( $date ) ) . " - " . $month_at_weekend : "<span class='book-a-session-datetime-picker-label book-a-session-datetime-picker-label-date'>" . date( "F", strtotime( $date ) );
                    $label_month .= "<span class='book-a-session-datetime-picker-label book-a-session-datetime-picker-label-year'>" . date( "Y", strtotime( $date ) );
                    $label_month .= date( "Y", strtotime( $date ) ) !== $year_at_weekend ? " - " . $year_at_weekend . "</span></span>" : "</span></span>";
                    $output .= "<div class='book-a-session-datetime-picker-container-week book-a-session-flex-row ";
                    $output .= $day_counter > 0 ? "book-a-session-hidden book-a-session-animated " : "book-a-session-animated ";
                    $output .= "book-a-session-datetime-picker-week-$week_counter' style='width: calc( 100% / $total_weeks );'>";
                    $output .= "<div class='book-a-session-datetime-picker-label book-a-session-datetime-picker-label-month'>Pick a date in $label_month</div>";

                } 

                // Tick counters forward

                $day_counter++;
                $day_of_week_counter++;

                // Date option container

                $output .= "<div class='book-a-session-animated book-a-session-frontend-option book-a-session-frontend-date book-a-session-datetime-picker-day-" . strtolower( date( "D", strtotime( $date ) ) );
                $output .= " book-a-session-frontend-date-" . date( "Y-m-d", strtotime( $date ) );
                $output .= strtotime( $date ) < strtotime( "today" ) && strtotime( $date ) !== strtotime( "today" ) ? " book-a-session-datetime-picker-date-past" : " book-a-session-datetime-picker-date-future";
                $output .= " book-a-session-datetime-picker-month-" . strtolower( date( "M", strtotime( $date ) ) );
                $output .= strtotime( $date ) === strtotime( "today" ) ? " book-a-session-datetime-picker-date-today'" : "'";
                $output .= " style='width: calc( ( ( 100% - ( 17.5px * 6 ) ) / 7 ) );";
                $output .= $day_of_week_counter === 7 ? "margin-right:0;'>" : "'>";
               
                // Date label

                $output .= "<input type='radio' name='date' value='date_" . date( "Y_m_d", strtotime( $date ) ) . "' id='date_" . date( "Y_m_d", strtotime( $date ) ) . "' data-frontend-date='" . date( "Y-m-d", strtotime( $date ) ) . "'>";
                $output .= "<label for='date_" . date( "Y_m_d", strtotime( $date ) ) . "' " ;
                $output .= "class='book-a-session-animated book-a-session-card'>";

                $output .= "<span class='book-a-session-frontend-option-title'>" . date( "j", strtotime( $date ) ) . "</span>";

                // Day label

                $output .= "<span class='book-a-session-frontend-option-subtitle'>" . date( "l", strtotime( $date ) ) . "</span>";

                // End date header container

                $output .= "</label></div>";

                // Loop through session times according to schedule

                $schedule_counter = 0;

                if ( ! empty( $schedule_array ) ) {

                    //if ( strtotime( $date ) > strtotime( "today" ) ) {

                        // Schedule container

                        $output_schedule .= "<div class='";
                        if ( $schedule_counter > 1 ) $output_schedule .= "book-a-session-hidden ";
                        $output_schedule .= "book-a-session-animated book-a-session-flex-row book-a-session-datetime-picker-date-" . date( "Y-m-d", strtotime( $date ) ) . "'>";
                        $output_schedule .= "<p>Sessions available on " . date( get_option( "date_format" ), strtotime( $date ) ) . "</p>";

                        $schedule_key_counter = 0;

                        $session_array = array();

                        foreach ( $schedule_array as $session_time ) {

                            $schedule_key_counter++;
                            $match = false;

                            // Loop through timetable array and look for any sessions occuring for this date and timeslot

                            foreach ( $merged_timetable_array as $session ) {

                                /**
                                 * TO DO: Count by location and check to see whether or not the location has reached its $capacity
                                 */

                                if ( $session["schedule_id"] == $session_time->id && date( "Y-m-d", strtotime( $session["date"] ) ) == $date ) {

                                    $output_schedule .= "<div class='book-a-session-frontend-option book-a-session-frontend-session-time ";
                                    $output_schedule .= "book-a-session-frontend-session-time-" . $session_time->id . " book-a-session-session-time-unavailable' style='width: calc( ( ( 100% - ( 17.5px * " . ( count( $schedule_array ) - 1 ) . " ) ) / " . ( count( $schedule_array ) ) . " ) );";
                                    $output_schedule .= $schedule_key_counter === count( $schedule_array ) ? "margin-right:0;'>" : "'>";
                                    $output_schedule .= "<input type='radio' name='" . date( "Y_m_d", strtotime( $date ) ) . "_schedule' value='schedule_" . $session_time->id . "' id='" . date( "Y_m_d", strtotime( $date ) ) . "_schedule_" . $session_time->id . "'>";
                                    $output_schedule .= "<label class='book-a-session-animated book-a-session-card' for='" . date( "Y_m_d", strtotime( $date ) ) . "_schedule_" . $session_time->id . "'>";

                                    // Session time label

                                    $output_schedule .= "<span class='book-a-session-frontend-option-title'>" . $session_time->id . "</span>";

                                    // End session time

                                    $output_schedule .= "</label></div>";

                                    $mapped_and_merged_timetable_array[ date( "Y-m-d", strtotime( $date ) ) ][ $session_time->id ] = $session;

                                    $match = true;
                                    break;

                                }

                            }

                            if ( ! $match ) {

                                $output_schedule .= "<div class='book-a-session-frontend-option book-a-session-frontend-session-time ";
                                $output_schedule .= "book-a-session-frontend-session-time-" . $session_time->id . " book-a-session-session-time-available' style='width: calc( ( ( 100% - ( 17.5px * " . ( count( $schedule_array ) - 1 ) . " ) ) / " . ( count( $schedule_array ) ) . " ) );";
                                $output_schedule .= $schedule_key_counter === count( $schedule_array ) ? "margin-right:0;'>" : "'>";
                                $output_schedule .= "<input type='radio' name='" . date( "Y_m_d", strtotime( $date ) ) . "_schedule' value='schedule_" . $session_time->id . "' id='" . date( "Y_m_d", strtotime( $date ) ) . "_schedule_" . $session_time->id . "'>";
                                $output_schedule .= "<label class='book-a-session-animated book-a-session-card' for='" . date( "Y_m_d", strtotime( $date ) ) . "_schedule_" . $session_time->id . "'>";

                                // Session time label

                                $output_schedule .= "<span class='book-a-session-frontend-option-title'>" . $session_time->id . "</span>";

                                // End session time

                                $output_schedule .= "</label></div>";

                                // Add empty session slot to the mapped array to ensure all possible dates and times are knowable in the front end, ensuring all the dates and times have been converted to the user's region's timezone

                                $datetime = book_a_session_get_session_time( $session_time->id, $region_id ? $region_id : false, $date );

                                $mapped_and_merged_timetable_array[ date( "Y-m-d", strtotime( $date ) ) ][ $session_time->id ] = array(

                                    "date"                  => $datetime["start_datetime_object"]->format( get_option( 'date_format' ) ), 
                                    "time"                  => $datetime["time_string"],
                                    "start_datetime"        => $datetime["start_datetime_object"]->format( "Y-m-d H:i:s" ),
                                    "end_datetime"          => $datetime["end_datetime_object"]->format( "Y-m-d H:i:s" ),
                                    "time_string_array"     => $datetime["time_string_array"],
                                    "start_datetime_object" => $datetime["start_datetime_object"],
                                    "end_datetime_object"   => $datetime["end_datetime_object"],
                                    "time_string_array"     => $datetime["time_string_array"],    

                                );

                            }

                        }

                        // End schedule container

                        $output_schedule .= "</div>";

                        $schedule_counter++;

                    //}

                }

                // End week container

                if ( $day_counter % 7 === 0 ) $output .= "</div>";

                // Tick forward to the next day

                $date = date( "Y-m-d", strtotime( "+1 day", strtotime( $date ) ) );

            }

            // End datepicker container

            $output .= "</div>";

            // End datepicker wrap

            $output .= "</div>";

            // Next button

            $output .= "<div class='book-a-session-circle-button book-a-session-animated book-a-session-next-button'>❯</div>";

            // End card wrap

            $output .= "</div>";

        }

    }

    // Reset timezone

    book_a_session_set_default_timezone_mysql();
    
    if ( $datetime_picker && $output_schedule && $mapped_and_merged_timetable_array ) return array( 

        "dates" => $output, 
        "timetable_array" => $mapped_and_merged_timetable_array,

    );

    else return $output;

}