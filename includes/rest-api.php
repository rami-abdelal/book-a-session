<?php

/**
 * Book A Session - REST API
 * 
 */

if ( ! defined( 'ABSPATH' ) ) exit;

function book_a_session_send_invoice_rest( $data ) {

    // Validate posted values

    $reminder               = $data["reminder"]                   == "true" ? true : false;
    $preview                = $data["preview"]                    == "true" ? true : false;
    $recipient_client       = $data["recipients"]["client"]       == "true" ? true : false;
    $recipient_self         = $data["recipients"]["self"]         == "true" ? true : false;
    $recipient_practitioner = $data["recipients"]["practitioner"] == "true" ? true : false;
    
    $recipients_array = array(

        "client" => $recipient_client,
        "practitioner" => $recipient_practitioner,
        "self" => $recipient_self,

    );

    // Check invoice id

    $invoice_id = intval( $data["invoice_id"] ) > 0 ? intval( $data["invoice_id"] ) : 0;

    // If it was provided, and is valid, send that invoice

    if ( $invoice_id > 0 ) {

        $invoice = ! empty( book_a_session_get_table_array( "invoice", "issue_date", "DESC", "id", array( "id", "=", $invoice_id ), 1 ) ) ? book_a_session_get_table_array( "invoice", "issue_date", "DESC", "id", array( "id", "=", $invoice_id ), 1 )[0] : false;

        // If we can confirm the invoice was found, then send it according to the posted values

        if ( $invoice ) {

            return rest_ensure_response( book_a_session_send_invoice( $invoice_id, $reminder, $recipients_array, $preview ) );

        } else return rest_ensure_response( new WP_Error( __( "Requested invoice could not be found.", "book_a_session" ) ) );

    } else if ( $data["order_id"] ) {

        // Otherwise, if the order_id is present, get the latest invoice for that order and send that instead

        $data["order_id"] = intval( $data["order_id"] ) > 0 ? intval( $data["order_id"] ) : 0;

        // If it's a valid number (which it should be if the user isn't an admin, as it's checked in the permission callback, though it's not checked for admins)

        if ( $data["order_id"] ) {

            // Get the order to confirm that it's present

            $order = ! empty( book_a_session_get_table_array( "order", "created", "DESC", "order_id", array( "order_id", "=", $data["order_id"] ), 1 ) ) ? book_a_session_get_table_array( "order", "created", "DESC", "order_id", array( "order_id", "=", $order_id ), 1 )[0] : false;

            if ( $order ) {

                // Search for the latest invoice for the provided order

                $invoice = ! empty( book_a_session_get_table_array( "invoice", "issue_date", "DESC", "id", array( "order_id", "=", $data["order_id"] ), 1 ) ) ? book_a_session_get_table_array( "invoice", "issue_date", "DESC", "id", array( "order_id", "=", $data["order_id"] ), 1 )[0] : false;

                if ( $invoice ) {

                    // Send this invoice according to posted values

                    return rest_ensure_response( book_a_session_send_invoice( $invoice->id, $data["reminder"], $data["recipients"], $data["preview"] ) );

                } else return rest_ensure_response( new WP_Error( __( "Invoice ID is invalid and the order was found but its invoice could not be found.", "book_a_session" ) ) );

            } else return rest_ensure_response( new WP_Error( __( "Invoice ID is invalid and the order could not be found.", "book_a_session" ) ) );

        } else return rest_ensure_response( new WP_Error( __( "Invoice ID and order ID are invalid.", "book_a_session" ) ) );

    } else return rest_ensure_response( new WP_Error( __( "Invoice number provided is invalid.", "book_a_session" ) ) );

}

add_action( 'rest_api_init', function () {

    register_rest_route( 'book-a-session/v1', '/invoices/(?P<invoice_id>\d+)/send', array(

        'methods'               => 'POST',
        'callback'              => 'book_a_session_send_invoice_rest',
        'permission_callback'   => function( $request ) {

            // If admin or can manage options, you can do everything

            if ( ! current_user_can( 'manage_options' ) ) {

                // Otherwise, check the database for the client and practitioner for this order

                $order = book_a_session_get_table_array( "order", false, false, "practitioner_id, user_id", array( "order_id", "=", $request["order_id"] ), 1 )[0];

                if ( ! empty( $order ) ) {
                    
                    // If the posting user is the client or the practitioner, they can record a payment for this order, otherwise they can't

                    if ( intval( $order->user_id ) === intval( get_current_user_id() ) || intval( $order->practitioner_id ) === intval( get_current_user_id() ) ) {
                     
                        return true;
                        
                    } else return false;

                } else return false;

            } else return true;

        }

    ) );

} );

// Due Date POST Endpoint. On posting an array of items, each with a key value pair for date => Y-m-d date and schedule_id => number id, and optionally a region id for timezone conversion, this endpoint returns the due date calculated according to admin settings 

function book_a_session_get_due_date_rest( $data ) {

    if ( $data["session_array"] ) {

        if ( is_array( $data["session_array"] ) ) {

            return rest_ensure_response( book_a_session_get_due_date( $data["session_array"], $data["region_id"] ) );

        } else return rest_ensure_response( new WP_Error( __( "'session_array' is not actually an array.", "book_a_session" ) ) );

    } else return rest_ensure_response( new WP_Error( __( "'session_array' is not present or otherwise invalid.", "book_a_session" ) ) );

}

add_action( 'rest_api_init', function () {

    register_rest_route( 'book-a-session/v1', '/invoices/due_date', array(

        'methods'               => 'POST',
        'callback'              => 'book_a_session_get_due_date_rest',

    ) );

} );

function book_a_session_get_datetime_picker( $data ) {

    $region = $data["region_id"];
    $quantity = $data["quantity"];
    $location = $data["location_id"];
    $practitioner = $data["practitioner_id"]; 
    $user = $data["user_id"];

    $datetime_picker = book_a_session_timetable( "user_" . $user . "_practitioner_" . $practitioner . "_location_" . $location, "datetime_picker", $order = "ASC", $limit = null, "region", $region );


    if ( $datetime_picker ) return rest_ensure_response( $datetime_picker );
    else return rest_ensure_response( new WP_Error( __( "Could not retrieve dates and times for your choices.", "book_a_session" ) ) );

}

function book_a_session_record_payment( $data ) {
    
    $order_id = $data["order_id"];
    $payment_method_id = $data["payment_method_id"];
    $invoice_id = $data["invoice_id"];
    $payment_id = $data["payment_id"] ? $data["payment_id"] : null;
    $total = $data["total"];
    $success = $data["success"];
    $note = $data["note"] ? $data["note"] : null;

    global $wpdb;
    $table_name = $wpdb->prefix . "book_a_session_payment";

    $data_array = array(

        "payment_method_id" => $payment_method_id,
        "order_id"          => $order_id,
        "invoice_id"        => $invoice_id,
        "payment_id"        => $payment_id,
        "total"             => $total,
        "success"           => $success,
        "note"              => $note,

    );

    $format_array = array(

        "%d", // payment_method_id
        "%d", // order_id       
        "%d", // invoice_id    
        "%s", // payment_id     
        "%f", // total          
        "%d", // success        
        "%s", // note           

    );

    // Record the payment

    $result = $wpdb->insert( $table_name, $data_array, $format_array );

    // If it was recorded successfully

    if ( $result ) {

        // Check the order to see if it needs to be updated

        $order = book_a_session_get_table_array( "order", false, false, "*", array( "order_id", "=", $order_id ), 1 )[0];

        if ( $order ) {

            // Prepare updated order values

            $updated_total_due = $order->total_due - $total;

            $table_name_order =  $wpdb->prefix . "book_a_session_order";

            if ( $payment_method_id == 1 || $payment_method_id == 4 ) {

                // PayPal and Credit / Debit Card (Stripe) / Online payment.

                $booking_status_due = "Pending";
                $booking_status_partly_paid = "Pending";
                $booking_status_paid = "Booked";
                
                $payment_status_due = "Online payment due";
                $payment_status_partly_paid = "Online payment partly paid";
                $payment_status_paid = "Online payment received";

            } elseif ( $payment_method_id == 2 ) {

                // Cash

                $booking_status_due = "Booked";
                $booking_status_partly_paid = "Booked";
                $booking_status_paid = "Booked";
                
                $payment_status_due = "Cash payment due";
                $payment_status_partly_paid = "Cash payment partly paid";
                $payment_status_paid = "Cash payment received";

            } elseif ( $payment_method_id == 3 ) {

                // Vodafone Cash

                $booking_status_due = "Pending";
                $booking_status_partly_paid = "Pending";
                $booking_status_paid = "Booked";
                
                $payment_status_due = "Vodafone Cash payment due";
                $payment_status_partly_paid = "Vodafone Cash partly paid";
                $payment_status_paid = "Vodafone Cash payment received";

            }

            if ( $updated_total_due <= 0.00 ) {

                // If this record payment clears the total due in this order, it has been paid.

                $booking_status = $booking_status_paid;
                $payment_status = $payment_status_paid;

            } elseif ( $updated_total_due > 0.00 && $order->total_due > $total && $total > 0.00 ) {

                // If this recorded payment doesn't clear the total due in this order, it has been partly paid.

                $booking_status = $booking_status_partly_paid;
                $payment_status = $payment_status_partly_paid;

            } else {

                // Otherwise, this recorded payment is probably 0 somehow, so don't change the order

                $booking_status = $order->booking_status;
                $payment_status = $order->payment_status;

            }

            $updated_order_data_array = array(

                "amount_paid"       => $order->amount_paid + $total,
                "total_due"         => $updated_total_due,
                "booking_status"    => $booking_status,
                "payment_status"    => $payment_status,

            );

            $updated_order_where_array = array(

                "order_id" => $order_id,

            );

            $updated_order_format_array = array(

                "%f", // Amount paid
                "%f", // Total due 
                "%s", // Booking status
                "%s", // Payment status

            );

            $updated_order_where_format_array = array(

                "%d",
            
            );

            $order_updated = $wpdb->update(

                $table_name_order,
                $updated_order_data_array,
                $updated_order_where_array,
                $updated_order_format_array,
                $updated_order_where_format_array

            );

            if ( $order_updated ) {

                // Create a new invoice

                $total_overridden = false;
                $order->payment_method_id == 3  ? $vfc_invoice         = 1 : $vfc_invoice      = 0;
                $total_overridden               ? $total_overridden    = 1 : $total_overridden = 0;

                // Get sessions, sorted by earliest to latest

                $sessions_array = book_a_session_get_table_array( "session", "start_datetime", "ASC", "*", array( "order_id", "=", $order_id ) );
                
                // Determine payment instructions

                if (
                    $payment_status == 'Cash payment due'              || 
                    $payment_status == 'Online payment due'            || 
                    $payment_status == 'Vodafone Cash payment due'     ||
                    $payment_status == 'Cash payment partly paid'      ||
                    $payment_status == 'Online payment partly paid'    ||
                    $payment_status == 'Vodafone Cash partly paid'     ){

                    $due = 1;
                    $due_by_date = $sessions_array[0]->start_datetime;

                    if ( $payment_status == 'Cash payment due' || $payment_status == 'Cash payment partly paid' ) { 

                        // Cash instructions
                        $cash_due_instruction = 'Cash payment due on first meeting.';
                        $due_by_instruction = $cash_due_instruction;
                        
                    } elseif ( $payment_status == 'Vodafone Cash payment due' || $payment_status == 'Vodafone Cash partly paid' ) {

                        // Get Vodafone Cash invoice option values

                        $receiving_vfc_mobile           = get_option( 'book_a_session_options_invoices' )['vfc_receiving_mobile_number'];
                        $due_date_offset_measurement    = get_option( 'book_a_session_options_invoices' )['due_date_offset_measurement'];
                        $due_date_offset_anchor         = get_option( 'book_a_session_options_invoices' )['due_date_offset_anchor'];
                        $due_date_offset_value          = get_option( 'book_a_session_options_invoices' )['due_date_offset_value'];

                        if ( (int)$due_date_offset_value === 1 ) {

                            // Remove 's' at end of string (minutes, hours, days) when it's only one minute, hour, or day.

                            $due_date_offset_measurement = preg_replace( "/s$/", "", $due_date_offset_measurement );

                        }

                        // If due date options have been entered and retrieved correctly

                        if ( ! empty( $due_date_offset_anchor ) && ! empty( $due_date_offset_measurement ) && ! empty( $due_date_offset_value ) ) {

                            // Calculate due date if the period is to be measured from the issue date, i.e. issue date +1 day

                            if ( $due_date_offset_anchor === 'issue_date' ) {

                                $issue_date = date("Y-m-d H:i:s");
                                $str = '+' . $due_date_offset_value . ' ' . $due_date_offset_measurement;
                                $due_by_date = date( "Y-m-d H:i:s", strtotime( $str, strtotime( $issue_date ) ) );

                            // Calculate due date if the period is to be measured from the session date, by getting the first session date, its time, putting them together as a datetime object, and modifying it according to user options, i.e. session date -1 day.

                            } elseif ( $due_date_offset_anchor === 'session_date' ) {

                                $str = '-' . $due_date_offset_value . ' ' . $due_date_offset_measurement;
                                $due_by_date = date( "Y-m-d H:i:s", strtotime( $str, strtotime( $sessions_array[0]->start_datetime ) ) );

                            }

                        }

                        // VFC instructions

                        $vfc_due_instruction = 'Vodafone Cash payment due before order can be confirmed. Please send the payment to the number above. Once received and confirmed, we\'ll send you your confirmation email. Thanks!';
                        $due_by_instruction = $vfc_due_instruction;

                    } elseif ( $payment_status == 'Online payment due' || $payment_status == 'Online payment partly paid' ) {

                        // Online payment instructions

                        $online_payable = isset( get_option( "book_a_session_options_paymentmethods" )["online_payable"] ) ? get_option( "book_a_session_options_paymentmethods" )["online_payable"] : book_a_session_options_default( "paymentmethods", "online_payable" );

                        if ( $online_payable === "due_date" ) {

                            $online_due_instruction = 'Payment due before order can be confirmed.';
                            $due_by_instruction = $online_due_instruction;
        
                        } else {

                            $due                = 0;
                            $due_by_date        = date("Y-m-d H:i:s");
                            $due_by_instruction = null;
            
                        }

                    }

                } else {

                    $due                = 0;
                    $due_by_date        = date("Y-m-d H:i:s");
                    $due_by_instruction = null;

                }

                // Prepare invoice insertion

                $table_name_invoice = $wpdb->prefix . "book_a_session_invoice";

                // Prepare invoice values

                $invoice_value_array = array(

                    'issue_date'            => date("Y-m-d"),
                    'order_id'              => $order_id,
                    'user_id'               => $order->user_id,
                    'vfc_invoice'           => $vfc_invoice,
                    'vfc_mobile_number'     => $order->vfc_mobile,
                    'due'                   => $due,
                    'due_by_date'           => $due_by_date,
                    'due_by_instruction'    => $due_by_instruction,
                    'total_overridden'      => $total_overridden,
                    'currency_id'           => $order->currency_id,
                    'base_price'            => $order->base_price,
                    'location_charge'       => $order->location_charge,
                    'discount'              => $order->discount,
                    'quantity'              => $order->quantity,
                    'amount_paid'           => $order->amount_paid + $total,
                    'total'                 => $order->total,
                    'grand_total'           => $order->total,
                    'grand_total_due'       => $order->total - $order->amount_paid - $total,

                );

                // Prepare invoice value formats

                $invoice_format_array = array(

                    '%s', // Issue Date
                    '%d', // Order ID
                    '%d', // User ID
                    '%d', // VFC Invoice
                    '%s', // VFC Mobile Number
                    '%d', // Due
                    '%s', // Due By Date
                    '%s', // Due By Instruction
                    '%d', // Total Overridden
                    '%d', // Currency ID
                    '%f', // Base Price
                    '%f', // Location Charge
                    '%f', // Discount
                    '%d', // Quantity
                    '%f', // Amount Paid
                    '%f', // Total
                    '%f', // Grand Total
                    '%f'  // Grand Total Due

                );

                // Add Invoice

                $add_invoice_db_result = $wpdb->insert( $table_name_invoice, $invoice_value_array, $invoice_format_array );

                // If that worked, check if we are required to send any invoice emails

                if ( ! empty ( $add_invoice_db_result ) ) {

                    // If the payment has changed either status of the order, if so, we send an email about the new invoice, otherwise no

                    if ( $booking_status != $order->booking_status || $payment_status != $order->payment_status ) {

                        // Get invoice id

                        $added_invoice = $wpdb->get_row( "SELECT id from $table_name_invoice ORDER BY id DESC LIMIT 1", OBJECT );
                        $added_invoice_id = $added_invoice->id;

                        // Initialise recipients argument array for book_a_session_send_invoice()

                        $recipients = array();

                        // Populate the array, we want to send an email to both the client and the practitioner in this case

                        $recipients["client"]       = true;
                        $recipients["practitioner"] = true;
                        $recipients["self"]         = false;

                        // If any of the above are true, send invoice

                        if ( in_array( true, $recipients, true ) ) {

                            $invoice_send_result[] = book_a_session_send_invoice( $added_invoice_id, false, $recipients );

                        }

                    }

                }

                // Return results

                return rest_ensure_response(

                    array( 

                        "payment_result" => $result,
                        "invoice_result" => $add_invoice_db_result,
                        "invoice_email_result" => isset( $invoice_send_result ) ? $invoice_send_result : null,

                    )

                );

            } else return rest_ensure_response( new WP_Error( __( "Payment recorded, but the order could not be updated to reflect payment.", "book_a_session" ) ) );

        } else return rest_ensure_response( new WP_Error( __( "Payment recorded, but the order doesn't exist.", "book_a_session" ) ) );

    } else return rest_ensure_response( new WP_Error( __( "Payment could not be recorded, please check values and try again.", "book_a_session" ) ) );

}

// Record payment WP REST API POST Endpoint

add_action( 'rest_api_init', function () {

    register_rest_route( 'book-a-session/v1', '/orders/(?P<order_id>\d+)/payment/', array(

        'methods'               => 'POST',
        'callback'              => 'book_a_session_record_payment',
        'permission_callback'   => function( $request ) {

            // If admin or can manage options, you can do everything

            if ( ! current_user_can( 'manage_options' ) ) {

                // Otherwise, check the database for the client and practitioner for this order

                $order = book_a_session_get_table_array( "order", false, false, "practitioner_id, user_id", array( "order_id", "=", $request["order_id"] ), 1 )[0];

                if ( ! empty( $order ) ) {
                    
                    // If the posting user is the client or the practitioner, they can record a payment for this order, otherwise they can't

                    if ( intval( $order->user_id ) === intval( get_current_user_id() ) || intval( $order->practitioner_id ) === intval( get_current_user_id() ) ) {
                     
                        return true;
                        
                    } else return false;

                } else return false;

            } else return true;

        }
    ) );

} );

/**
 * Sends message, usually posted via REST API, by providing the order ID and optionally the user ID, returning the message if successful, or false otherwise.
 * 
 */

function book_a_session_send_message( $data ) {

    // Get primary data

    $order_id = $data["order_id"];
    $user_id = $data["user_id"];
    $user = get_userdata( $user_id );
    $message = $data->get_body() ? $data->get_body() : $data["message"];

    // Determine whether the user is a practitioner or admin

    $isprac = in_array( "admin_practitioner", $user->roles, true ) || in_array( "practitioner", $user->roles, true ) ? true : false;
    $isadmin = user_can( $user_id, "manage_options" ) ? true : false;

    // Each message is inherently read by the person who authored it, so set the read practitioner and read client columns as the current datetime as of insertion depending on if it's the client or the practitioner who authored this message. In practice this value won't be shown to the recipient, but it's here for consistency

    $read_prac = $isprac ? date( "Y-m-d H:i:s" ) : null;
    $read_client = $isprac ? null : date( "Y-m-d H:i:s" );

    // Prepare insertion

    global $wpdb;
    $table_name = $wpdb->prefix . "book_a_session_message";

    $data = array(

        "order_id"          => $order_id,
        "date"              => date( "Y-m-d" ),
        "time"              => date( "H:i:s" ),
        "datetime"          => date( "Y-m-d H:i:s" ),
        "user_id"           => $user_id,
        "content"           => $message,
        "read_practitioner" => $read_prac,
        "read_client"       => $read_client,

    );

    $format = array(

        "%d", // Order ID
        "%s", // Date 
        "%s", // Time 
        "%s", // Date Time 
        "%d", // User ID
        "%s", // Content 
        "%s", // Read Practitioner 
        "%s", // Read Client

    );

    $message_result = $wpdb->insert(

        $table_name,
        $data,
        $format

    );

    if ( $message_result ) { return $message; } else { return false; }

}

function book_a_session_get_orders() {

    return book_a_session_get_table_array( "order" );

}

function book_a_session_get_messages( $data ) {

    // Get order

    $order_id = $data["order_id"];
    $user_id = $data["user_id"] ? $data["user_id"] : null; 

    global $wpdb;
    $table_name = $wpdb->prefix . "book_a_session_order";

    $order = $wpdb->get_row(
        "SELECT 
        * 
        FROM $table_name 
        WHERE order_id = $order_id",
        ARRAY_A );

    if ( $order ) {

        // Get messages

        $table_name_messages = $wpdb->prefix . "book_a_session_message";

        $query =   "SELECT 
                    * 
                    FROM $table_name_messages
                    WHERE order_id = $order_id
                    ";

        if ( intval( $user_id > 0 ) ) $query .= "AND user_id = " . $user_id . " ";

        $query .= "ORDER BY datetime ASC";

        $messages = $wpdb->get_results( $query, ARRAY_A );

        if ( $messages ) {

            for ( $i = 0; $i < count( $messages ); $i++ ) {

                $datetime_object = new DateTime( $messages[$i]["datetime"] );
                $converted_datetime_object = book_a_session_convert_datetime( $datetime_object, false, intval( $order["region_id"] ) );

                $messages[$i]["date_converted_formatted"] = $converted_datetime_object->format( get_option( "date_format" ) );
                $messages[$i]["time_converted_formatted"] = $converted_datetime_object->format( get_option( "time_format" ) );
                $messages[$i]["user_name"] = book_a_session_get_user_name( intval( $messages[$i]["user_id"] ) );

            }

            return rest_ensure_response( $messages );

        } else return rest_ensure_response( new WP_Error( "Messages not found", __( "The database query triggered by this request failed. Please see book_a_session_get_messages() in /book-a-session/includes/core-functionality.php", "book_a_session" ) ) );

    } else return rest_ensure_response( new WP_Error( "Order not found", __( "The database query triggered by this request failed. Please see book_a_session_get_messages() in /book-a-session/includes/core-functionality.php", "book_a_session" ) ) );

}

function book_a_session_get_order_by_id( $data ) {
    
    $id_number = $data["order_id"];
    global $wpdb;
    $table_name = $wpdb->prefix . "book_a_session_order";
    $order = $wpdb->get_row(
        "SELECT 
        * 
        FROM $table_name 
        WHERE order_id = $id_number",
        ARRAY_A );

    if ( $order ) {

        // Check if the current user is an administrator or the practitioner for this order
        
        if ( current_user_can( "manage_options" ) || intval( $order["practitioner_id"] ) === intval( get_current_user_id() ) ) {
            
            $order["current_user_is_prac"] = true;
            
        } else $order["current_user_is_prac"] = false;

        if ( current_user_can( "manage_options" ) ) {
            
            $order["current_user_is_admin"] = true;
            
        } else $order["current_user_is_admin"] = false;

        // Get messages

        $table_name_messages = $wpdb->prefix . "book_a_session_message";
        $messages = $wpdb->get_results(
            "SELECT 
            * 
            FROM $table_name_messages
            WHERE order_id = $id_number
            ORDER BY datetime ASC",
            ARRAY_A );

        // Get invoices

        $invoices = book_a_session_get_table_array( "invoice", "issue_date", "DESC", "*", array( "order_id", "=", $order["order_id"] ) );
    
        // Get some more data

        $service_row = book_a_session_get_table_array( "service", false, false, "name, image_id", array( "id", "=", $order["service_id"] ), 1 )[0];
        $service_image_array = wp_get_attachment_image_src( intval( $service_row->image_id ), "full" );
        $service_type_id = book_a_session_get_service_by_id( array( "id" => $order["service_id"] ) )["service_type_id"];
        $service_type_row = book_a_session_get_table_array( "service_type", false, false, "name, practitioner_title", array( "id", "=", $service_type_id ), 1 )[0];
        $location_row = book_a_session_get_table_array( "location", false, false, "*", array( "id", "=", $order["location_id"] ), 1 )[0];

        // Assign new keys and values

        $order["service_type_name"] = $service_type_row->name;
        $order["service_name"] = $service_row->name;
        $order["service_image"] = count( $service_image_array ) === 4 ? array( "url" => $service_image_array[0], "width" => $service_image_array[1], "height" => $service_image_array[2], "is_intermediate" => $service_image_array[3] ) : false;
        $order["region_name"] = book_a_session_get_table_array( "region", false, false, "name", array( "id", "=", $order["region_id"] ), 1 )[0]->name;
        $order["practitioner_name"] = book_a_session_get_user_name( $order["practitioner_id"] );
        $order["practitioner_title"] = $service_type_row->practitioner_title;
        $order["user_name"] = book_a_session_get_user_name( $order["user_id"] );
        $order["payment_method_name"] = book_a_session_get_table_array( "payment_method", false, false, "name", array( "id", "=", $order["payment_method_id"] ), 1 )[0]->name;
        $order["currency_code"] = book_a_session_get_table_array( "currency", false, false, "code", array( "id", "=", $order["currency_id"] ), 1 )[0]->code;
        $order["base_price_tag"] = book_a_session_get_price_tag( $order["base_price"], $order["currency_id"] );
        $order["location_charge_tag"] = book_a_session_get_price_tag( $order["location_charge"], $order["currency_id"] );
        $order["discount_tag"] = book_a_session_get_price_tag( $order["discount"], $order["currency_id"] );
        $order["total_tag"] = book_a_session_get_price_tag( $order["total"], $order["currency_id"] );
        $order["grand_total"] = (float)number_format( $order["total"], 2);     
        $order["grand_total_tag"] = book_a_session_get_price_tag( $order["grand_total"], $order["currency_id"] );        
        $order["amount_paid_tag"] = book_a_session_get_price_tag( $order["amount_paid"], $order["currency_id"] );
        $order["total_due_tag"] = book_a_session_get_price_tag( $order["total_due"], $order["currency_id"] );
        $order["location"] = $location_row;
        $order["location_name"] = $location_row->name;
        $order["location_address_line_1"] = $location_row->address_line_1 ? $location_row->address_line_1 : null;
        $order["location_address_line_2"] = $location_row->address_line_2 ? $location_row->address_line_2 : null;
        $order["location_address_line_3"] = $location_row->address_line_3 ? $location_row->address_line_3 : null;
        $order["location_city"] = $location_row->city ? $location_row->city : null;
        $order["location_country"] = $location_row->country ? $location_row->country : null;
        $order["location_postcode_zipcode"] = $location_row->postcode_zipcode ? $location_row->postcode_zipcode : null;
        $order["current_user_id"] = get_current_user_id();
        $order["current_user_username"] = get_userdata( get_current_user_id() )->display_name;
        $order["current_user_email"] = get_userdata( get_current_user_id() )->user_email;
        $order["client_name"] = get_userdata( $order["user_id"] )->display_name;
        $order["client_email"] = get_userdata( $order["user_id"] )->user_email;

        // Reset datatypes

        $order["amount_paid"] = (float)number_format($order["amount_paid"], 2);
        $order["base_price"] = (float)number_format($order["base_price"], 2);
        $order["discount"] = (float)number_format($order["discount"], 2);
        $order["location_charge"] = (float)number_format($order["location_charge"], 2);
        $order["total"] = (float)number_format($order["total"], 2);
        $order["total_due"] = number_format($order["total_due"], 2);
        $order["quantity"] = (int)$order["quantity"];

        // Convert and format order datetime

        $order_created_datetime_object = new DateTime( $order["created"] );
        $order_created_converted_datetime_object = book_a_session_convert_datetime( $order_created_datetime_object, false, intval( $order["region_id"] ) );

        $order["created_datetime_converted_formatted"] = array( 
            "date" => $order_created_converted_datetime_object->format( get_option( "date_format" ) ), 
            "time" => $order_created_converted_datetime_object->format( get_option( "time_format" ) ), 
        );

        // Convert and format messages datetimes
        
        if ( $messages ) {

            for ( $i = 0; $i < count( $messages ); $i++ ) {

                $datetime_object = new DateTime( $messages[$i]["datetime"] );
                $converted_datetime_object = book_a_session_convert_datetime( $datetime_object, false, intval( $order["region_id"] ) );

                $messages[$i]["date_converted_formatted"] = $converted_datetime_object->format( get_option( "date_format" ) );
                $messages[$i]["time_converted_formatted"] = $converted_datetime_object->format( get_option( "time_format" ) );
                $messages[$i]["user_name"] = get_userdata( intval( $messages[$i]["user_id"] ) )->display_name;

            }

        }

        if ( $order["session"] ) {

            // Session orders

            $table_name_session = $wpdb->prefix . "book_a_session_session";

            // Get sessions

            $sessions = $wpdb->get_results(
                "SELECT 
                * 
                FROM $table_name_session 
                WHERE order_id = $id_number
                ORDER BY start_datetime ASC",
                ARRAY_A );

            // Add converted times to array

            if ( $sessions ) {

                for ( $i = 0; $i < count( $sessions ); $i++ ) {

                    if ( $sessions[$i]["schedule_id"] && $order["region_id"] && $sessions[$i]["date"] ) {

                        $sessions[$i]["session_time_converted_array"] = array_merge( book_a_session_get_session_time( $sessions[$i]["schedule_id"], $order["region_id"], $sessions[$i]["date"] ), array( "upcoming" => book_a_session_convert_datetime( new DateTime(), false, $order["region_id"] ) < book_a_session_get_session_time( $sessions[$i]["schedule_id"], $order["region_id"], $sessions[$i]["date"] )["end_datetime_object"] ? true : false,) );

                    }

                }
    
            }

            // Send it all away

            if ( $sessions ) return rest_ensure_response( array( "order" => $order, "invoices" => $invoices, "messages" => $messages, "sessions" => $sessions ) );
        
        } elseif ( $order["project"] ) {

            // Project orders

            $table_name_project = $wpdb->prefix . "book_a_session_project";
            $projects = $wpdb->get_results(
                "SELECT 
                * 
                FROM $table_name_project 
                WHERE order_id = $id_number
                ORDER BY start_date ASC",
                ARRAY_A );

            if ( $projects ) return rest_ensure_response( array( "order" => $order, "invoices" => $invoices, "messages" => $messages, "projects" => $sessions ) );

        } else {

            // If for some reason both session and project columns are 0 in an order row, just return the order data without associated sessions or projects, and include a WP error object

            return rest_ensure_response( array( "order" => $order, "WP_Error" => new WP_Error( "Data entry error", __( "This order's session and project columns are both set to 0. Fix this by setting the correct column to 1.", "book_a_session" ) ) ) );

        }

    } else return false;
    
}

// Orders REST API GET endpoint

add_action( 'rest_api_init', function () {

    register_rest_route( 'book-a-session/v1', '/orders/(?P<order_id>\d+)', array(

        'methods'               => 'GET',
        'callback'              => 'book_a_session_get_order_by_id',
        'permission_callback'   => function( $request ) {

            // If admin or can manage options, you're allowed to see any order

            if ( ! current_user_can( 'manage_options' ) ) {

                // Otherwise, check the database for the client and practitioner for this order

                $order = book_a_session_get_table_array( "order", false, false, "practitioner_id, user_id", array( "order_id", "=", $request["order_id"] ), 1 )[0];

                if ( ! empty( $order ) ) {

                    // If the the requesting user is the client or practitioner for this order, they can see it

                    if ( intval( $order->user_id ) === intval( get_current_user_id() ) || intval( $order->practitioner_id ) === intval( get_current_user_id() ) ) {
                     
                        return true;
                        
                    } else return false;

                } else return false;

            } else return true;

        }
    ) );

} );

// Order Messages REST API POST endpoint

add_action( 'rest_api_init', function () {

    register_rest_route( 'book-a-session/v1', '/orders/(?P<order_id>\d+)/messages/(?P<user_id>\d+)', array(

        'methods'               => 'POST',
        'callback'              => 'book_a_session_send_message',
        'permission_callback'   => function( $request ) {

            // If admin or can manage options, you're allowed to post a message or do anything really

            if ( ! current_user_can( 'manage_options' ) ) {

                // Otherwise, check the database for the client and practitioner for this order

                $order = book_a_session_get_table_array( "order", false, false, "practitioner_id, user_id", array( "order_id", "=", $request["order_id"] ), 1 )[0];

                if ( ! empty( $order ) ) {
                    
                    // If the posting user is the client or the practitioner, they can post the message, otherwise they can't

                    if ( intval( $order->user_id ) === intval( get_current_user_id() ) || intval( $order->practitioner_id ) === intval( get_current_user_id() ) ) {
                     
                        return true;
                        
                    } else return false;

                } else return false;

            } else return true;

        }
    ) );

} );

// PayPal Payment REST API POST endpoint

add_action( 'rest_api_init', function () {

    register_rest_route( 'book-a-session/v1', '/payment/paypal', array(

        'methods'               => 'POST',
        'callback'              => 'book_a_session_request_paypal_payment',

    ) );

} );

// Datetime picker REST API Get endpoint

add_action( 'rest_api_init', function () {

    register_rest_route( 'book-a-session/v1', '/datetime_pickers/region_id=(?P<region_id>\d+)/quantity=(?P<quantity>\d+)/location_id=(?P<location_id>\d+)/practitioner_id=(?P<practitioner_id>\d+)/user_id=(?P<user_id>\d+)', array(

        'methods'               => 'GET',
        'callback'              => 'book_a_session_get_datetime_picker',

    ) );

} );

// Due Date REST API Get endpoint

add_action( 'rest_api_init', function () {

    register_rest_route( 'book-a-session/v1', '/vfc_due_date/region_id=(?P<region_id>\d+)', array(

        'methods'               => 'GET',
        'callback'              => 'book_a_session_get_due_date',

    ) );

} );

// Order Messages REST API Get endpoint

add_action( 'rest_api_init', function () {

    register_rest_route( 'book-a-session/v1', '/orders/(?P<order_id>\d+)/messages/(?P<user_id>\d+)', array(

        'methods'               => 'GET',
        'callback'              => 'book_a_session_get_messages',
        'permission_callback'   => function( $request ) {

            // If admin or can manage options, you're allowed to post a message or do anything really

            if ( ! current_user_can( 'manage_options' ) ) {

                // Otherwise, check the database for the client and practitioner for this order

                $order = book_a_session_get_table_array( "order", false, false, "practitioner_id, user_id", array( "order_id", "=", $request["order_id"] ), 1 )[0];

                if ( ! empty( $order ) ) {
                    
                    // If the requesting user is the client or the practitioner, they can get the message, otherwise they can't

                    if ( intval( $order->user_id ) === intval( get_current_user_id() ) || intval( $order->practitioner_id ) === intval( get_current_user_id() ) ) {
                     
                        return true;
                        
                    } else return false;

                } else return false;

            } else return true;

        }
    ) );

} );


function book_a_session_get_invoice_by_id( $data ) {

    $invoice_id = ! empty( intval( $data["id"] ) ) ? intval( $data["id"] ) : false;

    if ( $invoice_id ) $invoice = book_a_session_send_invoice( $invoice_id, false, false, true );

    return rest_ensure_response( $invoice );
    
}

add_action( 'rest_api_init', function () {

    register_rest_route( 'book-a-session/v1', '/invoices/(?P<id>\d+)', array(

        'methods'               => 'GET',
        'callback'              => 'book_a_session_get_invoice_by_id',
        'permission_callback'   => function( $request ) {

            if ( ! current_user_can( 'manage_options' ) ) {

                $invoice = book_a_session_get_table_array( "invoice", false, false, "user_id", array( "id", "=", $request["id"] ), 1 )[0];

                if ( ! empty( $invoice ) ) {

                    return intval( $invoice->user_id ) === intval( get_current_user_id() );

                } else return false;

            } else return true;

        }

    ) );

} );

function book_a_session_get_location_payment_methods() {

    return book_a_session_get_table_array( "location_payment_methods" );

}

function book_a_session_get_location_payment_method_by_id( $data ) {

    $location_id = $data["location_id"];
    $payment_method_id = $data["payment_method_id"];

    global $wpdb;
    $table_name = $wpdb->prefix . "book_a_session_location_payment_method";
    $result = $wpdb->get_row(
        "SELECT * FROM $table_name WHERE location_id = $location_id AND payment_method_id = $payment_method_id"
        , ARRAY_A);

    return $result;
    
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/location-payment-methods/', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_location_payment_methods',

    ) );
} );

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/location-payment-methods/(?P<location_id>\d+)/(?P<payment_method_id>\d+)', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_location_payment_method_by_id',

    ) );
} );

function book_a_session_get_location_charges() {

    return book_a_session_get_table_array( "location_charge" );

}

function book_a_session_get_location_charge_by_id( $data ) {

    $location_id = $data["location_id"];
    $currency_id = $data["currency_id"];

    global $wpdb;
    $table_name = $wpdb->prefix . "book_a_session_location_charge";
    $result = $wpdb->get_row(
        "SELECT * FROM $table_name WHERE location_id = $location_id AND currency_id = $currency_id"
        , ARRAY_A);

    return $result;
    
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/location-charges/', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_location_charges',

    ) );
} );

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/location-charges/(?P<location_id>\d+)/(?P<currency_id>\d+)', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_location_charge_by_id',

    ) );
} );

function book_a_session_get_bundle_currencies() {

    return book_a_session_get_table_array( "bundle_currency" );

}

function book_a_session_get_bundle_currency_by_id( $data ) {

    $bundle_id = $data["bundle_id"];
    $currency_id = $data["currency_id"];

    global $wpdb;
    $table_name = $wpdb->prefix . "book_a_session_bundle_currency";
    $result = $wpdb->get_row(
        "SELECT * FROM $table_name WHERE quantity = $bundle_id AND currency_id = $currency_id"
        , ARRAY_A);

    return $result;
    
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/bundle-currency/', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_bundle_currencies',

    ) );
} );

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/bundle-currency/(?P<bundle_id>\d+)/(?P<currency_id>\d+)', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_bundle_currency_by_id',

    ) );
} );

function book_a_session_get_regions() {

    return book_a_session_get_table_array( "region" );

}

function book_a_session_get_region_by_id( $data ) {

    $id_number = $data["id"];

    global $wpdb;
    $table_name = $wpdb->prefix . "book_a_session_region";
    $result = $wpdb->get_row(
        "SELECT * FROM $table_name WHERE id = $id_number"
        , ARRAY_A);

    return $result;
    
}

function book_a_session_core_get_region_by_id( $id_number ) {

    global $wpdb;
    $table_name = $wpdb->prefix . "book_a_session_region";
    $result = $wpdb->get_row(
        "SELECT * FROM $table_name WHERE id = $id_number"
        , ARRAY_A);

    return $result;
    
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/regions/', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_regions',

    ) );
} );

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/regions/(?P<id>\d+)', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_region_by_id',

    ) );
} );

function book_a_session_get_bundles() {

    return rest_ensure_response( array(
        
        "bundles" => book_a_session_get_table_array( "bundle", "quantity", "ASC" ),
        "bundle_discounts" => book_a_session_get_table_array( "bundle_currency", "quantity", "ASC" ),
        "bundle_payment_methods" => book_a_session_get_table_array( "bundle_payment_method", "quantity", "ASC" ),
    
    ) );

}

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/bundles/', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_bundles',

    ) );
} );

function book_a_session_get_locations() {

    return book_a_session_get_table_array( "location" );

}

function book_a_session_get_location_by_id( $data ) {
    
    $id_number = $data["id"];
    global $wpdb;
    $table_name = $wpdb->prefix . "book_a_session_location";
    $result = $wpdb->get_row(
        "SELECT 
        * 
        FROM $table_name 
        WHERE id = $id_number",
        ARRAY_A );

    return $result;
    
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/locations/', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_locations',

    ) );
} );

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/locations/(?P<id>\d+)', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_location_by_id',

    ) );
} );

function book_a_session_get_services() {

    return book_a_session_get_table_array( "service" );

}

function book_a_session_get_service_by_id( $data ) {
    
    $id_number = $data["id"];
    global $wpdb;
    $table_name = $wpdb->prefix . "book_a_session_service";
    $result = $wpdb->get_row(
        "SELECT 
        * 
        FROM $table_name 
        WHERE id = $id_number",
        ARRAY_A );

    if ( $result ) {

        $service_img_full_src_array = wp_get_attachment_image_src( $result["image_id"], "full" );

        $result["image_full_url"]    = $service_img_full_src_array[0];
        $result["image_full_width"]  = $service_img_full_src_array[1];
        $result["image_full_height"] = $service_img_full_src_array[2];

        $service_img_medium_src_array = wp_get_attachment_image_src( $result["image_id"], "medium" );

        $result["image_medium_url"]    = $service_img_medium_src_array[0];
        $result["image_medium_width"]  = $service_img_medium_src_array[1];
        $result["image_medium_height"] = $service_img_medium_src_array[2];

        $service_img_thumbnail_src_array = wp_get_attachment_image_src( $result["image_id"], "thumbnail" );

        $result["image_thumbnail_url"]    = $service_img_thumbnail_src_array[0];
        $result["image_thumbnail_width"]  = $service_img_thumbnail_src_array[1];
        $result["image_thumbnail_height"] = $service_img_thumbnail_src_array[2];

        return $result;

    } else return new WP_Error( __( "Service not found", "book-a-session" ) );
    
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/services/', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_services',

    ) );
} );

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/services/(?P<id>\d+)', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_service_by_id',

    ) );
} );

function book_a_session_get_service_types() {

    return book_a_session_get_table_array( "service_type" );

}

function book_a_session_get_service_type_by_id( $data ) {
    
    $id_number = $data["id"];
    global $wpdb;
    $table_name = $wpdb->prefix . "book_a_session_service_type";
    $result = $wpdb->get_row(
        "SELECT 
        * 
        FROM $table_name 
        WHERE id = $id_number",
        ARRAY_A );

    return $result;
    
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/service-types/', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_service_types',

    ) );
} );

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/services-type/(?P<id>\d+)', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_service_type_by_id',

    ) );
} );

function book_a_session_get_currencies() {

    return book_a_session_get_table_array( "currency" );

}

function book_a_session_get_currency_by_id( $data ) {
    
    $id_number = $data["id"];
    global $wpdb;
    $table_name = $wpdb->prefix . "book_a_session_currency";
    $result = $wpdb->get_row(
        "SELECT 
        * 
        FROM $table_name 
        WHERE id = $id_number",
        ARRAY_A );

    return $result;
    
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/currencies/', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_currencies',

    ) );
} );

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/currencies/(?P<id>\d+)', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_currency_by_id',

    ) );
} );

function book_a_session_get_payment_methods() {

    return book_a_session_get_table_array( "payment_method" );

}

function book_a_session_get_payment_method_by_id( $data ) {
    
    $id_number = $data["id"];
    global $wpdb;
    $table_name = $wpdb->prefix . "book_a_session_payment_method";
    $result = $wpdb->get_row(
        "SELECT 
        * 
        FROM $table_name 
        WHERE id = $id_number",
        ARRAY_A );

    return $result;
    
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/payment-methods/', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_payment_methods',

    ) );
} );

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/payment-methods/(?P<id>\d+)', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_payment_method_by_id',

    ) );
} );

function book_a_session_get_schedule() {

    return book_a_session_get_table_array( "schedule" );

}


function book_a_session_get_schedule_by_id( $data ) {
    
    $result = book_a_session_get_session_time( $data["id"]);

    if ( ! empty( $result ) ) {

        return array( $result );

    } else {

        return false;

    }
    
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/schedule/', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_schedule',

    ) );
} );

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/schedule/(?P<id>\d+)', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_schedule_by_id',

    ) );
} );

// This may have to be authenticated in some way

function book_a_session_get_receipts() {

    return book_a_session_get_table_array( "receipt" );

}

function book_a_session_get_receipt_by_id( $data ) {
    
    $id_number = $data["id"];
    global $wpdb;
    $table_name = $wpdb->prefix . "book_a_session_receipt";
    $result = $wpdb->get_row(
        "SELECT 
        * 
        FROM $table_name 
        WHERE id = $id_number",
        ARRAY_A );

    return $result;
    
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/receipts/', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_receipts',

    ) );
} );

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/receipits/(?P<id>\d+)', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_receipt_by_id',

    ) );
} );

function book_a_session_get_practitioners() {

    return book_a_session_get_practitioner_array();

}


function book_a_session_get_api_price_tag( $data ) {

    return array( book_a_session_get_price_tag( $data["amount"], $data["id"] ) );

}

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/practitioners/', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_practitioners',

    ) );
} );

add_action( 'rest_api_init', function () {
    register_rest_route( 'book-a-session/v1', '/price-tag/(?P<id>\d+)/(?P<amount>\d+(\.\d{1,2})?)', array(
        'methods' => 'GET',
        'callback' => 'book_a_session_get_api_price_tag',

    ) );
} );