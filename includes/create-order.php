<?php

if ( ! defined( 'ABSPATH' ) ) exit;

function book_a_session_create_order( $data ){

    if ( ! empty( $data['quantity'] ) ) {

        // Prepare database insertion

        $order_id = book_a_session_generate_order_id();
        $quantity = $data['quantity'];
        $insertionCount = 0;
        $session = $project = 0;
        $add_order_db_result = array();

        if ( ! empty( $data['session_project'] ) ) {

            if ( $data['session_project'] === 'session' ) { $session = 1; $project = 0; }
            else { $project = 1; $session = 0; }

        } else $session = 1;

        // Insert order

        if (! empty( $data['service_id'] )         
        &&  ! empty( $data['region_id'] )          
        &&  ! empty( $data['practitioner_id'] )    
        &&  ! empty( $data['user_id'] )            
        &&  ! empty( $data['location_id'] )        
        &&  ! empty( $data["currency_id"] )                          
        &&  ! empty( $data["total"] )                 
        &&  ! empty( $data['payment_method_id'] )  
        &&  ! empty( $data['booking_status'] )     
        &&  ! empty( $data['payment_status'] )           
        ) :

            global $wpdb;
            $table_name_order = $wpdb->prefix . "book_a_session_order";

            // Prepare order values

            $order_value_array = array( 

                'order_id'          => $order_id,
                'created'           => date("Y-m-d H:i:s"), 
                'session'           => $session,
                'project'           => $project,          
                'quantity'          => $data['quantity'], 
                'service_id'        => $data['service_id'], 
                'region_id'         => $data['region_id'], 
                'practitioner_id'   => $data['practitioner_id'], 
                'user_id'           => $data['user_id'], 
                'location_id'       => $data['location_id'], 
                'payment_method_id' => $data['payment_method_id'], 
                'currency_id'       => $data["currency_id"], 
                'base_price'        => $data["base_price"],
                'location_charge'   => $data["location_charge"],
                'discount'          => $data["discount"],
                'total'             => $data["total"], 
                'amount_paid'       => $data["amount_paid"], 
                'total_due'         => $data["total_due"],
                'booking_status'    => $data['booking_status'], 
                'payment_status'    => $data['payment_status'], 
                'vfc_mobile'        => $data['vfc_mobile'], 
                'note'              => $data['note']

            );

            // Insert order

            $order_result = $wpdb->insert( 

                $table_name_order, 
                $order_value_array, 

                array( 

                    '%d', // Order ID
                    '%s', // Created
                    '%d', // Session
                    '%d', // Project
                    '%d', // Quantity
                    '%d', // Service ID
                    '%d', // Region ID
                    '%d', // Practitioner ID
                    '%d', // User ID
                    '%d', // Location ID
                    '%d', // Payment Method ID
                    '%d', // Currency ID
                    '%f', // Base Price
                    '%f', // Location Charge
                    '%f', // Discount
                    '%f', // Total
                    '%f', // Amount Paid
                    '%f', // Total Due
                    '%s', // Booking Status
                    '%s', // Payment Status
                    '%s', // VFC Mobile
                    '%s', // Note

                    ) 

            );

            $add_order_db_result[] = $order_result;

            // Loop through the posted quantity

            for ($i = 0; $i < $quantity; $i++) :

                global $wpdb;
                $table_name_session = $wpdb->prefix . "book_a_session_session";

                // Check all required values have been posted or obtained otherwise
                
                if ( $session ) :

                    // Prepare session values

                    $session_datetime = book_a_session_get_session_time( $data['session_time'][$i], false, $data['session_date'][$i] );

                    $session_value_array = array( 

                        'order_id'          => $order_id,
                        'schedule_id'       => $data['session_time'][$i],
                        'date'              => $data['session_date'][$i],
                        'start_datetime'    => $session_datetime["start_datetime_object"]->format("Y-m-d H:i:s"),
                        'end_datetime'      => $session_datetime["end_datetime_object"]->format("Y-m-d H:i:s"),

                    );

                    // Insert order

                    $session_result = $wpdb->insert( 

                        $table_name_session, 
                        $session_value_array, 
                        array( 

                            '%d', // Order ID
                            '%d', // Schedule ID
                            '%s', // Date
                            '%s', // Start Date Time
                            '%s', // End Date Time

                            ) 

                    );

                    $add_order_db_result[] = $session_result;

                else :

                    // TO DO: Add Project Start Date and Project End Fields that appear if $data['session_project] === 'project' 

                endif;

            endfor;

        endif;

        // Create Invoice

        if ( ! in_array( false, $add_order_db_result, true ) ) {
            
            // Set defaults for optional values

            $total_overridden = false;
            $data['payment_method_id'] == 3 ? $vfc_invoice         = 1 : $vfc_invoice      = 0;
            $total_overridden               ? $total_overridden    = 1 : $total_overridden = 0;

            // Get sessions, sorted by earliest to latest

            $sessions_array = book_a_session_get_table_array( "session", "start_datetime", "ASC", "*", array( "order_id", "=", $order_id ) );
            
            // Determine payment instructions

            if (
                $data['payment_status'] == 'Cash payment due'              || 
                $data['payment_status'] == 'Online payment due'            || 
                $data['payment_status'] == 'Vodafone Cash payment due'     ||
                $data['payment_status'] == 'Cash payment partly paid'      ||
                $data['payment_status'] == 'Online payment partly paid'    ||
                $data['payment_status'] == 'Vodafone Cash partly paid'     ){

                $due = 1;
                $due_by_date = $sessions_array[0]->start_datetime;

                if ( $data['payment_status'] == 'Cash payment due' || $data['payment_status'] == 'Cash payment partly paid' ) { 

                    // Cash instructions
                    $cash_due_instruction = 'Cash payment due on first meeting.';
                    $due_by_instruction = $cash_due_instruction;
                    
                } elseif ( $data['payment_status'] == 'Vodafone Cash payment due' || $data['payment_status'] == 'Vodafone Cash partly paid' ) {

                    // Get Vodafone Cash invoice option values

                    $receiving_vfc_mobile           = get_option( 'book_a_session_options_invoices' )['vfc_receiving_mobile_number'];
                    $due_date_offset_measurement    = get_option( 'book_a_session_options_invoices' )['due_date_offset_measurement'];
                    $due_date_offset_anchor         = get_option( 'book_a_session_options_invoices' )['due_date_offset_anchor'];
                    $due_date_offset_value          = get_option( 'book_a_session_options_invoices' )['due_date_offset_value'];

                    if ( (int)$due_date_offset_value === 1 ) {

                        // Remove 's' at end of string (minutes, hours, days) when it's only one minute, hour, or day.

                        $due_date_offset_measurement = preg_replace( "/s$/", "", $due_date_offset_measurement );

                    }

                    // If due date options have been entered and retrieved correctly

                    if ( ! empty( $due_date_offset_anchor ) && ! empty( $due_date_offset_measurement ) && ! empty( $due_date_offset_value ) ) {

                        // Calculate due date if the period is to be measured from the issue date, i.e. issue date +1 day

                        if ( $due_date_offset_anchor === 'issue_date' ) {

                            $issue_date = date("Y-m-d H:i:s");
                            $str = '+' . $due_date_offset_value . ' ' . $due_date_offset_measurement;
                            $due_by_date = date( "Y-m-d H:i:s", strtotime( $str, strtotime( $issue_date ) ) );

                        // Calculate due date if the period is to be measured from the session date, by getting the first session date, its time, putting them together as a datetime object, and modifying it according to user options, i.e. session date -1 day.

                        } elseif ( $due_date_offset_anchor === 'session_date' ) {

                            $str = '-' . $due_date_offset_value . ' ' . $due_date_offset_measurement;
                            $due_by_date = date( "Y-m-d H:i:s", strtotime( $str, strtotime( $sessions_array[0]->start_datetime ) ) );

                        }

                    }

                    // VFC instructions

                    $vfc_due_instruction = 'Vodafone Cash payment due before order can be confirmed. Please send the payment to the number above. Once received and confirmed, we\'ll send you your confirmation email. Thanks!';
                    $due_by_instruction = $vfc_due_instruction;

                } elseif ( $data['payment_status'] == 'Online payment due' || $data['payment_status'] == 'Online payment partly paid' ) {

                    // Online payment instructions

                    $online_payable = isset( get_option( "book_a_session_options_paymentmethods" )["online_payable"] ) ? get_option( "book_a_session_options_paymentmethods" )["online_payable"] : book_a_session_options_default( "paymentmethods", "online_payable" );

                    if ( $online_payable === "due_date" ) {

                        $online_due_instruction = 'Payment due before order can be confirmed.';
                        $due_by_instruction = $online_due_instruction;
    
                    } else {

                        $due                = 0;
                        $due_by_date        = date("Y-m-d H:i:s");
                        $due_by_instruction = null;
        
                    }

                }

            } else {

                $due                = 0;
                $due_by_date        = date("Y-m-d H:i:s");
                $due_by_instruction = null;

            }

            // Set price breakdown values and totals

            (float)$grand_total = ( (float)$data["base_price"] + (float)$data["location_charge"] - (float)$data["discount"] ) * $data['quantity'];
            (float)$grand_total_due = (float)$grand_total - (float)$data["amount_paid"];

            // Prepare invoice insertion

            $table_name_invoice = $wpdb->prefix . "book_a_session_invoice";

            // Prepare invoice values

            $invoice_value_array = array(

                'issue_date'            => date("Y-m-d"),
                'order_id'              => $order_id,
                'user_id'               => $data['user_id'],
                'vfc_invoice'           => $vfc_invoice,
                'vfc_mobile_number'     => $data['vfc_mobile'],
                'due'                   => $due,
                'due_by_date'           => $due_by_date,
                'due_by_instruction'    => $due_by_instruction,
                'total_overridden'      => $total_overridden,
                'currency_id'           => $data["currency_id"],
                'base_price'            => $data["base_price"],
                'location_charge'       => $data["location_charge"],
                'discount'              => $data["discount"],
                'quantity'              => $data['quantity'],
                'amount_paid'           => $data["amount_paid"],
                'total'                 => $data["total"],
                'grand_total'           => $data["total"],
                'grand_total_due'       => $data["total"] - $data["amount_paid"],

            );

            // Prepare invoice value formats

            $invoice_format_array = array(

                '%s', // Issue Date
                '%d', // Order ID
                '%d', // User ID
                '%d', // VFC Invoice
                '%s', // VFC Mobile Number
                '%d', // Due
                '%s', // Due By Date
                '%s', // Due By Instruction
                '%d', // Total Overridden
                '%d', // Currency ID
                '%f', // Base Price
                '%f', // Location Charge
                '%f', // Discount
                '%d', // Quantity
                '%f', // Amount Paid
                '%f', // Total
                '%f', // Grand Total
                '%f'  // Grand Total Due

            );

            // Add Invoice

            $add_invoice_db_result = $wpdb->insert( $table_name_invoice, $invoice_value_array, $invoice_format_array );

            // If that worked, check if we are required to send any invoice emails

            if ( ! empty ( $add_invoice_db_result ) ) {

                // Get invoice id

                $added_invoice = $wpdb->get_row( "SELECT id from $table_name_invoice ORDER BY id DESC LIMIT 1", OBJECT );
                $added_invoice_id = $added_invoice->id;

                // Initialise recipients argument array for book_a_session_send_invoice()

                $recipients = array();

                // Populate the array depending on posted values

                $recipients["client"]       = ! empty( $data['send_invoice_client'] )          ? true : false;
                $recipients["practitioner"] = ! empty( $data['send_invoice_practitioner'] )    ? true : false;
                $recipients["self"]         = ! empty( $data['send_invoice_self'] )            ? true : false;

                // If any of the above are true, send invoice

                if ( in_array( true, $recipients, true ) ) {

                    $invoice_send_result[] = book_a_session_send_invoice( $added_invoice_id, false, $recipients );

                }


            }

            // Get the order we just inserted by searching for the order ID we used, using the order id and associated data to add timetable entries

            $order = $wpdb->get_row(
                "
                SELECT id, created, service_id, practitioner_id, user_id, location_id 
                FROM $table_name_order 
                WHERE order_id = $order_id
                ", OBJECT );

            $sessions = $wpdb->get_results(
                "
                SELECT * 
                FROM $table_name_session 
                WHERE order_id = $order_id
                ", OBJECT );

            // Prepare timetable insertion

            $table_name_timetable = $wpdb->prefix . "book_a_session_timetable";
            $add_timetable_db_result = array();

            // If we found more than one order, loop over the array and insert each order into the timetable

            if ( $order ) {

                if ( is_array( $sessions ) ) {

                    foreach ( $sessions as $session_row ) {
                        
                        // Prepare timetable values for each order

                        $timetable_value_array = array(

                            'date'              => $session_row->date,
                            'start_datetime'    => $session_row->start_datetime,
                            'end_datetime'      => $session_row->end_datetime,
                            'schedule_id'       => $session_row->schedule_id,
                            'order_id'          => $order_id, 
                            'service_id'        => $order->service_id,
                            'location_id'       => $order->location_id,
                            'practitioner_id'   => $order->practitioner_id, 
                            'client_id'         => $order->user_id,

                        );

                        // Prepare timetable value formats for each order

                        $timetable_format_array = array(

                            '%s', // Date
                            '%s', // Start Datetime
                            '%s', // End Datetime
                            '%d', // Schedule ID
                            '%d', // Service ID
                            '%d', // order ID
                            '%d', // Location ID
                            '%d', // Practitioner ID
                            '%d', // Client ID

                        );

                        // Add Timetable entries

                        $add_timetable_db_result[] = $wpdb->insert( $table_name_timetable, $timetable_value_array, $timetable_format_array );

                    }

                // Otherwise insert a single order

                } else {

                    $timetable_value_array = array(

                        'date'              => $sessions->date,
                        'start_datetime'    => $sessions->start_datetime,
                        'end_datetime'      => $sessions->end_datetime,
                        'schedule_id'       => $sessions->schedule_id,
                        'order_id'          => $order_id, 
                        'service_id'        => $order->service_id,
                        'location_id'       => $order->location_id,
                        'practitioner_id'   => $order->practitioner_id, 
                        'client_id'         => $order->user_id,

                    );

                    // Prepare timetable value formats for each order

                    $timetable_format_array = array(

                        '%s', // Date
                        '%s', // Start Datetime
                        '%s', // End Datetime
                        '%d', // Schedule ID
                        '%d', // Service ID
                        '%d', // order ID
                        '%d', // Location ID
                        '%d', // Practitioner ID
                        '%d', // Client ID

                    );

                    // Add Timetable entry

                    $add_timetable_db_result[] = $wpdb->insert( $table_name_timetable, $timetable_value_array, $timetable_format_array );

                }

            }

            return array(
                
                "order_id"   => $order_id,
                "invoice_id" => $added_invoice_id,
                "order"      => book_a_session_get_order_by_id( array( "order_id" => $order_id ) )->get_data(),
                "invoice"    => $invoice_value_array,

            );

        } else {

            return new WP_Error( __( "Couldn't create the order", "book_a_session" ) );

        }
        
    } else {

        return new WP_Error( __( "Missing details", "book_a_session" ) );

    }

}

// Create Order WP REST API POST Endpoint

add_action( 'rest_api_init', function () {

    register_rest_route( 'book-a-session/v1', '/orders/', array(

        'methods'               => 'POST',
        'callback'              => 'book_a_session_create_order',
        'permission_callback'   => function( $request ) {

            // You just need to be logged in to create an order

            if ( is_user_logged_in() ) return true;
            else return false;

        }
    ) );

} );
