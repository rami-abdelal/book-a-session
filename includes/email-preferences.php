<?php // Book A Session - Dashboard Email Preferences Partial. Currently unused as of version 1.0.

// Exit if file is called directly

if ( ! defined( 'ABSPATH' ) ) exit;

?>

<!--
<div class='book-a-session-content book-a-session-animated book-a-session-dashboard-email-preferences book-a-session-hidden'>
    <h2>Email Preferences</h2>
    <form action="" novalidate="novalidate">
    <?php //wp_nonce_field( "user_" . $user_id . "_update_email_preferences" ); ?>

    <div class='book-a-session-switch-field'>
        <label class='book-a-session-switch'>
            <input type="checkbox" id="email_preferences_invoices" name="email_preferences_invoices" value="1">
            <span class='book-a-session-animated book-a-session-slider'></span>
        </label>
        <label for="email_preferences_invoices">Receive invoices</label>
    </div>

    <div class='book-a-session-switch-field'>
        <label class='book-a-session-switch'>
            <input type="checkbox" id="email_preferences_reminders" name="email_preferences_reminders" value="1">
            <span class='book-a-session-animated book-a-session-slider'></span>
        </label>
        <label for="email_preferences_reminders">Receive reminder emails</label>
    </div>

    <div class='book-a-session-switch-field'>
        <label class='book-a-session-switch'>
            <input type="checkbox" id="email_preferences_promotional" name="email_preferences_promotional" value="1">
            <span class='book-a-session-animated book-a-session-slider'></span>
        </label>
        <label for="email_preferences_promotional">Receive promotional emails</label>
    </div>


    <input type="submit" class="book-a-session-animated" name="submit_email_preferences" value="Save changes">
    </form>
</div>-->

<?php 
