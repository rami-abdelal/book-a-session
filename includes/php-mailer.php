<?php

if ( ! defined( 'ABSPATH' ) ) exit;

// Initialise PHPMailer according to user settings

function book_a_session_php_mailer() {

  // Check if PHPMailer has been enabled in the admin settings area. It is disabled by default.

  $phpmailer_enabled = isset( get_option( "book_a_session_options_emails" )["phpmailer_enabled"] ) ? get_option( "book_a_session_options_emails" )["phpmailer_enabled"] : book_a_session_options_default( "emails", "phpmailer_enabled" );

  if ( $phpmailer_enabled ) {

      // Add action to the phpmailer_init hook

      add_action( 'phpmailer_init', 'mailer_config', 10, 1);

      // Define the phpmailer function as the action to be hooked onto the phpmailer_init hook

      function mailer_config( PHPMailer $mailer ){

        // Get essential options which should be set up in the Emails settings page for Book A Session

        $phpmailer_host      = isset( get_option( "book_a_session_options_emails" )["phpmailer_host"] )       ? get_option( "book_a_session_options_emails" )["phpmailer_host"]      : book_a_session_options_default( "emails", "phpmailer_host" );
        $phpmailer_smtp_port = isset( get_option( "book_a_session_options_emails" )["phpmailer_smtp_port"] )  ? get_option( "book_a_session_options_emails" )["phpmailer_smtp_port"] : book_a_session_options_default( "emails", "phpmailer_smtp_port" );
        $phpmailer_username  = isset( get_option( "book_a_session_options_emails" )["phpmailer_username"] )   ? get_option( "book_a_session_options_emails" )["phpmailer_username"]  : book_a_session_options_default( "emails", "phpmailer_username" );
        $phpmailer_password  = ! empty( get_option( "book_a_session_options_emails" )["phpmailer_password"] ) ? get_option( "book_a_session_options_emails" )["phpmailer_password"]  : BOOK_A_SESSION_SMTP_PASSWORD;

        // If all essential data can be found, set PHPMailer up

        if ( ! empty( $phpmailer_host ) && ! empty( intval( $phpmailer_smtp_port ) ) && ! empty( $phpmailer_username ) && ! empty( $phpmailer_password ) ) {

          // Get optional data

          $phpmailer_smtpsecure         = isset( get_option( "book_a_session_options_emails" )["phpmailer_smtpsecure"] )         ? get_option( "book_a_session_options_emails" )["phpmailer_smtpsecure"]         : book_a_session_options_default( "emails", "phpmailer_smtpsecure" );
          $phpmailer_from_email_address = isset( get_option( "book_a_session_options_emails" )["phpmailer_from_email_address"] ) ? get_option( "book_a_session_options_emails" )["phpmailer_from_email_address"] : book_a_session_options_default( "emails", "phpmailer_from_email_address" );
          $phpmailer_from_name          = isset( get_option( "book_a_session_options_emails" )["phpmailer_from_name"] )          ? get_option( "book_a_session_options_emails" )["phpmailer_from_name"]          : book_a_session_options_default( "emails", "phpmailer_from_name" );

          $mailer->IsSMTP();
          $mailer->Host = $phpmailer_host;
          $mailer->Port = intval( $phpmailer_smtp_port );
          $mailer->SMTPDebug = 0; // write 0 if you don't want to see client/server communication in page, 2 for full writing, this would usually appear before the array of email addresses that have been successfully sent to in book_a_session_send_invoice(), often used with the REST API via AJAX on the frontend script, public/js/script.js
          $mailer->CharSet  = "utf-8";
          $mailer->SMTPAuth = true;
          $mailer->Username = $phpmailer_username;
          $mailer->Password = $phpmailer_password;

          // Optional settings

          if ( $phpmailer_smtpsecure )          $mailer->SMTPSecure  = $phpmailer_smtpsecure;
          if ( $phpmailer_from_email_address )  $mailer->From        = $phpmailer_from_email_address;
          if ( $phpmailer_from_name )           $mailer->FromName    = $phpmailer_from_name;

        }

      }

    add_action('wp_mail_failed', 'log_mailer_errors', 10, 1);

    function log_mailer_errors(){

      $fn = ABSPATH . '/mail.log'; // say you've got a mail.log file in your server root
      $fp = fopen($fn, 'a');
      fputs($fp, "Mailer Error: " . $mailer->ErrorInfo ."\n");
      fclose($fp);

    }

    if ( count($_POST) != 0 ){

        // on top of page
        // Then collect form data and try to send the form and write messages and so on...
        // Note: set $mailer->SMTPDebug to 0 to prevent it to write server messages on the page

    }
    
  } 

}