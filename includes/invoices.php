<?php

if ( ! defined( 'ABSPATH' ) ) exit;

// Sends invoice emails. Takes an invoice id and uses that to find all the necessary information to build the HTML invoice email and send it to the specified users.
// $invoice_id = int. Used to identify the invoice we'll be creating and for which we'll sending an email
// $reminder = bool. If true, this will be a reminder email with different wording
// $recipients = Associative array of boolean values with the keys client, practitioner and self, used to identify who the email should be sent to.
// $preview = bool. If true, no emails are sent, instead, the invoice HTML will be returned to be used in a preview iframe. If false or not provided, emails are sent as normal.
// return = Returns false on error, or an indexed array of responses indicating success of wp_mail for any or all three of the specified recipients.
// Returned values here are the recipient's email address, which means wp_mail() was successful, and false if wp_mail() failed. Returns HTML string if preview is true.

function book_a_session_send_invoice( $invoice_id, $reminder = false, $recipients = array( "client" => false, "practitioner" => false, "self" => false ), $preview = false ) {

    // Prepare database

    global $wpdb;
    $table_name_invoice = $wpdb->prefix . "book_a_session_invoice";
    $table_name_order = $wpdb->prefix . "book_a_session_order";

    // Get invoice row and order id

    if ( ! empty( $invoice_id ) ) {

        $invoice = $wpdb->get_row( "SELECT * from $table_name_invoice WHERE id = $invoice_id LIMIT 1", OBJECT );
        $order_id = $invoice->order_id;

        // Use the order id to get order row(s)

        if ( ! empty( $order_id ) ) {

            $order = $wpdb->get_row( "SELECT * from $table_name_order WHERE order_id = $order_id", OBJECT );

            // If all database gets are successful, begin extracting data to build the email

            if ( ! empty( $order ) ) {

                // Extract primary order meta data found directly in the result

                $client_id                  = $order->user_id;
                $practitioner_id            = $order->practitioner_id;
                $region_id                  = $order->region_id;
                $quantity                   = $order->quantity;
                $service_id                 = $order->service_id;
                $location_id                = $order->location_id;
                $payment_method_id          = $order->payment_method_id;
                $booking_status             = $order->booking_status;
                $payment_status             = $order->payment_status;

                $session_order              = $order->session ? true : false;
                $project_order              = $order->project ? true : false;

                //if ( ! current_user_can( 'manage_options' ) ) return false;

                // Get secondary wave of order meta data from primary data

                $due_by_date_input          = new DateTime( $invoice->due_by_date );
                $due_by_date                = book_a_session_convert_datetime( $due_by_date_input, false, $region_id )->format("Y-m-d H:i:s");
                $issue_date_input           = new DateTime( $invoice->issue_date );
                $issue_date                 = book_a_session_convert_datetime( $issue_date_input, false, $region_id )->format("Y-m-d H:i:s");
                $client_first_name          = get_userdata( $client_id )->first_name;
                $client_last_name           = get_userdata( $client_id )->last_name;
                $client_email               = get_userdata( $client_id )->user_email;
                $practitioner_first_name    = get_userdata( $practitioner_id )->first_name;
                $practitioner_last_name     = get_userdata( $practitioner_id )->last_name;
                $practitioner_email         = get_userdata( $practitioner_id )->user_email;
                $service_row                = book_a_session_get_service_by_id( array( "id" => $service_id ) );
                $service_name               = $service_row["name"];
                $service_image_url          = ! empty( wp_get_attachment_image_src( $service_row["image_id"], "full" )[0] ) ? wp_get_attachment_image_src( $service_row["image_id"], "full" )[0] : false;
                $service_type_id            = $service_row["service_type_id"];
                $service_type_row           = book_a_session_get_service_type_by_id( array( "id" => $service_type_id ) );
                $practitioner_title         = $service_type_row["practitioner_title"];
                $location_row               = book_a_session_get_location_by_id( array( "id" => $location_id ) );
                $location_name              = $location_row["name"];
                $payment_method_name        = book_a_session_get_payment_method_by_id( array( "id" => $payment_method_id ) )["name"];
                $logo_url                   = wp_get_attachment_url( get_option( 'book_a_session_options_general' )["image_logo"] ) ? wp_get_attachment_url( get_option( 'book_a_session_options_general' )["image_logo"] ): false;
                $vfc_logo                   = isset( get_option( 'book_a_session_options_invoices' )["vfc_logo"] ) ? get_option( 'book_a_session_options_invoices' )["vfc_logo"] : book_a_session_options_default( "invoices", "vfc_logo" );
                $cash_logo                  = isset( get_option( 'book_a_session_options_invoices' )["cash_logo"] ) ? get_option( 'book_a_session_options_invoices' )["cash_logo"] : book_a_session_options_default( "invoices", "cash_logo" );
                $paypal_logo                = isset( get_option( 'book_a_session_options_invoices' )["paypal_logo"] ) ? get_option( 'book_a_session_options_invoices' )["paypal_logo"] : book_a_session_options_default( "invoices", "paypal_logo" );
                $stripe_logo                = isset( get_option( 'book_a_session_options_invoices' )["stripe_logo"] ) ? get_option( 'book_a_session_options_invoices' )["stripe_logo"] : book_a_session_options_default( "invoices", "stripe_logo" );
                $vfc_receiving_number       = get_option( 'book_a_session_options_invoices' )["vfc_receiving_mobile_number"] ? get_option( 'book_a_session_options_invoices' )["vfc_receiving_mobile_number"] : '';
                $vfc_sending_number         = $invoice->vfc_mobile_number ? $invoice->vfc_mobile_number : '';

                // Get location address from database

                $location_address_array     = array();

                if ( $location_row["address_line_1"] ) { 
                    $location_address_array[] = $location_row["address_line_1"];
                }
                if ( $location_row["address_line_2"] ) { 
                    $location_address_array[] = $location_row["address_line_2"];
                }
                if ( $location_row["address_line_3"] ) { 
                    $location_address_array[] = $location_row["address_line_3"];
                }
                if ( $location_row["city"] ) { 
                    $location_address_array[] = $location_row["city"];
                }
                if ( $location_row["country"] ) { 
                    $location_address_array[] = $location_row["country"];
                }
                if ( $location_row["postcode_zipcode"] ) { 
                    $location_address_array[] = $location_row["postcode_zipcode"];
                }

                // Compile into a single string ready for use

                if ( count( $location_address_array ) > 1 ) {

                    $location_address_string = '';
                    
                    for ( $i = 0; $i < count( $location_address_array ); $i++ ) {

                        $location_address_string .= $location_address_array[$i];

                        if ( ! $i === count( $location_address_array ) - 1 ) {
                            $location_address_string .= ", ";
                        }

                    }

                } else {

                    $location_address_string = ! empty( $location_address_array[0] ) ? $location_address_array[0] : '';

                }

                // Determine invoice type

                $online_due                 = $payment_status == 'Online payment due'               ? true : false;
                $cash_due                   = $payment_status == 'Cash payment due'                 ? true : false;
                $vfc_due                    = $payment_status == 'Vodafone Cash payment due'        ? true : false;
                $online_partly_paid         = $payment_status == 'Online payment partly paid'       ? true : false;
                $cash_partly_paid           = $payment_status == 'Cash payment partly paid'         ? true : false;
                $vfc_partly_paid            = $payment_status == 'Vodafone Cash partly paid'        ? true : false;
                $online_paid                = $payment_status == 'Online payment received'          ? true : false;
                $cash_paid                  = $payment_status == 'Cash payment received'            ? true : false;
                $vfc_paid                   = $payment_status == 'Vodafone Cash payment received'   ? true : false;
                
                $vfc_invoice                = $vfc_due    || $vfc_partly_paid    || $vfc_paid       ? true : false;
                $cash_invoice               = $cash_due   || $cash_partly_paid   || $cash_paid      ? true : false;
                $online_invoice             = $online_due || $online_partly_paid || $online_paid    ? true : false;

                $vfc_logo_url               = $vfc_logo ? plugin_dir_url( dirname( __FILE__ ) ) . 'public/img/vfc-logo.png' : false;
                $cash_logo_url              = $cash_logo ? plugin_dir_url( dirname( __FILE__ ) ) . 'public/img/cash-logo.png' : false;
                $paypal_logo_url            = $paypal_logo ? plugin_dir_url( dirname( __FILE__ ) ) . 'public/img/paypal-logo.png' : false;
                $stripe_logo_url            = $stripe_logo ? plugin_dir_url( dirname( __FILE__ ) ) . 'public/img/stripe-logo.png' : false;


                // If sessions, create array of session dates and times, converted to the selected region's timezone by book_a_session_get_session_time()

                if ( $session_order ) {

                    $session_array = book_a_session_get_table_array( "session", "start_datetime", "ASC", "*", array( "order_id", "=", $order_id ) );

                    if ( ! empty( $session_array ) ) :

                        foreach ( $session_array as $session ) :

                            $datetime = book_a_session_get_session_time( $session->schedule_id, $region_id, $session->date );
    
                            $session_date_and_time[] = array( 
    
                                "date" => $datetime["start_datetime_object"]->format( get_option( 'date_format' ) ), 
                                "time" => $datetime["time_string"], 
                                "start_datetime_object" => $datetime["start_datetime_object"],
                                "end_datetime_object" => $datetime["end_datetime_object"],
                                "upcoming" => book_a_session_convert_datetime( new DateTime(), false, $region_id ) < $datetime["end_datetime_object"] ? true : false,
                                "first" => false
    
                            );
    
                        endforeach;
    
                    endif;

                // If it's not a session order, it's a project order

                } else {

                    // Do project order stuff later

                }

                // Since the session are ordered by session datetime ascending, get the first session and flag that as ["first"] = true.

                $session_date_and_time[ 0 ][ "first" ] = true;

                // Check for next session date. As the session_date_and_time array is sorted chronologically from earliest to latest session, we'll loop through and stop at the first session that hasn't passed the current time

                $next_session = false;
                $last_session = false;

                for ( $i = 0; $i < count( $session_date_and_time ); $i++ ) {

                    if ( $session_date_and_time[$i]["upcoming"] ) {

                        $next_session = $session_date_and_time[$i];
                        break;

                    } elseif ( $i === count( $session_date_and_time ) - 1 ) {

                        $last_session = $session_date_and_time[$i];

                    }

                }

                // Prepare email with user settings, or fallback to defaults

                $from_name          = get_option( 'book_a_session_options_emails' )['from_name']             ?       get_option( 'book_a_session_options_emails' )['from_name']                : get_bloginfo( 'name' );
                $from_email_address = get_option( 'book_a_session_options_emails' )['from_email_address']    ? "<" . get_option( 'book_a_session_options_emails' )['from_email_address'] . ">" : "<noreply@" . book_a_session_get_top_level_url() . ">";

                $from = "From: " . $from_name . " " . $from_email_address;

                // Set From and HTML email headers (currently all emails are HTML emails, until possibly a later update)

                $headers = array( $from, 'Content-Type: text/html; charset=UTF-8' );      
                
                // Initialise variables that may not be set in some circumstances in the if structures below

                $completed = "";
                $payment = false;

                if ( $vfc_due ) {

                    if ( $reminder ) {
                        $subject        = get_option( 'book_a_session_options_invoices' )['reminder_email_subject_vfc_due'] ? get_option( 'book_a_session_options_invoices' )['reminder_email_subject_vfc_due'] : book_a_session_options_default( "invoices", "reminder_email_subject_vfc_due" );  
                    } else {
                        $subject        = get_option( 'book_a_session_options_invoices' )['email_subject_vfc_due']          ? get_option( 'book_a_session_options_invoices' )['email_subject_vfc_due']          :  book_a_session_options_default( "invoices", "email_subject_vfc_due" );
                    }                

                    $hidden_preheader   = get_option( 'book_a_session_options_invoices' )['hidden_preheader_vfc_due']       ? get_option( 'book_a_session_options_invoices' )['hidden_preheader_vfc_due']       : book_a_session_options_default( "invoices", "hidden_preheader_vfc_due" );
                    $header_large_text  = get_option( 'book_a_session_options_invoices' )['header_large_text_vfc_due']      ? get_option( 'book_a_session_options_invoices' )['header_large_text_vfc_due']      : book_a_session_options_default( "invoices", "header_large_text_vfc_due" );
                    $header_subtitle    = get_option( 'book_a_session_options_invoices' )['header_subtitle_text_vfc_due']   ? get_option( 'book_a_session_options_invoices' )['header_subtitle_text_vfc_due']   : book_a_session_options_default( "invoices", "header_subtitle_text_vfc_due" );
                    $table_title        = get_option( 'book_a_session_options_invoices' )['table_title_vfc_due']            ? get_option( 'book_a_session_options_invoices' )['table_title_vfc_due']            : book_a_session_options_default( "invoices", "table_title_vfc_due" );
                    $table_paragraph    = get_option( 'book_a_session_options_invoices' )['table_paragraph_vfc_due']        ? get_option( 'book_a_session_options_invoices' )['table_paragraph_vfc_due']        : book_a_session_options_default( "invoices", "table_paragraph_vfc_due" );

                } elseif ( $vfc_partly_paid ) {

                    if ( $reminder ) {
                        $subject        = get_option( 'book_a_session_options_invoices' )['reminder_email_subject_vfc_partly_paid'] ? get_option( 'book_a_session_options_invoices' )['reminder_email_subject_vfc_partly_paid'] : book_a_session_options_default( "invoices", "reminder_email_subject_vfc_partly_paid" );  
                    } else {
                        $subject        = get_option( 'book_a_session_options_invoices' )['email_subject_vfc_partly_paid']          ? get_option( 'book_a_session_options_invoices' )['email_subject_vfc_partly_paid']          :  book_a_session_options_default( "invoices", "email_subject_vfc_partly_paid" );
                    }                

                    $hidden_preheader   = get_option( 'book_a_session_options_invoices' )['hidden_preheader_vfc_partly_paid']       ? get_option( 'book_a_session_options_invoices' )['hidden_preheader_vfc_partly_paid']       : book_a_session_options_default( "invoices", "hidden_preheader_vfc_partly_paid" );
                    $header_large_text  = get_option( 'book_a_session_options_invoices' )['header_large_text_vfc_partly_paid']      ? get_option( 'book_a_session_options_invoices' )['header_large_text_vfc_partly_paid']      : book_a_session_options_default( "invoices", "header_large_text_vfc_partly_paid" );
                    $header_subtitle    = get_option( 'book_a_session_options_invoices' )['header_subtitle_text_vfc_partly_paid']   ? get_option( 'book_a_session_options_invoices' )['header_subtitle_text_vfc_partly_paid']   : book_a_session_options_default( "invoices", "header_subtitle_text_vfc_partly_paid" );
                    $table_title        = get_option( 'book_a_session_options_invoices' )['table_title_vfc_partly_paid']            ? get_option( 'book_a_session_options_invoices' )['table_title_vfc_partly_paid']            : book_a_session_options_default( "invoices", "table_title_vfc_partly_paid" );
                    $table_paragraph    = get_option( 'book_a_session_options_invoices' )['table_paragraph_vfc_partly_paid']        ? get_option( 'book_a_session_options_invoices' )['table_paragraph_vfc_partly_paid']        : book_a_session_options_default( "invoices", "table_paragraph_vfc_partly_paid" );

                } elseif ( $vfc_paid ) {

                    if ( $reminder ) {
                        $subject        = get_option( 'book_a_session_options_invoices' )['reminder_email_subject_vfc_paid'] ? get_option( 'book_a_session_options_invoices' )['reminder_email_subject_vfc_paid'] : book_a_session_options_default( "invoices", "reminder_email_subject_vfc_paid" );  
                    } else {
                        $subject        = get_option( 'book_a_session_options_invoices' )['email_subject_vfc_paid']          ? get_option( 'book_a_session_options_invoices' )['email_subject_vfc_paid']          :  book_a_session_options_default( "invoices", "email_subject_vfc_paid" );
                    }                

                    $hidden_preheader   = get_option( 'book_a_session_options_invoices' )['hidden_preheader_vfc_paid']       ? get_option( 'book_a_session_options_invoices' )['hidden_preheader_vfc_paid']       : book_a_session_options_default( "invoices", "hidden_preheader_vfcpaid" );
                    $header_large_text  = get_option( 'book_a_session_options_invoices' )['header_large_text_vfc_paid']      ? get_option( 'book_a_session_options_invoices' )['header_large_text_vfc_paid']      : book_a_session_options_default( "invoices", "header_large_text_vfc_paid" );
                    $header_subtitle    = get_option( 'book_a_session_options_invoices' )['header_subtitle_text_vfc_paid']   ? get_option( 'book_a_session_options_invoices' )['header_subtitle_text_vfc_paid']   : book_a_session_options_default( "invoices", "header_subtitle_text_vfc_paid" );
                    $table_title        = get_option( 'book_a_session_options_invoices' )['table_title_vfc_paid']            ? get_option( 'book_a_session_options_invoices' )['table_title_vfc_paid']            : book_a_session_options_default( "invoices", "table_title_vfc_paid" );
                    $table_paragraph    = get_option( 'book_a_session_options_invoices' )['table_paragraph_vfc_paid']        ? get_option( 'book_a_session_options_invoices' )['table_paragraph_vfc_paid']        : book_a_session_options_default( "invoices", "table_paragraph_vfc_paid" );
                    $completed          = get_option( 'book_a_session_options_invoices' )['completed_vfc_paid']              ? get_option( 'book_a_session_options_invoices' )['completed_vfc_paid']              : book_a_session_options_default( "invoices", "completed_vfc_paid" );

                } elseif ( $online_due ) {

                    if ( $reminder ) {
                        $subject        = get_option( 'book_a_session_options_invoices' )['reminder_email_subject_online_due'] ? get_option( 'book_a_session_options_invoices' )['reminder_email_subject_online_due'] : book_a_session_options_default( "invoices", "reminder_email_subject_online_due" );  
                    } else {
                        $subject        = get_option( 'book_a_session_options_invoices' )['email_subject_online_due']          ? get_option( 'book_a_session_options_invoices' )['email_subject_online_due']          :  book_a_session_options_default( "invoices", "email_subject_online_due" );
                    }                

                    $hidden_preheader   = get_option( 'book_a_session_options_invoices' )['hidden_preheader_online_due']       ? get_option( 'book_a_session_options_invoices' )['hidden_preheader_online_due']       : book_a_session_options_default( "invoices", "hidden_preheader_online_due" );
                    $header_large_text  = get_option( 'book_a_session_options_invoices' )['header_large_text_online_due']      ? get_option( 'book_a_session_options_invoices' )['header_large_text_online_due']      : book_a_session_options_default( "invoices", "header_large_text_online_due" );
                    $header_subtitle    = get_option( 'book_a_session_options_invoices' )['header_subtitle_text_online_due']   ? get_option( 'book_a_session_options_invoices' )['header_subtitle_text_online_due']   : book_a_session_options_default( "invoices", "header_subtitle_text_online_due" );
                    $table_title        = get_option( 'book_a_session_options_invoices' )['table_title_online_due']            ? get_option( 'book_a_session_options_invoices' )['table_title_online_due']            : book_a_session_options_default( "invoices", "table_title_online_due" );
                    $table_paragraph    = get_option( 'book_a_session_options_invoices' )['table_paragraph_online_due']        ? get_option( 'book_a_session_options_invoices' )['table_paragraph_online_due']        : book_a_session_options_default( "invoices", "table_paragraph_online_due" );

                } elseif ( $online_partly_paid ) {

                    if ( $reminder ) {
                        $subject        = get_option( 'book_a_session_options_invoices' )['reminder_email_subject_online_partly_paid'] ? get_option( 'book_a_session_options_invoices' )['reminder_email_subject_online_partly_paid'] : book_a_session_options_default( "invoices", "reminder_email_subject_online_partly_paid" );  
                    } else {
                        $subject        = get_option( 'book_a_session_options_invoices' )['email_subject_online_partly_paid']          ? get_option( 'book_a_session_options_invoices' )['email_subject_online_partly_paid']          :  book_a_session_options_default( "invoices", "email_subject_online_partly_paid" );
                    }                

                    $hidden_preheader   = get_option( 'book_a_session_options_invoices' )['hidden_preheader_online_partly_paid']       ? get_option( 'book_a_session_options_invoices' )['hidden_preheader_online_partly_paid']       : book_a_session_options_default( "invoices", "hidden_preheader_online_partly_paid" );
                    $header_large_text  = get_option( 'book_a_session_options_invoices' )['header_large_text_online_partly_paid']      ? get_option( 'book_a_session_options_invoices' )['header_large_text_online_partly_paid']      : book_a_session_options_default( "invoices", "header_large_text_online_partly_paid" );
                    $header_subtitle    = get_option( 'book_a_session_options_invoices' )['header_subtitle_text_online_partly_paid']   ? get_option( 'book_a_session_options_invoices' )['header_subtitle_text_online_partly_paid']   : book_a_session_options_default( "invoices", "header_subtitle_text_online_partly_paid" );
                    $table_title        = get_option( 'book_a_session_options_invoices' )['table_title_online_partly_paid']            ? get_option( 'book_a_session_options_invoices' )['table_title_online_partly_paid']            : book_a_session_options_default( "invoices", "table_title_online_partly_paid" );
                    $table_paragraph    = get_option( 'book_a_session_options_invoices' )['table_paragraph_online_partly_paid']        ? get_option( 'book_a_session_options_invoices' )['table_paragraph_online_partly_paid']        : book_a_session_options_default( "invoices", "table_paragraph_online_partly_paid" );
                    
                    $payment = book_a_session_get_table_array( "payment", false, false, "*", array( "order_id", "=", $order_id ), 1 )[0] ? book_a_session_get_table_array( "payment", false, false, "*", array( "order_id", "=", $order_id ), 1 )[0] : false;

                } elseif( $online_paid ) {

                    if ( $reminder ) {
                        $subject        = get_option( 'book_a_session_options_invoices' )['reminder_email_subject_online_paid'] ? get_option( 'book_a_session_options_invoices' )['reminder_email_subject_online_paid'] : book_a_session_options_default( "invoices", "reminder_email_subject_online_paid" );  
                    } else {
                        $subject        = get_option( 'book_a_session_options_invoices' )['email_subject_online_paid']          ? get_option( 'book_a_session_options_invoices' )['email_subject_online_paid']          :  book_a_session_options_default( "invoices", "email_subject_online_paid" );
                    }                

                    $hidden_preheader   = get_option( 'book_a_session_options_invoices' )['hidden_preheader_online_paid']       ? get_option( 'book_a_session_options_invoices' )['hidden_preheader_online_paid']       : book_a_session_options_default( "invoices", "hidden_preheader_online_paid" );
                    $header_large_text  = get_option( 'book_a_session_options_invoices' )['header_large_text_online_paid']      ? get_option( 'book_a_session_options_invoices' )['header_large_text_online_paid']      : book_a_session_options_default( "invoices", "header_large_text_online_paid" );
                    $header_subtitle    = get_option( 'book_a_session_options_invoices' )['header_subtitle_text_online_paid']   ? get_option( 'book_a_session_options_invoices' )['header_subtitle_text_online_paid']   : book_a_session_options_default( "invoices", "header_subtitle_text_online_paid" );
                    $table_title        = get_option( 'book_a_session_options_invoices' )['table_title_online_paid']            ? get_option( 'book_a_session_options_invoices' )['table_title_online_paid']            : book_a_session_options_default( "invoices", "table_title_online_paid" );
                    $table_paragraph    = get_option( 'book_a_session_options_invoices' )['table_paragraph_online_paid']        ? get_option( 'book_a_session_options_invoices' )['table_paragraph_online_paid']        : book_a_session_options_default( "invoices", "table_paragraph_online_paid" );
                    $completed          = get_option( 'book_a_session_options_invoices' )['completed_online_paid']              ? get_option( 'book_a_session_options_invoices' )['completed_online_paid']              : book_a_session_options_default( "invoices", "completed_onlined_paid" );

                    $payment = book_a_session_get_table_array( "payment", false, false, "*", array( "order_id", "=", $order_id ), 1 )[0] ? book_a_session_get_table_array( "payment", false, false, "*", array( "order_id", "=", $order_id ), 1 )[0] : false;

                } elseif ( $cash_due ) {

                    if ( $reminder ) {
                        $subject        = get_option( 'book_a_session_options_invoices' )['reminder_email_subject_cash_due'] ? get_option( 'book_a_session_options_invoices' )['reminder_email_subject_cash_due'] : book_a_session_options_default( "invoices", "reminder_email_subject_cash_due" );  
                    } else {
                        $subject        = get_option( 'book_a_session_options_invoices' )['email_subject_cash_due']          ? get_option( 'book_a_session_options_invoices' )['email_subject_cash_due']          :  book_a_session_options_default( "invoices", "email_subject_cash_due" );
                    }                

                    $hidden_preheader   = get_option( 'book_a_session_options_invoices' )['hidden_preheader_cash_due']       ? get_option( 'book_a_session_options_invoices' )['hidden_preheader_cash_due']       : book_a_session_options_default( "invoices", "hidden_preheader_cash_due" );
                    $header_large_text  = get_option( 'book_a_session_options_invoices' )['header_large_text_cash_due']      ? get_option( 'book_a_session_options_invoices' )['header_large_text_cash_due']      : book_a_session_options_default( "invoices", "header_large_text_cash_due" );
                    $header_subtitle    = get_option( 'book_a_session_options_invoices' )['header_subtitle_text_cash_due']   ? get_option( 'book_a_session_options_invoices' )['header_subtitle_text_cash_due']   : book_a_session_options_default( "invoices", "header_subtitle_text_cash_due" );
                    $table_title        = get_option( 'book_a_session_options_invoices' )['table_title_cash_due']            ? get_option( 'book_a_session_options_invoices' )['table_title_cash_due']            : book_a_session_options_default( "invoices", "table_title_cash_due" );
                    $table_paragraph    = get_option( 'book_a_session_options_invoices' )['table_paragraph_cash_due']        ? get_option( 'book_a_session_options_invoices' )['table_paragraph_cash_due']        : book_a_session_options_default( "invoices", "table_paragraph_cash_due" );

                } elseif ( $cash_partly_paid ) {

                    if ( $reminder ) {
                        $subject        = get_option( 'book_a_session_options_invoices' )['reminder_email_subject_cash_partly_paid'] ? get_option( 'book_a_session_options_invoices' )['reminder_email_subject_cash_partly_paid'] : book_a_session_options_default( "invoices", "reminder_email_subject_cash_partly_paid" );  
                    } else {
                        $subject        = get_option( 'book_a_session_options_invoices' )['email_subject_cash_partly_paid']          ? get_option( 'book_a_session_options_invoices' )['email_subject_cash_partly_paid']          :  book_a_session_options_default( "invoices", "email_subject_cash_partly_paid" );
                    }                

                    $hidden_preheader   = get_option( 'book_a_session_options_invoices' )['hidden_preheader_cash_partly_paid']       ? get_option( 'book_a_session_options_invoices' )['hidden_preheader_cash_partly_paid']       : book_a_session_options_default( "invoices", "hidden_preheader_cash_partly_paid" );
                    $header_large_text  = get_option( 'book_a_session_options_invoices' )['header_large_text_cash_partly_paid']      ? get_option( 'book_a_session_options_invoices' )['header_large_text_cash_partly_paid']      : book_a_session_options_default( "invoices", "header_large_text_cash_partly_paid" );
                    $header_subtitle    = get_option( 'book_a_session_options_invoices' )['header_subtitle_text_cash_partly_paid']   ? get_option( 'book_a_session_options_invoices' )['header_subtitle_text_cash_partly_paid']   : book_a_session_options_default( "invoices", "header_subtitle_text_cash_partly_paid" );
                    $table_title        = get_option( 'book_a_session_options_invoices' )['table_title_cash_partly_paid']            ? get_option( 'book_a_session_options_invoices' )['table_title_cash_partly_paid']            : book_a_session_options_default( "invoices", "table_title_cash_partly_paid" );
                    $table_paragraph    = get_option( 'book_a_session_options_invoices' )['table_paragraph_cash_partly_paid']        ? get_option( 'book_a_session_options_invoices' )['table_paragraph_cash_partly_paid']        : book_a_session_options_default( "invoices", "table_paragraph_cash_partly_paid" );

                } elseif ( $cash_paid ) {

                    if ( $reminder ) {
                        $subject        = get_option( 'book_a_session_options_invoices' )['reminder_email_subject_cash_paid'] ? get_option( 'book_a_session_options_invoices' )['reminder_email_subject_cash_paid'] : book_a_session_options_default( "invoices", "reminder_email_subject_cash_paid" );  
                    } else {
                        $subject        = get_option( 'book_a_session_options_invoices' )['email_subject_cash_paid']          ? get_option( 'book_a_session_options_invoices' )['email_subject_cash_paid']          :  book_a_session_options_default( "invoices", "email_subject_cash_paid" );
                    }                

                    $hidden_preheader   = get_option( 'book_a_session_options_invoices' )['hidden_preheader_cash_paid']       ? get_option( 'book_a_session_options_invoices' )['hidden_preheader_cash_paid']       : book_a_session_options_default( "invoices", "hidden_preheader_cash_paid" );
                    $header_large_text  = get_option( 'book_a_session_options_invoices' )['header_large_text_cash_paid']      ? get_option( 'book_a_session_options_invoices' )['header_large_text_cash_paid']      : book_a_session_options_default( "invoices", "header_large_text_cash_paid" );
                    $header_subtitle    = get_option( 'book_a_session_options_invoices' )['header_subtitle_text_cash_paid']   ? get_option( 'book_a_session_options_invoices' )['header_subtitle_text_cash_paid']   : book_a_session_options_default( "invoices", "header_subtitle_text_cash_paid" );
                    $table_title        = get_option( 'book_a_session_options_invoices' )['table_title_cash_paid']            ? get_option( 'book_a_session_options_invoices' )['table_title_cash_paid']            : book_a_session_options_default( "invoices", "table_title_cash_paid" );
                    $table_paragraph    = get_option( 'book_a_session_options_invoices' )['table_paragraph_cash_paid']        ? get_option( 'book_a_session_options_invoices' )['table_paragraph_cash_paid']        : book_a_session_options_default( "invoices", "table_paragraph_cash_paid" );
                    $completed          = get_option( 'book_a_session_options_invoices' )['completed_cash_paid']              ? get_option( 'book_a_session_options_invoices' )['completed_cash_paid']              : book_a_session_options_default( "invoices", "completed_cash_paid" );

                } else {

                    // In the case of a failure to determine the type of invoice, create a plain language agnostic invoice

                    if ( $reminder ) {
                        $subject        = "(Reminder) Your invoice.";
                    } else {
                        $subject        = "Your invoice.";
                    }                

                    $hidden_preheader   = "Please find your invoice for %quantity% %session(s)% of %service% - %location% within.";
                    $header_large_text  = "Invoice";
                    $header_subtitle    = "%quantity% %session(s)% of %service% - %location%";
                    $table_title        = "Due: %totaldue%. Paid: %amountpaid%.";
                    $table_paragraph    = "Please find the itemised breakdown of your order below.";
                    $completed          = "";

                }

                // Insert invoice tag values

                $preprocessed_str_array = array(
                    $subject,  
                    $hidden_preheader,
                    $header_large_text,
                    $header_subtitle,
                    $table_title,
                    $table_paragraph,
                    $completed
                );

                $processed_str_array = array();

                foreach ( $preprocessed_str_array as $str ) {

                    // Site name
                    $str = str_replace( "%sitename%", get_bloginfo( 'name' ), $str );

                    // 10 digit order ID number
                    $str = str_replace( "%orderid%", $order_id, $str );

                    // Number of sessions in this order, e.g. 1
                    $str = str_replace( "%quantity%", $quantity, $str );

                    // Due by time
                    $str = str_replace( "%duebytime%", date( "h:i A", strtotime( $due_by_date ) ), $str );

                    // Due by date
                    $str = str_replace( "%duebydate%", date( get_option( "date_format" ), strtotime( $due_by_date ) ), $str );

                    // Service name
                    $str = str_replace( "%service%", $service_name, $str );

                    // Location name
                    $str = str_replace( "%location%", $location_name, $str );

                    // Prints Location address, if available
                    $str = str_replace( "locationaddress", $location_address_string, $str );

                    // Payment method name
                    $str = str_replace( "%paymentmethod%", $payment_method_name, $str );

                    // Booking status, e.g. Pending
                    $str = str_replace( "%bookingstatus%", $booking_status, $str );

                    // Payment status, e.g. Cash payment due
                    $str = str_replace( "%paymentstatus%", $payment_status, $str );

                    // Grand total due to be paid
                    $str = str_replace( "%totaldue%", book_a_session_get_price_tag( $invoice->grand_total_due, $invoice->currency_id ), $str );

                    // Amount paid toward this invoice
                    $str = str_replace( "%amountpaid%", book_a_session_get_price_tag( $invoice->amount_paid, $invoice->currency_id ), $str );

                    // Client's first name
                    $str = str_replace( "%clientfname%", $client_first_name, $str );

                    // Client's last name
                    $str = str_replace( "%clientlname%", $client_last_name, $str );

                    // Client's email address
                    $str = str_replace( "%clientemail%", $client_email, $str );

                    // Practitioner's title according to service type, e.g. Designer
                    $str = str_replace( "%practitle%", $practitioner_title, $str );

                    // Practitioner's first name
                    $str = str_replace( "%pracfname%", $practitioner_first_name, $str );

                    // Practitioner's last name
                    $str = str_replace( "%praclname%", $practitioner_last_name, $str );

                    // Practitioner's email address
                    $str = str_replace( "%pracemail%", $practitioner_email, $str );

                    // Prints "is" if there are any upcoming sessions in the order, or "was if there aren't
                    $str = str_replace( "%is/was%", $next_session ? "is" : "was", $str );

                    // Prints "first" if the order's first of multiple sessions is upcoming, "next" if the next session won't be their first in the order, and "last if all their sessions were completed. Prints nothing if there's only 1 session in the order.
                    if ( $quantity > 1 ) {

                        if ( $next_session["first"] ) {

                            $str = str_replace( "%first/next/last%", "first", $str );

                        } elseif ( $next_session ) {

                            $str = str_replace( "%first/next/last%", "next", $str );

                        } elseif ( $last_session ) {

                            $str = str_replace( "%first/next/last%", "last", $str );

                        }

                    } else {
                            $str = str_replace( "%first/next/last%", "", $str );
                    }

                    // First, next, or last session's time
                    $str = str_replace( "%fnltime%", $next_session ? str_replace( "-", " - ", $next_session["time"] ) : str_replace( "-", " - ", $last_session["time"] ), $str );

                    // First, next or last session's date
                    $str = str_replace( "%fnldate%", $next_session ? date( get_option( "date_format" ), strtotime( $next_session["date"] ) ) : date( get_option( "date_format" ), strtotime( $last_session["date"] ) ), $str );

                    // Prints "session" if there's only 1 session in the order, or "sessions" if there are more
                    $str = str_replace( "%session(s)%", $quantity == 1 ? "session" : "sessions", $str );

                    // Prints the mobile number to receive Vodafone Cash payments if applicable and available
                    $str = str_replace( "%vfcreceiver%", $vfc_receiving_number ? $vfc_receiving_number : "", $str );

                    // Prints the mobile number the client has provided that will be sending Vodafone Cash payments, if applicable and available
                    $str = str_replace( "%vfcsender%", $vfc_sending_number ? $vfc_sending_number : "", $str );

                    $processed_str_array[] = $str;

                }

                $subject            = $processed_str_array[0];
                $hidden_preheader   = $processed_str_array[1];
                $header_large_text  = $processed_str_array[2];
                $header_subtitle    = $processed_str_array[3];
                $table_title        = $processed_str_array[4];
                $table_paragraph    = $processed_str_array[5];
                $completed          = $processed_str_array[6];

                // Output hidden message to display for preview reading after the subject on email clients

                $body = $hidden_preheader ? "<div style='display: none;'>" . $hidden_preheader . " </div>" : "";

                // Insert logo if available

                $default_logo_background_color = book_a_session_options_default( "emails", "logo_background_color" );

                $logo_background_color = get_option( "book_a_session_options" )["email_logo_background_color"] ? get_option( "book_a_session_options" )["email_logo_background_color"] : $default_logo_background_color;

                if ( ! empty( $logo_url ) ) { 
                    
                    // Logo Wrapper

                    $body .= "<div style='
                                        width:                  100%;
                                        background-color:       " . $logo_background_color . ";'>";
                    
                    // Logo Image

                    $body .= "<img style='
                                        display:                block;
                                        width:                  320px; 
                                        height:                 auto;
                                        margin:                 0 auto;'
                                src='" . $logo_url . "' alt='" . get_bloginfo( "name" ) . " Logo'>";

                    // End Logo Wrapper and Logo Container

                    $body .= "</div>";

                }

                // Header Wrapper 

                $default_header_background_color            = book_a_session_options_default( "emails", "header_background_color" );
                $default_header_background_gradient_left    = book_a_session_options_default( "emails", "header_background_gradient_left" );
                $default_header_background_gradient_right   = book_a_session_options_default( "emails", "header_background_gradient_right" );
                $default_header_background_screen_color     = book_a_session_options_default( "emails", "header_background_screen_color" );
                $default_header_background_image            = book_a_session_options_default( "emails", "header_background_image" );
                $default_header_background_gradient         = book_a_session_options_default( "emails", "header_background_gradient" );

                $new_image_url                              = get_option( "book_a_session_options_emails" )["new_image_url"]                         ? get_option( "book_a_session_options" )["new_image_url"]                          : false;
                $header_background_gradient                 = get_option( "book_a_session_options_emails" )["header_background_gradient"] == true    ? true                                                                                   : false;

                $header_background_color                    = get_option( "book_a_session_options_emails" )["header_background_color"]               ? get_option( "book_a_session_options_emails" )["header_background_color"]               : $default_header_background_color;
                $header_background_image_option             = get_option( "book_a_session_options_emails" )["header_background_image"]               ? get_option( "book_a_session_options_emails" )["header_background_image"]               : $default_header_background_image;
                $header_background_gradient_left            = get_option( "book_a_session_options_emails" )["header_background_gradient_left"]       ? get_option( "book_a_session_options_emails" )["header_background_gradient_left"]       : $default_header_background_gradient_left;
                $header_background_gradient_right           = get_option( "book_a_session_options_emails" )["header_background_gradient_right"]      ? get_option( "book_a_session_options_emails" )["header_background_gradient_right"]      : $default_header_background_gradient_right;
                $header_background_screen_color             = get_option( "book_a_session_options_emails" )["header_background_screen_color"]        ? get_option( "book_a_session_options_emails" )["header_background_screen_color"]        : $default_header_background_screen_color;
                
                if ( $header_background_image_option === "service_image" && $service_image_url ) {

                    $header_background_image_url = $service_image_url;

                } elseif ( $header_background_image_option === "new_image" && $new_image_url ) {

                    $header_background_image_url = $new_image_url;

                } else {

                    $header_background_image_url = false;

                }

                $body .= "<table style='background-color:       $header_background_color;
                                        border-spacing:         0;
                                        border-collapse:        collapse;
                                        background-position:    center center;'";
                
                if ( ! empty( $header_background_image_url ) ) { $body .= " background='" . $header_background_image_url . "'"; } 
                
                $body .= " width='100%' height='auto'>";

                $body .= "<tr style='   background-color:       $header_background_screen_color;";
                
                if ( $header_background_gradient ) {

                    $body .= "
                                        background:             -moz-linear-gradient(left,$header_background_gradient_left 0%,$header_background_gradient_right 100%);
                                        background:             -webkit-linear-gradient(left,$header_background_gradient_left 0%,$header_background_gradient_right 100%);
                                        background:             linear-gradient(to right,$header_background_gradient_left 0%,$header_background_gradient_right 100%);";

                }       

                $body .= "'>";

                // Header Container

                $default_max_width = book_a_session_options_default( "emails", "max_width" );

                $max_width = get_option( "book_a_session_options" )["email_max_width"] ? get_option( "book_a_session_options" )["email_max_width"] . "px" : $default_max_width . "px";
                
                $body .= "<td style='   max-width:              $max_width;
                                        width:                  100%;
                                        margin:                 0 auto;'>";
            
                // Primary message

                $body .= "<h2 style='   text-align:             center;
                                        padding:                100px 5%;
                                        width:                  90%;
                                        color:                  #ffffff;
                                        font-weight:            normal;
                                        line-height:            1.2;'>";

                $body .= "<span style=' font-size:              36px;
                                        font-weight:            bold;
                                        line-height:            1;
                                        display:                inline-block;
                                        margin-bottom:          25px;'>$header_large_text</span><br>";


                $body .= $header_subtitle . "</h2>";                        

                // End Header Wrapper and Header Container

                $body .= "</td>";
                $body .= "</tr>";
                $body .= "</table>";

                // Table Wrapper
                
                $body .= "<div style='  width:                  90%;
                                        height:                 100%;
                                        background-color:       #ececec;
                                        padding:                5%;'>";

                // Table Container                        
                
                $body .= "<div style='  max-width:              $max_width;
                                        width:                  90%;
                                        margin:                 0 auto;
                                        position:               relative;
                                        background-color:       #ffffff;
                                        padding:                5%;
                                        border-radius:          4px;
                                        position:               relative;'>";

                // Info Circle
                /*                       
                $body .= "<div style='
                color:                  #ffffff;
                border-radius:          100%;
                width:                  50px;
                height:                 50px;
                line-height:            50px;
                font-size:              30px;
                font-weight:            bold;
                font-family:            serif;
                font-style:             italic;
                text-align:             center;
                margin:                 -5% calc( 50% - 25px ) 5% calc( 50% - 25px );
                display:                inline-block;
                position:               relative;
                background:             #30b6fe;
                background:             -moz-linear-gradient(left, rgba(106,10,206,1) 0%, rgba(48,182,254,1) 100%);
                background:             -webkit-linear-gradient(left, rgba(106,10,206,1) 0%,rgba(48,182,254,1) 100%);
                background:             linear-gradient(to right, rgba(106,10,206,1) 0%,rgba(48,182,254,1) 100%);
                '>i</div>";
                */
                
                $body .= "<table style='width:                  100%;
                                        margin-bottom:          7.5%;
                                        padding-bottom:         25px;
                                        border-bottom:          1px solid #dadada;'><tbody>";
                $body .= "<tr>";
                $body .= "<td style='text-align: left;'><strong>Order ID:</strong><br>" . $order_id;
                $body .= "<br>";
                $body .= "<strong>" . $booking_status . ":</strong><br>" . $payment_status;
                $body .= "</td>";
                $body .= "<td style='text-align: right;'><strong>Issued:</strong><br>" . date( get_option( "date_format" ), strtotime( $issue_date ) );
                $body .= "<br>";
                $body .= "<strong>Due by:</strong><br>" . date( "h:i A", strtotime( $due_by_date ) ) . " " . date( get_option( "date_format" ), strtotime( $due_by_date ) );
                $body .= "</td>";
                $body .= "</tr>";
                $body .= "</tbody></table>";

                // Large Price Tag

                $body .= $online_due || $cash_due || $vfc_due ? "<h1 style='text-align: center; font-size: 3em; font-weight: bold; margin-bottom: 25px;'>" . book_a_session_get_price_tag( $invoice->grand_total_due, $invoice->currency_id ) . "</h1>" : "";

                // VFC Logo

                if ( $vfc_due || $vfc_partly_paid ) {

                    if ( $vfc_logo ) {

                        $body .= "<img style='
                                        width:                  100%;
                                        max-width:              200px;
                                        height:                 auto;
                                        display:                block;
                                        margin:                 1.5% auto 7.5% auto;
                                        position:               relative;
                                        ' ";
                        $body .= "src='" . $vfc_logo_url . "' alt='Vodafone Cash Logo'>";

                    }

                // Cash Logo
                
                } elseif ( $cash_due || $cash_partly_paid ) {

                    if ( $cash_logo ) {

                        $body .= "<img style='
                                        width:                  100%;
                                        max-width:              100px;
                                        height:                 auto;
                                        display:                block;
                                        margin:                 1.5% auto 3% auto;
                                        position:               relative;
                                        ' ";
                        $body .= "src='" . $cash_logo_url . "' alt='Cash Logo'>";

                    }

                // Online Logo

                } elseif ( $online_due || $online_partly_paid ) {

                    if ( $paypal_logo && $payment_method_id == 1 ) {

                        $body .= "<img style='
                                        width:                  100%;
                                        max-width:              125px;
                                        height:                 auto;
                                        display:                block;
                                        margin:                 1.5% auto 3% auto;
                                        position:               relative;
                                        ' ";
                        $body .= "src='" . $paypal_logo_url . "' alt='PayPal Logo'>";

                    } elseif ( $stripe_logo && $payment_method_id == 4 ) {

                        $body .= "<img style='
                                        width:                  100%;
                                        max-width:              150px;
                                        height:                 auto;
                                        display:                block;
                                        margin:                 1.5% auto 3% auto;
                                        position:               relative;
                                        ' ";
                        $body .= "src='" . $stripe_logo_url . "' alt='Stripe Logo'>";

                    }

                }

                // Payment instruction

                $body .= "<h3 style='text-align: center;'>" . $table_title . "</h3>";
                if ( $payment ) $body .= "<h3 style='text-align: center; font-weight: normal; font-size: 1em; opacity: .75;'>You paid " . book_a_session_get_price_tag( $payment->total, $invoice->currency_id ) . " via " . $payment_method_name . ".<br>" . $payment_method_name . " Payment ID: " . $payment->payment_id . ".</h3>";
                $body .= "<h3 style='text-align: center; font-weight: normal; font-size: 1.15em;'>" . $table_paragraph . "</h3>";
                $body .= ! $next_session && ! empty( $completed ) ? "<p style='text-align: center;'>" . $completed . "</p>" : "";

                // Itemised table

                $body .= "<table style='
                                        width:                  100%;
                                        padding-top:            7.5%; 
                                        margin-top:             7.5%; 
                                        border-top:             1px solid #dadada;'><tbody>";
                $body .= "<tr>";
                $body .= "<th scope='col' style='width: 120px; text-align: left;'>Date</th>";
                $body .= "<th scope='col' style='width: 120px; text-align: left;'>Time</th>";
                $body .= "<th scope='col' style='text-align: right;'>Price</th>";
                $body .= "</tr>";

                foreach ( $session_date_and_time as $invoice_datetime ) {

                    $body .= "<tr>";
                    $body .= "<td>" . $invoice_datetime["date"]        . "</td>";
                    $body .= "<td>" . $invoice_datetime["time"] . "</td>";
                    $body .= "<td style='text-align: right'>";
                    $body .= number_format( $invoice->base_price / $quantity, 2);
                    $body .= "</td>";
                    $body .= "</tr>"; 

                }
                
                $body .= "</tbody>";
                $body .= "</table>";

                // Total table

                $body .= "<table style='width:                  100%; 
                                        padding-top:            7.5%; 
                                        margin-top:             7.5%; 
                                        border-top:         1px solid #dadada;'><tbody>";
                $body .= "<tr><td style='width: 50%;'></td><td style='width: 25%; min-width: 100px; text-align: left; color: #909090;'>";
                $body .= "Location";
                $body .= "</td>";
                $body .= "<td style='width: 25%; min-width: 100px; text-align: right;'>";
                $body .= number_format( $invoice->location_charge, 2);
                $body .= "</td></tr>";
                $body .= "<tr><td style='width: 50%;'></td><td style='width: 25%; min-width: 100px; text-align: left; color: #909090;'>";
                $body .= "Discount";
                $body .= "</td>";
                $body .= "<td style='width: 25%; min-width: 100px; text-align: right;'>-";
                $body .= number_format( $invoice->discount, 2);
                $body .= "</td></tr>";
                $body .= "<tr><td style='width: 50%;'></td><td style='width: 25%; min-width: 100px; text-align: left; color: #909090;'>";
                $body .= "Total";
                $body .= "</td>";
                $body .= "<td style='width: 25%; min-width: 100px; text-align: right;'>";
                $body .= number_format( $invoice->grand_total, 2);
                $body .= "</td></tr>";
                $body .= "<tr><td style='width: 50%;'></td><td style='width: 25%; min-width: 100px; text-align: left; color: #909090;'>";
                $body .= "Paid";
                $body .= "</td>";
                $body .= "<td style='width: 25%; min-width: 100px; text-align: right;'>";
                $body .= isset( $invoice->amount_paid ) ? "-" . number_format( $invoice->amount_paid, 2) : "&mdash;";
                $body .= "</td></tr>";
                $body .= "<tr><td style='width: 50%;'></td><td style='width: 25%; min-width: 100px; text-align: left; font-weight:bold; font-size: 16px; line-height: 3;'>";
                $body .= "Total Due";
                $body .= "</td>";
                $body .= "<td style='width: 25%; min-width: 100px; text-align: right; font-weight:bold; font-size: 16px; line-height: 3;'>";
                $body .= book_a_session_get_price_tag( $invoice->grand_total_due, $invoice->currency_id );
                $body .= "</td></tr>";
                $body .= "</tbody>";
                $body .= "</table>";
                $body .= "";
                $body .= "</div>";
                $body .= "</div>";
                $body .= "";

                if ( ! $preview ) {

                    $result_array = array();

                    // Send to client
    
                    if ( ! empty( $recipients["client"] ) ) {
    
                        $to_client = get_userdata( $client_id )->user_email;
    
                        $client_mail_result = wp_mail( $to_client, $subject, $body, $headers );
    
                        $result_array[] = $client_mail_result ? $to_client : false;
    
                    }
    
                    // Send to practitioner
    
                    if ( ! empty( $recipients["practitioner"] ) ) {
    
                        $to_practitioner = get_userdata( $practitioner_id )->user_email;
    
                        $practitioner_mail_result = wp_mail( $to_practitioner, $subject, $body, $headers );
    
                        $result_array[] = $practitioner_mail_result ? $to_practitioner : false;
    
                    }
    
                    // Send to current user
    
                    if ( ! empty( $recipients["self"] ) ) {
    
                        $to_self = get_userdata( get_current_user_id() )->user_email;
    
                        $self_mail_result = wp_mail( $to_self, $subject, $body, $headers );
    
                        $result_array[] = $self_mail_result ? $to_self : false;
    
                    }

                    /*
    
                    $to_outlook = "ramiabdelal@outlook.com";
    
                    $outlook_mail_result = wp_mail( $to_outlook, $subject, $body, $headers );
    
                    $result_array[] = $outlook_mail_result ? $to_outlook : false;
    
                    $to_rgb = "r.abdelal@rgbwebsites.co.uk";
    
                    $rgb_mail_result = wp_mail( $to_rgb, $subject, $body, $headers );
    
                    $result_array[] = $rgb_mail_result ? $to_rgb : false;
    
                    $to_ara = "ahmed.rami.abdelal@gmail.com";
    
                    $ara_mail_result = wp_mail( $to_ara, $subject, $body, $headers );
    
                    $result_array[] = $ara_mail_result ? $to_ara : false;

                    $to_diana = "dianaprmanima@outlook.com";
    
                    $diana_mail_result = wp_mail( $to_diana, $subject, $body, $headers );
    
                    $result_array[] = $diana_mail_result ? $to_diana : false;

                    */
    
                    return $result_array;

                } else {

                    return $body;

                }

            } else {

                return false;

            }

        } else {

            return false;

        }

    } else {

        return false;

    }


    
    

}
