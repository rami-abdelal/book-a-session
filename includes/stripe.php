<?php
/**
 * Book A Session - Stripe API Functionality.
 * 
 * Here you will find all Stripe related functions. The code here uses the Stripe library found in the same folder e.g. includes/stripe-php-6.12.0/
 * 
 */

if ( ! defined( 'ABSPATH' ) ) exit;

// Check if Stripe has been enabled in admin settings

$stripe_accepted = isset( get_option( "book_a_session_options_paymentmethods" )["accept4"] ) ? get_option( "book_a_session_options_paymentmethods" )["accept4"] : book_a_session_options_default( "paymentmethods", "accept4" );

if ( $stripe_accepted ) {

    // Load the Stripe API Library

    require_once plugin_dir_path( __FILE__ ) . 'stripe-php-6.12.0/init.php';

    // Check whether it's a test or live environment

    $test_live = isset( get_option( "book_a_session_options_paymentmethods" )["test_live_4"] ) ? get_option( "book_a_session_options_paymentmethods" )["test_live_4"] : book_a_session_options_default( "paymentmethods", "test_live_4" );

    // Get the API keys according to whether it's a test or live environment from admin settings entered by the plugin user

    $publishable_key =  $test_live === "test" ? get_option( "book_a_session_options_paymentmethods" )["test_pub_key_4"] : get_option( "book_a_session_options_paymentmethods" )["live_pub_key_4"];
    $secret_key =       $test_live === "test" ? get_option( "book_a_session_options_paymentmethods" )["test_sec_key_4"] : get_option( "book_a_session_options_paymentmethods" )["live_sec_key_4"];

    $stripe = array(

        "publishable_key" => $publishable_key,
        "secret_key"      => $secret_key,

    );

    // Set the secret key to the Stripe class made available by the Stripe library we loaded above

    \Stripe\Stripe::setApiKey( $stripe[ "secret_key" ] );
    
    // Create function that charges with Stripe, to be triggered via a custom POST endpoint in the WordPress REST API by client side AJAX on the frontend form

    function book_a_session_charge_stripe( $data ) {

        // Get the posted data together

        $token      = $data['stripeToken'];
        $email      = $data['stripeEmail'];
        $amount     = $data["total"];
        $currency   = $data["currency_code"];

        // Create a Stripe Customer

        $customer   = \Stripe\Customer::create(

            array(

                'email'     => $email,
                'source'    => $token,
            
            )
            
        );

        // Create a Stripe Charge using the Customer ID and posted data. The Stripe library takes care of the rest.

        $charge = \Stripe\Charge::create(

            array(

                'customer'  => $customer->id,
                'amount'    => $amount,
                'currency'  => $currency,

            )

        );

        if ( $charge ) {

            // Respond with information to use for success or error handling on the client side
            
            return rest_ensure_response( 
                
                array(

                    "customer"  => $customer, 
                    "charge"    => $charge,

                )
            
            );

        } else {

            return rest_ensure_response( new WP_Error( __( "Stripe Charge Failed", "book_a_session" ) ) );

        }

    }

    // Charge Stripe WP REST API POST Endpoint

    add_action( 'rest_api_init', function () {

        register_rest_route( 'book-a-session/v1', '/payment/stripe', array(

            'methods'               => 'POST',
            'callback'              => 'book_a_session_charge_stripe',

        ) );

    } );

}

