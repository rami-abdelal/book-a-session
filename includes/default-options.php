<?php
/**
 * Book A Session - Default options can be found here.
 * 
 */

if ( ! defined( 'ABSPATH' ) ) exit;

function book_a_session_options_default( $group, $id = false ) {

	$default_array = array(

		'general' => array(

			'logo_url' => "",
            'dashboard_private_locations' => true,
            'savings_tag' => true,
            'practitioner_title' => true,
            'quantity_top' => __( "How many sessions are you booking?", "book_a_session" ),
            'quantity_bottom' => __( "Ordering multiple sessions requires payment in advance.", "book_a_session" ),
            'location_top' => __( "Choose a location", "book_a_session" ),
            'location_bottom' => __( "Any extra charges for a chosen location apply to every session in your order.", "book_a_session" ),
            'practitioner_top' => __( "Choose a practitioner", "book_a_session" ),
            'practitioner_bottom' => __( "You can message your practitioner once your order has been placed.", "book_a_session" ),            
            'payment_method_top' => __( "Choose a payment method", "book_a_session" ),
            'payment_method_bottom' => __( "All payments are made safely and securely, without ever storing any card details with us. You'll receive a confirmation or invoice email once you place your order.", "book_a_session" ),
            
		),

		'practitioners' => array(

		),

		'currencies' => array(

		),

		'schedule' => array(

		),

		'regions' => array(

		),

		'paymentmethods' => array(

			'accept1' 		        => true,
			'accept2' 		        => true,
			'accept3' 		        => true,
            'accept4' 		        => true,	
            'frontend_icon_1'       => true,
			'frontend_icon_2'       => true,
			'frontend_icon_3'       => true,
			'frontend_icon_4'       => true,		
            'frontend_title_1'      => __( "PayPal", "book_a_session" ),
			'frontend_title_2'      => __( "Cash", "book_a_session" ),
			'frontend_title_3'      => "",
            'frontend_title_4'      => "",            
            'frontend_subtitle_1'   => __( "Pay online with PayPal", "book_a_session" ),
			'frontend_subtitle_2'   => __( "Pay on the day", "book_a_session" ),
			'frontend_subtitle_3'   => __( "Book now, pay later", "book_a_session" ),
			'frontend_subtitle_4'   => __( "Credit / Debit Card", "book_a_session" ),
            'frontend_tag_1'        => __( "Instant", "book_a_session" ),
			'frontend_tag_2'        => "",
			'frontend_tag_3'        => "",
            'frontend_tag_4'        => __( "Instant", "book_a_session" ),
            'frontend_top_1'        => __( "Review your order and pay with PayPal to complete your booking instantly." ),
			'frontend_top_2'        => __( "Review and book your order below to pay with cash in person on the day of your session.", "book_a_session" ),
			'frontend_top_3'        => __( "Enter the mobile number you'll be sending your Vodafone Cash payment from and press the button below to place your order.", "book_a_session" ),
            'frontend_top_4'        => __( "Review your order and pay with your credit / debit card to complete your booking instantly." ),            
            'frontend_bottom_1'     => __( "Once your payment has been verified, you'll be returned here to check your dashboard, review your order, and send and receive messages. Thanks!", "book_a_session" ),
			'frontend_bottom_2'     => __( "Once you're finished, your session will be booked. You can then go to your dashboard to review your order and send and receive messages. Your payment will be due in person on the day of your session. Thanks!", "book_a_session" ),
			'frontend_bottom_3'     => __( "Once you place your order, you just need to send the payment to the number we'll send you. You'll also receive an invoice with further instructions on how to make your payment. Be sure to check your dashboard to see your order there. Thanks!", "book_a_session" ),
            'frontend_bottom_4'     => __( "Once your payment has been verified, you'll be returned here to check your dashboard and review your order. Thanks!", "book_a_session" ),            
            'online_payable'        => "checkout",
            'sandbox_live_1'        => "sandbox",
            'sandbox_id_1'          => "",
            'live_id_1'             => "",
            'sandbox_secret_1'      => "",
            'live_secret_1'         => "",
            'test_live_4'           => "test",
            "test_pub_key_4"        => "",
            "test_sec_key_4"        => "",
            "live_pub_key_4"        => "",
            "live_sec_key_4"        => "",

		),

		'locations' => array(

		),

		'servicetypes' => array(

		),

		'services' => array(

		),

		'bundles' => array(

		),

		'orders' => array(

		),

		'timetables' => array(
			'tooltip_max' => 99,
			'result_limit' => 30,
			'week_load' => 12,
			'month_load' => 12,
		),

		'emails' => array(

			'from_email_address' 				=> "noreply@" . book_a_session_get_top_level_url(),
			'from_name' 						=> get_bloginfo( 'name' ),
			'max_width' 						=> 650,
			'logo_background_color' 			=> "#fafafa",
			'header_background_color' 			=> "#424242",
			'header_background_image' 			=> "service_image",
			'new_image_url' 					=> "",
			'header_background_screen_color' 	=> "rgba(66,66,66,.6)",
			'header_background_gradient' 		=> "false",
			'header_background_gradient_left' 	=> "rgba(106,10,206,.6)",
			'header_background_gradient_right'	=> "rgba(48,182,254,.6)",

			'phpmailer_enabled'					=> false,
			'phpmailer_from_name'				=> get_bloginfo( 'name' ),
			'phpmailer_from_email_address'		=> get_option( "admin_email" ),
			'phpmailer_host'					=> "",
			'phpmailer_smtp_port'				=> 465,
			'phpmailer_username'				=> "",
			'phpmailer_password'				=> "",
			'phpmailer_smtpsecure'				=> "ssl",


		),

		'invoices' => array(

			'due_date_offset_measurement' 				=> "hours",
			'due_date_offset_value' 					=> 24,
			'due_date_offset_anchor' 					=> "session_date",
			'vfc_receiving_mobile_number' 				=> '',

			'paypal_logo'								=> true,
			'stripe_logo'								=> true,
			'email_subject_online_due' 					=> "Payment due",
			'reminder_email_subject_online_due' 		=> "(Reminder) Payment due soon",
			'hidden_preheader_online_due' 				=> "Your order for %quantity% %session(s)% of %service% - %location% is almost finalised, we\'re just waiting for your payment. Please pay by %duebytime% on %duebydate%.",
			'header_large_text_online_due' 				=> "You're almost there, %clientfname%!",
			'header_subtitle_text_online_due' 			=> "Your order for %quantity% %session(s)% of %service% - %location% is pending.",
			'table_title_online_due' 					=> "Please pay %totaldue% by %paymentmethod% to complete your order.",
			'table_paragraph_online_due' 				=> "Your payment is due by %duebytime% on %duebydate%.",

			'email_subject_online_partly_paid' 			=> "Part payment due",
			'reminder_email_subject_online_partly_paid' => "(Reminder) Part payment due by %duebytime% on %duebydate%",
			'hidden_preheader_online_partly_paid' 		=> "Thanks for your earlier payment, however, your order for %quantity% %session(s)% of %service% - %location% is still pending. Please pay the remaining %totaldue% by %duebytime% on %duebydate%.",
			'header_large_text_online_partly_paid' 		=> "There's still a bit to go, %clientfname%!",
			'header_subtitle_text_online_partly_paid' 	=> "Thanks for your previous payment, however, your order for %quantity% %session(s)% of %service% - %location% is still pending.",
			'table_title_online_partly_paid' 			=> "Please pay %totaldue% to complete your order.",
			'table_paragraph_online_partly_paid' 		=> "Your payment is due by %duebytime% on %duebydate%.",

			'email_subject_online_paid' 				=> "Your " . get_bloginfo( 'name' ) . " order has been paid for.",
			'reminder_email_subject_online_paid' 		=> "Confirmation for your order on " . get_bloginfo( 'name' ),
			'hidden_preheader_online_paid' 				=> "Thanks for your payment for %quantity% %session(s)% of %service% - %location%. Please find details within.",
			'header_large_text_online_paid' 			=> "Payment received!",
			'header_subtitle_text_online_paid' 			=> "Your order for %quantity% %session(s)% of %service% - %location% has been booked and fully paid for.",
			'table_title_online_paid' 					=> "Thanks!",
			'table_paragraph_online_paid' 				=> "Your %first/next/last% session %is/was% for %fnltime% on %fnldate% with your %practitle%, %pracfname% %praclname%.",
			'completed_online_paid' 					=> "We hope you enjoyed your %session(s)%.",

			'email_subject_cash_due' 					=> "Payment due",
			'reminder_email_subject_cash_due' 			=> "(Reminder) Payment due soon",
			'hidden_preheader_cash_due' 				=> "Your order for %quantity% %session(s)% of %service% - %location% has been booked. You'll need to pay in cash on first meeting for %duebytime% on %duebydate%.",
			'header_large_text_cash_due' 				=> "Booked!",
			'header_subtitle_text_cash_due' 			=> "Your order for %quantity% %session(s)% of %service% - %location% has been confirmed.",
			'table_title_cash_due' 						=> "Please pay %totaldue% by cash.",
			'table_paragraph_cash_due' 					=> "Please make your payment in person on first meeting for %duebytime% on %duebydate%.",

			'email_subject_cash_partly_paid' 			=> "Part payment due",
			'reminder_email_subject_cash_partly_paid' 	=> "(Reminder) Part payment due by %duebytime% on %duebydate%",
			'hidden_preheader_cash_partly_paid' 		=> "Thanks for your earlier payment towards your order for %quantity% %session(s)% of %service% - %location%. Please pay the remaining %totaldue% on next meeting.",
			'header_large_text_cash_partly_paid' 		=> "There's still a bit to go, %clientfname%!",
			'header_subtitle_text_cash_partly_paid' 	=> "Thanks for your earlier payment towards your order for %quantity% %session(s)% of %service% - %location%. Please pay the remaining %totaldue% on next meeting.",
			'table_title_cash_partly_paid' 				=> "Please pay %totaldue% by cash on next meeting",
			'table_paragraph_cash_partly_paid' 			=> "Your %first/next/last% session %is/was% for %fnltime% on %fnldate% with your %practitle%, %pracfname% %praclname%.",
			
			'cash_logo'									=> false,
			'email_subject_cash_paid' 					=> "Your " . get_bloginfo( 'name' ) . " order has been paid for.",
			'reminder_email_subject_cash_paid' 			=> "Here's your cleared invoice",
			'hidden_preheader_cash_paid' 				=> "Thanks for your payment for %quantity% %session(s)% of %service% - %location%. Please find your invoice within.",
			'header_large_text_cash_paid' 				=> "Payment received!",
			'header_subtitle_text_cash_paid' 			=> "Your order for %quantity% %session(s)% of %service% - %location% has been booked and fully paid for.",
			'table_title_cash_paid' 					=> "Thanks!",
			'table_paragraph_cash_paid' 				=> "Your %first/next/last% session %is/was% for %fnltime% on %fnldate% with your %practitle%, %pracfname% %praclname%.",
			'completed_cash_paid' 						=> "We hope you enjoyed your %session(s)%.",

			'vfc_logo'									=> true,
			'email_subject_vfc_due' 					=> "Payment due",
			'reminder_email_subject_vfc_due' 			=> "(Reminder) Payment due soon",
			'hidden_preheader_vfc_due' 					=> "Your order for %quantity% %session(s)% of %service% - %location% is almost finalised, we\'re just waiting for your payment. Please pay by %duebytime% on %duebydate%.",
			'header_large_text_vfc_due' 				=> "You're almost there, %clientfname%!",
			'header_subtitle_text_vfc_due' 				=> "Your order for %quantity% %session(s)% of %service% - %location% is pending.",
			'table_title_vfc_due' 						=> "Please pay %totaldue% by %paymentmethod% to complete your order.",
			'table_paragraph_vfc_due' 					=> "Please send your payment to %vfcreceiver% by %duebytime% on %duebydate%.",

			'email_subject_vfc_partly_paid' 			=> "Part payment due",
			'reminder_email_subject_vfc_partly_paid'	=> "(Reminder) Part payment due by %duebytime% on %duebydate%",
			'hidden_preheader_vfc_partly_paid' 			=> "Thanks for your earlier payment, however, your order for %quantity% %session(s)% of %service% - %location% is still pending. Please pay the remaining %totaldue% by %duebytime% on %duebydate%.",
			'header_large_text_vfc_partly_paid' 		=> "There's still a bit to go, %clientfname%!",
			'header_subtitle_text_vfc_partly_paid'		=> "Thanks for your previous payment, however, your order for %quantity% %session(s)% of %service% - %location% is still pending.",
			'table_title_vfc_partly_paid' 				=> "Please pay %totaldue% by %paymentmethod% to complete your order.",
			'table_paragraph_vfc_partly_paid' 			=> "Please send your payment to %vfcreceiver% by %duebytime% on %duebydate%.",

			'email_subject_vfc_paid' 					=> "Your " . get_bloginfo( 'name' ) . " order has been paid for.",
			'reminder_email_subject_vfc_paid' 			=> "Confirmation for your order on " . get_bloginfo( 'name' ),
			'hidden_preheader_vfc_paid' 				=> "Thanks for your payment for %quantity% %session(s)% of %service% - %location%. Please find details within.",
			'header_large_text_vfc_paid' 				=> "Payment received!",
			'header_subtitle_text_vfc_paid' 			=> "Your order for %quantity% %session(s)% of %service% - %location% has been booked and fully paid for.",
			'table_title_vfc_paid' 						=> "Thanks!",
			'table_paragraph_vfc_paid' 					=> "Your %first/next/last% session %is/was% for %fnltime% on %fnldate% with your %practitle%, %pracfname% %praclname%.",
			'completed_vfc_paid' 						=> "We hope you enjoyed your %session(s)%.",			

		),

		'payments' => array(

		),

		'receipts' => array(

		),

	);

	$default = $default_array[$group][$id];

	if ( $group === "all" ) {

		return $default_array;

	} elseif ( $default ) {

		return $default;

	} else {

		return false;

	}

}
