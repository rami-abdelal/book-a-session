<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Renders a dashboard for clients or practitioners.
 * 
 * @param   int     $user_id    Required integer used to find the user to display a dashboard for.
 * @return  null    void        No return value exactly, simply spits out the correct markup.
 * 
 */

function book_a_session_dashboard() {

    $password_match_error = false;
    $resetting_password = false;
    $password_reset_array = array();

    // If they're following a password reset verification link, try to log them in 

    if ( isset( $_GET["reset"] ) && isset( $_GET["key"] ) && isset( $_GET["login"] ) ) {

        // Set variables

        $password_updated = false;
        $resetting_password = true;
        $password_reset_array = array(

            'key' => rawurldecode( $_GET["key"] ),
            'login' => rawurldecode( $_GET["login"] ),
    
        );

        // Check the key against the login

        $check_reset_key = check_password_reset_key( $password_reset_array["key"], $password_reset_array["login"] );

        if ( ! is_wp_error( $check_reset_key ) ) {

            // If the key matches, log them in. Then they can use their dashboard settings form to change their password.

            $user_id = get_user_by( "login", $password_reset_array["login"] )->ID;
                        
            if ( intval( $user_id > 0 ) ) {

                wp_set_current_user( $user_id, $password_reset_array["login"] );
                wp_set_auth_cookie( $user_id, false, is_ssl(), false );
                do_action( "wp_login", $password_reset_array["login"] );

            }

        }

    }

    // Get userdata

    $user_id = get_current_user_id();
    $user = get_userdata( $user_id );

    if ( $user ) {

        // Find out if user is a practitioner or not

        $isprac = in_array( "admin_practitioner", $user->roles, true ) || in_array( "practitioner", $user->roles, true ) ? true : false;

        if ( ! $isprac ) {

            // Client settings post

            if ( ! empty( $_POST['submit_settings'] ) ) {

                // Security check

                if ( get_current_user_id() === $user_id && wp_verify_nonce( $_REQUEST['_wpnonce'], "user_" . $user_id . "_update_settings" ) || 
                     current_user_can( "edit_users" )   && wp_verify_nonce( $_REQUEST['_wpnonce'], "user_" . $user_id . "_update_settings" ) ){

                    // Required values check

                    if ( ! empty( $_POST['first_name'] )    && 
                         ! empty( $_POST['last_name'] )     &&
                         ! empty( $_POST['email'] )         && 
                         ! empty( $_POST['region_id'] )     ){

                        // Prepare updated user values

                        if ( ! empty( $_POST['book_a_session_settings_password'] ) && ! empty( $_POST["book_a_session_settings_confirm_password"] ) ) {

                            if ( $_POST["book_a_session_settings_password"] === $_POST["book_a_session_settings_confirm_password"] ) {
                
                                $user_updated_array = array(

                                    "ID"            => $user_id,
                                    "first_name"    => $_POST['first_name'],
                                    "last_name"     => $_POST['last_name'],
                                    "user_email"    => $_POST['email'],
                                    "user_pass"     => $_POST["book_a_session_settings_password"],

                                );  

                                $password_updated = true;

                            } else {

                                $user_updated_array = array(

                                    "ID"            => $user_id,
                                    "first_name"    => $_POST['first_name'],
                                    "last_name"     => $_POST['last_name'],
                                    "user_email"    => $_POST['email']
        
                                );    
                                
                                $password_match_error = "Passwords do not match.";

                            }

                        } else {

                            $user_updated_array = array(

                                "ID"            => $user_id,
                                "first_name"    => $_POST['first_name'],
                                "last_name"     => $_POST['last_name'],
                                "user_email"    => $_POST['email']
    
                            ); 

                        }

                        // Update user

                        $update_user = wp_update_user( $user_updated_array );

                        // Update Region ID user meta
                        
                        $prev_region = ! empty ( intval( get_user_meta( $user_id, "region_id", true ) ) ) ? intval( get_user_meta( $user_id, "region_id", true ) ) : null;

                        if ( intval( $_POST['region_id'] ) !== $prev_region ) $update_region = update_user_meta( $user_id, "region_id", intval( $_POST['region_id'] ), $prev_region );

                        // Update Phone user meta

                        if ( ! empty( $_POST['phone'] ) ) $update_phone = update_user_meta( $user_id, "phone", $_POST['phone'] );

                    } else $required_error = true;

                }    

            }

            $simple_upcoming = book_a_session_timetable( "client_" . $user_id, "simple_upcoming", "ASC", 10, "region", get_user_meta( $user_id, "region_id", true ) );

            // Client dashboard

            $private_location = isset( get_option( "book_a_session_options_general" )["dashboard_private_locations"] ) ? get_option( "book_a_session_options_general" )["dashboard_private_locations"] : book_a_session_options_default( "general", "dashboard_private_locations" );

            ?>

                <div class='book-a-session-dashboard book-a-session-card book-a-session-animated'>
                    <div class='book-a-session-sidebar'>
                        <nav class='book-a-session-navigation'>
                            <ul>
                            <li><a data-content='book-a-session-dashboard-summary' class='book-a-session-animated book-a-session-current' href='javascript:void(0)'>Summary</a></li>
                            <li><a data-content='book-a-session-dashboard-orders' class='book-a-session-animated' href='javascript:void(0)'>Orders</a></li>
                            <li><a data-content='book-a-session-dashboard-upcoming-sessions' class='book-a-session-animated' href='javascript:void(0)'>Upcoming Sessions</a></li>
                            <li><a data-content='book-a-session-dashboard-session-history' class='book-a-session-animated' href='javascript:void(0)'>Session History</a></li>                      
                            <li><a data-content='book-a-session-dashboard-settings' class='book-a-session-animated ' href='javascript:void(0)'>Settings</a></li>
                            <li><a data-content='book-a-session-dashboard-invoices' class='book-a-session-animated' href='javascript:void(0)'>Invoices</a></li>
                            <!-- Coming soon <li><a data-content='book-a-session-dashboard-email-preferences' class='book-a-session-animated' href='javascript:void(0)'>Email Preferences</a></li>-->
                            <?php if ( $private_location ) { ?> <li><a data-content='book-a-session-dashboard-private-locations' class='book-a-session-animated' href='javascript:void(0)'>Private Locations</a></li> <?php } ?>
                            <li><a data-content='book-a-session-dashboard-logout' class='book-a-session-animated' href='javascript:void(0)'>Log Out</a></li>
                            </ul>
                        </nav>
                    </div>

                    <div class='book-a-session-content book-a-session-animated book-a-session-dashboard-summary'>
                        <h2>Welcome back, <?php echo $user->first_name . " " .  $user->last_name; ?>.</h2>
                        <div class='book-a-session-upcoming-sessions-widget book-a-session-widget'>
                            <h3>Upcoming Sessions</h3>
                            <?php echo book_a_session_timetable( "client_" . $user_id, "calendar", false, false, false, get_user_meta( $user_id, "region_id", true ) ); ?>
                        </div>

                        <div class='book-a-session-session-history-widget book-a-session-widget'>
                            <h3>Session History</h3>
                            <?php echo book_a_session_timetable( "client_" . $user_id, "simple_history", "DESC", 4, "region", get_user_meta( $user_id, "region_id", true ) ); ?>
                        </div>
                    </div>
                    <div class='book-a-session-content book-a-session-animated book-a-session-dashboard-orders book-a-session-hidden'>
                        <h2>Orders</h2>

                        <?php 

                        $order_count = book_a_session_count_table( "order", array( "user_id", "=", $user_id ) );

                        $order_array = book_a_session_get_table_array( "order", "created", "DESC", "*", array( "user_id", "=", $user_id ), 10 );

                        if ( ! empty( $order_array ) ) {

                            echo "<table class='book-a-session-table'><thead><th>Date</th><th>Order ID</th><th>Status</th><th>Total</th><th>Due</th><th>Order</th></thead><tbody>";

                            foreach ( $order_array as $order ) :

                                $session_array = book_a_session_get_table_array( "session", "start_datetime", "ASC", "*", array( "order_id", "=", $order->order_id ) );
                                $project_array = book_a_session_get_table_array( "project", false, false, "*", array( "order_id", "=", $order->order_id ) );
                                $order_cell = "";

                                if ( $order->session && ! empty( $session_array ) ) :

                                    $order_cell = count( $session_array ) > 1 ? count( $session_array ) . " Sessions" : "1 Session";
                                    $order_attribute = "data-session-order='" . $order->order_id . "'";

                                elseif ( $order->project && ! empty( $project_array ) ) :

                                    $order_cell = "Project";
                                    $order_attribute = "data-project-order='" . $order->order_id . "'";


                                endif;
                                
                                // Created

                                echo "<tr><td>" . date( get_option( "date_format" ), strtotime( $order->created ) ) . "</td>"; 

                                // Order ID

                                echo "<td>" . $order->order_id . "</td>";

                                // Status

                                echo "<td>" . $order->booking_status . "</td>";

                                // Total

                                echo "<td>" . book_a_session_get_price_tag( ( $order->total ), $order->currency_id ) . "</td>";

                                // Due

                                echo "<td>" . book_a_session_get_price_tag( ( $order->total_due ), $order->currency_id ) . "</td>";

                                // Order

                                echo "<td><a href='javascript:void(0)' $order_attribute class='book-a-session-button book-a-session-animated book-a-session-dark-button book-a-session-mini-button book-a-session-button-icon book-a-session-button-icon-right book-a-session-view-order'>" . $order_cell . "<span class='book-a-session-icon'>❯</span></a></td></tr>";



                            endforeach;

                            echo "</tbody></table>";

                            echo "<div class='book-a-session-button-row book-a-session-hidden'><a href='javascript:void(0)' class='book-a-session-animated book-a-session-button book-a-session-dark-button book-a-session-button-icon book-a-session-previous-orders'><span class='book-a-session-icon book-a-session-icon-chevron-left'></span></a><a href='javascript:void(0)' class='book-a-session-animated book-a-session-button book-a-session-dark-button book-a-session-button-icon book-a-session-next-orders'><span class='book-a-session-icon book-a-session-icon-chevron-right2'></span></a></div>";

                        } else echo "<p>No orders found</p>";

                        
                        $payment_method_options_array = array(

                            'frontend_icon_1'     => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_icon_1"] )     ? get_option( "book_a_session_options_paymentmethods" )["frontend_icon_1"]     : book_a_session_options_default( "paymentmethods", "frontend_icon_1" ),
                            'frontend_icon_2'     => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_icon_2"] )     ? get_option( "book_a_session_options_paymentmethods" )["frontend_icon_2"]     : book_a_session_options_default( "paymentmethods", "frontend_icon_2" ),
                            'frontend_icon_3'     => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_icon_3"] )     ? get_option( "book_a_session_options_paymentmethods" )["frontend_icon_3"]     : book_a_session_options_default( "paymentmethods", "frontend_icon_3" ),
                            'frontend_icon_4'     => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_icon_4"] )     ? get_option( "book_a_session_options_paymentmethods" )["frontend_icon_4"]     : book_a_session_options_default( "paymentmethods", "frontend_icon_4" ),
                            'frontend_title_1'    => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_title_1"] )    ? get_option( "book_a_session_options_paymentmethods" )["frontend_title_1"]    : book_a_session_options_default( "paymentmethods", "frontend_title_1" ),
                            'frontend_title_2'    => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_title_2"] )    ? get_option( "book_a_session_options_paymentmethods" )["frontend_title_2"]    : book_a_session_options_default( "paymentmethods", "frontend_title_2" ),
                            'frontend_title_3'    => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_title_3"] )    ? get_option( "book_a_session_options_paymentmethods" )["frontend_title_3"]    : book_a_session_options_default( "paymentmethods", "frontend_title_3" ),
                            'frontend_title_4'    => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_title_4"] )    ? get_option( "book_a_session_options_paymentmethods" )["frontend_title_4"]    : book_a_session_options_default( "paymentmethods", "frontend_title_4" ),
                            'frontend_subtitle_1' => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_subtitle_1"] ) ? get_option( "book_a_session_options_paymentmethods" )["frontend_subtitle_1"] : book_a_session_options_default( "paymentmethods", "frontend_subtitle_1" ),
                            'frontend_subtitle_2' => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_subtitle_2"] ) ? get_option( "book_a_session_options_paymentmethods" )["frontend_subtitle_2"] : book_a_session_options_default( "paymentmethods", "frontend_subtitle_2" ),
                            'frontend_subtitle_3' => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_subtitle_3"] ) ? get_option( "book_a_session_options_paymentmethods" )["frontend_subtitle_3"] : book_a_session_options_default( "paymentmethods", "frontend_subtitle_3" ),
                            'frontend_subtitle_4' => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_subtitle_4"] ) ? get_option( "book_a_session_options_paymentmethods" )["frontend_subtitle_4"] : book_a_session_options_default( "paymentmethods", "frontend_subtitle_4" ),
                            'frontend_tag_1'      => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_tag_1"] )      ? get_option( "book_a_session_options_paymentmethods" )["frontend_tag_1"]      : book_a_session_options_default( "paymentmethods", "frontend_tag_1" ),
                            'frontend_tag_2'      => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_tag_2"] )      ? get_option( "book_a_session_options_paymentmethods" )["frontend_tag_2"]      : book_a_session_options_default( "paymentmethods", "frontend_tag_2" ),
                            'frontend_tag_3'      => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_tag_3"] )      ? get_option( "book_a_session_options_paymentmethods" )["frontend_tag_3"]      : book_a_session_options_default( "paymentmethods", "frontend_tag_3" ),
                            'frontend_tag_4'      => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_tag_4"] )      ? get_option( "book_a_session_options_paymentmethods" )["frontend_tag_4"]      : book_a_session_options_default( "paymentmethods", "frontend_tag_4" ),
                            'frontend_top_1'      => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_top_1"] )      ? get_option( "book_a_session_options_paymentmethods" )["frontend_top_1"]      : book_a_session_options_default( "paymentmethods", "frontend_top_1" ),
                            'frontend_top_2'      => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_top_2"] )      ? get_option( "book_a_session_options_paymentmethods" )["frontend_top_2"]      : book_a_session_options_default( "paymentmethods", "frontend_top_2" ),
                            'frontend_top_3'      => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_top_3"] )      ? get_option( "book_a_session_options_paymentmethods" )["frontend_top_3"]      : book_a_session_options_default( "paymentmethods", "frontend_top_3" ),
                            'frontend_top_4'      => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_top_4"] )      ? get_option( "book_a_session_options_paymentmethods" )["frontend_top_4"]      : book_a_session_options_default( "paymentmethods", "frontend_top_4" ),
                            'frontend_bottom_1'   => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_bottom_1"] )   ? get_option( "book_a_session_options_paymentmethods" )["frontend_bottom_1"]   : book_a_session_options_default( "paymentmethods", "frontend_bottom_1" ),
                            'frontend_bottom_2'   => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_bottom_2"] )   ? get_option( "book_a_session_options_paymentmethods" )["frontend_bottom_2"]   : book_a_session_options_default( "paymentmethods", "frontend_bottom_2" ),
                            'frontend_bottom_3'   => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_bottom_3"] )   ? get_option( "book_a_session_options_paymentmethods" )["frontend_bottom_3"]   : book_a_session_options_default( "paymentmethods", "frontend_bottom_3" ),
                            'frontend_bottom_4'   => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_bottom_4"] )   ? get_option( "book_a_session_options_paymentmethods" )["frontend_bottom_4"]   : book_a_session_options_default( "paymentmethods", "frontend_bottom_4" ),
                            'sandbox_live_1'      => isset( get_option( "book_a_session_options_paymentmethods" )["sandbox_live_1"] )      ? get_option( "book_a_session_options_paymentmethods" )["sandbox_live_1"]      : book_a_session_options_default( "paymentmethods", "sandbox_live_1" ),
                            'sandbox_id_1'        => isset( get_option( "book_a_session_options_paymentmethods" )["sandbox_id_1"] )        ? get_option( "book_a_session_options_paymentmethods" )["sandbox_id_1"]        : book_a_session_options_default( "paymentmethods", "sandbox_id_1" ),
                            'live_id_1'           => isset( get_option( "book_a_session_options_paymentmethods" )["live_id_1"] )           ? get_option( "book_a_session_options_paymentmethods" )["live_id_1"]           : book_a_session_options_default( "paymentmethods", "live_id_1" ),
                            'test_live_4'         => isset( get_option( "book_a_session_options_paymentmethods" )["test_live_4"] )         ? get_option( "book_a_session_options_paymentmethods" )["test_live_4"]         : book_a_session_options_default( "paymentmethods", "test_live_4" ),
                            'test_pub_key_4'      => isset( get_option( "book_a_session_options_paymentmethods" )["test_pub_key_4"] )      ? get_option( "book_a_session_options_paymentmethods" )["test_pub_key_4"]      : book_a_session_options_default( "paymentmethods", "test_pub_key_4" ),
                            'live_pub_key_4'      => isset( get_option( "book_a_session_options_paymentmethods" )["live_pub_key_4"] )      ? get_option( "book_a_session_options_paymentmethods" )["live_pub_key_4"]      : book_a_session_options_default( "paymentmethods", "live_pub_key_4" ),

                        );

                        wp_localize_script( 'book-a-session-js', 'book_a_session_api_order_authentication', array(

                            'api_root'                      => esc_url_raw( rest_url() . 'book-a-session/v1/' ),
                            'order_root'                    => esc_url_raw( rest_url() . 'book-a-session/v1/orders/' ),
                            'order_nonce'                   => wp_create_nonce( 'wp_rest' ),
                            'payment_method_options_array'  => $payment_method_options_array,
                            'vfc_receiving_number'          => isset( get_option( "book_a_session_options_invoices" )["vfc_receiving_mobile_number"] ) ? get_option( "book_a_session_options_invoices" )["vfc_receiving_mobile_number"] : null,
                            'site_name'                     => get_bloginfo( "name" ),
                            'stripe_cc_img_url'             => plugin_dir_url( dirname( __FILE__ ) ) . 'public/img/stripe-logo.png',
                            'logo_square_url'               => ! empty( get_option( "book_a_session_options_general" )["image_logo_square"] ) ? wp_get_attachment_image_src( get_option( "book_a_session_options_general" )["image_logo_square"], "full" ) : null,
                            'redirect_url'                  => ! empty( get_permalink( get_option( "book_a_session_options_general" )["dashboard_page_id"] ) ) ? get_permalink( get_option( "book_a_session_options_general" )["dashboard_page_id"] ) : home_url(),
                            'resetting_password'            => $resetting_password,

                        ) );

                        ?>
                    </div>
                    <div class='book-a-session-content book-a-session-animated book-a-session-dashboard-settings book-a-session-hidden'>
                        <h2>Settings</h2>

                        <form action="" method="post" novalidate="novalidate">

                            <div class="book-a-session-form-fields">

                                <?php wp_nonce_field( "user_" . $user_id . "_update_settings" ); ?>

                                <fieldset class="book-a-session-half">
                                    <label for="first_name">First name <span class='book-a-session-required'>*</span></label>
                                    <input type="text" id="first_name" name="first_name" placeholder="Enter your first name" class="book-a-session-animated" value="<?php echo isset( $_POST['first_name'] ) ? $_POST['first_name'] : $user->first_name; ?>">
                                </fieldset>

                                <fieldset class="book-a-session-half">
                                    <label for="first_name">Last name <span class='book-a-session-required'>*</span></label>
                                    <input type="text" id="last_name" name="last_name" placeholder="Enter your last name" class="book-a-session-animated" value="<?php echo isset( $_POST['last_name'] ) ? $_POST['last_name'] : $user->last_name; ?>">
                                </fieldset>

                                <fieldset>
                                    <label for="email">Email <span class='book-a-session-required'>*</span></label>
                                    <input type="text" id="email" name="email" placeholder="Enter your email address" class="book-a-session-animated" value="<?php echo isset( $_POST['email'] ) ? $_POST['email'] : $user->user_email; ?>">
                                </fieldset>

                                <fieldset>

                                <?php 

                                    // Region label and outer select markup

                                    echo "<label for='region_id'>Region <span class='book-a-session-required'>*</span></label>";
                                    echo "<select name='region_id' id='region_id' required>";

                                    // Get region array

                                    $region_array = book_a_session_get_table_array( "region" );

                                    // Options

                                    if ( ! empty( $region_array ) ) {

                                        echo "<option>Select your region</option>";

                                        // Loop through regions

                                        foreach ( $region_array as $region ) {

                                            echo "<option value='" . $region->id . "'";

                                            if ( ! empty( $_POST['region_id'] ) ) {

                                                // If they've already selected and posted a region but something else went wrong in the form, select that region again for them first and foremost.

                                                if ( $_POST['region_id'] == $region->id ) echo " selected";
                                                
                                            } elseif ( ! empty( get_user_meta( $user_id, "region_id", true ) ) ) {

                                                // Otherwise, if they've got a region already associated with their account, select that one.

                                                if ( get_user_meta( $user_id, "region_id", true ) == $region->id ) echo " selected";

                                            } else {

                                                // Otherwise, get their country code via GeoIP API and use that to select the best region for them

                                                $country_code = book_a_session_get_country_code();

                                                if ( ! empty( $country_code ) ) {

                                                    $region_country = book_a_session_get_table_array( "region_country", false, false, "*", array( "region_id", "=", intval( $region->id ), "country_code", "=", "'" . $country_code . "'" ) );

                                                    if ( ! empty( $region_country ) ) echo " selected";

                                                }

                                            }

                                            echo ">" . $region->name . "</option>";

                                        }

                                        echo "</select>";

                                    }

                                ?>
                                </fieldset>

                                <fieldset>
                                    <label for="email">Phone <span class='book-a-session-optional'>(Optional)</span></label>
                                    <input type="text" id="phone" name="phone" placeholder="Enter your phone number" class="book-a-session-animated" value="<?php echo isset( $_POST['phone'] ) ? $_POST['phone'] :  get_user_meta( $user_id, "phone", true ); ?>">
                                </fieldset>

                                <fieldset class="book-a-session-settings-password <?php if ( $password_match_error ) echo "book-a-session-error"; ?>">

                                    <label for="book-a-session-settings-password">New password <span class='book-a-session-optional'>(Optional)</span></label>
                                    <input class="book-a-session-animated" id="book-a-session-settings-password" type="password" name="book_a_session_settings_password">

                                </fieldset> 

                                <fieldset class="book-a-session-settings-confirm-password <?php if ( $password_match_error ) echo "book-a-session-error"; ?>">

                                    <label for="book-a-session-settings-confirm-password">Confirm new password <span class='book-a-session-optional'>(Optional)</span></label>
                                    <input class="book-a-session-animated" id="book-a-session-settings-confirm-password" type="password" name="book_a_session_settings_confirm_password">

                                </fieldset> 

                                <input type="submit" class="book-a-session-animated" name="submit_settings" value="Save changes">

                            </div>
                        
                        </form>
                        
                    </div>
                    <div class='book-a-session-content book-a-session-animated book-a-session-dashboard-upcoming-sessions book-a-session-hidden'>
                        <h2>Upcoming Sessions</h2>
                        <p>You can view your upcoming sessions in a calendar by going back to Summary.</p>
                        <?php echo $simple_upcoming; ?>
                    </div>
                    <div class='book-a-session-content book-a-session-animated book-a-session-dashboard-session-history book-a-session-hidden'>
                        <h2>Session History</h2>
                        <?php echo book_a_session_timetable( "client_" . $user_id, "simple_history", "DESC", 10, "region", get_user_meta( $user_id, "region_id", true ) ); ?>
                    </div>
                    <div class='book-a-session-content book-a-session-animated book-a-session-dashboard-invoices book-a-session-hidden'>
                        <h2>Invoices</h2>

                        <?php 

                            // Get invoices by user id

                            $invoice_array = book_a_session_get_table_array( "invoice", "issue_date", "DESC", "*", array( "user_id", "=", $user_id ), 10 );

                            if ( ! empty( $invoice_array ) ) {

                                // Create table head

                                echo "<table class='book-a-session-table'><thead>";
                                echo "<th>Issue Date</th>";
                                echo "<th>Due By</th>";
                                echo "<th>Order ID</th>";
                                echo "<th>Total Due</th>";
                                echo "<th>Status</th>";
                                echo "<th>Action</th>";
                                echo "</thead><tbody>";

                                // Table body rows

                                foreach ( $invoice_array as $invoice ) {

                                    // Get order data for each invoice and determine payment status, and decide whether or not to display a payment link

                                    $order = book_a_session_get_table_array( "order", false, false, "*", array( "user_id", "=", $user_id, "order_id", "=", $invoice->order_id ), 1 )[0];
                                    $status = false;

                                    if ( $order ) {

                                        if ( $order->booking_status === "Booked" || $order->booking_status === "Pending" ) {

                                            $status = explode( " ", $order->payment_status );
                                            $status = $status[ count( $status ) - 1 ];
    
                                            if ( $status === "due" ) { $status = "Due"; } elseif ( $status === "paid" ) { $status = "Partly Paid"; } elseif ( $status === "received" ) { $status = "Paid"; }
    
                                        } else {

                                            $status = $order->booking_status;

                                        }

                                    }

                                    echo "<tr>";
                                    // Issue Date
                                    echo "<td>" . date( get_option( "date_format" ), strtotime( $invoice->issue_date ) ) . "</td>";
                                    // Due By
                                    echo "<td>" . date( get_option( "date_format" ), strtotime( $invoice->due_by_date ) ) . " " . date( get_option( "time_format" ), strtotime( $invoice->due_by_date ) ) . "</td>";
                                    // Order ID
                                    echo "<td>" . $invoice->order_id . "</td>";
                                    // Total Due
                                    echo "<td>". book_a_session_get_price_tag( $invoice->grand_total_due, $invoice->currency_id ) . "</td>";
                                    // Status
                                    echo "<td>" . $status . "</td>";
                                    // Action
                                    echo "<td>";
                                    // View invoice
                                    echo "<a href='javascript:void(0)' data-invoice='" . $invoice->id . "' class='book-a-session-button book-a-session-animated book-a-session-dark-button book-a-session-mini-button book-a-session-button-icon book-a-session-button-icon-right book-a-session-view-invoice'>View<span class='book-a-session-icon'>❯</span></a>";
                                    echo "</tr>";

                                } 

                            } else echo "<tr><td scope='4'>No orders found</td></tr>";

                            echo "</tbody></table>";

                            wp_localize_script( 'book-a-session-js', 'book_a_session_api_invoice_authentication', array(
                                'invoice_root'  => esc_url_raw( rest_url() . 'book-a-session/v1/invoices/' ),
                                'invoice_nonce' => wp_create_nonce( 'wp_rest' ),
                            ) );

                        ?>
                    </div>
                    <?php if ( $private_location ) : ?>
                    <div class='book-a-session-content book-a-session-animated book-a-session-dashboard-private-locations book-a-session-hidden'>
                        <h2>Private Locations</h2>
                        <p>If you book at a location with a private address, and your order is confirmed, you'll find its address here. Please do not share this information freely.</p>
                        <?php 

                            // Get all private locations

                            $location_array = book_a_session_get_table_array( "location", false, false, "*", array( "public_address", "=", 0 ) );

                            if ( ! empty( $location_array ) ) :

                                // Get all orders made by the client

                                $order_array = book_a_session_get_table_array( "order", false, false, "location_id, booking_status, service_id, practitioner_id", array( "user_id", "=", $user_id ) );

                                if ( ! empty( $order_array ) ) :

                                    // For each private location, loop through the orders to find one that is for that location and is not pending or cancelled.

                                    foreach( $location_array as $location ) :

                                        foreach( $order_array as $order ) :

                                            if ( intval( $order->location_id ) === intval( $location->id ) && $order->booking_status !== "Pending" && $order->booking_status !== "Cancelled" ) :

                                                $address_line_counter = 0;

                                                echo "<p><strong>" . $location->name . "</strong><br>";

                                                if ( ! empty( $location->address_line_1 ) )   { echo $location->address_line_1 . "<br>"; $address_line_counter++; }
                                                if ( ! empty( $location->address_line_2 ) )   { echo $location->address_line_2 . "<br>"; $address_line_counter++; }
                                                if ( ! empty( $location->address_line_3 ) )   { echo $location->address_line_3 . "<br>"; $address_line_counter++; }
                                                if ( ! empty( $location->city ) )             { echo $location->city . "<br>"; $address_line_counter++; }
                                                if ( ! empty( $location->country ) )          { echo $location->country . "<br>"; $address_line_counter++; }
                                                if ( ! empty( $location->postcode_zipcode ) ) { echo $location->postcode_zipcode; $address_line_counter++; }

                                                if ( $address_line_counter === 0 )  {

                                                    $service_id = $order->service_id;
                                                    $service_type_id = book_a_session_get_service_by_id( array( "id" => $service_id ) )["service_type_id"];
                                                    $practitioner_title = book_a_session_get_service_type_by_id( array( "id" => $service_type_id ) )["practitioner_title"];
                                                    $practitioner =  get_userdata( intval( $order->practitioner_id ) );
                                                    echo 'Address not found. It might be that we haven\'t gotten it yet. Ask your ' . strtolower( $practitioner_title ) . ", " . $practitioner->first_name . " " . $practitioner->last_name . ", by emailing <a href='mailto:$practitioner->user_email'>" . $practitioner->user_email . "</a>";

                                                }
                                                echo "</p>";

                                                break 1;
                                                
                                            endif;

                                        endforeach;

                                    endforeach;
        
                                endif;

                            endif;


                        ?>
                    </div>
                    <?php endif; ?>
                    <div class='book-a-session-content book-a-session-animated book-a-session-dashboard-logout book-a-session-hidden'>
                        <h2>You're about to log out</h2>
                        <p>Are you sure?</p>
                        <?php echo '<a class="book-a-session-animated book-a-session-button book-a-session-dark-button book-a-session-button-icon-right book-a-session-logout" href="' . wp_logout_url( get_permalink() ? get_permalink() : home_url() ) . '">Logout<span class="book-a-session-icon book-a-session-icon-exit_to_app"></span></a>'; ?>                    </div>
                    </div>

                </div>

            <?php

        } else {
            
            // Practitioner settings post

            if ( ! empty( $_POST['submit_settings'] ) ) {

                // Security check

                if ( get_current_user_id() === $user_id && wp_verify_nonce( $_REQUEST['_wpnonce'], "user_" . $user_id . "_update_settings" ) || 
                     current_user_can( "edit_users" )   && wp_verify_nonce( $_REQUEST['_wpnonce'], "user_" . $user_id . "_update_settings" ) ){

                    // Required values check

                    if ( ! empty( $_POST['first_name'] )    && 
                         ! empty( $_POST['last_name'] )     &&
                         ! empty( $_POST['email'] )         && 
                         ! empty( $_POST['region_id'] )     ){

                        // Prepare updated user values

                        if ( ! empty( $_POST['book_a_session_settings_password'] ) && ! empty( $_POST["book_a_session_settings_confirm_password"] ) ) {

                            if ( $_POST["book_a_session_settings_password"] === $_POST["book_a_session_settings_confirm_password"] ) {
                
                                $user_updated_array = array(

                                    "ID"            => $user_id,
                                    "first_name"    => $_POST['first_name'],
                                    "last_name"     => $_POST['last_name'],
                                    "user_email"    => $_POST['email'],
                                    "user_pass"     => $_POST["book_a_session_settings_password"],

                                );  

                                $password_updated = true;

                            } else {

                                $user_updated_array = array(

                                    "ID"            => $user_id,
                                    "first_name"    => $_POST['first_name'],
                                    "last_name"     => $_POST['last_name'],
                                    "user_email"    => $_POST['email']
        
                                );    
                                
                                $password_match_error = "Passwords do not match.";

                            }

                        } else {

                            $user_updated_array = array(

                                "ID"            => $user_id,
                                "first_name"    => $_POST['first_name'],
                                "last_name"     => $_POST['last_name'],
                                "user_email"    => $_POST['email']
    
                            ); 

                        }

                        // Update user

                        $update_user = wp_update_user( $user_updated_array );

                        // Update Region ID user meta
                        
                        $prev_region = ! empty ( intval( get_user_meta( $user_id, "region_id", true ) ) ) ? intval( get_user_meta( $user_id, "region_id", true ) ) : null;

                        if ( intval( $_POST['region_id'] ) !== $prev_region ) $update_region = update_user_meta( $user_id, "region_id", intval( $_POST['region_id'] ), $prev_region );

                        // Update Phone user meta

                        if ( ! empty( $_POST['phone'] ) ) $update_phone = update_user_meta( $user_id, "phone", $_POST['phone'] );

                    } else $required_error = true;

                }    

            }

            $simple_upcoming = book_a_session_timetable( "practitioner_" . $user_id, "simple_upcoming", "ASC", 10, "region", get_user_meta( $user_id, "region_id", true ) );

            // Practitioner dashboard

            ?>

                <div class='book-a-session-dashboard book-a-session-card book-a-session-animated'>
                    <div class='book-a-session-sidebar'>
                        <nav class='book-a-session-navigation'>
                            <ul>
                            <li><a data-content='book-a-session-dashboard-summary' class='book-a-session-animated book-a-session-current' href='javascript:void(0)'>Summary</a></li>
                            <li><a data-modal='book-a-session-modal-week-timetable' class='book-a-session-animated' href='javascript:void(0)'>Week Timetable</a></li>
                            <li><a data-content='book-a-session-dashboard-orders' class='book-a-session-animated' href='javascript:void(0)'>Client orders</a></li>
                            <li><a data-content='book-a-session-dashboard-upcoming-sessions' class='book-a-session-animated' href='javascript:void(0)'>Upcoming Sessions</a></li>
                            <li><a data-content='book-a-session-dashboard-session-history' class='book-a-session-animated' href='javascript:void(0)'>Session History</a></li>
                            <li><a data-content='book-a-session-dashboard-settings' class='book-a-session-animated ' href='javascript:void(0)'>Settings</a></li>
                            <li><a data-content='book-a-session-dashboard-logout' class='book-a-session-animated' href='javascript:void(0)'>Log Out</a></li>
                            </ul>
                        </nav>
                    </div>

                    <div class='book-a-session-content book-a-session-animated book-a-session-dashboard-summary'>
                        <h2>Welcome back, <?php echo $user->first_name . " " .  $user->last_name; ?>.</h2>
                        <div class='book-a-session-upcoming-sessions-widget book-a-session-widget'>
                            <h3>Upcoming Sessions</h3>
                            <?php echo book_a_session_timetable( "practitioner_" . $user_id, "calendar", false, false, "region", get_user_meta( $user_id, "region_id", true ) ); ?>
                        </div>

                        <div class='book-a-session-session-history-widget book-a-session-widget'>
                            <h3>Session History</h3>
                            <?php echo book_a_session_timetable( "practitioner_" . $user_id, "simple_history", "DESC", 4, "region", get_user_meta( $user_id, "region_id", true ) ); ?>
                        </div>
                    </div>
                    <div class='book-a-session-content book-a-session-animated book-a-session-dashboard-orders book-a-session-hidden'>
                        <h2>Client orders</h2>

                        <?php 
                        $order_array = book_a_session_get_table_array( "order", "created", "DESC", "*", array( "practitioner_id", "=", $user_id ), 10 );

                        if ( ! empty( $order_array ) ) {

                            echo "<table class='book-a-session-table'><thead><th>Date</th><th>Order ID</th><th>Status</th><th>Total</th><th>Due</th><th>Order</th></thead><tbody>";

                            foreach ( $order_array as $order ) :

                                $session_array = book_a_session_get_table_array( "session", "start_datetime", "ASC", "*", array( "order_id", "=", $order->order_id ) );
                                $project_array = book_a_session_get_table_array( "project", false, false, "*", array( "order_id", "=", $order->order_id ) );
                                $order_cell = "";

                                if ( $order->session && ! empty( $session_array ) ) :

                                    $order_cell = count( $session_array ) > 1 ? count( $session_array ) . " Sessions" : "1 Session";
                                    $order_attribute = "data-session-order='" . $order->order_id . "'";

                                elseif ( $order->project && ! empty( $project_array ) ) :

                                    $order_cell = "Project";
                                    $order_attribute = "data-project-order='" . $order->order_id . "'";


                                endif;
                                
                                // Created

                                echo "<tr><td>" . date( get_option( "date_format" ), strtotime( $order->created ) ) . "</td>"; 

                                // Order ID

                                echo "<td>" . $order->order_id . "</td>";

                                // Status

                                echo "<td>" . $order->booking_status . "</td>";

                                // Total

                                echo "<td>" . book_a_session_get_price_tag( ( $order->total ), $order->currency_id ) . "</td>";                                
                                
                                // Due

                                echo "<td>" . book_a_session_get_price_tag( ( $order->total_due ), $order->currency_id ) . "</td>";

                                // Order

                                echo "<td><a href='javascript:void(0)' $order_attribute class='book-a-session-button book-a-session-animated book-a-session-dark-button book-a-session-mini-button book-a-session-button-icon book-a-session-button-icon-right book-a-session-view-order'>" . $order_cell . "<span class='book-a-session-icon'>❯</span></a></td></tr>";



                            endforeach;

                            echo "</tbody></table>";

                        } else echo "<p>No orders found</p>";

                        
                        $payment_method_options_array = array(

                            'frontend_icon_1'     => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_icon_1"] )     ? get_option( "book_a_session_options_paymentmethods" )["frontend_icon_1"]     : book_a_session_options_default( "paymentmethods", "frontend_icon_1" ),
                            'frontend_icon_2'     => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_icon_2"] )     ? get_option( "book_a_session_options_paymentmethods" )["frontend_icon_2"]     : book_a_session_options_default( "paymentmethods", "frontend_icon_2" ),
                            'frontend_icon_3'     => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_icon_3"] )     ? get_option( "book_a_session_options_paymentmethods" )["frontend_icon_3"]     : book_a_session_options_default( "paymentmethods", "frontend_icon_3" ),
                            'frontend_icon_4'     => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_icon_4"] )     ? get_option( "book_a_session_options_paymentmethods" )["frontend_icon_4"]     : book_a_session_options_default( "paymentmethods", "frontend_icon_4" ),
                            'frontend_title_1'    => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_title_1"] )    ? get_option( "book_a_session_options_paymentmethods" )["frontend_title_1"]    : book_a_session_options_default( "paymentmethods", "frontend_title_1" ),
                            'frontend_title_2'    => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_title_2"] )    ? get_option( "book_a_session_options_paymentmethods" )["frontend_title_2"]    : book_a_session_options_default( "paymentmethods", "frontend_title_2" ),
                            'frontend_title_3'    => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_title_3"] )    ? get_option( "book_a_session_options_paymentmethods" )["frontend_title_3"]    : book_a_session_options_default( "paymentmethods", "frontend_title_3" ),
                            'frontend_title_4'    => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_title_4"] )    ? get_option( "book_a_session_options_paymentmethods" )["frontend_title_4"]    : book_a_session_options_default( "paymentmethods", "frontend_title_4" ),
                            'frontend_subtitle_1' => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_subtitle_1"] ) ? get_option( "book_a_session_options_paymentmethods" )["frontend_subtitle_1"] : book_a_session_options_default( "paymentmethods", "frontend_subtitle_1" ),
                            'frontend_subtitle_2' => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_subtitle_2"] ) ? get_option( "book_a_session_options_paymentmethods" )["frontend_subtitle_2"] : book_a_session_options_default( "paymentmethods", "frontend_subtitle_2" ),
                            'frontend_subtitle_3' => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_subtitle_3"] ) ? get_option( "book_a_session_options_paymentmethods" )["frontend_subtitle_3"] : book_a_session_options_default( "paymentmethods", "frontend_subtitle_3" ),
                            'frontend_subtitle_4' => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_subtitle_4"] ) ? get_option( "book_a_session_options_paymentmethods" )["frontend_subtitle_4"] : book_a_session_options_default( "paymentmethods", "frontend_subtitle_4" ),
                            'frontend_tag_1'      => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_tag_1"] )      ? get_option( "book_a_session_options_paymentmethods" )["frontend_tag_1"]      : book_a_session_options_default( "paymentmethods", "frontend_tag_1" ),
                            'frontend_tag_2'      => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_tag_2"] )      ? get_option( "book_a_session_options_paymentmethods" )["frontend_tag_2"]      : book_a_session_options_default( "paymentmethods", "frontend_tag_2" ),
                            'frontend_tag_3'      => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_tag_3"] )      ? get_option( "book_a_session_options_paymentmethods" )["frontend_tag_3"]      : book_a_session_options_default( "paymentmethods", "frontend_tag_3" ),
                            'frontend_tag_4'      => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_tag_4"] )      ? get_option( "book_a_session_options_paymentmethods" )["frontend_tag_4"]      : book_a_session_options_default( "paymentmethods", "frontend_tag_4" ),
                            'frontend_top_1'      => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_top_1"] )      ? get_option( "book_a_session_options_paymentmethods" )["frontend_top_1"]      : book_a_session_options_default( "paymentmethods", "frontend_top_1" ),
                            'frontend_top_2'      => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_top_2"] )      ? get_option( "book_a_session_options_paymentmethods" )["frontend_top_2"]      : book_a_session_options_default( "paymentmethods", "frontend_top_2" ),
                            'frontend_top_3'      => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_top_3"] )      ? get_option( "book_a_session_options_paymentmethods" )["frontend_top_3"]      : book_a_session_options_default( "paymentmethods", "frontend_top_3" ),
                            'frontend_top_4'      => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_top_4"] )      ? get_option( "book_a_session_options_paymentmethods" )["frontend_top_4"]      : book_a_session_options_default( "paymentmethods", "frontend_top_4" ),
                            'frontend_bottom_1'   => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_bottom_1"] )   ? get_option( "book_a_session_options_paymentmethods" )["frontend_bottom_1"]   : book_a_session_options_default( "paymentmethods", "frontend_bottom_1" ),
                            'frontend_bottom_2'   => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_bottom_2"] )   ? get_option( "book_a_session_options_paymentmethods" )["frontend_bottom_2"]   : book_a_session_options_default( "paymentmethods", "frontend_bottom_2" ),
                            'frontend_bottom_3'   => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_bottom_3"] )   ? get_option( "book_a_session_options_paymentmethods" )["frontend_bottom_3"]   : book_a_session_options_default( "paymentmethods", "frontend_bottom_3" ),
                            'frontend_bottom_4'   => isset( get_option( "book_a_session_options_paymentmethods" )["frontend_bottom_4"] )   ? get_option( "book_a_session_options_paymentmethods" )["frontend_bottom_4"]   : book_a_session_options_default( "paymentmethods", "frontend_bottom_4" ),
                            'sandbox_live_1'      => isset( get_option( "book_a_session_options_paymentmethods" )["sandbox_live_1"] )      ? get_option( "book_a_session_options_paymentmethods" )["sandbox_live_1"]      : book_a_session_options_default( "paymentmethods", "sandbox_live_1" ),
                            'sandbox_id_1'        => isset( get_option( "book_a_session_options_paymentmethods" )["sandbox_id_1"] )        ? get_option( "book_a_session_options_paymentmethods" )["sandbox_id_1"]        : book_a_session_options_default( "paymentmethods", "sandbox_id_1" ),
                            'live_id_1'           => isset( get_option( "book_a_session_options_paymentmethods" )["live_id_1"] )           ? get_option( "book_a_session_options_paymentmethods" )["live_id_1"]           : book_a_session_options_default( "paymentmethods", "live_id_1" ),
                            'test_live_4'         => isset( get_option( "book_a_session_options_paymentmethods" )["test_live_4"] )         ? get_option( "book_a_session_options_paymentmethods" )["test_live_4"]         : book_a_session_options_default( "paymentmethods", "test_live_4" ),
                            'test_pub_key_4'      => isset( get_option( "book_a_session_options_paymentmethods" )["test_pub_key_4"] )      ? get_option( "book_a_session_options_paymentmethods" )["test_pub_key_4"]      : book_a_session_options_default( "paymentmethods", "test_pub_key_4" ),
                            'live_pub_key_4'      => isset( get_option( "book_a_session_options_paymentmethods" )["live_pub_key_4"] )      ? get_option( "book_a_session_options_paymentmethods" )["live_pub_key_4"]      : book_a_session_options_default( "paymentmethods", "live_pub_key_4" ),

                        );

                        wp_localize_script( 'book-a-session-js', 'book_a_session_api_order_authentication', array(

                            'api_root'                      => esc_url_raw( rest_url() . 'book-a-session/v1/' ),
                            'order_root'                    => esc_url_raw( rest_url() . 'book-a-session/v1/orders/' ),
                            'order_nonce'                   => wp_create_nonce( 'wp_rest' ),
                            'payment_method_options_array'  => $payment_method_options_array,
                            'vfc_receiving_number'          => isset( get_option( "book_a_session_options_invoices" )["vfc_receiving_mobile_number"] ) ? get_option( "book_a_session_options_invoices" )["vfc_receiving_mobile_number"] : null,
                            'site_name'                     => get_bloginfo( "name" ),
                            'stripe_cc_img_url'             => plugin_dir_url( dirname( __FILE__ ) ) . 'public/img/stripe-logo.png',
                            'logo_square_url'               => ! empty( get_option( "book_a_session_options_general" )["image_logo_square"] ) ? wp_get_attachment_image_src( get_option( "book_a_session_options_general" )["image_logo_square"], "full" ) : null,
                            'resetting_password'            => $resetting_password,
                            
                        ) );

                        ?>
                    </div>
                    <div class='book-a-session-content book-a-session-animated book-a-session-dashboard-settings book-a-session-hidden'>
                        <h2>Settings</h2>

                            <form action="" method="post" novalidate="novalidate">

                                <div class="book-a-session-form-fields">

                                    <?php wp_nonce_field( "user_" . $user_id . "_update_settings" ); ?>

                                    <fieldset class="book-a-session-half">
                                        <label for="first_name">First name <span class='book-a-session-required'>*</span></label>
                                        <input type="text" id="first_name" name="first_name" placeholder="Enter your first name" class="book-a-session-animated" value="<?php echo isset( $_POST['first_name'] ) ? $_POST['first_name'] : $user->first_name; ?>">
                                    </fieldset>

                                    <fieldset class="book-a-session-half">
                                        <label for="first_name">Last name <span class='book-a-session-required'>*</span></label>
                                        <input type="text" id="last_name" name="last_name" placeholder="Enter your last name" class="book-a-session-animated" value="<?php echo isset( $_POST['last_name'] ) ? $_POST['last_name'] : $user->last_name; ?>">
                                    </fieldset>

                                    <fieldset>
                                        <label for="email">Email <span class='book-a-session-required'>*</span></label>
                                        <input type="text" id="email" name="email" placeholder="Enter your email address" class="book-a-session-animated" value="<?php echo isset( $_POST['email'] ) ? $_POST['email'] : $user->user_email; ?>">
                                    </fieldset>

                                    <fieldset>

                                    <?php 

                                        // Region label and outer select markup

                                        echo "<label for='region_id'>Region <span class='book-a-session-required'>*</span></label>";
                                        echo "<select name='region_id' id='region_id' required>";

                                        // Get region array

                                        $region_array = book_a_session_get_table_array( "region" );

                                        // Options

                                        if ( ! empty( $region_array ) ) {

                                            echo "<option>Select your region</option>";

                                            // Loop through regions

                                            foreach ( $region_array as $region ) {

                                                echo "<option value='" . $region->id . "'";

                                                if ( ! empty( $_POST['region_id'] ) ) {

                                                    // If they've already selected and posted a region but something else went wrong in the form, select that region again for them first and foremost.

                                                    if ( $_POST['region_id'] == $region->id ) echo " selected";
                                                    
                                                } elseif ( ! empty( get_user_meta( $user_id, "region_id", true ) ) ) {

                                                    // Otherwise, if they've got a region already associated with their account, select that one.

                                                    if ( get_user_meta( $user_id, "region_id", true ) == $region->id ) echo " selected";

                                                } else {

                                                    // Otherwise, get their country code via GeoIP API and use that to select the best region for them

                                                    $country_code = book_a_session_get_country_code();

                                                    if ( ! empty( $country_code ) ) {

                                                        $region_country = book_a_session_get_table_array( "region_country", false, false, "*", array( "region_id", "=", intval( $region->id ), "country_code", "=", "'" . $country_code . "'" ) );

                                                        if ( ! empty( $region_country ) ) echo " selected";

                                                    }

                                                }

                                                echo ">" . $region->name . "</option>";

                                            }

                                            echo "</select>";

                                        }

                                    ?>
                                    </fieldset>

                                    <fieldset>
                                        <label for="email">Phone <span class='book-a-session-optional'>(Optional)</span></label>
                                        <input type="text" id="phone" name="phone" placeholder="Enter your phone number" class="book-a-session-animated" value="<?php echo isset( $_POST['phone'] ) ? $_POST['phone'] :  get_user_meta( $user_id, "phone", true ); ?>">
                                    </fieldset>

                                    <fieldset class="book-a-session-settings-password <?php if ( $password_match_error ) echo "book-a-session-error"; ?>">

                                        <label for="book-a-session-settings-password">New password <span class='book-a-session-optional'>(Optional)</span></label>
                                        <input class="book-a-session-animated" id="book-a-session-settings-password" type="password" name="book_a_session_settings_password">

                                    </fieldset> 

                                    <fieldset class="book-a-session-settings-confirm-password <?php if ( $password_match_error ) echo "book-a-session-error"; ?>">

                                        <label for="book-a-session-settings-confirm-password">Confirm new password <span class='book-a-session-optional'>(Optional)</span></label>
                                        <input class="book-a-session-animated" id="book-a-session-settings-confirm-password" type="password" name="book_a_session_settings_confirm_password">

                                    </fieldset> 

                                    <input type="submit" class="book-a-session-animated" name="submit_settings" value="Save changes">

                                </div>

                            </form>

                    </div>
                    <div class='book-a-session-content book-a-session-animated book-a-session-dashboard-upcoming-sessions book-a-session-hidden'>
                        <h2>Upcoming Sessions</h2>
                        <p>You can view your upcoming sessions in a calendar by going back to Summary.</p>
                        <?php echo $simple_upcoming; ?>
                    </div>
                    <div class='book-a-session-content book-a-session-animated book-a-session-dashboard-session-history book-a-session-hidden'>
                        <h2>Session History</h2>
                        <?php echo book_a_session_timetable( "practitioner_" . $user_id, "simple_history", "DESC", 10, "region", get_user_meta( $user_id, "region_id", true ) ); ?>
                    </div>
                    <div class='book-a-session-content book-a-session-animated book-a-session-dashboard-logout book-a-session-hidden'>
                        <h2>You're about to log out</h2>
                        <p>Are you sure?</p>
                        <?php echo '<a class="book-a-session-animated book-a-session-button book-a-session-dark-button book-a-session-button-icon-right book-a-session-logout" href="' . wp_logout_url( get_permalink() ? get_permalink() : home_url() ) . '">Logout<span class="book-a-session-icon book-a-session-icon-exit_to_app"></span></a>'; ?>                    </div>

                </div>

                <div class='book-a-session-modal book-a-session-modal-week-timetable book-a-session-animated book-a-session-hidden'>

                    <div class="book-a-session-modal-inner book-a-session-animated book-a-session-dashboard-week-timetable-inner">
                        <div class="book-a-session-modal-ui book-a-session-icon-close book-a-session-animated"></div>
                        <?php echo book_a_session_timetable( "practitioner_" . $user_id, "week", false, false, "region", get_user_meta( $user_id, "region_id", true ) ); ?>

                    </div>

                </div>

            <?php

        }

    } else {
        
        // Login or register

        book_a_session_print_login_register_form();
        
    }

}