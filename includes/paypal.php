<?php
/**
 * Book A Session - PayPal REST API Functionality.
 * 
 * Here you will find all PayPal related functions, some of which are strictly accessed via AJAX on the frontend, and some of which are triggered here, such as refunds.
 * 
 */

if ( ! defined( 'ABSPATH' ) ) exit;

function book_a_session_get_paypal_info( $data ) {

    $options = array(

        'sandbox_live_1' => isset( get_option( "book_a_session_options_paymentmethods" )["sandbox_live_1"] ) ? get_option( "book_a_session_options_paymentmethods" )["sandbox_live_1"] : book_a_session_options_default( "paymentmethods", "sandbox_live_1" ),
        'sandbox_id_1'   => isset( get_option( "book_a_session_options_paymentmethods" )["sandbox_id_1"] )   ? get_option( "book_a_session_options_paymentmethods" )["sandbox_id_1"]   : book_a_session_options_default( "paymentmethods", "sandbox_id_1" ),
        'live_id_1'      => isset( get_option( "book_a_session_options_paymentmethods" )["live_id_1"] )      ? get_option( "book_a_session_options_paymentmethods" )["live_id_1"]      : book_a_session_options_default( "paymentmethods", "live_id_1" ),
    
    );

    return rest_ensure_response( $options );

}

function book_a_session_get_paypal_auth_token(){

    $url        = get_option( "book_a_session_options_paymentmethods" )["sandbox_live_1"] === "sandbox" ? "https://api.sandbox.paypal.com/v1/oauth2/token"                          : "https://api.paypal.com/v1/oauth2/token";
    $client_id  = get_option( "book_a_session_options_paymentmethods" )["sandbox_live_1"] === "sandbox" ? get_option( "book_a_session_options_paymentmethods" )["sandbox_id_1"]     : get_option( "book_a_session_options_paymentmethods" )["live_id_1"];
    $secret     = get_option( "book_a_session_options_paymentmethods" )["sandbox_live_1"] === "sandbox" ? get_option( "book_a_session_options_paymentmethods" )["sandbox_secret_1"] : get_option( "book_a_session_options_paymentmethods" )["live_secret_1"];

    $args = array(
    
        "headers" => array(
    
            "Accept"            => "application/json",
            "Accept-Language"   => "en_US",
            "Content-Type"      => "application/x-www-form-urlencoded",
            'Authorization'     => 'Basic ' . base64_encode( $client_id . ':' . $secret ),
    
        ),
        
        "body" => array(
    
            "grant_type" => "client_credentials",
    
        )
    
    );

    $result = wp_remote_post( $url, $args );

    $access_token = json_decode( $result["body"] )->access_token;

    if ( $access_token ) return array( 

        "access_token"  => $access_token,
        "result"        => $result,

    );
    
}

function book_a_session_request_paypal_payment( $data ){

    if ( empty( $data ) ) {

        $data = array(

            // Sample data

            "intent"    => "sale",

            "payer"     => array(

                "payment_method" => "paypal",

            ),

            "transactions" => array(

                "amount" => array(

                    "total"     => "49.99",
                    "currency"  => "GBP",

                ),

            ),

        );

    }

    $url        = get_option( "book_a_session_options_paymentmethods" )["sandbox_live_1"] === "sandbox" ? "https://api.sandbox.paypal.com/v1/payments/payment/"                      : "https://api.paypal.com/v1/payments/payment/";
    $client_id  = get_option( "book_a_session_options_paymentmethods" )["sandbox_live_1"] === "sandbox" ? get_option( "book_a_session_options_paymentmethods" )["sandbox_id_1"]     : get_option( "book_a_session_options_paymentmethods" )["live_id_1"];
    $secret     = get_option( "book_a_session_options_paymentmethods" )["sandbox_live_1"] === "sandbox" ? get_option( "book_a_session_options_paymentmethods" )["sandbox_secret_1"] : get_option( "book_a_session_options_paymentmethods" )["live_secret_1"];

    $auth_token = book_a_session_get_paypal_auth_token();

    $args = array(
    
        "headers" => array(
    
            "Content-Type"      => "application/json",
            'Authorization'     => 'Bearer ' . $auth_token["access_token"],
    
        ),
        
        "body" => array(
    
            "intent"        => $data["intent"],
            "payer"         => $data["payer"],
            "transactions"  => $data["transactions"],

        )
    
    );

    if ( ! empty( $auth_token ) && ! empty( $url ) && ! empty( $client_id ) && ! empty( $secret ) ) {

        return rest_ensure_response( 

            array( 

                "payment_result"        => wp_remote_post( $url, $args ), 
                "access_token_result"   => $auth_token,

            )

        );

    } else return rest_ensure_response( new WP_Error( __( "Required data for PayPal REST API missing", "book_a_session" ) ) );

}
