<?php // Book A Session - Activation. Create all custom post types, tables and such.

// Exit if file is called directly

if ( ! defined( 'ABSPATH' ) ) exit;

function book_a_session_get_booking_status_array(){

    return array(

        "'Booked'",
        "'Pending'",
        "'Completed'",
        "'Cancelled'",
        "'Missing Client'",
        "'Missing Practitioner'"

    );

}

function book_a_session_get_payment_status_array() {

    return array(

        "'Online payment due'",
        "'Cash payment due'",
        "'Vodafone Cash payment due'",
        "'Cash payment partly paid'",
        "'Vodafone Cash partly paid'",
        "'Online payment partly paid'",
        "'Online payment received'",
        "'Cash payment received'",
        "'Vodafone Cash payment received'"

    );

}

function book_a_session_get_unavailable_array() {

    return array(

        "'All'",
        "'Client'",
        "'Location'",
        "'Practitioner'"

    );

}

// Create Region Table

    // ID
    // International
    // Name
    // Description
    // Image

function book_a_session_create_region_table() {
    
    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_region';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
      id mediumint(9) NOT NULL AUTO_INCREMENT,
      international  tinyint(1) NOT NULL,
      name tinytext NOT NULL,
      description text NOT NULL,
      timezone varchar(64) NOT NULL,
      sunday_week tinyint(1) NOT NULL,
      code varchar(2) NOT NULL,
      currency_id mediumint(9) NOT NULL,
      session_price decimal(10, 2) NOT NULL,
      image_id mediumint(9),
      PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}

// Create Bundle Table

    // Quantity
    // Name


    function book_a_session_create_bundle_table() {
    
        global $wpdb;
        $table_name = $wpdb->prefix . 'book_a_session_bundle';
    
        $charset_collate = $wpdb->get_charset_collate();
    
        $sql = "CREATE TABLE $table_name (
          quantity mediumint(9) NOT NULL,
          name varchar(255) NOT NULL,
          PRIMARY KEY  (quantity)
        ) $charset_collate;";
    
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
        
    }

// Create Service Type Table

    // ID
    // Name: 'Coaching'
    // Slug: 'coaching'
    // Practitioner Title: 'Coach'
    // Practitioner Slug: 'coach' 

function book_a_session_create_service_type_table() {
    
    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_service_type';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
      id mediumint(9) NOT NULL AUTO_INCREMENT,
      name  varchar(55) NOT NULL,
      slug varchar(55) NOT NULL,
      practitioner_title varchar(55) NOT NULL,
      practitioner_slug varchar(55) NOT NULL,
      PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}


// Create Payment Method Table

    // ID
    // Name

function book_a_session_create_payment_method_table() {
    
    // Create table

    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_payment_method';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
      id mediumint(9) NOT NULL AUTO_INCREMENT,
      name  varchar(55) NOT NULL,
      PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );

}


// Create Practitioners

function book_a_session_add_practitioner_roles() {
    
    global $wp_roles;

    $admin_role = 'administrator';
    $admin_role_obj = get_role( $admin_role );
    $admin_role_cap = $admin_role_obj->capabilities;
    $role = 'practitioner';
    $role_name = 'Practitioner';
    $role_obj = get_role( 'contributor' );
    $role_exists = get_role( $role );
    $role_cap = $role_obj->capabilities;
    $admin_prac_role = 'admin_practitioner';
    $admin_prac_role_name = 'Administrator Practitioner';
    $admin_prac_role_exists = get_role( $admin_prac_role );
    $admin_prac_role_cap = $admin_role_cap;


    if ($wp_roles && !$role_exists) {
        
        add_role( $role, $role_name, $role_cap );

        // Add Custom Field Service Type

        // Add Custom Field Description

        // Add Custom Option Accept Cash

        // Add Custom Option Accept PayPal

        // Add Custom Option Accept VFC
        
    }
    
    if ($wp_roles && !$admin_prac_role_exists) {
        
        add_role( $admin_prac_role, $admin_prac_role_name, $admin_prac_role_cap );

    }
    
}


// Create Location Table

        // ID
        // Region ID
        // Name
        // Address Line 1
        // Address Line 2
        // Address Line 3
        // City
        // Country
        // Postcode
        // Coordinates
        // Public Address: 0, 1
        // Accept Cash: 0, 1
        // Accept PayPal: 0, 1
        // Accept VFC: 0, 1

function book_a_session_create_location_table() {
    
    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_location';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
      id mediumint(9) NOT NULL AUTO_INCREMENT,
      region_id mediumint(9) NOT NULL,
      name varchar(64) NOT NULL,
      address_line_1 varchar(64),
      address_line_2 varchar(64),
      address_line_3 varchar(64),
      city varchar(64),
      country varchar(64),
      postcode_zipcode varchar(64),
      public_address tinyint(1) NOT NULL,
      accept_mon tinyint(1) NOT NULL,
      accept_tue tinyint(1) NOT NULL,
      accept_wed tinyint(1) NOT NULL,
      accept_thu tinyint(1) NOT NULL,
      accept_fri tinyint(1) NOT NULL,
      accept_sat tinyint(1) NOT NULL,
      accept_sun tinyint(1) NOT NULL,
      tag varchar(64),
      capacity mediumint(9) NOT NULL,
      PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}

// Create Services Table

        // ID
        // Service Type ID
        // Title
        // Description
        // Image

function book_a_session_create_service_table() {
    
    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_service';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
      id mediumint(9) NOT NULL AUTO_INCREMENT,
      service_type_id mediumint(9) NOT NULL,
      session tinyint(1),
      project tinyint(1),
      name varchar(64) NOT NULL,
      description text NOT NULL,
      image_id mediumint(9),
      PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}

// Create Schedule Table

    // ID
    // Start Time: 11:00
    // End Time: 12:00

function book_a_session_create_schedule_table() {
    
    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_schedule';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
      id mediumint(9) NOT NULL AUTO_INCREMENT,
      start_time time NOT NULL,
      end_time time NOT NULL,
      PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}


// Create Currency Table

    // ID
    // Currency Code: EGP
    // Currency Name: Egyptian Pounds
    // Symbol After: 0, 1
    // Use Alternative Symbol: 0,1
    // Alternative Symbol: 'LE'

function book_a_session_create_currency_table() {
    
    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_currency';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
      id mediumint(9) NOT NULL AUTO_INCREMENT,
      code tinytext NOT NULL,
      symbol_after tinyint(1) NOT NULL,
      use_alternative_symbol tinyint(1) NOT NULL,
      symbol varchar(20),
      PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}

// Create Pracitioner Location Table

    // Practitioner ID
    // Location ID
    // Accepted: 0, 1

function book_a_session_create_practitioner_location_table() {
    
    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_practitioner_location';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
      practitioner_id mediumint(9) NOT NULL,
      location_id mediumint(9) NOT NULL,
      accepted tinyint(1) NOT NULL,
      PRIMARY KEY  (practitioner_id,location_id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}

// Create Location Schedule Table

    // Location ID
    // Schedule ID

function book_a_session_create_location_schedule_table() {
    
    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_location_schedule';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
      location_id mediumint(9) NOT NULL,
      schedule_id mediumint(9) NOT NULL,
      PRIMARY KEY  (location_id,schedule_id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}
        
// Create Timetable Table

function book_a_session_create_timetable_table() {

    $unavailable_string = implode( ', ', book_a_session_get_unavailable_array() );

    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_timetable';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
      id mediumint(9) NOT NULL AUTO_INCREMENT, 
      date date NOT NULL,
      start_datetime datetime NOT NULL,
      end_datetime datetime NOT NULL,
      schedule_id mediumint(9) NOT NULL,
      order_id varchar(10),
      service_id mediumint(9),
      location_id mediumint(9),
      practitioner_id mediumint(9),
      client_id mediumint(9),
      unavailable enum($unavailable_string),
      PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}

// Create Practitioner Schedule Table

    // Practitioner ID
    // Schedule ID

function book_a_session_create_practitioner_schedule_table() {
    
    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_practitioner_schedule';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
      practitioner_id mediumint(9) NOT NULL,
      schedule_id mediumint(9) NOT NULL,
      PRIMARY KEY  (practitioner_id,schedule_id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}

// Create Booking Table

    // ID
    // Client Datetime
    // Session Date
    // Schedule ID
    // Bundle ID: Client Datetime + User ID
    // Service ID
    // Region ID
    // Practitioner ID
    // User ID
    // Location ID
    // Currency ID
    // Session Total
    // Payment method ID
    // VFC Mobile Number: If VFC
    // Booked: 0, 1
    // Booked Pending Payment: 0, 1
    // Completed: 0, 1
    // No Show Client: 0, 1
    // No Show Practitioner: 0, 1
    // Cash Paid: 0, 1
    // VFC Paid: 0, 1
    // Amount Paid
    // Note

function book_a_session_create_booking_table() {
    
    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_booking';

    $charset_collate = $wpdb->get_charset_collate();
    $booking_status_string = implode( ', ', book_a_session_get_booking_status_array() );
    $payment_status_string = implode( ', ', book_a_session_get_payment_status_array() );
    
    $sql = "CREATE TABLE $table_name (
      id mediumint(9) NOT NULL AUTO_INCREMENT,
      client_datetime datetime NOT NULL,
      session_date date NOT NULL,
      schedule_id mediumint(9) NOT NULL,
      bundle_id mediumint(9) NOT NULL,
      order_id varchar(10) NOT NULL,
      service_id mediumint(9) NOT NULL,
      region_id mediumint(9) NOT NULL,
      practitioner_id mediumint(9) NOT NULL,
      user_id mediumint(9) NOT NULL,
      location_id mediumint(9) NOT NULL,
      currency_id mediumint(9) NOT NULL,
      session_total decimal(10, 2) NOT NULL,
      payment_method_id mediumint(9) NOT NULL,
      vfc_mobile varchar(55),
      booking_status enum($booking_status_string) NOT NULL,
      payment_status enum($payment_status_string) NOT NULL,
      amount_paid decimal(10, 2),
      note varchar(255),
      PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}

// Create Invoice Table

function book_a_session_create_invoice_table() {
    
    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_invoice';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
      id mediumint(9) NOT NULL AUTO_INCREMENT,
      issue_date date NOT NULL,
      order_id varchar(10) NOT NULL,
      user_id mediumint(9) NOT NULL,
      vfc_invoice tinyint(1) NOT NULL,
      vfc_mobile_number varchar(15),
      due tinyint(1) NOT NULL,
      due_by_date datetime,
      due_by_instruction varchar(255),
      total_overridden tinyint(1) NOT NULL,
      currency_id mediumint(9) NOT NULL,
      base_price decimal(10, 2) NOT NULL,
      location_charge decimal(10, 2) NOT NULL,
      discount decimal(10, 2) NOT NULL,
      quantity mediumint(9) NOT NULL,
      total decimal(10, 2) NOT NULL,
      grand_total decimal(10, 2) NOT NULL,
      amount_paid decimal(10, 2) NOT NULL,
      grand_total_due decimal(10, 2) NOT NULL,
      PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}
    
// Create Payment Table

function book_a_session_create_payment_table() {
    
    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_payment';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
      id mediumint(9) NOT NULL AUTO_INCREMENT,
      payment_method_id mediumint(9) NOT NULL,
      order_id varchar(10) NOT NULL,
      invoice_id mediumint(9), 
      payment_id varchar(128),
      total decimal(10, 2) NOT NULL,
      success tinyint(1) NOT NULL,
      note varchar(10000),
      PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}

// Create Location Charge Table

function book_a_session_create_location_charge_table() {

    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_location_charge';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
        location_id mediumint(9) NOT NULL,
        currency_id  mediumint(9) NOT NULL,
        addition_charge decimal(10, 2) NOT NULL,
        PRIMARY KEY  (location_id,currency_id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}

// Create Bundle Currency Table

function book_a_session_create_bundle_currency_table() {

    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_bundle_currency';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
        quantity mediumint(9) NOT NULL,
        currency_id  mediumint(9) NOT NULL,
        discount decimal(10, 2) NOT NULL,
        PRIMARY KEY  (quantity,currency_id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}

// Create Bundle Payment Method Table

function book_a_session_create_bundle_payment_method_table() {

    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_bundle_payment_method';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
        quantity mediumint(9) NOT NULL,
        payment_method_id  mediumint(9) NOT NULL,
        PRIMARY KEY  (quantity,payment_method_id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}

// Create Location Payment Method Table

function book_a_session_create_location_payment_method_table() {

    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_location_payment_method';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
        location_id mediumint(9) NOT NULL,
        payment_method_id  mediumint(9) NOT NULL,
        PRIMARY KEY  (location_id,payment_method_id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}

// Create Service Price Table

function book_a_session_create_service_price_table() {

    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_service_price';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
        service_id mediumint(9) NOT NULL,
        currency_id  mediumint(9) NOT NULL,
        session_price decimal(10, 2) NOT NULL,
        PRIMARY KEY  (service_id,currency_id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}

// Create Service Location Table

function book_a_session_create_service_location_table() {

    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_service_location';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
        service_id mediumint(9) NOT NULL,
        location_id  mediumint(9) NOT NULL,
        PRIMARY KEY  (service_id,location_id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}

// Create Country Table

function book_a_session_create_country_table() {

    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_country';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
        code varchar(2) NOT NULL,
        name  varchar(255) NOT NULL,
        PRIMARY KEY  (code)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}

// Create Region Country Table

function book_a_session_create_region_country_table() {

    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_region_country';

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
        country_code varchar(2) NOT NULL,
        region_id  mediumint(9) NOT NULL,
        PRIMARY KEY  (country_code, region_id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}

function book_a_session_create_order_table() {

    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_order';

    $charset_collate = $wpdb->get_charset_collate();
    $booking_status_string = implode( ', ', book_a_session_get_booking_status_array() );
    $payment_status_string = implode( ', ', book_a_session_get_payment_status_array() );
    
    $sql = "CREATE TABLE $table_name (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        order_id varchar(10) NOT NULL,
        created datetime NOT NULL,
        session tinyint(1),
        project tinyint(1),
        quantity mediumint(9) NOT NULL,
        service_id mediumint(9) NOT NULL,
        region_id mediumint(9) NOT NULL,
        practitioner_id mediumint(9) NOT NULL,
        user_id mediumint(9) NOT NULL,
        location_id mediumint(9) NOT NULL,
        payment_method_id mediumint(9) NOT NULL,
        currency_id mediumint(9) NOT NULL,
        base_price decimal(10, 2) NOT NULL,
        location_charge decimal(10, 2) NOT NULL,
        discount decimal(10, 2) NOT NULL,
        total decimal(10, 2) NOT NULL,
        amount_paid decimal(10, 2) NOT NULL,          
        total_due decimal(10, 2) NOT NULL,
        booking_status enum($booking_status_string) NOT NULL,
        payment_status enum($payment_status_string) NOT NULL,
        vfc_mobile varchar(55),
        note varchar(255),
        PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}

function book_a_session_create_session_table() {

    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_session';

    $charset_collate = $wpdb->get_charset_collate();
    
    $sql = "CREATE TABLE $table_name (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        order_id varchar(10) NOT NULL,
        schedule_id mediumint(9) NOT NULL,
        date date NOT NULL,
        start_datetime datetime NOT NULL,
        end_datetime datetime NOT NULL,
        PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}

function book_a_session_create_project_table() {

    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_project';
    $booking_status_string = implode( ', ', book_a_session_get_booking_status_array() );
    $payment_status_string = implode( ', ', book_a_session_get_payment_status_array() );

    $charset_collate = $wpdb->get_charset_collate();
    
    $sql = "CREATE TABLE $table_name (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        order_id varchar(10) NOT NULL,
        created datetime NOT NULL,
        start_date date NOT NULL,
        start_schedule_id mediumint(9) NOT NULL,
        start_datetime datetime NOT NULL,
        end_date date NOT NULL,
        end_schedule_id mediumint(9) NOT NULL,
        end_datetime datetime NOT NULL,
        booking_status enum($booking_status_string) NOT NULL,
        payment_status enum($payment_status_string) NOT NULL,
        PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}

function book_a_session_create_message_table() {

    global $wpdb;
    $table_name = $wpdb->prefix . 'book_a_session_message';

    $charset_collate = $wpdb->get_charset_collate();
    
    $sql = "CREATE TABLE $table_name (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        order_id varchar(10) NOT NULL,
        date date NOT NULL,
        time time NOT NULL,
        datetime datetime NOT NULL,
        user_id mediumint(9) NOT NULL,
        content text NOT NULL,
        read_practitioner datetime NULL,
        read_client datetime NULL,
        PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
}



    // Populate timetable. This is a temporary helper function that should be removed as it will be obsolete once development and testing is complete.
    // This gets all bookings and populates the timetable table based on those bookings found. Once everything is complete, any new bookings will be
    // inserted into the timetable automatically so there would be no need for this function.
    /*
    function book_a_session_populate_timetable() {

        global $wpdb;
        $table_name_timetable = $wpdb->prefix . "book_a_session_timetable";
        $table_name_booking = $wpdb->prefix . "book_a_session_booking";

        $add_timetable_db_result = array();

        $bookings = $wpdb->get_results(
            "
            SELECT id, session_date, schedule_id, service_id, practitioner_id, user_id, location_id 
            FROM $table_name_booking
            ", OBJECT );

        if ( $bookings ) {

            if ( is_array( $bookings ) ) {

                // Prepare timetable insertion

                foreach ( $bookings as $booking_single ) {

                    // Get datetimes

                    $datetime = book_a_session_get_session_time( $booking_single->schedule_id, false, $booking_single->session_date );

                    // Prepare timetable values for each booking
    
                    $timetable_value_array = array(
    
                        'date'              => $booking_single->session_date,
                        'start_datetime'    => $datetime["start_datetime_object"]->format( "Y-m-d H:i:s" ),
                        'end_datetime'      => $datetime["end_datetime_object"]->format( "Y-m-d H:i:s" ),
                        'schedule_id'       => $booking_single->schedule_id,
                        'service_id'        => $booking_single->service_id,
                        'booking_id'        => $booking_single->id, 
                        'location_id'       => $booking_single->location_id,
                        'practitioner_id'   => $booking_single->practitioner_id, 
                        'client_id'         => $booking_single->user_id,
    
                    );
    
                    // Prepare timetable value formats for each booking
    
                    $timetable_format_array = array(
    
                        '%s', // Date
                        '%s', // Start Datetime
                        '%s', // End Datetime
                        '%d', // Schedule ID
                        '%d', // Service ID
                        '%d', // Booking ID
                        '%d', // Location ID
                        '%d', // Practitioner ID
                        '%d', // Client ID
    
                    );
    
                    // Add Timetable entries
    
                    $add_timetable_db_result[] = $wpdb->insert( $table_name_timetable, $timetable_value_array, $timetable_format_array );

                }

            }

        }   

    }
*/
register_activation_hook( __FILE__, 'book_a_session_activation_error' );

function book_a_session_activation_error() {
    file_put_contents( __DIR__ . '/book-a-session-activation-error-log.txt', ob_get_contents() );
}