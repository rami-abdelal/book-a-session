<?php // Book A Session - Services

// Exit if file is called directly

if ( ! defined( 'ABSPATH' ) ) exit;

function book_a_session_services(){

    /*

    $services = book_a_session_get_table_array( "service", "name", "ASC", "*" );

    ?><div class="book-a-session-services"><?php

    foreach( $services as $service ) :

        $service_image = ! empty( $service->image_id ) ? wp_get_attachment_image_src( $service->image_id, "full" )[0] : false;

        ?>

        <div class="book-a-session-card-large book-a-session-card book-a-session-animated book-a-session-service">
            <div class="book-a-session-service-image" <?php if ( $service_image ) echo 'style="background-image:url(' . $service_image . ');"'; ?>>
                <div class="book-a-session-scrim"><h2><?php echo $service->name ?></h2></div>
            </div>
                <div class="book-a-session-service-description"><?php echo $service->description ?></div>
        </div>

        <?php

    endforeach;

    ?></div><?php

    */

    book_a_session_frontend( "services" );
    
}